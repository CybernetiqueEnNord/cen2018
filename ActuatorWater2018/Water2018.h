/*
 * Water2018.h
 *
 *  Created on: 21 avr. 2018
 *      Author: Emmanuel
 */

#ifndef WATER2018_H_
#define WATER2018_H_

#include "A4988.h"
#include "Constants.h"
#include "Program.h"
#include "SoftWire.h"
#include "Servo.h"
#include "Wire.h"

#define slave Wire

enum class Color {
	Undefined,
	Orange,
	Green
};

enum class Action {
	None,
	SetInitialPosition,
	HandleBee,
	HandleTap,
	OpenDispenser
};

class Water2018: public Program {
private:
	static uint8_t slaveParametersCount;
	static uint8_t slaveParameters[8];

	static void i2cHandleSlaveInput(int bytesCount);
	static void i2cHandleSlaveRequest();
	static void i2cHandleCommand();

	uint8_t i2cSlaveAddress;
	SoftWire& master;
	Color color = Color::Undefined;
	Action command = Action::None;
	//Store the last required position.
	//Usefull to vibrate the servo and set it back to the required position
	int position_servo_repartiteur = SERVO_REPARTITEUR_CENTER;

	void handleAction();

	void i2cScanMasterBus();
	void i2cSendStatus();
	void initializeActuators();
	void initializeMasterI2C();
	void initializeSlaveI2C();
	bool isBusy() const;

	void printI2CAddress();

	// Abeille
	void abeilleGetUp();
	void abeilleGetIn();
	void handleBee();

	// Distributeur
	void handleOpenDispenser();
	void handleTap();
	void arreter_brushless();
	void centrer_servo_repartiteur();
	void demarrer_brushless();
	void positionInitialeServo();
	void servo_repartiteur_centre();
	void servo_repartiteur_chateau_deau();
	void servo_repartiteur_droite();
	void servo_repartiteur_gauche();
	void servo_repartiteur_epuration();
	void servo_repartiteur_fond_droite();
	void servo_repartiteur_fond_gauche();
	void stepper_roulette_prepare_long_rotation();
	void stepper_controleur_descente_prepare_long_rotation();
	void testBrushless();
	void testServoRepartiteur();
	void testStepperControleurDescente();
	void testStepperRoulette();
	void traiterDistributeurComplet();
	void waitAndComputeStepper(unsigned long milliseconds);
	void waitAndComputeStepperAndVibrateServo(long milliseconds);

	void temoinFonctionnement();

protected:
	virtual void enterDebug();
	virtual void runInitialization();
	virtual void runStart();
	virtual void runMain();
	virtual void runDebug();
	virtual void serialParseInput(const char *buffer, int length);

public:
	Water2018(uint8_t address, SoftWire& master);
};

extern A4988 stepper_controleur_descente;
extern A4988 stepper_roulette;

extern Servo servo_repartiteur;
extern Servo brushless;
extern Servo servo_abeille;

extern Water2018 water2018;

#endif /* WATER2018_H_ */
