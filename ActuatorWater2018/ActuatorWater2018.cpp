#include "SoftWire.h"
#include "Water2018.h"
#include "../Commons/i2c/ActuatorWater2018Commands.h"

SoftWire masterSoftWire;
Water2018 water2018(I2C_ADDRESS, masterSoftWire);

A4988 stepper_controleur_descente(MOTOR_STEPS, CONTROLEUR_DESCENTE_DIR, CONTROLEUR_DESCENTE_STEP, CONTROLEUR_DESCENTE_ENABLE);
A4988 stepper_roulette(MOTOR_STEPS, ROULETTE_DIR, ROULETTE_STEP, ROULETTE_ENABLE);

Servo servo_repartiteur;
Servo brushless;
Servo servo_abeille;

void setupPins() {
	//Servo pin must be output
	pinMode(PIN_SERVO_REPARTITEUR, OUTPUT);
	pinMode(PIN_BRUSHLESS, OUTPUT);
	pinMode(PIN_SERVO_ABEILLE, OUTPUT);

	//Attach servo and write safe value
	servo_repartiteur.attach(PIN_SERVO_REPARTITEUR);
	servo_repartiteur.write(SERVO_REPARTITEUR_CENTER);

	//Attach servo, and write safe value
	servo_abeille.attach(PIN_SERVO_ABEILLE);
	servo_abeille.write(SERVO_ABEILLE_CENTER);

	//Attach servo and write safe value
	brushless.attach(PIN_BRUSHLESS);
	brushless.write(90);
}

void setup() {
	setupPins();

	Serial.begin(115200);
	Serial.setTimeout(3);

	water2018.init();
}

void loop() {
	water2018.loop();
}
