#ifndef PROGRAM_H
#define PROGRAM_H

#include "Arduino.h"

#define ARRAY_SIZE(x) (sizeof x / sizeof(x[0]))

#define COMMAND_SERIAL_EXIT 'x'

enum class State {
  Init,
  Start,
  Main,
  Debug
};

enum class MatchSide
{
	Undefined,
	Green,
	Orange,
};

enum class CubeColor
{
	Not_A_Cube,
	Black,
	Blue,
	Green,
	Orange,
	Yellow,
	White
};

enum class Orientation
{
	// north of the table is where the construction plan is scratched
	North,
	// east (right) is the orange side
	East,
	// south is the side where the bees live
	South,
	// west (left) is the green side
	West,
};

enum class CubeLocation
{
	// the cubes are in the green side of the table
	NearestGreen,
	// the cubes are in the orange side of the table
	NearestOrange,
};

enum class TakingPosition
{
	/**
	 *
	 * Left Center Right
	 *      +---+
	 *      |   |       <== Away row
	 *      +---+
	 * +---++---++---+
	 * |   ||   ||   |  <== Middle row
	 * +---++---++---+
	 *      +---+
	 *      |   |       <== Front row
	 *      +---+
	 *
	 *     +-----+
	 *     |     |
	 * +---+     +---+
	 * |    robot    |
	 * +-------------+
	 */

	FrontCenter,
	MiddleLeft,
	MiddleCenter,
	MiddleRight,
	AwayCenter,
};

enum class RackEmplacement
{
	Not_An_Emplacement,
	Level1Left,
	Level1Right,
	Level2Left,
	Level2Right,
	Level3Left,
	Level3Right,
};

enum class TowerLevel
{
	Level1,
	Level2,
	Level3,
	Level4,
	Level5,
};

class Program {
  private:
    State state = State::Init;
    void serialHandleInput();

  protected:
    static PROGMEM const char * const versionString;

    virtual void enterDebug();
    virtual void exitDebug();
    virtual void runInitialization();
    virtual void runStart();
    virtual void runMain();
    virtual void runDebug();
    virtual void serialParseInput(const char *buffer, int length);
    void serialPrintError();
    void serialPrintHex(uint8_t value, bool addNewLine = true);
    void serialPrintSuccess();
    void serialPrintVersion();

	public:
    Program();
    void init();
    void loop();
};

#endif
