/*
 * Pins.h
 *
 *  Created on: 21 avr. 2018
 *      Author: Emmanuel
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

// Pin Mapping
#define CONTROLEUR_DESCENTE_DIR 7
#define CONTROLEUR_DESCENTE_STEP 6
#define CONTROLEUR_DESCENTE_ENABLE 5

#define ROULETTE_DIR 3
#define ROULETTE_STEP 4
#define ROULETTE_ENABLE 2

#define PIN_SERVO_REPARTITEUR 8
#define PIN_BRUSHLESS 9
#define PIN_SERVO_ABEILLE 10


// Brushless
#define POWER_BRUSHLESS 58 // minimum 51

// SERVO
#define DELAY_SECOUAGE 300
#define QUANTITE_SECOUAGE 12

#define SERVO_REPARTITEUR_CENTER 90
#define SERVO_REPARTITEUR_GAUCHE 130
#define SERVO_REPARTITEUR_DROITE 50
#define SERVO_REPARTITEUR_FOND_GAUCHE 180
#define SERVO_REPARTITEUR_FOND_DROITE 0

#define SERVO_ABEILLE_CENTER 40
#define SERVO_ABEILLE_IN 5
#define SERVO_ABEILLE_UP 95

// STEPPER Constants
#define MOTOR_STEPS 200
// Target RPM for cruise speed
#define RPM_LOW 40
#define RPM_HIGH 200
#define RPM_HIGH_PLUS 400
// Acceleration and deceleration values are always in FULL steps / s^2
#define MOTOR_ACCEL 2000
#define MOTOR_DECEL 2000
// Microstepping mode. If you hardwired it to save pins, set to the same value here.
#define MICROSTEPS 1

#endif /* CONSTANTS_H_ */
