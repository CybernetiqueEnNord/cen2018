#include "Water2018.h"

#include <avr/pgmspace.h>
#include <Arduino.h>
#include <BasicStepperDriver.h>
#include <HardwareSerial.h>
#include <stdint.h>
#include <WString.h>

#include "SerialCommands.h"

#define log(x) Serial.println(x)

PROGMEM const char * const Program::versionString = "vWater " __DATE__ " - " __TIME__;

Water2018::Water2018(uint8_t address, SoftWire& master) :
		Program(), i2cSlaveAddress(address), master(master) {
}

uint8_t Water2018::slaveParametersCount = 0;

uint8_t Water2018::slaveParameters[8];

void Water2018::initializeSlaveI2C() {
	slave.begin(i2cSlaveAddress);
	slave.onRequest(i2cHandleSlaveRequest);
	slave.onReceive(i2cHandleSlaveInput);
}

void Water2018::initializeMasterI2C() {
	master.begin();
}

void Water2018::initializeActuators() {
	//Delay to be sure the ESC is initialyzed
	delay(3000);

	// abeille
	abeilleGetIn();

	//Initialyse stepper
	stepper_controleur_descente.begin(RPM_HIGH, MICROSTEPS);
	stepper_controleur_descente.setSpeedProfile(stepper_controleur_descente.LINEAR_SPEED, MOTOR_ACCEL, MOTOR_DECEL);
	stepper_controleur_descente.disable();

	//Initialyse stepper
	stepper_roulette.begin(RPM_HIGH, MICROSTEPS);
	stepper_roulette.setSpeedProfile(stepper_roulette.LINEAR_SPEED, MOTOR_ACCEL, MOTOR_DECEL);
	stepper_roulette.disable();

	//Usefull tests to debug:
	//testStepperControleurDescente();
	//testStepperRoulette();
	//testServoRepartiteur();
	//testBrushless();

	//Run all actuator to asses good working conditions.
	//MUST be run in real cup.
	//ROU: disabled to fasten devlopment
	temoinFonctionnement();

	//Set the Servo in initial position, ready to open the distributor
	positionInitialeServo();

	delay(5000);
}

void Water2018::runInitialization() {
	Program::runInitialization();
	initializeSlaveI2C();
	initializeMasterI2C();
	initializeActuators();
}

void Water2018::runStart() {
	Program::runStart();
}

void Water2018::runMain() {
	Program::runMain();
	handleAction();
}

void Water2018::runDebug() {
	Program::runDebug();
	uint8_t count = slaveParametersCount;
	if (count > 0) {
		Serial.print(F("i2c buffer: "));
		for (uint8_t i = 0; i < count; i++) {
			Serial.print((char) slaveParameters[i]);
		}
		Serial.println();
		slaveParametersCount = 0;
	}
	handleAction();
}

void Water2018::i2cHandleSlaveInput(int bytesCount) {
	unsigned int i = 0;
	while (slave.available() > 0 && i < min(ARRAY_SIZE(slaveParameters), bytesCount)) {
		char c = slave.read();
		slaveParameters[i++] = c;
	}
	slaveParametersCount = i;
}

void Water2018::i2cHandleSlaveRequest() {
	if (slaveParametersCount != 0) {
		i2cHandleCommand();
		slaveParametersCount = 0;
	}
}

bool Water2018::isBusy() const {
	return command != Action::None;
}

void Water2018::i2cSendStatus() {
	slave.write(command == Action::None ? I2C_STATUS_READY : I2C_STATUS_BUSY);
}

void Water2018::i2cHandleCommand() {
	if (slaveParametersCount == 0) {
		water2018.i2cSendStatus();
	} else if (slaveParameters[0] == I2C_COMMAND_STATUS) {
		water2018.i2cSendStatus();
	} else {
		if (water2018.isBusy()) {
			return;
		}
		switch (slaveParameters[0]) {
			case I2C_COMMAND_SET_COLOR_GREEN:
				water2018.color = Color::Green;
				water2018.command = Action::SetInitialPosition;
				break;

			case I2C_COMMAND_SET_COLOR_ORANGE:
				water2018.color = Color::Orange;
				water2018.command = Action::SetInitialPosition;
				break;

			case I2C_COMMAND_HANDLE_BEE:
				water2018.command = Action::HandleBee;
				break;

			case I2C_COMMAND_HANDLE_TAP:
				water2018.command = Action::HandleTap;
				break;

			case I2C_COMMAND_OPEN_DISPENSER:
				water2018.command = Action::OpenDispenser;
				break;
		}
	}
}

void Water2018::enterDebug() {
	Program::enterDebug();
}

void Water2018::i2cScanMasterBus() {
	Serial.println(F(":scanning I2C bus"));
	for (uint8_t address = 1; address < 127; address++) {
		// The i2c_scanner uses the return value of
		// the Write.endTransmisstion to see if
		// a device did acknowledge to the address.
		master.beginTransmission(address);
		uint8_t error = master.endTransmission();

		if (error == 0) {
			Serial.print('D');
			serialPrintHex(address);
		}
	}
	serialPrintSuccess();
}

void Water2018::printI2CAddress() {
	Serial.print('a');
	serialPrintHex(i2cSlaveAddress);
}

void Water2018::serialParseInput(const char *buffer, int length) {
	switch (buffer[0]) {
		case COMMAND_SERIAL_I2C_SCAN:
			i2cScanMasterBus();
			break;

		case COMMAND_SERIAL_I2C_ADDRESS:
			printI2CAddress();
			break;

		case COMMAND_SERIAL_SET_COLOR_GREEN:
			color = Color::Green;
			command = Action::SetInitialPosition;
			break;

		case COMMAND_SERIAL_SET_COLOR_ORANGE:
			color = Color::Orange;
			command = Action::SetInitialPosition;
			break;

		case COMMAND_SERIAL_HANDLE_BEE:
			command = Action::HandleBee;
			break;

		case COMMAND_SERIAL_HANDLE_TAP:
			command = Action::HandleTap;
			break;

		case COMMAND_SERIAL_OPEN_DISPENSER:
			command = Action::OpenDispenser;
			break;

		default:
			Program::serialParseInput(buffer, length);
			break;
	}
}

/***********************************************/
//Action core methods:
//*
//*
void Water2018::abeilleGetUp() {
	servo_abeille.write(SERVO_ABEILLE_UP);
}

void Water2018::abeilleGetIn() {
	servo_abeille.write(SERVO_ABEILLE_IN);
}

void Water2018::temoinFonctionnement() {
	centrer_servo_repartiteur();
	delay(500);
	servo_repartiteur_gauche();
	delay(500);
	servo_repartiteur_droite();
	delay(500);
	centrer_servo_repartiteur();
	stepper_roulette.enable();
	stepper_roulette.rotate(200);
	stepper_roulette.disable();

	stepper_controleur_descente.enable();
	stepper_controleur_descente.rotate(200);
	stepper_controleur_descente.disable();

	brushless.write(82);
	delay(500);
	brushless.write(90);
}

void Water2018::traiterDistributeurComplet() {

	//Demarrer brushless
	demarrer_brushless();

	//Preparer la rotation du controleur de descente
	stepper_controleur_descente_prepare_long_rotation();

	//Ouverture locket
	centrer_servo_repartiteur();
	waitAndComputeStepperAndVibrateServo(800);

	for (int i = 0; i < 10; i++) {
		stepper_roulette_prepare_long_rotation();
		servo_repartiteur_chateau_deau();
		waitAndComputeStepperAndVibrateServo(800);
		stepper_roulette.stop();
		stepper_roulette.disable();
		centrer_servo_repartiteur();
		waitAndComputeStepperAndVibrateServo(800);
	}

	arreter_brushless();
	stepper_controleur_descente.stop();
	stepper_controleur_descente.disable();
	positionInitialeServo();
}
//*
//*
//Action methods
/***********************************************/

/***********************************************/
//Method to wait and keep running asynchronous task (Stepper, vibrating...)
//*
//*
void Water2018::waitAndComputeStepper(unsigned long milliseconds) {
	unsigned long startTime = millis();
	while ((millis() - startTime) < milliseconds) {
		stepper_roulette.nextAction();
		stepper_controleur_descente.nextAction();
	}
}

void Water2018::waitAndComputeStepperAndVibrateServo(long milliseconds) {
	unsigned long startTime = millis();
	unsigned long lastSecouageTime = millis();
	boolean toogle = false;
	while ((millis() - startTime) < milliseconds) {
		stepper_roulette.nextAction();
		stepper_controleur_descente.nextAction();

		if ((millis() - lastSecouageTime) < DELAY_SECOUAGE) {
			lastSecouageTime = millis();
			toogle = !toogle;
			if (toogle) {
				servo_repartiteur.write(position_servo_repartiteur - QUANTITE_SECOUAGE / 2);
			} else {
				servo_repartiteur.write(position_servo_repartiteur + QUANTITE_SECOUAGE / 2);
			}
		}
	}
	servo_repartiteur.write(position_servo_repartiteur);
}

//*
//*
//Method to wait and keep running asynchronous task (Stepper, vibrating...)
/***********************************************/

/***********************************************/
//Different test to debug:
//*
//*
void Water2018::testStepperRoulette() {
	while (1) {
		stepper_roulette.enable();
		delay(100);
		stepper_roulette.rotate(700);
		delay(100);
		stepper_roulette.rotate(-700);
		delay(100);
		stepper_roulette.disable();
	}
}

void Water2018::testStepperControleurDescente() {
	while (1) {
		stepper_controleur_descente.enable();
		delay(100);
		stepper_controleur_descente.rotate(700);
		delay(100);
		stepper_controleur_descente.rotate(-700);
		stepper_controleur_descente.disable();
	}
}

void Water2018::testBrushless() {
	while (true) {
		delay(3000);
		brushless.write(82);
		delay(3000);
		brushless.write(90);
	}
}

void Water2018::testServoRepartiteur() {
	while (1) {
		centrer_servo_repartiteur();
		delay(2000);
		servo_repartiteur_gauche();
		delay(2000);
		centrer_servo_repartiteur();
		delay(2000);
		servo_repartiteur_droite();
		delay(2000);
		servo_repartiteur_fond_gauche();
		delay(3000);
		servo_repartiteur_fond_droite();
		delay(3000);
	}
}
//*
//*
//Different test to debug
/***********************************************/

/***********************************************/
//Different method to abstract color handling:
//*
//*
void Water2018::stepper_controleur_descente_prepare_long_rotation() {
	if (color == Color::Orange) {
		stepper_controleur_descente.enable();
		stepper_controleur_descente.startRotate(50 * 360);
	} else if (color == Color::Green) {
		stepper_controleur_descente.enable();
		stepper_controleur_descente.startRotate(-50 * 360);
	}
}

void Water2018::stepper_roulette_prepare_long_rotation() {
	if (color == Color::Orange) {
		stepper_roulette.enable();
		stepper_roulette.startRotate(-10 * 360);
	} else if (color == Color::Green) {
		stepper_roulette.enable();
		stepper_roulette.startRotate(10 * 360);
	}
}

void Water2018::positionInitialeServo() {
	if (color == Color::Orange) {
		servo_repartiteur_fond_droite();
		delay(500);
	} else if (color == Color::Green) {
		servo_repartiteur_fond_gauche();
		delay(500);
	}
}

void Water2018::servo_repartiteur_chateau_deau() {
	if (color == Color::Orange) {
		servo_repartiteur_gauche();
	} else if (color == Color::Green) {
		servo_repartiteur_droite();
	}
}
void Water2018::servo_repartiteur_epuration() {
	if (color == Color::Orange) {
		servo_repartiteur_droite();
	} else if (color == Color::Green) {
		servo_repartiteur_gauche();
	}
}

void Water2018::demarrer_brushless() {
	if (color == Color::Orange) {
		//brushless.write(90+7);
		brushless.writeMicroseconds(1472 + POWER_BRUSHLESS);

	} else if (color == Color::Green) {
		//brushless.write(90-7);
		brushless.writeMicroseconds(1472 - POWER_BRUSHLESS);
	}
}

void Water2018::arreter_brushless() {
	brushless.write(90);
}
//*
//*
//Different method to abstract color handling:
/***********************************************/

/***********************************************/
//Different method to set servo position:
//*
//*
//Set the servo to the center position.
void Water2018::centrer_servo_repartiteur() {
	position_servo_repartiteur = SERVO_REPARTITEUR_CENTER;
	servo_repartiteur.write(position_servo_repartiteur);
}
//Set the servo to Left position.
//Warning, you should better call servo_repartiteur_chateau_deau or servo_repartiteur_epuration, witch handle color
void Water2018::servo_repartiteur_gauche() {
	position_servo_repartiteur = SERVO_REPARTITEUR_GAUCHE;
	servo_repartiteur.write(position_servo_repartiteur);
}

//Set the servo to Right position.
//Warning, you should better call servo_repartiteur_chateau_deau or servo_repartiteur_epuration, witch handle color
void Water2018::servo_repartiteur_droite() {
	position_servo_repartiteur = SERVO_REPARTITEUR_DROITE;
	servo_repartiteur.write(position_servo_repartiteur);
}

//Set the servo to Left max position, ready to open lock.
//Warning, you should better call positionInitialeServo, witch handle color
void Water2018::servo_repartiteur_fond_gauche() {
	position_servo_repartiteur = SERVO_REPARTITEUR_FOND_GAUCHE;
	servo_repartiteur.write(position_servo_repartiteur);
}

//Set the servo to Right max position, ready to open lock.
//Warning, you should better call positionInitialeServo, witch handle color
void Water2018::servo_repartiteur_fond_droite() {
	position_servo_repartiteur = SERVO_REPARTITEUR_FOND_DROITE;
	servo_repartiteur.write(position_servo_repartiteur);
}

void Water2018::servo_repartiteur_centre() {
	position_servo_repartiteur = SERVO_REPARTITEUR_CENTER;
	servo_repartiteur.write(position_servo_repartiteur);
}

void Water2018::handleTap() {
	log(F("Distributeur d�marr�"));
	traiterDistributeurComplet();
	log(F("Distributeur termin�"));
}

void Water2018::handleOpenDispenser() {
	log(F("Ouvrir distributeur d�marr�"));
	servo_repartiteur_centre();
	log(F("Ouvrir distributeur termin�"));
}

void Water2018::handleBee() {
	log(F("Abeille d�marr�"));
	abeilleGetUp();
	delay(10000);
	abeilleGetIn();
	log(F("Abeille termin�"));
}

void Water2018::handleAction() {
	switch (command) {
		case Action::SetInitialPosition:
			log(F("Changement couleur"));
			positionInitialeServo();
			break;

		case Action::HandleBee:
			handleBee();
			break;

		case Action::HandleTap:
			handleTap();
			break;

		case Action::OpenDispenser:
			handleOpenDispenser();
			break;

		case Action::None:
			break;
	}

	command = Action::None;
	delay(200);
}
