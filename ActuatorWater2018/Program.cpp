#include "Program.h"

Program::Program() {
}

void Program::serialPrintVersion() {
  Serial.println(versionString);
}

void Program::init() {
  serialPrintVersion();
}

void Program::runInitialization() {
  state = State::Start;
}

void Program::runStart() {
  state = State::Main;
}

void Program::runMain() {
  if (Serial.available()) {
    if (Serial.find("d\n")) {
      enterDebug();
      return;
    }
  }
}

void Program::serialHandleInput() {
  char buffer[10];
  int n = Serial.readBytesUntil('\n', buffer, ARRAY_SIZE(buffer) - 1);
  if (n > 0) {
    buffer[n] = 0;
    serialParseInput(buffer, n);
  }
}

void Program::runDebug() {
  if (Serial.available()) {
    serialHandleInput();
  }
}

void Program::loop() {
	// Serial.write("loop");
  switch (state) {
    case State::Init:
      runInitialization();
      break;

    case State::Start:
      runStart();
      break;

    case State::Main:
      runMain();
      break;

    case State::Debug:
      runDebug();
      break;
  }
}

void Program::enterDebug() {
  state = State::Debug;
  Serial.println(F(":entering debug mode"));
}

void Program::exitDebug() {
  state = State::Start;
  Serial.println(F(":exiting debug mode"));
}

void Program::serialPrintHex(uint8_t value, bool addNewLine) {
  if (value < 16) {
    Serial.print('0');
  }
  Serial.print(value, HEX);
  if (addNewLine) {
    Serial.println();
  }
}

void Program::serialPrintError() {
  Serial.println(F("ERROR"));
}

void Program::serialPrintSuccess() {
  Serial.println(F("OK"));
}

void Program::serialParseInput(const char *buffer, int length)
{
  switch (buffer[0]) {
    case COMMAND_SERIAL_EXIT:
      exitDebug();
      break;

    default:
      goto error;
      break;
  }
  return;

error:
  serialPrintError();
}
