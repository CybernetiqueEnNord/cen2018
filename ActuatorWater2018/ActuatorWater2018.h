#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2018-04-16 23:17:36

#include "Arduino.h"
#include <Servo.h>
#define CONTROLEUR_DESCENTE_DIR 7
#define CONTROLEUR_DESCENTE_STEP 6
#define CONTROLEUR_DESCENTE_ENABLE 5
#define ROULETTE_DIR 3
#define ROULETTE_STEP 4
#define ROULETTE_ENABLE 2
#define PIN_SERVO_REPARTITEUR 8
#define PIN_BRUSHLESS 9
#define DELAY_SECOUAGE 200
#define QUANTITE_SECOUAGE 8
#define SERVO_REPARTITEUR_CENTER 90
#define SERVO_REPARTITEUR_GAUCHE 130
#define SERVO_REPARTITEUR_DROITE 50
#define SERVO_REPARTITEUR_FOND_GAUCHE 180
#define SERVO_REPARTITEUR_FOND_DROITE 0
extern int position_servo_repartiteur;
extern Servo servo_repartiteur;
extern Servo brushless;
#define MOTOR_STEPS 200
#define RPM_LOW 40
#define RPM_HIGH 200
#define RPM_HIGH_PLUS 400
#define MOTOR_ACCEL 2000
#define MOTOR_DECEL 2000
#define MICROSTEPS 1
#include "A4988.h"

void setup() ;
void abeilleGetUp();
void abeilleGetIn();
void temoinFonctionnement() ;
void traiterDistributeurComplet();
void waitAndComputeStepper(unsigned long milliseconds) ;
void waitAndComputeStepperAndVibrateServo(long milliseconds) ;
void testStepperRoulette();
void testStepperControleurDescente();
void testBrushless();
void testServoRepartiteur();
void stepper_controleur_descente_prepare_long_rotation() ;
void stepper_roulette_prepare_long_rotation() ;
void positionInitialeServo() ;
void servo_repartiteur_chateau_deau() ;
void servo_repartiteur_epuration() ;
void demarrer_brushless() ;
void arreter_brushless() ;
void centrer_servo_repartiteur() ;
void servo_repartiteur_gauche() ;
void servo_repartiteur_droite() ;
void servo_repartiteur_fond_gauche() ;
void servo_repartiteur_fond_droite() ;
void loop() ;



#endif
