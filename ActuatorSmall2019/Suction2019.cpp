#include <avr/pgmspace.h>
#include <Arduino.h>
#include <BasicStepperDriver.h>
#include <HardwareSerial.h>
#include <stdint.h>
#include <WString.h>

#include "SerialCommands.h"
#include "Suction2019.h"

#define log(x) Serial.println(x)

PROGMEM const char * const Actuator::versionString = "vSuction " __DATE__ " - " __TIME__;

Suction2019::Suction2019(uint8_t address, SoftWire& master) :
		Actuator(), i2cSlaveAddress(address), master(master) {
}

uint8_t Suction2019::slaveParametersCount = 0;

uint8_t Suction2019::slaveParameters[8];

bool Suction2019::i2cAcknowledged = false;

void Suction2019::initializeSlaveI2C() {
	slave.begin(i2cSlaveAddress);
	slave.onRequest(i2cHandleSlaveRequest);
	slave.onReceive(i2cHandleSlaveInput);
}

void Suction2019::initializeMasterI2C() {
	master.begin();
}

void Suction2019::initializeActuators() {
}

void Suction2019::runInitialization() {
	Actuator::runInitialization();
	initializeSlaveI2C();
	initializeMasterI2C();
	initializeActuators();
}

void Suction2019::runStart() {
	Actuator::runStart();
}

void Suction2019::runMain() {
	Actuator::runMain();
	handleAction();
}

void Suction2019::runDebug() {
	Actuator::runDebug();
	uint8_t count = slaveParametersCount;
	if (count > 0) {
		Serial.print(F("i2c buffer: "));
		for (uint8_t i = 0; i < count; i++) {
			Serial.print((char) slaveParameters[i]);
		}
		Serial.println();
		slaveParametersCount = 0;
	}
	handleAction();
}

void Suction2019::i2cHandleSlaveInput(int bytesCount) {
	unsigned int i = 0;
	while (slave.available() > 0 && i < min(ARRAY_SIZE(slaveParameters), (unsigned ) bytesCount)) {
		char c = slave.read();
		slaveParameters[i++] = c;
	}
	slaveParametersCount = i;
}

void Suction2019::i2cHandleSlaveRequest() {
	if (slaveParametersCount != 0) {
		i2cHandleCommand();
		slaveParametersCount = 0;
	}
}

bool Suction2019::isBusy() const {
	return command != Action::None;
}

void Suction2019::i2cSendStatus() {
	i2cAcknowledged = true;
	slave.write(command == Action::None ? I2C_STATUS_READY : I2C_STATUS_BUSY);
}

void Suction2019::i2cHandleCommand() {
	if (slaveParametersCount == 0) {
		suction2019.i2cSendStatus();
	} else if (slaveParameters[0] == I2C_COMMAND_STATUS) {
		suction2019.i2cSendStatus();
	} else {
		if (suction2019.isBusy()) {
			return;
		}
		i2cAcknowledged = false;
		switch (slaveParameters[0]) {
			case I2C_COMMAND_SET_COLOR_PURPLE:
				suction2019.color = Color::Purple;
				suction2019.command = Action::SetInitialPosition;
				break;

			case I2C_COMMAND_SET_COLOR_YELLOW:
				suction2019.color = Color::Yellow;
				suction2019.command = Action::SetInitialPosition;
				break;

			case I2C_COMMAND_GOLDENIUM_PUMP_ON:
				suction2019.command = Action::PumpOn;
				break;

			case I2C_COMMAND_GOLDENIUM_PUMP_OFF:
				suction2019.command = Action::PumpOff;
				break;

			case I2C_COMMAND_TRIO_PUMP_ON:
				suction2019.command = Action::TrioPumpOn;
				break;

			case I2C_COMMAND_TRIO_PUMP_OFF:
				suction2019.command = Action::TrioPumpOff;
				break;

			case I2C_COMMAND_TRIO_PUMP_OFF_AV_LEFT:
				suction2019.command = Action::TrioPumpAvantOffLeft;
				break;

			case I2C_COMMAND_TRIO_PUMP_OFF_AV_CENTER:
				suction2019.command = Action::TrioPumpAvantOffCenter;
				break;

			case I2C_COMMAND_TRIO_PUMP_OFF_AV_RIGHT:
				suction2019.command = Action::TrioPumpAvantOffRight;
				break;

			case I2C_COMMAND_ARM_DOWN:
				suction2019.command = Action::ArmDown;
				break;

			case I2C_COMMAND_ARM_UP:
				suction2019.command = Action::ArmUp;
				break;

			case I2C_COMMAND_PELLE_RENTREE:
				suction2019.command = Action::PelleRentre;
				break;

			case I2C_COMMAND_PELLE_PRISE:
				suction2019.command = Action::PellePrise;
				break;

			case I2C_COMMAND_PELLE_DEPOSE_GOLDENIUM:
				suction2019.command = Action::PelleDeposeGoldenium;
				break;

			case I2C_COMMAND_CHECK_GOLDENIUM:
				suction2019.command = Action::CheckGoldenium;
				break;

			case I2C_COMMAND_QUERY_GOLDENIUM:
				suction2019.i2cSendGoldeniumDoorStatus();
				suction2019.command = Action::None;
				break;

			case I2C_COMMAND_SET_SERVO_GUIDE:
				suction2019.command = Action::SetServoGuide;
				break;

			case I2C_COMMAND_UNSET_SERVO_GUIDE:
				suction2019.command = Action::UnSetServoGuide;
				break;

			case I2C_COMMAND_KILL:
				suction2019.command = Action::Kill;
				break;
		}
	}
}

void Suction2019::enterDebug() {
	Actuator::enterDebug();
}

void Suction2019::i2cScanMasterBus() {
	Serial.println(F(":scanning I2C bus"));
	for (uint8_t address = 1; address < 127; address++) {
		// The i2c_scanner uses the return value of
		// the Write.endTransmisstion to see if
		// a device did acknowledge to the address.
		master.beginTransmission(address);
		uint8_t error = master.endTransmission();

		if (error == 0) {
			Serial.print('D');
			serialPrintHex(address);
		}
	}
	serialPrintSuccess();
}

void Suction2019::printI2CAddress() {
	Serial.print('a');
	serialPrintHex(i2cSlaveAddress);
}

void Suction2019::serialParseInput(const char *buffer, int length) {
	switch (buffer[0]) {
		case COMMAND_SERIAL_I2C_SCAN:
			i2cScanMasterBus();
			break;

		case COMMAND_SERIAL_I2C_ADDRESS:
			printI2CAddress();
			break;

		case COMMAND_SERIAL_SET_COLOR_PURPLE:
			color = Color::Purple;
			command = Action::SetInitialPosition;
			break;

		case COMMAND_SERIAL_SET_COLOR_YELLOW:
			color = Color::Yellow;
			command = Action::SetInitialPosition;
			break;

		case COMMAND_SERIAL_GOLDENIUM_PUMP_ON:
			command = Action::PumpOn;
			break;

		case COMMAND_SERIAL_GOLDENIUM_PUMP_OFF:
			command = Action::PumpOff;
			break;

		case COMMAND_SERIAL_TRIO_PUMP_ON:
			command = Action::TrioPumpOn;
			break;

		case COMMAND_SERIAL_TRIO_PUMP_OFF:
			command = Action::TrioPumpOff;
			break;

		case I2C_COMMAND_TRIO_PUMP_OFF_AV_LEFT:
			command = Action::TrioPumpAvantOffLeft;
			break;
		case I2C_COMMAND_TRIO_PUMP_OFF_AV_CENTER:
			command = Action::TrioPumpAvantOffCenter;
			break;
		case I2C_COMMAND_TRIO_PUMP_OFF_AV_RIGHT:
			command = Action::TrioPumpAvantOffRight;
			break;

		case COMMAND_SERIAL_ARM_DOWN:
			command = Action::ArmDown;
			break;

		case COMMAND_SERIAL_ARM_UP:
			command = Action::ArmUp;
			break;

		case COMMAND_SERIAL_PELLE_RENTREE:
			command = Action::PelleRentre;
			break;
		case COMMAND_SERIAL_PELLE_PRISE:
			command = Action::PellePrise;
			break;
		case COMMAND_SERIAL_PELLE_DEPOSE_GOLDENIUM:
			command = Action::PelleDeposeGoldenium;
			break;

		case COMMAND_SERIAL_CHECK_GOLDENIUM:
			command = Action::CheckGoldenium;
			break;

		case COMMAND_SERIAL_SET_SERVO_GUIDE:
			suction2019.command = Action::SetServoGuide;
			break;

		case COMMAND_SERIAL_UNSET_SERVO_GUIDE:
			suction2019.command = Action::UnSetServoGuide;
			break;

		default:
			Actuator::serialParseInput(buffer, length);
			break;
	}
}

void Suction2019::handleAction() {
	switch (command) {
		case Action::SetInitialPosition:
			break;

		case Action::PumpOn:
			pumpOn();
			break;

		case Action::PumpOff:
			pumpOff();
			break;

		case Action::ArmDown:
			armDown();
			break;

		case Action::ArmUp:
			armUp();
			break;

		case Action::PellePrise:
			pellePrise();
			break;

		case Action::PelleDeposeGoldenium:
			pelleDeposeGoldenium();
			break;

		case Action::PelleRentre:
			pelleRentre();
			break;

		case Action::TrioPumpOff:
			trioPumpOff();
			break;

		case Action::TrioPumpOn:
			trioPumpOn();
			break;

		case Action::TrioPumpAvantOffLeft:
			trioPumpAvantOffLeft();
			break;

		case Action::TrioPumpAvantOffCenter:
			trioPumpAvantOffCenter();
			break;

		case Action::TrioPumpAvantOffRight:
			trioPumpAvantOffRight();
			break;

		case Action::CheckGoldenium:
			checkGoldenium();
			break;

		case Action::SetServoGuide:
			setServoGuide();
			break;

		case Action::UnSetServoGuide:
			unSetServoGuide();
			break;

		case Action::None:
			break;

		case Action::Kill:
			kill();
			break;
	}

	command = Action::None;
	for (int i = 0; !i2cAcknowledged && i < 200; i++) {
		delay(1);
	}
}

void Suction2019::kill() {
	pumpOff();
	trioPumpOff();
	servoArmLeft.detach();
	servoArmRight.detach();
	servoLeft.detach();
	servoRight.detach();
	servoCheckGoldenium.detach();
	servoGuideGoldeniumYellow.detach();
	servoGuideGoldeniumPurple.detach();
	slave.end();
}

void Suction2019::checkGoldenium() {
	servoCheckGoldenium.write(SERVO_CHECK_DOWN);
	goldeniumDoorOpen = true;
	for (int i = 0; i < 300; i++) {
		goldeniumDoorOpen &= digitalRead(PIN_CHECK_GOLDENIUM) == LOW;
		delay(1);
	}
	servoCheckGoldenium.write(SERVO_CHECK_UP);
	if (getState() == State::Debug) {
		Serial.print("door ");
		Serial.println(goldeniumDoorOpen ? "open" : "closed");
	}
}

void Suction2019::i2cSendGoldeniumDoorStatus() {
	i2cAcknowledged = true;
	slave.write((uint8_t) (goldeniumDoorOpen ? 0xFF : 0));
}

void Suction2019::pumpOn() {
	digitalWrite(PIN_PUMP_GOLDENIUM, HIGH);
}

void Suction2019::pumpOff() {
	digitalWrite(PIN_PUMP_GOLDENIUM, LOW);
}

void Suction2019::trioPumpOn() {
	digitalWrite(PIN_PUMP_CENTER, HIGH);
	digitalWrite(PIN_PUMP_LEFT, HIGH);
	digitalWrite(PIN_PUMP_RIGHT, HIGH);
}

void Suction2019::trioPumpOff() {
	digitalWrite(PIN_PUMP_CENTER, LOW);
	digitalWrite(PIN_PUMP_LEFT, LOW);
	digitalWrite(PIN_PUMP_RIGHT, LOW);
}

void Suction2019::trioPumpAvantOffCenter() {
	digitalWrite(PIN_PUMP_CENTER, LOW);
}
void Suction2019::trioPumpAvantOffRight() {
	digitalWrite(PIN_PUMP_RIGHT, LOW);
}
void Suction2019::trioPumpAvantOffLeft() {
	digitalWrite(PIN_PUMP_LEFT, LOW);
}

void Suction2019::pellePrise() {
	servoLeft.write(SERVO_LEFT_PRISE);
	servoRight.write(SERVO_RIGHT_PRISE);
}

void Suction2019::pelleRentre() {
	servoLeft.write(SERVO_LEFT_RENTRE);
	servoRight.write(SERVO_RIGHT_RENTRE);
}

void Suction2019::pelleDeposeGoldenium() {
	servoLeft.write(SERVO_LEFT_DEPOSE_GOLDENIUM);
	servoRight.write(SERVO_RIGHT_DEPOSE_GOLDENIUM);
}

void Suction2019::armUp() {
	if (color == Color::Purple) {
		servoArmRight.write(SERVO_ARM_RIGHT_UP);
	} else {
		servoArmLeft.write(SERVO_ARM_LEFT_UP);
	}
}

void Suction2019::armDown() {
	if (color == Color::Purple) {
		servoArmRight.write(SERVO_ARM_RIGHT_DOWN);
	} else {
		servoArmLeft.write(SERVO_ARM_LEFT_DOWN);
	}
}

void Suction2019::setServoGuide() {
	if (color == Color::Purple) {
		servoGuideGoldeniumPurple.write(SERVO_GUIDE_SET_PURPLE);
	} else if (color == Color::Yellow) {
		servoGuideGoldeniumYellow.write(SERVO_GUIDE_SET_YELLOW);
	}
}

void Suction2019::unSetServoGuide() {
	if (color == Color::Purple) {
		servoGuideGoldeniumPurple.write(SERVO_GUIDE_UNSET_PURPLE);
	} else if (color == Color::Yellow) {
		servoGuideGoldeniumYellow.write(SERVO_GUIDE_UNSET_YELLOW);
	}
}
