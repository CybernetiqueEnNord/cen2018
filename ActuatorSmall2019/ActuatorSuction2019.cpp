#include "SoftWire.h"

#include "../Commons/i2c/ActuatorSmall2019Commands.h"
#include "Suction2019.h"

SoftWire masterSoftWire;
Suction2019 suction2019(I2C_ADDRESS_ACTUATORSMALL2019, masterSoftWire);

Servo servoArmRight;
Servo servoArmLeft;
Servo servoLeft;
Servo servoRight;
Servo servoCheckGoldenium;
Servo servoGuideGoldeniumYellow;
Servo servoGuideGoldeniumPurple;

void setupPins() {
	//Servo pin must be output
	pinMode(PIN_SERVO_ARM_RIGHT, OUTPUT);
	pinMode(PIN_SERVO_ARM_LEFT, OUTPUT);
	pinMode(PIN_SERVO_PELLE_LEFT, OUTPUT);
	pinMode(PIN_SERVO_PELLE_RIGHT, OUTPUT);
	pinMode(PIN_PUMP_GOLDENIUM, OUTPUT);
	pinMode(PIN_PUMP_CENTER, OUTPUT);
	pinMode(PIN_PUMP_LEFT, OUTPUT);
	pinMode(PIN_PUMP_RIGHT, OUTPUT);
	pinMode(PIN_SERVO_CHECK_GOLDENIUM, OUTPUT);
	pinMode(PIN_CHECK_GOLDENIUM, INPUT);
	pinMode(PIN_SERVO_GUIDE_GOLDENIUM_YELLOW, OUTPUT);
	pinMode(PIN_SERVO_GUIDE_GOLDENIUM_PURPLE, OUTPUT);

	//Attach servo and write safe value
	servoArmRight.attach(PIN_SERVO_ARM_RIGHT);
	servoArmRight.write(SERVO_ARM_RIGHT_UP);

	servoArmLeft.attach(PIN_SERVO_ARM_LEFT);
	servoArmLeft.write(SERVO_ARM_LEFT_UP);

	servoLeft.attach(PIN_SERVO_PELLE_LEFT);
	servoLeft.write(SERVO_LEFT_SAFE);

	servoRight.attach(PIN_SERVO_PELLE_RIGHT);
	servoRight.write(SERVO_RIGHT_SAFE);

	servoCheckGoldenium.attach(PIN_SERVO_CHECK_GOLDENIUM);
	servoCheckGoldenium.write(SERVO_CHECK_UP);

	servoGuideGoldeniumYellow.attach(PIN_SERVO_GUIDE_GOLDENIUM_YELLOW);
	servoGuideGoldeniumYellow.write(SERVO_GUIDE_UNSET_YELLOW);

	servoGuideGoldeniumPurple.attach(PIN_SERVO_GUIDE_GOLDENIUM_PURPLE);
	servoGuideGoldeniumPurple.write(SERVO_GUIDE_UNSET_PURPLE);
}

void setup() {
	setupPins();

	Serial.begin(115200);
	Serial.setTimeout(3);

	suction2019.init();
}

void loop() {
	suction2019.loop();
}
