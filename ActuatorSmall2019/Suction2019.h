/*
 * Water2018.h
 *
 *  Created on: 21 avr. 2018
 *      Author: Emmanuel
 */

#ifndef SUCTION2019_H_
#define SUCTION2019_H_

#include "A4988.h"
#include "Constants.h"
#include "SoftWire.h"
#include "Servo.h"

#include "Actuator.h"
#include "Wire.h"

#define slave Wire

enum class Color {
	Undefined, Yellow, Purple
};

enum class Action {
	None, SetInitialPosition, PumpOn, PumpOff, ArmDown, ArmUp, Kill, PelleRentre, PellePrise, PelleDeposeGoldenium, TrioPumpOn, TrioPumpOff, TrioPumpAvantOffLeft, TrioPumpAvantOffCenter, TrioPumpAvantOffRight, CheckGoldenium, SetServoGuide,UnSetServoGuide
};

class Suction2019: public Actuator {
private:
	static uint8_t slaveParametersCount;
	static uint8_t slaveParameters[8];
	static bool i2cAcknowledged;

	static void i2cHandleSlaveInput(int bytesCount);
	static void i2cHandleSlaveRequest();
	static void i2cHandleCommand();

	uint8_t i2cSlaveAddress;
	SoftWire& master;
	Color color = Color::Undefined;
	Action command = Action::None;
	bool goldeniumDoorOpen = false;

	void handleAction();

	void i2cScanMasterBus();
	void i2cSendStatus();
	void initializeActuators();
	void initializeMasterI2C();
	void initializeSlaveI2C();
	bool isBusy() const;

	void printI2CAddress();

	// Servo
	void moveHigh();
	void moveMedium();
	void moveLow();

	// Pompe
	void pumpOn();
	void pumpOff();
	void trioPumpOn();
	void trioPumpOff();
	void trioPumpAvantOffCenter();
	void trioPumpAvantOffRight();
	void trioPumpAvantOffLeft();

	// Bras
	void armDown();
	void armUp();

	void setServoGuide();
	void unSetServoGuide();

	void pellePrise();
	void pelleDeposeGoldenium();
	void pelleRentre();

	// Goldenium
	void checkGoldenium();
	void i2cSendGoldeniumDoorStatus();

	// arr�t d�finitif
	void kill();

protected:
	virtual void enterDebug();
	virtual void runInitialization();
	virtual void runStart();
	virtual void runMain();
	virtual void runDebug();
	virtual void serialParseInput(const char *buffer, int length);

public:
	Suction2019(uint8_t address, SoftWire& master);
};

extern Suction2019 suction2019;
extern Servo servoArmRight;
extern Servo servoArmLeft;
extern Servo servoRight;
extern Servo servoLeft;
extern Servo servoCheckGoldenium;
extern Servo servoGuideGoldeniumYellow;
extern Servo servoGuideGoldeniumPurple;

#endif /* SUCTION2019_H_ */
