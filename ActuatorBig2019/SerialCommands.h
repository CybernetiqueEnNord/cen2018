/*
 * SerialCommands.h
 *
 *  Created on: 21 avr. 2018
 *      Author: Emmanuel
 */

#ifndef SERIALCOMMANDS_H_
#define SERIALCOMMANDS_H_

#include "../Commons/i2c/ActuatorBig2019Commands.h"

#define COMMAND_SERIAL_I2C_SCAN 's'
#define COMMAND_SERIAL_I2C_ADDRESS 'a'

#endif /* SERIALCOMMANDS_H_ */
