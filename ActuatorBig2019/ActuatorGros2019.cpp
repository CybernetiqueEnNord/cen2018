#include "SoftWire.h"

#include "../Commons/i2c/ActuatorBig2019Commands.h"
#include "Gros2019.h"

SoftWire masterSoftWire;
Gros2019 gros2019(I2C_ADDRESS_ACTUATORBIG2019, masterSoftWire);

Servo servoLifterArriere;
Servo servoLifterAvant;
Servo servoLeft;
Servo servoRight;

void setupPins() {
	//Servo pin must be output
	pinMode(PIN_SERVO_LIFTER_AR, OUTPUT);
	pinMode(PIN_SERVO_PELLE_LEFT, OUTPUT);
	pinMode(PIN_SERVO_PELLE_RIGHT, OUTPUT);
	pinMode(PIN_PUMP_CENTER_AR, OUTPUT);
	pinMode(PIN_PUMP_LEFT_AR, OUTPUT);
	pinMode(PIN_PUMP_RIGHT_AR, OUTPUT);

	pinMode(PIN_SERVO_LIFTER_AV, OUTPUT);
	pinMode(PIN_PUMP_CENTER_AV, OUTPUT);
	pinMode(PIN_PUMP_LEFT_AV, OUTPUT);
	pinMode(PIN_PUMP_RIGHT_AV, OUTPUT);

	//Attach servo and write safe value
	servoLifterArriere.attach(PIN_SERVO_LIFTER_AR);
	servoLifterArriere.write(SERVO_LIFTER_AR_UP);
	servoLifterAvant.attach(PIN_SERVO_LIFTER_AV);
	servoLifterAvant.write(SERVO_LIFTER_AV_UP);


	servoLeft.attach(PIN_SERVO_PELLE_LEFT);
	servoLeft.write(SERVO_LEFT_RENTRE);

	servoRight.attach(PIN_SERVO_PELLE_RIGHT);
	servoRight.write(SERVO_RIGHT_RENTRE);
}

void setup() {
	setupPins();

	Serial.begin(115200);
	Serial.setTimeout(3);

	gros2019.init();
}

void loop() {
	gros2019.loop();
}
