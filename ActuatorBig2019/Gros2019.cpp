#include "Gros2019.h"

#include <avr/pgmspace.h>
#include <Arduino.h>
#include <BasicStepperDriver.h>
#include <HardwareSerial.h>
#include <stdint.h>
#include <WString.h>

#include "SerialCommands.h"

#define log(x) Serial.println(x)

PROGMEM const char * const Actuator::versionString = "vSuction " __DATE__ " - " __TIME__;

Gros2019::Gros2019(uint8_t address, SoftWire& master) :
		Actuator(), i2cSlaveAddress(address), master(master) {
}

uint8_t Gros2019::slaveParametersCount = 0;

uint8_t Gros2019::slaveParameters[8];

bool Gros2019::i2cAcknowledged = false;

void Gros2019::initializeSlaveI2C() {
	slave.begin(i2cSlaveAddress);
	slave.onRequest(i2cHandleSlaveRequest);
	slave.onReceive(i2cHandleSlaveInput);
}

void Gros2019::initializeMasterI2C() {
	master.begin();
}

void Gros2019::initializeActuators() {
}

void Gros2019::runInitialization() {
	Actuator::runInitialization();
	initializeSlaveI2C();
	initializeMasterI2C();
	initializeActuators();
}

void Gros2019::runStart() {
	Actuator::runStart();
}

void Gros2019::runMain() {
	Actuator::runMain();
	handleAction();
}

void Gros2019::runDebug() {
	Actuator::runDebug();
	uint8_t count = slaveParametersCount;
	if (count > 0) {
		Serial.print(F("i2c buffer: "));
		for (uint8_t i = 0; i < count; i++) {
			Serial.print((char) slaveParameters[i]);
		}
		Serial.println();
		slaveParametersCount = 0;
	}
	handleAction();
}

void Gros2019::i2cHandleSlaveInput(int bytesCount) {
	unsigned int i = 0;
	while (slave.available() > 0 && i < min(ARRAY_SIZE(slaveParameters), (unsigned ) bytesCount)) {
		char c = slave.read();
		slaveParameters[i++] = c;
	}
	slaveParametersCount = i;
}

void Gros2019::i2cHandleSlaveRequest() {
	if (slaveParametersCount != 0) {
		i2cHandleCommand();
		slaveParametersCount = 0;
	}
}

bool Gros2019::isBusy() const {
	return command != Action::None;
}

void Gros2019::i2cSendStatus() {
	i2cAcknowledged = true;
	slave.write(command == Action::None ? I2C_STATUS_READY : I2C_STATUS_BUSY);
}

void Gros2019::i2cHandleCommand() {
	if (slaveParametersCount == 0) {
		gros2019.i2cSendStatus();
	} else if (slaveParameters[0] == I2C_COMMAND_STATUS) {
		gros2019.i2cSendStatus();
	} else {
		if (gros2019.isBusy()) {
			return;
		}
		i2cAcknowledged = false;
		switch (slaveParameters[0]) {
			case I2C_COMMAND_SET_COLOR_PURPLE:
				gros2019.color = Color::Purple;
				gros2019.command = Action::SetInitialPosition;
				break;
			case I2C_COMMAND_SET_COLOR_YELLOW:
				gros2019.color = Color::Yellow;
				gros2019.command = Action::SetInitialPosition;
				break;

			case I2C_COMMAND_PELLE_RENTREE:
				gros2019.command = Action::PelleRentre;
				break;
			case I2C_COMMAND_PELLE_PRISE:
				gros2019.command = Action::PellePrise;
				break;
			case I2C_COMMAND_PELLE_DEBLAYAGE:
				gros2019.command = Action::PelleMaintien;
				break;

			case I2C_COMMAND_TRIO_PUMP_AR_ON:
				gros2019.command = Action::TrioPumpArriereOn;
				break;
			case I2C_COMMAND_TRIO_PUMP_AR_OFF:
				gros2019.command = Action::TrioPumpArriereOff;
				break;
			case I2C_COMMAND_TRIO_PUMP_OFF_AR_LEFT:
				gros2019.command = Action::TrioPumpArriereOffLeft;
				break;
			case I2C_COMMAND_TRIO_PUMP_OFF_AR_CENTER:
				gros2019.command = Action::TrioPumpArriereOffCenter;
				break;
			case I2C_COMMAND_TRIO_PUMP_OFF_AR_RIGHT:
				gros2019.command = Action::TrioPumpArriereOffRight;
				break;
			case I2C_COMMAND_AR_DOWN:
				gros2019.command = Action::LifterArriereDown;
				break;
			case I2C_COMMAND_AR_UP:
				gros2019.command = Action::LifterArriereUp;
				break;
			case  I2C_COMMAND_AR_DEPOSE_BAS:
				gros2019.command = Action::LifterArriereDeposeDown;
				break;
			case  I2C_COMMAND_AR_DEPOSE_CENTER:
				gros2019.command = Action::LifterArriereDeposeCenter;
				break;
			case  I2C_COMMAND_AR_DEPOSE_HAUT:
				gros2019.command = Action::LifterArriereDeposeUp;
				break;


			case I2C_COMMAND_TRIO_PUMP_AV_ON:
				gros2019.command = Action::TrioPumpAvantOn;
				break;
			case I2C_COMMAND_TRIO_PUMP_AV_OFF:
				gros2019.command = Action::TrioPumpAvantOff;
				break;
			case I2C_COMMAND_TRIO_PUMP_OFF_AV_LEFT:
				gros2019.command = Action::TrioPumpAvantOffLeft;
				break;
			case I2C_COMMAND_TRIO_PUMP_OFF_AV_CENTER:
				gros2019.command = Action::TrioPumpAvantOffCenter;
				break;
			case I2C_COMMAND_TRIO_PUMP_OFF_AV_RIGHT:
				gros2019.command = Action::TrioPumpAvantOffRight;
				break;

			case I2C_COMMAND_AV_DOWN:
				gros2019.command = Action::LifterAvantDown;
				break;
			case I2C_COMMAND_AV_UP:
				gros2019.command = Action::LifterAvantUp;
				break;
			case  I2C_COMMAND_AV_DEPOSE_BAS:
				gros2019.command = Action::LifterAvantDeposeDown;
				break;
			case  I2C_COMMAND_AV_DEPOSE_CENTER:
				gros2019.command = Action::LifterAvantDeposeCenter;
				break;
			case  I2C_COMMAND_AV_DEPOSE_HAUT:
				gros2019.command = Action::LifterAvantDeposeUp;
				break;


			case I2C_COMMAND_KILL:
				gros2019.command = Action::Kill;
				break;
		}
	}
}

void Gros2019::enterDebug() {
	Actuator::enterDebug();
}

void Gros2019::i2cScanMasterBus() {
	Serial.println(F(":scanning I2C bus"));
	for (uint8_t address = 1; address < 127; address++) {
		// The i2c_scanner uses the return value of
		// the Write.endTransmisstion to see if
		// a device did acknowledge to the address.
		master.beginTransmission(address);
		uint8_t error = master.endTransmission();

		if (error == 0) {
			Serial.print('D');
			serialPrintHex(address);
		}
	}
	serialPrintSuccess();
}

void Gros2019::printI2CAddress() {
	Serial.print('a');
	serialPrintHex(i2cSlaveAddress);
}

void Gros2019::serialParseInput(const char *buffer, int length) {
	switch (buffer[0]) {
		case COMMAND_SERIAL_I2C_SCAN:
			i2cScanMasterBus();
			break;
		case COMMAND_SERIAL_I2C_ADDRESS:
			printI2CAddress();
			break;
		case  I2C_COMMAND_SET_COLOR_PURPLE:
			color = Color::Purple;
			command = Action::SetInitialPosition;
			break;
		case  I2C_COMMAND_SET_COLOR_YELLOW:
			color = Color::Yellow;
			command = Action::SetInitialPosition;
			break;

		case  I2C_COMMAND_PELLE_RENTREE:
			command = Action::PelleRentre;
			break;
		case  I2C_COMMAND_PELLE_PRISE:
			command = Action::PellePrise;
			break;
		case  I2C_COMMAND_PELLE_DEBLAYAGE:
			command = Action::PelleMaintien;
			break;


		case  I2C_COMMAND_TRIO_PUMP_AR_ON:
			command = Action::TrioPumpArriereOn;
			break;
		case  I2C_COMMAND_TRIO_PUMP_AR_OFF:
			command = Action::TrioPumpArriereOff;
			break;
		case I2C_COMMAND_TRIO_PUMP_OFF_AR_LEFT:
			gros2019.command = Action::TrioPumpArriereOffLeft;
			break;
		case I2C_COMMAND_TRIO_PUMP_OFF_AR_CENTER:
			gros2019.command = Action::TrioPumpArriereOffCenter;
			break;
		case I2C_COMMAND_TRIO_PUMP_OFF_AR_RIGHT:
			gros2019.command = Action::TrioPumpArriereOffRight;
			break;
		case  I2C_COMMAND_AR_DOWN:
			command = Action::LifterArriereDown;
			break;
		case  I2C_COMMAND_AR_UP:
			command = Action::LifterArriereUp;
			break;
		case  I2C_COMMAND_AR_DEPOSE_BAS:
			command = Action::LifterArriereDeposeDown;
			break;
		case  I2C_COMMAND_AR_DEPOSE_CENTER:
			command = Action::LifterArriereDeposeCenter;
			break;
		case  I2C_COMMAND_AR_DEPOSE_HAUT:
			command = Action::LifterArriereDeposeUp;
			break;


		case  I2C_COMMAND_TRIO_PUMP_AV_ON:
			command = Action::TrioPumpAvantOn;
			break;
		case  I2C_COMMAND_TRIO_PUMP_AV_OFF:
			command = Action::TrioPumpAvantOff;
			break;
		case I2C_COMMAND_TRIO_PUMP_OFF_AV_LEFT:
			gros2019.command = Action::TrioPumpAvantOffLeft;
			break;
		case I2C_COMMAND_TRIO_PUMP_OFF_AV_CENTER:
			gros2019.command = Action::TrioPumpAvantOffCenter;
			break;
		case I2C_COMMAND_TRIO_PUMP_OFF_AV_RIGHT:
			gros2019.command = Action::TrioPumpAvantOffRight;
			break;
		case  I2C_COMMAND_AV_DOWN:
			command = Action::LifterAvantDown;
			break;
		case  I2C_COMMAND_AV_UP:
			command = Action::LifterAvantUp;
			break;
		case  I2C_COMMAND_AV_DEPOSE_BAS:
			command = Action::LifterAvantDeposeDown;
			break;
		case  I2C_COMMAND_AV_DEPOSE_CENTER:
			command = Action::LifterAvantDeposeCenter;
			break;
		case  I2C_COMMAND_AV_DEPOSE_HAUT:
			command = Action::LifterAvantDeposeUp;
			break;



		default:
			Actuator::serialParseInput(buffer, length);
			break;
	}
}

void Gros2019::handleAction() {
	switch (command) {
		case Action::SetInitialPosition:
			break;
		case Action::None:
			break;
		case Action::Kill:
			kill();
			break;

		case Action::PellePrise:
			pellePrise();
			break;

		case Action::PelleMaintien:
			pelleMaintien();
			break;

		case Action::PelleRentre:
			pelleRentre();
			break;

		case Action::TrioPumpArriereOff:
			trioPumpArriereOff();
			break;
		case Action::TrioPumpArriereOffLeft:
			trioPumpArriereOffLeft();
			break;
		case Action::TrioPumpArriereOffCenter:
			trioPumpArriereOffCenter();
			break;
		case Action::TrioPumpArriereOffRight:
			trioPumpArriereOffRight();
			break;
		case Action::TrioPumpArriereOn:
			trioPumpArriereOn();
			break;
		case Action::LifterArriereDown:
			lifterArrierePosition(SERVO_LIFTER_AR_DOWN);
			break;
		case Action::LifterArriereUp:
			lifterArrierePosition(SERVO_LIFTER_AR_UP);
			break;
		case Action::LifterArriereDeposeCenter:
			lifterArrierePosition(SERVO_LIFTER_AR_DEPOSE_CENTER);
			break;
		case Action::LifterArriereDeposeDown:
			lifterArrierePosition(SERVO_LIFTER_AR_DEPOSE_DOWN);
			break;
		case Action::LifterArriereDeposeUp:
			lifterArrierePosition(SERVO_LIFTER_AR_DEPOSE_UP);
			break;

		case Action::TrioPumpAvantOff:
			trioPumpAvantOff();
			break;
		case Action::TrioPumpAvantOffLeft:
			trioPumpAvantOffLeft();
			break;
		case Action::TrioPumpAvantOffCenter:
			trioPumpAvantOffCenter();
			break;
		case Action::TrioPumpAvantOffRight:
			trioPumpAvantOffRight();
			break;
		case Action::TrioPumpAvantOn:
			trioPumpAvantOn();
			break;
		case Action::LifterAvantDown:
			lifterAvantPosition(SERVO_LIFTER_AV_DOWN);
			break;
		case Action::LifterAvantUp:
			lifterAvantPosition(SERVO_LIFTER_AV_UP);
			break;
		case Action::LifterAvantDeposeCenter:
			lifterAvantPosition(SERVO_LIFTER_AV_DEPOSE_CENTER);
			break;
		case Action::LifterAvantDeposeDown:
			lifterAvantPosition(SERVO_LIFTER_AV_DEPOSE_DOWN);
			break;
		case Action::LifterAvantDeposeUp:
			lifterAvantPosition(SERVO_LIFTER_AV_DEPOSE_UP);
			break;

	}

	command = Action::None;
	for (int i = 0; !i2cAcknowledged && i < 200; i++) {
		delay(1);
	}
}

void Gros2019::kill() {
	trioPumpArriereOff();
	trioPumpAvantOff();
	servoLifterArriere.detach();
	servoLifterAvant.detach();
	servoLeft.detach();
	servoRight.detach();
	slave.end();
}

void Gros2019::trioPumpArriereOn() {
	digitalWrite(PIN_PUMP_CENTER_AR, HIGH);
	digitalWrite(PIN_PUMP_LEFT_AR, HIGH);
	digitalWrite(PIN_PUMP_RIGHT_AR, HIGH);
}

void Gros2019::trioPumpArriereOff() {
	digitalWrite(PIN_PUMP_CENTER_AR, LOW);
	digitalWrite(PIN_PUMP_LEFT_AR, LOW);
	digitalWrite(PIN_PUMP_RIGHT_AR, LOW);
}
void Gros2019::trioPumpArriereOffCenter() {
	digitalWrite(PIN_PUMP_CENTER_AR, LOW);
}
void Gros2019::trioPumpArriereOffRight() {
	digitalWrite(PIN_PUMP_LEFT_AR, LOW);
}
void Gros2019::trioPumpArriereOffLeft() {
	digitalWrite(PIN_PUMP_RIGHT_AR, LOW);
}
void Gros2019::lifterArrierePosition(int position) {
	servoLifterArriere.write(position);
}

void Gros2019::trioPumpAvantOn() {
	digitalWrite(PIN_PUMP_CENTER_AV, HIGH);
	digitalWrite(PIN_PUMP_LEFT_AV, HIGH);
	digitalWrite(PIN_PUMP_RIGHT_AV, HIGH);
}

void Gros2019::trioPumpAvantOff() {
	digitalWrite(PIN_PUMP_CENTER_AV, LOW);
	digitalWrite(PIN_PUMP_LEFT_AV, LOW);
	digitalWrite(PIN_PUMP_RIGHT_AV, LOW);
}
void Gros2019::trioPumpAvantOffCenter() {
	digitalWrite(PIN_PUMP_CENTER_AV, LOW);
}
void Gros2019::trioPumpAvantOffRight() {
	digitalWrite(PIN_PUMP_RIGHT_AV, LOW);
}
void Gros2019::trioPumpAvantOffLeft() {
	digitalWrite(PIN_PUMP_LEFT_AV, LOW);
}
void Gros2019::lifterAvantPosition(int position) {
	servoLifterAvant.write(position);
}

void Gros2019::pellePrise() {
	servoLeft.write(SERVO_LEFT_PRISE);
	servoRight.write(SERVO_RIGHT_PRISE);
}

void Gros2019::pelleRentre() {
	servoLeft.write(SERVO_LEFT_RENTRE);
	servoRight.write(SERVO_RIGHT_RENTRE);
}

void Gros2019::pelleMaintien() {
	servoLeft.write(SERVO_LEFT_MAINTIEN);
	servoRight.write(SERVO_RIGHT_MAINTIEN);
}









