/*
 * Water2018.h
 *
 *  Created on: 21 avr. 2018
 *      Author: Emmanuel
 */

#ifndef GROS2019_H_
#define GROS2019_H_

#include "A4988.h"
#include "Constants.h"
#include "SoftWire.h"
#include "Servo.h"

#include "Actuator.h"
#include "Wire.h"

#define slave Wire

enum class Color {
	Undefined, Yellow, Purple
};

enum class Action {
	None,
	SetInitialPosition,
	Kill,
	PelleRentre,
	PellePrise,
	PelleMaintien,
	TrioPumpArriereOn,
	TrioPumpArriereOff,
	TrioPumpArriereOffCenter,
	TrioPumpArriereOffLeft,
	TrioPumpArriereOffRight,
	LifterArriereDown,
	LifterArriereUp,
	LifterArriereDeposeUp,
	LifterArriereDeposeCenter,
	LifterArriereDeposeDown,
	TrioPumpAvantOn,
	TrioPumpAvantOff,
	TrioPumpAvantOffCenter,
	TrioPumpAvantOffLeft,
	TrioPumpAvantOffRight,
	LifterAvantDown,
	LifterAvantUp,
	LifterAvantDeposeCenter,
	LifterAvantDeposeUp,
	LifterAvantDeposeDown
};

class Gros2019: public Actuator {
private:
	static uint8_t slaveParametersCount;
	static uint8_t slaveParameters[8];
	static bool i2cAcknowledged;

	static void i2cHandleSlaveInput(int bytesCount);
	static void i2cHandleSlaveRequest();
	static void i2cHandleCommand();

	uint8_t i2cSlaveAddress;
	SoftWire& master;
	Color color = Color::Undefined;
	Action command = Action::None;

	void handleAction();

	void i2cScanMasterBus();
	void i2cSendStatus();
	void initializeActuators();
	void initializeMasterI2C();
	void initializeSlaveI2C();
	bool isBusy() const;

	void printI2CAddress();

	// Servo
	void moveHigh();
	void moveMedium();
	void moveLow();

	// Pompe
	void trioPumpArriereOn();
	void trioPumpArriereOff();
	void trioPumpArriereOffCenter();
	void trioPumpArriereOffRight();
	void trioPumpArriereOffLeft();
	void lifterArrierePosition(int position);

	void trioPumpAvantOn();
	void trioPumpAvantOff();
	void trioPumpAvantOffCenter();
	void trioPumpAvantOffRight();
	void trioPumpAvantOffLeft();
	void lifterAvantPosition(int position);

	// Bras


	void pellePrise();
	void pelleMaintien();
	void pelleRentre();

	// arr�t d�finitif
	void kill();

protected:
	virtual void enterDebug();
	virtual void runInitialization();
	virtual void runStart();
	virtual void runMain();
	virtual void runDebug();
	virtual void serialParseInput(const char *buffer, int length);

public:
	Gros2019(uint8_t address, SoftWire& master);
};

extern Gros2019 gros2019;
extern Servo servoLifterArriere;
extern Servo servoLifterAvant;
extern Servo servoRight;
extern Servo servoLeft;

#endif /* GROS2019_H_ */
