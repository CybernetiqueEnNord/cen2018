#ifndef BEACON2018_H
#define BEACON2018_H

// Configuration

// A0
#define SDA_PORT PORTC
#define SDA_PIN 0
// A1
#define SCL_PORT PORTC
#define SCL_PIN 1

#define I2C_TIMEOUT 5
#define I2C_FASTMODE 1

//

#include <EEPROMex.h>
#include <Wire.h>
extern "C" {
#include <utility/twi.h>
}
#include "SoftWire.h"
#include "SensorVL53L0X.h"

#include "../Commons/configuration/Beacon2018Configuration.h"
#include "../Commons/i2c/BeaconCommands.h"

#define PIN_BUZZER 13
#define PIN_START_LED 11
#define PIN_START_IN 10
#define PIN_SENSOR_LOW 2
#define PIN_SENSOR_HIGH 11
#define PIN_SENSOR_OFFSET_BASE 10
#define PIN_SENSOR_OFFSET 6

#define PIN_ANALOG_1 6 // fil Bleu
#define PIN_ANALOG_2 7 // fil Jaune

#define EEPROM_I2C_ADDRESS 0
#define EEPROM_DISTANCE_THRESHOLD (EEPROM_I2C_ADDRESS + sizeof(uint8_t))
#define EEPROM_SENSORS_COUNT (EEPROM_DISTANCE_THRESHOLD + sizeof(unsigned int))
#define EEPROM_SENSORS_CONFIG (EEPROM_SENSORS_COUNT + sizeof(uint8_t))

#define COMMAND_SERIAL_I2C_ADDRESS 'a'
#define COMMAND_SERIAL_I2C_SCAN 's'
#define COMMAND_SERIAL_EXIT 'x'
#define COMMAND_SERIAL_SENSOR_ADD 'i'
#define COMMAND_SERIAL_SENSOR_CHANGE_ADDRESS 'c'
#define COMMAND_SERIAL_SENSOR_LIST 'l'
#define COMMAND_SERIAL_SENSOR_REMOVE 'r'
#define COMMAND_SERIAL_SENSOR_PING 'p'
#define COMMAND_SERIAL_DISTANCE_THRESHOLD 't'
#define COMMAND_SERIAL_BUZZER_SWITCH 'b'
#define COMMAND_SERIAL_SENSOR_PRINT 'e'

#define ARRAY_SIZE(x) (sizeof x / sizeof(x[0]))

#define DELAY_SHUTDOWN 2

#define TIMEOUT_MEASURE 500

#define DEFAULT_DISTANCE_THRESHOLD 1500
#define DEFAULT_SLAVE_ADDRESS 0x25

// Décompte de l'état de la tirette avant de passer à l'état démarré
#define DEFAULT_START_COUNT 10

enum class State {
	Init, Start, Main, Debug
};

enum class Obstacle {
	None = 0, Front = 1, Back = 2
};

inline Obstacle operator&(Obstacle a, Obstacle b) {
	return static_cast<Obstacle>(static_cast<int>(a) & static_cast<int>(b));
}

inline Obstacle& operator|=(Obstacle &a, Obstacle b) {
	return a = static_cast<Obstacle>(static_cast<int>(a) | static_cast<int>(b));
}

PROGMEM const char * const versionString = "vBeacon " __DATE__ " - " __TIME__;

struct BeaconConfig {
	uint8_t slaveAddress = 0xFF;
	unsigned int distanceThreshold = DEFAULT_DISTANCE_THRESHOLD;
	SensorVL53L0X sensors[BEACON2018_SENSORS_COUNT];
};

struct HMIData {
	uint8_t value1;
	uint8_t value2;
};

void printVersion();
void printSlaveAddress();
void printDistanceThreshold();
void printConfig();
void printError();
void printSuccess();
void printStarted();
void printObstacle();
void printSensor(uint8_t index);
void printSensorMeasure(uint8_t index);
void printSensorsList();
void printSensorsMeasures();
inline uint8_t getSensorDigitalPin(uint8_t sensorIndex);
void setSlaveAddress(uint8_t address);
void addSensor(uint8_t address, long angle);
void removeSensor(uint8_t index);
void scanMasterBus();
bool setSensorAddress(uint8_t index, uint8_t newAddress);
bool isSensorPresent(int8_t address);
void setDistanceThreshold(unsigned int distance);
void parseInput(const char *buffer, int length);
void handleInput();
void runDebug();
void initializeSensor(uint8_t index, SensorVL53L0X &sensor);
void setSensorShutdown(uint8_t index, bool value);
void shutdownSensors();
void initializeSensors();
void initializeMasterI2C();
void initializeSlaveI2C();
void i2cSendStatus();
void updateHMIState();
void i2cSendHMIState();
void i2cSendSensorConfiguration();
void i2cSendSensorMeasures();
void i2cSendVersion();
void i2cSetThreshold();
void i2cSetLed();
void i2cSetBuzzer();
void i2cHandleSlaveInput(int bytesCount);
void i2cHandleSlaveRequest();
void i2cHandleCommand();
void runStart();
void runInitialization();
void enterDebug();
void exitDebug();
void readSensor(SensorVL53L0X &sensor);
void readSensors();
void setObstacle(Obstacle value);
void setBuzzerValue(bool value);
void beep(int onDuration, int offDuration);
void toggleBuzzerEnabled();
void setBuzzerEnabled(bool enabled);
inline bool isStarted();
void checkStarted();
void checkObstacle();
void updateLed();
void blinkLed(unsigned int onDuration, unsigned int offDuration, unsigned int count);
void runMain();
void loadConfig();
void saveConfig();
void setPinModeAndValue(uint8_t index, uint8_t mode, uint8_t value);
void setupPins();
void setup();
void loop();
void dring();

#endif
