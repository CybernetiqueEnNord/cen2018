#include "Beacon2018.h"

Obstacle obstacle = Obstacle::None;
uint8_t startCount = DEFAULT_START_COUNT;
bool buzzerEnabled = false;
bool ledState = false;
bool printSensors = false;
unsigned int ledOffDuration = 0;
unsigned int ledOnDuration = 0;
unsigned int ledCount = 0;
unsigned long ledStateTimestamp = 0;

uint8_t slaveParametersCount = 0;
uint8_t slaveParameters[8];

BeaconConfig beaconConfig;
HMIData hmi;
State state;
SoftWire master;
#define slave Wire

void printVersion() {
	Serial.println(versionString);
}

void printHex(uint8_t value, bool addNewLine = true) {
	if (value < 16) {
		Serial.print('0');
	}
	Serial.print(value, HEX);
	if (addNewLine) {
		Serial.println();
	}
}

void printSlaveAddress() {
	Serial.print('A');
	printHex(beaconConfig.slaveAddress);
}

void printDistanceThreshold() {
	Serial.print('T');
	Serial.println(beaconConfig.distanceThreshold);
}

void printConfig() {
	printSlaveAddress();
	printDistanceThreshold();
}

void printError() {
	Serial.println(F("ERROR"));
}

void printSuccess() {
	//Serial.println(F("OK"));
}

void printStarted() {
	bool started = isStarted();
	if (started) {
		Serial.println(F("STARTED"));
	}
	blinkLed(1000, 0, 0);
}

void printObstacle() {
	if (obstacle != Obstacle::None) {
		Serial.print(F("OBSTACLE"));
	} else {
		Serial.print(F("NOTHING"));
	}
	if ((obstacle & Obstacle::Front) != Obstacle::None) {
		Serial.print(F(" FRONT"));
	}
	if ((obstacle & Obstacle::Back) != Obstacle::None) {
		Serial.print(F(" BACK"));
	}
	Serial.println();
}

void printSensor(uint8_t index) {
	const SensorVL53L0X &sensor = beaconConfig.sensors[index];
	const SensorVL53L0XConfig &config = sensor.getConfig();
	Serial.print('S');
	Serial.print(index);
	Serial.print(';');
	printHex(config.address, false);
	Serial.print(';');
	Serial.println(config.getAngle());
}

void printSensorMeasure(uint8_t index) {
	const SensorVL53L0X &sensor = beaconConfig.sensors[index];
	const SensorMeasure &measure = sensor.getMeasure();
	Serial.print('M');
	Serial.print(index);
	Serial.print(';');
	Serial.print(measure.distance);
	Serial.print(';');
	Serial.println(sensor.isEnabled() ? '1' : '0');
}

void printSensorsList() {
	for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
		if (beaconConfig.sensors[i].isConfigured()) {
			printSensor(i);
		} else {
			break;
		}
	}
	printSuccess();
}

void printSensorsMeasures() {
	if (!printSensors) {
		return;
	}
	for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
		if (beaconConfig.sensors[i].isConfigured()) {
			printSensorMeasure(i);
		} else {
			break;
		}
	}
	printSuccess();
}

inline uint8_t getSensorDigitalPin(uint8_t sensorIndex) {
	uint8_t pin = sensorIndex + PIN_SENSOR_LOW;
	if (pin >= PIN_SENSOR_OFFSET_BASE) {
		pin += PIN_SENSOR_OFFSET;
	}
	return pin;
}

void setSlaveAddress(uint8_t address) {
	beaconConfig.slaveAddress = address;
	twi_setAddress(address);
	printSlaveAddress();
}

void addSensor(uint8_t address, long angle) {
	for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
		if (!beaconConfig.sensors[i].isConfigured()) {
			SensorVL53L0XConfig config(address, angle);
			beaconConfig.sensors[i].setConfig(config);
			printSuccess();
			return;
		}
	}
	// Tous les capteurs ont déjà été définis
	printError();
}

void removeSensor(uint8_t index) {
	const int n = ARRAY_SIZE(beaconConfig.sensors);
	if (index >= n) {
		printError();
	}
	while (index < n - 1) {
		beaconConfig.sensors[index] = beaconConfig.sensors[index + 1];
		index++;
	}
	beaconConfig.sensors[n - 1].clearConfiguration();
	printSuccess();
}

void scanMasterBus() {
	Serial.println(F(":scanning I2C bus"));
	for (uint8_t address = 1; address < 127; address++) {
		// The i2c_scanner uses the return value of
		// the Write.endTransmisstion to see if
		// a device did acknowledge to the address.
		master.beginTransmission(address);
		uint8_t error = master.endTransmission();

		if (error == 0) {
			Serial.print('D');
			printHex(address);
		}
	}
	printSuccess();
}

bool setSensorAddress(uint8_t index, uint8_t newAddress) {
	Serial.print(F(":setting sensor address at index "));
	Serial.print(index);
	Serial.print(F(" to "));
	printHex(newAddress);

	// reset
	uint8_t pin = getSensorDigitalPin(index);
	setSensorShutdown(pin, true);
	delay(DELAY_SHUTDOWN);
	setSensorShutdown(pin, false);
	delay(DELAY_SHUTDOWN);

	VL53L0X<SoftWire> sensor(master);
	sensor.setAddress(newAddress);
	return sensor.last_status == 0;
}

bool isSensorPresent(int8_t address) {
	master.beginTransmission(address);
	uint8_t status = master.endTransmission();
	return status == 0;
}

void setDistanceThreshold(unsigned int distance) {
	if (distance == 0) {
		printError();
		return;
	}
	beaconConfig.distanceThreshold = distance;
	printDistanceThreshold();
}

void parseInput(const char *buffer, int length) {
	(void) length;

	switch (buffer[0]) {
		case COMMAND_SERIAL_I2C_ADDRESS: {
			uint8_t address = strtol(&buffer[1], NULL, 16);
			if (address == 0) {
				goto error;
			}
			setSlaveAddress(address);
			break;
		}

		case COMMAND_SERIAL_I2C_SCAN:
			scanMasterBus();
			break;

		case COMMAND_SERIAL_SENSOR_ADD: {
			char *buf;
			uint8_t address = strtol(&buffer[1], &buf, 16);
			if (address == 0 || *buf != ';') {
				goto error;
			}
			long angle = strtol(buf + 1, &buf, 10);
			addSensor(address, angle);
			break;
		}

		case COMMAND_SERIAL_SENSOR_REMOVE: {
			uint8_t index = atoi(&buffer[1]);
			removeSensor(index);
			break;
		}

		case COMMAND_SERIAL_SENSOR_LIST:
			printSensorsList();
			break;

		case COMMAND_SERIAL_SENSOR_PRINT:
			printSensors = !printSensors;
			break;

		case COMMAND_SERIAL_SENSOR_CHANGE_ADDRESS: {
			char *buf;
			uint8_t index = strtol(&buffer[1], &buf, 10);
			if (index >= ARRAY_SIZE(beaconConfig.sensors) || *buf != ';') {
				goto error;
			}
			uint8_t newAddress = strtol(buf + 1, &buf, 16);
			if (newAddress == 0) {
				goto error;
			}
			if (!setSensorAddress(index, newAddress)) {
				goto error;
			}
			break;
		}

		case COMMAND_SERIAL_SENSOR_PING: {
			uint8_t address = strtol(&buffer[1], NULL, 16);
			if (address == 0 || !isSensorPresent(address)) {
				goto error;
			}
			printSuccess();
			break;
		}

		case COMMAND_SERIAL_DISTANCE_THRESHOLD: {
			unsigned int distance = atoi(&buffer[1]);
			setDistanceThreshold(distance);
			break;
		}

		case COMMAND_SERIAL_EXIT:
			exitDebug();
			break;

		case COMMAND_SERIAL_BUZZER_SWITCH:
			toggleBuzzerEnabled();
			break;

		default:
			goto error;
			break;
	}
	return;

	error: printError();
}

void handleInput() {
	char buffer[10];
	int n = Serial.readBytesUntil('\n', buffer, ARRAY_SIZE(buffer) - 1);
	if (n > 0) {
		buffer[n] = 0;
		parseInput(buffer, n);
	}
}

void runDebug() {
	if (Serial.available()) {
		handleInput();
	}

	static unsigned long last = 0;
	unsigned long now = millis();
	if (now - last > 250) {
		last = now;
		readSensors();
		checkObstacle();
		printSensorsMeasures();
	}
}

void initializeSensor(uint8_t index, SensorVL53L0X &sensor) {
	if (!sensor.isConfigured()) {
		return;
	}

	const SensorVL53L0XConfig &config = sensor.getConfig();
	if (!setSensorAddress(index, config.address)) {
		sensor.setEnabled(false);
		return;
	}

	VL53L0X<SoftWire> device(master, config.address);
	if (!device.init() || device.last_status != 0) {
		goto error;
	}
	device.setTimeout(500);
	if (!device.setVcselPulsePeriod(vcselPeriodType::VcselPeriodPreRange, 18) || device.last_status != 0) {
		goto error;
	}
	if (!device.setVcselPulsePeriod(vcselPeriodType::VcselPeriodFinalRange, 14) || device.last_status != 0) {
		goto error;
	}
	if (!device.setMeasurementTimingBudget(66000) || device.last_status != 0) {
		goto error;
	}

	// Start continuous back-to-back mode (take readings as
	// fast as possible).  To use continuous timed mode
	// instead, provide a desired inter-measurement period in
	// ms (e.g. sensor.startContinuous(100)).
	device.startContinuous();
	if (device.last_status != 0) {
		goto error;
	}
	return;

	error: printError();
	sensor.setEnabled(false);
}

void setSensorShutdown(uint8_t pin, bool value) {
	if (value) {
		// extinction : mise à 0V
		setPinModeAndValue(pin, OUTPUT, LOW);
	} else {
		// allumage : utilisation des pull-ups
		setPinModeAndValue(pin, INPUT, LOW);
	}
}

void shutdownSensors() {
	for (uint8_t i = 0; i < BEACON2018_SENSORS_COUNT; i++) {
		uint8_t pin = getSensorDigitalPin(i);
		setPinModeAndValue(pin, OUTPUT, LOW);
	}
}

void initializeSensors() {
	obstacle = Obstacle::None;
	shutdownSensors();
	for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
		SensorVL53L0X &sensor = beaconConfig.sensors[i];
		initializeSensor(i, sensor);
	}
}

void initializeMasterI2C() {
	master.begin();
}

void initializeSlaveI2C() {
	slave.begin(beaconConfig.slaveAddress);
	slave.onRequest(i2cHandleSlaveRequest);
	slave.onReceive(i2cHandleSlaveInput);
}

void i2cSendStatus() {
	uint8_t status = static_cast<int>(obstacle) << 0;
	status |= (isStarted() ? 1 : 0) << 2;
	slave.write(status);
	if (state == State::Debug) {
		Serial.print(F(":i2c status: "));
		Serial.println(status);
	}
}

void updateHMIState() {
	hmi.value1 = analogRead(PIN_ANALOG_1) >> 2;
	hmi.value2 = analogRead(PIN_ANALOG_2) >> 2;
}

void i2cSendHMIState() {
	updateHMIState();
	slave.write(hmi.value1);
	slave.write(hmi.value2);
}

void i2cSendSensorConfiguration() {
	for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
		const SensorVL53L0X &sensor = beaconConfig.sensors[i];
		if (!sensor.isConfigured()) {
			continue;
		}
		const SensorVL53L0XConfig &config = sensor.getConfig();
		slave.write(i);
		slave.write(config.angle);
	}
	slave.write(0xFF);
}

void i2cSendSensorMeasures() {
	unsigned long now = millis();
	for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
		const SensorVL53L0X &sensor = beaconConfig.sensors[i];
		if (!sensor.isEnabled()) {
			continue;
		}
		const SensorMeasure &measure = sensor.getMeasure();
		if (now - measure.timestamp < TIMEOUT_MEASURE) {
			slave.write(i);
			slave.write(measure.distance / 32);
		}
	}
	slave.write(0xFF);
}

void i2cSendVersion() {
	slave.print(versionString);
}

void i2cSetThreshold() {
	if (slaveParametersCount == 2) {
		beaconConfig.distanceThreshold = slaveParameters[1] * 8;
	}
}

void i2cSetLed() {
	if (slaveParametersCount == 4) {
		ledOnDuration = slaveParameters[1] * 16;
		ledOffDuration = slaveParameters[2] * 16;
		ledCount = slaveParameters[3];
	}
}

void i2cSetBuzzer() {
	if (slaveParametersCount == 2) {
		setBuzzerEnabled(slaveParameters[1] != 0);
	}
}
void i2cHandleSlaveInput(int bytesCount) {
	(void) bytesCount;

	unsigned int i = 0;
	while (slave.available() > 0 && i < ARRAY_SIZE(slaveParameters)) {
		char c = slave.read();
		slaveParameters[i++] = c;
	}
	slaveParametersCount = i;
}

void i2cHandleSlaveRequest() {
	if (slaveParametersCount != 0) {
		i2cHandleCommand();
		slaveParametersCount = 0;
	}
}

void i2cHandleCommand() {
	char command = slaveParameters[0];
	if (state == State::Debug) {
		Serial.print(F(":i2c command: "));
		Serial.println(command);
	}
	switch (command) {
		case COMMAND_I2C_CONFIG:
			i2cSendSensorConfiguration();
			break;

		case COMMAND_I2C_MEASURES:
			i2cSendSensorMeasures();
			break;

		case COMMAND_I2C_HMI:
			i2cSendHMIState();
			break;

		case COMMAND_I2C_VERSION:
			i2cSendVersion();
			break;

		case COMMAND_I2C_THRESHOLD:
			i2cSetThreshold();
			break;

		case I2C_COMMAND_STATUS:
			i2cSendStatus();
			break;

		case COMMAND_I2C_LED:
			i2cSetLed();
			break;

		case COMMAND_I2C_BUZZER:
			i2cSetBuzzer();
			break;

		default:
			break;
	}
}

void runStart() {
	state = State::Main;
	printConfig();
	initializeSensors();
	initializeSlaveI2C();
}

void runInitialization() {
	state = State::Start;
	loadConfig();
	initializeMasterI2C();
}

void enterDebug() {
	state = State::Debug;
	Serial.println(F(":entering debug mode"));
	setObstacle(Obstacle::None);
	printSensorsList();
	setBuzzerEnabled(true);
	dring();
	setBuzzerEnabled(false);
}

void exitDebug() {
	saveConfig();
	state = State::Start;
	Serial.println(F(":exiting debug mode"));
}

void readSensor(SensorVL53L0X &sensor) {
	if (!sensor.isEnabled()) {
		return;
	}

	const SensorVL53L0XConfig &config = sensor.getConfig();
	VL53L0X<SoftWire> device(master, config.address);
	device.setTimeout(1);
	unsigned int range = device.readRangeContinuousMillimeters();
	if (!device.timeoutOccurred()) {
		sensor.setDistance(range);
	}
}

void readSensors() {
	for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
		auto &sensor = beaconConfig.sensors[i];
		readSensor(sensor);
	}

	auto now = millis();
	bool timeout = false;
	for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
		auto &sensor = beaconConfig.sensors[i];
		auto &measure = sensor.getMeasure();
		timeout = now - measure.timestamp > 1500;
		if (timeout) {
			Serial.print(now);
			Serial.print("  ");
			Serial.print(measure.timestamp);
			Serial.print("  ");
			Serial.println(sensor.getConfig().address, 16);
			dring();
			initializeMasterI2C();
		}
	}
}

void updateBuzzer() {
	bool b = (obstacle != Obstacle::None) && (state != State::Debug);
	setBuzzerValue(b);
}

void setObstacle(Obstacle value) {
	bool changed = value != obstacle;
	if (!changed) {
		return;
	}
	obstacle = value;
	printObstacle();
	updateBuzzer();
}

void setBuzzerValue(bool value) {
	digitalWrite(PIN_BUZZER, value && buzzerEnabled);
}

void beep(int onDuration, int offDuration) {
	setBuzzerValue(true);
	delay(onDuration);
	setBuzzerValue(false);
	delay(offDuration);
}

void toggleBuzzerEnabled() {
	buzzerEnabled = !buzzerEnabled;
	updateBuzzer();
}

void setBuzzerEnabled(bool enabled) {
	buzzerEnabled = enabled;
	updateBuzzer();
}

inline bool isStarted() {
	return startCount == 0;
}

void checkStarted() {
	/*
	 // Une fois démarré, l'état reste toujours à démarré
	 if (isStarted()) {
	 return;
	 }
	 */
	bool value = digitalRead(PIN_START_IN) == HIGH;
	if (!value) {
		startCount = DEFAULT_START_COUNT;
	}
	if (startCount > 0 && --startCount == 0) {
		printStarted();
	}
}

void checkObstacle() {
	Obstacle value = Obstacle::None;
	unsigned long now = millis();
	for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
		const SensorVL53L0X &sensor = beaconConfig.sensors[i];
		if (!sensor.isEnabled()) {
			continue;
		}
		const SensorMeasure &measure = sensor.getMeasure();
		if (now - measure.timestamp < TIMEOUT_MEASURE) {
			bool b = measure.distance > 0 && measure.distance < beaconConfig.distanceThreshold;
			if (!b) {
				continue;
			}
			const SensorVL53L0XConfig &config = sensor.getConfig();
			long angle = config.getAngle();
			if (angle < 270 && angle > 90) {
				value |= Obstacle::Back;
			} else {
				value |= Obstacle::Front;
			}
		}
	}
	setObstacle(value);
}

void updateLed() {
	unsigned long now = millis();
	if (now >= ledStateTimestamp) {
		ledState = !ledState;
		unsigned int interval = ledState ? ledOnDuration : ledOffDuration;
		if (interval > 0) {
			digitalWrite(PIN_START_LED, ledState ? HIGH : LOW);
		}
		ledStateTimestamp = now + interval;
		if (ledState && ledCount > 0) {
			if (--ledCount == 0) {
				blinkLed(0, 1, 0);
			}
		}
	}
}

void blinkLed(unsigned int onDuration, unsigned int offDuration, unsigned int count) {
	ledOnDuration = onDuration;
	ledOffDuration = offDuration;
	ledCount = count;
}

void runMain() {
	if (Serial.available()) {
		if (Serial.find("d\n")) {
			enterDebug();
			return;
		}
	}
	readSensors();
	checkObstacle();
	checkStarted();
}

void loadConfig() {
	// adresse
	uint8_t slaveAddress = EEPROM.read(EEPROM_I2C_ADDRESS);
	if (slaveAddress == 0xFF) {
		slaveAddress = DEFAULT_SLAVE_ADDRESS;
	}
	beaconConfig.slaveAddress = slaveAddress;

	// seuil de détection
	unsigned int distanceThreshold = EEPROM.readInt(EEPROM_DISTANCE_THRESHOLD);
	if (distanceThreshold == 0xFFFF) {
		distanceThreshold = DEFAULT_DISTANCE_THRESHOLD;
	}
	beaconConfig.distanceThreshold = distanceThreshold;

	// configuration des capteurs
	uint8_t count = EEPROM.read(EEPROM_SENSORS_COUNT);
	if (count == 0xFF) {
		// Pas de données
		return;
	}
	uint8_t max = ARRAY_SIZE(beaconConfig.sensors);
	if (count > max) {
		count = max;
	}
	int address = EEPROM_SENSORS_CONFIG;
	for (int i = 0; i < count; i++) {
		SensorVL53L0XConfig config;
		address += EEPROM.readBlock(address, config);
		beaconConfig.sensors[i].setConfig(config);
	}
}

void saveConfig() {
	EEPROM.write(EEPROM_I2C_ADDRESS, beaconConfig.slaveAddress);
	EEPROM.writeInt(EEPROM_DISTANCE_THRESHOLD, beaconConfig.distanceThreshold);
	uint8_t count = 0;
	int address = EEPROM_SENSORS_CONFIG;
	for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
		if (!beaconConfig.sensors[i].isConfigured()) {
			continue;
		}
		const SensorVL53L0XConfig &config = beaconConfig.sensors[i].getConfig();
		address += EEPROM.writeBlock(address, config);
		count++;
	}
	EEPROM.write(EEPROM_SENSORS_COUNT, count);
}

void setPinModeAndValue(uint8_t index, uint8_t mode, uint8_t value) {
	digitalWrite(index, value);
	pinMode(index, mode);
}

void setupPins() {
	setPinModeAndValue(PIN_START_IN, INPUT_PULLUP, HIGH);
	setPinModeAndValue(PIN_START_LED, OUTPUT, LOW);
	setPinModeAndValue(PIN_BUZZER, OUTPUT, LOW);

	shutdownSensors();

	// active le capteur d'indice 0
	digitalWrite(PIN_SENSOR_LOW, HIGH);
}

void dring() {
	setBuzzerEnabled(true);
	for (int i = 0; i < 15; i++) {
		beep(30, 20);
	}
	setBuzzerEnabled(false);
}

void setup() {
	setupPins();
	blinkLed(500, 500, 3);

	Serial.begin(115200);
	Serial.setTimeout(3);
	printVersion();

	dring();
}

void loop() {
	updateLed();
	switch (state) {
		case State::Init:
			runInitialization();
			break;

		case State::Start:
			runStart();
			break;

		case State::Main:
			runMain();
			break;

		case State::Debug:
			runDebug();
			break;
	}
}
