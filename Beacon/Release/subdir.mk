################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
..\Beacon2018.cpp \
..\SensorVL53L0X.cpp \
..\VL53L0X.cpp 

LINK_OBJ += \
.\Beacon2018.cpp.o \
.\SensorVL53L0X.cpp.o \
.\VL53L0X.cpp.o 

CPP_DEPS += \
.\Beacon2018.cpp.d \
.\SensorVL53L0X.cpp.d \
.\VL53L0X.cpp.d 


# Each subdirectory must supply rules for building sources it contributes
Beacon2018.cpp.o: ..\Beacon2018.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:\eclipse\eclipse.neon.cdt\/arduinoPlugin/packages/arduino/tools/avr-gcc/4.9.2-atmel3.5.4-arduino2/bin/avr-g++" -c -g -Os -Wall -Wextra -std=gnu++11 -fpermissive -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -flto -mmcu=atmega328p -DF_CPU=16000000L -DARDUINO=10802 -DARDUINO_AVR_NANO -DARDUINO_ARCH_AVR   -I"C:\eclipse\202003-cpp\arduinoPlugin\packages\arduino\hardware\avr\1.6.21\cores\arduino" -I"C:\eclipse\202003-cpp\arduinoPlugin\packages\arduino\hardware\avr\1.6.21\variants\eightanaloginputs" -I"C:\eclipse\202003-cpp\arduinoPlugin\packages\arduino\hardware\avr\1.6.21\libraries\Wire" -I"C:\Users\emmanuel\workspace-cpp\ArduinoLibrary\External\EEPROMEx" -I"C:\Users\emmanuel\workspace-cpp\ArduinoLibrary\ExternalCustomised\SoftI2CMaster" -I"C:\Users\emmanuel\workspace-cpp\ArduinoLibrary\ExternalCustomised\SoftWire" -I"C:\Users\emmanuel\workspace-cpp\ArduinoLibrary\ExternalCustomised\SoftWire\src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

SensorVL53L0X.cpp.o: ..\SensorVL53L0X.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:\eclipse\eclipse.neon.cdt\/arduinoPlugin/packages/arduino/tools/avr-gcc/4.9.2-atmel3.5.4-arduino2/bin/avr-g++" -c -g -Os -Wall -Wextra -std=gnu++11 -fpermissive -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -flto -mmcu=atmega328p -DF_CPU=16000000L -DARDUINO=10802 -DARDUINO_AVR_NANO -DARDUINO_ARCH_AVR   -I"C:\eclipse\202003-cpp\arduinoPlugin\packages\arduino\hardware\avr\1.6.21\cores\arduino" -I"C:\eclipse\202003-cpp\arduinoPlugin\packages\arduino\hardware\avr\1.6.21\variants\eightanaloginputs" -I"C:\eclipse\202003-cpp\arduinoPlugin\packages\arduino\hardware\avr\1.6.21\libraries\Wire" -I"C:\Users\emmanuel\workspace-cpp\ArduinoLibrary\External\EEPROMEx" -I"C:\Users\emmanuel\workspace-cpp\ArduinoLibrary\ExternalCustomised\SoftI2CMaster" -I"C:\Users\emmanuel\workspace-cpp\ArduinoLibrary\ExternalCustomised\SoftWire" -I"C:\Users\emmanuel\workspace-cpp\ArduinoLibrary\ExternalCustomised\SoftWire\src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

VL53L0X.cpp.o: ..\VL53L0X.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:\eclipse\eclipse.neon.cdt\/arduinoPlugin/packages/arduino/tools/avr-gcc/4.9.2-atmel3.5.4-arduino2/bin/avr-g++" -c -g -Os -Wall -Wextra -std=gnu++11 -fpermissive -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -flto -mmcu=atmega328p -DF_CPU=16000000L -DARDUINO=10802 -DARDUINO_AVR_NANO -DARDUINO_ARCH_AVR   -I"C:\eclipse\202003-cpp\arduinoPlugin\packages\arduino\hardware\avr\1.6.21\cores\arduino" -I"C:\eclipse\202003-cpp\arduinoPlugin\packages\arduino\hardware\avr\1.6.21\variants\eightanaloginputs" -I"C:\eclipse\202003-cpp\arduinoPlugin\packages\arduino\hardware\avr\1.6.21\libraries\Wire" -I"C:\Users\emmanuel\workspace-cpp\ArduinoLibrary\External\EEPROMEx" -I"C:\Users\emmanuel\workspace-cpp\ArduinoLibrary\ExternalCustomised\SoftI2CMaster" -I"C:\Users\emmanuel\workspace-cpp\ArduinoLibrary\ExternalCustomised\SoftWire" -I"C:\Users\emmanuel\workspace-cpp\ArduinoLibrary\ExternalCustomised\SoftWire\src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  -o "$@"
	@echo 'Finished building: $<'
	@echo ' '


