/*
 * WaitingEffect.cpp
 *
 *  Created on: 18 janv. 2020
 *      Author: emmanuel
 */

#include "WaitingEffect.h"

WaitingEffect::WaitingEffect() {
}

void WaitingEffect::apply(Leds &leds) {
	switch (++steps) {
		case 160:
			leds.setRange(0, 1, Color(8, 8, 8));
			steps = 0;
			break;
	}

}

String WaitingEffect::getId() const {
	return F("Waiting");
}
