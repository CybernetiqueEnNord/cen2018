#ifndef VECTOR_3D_H
#define VECTOR_3D_H

#include "Print.h"

class Vector3D : public Printable
{
private:
    float x = 0, y = 0, z = 0;

public:
    Vector3D(float x = 0, float y = 0, float z = 0);
    float angle(Vector3D v) const;
    Vector3D cross(Vector3D v) const;
    Vector3D dot(Vector3D v) const;
    bool isEmpty() const;
    float norm() const;
    void normalize();
    Vector3D normalized() const;
    virtual size_t printTo(Print &p) const;
};

#endif
