/*
 * SolidEffect.h
 *
 *  Created on: 18 janv. 2020
 *      Author: emmanuel
 */

#ifndef SOLIDEFFECT_H_
#define SOLIDEFFECT_H_

#include "Leds.h"

class SolidEffect: public Effect {
public:
	SolidEffect();
	virtual void apply(Leds &leds) override;
	virtual String getId() const override;

	Color color = Color(0, 0, 0);
};

#endif /* SOLIDEFFECT_H_ */
