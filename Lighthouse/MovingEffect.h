#ifndef MOVING_EFFECT_H
#define MOVING_EFFECT_H

#include "Color.h"
#include "Leds.h"

class MovingEffect : public Effect
{
private:
  int fOffset = 0;

public:
  Color color = Color(24, 24, 10);
  int lightsCount = 5;
  int stepsCount = 80;
  bool backwards = false;
  float width = 0.1f;

  virtual void apply(Leds &leds) override;
  virtual String getId() const override { return F("Moving"); }
};

#endif
