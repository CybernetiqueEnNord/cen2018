#ifndef LIGHTHOUSE_STATUS_H
#define LIGHTHOUSE_STATUS_H

#define SIGNATURE 0x12345678

enum class State {
	Initialization, Calibration, Waiting, Activation, Activated
};

struct LighthouseStatus {
    int signature = SIGNATURE;
    int bootCount = 0;
    State state = State::Initialization;
};

#endif
