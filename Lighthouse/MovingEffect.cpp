#include "MovingEffect.h"

void MovingEffect::apply(Leds &aLeds)
{
  float lStep = 1.0f / lightsCount;
  float lStart = lStep / stepsCount * fOffset - lStep / 2;
  float lWidth = lStep * width;
  for (int i = -1; i < lightsCount + 1; i++)
  {
    aLeds.blend(lStart + lStep * i, lWidth, 0, 0, 0, color.red, color.green, color.blue);
    aLeds.blend(lStart + lWidth + lStep * i, lWidth, color.red, color.green, color.blue, 0, 0, 0);
  }
  fOffset += backwards ? -1 : 1;
  if (fOffset >= stepsCount)
  {
    fOffset = 0;
  } else if (fOffset < 0) {
    fOffset = stepsCount - 1;
  }
}
