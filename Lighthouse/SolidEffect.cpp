/*
 * SolidEffect.cpp
 *
 *  Created on: 18 janv. 2020
 *      Author: emmanuel
 */

#include "SolidEffect.h"

SolidEffect::SolidEffect() {
}

void SolidEffect::apply(Leds &leds) {
	leds.setRange(0, 1, color);
}

String SolidEffect::getId() const {
	return F("Solid");
}
