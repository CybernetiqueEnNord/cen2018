/*
 * WaitingEffect.h
 *
 *  Created on: 18 janv. 2020
 *      Author: emmanuel
 */

#ifndef WAITINGEFFECT_H_
#define WAITINGEFFECT_H_

#include "Leds.h"

class WaitingEffect: public Effect {
private:
	int steps = 0;

public:
	WaitingEffect();
	virtual void apply(Leds &leds) override;
	virtual String getId() const override;
};

#endif /* WAITINGEFFECT_H_ */
