#ifndef LEDS_H
#define LEDS_H

#include "Arduino.h"

#include <FastLED.h>
#include "Color.h"
#include "Pointer.h"

class Leds;

class Effect {
private:
	Pointer<Effect> nextEffect;

public:
	virtual void apply(Leds &leds) = 0;
	void applyAll(Leds &leds);
	virtual String getId() const = 0;
	const Pointer<Effect>& getNext() const;
	virtual void prepare(Leds &leds);
	void setNext(Effect &next);
};

class Leds {
private:
	int ledsCount;
	int refreshInterval = 10;
	int nextRun = 0;
	Pointer<Effect> effect;
	Effect * const *effects = NULL;
	int effectsCount = 0;
	float power = 0.0f;
	float meanPower = 0.0f;
	float energy = 0.0f;
	const float LED_CURRENT = 20e-3f;
	const float LED_VOLTAGE = 5.0f;
	bool composition = true;
	bool circular = false;
	CRGB *leds;

	uint8_t compose(uint8_t value1, uint8_t value2) const;

public:
	Leds(int ledsCount, CRGB *leds);
	void blend(float aStart, float aWidth, const Color &aStartColor, const Color &aEndColor);
	void blend(float aStart, float aWidth, uint8_t aRedStart, uint8_t aGreenStart, uint8_t aBlueStart, uint8_t aRedEnd, uint8_t aGreenEnd, uint8_t aBlueEnd);
	void clear();
	bool getComposition() const;
	Pointer<Effect> &getEffect();
	Effect * const * const getEffects(int &count) const;
	float getEnergy() const;
	int getLedsCount() const;
	float getMeanPower() const;
	int getPower() const;
	void setLed(int aIndex, const Color &aColor);
	void setLed(int aIndex, uint8_t aRed, uint8_t aGreen, uint8_t aBlue);
	void setRange(float aStart, float aWidth, const Color &aColor);
	void setRange(float aStart, float aWidth, uint8_t aRed, uint8_t aGreen, uint8_t aBlue);
	void loop();
	void setComposition(bool value);
	void setEffect(Effect &effect);
	bool setEffect(int aIndex);
	void setEffects(Effect * const * const aList, int aCount);
	void update();
};

#endif
