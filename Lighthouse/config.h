#ifndef CONFIG_H_
#define CONFIG_H_

#define ACCELERATOR_MPU6050 1
#define ACCELERATOR_ADXL345 2

#define ACCELEROMETER ACCELERATOR_ADXL345

#define LEDS_COUNT 26
#define PIN_STRIP 18

// Durée de la calibration (ms)
#define TIME_CALIBRATION 5000
// Nombre de mesures consécutives nécessaires à déclencher l'activation
#define THRESHOLD_ACTIVATION 10

// Configuration BLE
#define BLE_NAME "Lighthouse"
#define BLE_UUID "bf63f140-1251-4def-b3c2-bc2765fa16fe"
#define BLE_NOTIFICATION_READY "Ready"
#define BLE_NOTIFICATION_ACTIVATED "Activated"
#define BLE_NOTIFICATION_CALIBRATING "Calibrating"

#ifdef CENWIFI
#include <IPAddress.h>

const char *apSSID = "CenWifiHub";
const char *apPassword = "C3mb2*r*FFp@hRPw";

uint16_t udpPort = 9999;

IPAddress localIp(192, 168, 66, 31);
IPAddress gatewayIp(192, 168, 66, 254);
IPAddress netMask(255, 255, 255, 0);
IPAddress hubIp(192, 168, 66, 1);
uint16_t hubPort = 9999;
#endif

#endif /* CONFIG_H_ */
