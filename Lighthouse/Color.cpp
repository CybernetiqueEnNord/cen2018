#include "Color.h"

Color Color::Black = Color(0, 0, 0);
Color Color::Red = Color(32, 0, 0);

Color::Color(uint8_t aRed, uint8_t aGreen, uint8_t aBlue) :
		red(aRed), green(aGreen), blue(aBlue) {
}

Color Color::adjustLuminance(float aFactor) const {
	hsv lHSV = toHSV();
	lHSV.v *= aFactor;
	Color lColor = toColor(lHSV);
	return lColor;
}

Color Color::fromHSV(float aHue, float aSaturation, float aValue) {
	hsv lHSV;
	lHSV.h = aHue;
	lHSV.s = aSaturation;
	lHSV.v = aValue;
	Color lColor = toColor(lHSV);
	return lColor;
}

hsv Color::toHSV() const {
	rgb lRGB = toRBG();
	hsv lHSV = rgb2hsv(lRGB);
	return lHSV;
}

rgb Color::toRBG() const {
	rgb lRGB;
	lRGB.r = red / 255.0f;
	lRGB.g = green / 255.0f;
	lRGB.b = blue / 255.0f;
	return lRGB;
}

Color Color::toColor(const rgb &aRGB) {
	Color lColor = Color((byte) (aRGB.r * 255), (byte) (aRGB.g * 255), (byte) (aRGB.b * 255));
	return lColor;
}

Color Color::toColor(const hsv &aHSV) {
	rgb lRGB = hsv2rgb(aHSV);
	Color lColor = toColor(lRGB);
	return lColor;
}

hsv Color::rgb2hsv(const rgb &aRGB) {
	hsv lHSV;
	float mrgb, max, delta;

	mrgb = aRGB.r < aRGB.g ? aRGB.r : aRGB.g;
	mrgb = mrgb < aRGB.b ? mrgb : aRGB.b;

	max = aRGB.r > aRGB.g ? aRGB.r : aRGB.g;
	max = max > aRGB.b ? max : aRGB.b;

	lHSV.v = max;                                // v
	delta = max - mrgb;
	if (delta < 0.00001) {
		lHSV.s = 0;
		lHSV.h = 0; // undefrgbed, maybe nan?
		return lHSV;
	}

	if (max > 0.0) {
		// NOTE: if Max is == 0, this divide would cause a crash
		lHSV.s = (delta / max);                  // s
	} else {
		// if max is 0, then r = g = b = 0
		// s = 0, h is undefrgbed
		lHSV.s = 0.0f;
		lHSV.h = NAN;                            // its now undefrgbed
		return lHSV;
	}

	if (aRGB.r >= max) {
		// > is bogus, just keeps compilor happy
		// between yellow & magenta
		lHSV.h = (aRGB.g - aRGB.b) / delta;
	} else if (aRGB.g >= max) {
		// between cyan & yellow
		lHSV.h = 2.0f + (aRGB.b - aRGB.r) / delta;
	} else {
		// between magenta & cyan
		lHSV.h = 4.0f + (aRGB.r - aRGB.g) / delta;
	}

	// degrees
	lHSV.h *= 60.0f;

	if (lHSV.h < 0.0) {
		lHSV.h += 360.0f;
	}

	return lHSV;
}

rgb Color::hsv2rgb(const hsv &aHSV) {
	float hh, p, q, t, ff;
	long i;
	rgb lRGB;

	// < is bogus, just shuts up warnrgbgs
	if (aHSV.s <= 0.0) {
		lRGB.r = aHSV.v;
		lRGB.g = aHSV.v;
		lRGB.b = aHSV.v;
		return lRGB;
	}
	hh = aHSV.h;
	if (hh >= 360.0)
		hh = 0.0f;
	hh /= 60.0f;
	i = (long) hh;
	ff = hh - i;
	p = aHSV.v * (1.0f - aHSV.s);
	q = aHSV.v * (1.0f - (aHSV.s * ff));
	t = aHSV.v * (1.0f - (aHSV.s * (1.0f - ff)));

	switch (i) {
		case 0:
			lRGB.r = aHSV.v;
			lRGB.g = t;
			lRGB.b = p;
			break;

		case 1:
			lRGB.r = q;
			lRGB.g = aHSV.v;
			lRGB.b = p;
			break;

		case 2:
			lRGB.r = p;
			lRGB.g = aHSV.v;
			lRGB.b = t;
			break;

		case 3:
			lRGB.r = p;
			lRGB.g = q;
			lRGB.b = aHSV.v;
			break;

		case 4:
			lRGB.r = t;
			lRGB.g = p;
			lRGB.b = aHSV.v;
			break;

		case 5:
		default:
			lRGB.r = aHSV.v;
			lRGB.g = p;
			lRGB.b = q;
			break;
	}
	return lRGB;
}
