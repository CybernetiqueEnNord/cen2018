#include "config.h"

#include <Arduino.h>

#include <Wire.h>

#include "arrays.h"
#include "CenDebug.h"
#include "Leds.h"
#include "MovingEffect.h"
#include "SolidEffect.h"
#include "WaitingEffect.h"
#include "Vector3D.h"

#include <cmath>

#include "Adafruit_Sensor.h"
#include "Adafruit_ADXL345_U.h"
#include "MPU6050.h"
#include "LighthouseStatus.h"

#ifdef CENBLE
#include "CenBLEServer.h"
#endif

#ifdef CENWIFI
#include <WiFi.h>
#include "CenUdpNode.h"
#endif

#ifdef CENOTA
#include "CenOTA.h"
CenOTA ota;
#endif

CRGB strip[LEDS_COUNT];
Leds leds(LEDS_COUNT, strip);
RTC_NOINIT_ATTR int rtcRam;
LighthouseStatus lighthouseStatus;
LighthouseStatus *rtcStatus = (LighthouseStatus *)&rtcRam;

#if ACCELEROMETER == ACCELERATOR_MPU6050
MPU6050 accelerometer;
#elif ACCELEROMETER == ACCELERATOR_ADXL345
Adafruit_ADXL345_Unified accelerometer;
#endif

unsigned long timestamp = 0;
Vector3D reference;

#ifdef CENBLE
CenBLEServer bleServer;
#endif

#ifdef CENWIFI
WiFiUDP udp;
CenUdpNode bleServer(udp, hubIp, hubPort);
#endif

SolidEffect solidEffect;

#define TO_RADIANS(x) (x * M_PI / 180.0f)

#define EFFECT_NONE 0
#define EFFECT_CALIBRATING 2
#define EFFECT_WAITING 3
#define EFFECT_RUNNING 1

#define PIN_BUTTON 0

void persistData()
{
	*rtcStatus = lighthouseStatus;
}

void initializeLedEffects()
{
	static MovingEffect light1;
	static MovingEffect light2;
	static WaitingEffect waitingEffect;

	FastLED.addLeds<WS2812, PIN_STRIP, GRB>(strip, LEDS_COUNT);

	light1.color = Color(255, 128, 0);
	light1.stepsCount = 80;
	light1.width = 0.15f;
	light1.lightsCount = 1;

	light2.color = Color(0, 0, 8);
	light2.stepsCount = 240;
	light2.lightsCount = 1;
	light2.backwards = true;

	static Effect *list[] = {&light1, &solidEffect, &waitingEffect};
	leds.setEffects(list, ARRAY_SIZE(list));
}

Vector3D readAccelerometer()
{
#if ACCELEROMETER == ACCELERATOR_MPU6050
	float x = accelerometer.getAccelerationX();
	float y = accelerometer.getAccelerationY();
	float z = accelerometer.getAccelerationZ();
#elif ACCELEROMETER == ACCELERATOR_ADXL345
	float x = accelerometer.getX();
	float y = accelerometer.getY();
	float z = accelerometer.getZ();
#endif
	Vector3D value = Vector3D(x, y, z);
	return value;
}

void initializeAccelerometer()
{
#if ACCELEROMETER == ACCELERATOR_MPU6050
	accelerometer.initialize();
#elif ACCELEROMETER == ACCELERATOR_ADXL345
	accelerometer.begin();
#endif
}

void setLed(bool value)
{
	digitalWrite(LED_BUILTIN, value ? HIGH : LOW);
}

void setLighthouseColor(Color color)
{
	solidEffect.color = color;
	leds.setEffect(EFFECT_CALIBRATING);
}

void testButton()
{
	if (digitalRead(PIN_BUTTON) == LOW)
	{
		static unsigned long timestamp = 0;
		unsigned long now = millis();
		if (now > timestamp + 500)
		{
			setLed(true);
			DBG_skv(F("test"), F("notify"), now);
			bleServer.send(String(now));
			timestamp = now;
			setLed(false);
		}
	}
}

void calibrate()
{
	// Calibration
	// On stocke le vecteur de référence de l'orientation au repos.
	// On attend un temps de calibration pendant lequel la variation angulaire ne doit pas dépasser 10°, sinon on recommence la calibration
	Vector3D value = readAccelerometer();
	float angle = value.angle(reference);
	if (reference.isEmpty() || (!isnan(angle) && fabsf(angle) > TO_RADIANS(10)))
	{
		reference = value;
		DBG_p(reference);
		timestamp = millis();
	}
	if (millis() - timestamp > TIME_CALIBRATION)
	{
		lighthouseStatus.state = State::Waiting;
		leds.clear();
		setLed(false);
		leds.setEffect(EFFECT_WAITING);
		bleServer.send(BLE_NOTIFICATION_READY);
		DBG("calibrated");
	}
}

void checkOrientation()
{
	// On compare l'orientation courante au vecteur de référence
	// S'il y a un angle suffisant sur plusieurs mesures consécutives, on passe à l'état Activé
	static int count = 0;
	Vector3D value = readAccelerometer();
	float angle = value.angle(reference);
	if (!isnan(angle) && fabsf(angle) > TO_RADIANS(50))
	{
		if (++count > THRESHOLD_ACTIVATION)
		{
			lighthouseStatus.state = State::Activation;
			setLed(true);
			persistData();
		}
	}
	else
	{
		count = 0;
	}
}

void notifyActivation()
{
	DBG("activated");
	bleServer.send(BLE_NOTIFICATION_ACTIVATED);
}

void activate()
{
	leds.setEffect(EFFECT_RUNNING);
	notifyActivation();
	lighthouseStatus.state = State::Activated;
	persistData();
}

void initialize()
{
	timestamp = millis();
	leds.setEffect(EFFECT_NONE);
	setLed(true);
	setLighthouseColor(Color::Red);
	bleServer.send(BLE_NOTIFICATION_CALIBRATING);
	lighthouseStatus.state = State::Calibration;
}

void setupData()
{
	DBG_skv(F("setup"), F("rtcram"), sizeof rtcStatus);

	// gestion des données persistantes
	if (rtcRam == SIGNATURE)
	{
		// cas d'un reboot
		DBG(F("warm boot"));
		lighthouseStatus = *rtcStatus;
		if (rtcStatus->state >= State::Activation)
		{
			// Si le phare a été activé, on repasse par l'étape activation
			lighthouseStatus.state = State::Activation;
		}
		else
		{
			// Sinon, on fait un démarrage normal
			lighthouseStatus.state = State::Initialization;
		}
	}
	else
	{
		DBG(F("cold boot"));
		persistData();
	}
	DBG_skv(F("setup"), F("boot"), ++rtcStatus->bootCount);
}

void setupBLE()
{
#ifdef CENBLE
	bleServer.initialize(BLE_NAME, BLE_UUID);
#endif
}

void setupWifi()
{
#ifdef CENWIFI
	WiFi.config(localIp, gatewayIp, netMask);
	WiFi.begin(apSSID, apPassword);
	udp.begin(udpPort);
#endif
}

void setup()
{
	Serial.begin(115200);
	pinMode(PIN_STRIP, OUTPUT);
	pinMode(LED_BUILTIN, OUTPUT);
	pinMode(PIN_BUTTON, INPUT_PULLUP);

#ifdef CENOTA
	ota.setupOTA(172);
#endif

	setupData();

	Wire.begin();
	setLed(false);

	DBG("Lighthouse " __DATE__ " " __TIME__);
	DBG("setup");

	setupBLE();
	setupWifi();

	initializeLedEffects();
	initializeAccelerometer();

#ifdef CENOTA
	ota.beginOTA();
#endif

	DBG("started");
}

void loop()
{
#ifdef CENOTA
	ota.handle();
#endif

	switch (lighthouseStatus.state)
	{
	case State::Initialization:
		initialize();
		break;

	case State::Calibration:
		calibrate();
		break;

	case State::Waiting:
		checkOrientation();
		break;

	case State::Activation:
		activate();
		break;

	case State::Activated:
		break;

	default:
		break;
	}
	leds.loop();
	bleServer.loop();
	testButton();
}
