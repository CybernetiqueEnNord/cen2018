#include "Vector3D.h"

#include <math.h>

Vector3D::Vector3D(float x, float y, float z) : x(x), y(y), z(z)
{
}

Vector3D Vector3D::dot(Vector3D v) const
{
    Vector3D result;
    result.x = x * v.x;
    result.y = y * v.y;
    result.z = z * v.z;
    return result;
}

Vector3D Vector3D::cross(Vector3D v) const
{
    Vector3D result;
    result.x = y * v.z - z * v.y;
    result.y = z * v.x - x * v.z;
    result.z = x * v.y - y * v.x;
    return result;
}

float Vector3D::norm() const
{
    float result = x * x + y * y + z * z;
    result = sqrtf(result);
    return result;
}

void Vector3D::normalize()
{
    float n = norm();
    if (n > 0)
    {
        x /= n;
        y /= n;
        z /= n;
    }
    else
    {
        x = y = z = 0;
    }
}

Vector3D Vector3D::normalized() const
{
    Vector3D result = Vector3D(*this);
    result.normalize();
    return result;
}

float Vector3D::angle(Vector3D v) const
{
    Vector3D r = cross(v);
    float nr = r.norm();
    float nt = norm();
    float nv = v.norm();
    if (nr == 0 || nt == 0 || nv == 0)
    {
        return NAN;
    }
    float angle = asinf(nr / nt / nv);
    return angle;
}

size_t Vector3D::printTo(Print &p) const
{
    size_t n = p.print('(');
    n += p.print(x);
    n += p.print(" ; ");
    n += p.print(y);
    n += p.print(" ; ");
    n += p.print(z);
    n += p.println(')');
    return n;
}

bool Vector3D::isEmpty() const
{
    bool result = x == 0 && y == 0 && z == 0;
    return result;
}