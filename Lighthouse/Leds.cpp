#include "Leds.h"

Leds::Leds(int ledsCount, CRGB *leds) :
		ledsCount(ledsCount), leds(leds) {
}

void Leds::blend(float aStart, float aWidth, uint8_t aRedStart, uint8_t aGreenStart, uint8_t aBlueStart, uint8_t aRedEnd, uint8_t aGreenEnd, uint8_t aBlueEnd) {
	int n = ledsCount;
	int s = (int) (n * aStart);
	int w = (int) (n * aWidth);
	float lStep = 1.0f / w;
	int db = aBlueEnd - aBlueStart;
	int dg = aGreenEnd - aGreenStart;
	int dr = aRedEnd - aRedStart;
	for (int i = 0; i <= w; i++) {
		int lStart = s + i;
		if (lStart < 0 || lStart >= n) {
			continue;
		}
		float x = lStep * i;
		uint8_t lBlue = (uint8_t) (aBlueStart + db * x);
		uint8_t lGreen = (uint8_t) (aGreenStart + dg * x);
		uint8_t lRed = (uint8_t) (aRedStart + dr * x);
		setLed(lStart, lRed, lGreen, lBlue);
	}
}

uint8_t Leds::compose(uint8_t value1, uint8_t value2) const {
	uint32_t sq = value1 * value1 + value2 * value2;
	uint8_t result = (uint8_t) sqrt(sq);
	return result;
}

void Leds::setLed(int aIndex, uint8_t aRed, uint8_t aGreen, uint8_t aBlue) {
	if (aIndex < 0 || aIndex >= ledsCount) {
		if (circular) {
			if (aIndex >= ledsCount) {
				aIndex -= ledsCount;
			} else if (aIndex < 0) {
				aIndex += ledsCount;
			}
		} else {
			return;
		}
	}
	if (composition) {
		aRed = compose(aRed, leds[aIndex].r);
		aGreen = compose(aGreen, leds[aIndex].g);
		aBlue = compose(aBlue, leds[aIndex].b);
	}
	leds[aIndex].setRGB(aRed, aGreen, aBlue);
}

void Leds::setRange(float aStart, float aWidth, uint8_t aRed, uint8_t aGreen, uint8_t aBlue) {
	int n = ledsCount;
	int s = (int) (n * aStart);
	int e = s + (int) (n * aWidth);
	for (int i = s; i <= e && i < n; i++) {
		setLed(i, aRed, aGreen, aBlue);
	}
}

void Leds::update() {
	if (effect.isValid()) {
		effect->applyAll(*this);
	} else {
		FastLED.clear(false);
	}
	FastLED.show();
	power = (LED_CURRENT * LED_VOLTAGE) / 255.0f * getPower();
	meanPower = meanPower * 0.95f + power * 0.05f;
	energy += (1e-3f * refreshInterval) * power;
}

void Leds::loop() {
	int now = millis();
	if (now > nextRun) {
		nextRun = now + refreshInterval;
		update();
	}
}

void Leds::setEffect(Effect &effect) {
	this->effect = Pointer<Effect>(&effect);
}

bool Leds::setEffect(int aIndex) {
	if (aIndex <= 0 || aIndex > effectsCount) {
		effect.clear();
		return false;
	}
	setEffect(*effects[aIndex - 1]);
	return true;
}

int Leds::getLedsCount() const {
	return ledsCount;
}

float Leds::getMeanPower() const {
	return meanPower;
}

float Leds::getEnergy() const {
	return energy;
}

int Leds::getPower() const {
	CRGB *pixels = FastLED.leds();
	const int n = ledsCount;
	int power = 0;
	for (int i = 0; i < n; i++) {
		power += pixels[i].r + pixels[i].g + pixels[i].b;
	}
	return power;
}

void Leds::setRange(float aStart, float aWidth, const Color &aColor) {
	setRange(aStart, aWidth, aColor.red, aColor.green, aColor.blue);
}

void Leds::setLed(int aIndex, const Color &aColor) {
	setLed(aIndex, aColor.red, aColor.green, aColor.blue);
}

void Leds::blend(float aStart, float aWidth, const Color &aStartColor, const Color &aEndColor) {
	blend(aStart, aWidth, aStartColor.red, aStartColor.green, aStartColor.blue, aEndColor.red, aEndColor.green, aEndColor.blue);
}

void Leds::setEffects(Effect * const * const aList, int aCount) {
	effects = aList;
	effectsCount = aCount;
}

Effect * const * const Leds::getEffects(int &count) const {
	count = effectsCount;
	return effects;
}

void Leds::clear() {
	FastLED.clear(false);
}

Pointer<Effect> &Leds::getEffect() {
	return effect;
}

bool Leds::getComposition() const {
	return composition;
}

void Leds::setComposition(bool value) {
	composition = value;
}

const Pointer<Effect> &Effect::getNext() const {
	return nextEffect;
}

void Effect::setNext(Effect &next) {
	nextEffect = Pointer<Effect>(&next);
}

void Effect::applyAll(Leds &leds) {
	prepare(leds);
	Pointer<Effect> current = Pointer<Effect>(this);
	while (current.isValid()) {
		current->apply(leds);
		current = current->getNext();
	}
}

void Effect::prepare(Leds &leds) {
	leds.clear();
}
