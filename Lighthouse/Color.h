#ifndef COLOR_H
#define COLOR_H

#include "Arduino.h"

struct hsv {
	float h;
	float s;
	float v;
};

struct rgb {
	float b;
	float g;
	float r;
};

class Color {
private:
	static hsv rgb2hsv(const rgb &aRGB);
	static rgb hsv2rgb(const hsv &aHSV);
	static Color toColor(const rgb &aRGB);
	static Color toColor(const hsv &aHSV);

	hsv toHSV() const;
	rgb toRBG() const;

public:
	uint8_t red;
	uint8_t green;
	uint8_t blue;

	static Color fromHSV(float aHue, float aSaturation, float aValue);
	static Color Red;
	static Color Black;

	Color(uint8_t aRed, uint8_t aGreen, uint8_t aBlue);
	Color adjustLuminance(float aFactor) const;
};

#endif
