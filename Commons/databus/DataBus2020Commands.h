#ifndef DATABUS2020_COMMANDS_H
#define DATABUS2020_COMMANDS_H

#include "DataBusCommands.h"

#define DATABUS_COMMAND_ANCHOR_AREA 'a'
#define DATABUS_COMMAND_COMBINATION 'c'
#define DATABUS_COMMAND_LIGHTHOUSE 'l'
#define DATABUS_COMMAND_MATCH_SIDE 's'
#define DATABUS_COMMAND_SCORE 'p'
#define DATABUS_COMMAND_MATCH_STARTED 'm'
#define DATABUS_COMMAND_ELEMENT_DONE 'd'
#define DATABUS_COMMAND_ELEMENT_QUERY 'e'
#define DATABUS_COMMAND_ELEMENT_STATE_QUERY 'E'

#define DATABUS_RESULT_UNDEFINED 'U'

#define DATABUS_RESULT_ANCHOR_AREA_NORTH 'N'
#define DATABUS_RESULT_ANCHOR_AREA_SOUTH 'S'

#define DATABUS_RESULT_LIGHTHOUSE_INACTIVE '0'
#define DATABUS_RESULT_LIGHTHOUSE_ACTIVE '1'

#define DATABUS_RESULT_MATCHSIDE_DIRECT '0'
#define DATABUS_RESULT_MATCHSIDE_REVERSED '1'

#define DATABUS_RESULT_ELEMENT_NOT_DONE '0'
#define DATABUS_RESULT_ELEMENT_DONE '1'

enum class MatchElements2020 {
    // rack adverse
    OpponentSideReefs,
    // rack de notre côté
    OurSideReefs,
    // chenal dans le grand port (gros)
    BigPortFairway,
    // chenal dans le petit port (petit) première partie (gobelets de la piste)
    SmallPortFairwayPart1,
    // chenal dans le petit port (petit) seconde partie (gobelets du rack latéral)
    SmallPortFairwayPart2,
    // manches à air
    Windsocks,
    // phare
    Lighthouse,
    // rack latéral par le petit
    SideReefsBySmall,
    // rack latéral par le gros
    SideReefsByBig,
    // gobelets en vrac
    LooseBuoys,
    // élément correspondant à la taille de l'énumération
    Count
};

#endif
