/*
 * ActuatorSuction2019Commands.h
 *
 *  Created on: 22 avr. 2018
 *      Author: Anastaszor
 */

#ifndef I2C_ACTUATORSMALL2020COMMANDS_H_
#define I2C_ACTUATORSMALL2020COMMANDS_H_

#include "CommonCommands.h"
#include "ActuatorCommon2020Commands.h"

#define I2C_ADDRESS_ACTUATORSMALL2020 0x61


#define COMMAND_SET_PUMP 'o'  // o<PumpNumber><Status>
#define COMMAND_SET_ARM '9'  // m<ArmNumber><Status>
#define COMMAND_SET_BOTH_FRONT_ARM '-'  // -<position>



#endif /* I2C_ACTUATORSMALL2020COMMANDS_H_ */
