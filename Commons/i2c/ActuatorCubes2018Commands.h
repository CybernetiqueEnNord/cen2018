/*
 * ActuatorCubes2018Commands.h
 *
 *  Created on: 22 avr. 2018
 *      Author: Anastaszor
 */

#ifndef I2C_ACTUATORCUBES2018COMMANDS_H_
#define I2C_ACTUATORCUBES2018COMMANDS_H_

#include "CommonCommands.h"

#define I2C_ADDRESS_CUBES                       0x62

#define I2C_COMMAND_SET_COLOR_GREEN             'g'
#define I2C_COMMAND_SET_COLOR_ORANGE            'o'

#define I2C_COMMAND_SET_STRATEGY_1              'A'
#define I2C_COMMAND_SET_STRATEGY_2              'B'
#define I2C_COMMAND_SET_STRATEGY_3              'C'
#define I2C_COMMAND_SET_STRATEGY_4              'D'

#define I2C_COMMAND_SET_ORIENTATION_NORTH       'N'
#define I2C_COMMAND_SET_ORIENTATION_EAST        'E'
#define I2C_COMMAND_SET_ORIENTATION_SOUTH       'S'
#define I2C_COMMAND_SET_ORIENTATION_WEST        'W'

#define I2C_COMMAND_SET_INITIAL_POSITION        'i'
#define I2C_COMMAND_HANDLE_LOAD_PREPARE         'p'
#define I2C_COMMAND_HANDLE_LOAD_JOCKER          'j'
#define I2C_COMMAND_HANDLE_UNLOAD_JOCKER        'a'
#define I2C_COMMAND_HANDLE_UNLOAD_JOCKER_LEVEL2 'b'

#define I2C_COMMAND_HANDLE_OPEN                 '+'
#define I2C_COMMAND_HANDLE_CLOSE                '-'

#define I2C_COMMAND_MODE_PELLE                  'P'

#define I2C_COMMAND_HANDLE_LOAD_1               '1'
#define I2C_COMMAND_HANDLE_LOAD_2               '2'
#define I2C_COMMAND_HANDLE_LOAD_3               '3'
#define I2C_COMMAND_HANDLE_LOAD_4               '4'
#define I2C_COMMAND_HANDLE_LOAD_5               '5'

#define I2C_COMMAND_HANDLE_RESET_NEW_TOWER      'u'

#define I2C_COMMAND_HANDLE_PREPARE_UNLOAD_1     '6'
#define I2C_COMMAND_HANDLE_PREPARE_UNLOAD_2     '7'
#define I2C_COMMAND_HANDLE_PREPARE_UNLOAD_3     '8'
#define I2C_COMMAND_HANDLE_PREPARE_UNLOAD_4     '9'
#define I2C_COMMAND_HANDLE_PREPARE_UNLOAD_5     '0'

#define I2C_COMMAND_READ_COMBINATION            'c'

#define I2C_COMMAND_HANDLE_ABRITRARY_HEIGHT     'h'
#define I2C_COMMAND_HANDLE_ARBITRARY_DIRECTION  'd'

#define I2C_COMMAND_SET_UNLOAD_COMBINATION      'A'

#define I2C_COMMAND_HANDLE_PUSH                 'P'

#endif /* I2C_ACTUATORCUBES2018COMMANDS_H_ */
