/*
 * ActuatorCommon2019Commands.h
 *
 *  Created on: 25 mai 2019
 *      Author: emmanuel
 */

#ifndef I2C_ACTUATORCOMMON2020COMMANDS_H_
#define I2C_ACTUATORCOMMON2020COMMANDS_H_

#define I2C_COMMAND_SET_COLOR_BLUE             	'0'
#define I2C_COMMAND_SET_COLOR_YELLOW            	'1'
#define I2C_COMMAND_10_SECOND_WAIT_READY_TEST '5'

#define COMMAND_SERIAL_I2C_SCAN '2'
#define COMMAND_SERIAL_I2C_ADDRESS 'a'

#define I2C_COMMAND_SERVO 'S'  //SServoType;<ServoNumber>;<Position in 4096>

#define I2C_COMMAND_START_POSITION 'z'
#define I2C_COMMAND_TEST_ALL_SERVO_ARE_WORKING 'e'

#define COMMAND_FLAGS 'h' // Hisser les drapeau

#define COMMAND_LATERAL_ARM_MANCHE_A_AIR_OUT 'i' // Sortir le bras
#define COMMAND_LATERAL_ARM_MANCHE_A_AIR_RETRACTED 'j' // Rentrer le bras
#define COMMAND_LATERAL_ARM_PHARE_OUT 'c' // Sortir le bras
#define COMMAND_LATERAL_ARM_PHARE_RETRACTED 'v' // Rentrer le bras







#endif /* I2C_ACTUATORCOMMON2020COMMANDS_H_ */
