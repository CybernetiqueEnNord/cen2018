#ifndef BEACON_COMMANDS_H
#define BEACON_COMMANDS_H

#include "CommonCommands.h"

#define COMMAND_I2C_MEASURES 'm'
#define COMMAND_I2C_HMI 'h'
#define COMMAND_I2C_VERSION 'v'
#define COMMAND_I2C_THRESHOLD 't'
#define COMMAND_I2C_CONFIG 'c'
#define COMMAND_I2C_LED 'l'
#define COMMAND_I2C_BUZZER 'b'

#endif // BEACON_COMMANDS_H
