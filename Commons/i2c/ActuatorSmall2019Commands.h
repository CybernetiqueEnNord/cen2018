/*
 * ActuatorSuction2019Commands.h
 *
 *  Created on: 22 avr. 2018
 *      Author: Anastaszor
 */

#ifndef I2C_ACTUATORSMALL2019COMMANDS_H_
#define I2C_ACTUATORSMALL2019COMMANDS_H_

#include "CommonCommands.h"
#include "ActuatorCommon2019Commands.h"

#define I2C_ADDRESS_ACTUATORSMALL2019 0x61

#define I2C_COMMAND_SET_COLOR_BLUE             'p'
#define I2C_COMMAND_SET_COLOR_YELLOW            'y'

#define I2C_COMMAND_GOLDENIUM_PUMP_ON                     'n'
#define I2C_COMMAND_GOLDENIUM_PUMP_OFF                    'f'

#define I2C_COMMAND_TRIO_PUMP_ON                     'w'
#define I2C_COMMAND_TRIO_PUMP_OFF                    'x'

#define I2C_COMMAND_ARM_DOWN                    'd'
#define I2C_COMMAND_ARM_UP                      'u'

#define I2C_COMMAND_PELLE_DEPOSE_GOLDENIUM			'm'

#define I2C_COMMAND_CHECK_GOLDENIUM 'c'
#define I2C_COMMAND_QUERY_GOLDENIUM 'q'

#define I2C_COMMAND_SET_SERVO_GUIDE 'v'
#define I2C_COMMAND_UNSET_SERVO_GUIDE 'b'

#endif /* I2C_ACTUATORSMALL2019COMMANDS_H_ */
