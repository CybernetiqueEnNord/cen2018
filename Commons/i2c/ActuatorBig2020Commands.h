/*
 * ActuatorBig2020Commands.h
 *
 *  Created on: 28 sep. 2019
 *      Author: Emmanuel
 */

#ifndef I2C_ACTUATORBIG2020COMMANDS_H_
#define I2C_ACTUATORBIG2020COMMANDS_H_

#include "CommonCommands.h"
#include "ActuatorCommon2020Commands.h"

#define I2C_ADDRESS_ACTUATORBIG2020                 0x64

enum class ServoType {
	Arm, Finger, LateralArm, Flag, bottomCatcher
};

enum class CenGlassColor {
	undefined, Green, Red
};

enum class BottomBuoySensorStatus {
	Error, BuoyPresent, Uncertain, Nothing
};

enum class BottomBuoyStatus {
	Disarmed, Armed, Locked
};

//Not implemented yet. Méthode créé, mais vide.
#define COMMAND_SET_CODE 'r' // r;<Code number,0, 1 2 or 3>  From blue point of view, 1:VVR 2:VRV 3:RVV

enum class Code {
	Undefined,
	VRVVR_VRRVR_1,
	VVRVR_VRVRR_2,
	VVVRR_VVRRR_3,
	VVVVR_VRRRR_4, // 1 error
	RVRVR // our side
};
//En regardant la girouette:
//0:  Undefined
//1:  VVVRR_VVRRR_3
//2:  VVRVR_VRVRR_2
//3:  VRVVR_VRRVR_1

//Not implemented yet:
#define COMMAND_READY_TO_TAKE 't' // t<FrontRear>;<FirstOrComplement>  Descendre les bras correspondant a la couleur, pour le premier distributeur, ouvrir les doigts correspondants
#define COMMAND_READY_TO_TAKE_INDIVIDUAL 'b' // w<FrontRear>;01100  descendre les bras correspondants

enum class FrontRear {
	Front, Rear
};

//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
enum class FirstOrComplement {
	FirstOponent, ComplementOurSide, FirstOurSide, ComplementOponent
};

//Implemented Ok, to be tested.
#define COMMAND_HOLD 'q' // Commencer à pincer tous les gobelets, dont un des bras est descendu.
#define COMMAND_RAISE 'd' // Remonter tous les bras déja descendus.
#define COMMAND_FRONT_DOWN '-'
#define COMMAND_REAR_DOWN ','

#define COMMAND_FRONT_RELEASE 'f' // Descentre les bras, ouvrir les doigts. // Same as Ready to take All.
#define COMMAND_REAR_RELEASE 'g'

#define COMMAND_BOTTOM_BOTTOM_BUOY_FRONT_RELEASE_DISARM_AND_RESET 'm' //Liberer les bouées et désarmer la capture
#define COMMAND_BOTTOM_BOTTOM_BUOY_FRONT_ARM 'n'
#define COMMAND_BOTTOM_BOTTOM_BUOY_REAR_RELEASE_DISARM_AND_RESET 'o' //Liberer les bouées et désarmer la capture
#define COMMAND_BOTTOM_BOTTOM_BUOY_REAR_ARM 'p'

#define COMMAND_SET_ARM '9'

#define COMMAND_SENSOR_STATUS 'l'//Affiche en RS232 tous les capteurs, surtout pour le débug
#define COMMAND_GET_BOTTOM_BUOY_DATA 'w'//Envoi le décompte des gobelets avant / arrière
#define COMMAND_GET_TOP_BUOY_DATA 'x'//Envoi le décompte des gobelets avant / arrière

#define COMMAND_LATERAL_ARM_PUSH_BUOY '<' // Sortir le bras
#define COMMAND_LATERAL_ARM_PUSH_BUOY_RETRACTED '>' // Rentrer le bras

#define I2C_COMMAND_DANCE '#'

#endif /* I2C_ACTUATORBIG2020COMMANDS_H_ */
