/*
 * CommonCommands.h
 *
 *  Created on: 8 mai 2018
 *      Author: Emmanuel
 */

#ifndef I2C_COMMONCOMMANDS_H_
#define I2C_COMMONCOMMANDS_H_

#define I2C_COMMAND_STATUS 's'
#define I2C_COMMAND_KILL 'k'

#define I2C_STATUS_READY 'r'
#define I2C_STATUS_BUSY 'b'

#define COMMAND_SEPARATOR ';'

enum class ServoPosition {
	Deactivated, Center, Up, Down, Working, Depose
};

enum class Color {
	Undefined, Blue, Yellow
};

#endif /* I2C_COMMONCOMMANDS_H_ */
