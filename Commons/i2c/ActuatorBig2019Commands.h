/*
 * ActuatorCubes2018Commands.h
 *
 *  Created on: 22 avr. 2018
 *      Author: Anastaszor
 */

#ifndef I2C_ACTUATORCUBES2018COMMANDS_H_
#define I2C_ACTUATORCUBES2018COMMANDS_H_

#include "CommonCommands.h"
#include "ActuatorCommon2019Commands.h"

#define I2C_ADDRESS_ACTUATORBIG2019                 0x64

#define I2C_COMMAND_SET_COLOR_BLUE             	'p'
#define I2C_COMMAND_SET_COLOR_YELLOW            	'y'

#define I2C_COMMAND_TRIO_PUMP_AR_ON                 '2'
#define I2C_COMMAND_TRIO_PUMP_AR_OFF                'z'
#define I2C_COMMAND_TRIO_PUMP_OFF_AR_CENTER			'e'
#define I2C_COMMAND_TRIO_PUMP_OFF_AR_LEFT			'r'
#define I2C_COMMAND_TRIO_PUMP_OFF_AR_RIGHT			't'

//TODO refactor into one order
#define I2C_COMMAND_AR_DOWN                    		'u'
#define I2C_COMMAND_AR_DEPOSE_BAS              		'1'
#define I2C_COMMAND_AR_DEPOSE_CENTER                'o'
#define I2C_COMMAND_AR_DEPOSE_HAUT                  'q'
#define I2C_COMMAND_AR_UP                      		'5'


#define I2C_COMMAND_TRIO_PUMP_AV_ON                 'd'
#define I2C_COMMAND_TRIO_PUMP_AV_OFF                'f'

//TODO refactor into one order
#define I2C_COMMAND_AV_DOWN                    		'3'
#define I2C_COMMAND_AV_DEPOSE_BAS              		'm'
#define I2C_COMMAND_AV_DEPOSE_CENTER                'w'
#define I2C_COMMAND_AV_DEPOSE_HAUT                  'x'
#define I2C_COMMAND_AV_UP                      		'4'

#define I2C_COMMAND_PELLE_DEBLAYAGE					'n'


#endif /* I2C_ACTUATORCUBES2018COMMANDS_H_ */
