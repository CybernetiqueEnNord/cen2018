/*
 * ActuatorCommon2019Commands.h
 *
 *  Created on: 25 mai 2019
 *      Author: emmanuel
 */

#ifndef I2C_ACTUATORCOMMON2019COMMANDS_H_
#define I2C_ACTUATORCOMMON2019COMMANDS_H_

#define I2C_COMMAND_TRIO_PUMP_OFF_AV_CENTER			'g'
#define I2C_COMMAND_TRIO_PUMP_OFF_AV_LEFT			'h'
#define I2C_COMMAND_TRIO_PUMP_OFF_AV_RIGHT			'i'

#define I2C_COMMAND_PELLE_RENTREE                    'j'
#define I2C_COMMAND_PELLE_PRISE                      'l'

#endif /* I2C_ACTUATORCOMMON2019COMMANDS_H_ */
