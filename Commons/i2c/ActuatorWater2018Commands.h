/*
 * ActuatorWater2018Commands.h
 *
 *  Created on: 21 avr. 2018
 *      Author: Emmanuel
 */

#ifndef I2C_ACTUATORWATER2018COMMANDS_H_
#define I2C_ACTUATORWATER2018COMMANDS_H_

#include "CommonCommands.h"

#define I2C_ADDRESS 0x61

#define I2C_COMMAND_SET_COLOR_GREEN 'g'
#define I2C_COMMAND_SET_COLOR_ORANGE 'o'
#define I2C_COMMAND_HANDLE_TAP 't'
#define I2C_COMMAND_HANDLE_BEE 'b'
#define I2C_COMMAND_OPEN_DISPENSER 'd'

#endif /* I2C_ACTUATORWATER2018COMMANDS_H_ */
