#ifndef CENBLESCANNER_H
#define CENBLESCANNER_H

#include "BLEDevice.h"
#include "CenBLEClient.h"

class CenBLEScanner : public BLEAdvertisedDeviceCallbacks
{
private:
    CenBLEClient *const *clients;
    int clientsCount;
    bool active = false;

    void connectClients();
    void start();

protected:
    virtual void onResult(BLEAdvertisedDevice advertisedDevice) override;

public:
    CenBLEScanner(CenBLEClient *const *clients, unsigned int clientsCount);
    bool areAllClientsConnected() const;
    CenBLEClient *findClient(BLERemoteCharacteristic *characteristic) const;
    bool isScanning() const;
    void loop();
};

#endif
