#ifndef TESTINPUTPARSER_H
#define TESTINPUTPARSER_H

#include "Stream.h"

struct NullStream : public Stream
{
    NullStream(void) { return; }
    int available(void) { return 0; }
    void flush(void) { return; }
    int peek(void) { return -1; }
    int read(void) { return -1; }
    size_t write(uint8_t u_Data) { return 1; }
};

void runTest(Print &output);

#endif