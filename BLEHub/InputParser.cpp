#include "InputParser.h"

#include "arrays.h"
#include <Arduino.h>

#define SIZE_PAYLOAD (offsetof(MatchStatus, valid))

InputParser::InputParser(Stream &inputStream) : inputStream(inputStream)
{
}

void InputParser::parse(uint8_t input)
{
    status.valid = false;

    switch (state)
    {
    case 0 ... 7:
        if (input == getExpectedInput())
        {
            state++;
        }
        else
        {
            state = 0;
        }
        break;

    case 8 ... (SIZE_PAYLOAD + 7):
        uint8_t *buffer = (uint8_t *)&status.emitter;
        buffer[state - 8] = input;
        state++;
        break;
    }

    if (state == (SIZE_PAYLOAD + 8))
    {
        state = 0;
        status.switchBytesOrder();
        status.valid = true;
        status.validTimestamp = millis();
    }
}

uint8_t InputParser::getExpectedInput() const
{
    switch (state)
    {
    case 0:
        return 0xac;

    case 1:
        return 0xed;

    case 2:
        return 0;

    case 3:
        return 5;

    case 4:
        return 0x77;

    case 5:
        // taille de la structure sans l'en-tête (6 octets) + 2 octets
        return SIZE_PAYLOAD + 2;

    case 6:
        return 0x05;

    case 7:
        return 0x35;
    }
    return 0;
}

const MatchStatus &InputParser::getStatus() const
{
    return status;
}

void InputParser::loop()
{
    while (inputStream.available() > 0)
    {
        int input = inputStream.read();
        if (input >= 0)
        {
            parse(input);
            if (status.valid)
            {
                break;
            }
        }
    }
}

size_t printValue(Print &p, const __FlashStringHelper *str, long value)
{
    size_t result = p.print(str);
    result += p.println(value);
    return result;
}

void reverseBytes(uint16_t &x)
{
    x = (x >> 8) | (x << 8);
}

void reverseBytes(uint32_t &x)
{
    x = (x & 0x0000FFFF) << 16 | (x & 0xFFFF0000) >> 16;
    x = (x & 0x00FF00FF) << 8 | (x & 0xFF00FF00) >> 8;
}

void reverseBytes(uint64_t &x)
{
    x = (x & 0x00000000FFFFFFFF) << 32 | (x & 0xFFFFFFFF00000000) >> 32;
    x = (x & 0x0000FFFF0000FFFF) << 16 | (x & 0xFFFF0000FFFF0000) >> 16;
    x = (x & 0x00FF00FF00FF00FF) << 8 | (x & 0xFF00FF00FF00FF00) >> 8;
}

size_t MatchStatus::printTo(Print &p) const
{
    size_t result = printValue(p, F("dataTimestamp = "), validTimestamp);
    result += printValue(p, F("emitter = "), emitter);
    result += printValue(p, F("emitterTimestamp = "), timestamp);
    result += printValue(p, F("matchElapsedSeconds = "), matchElapsedSeconds);
    result += printValue(p, F("weatherVane = "), (uint8_t)weatherVane);
    result += printValue(p, F("matchSide = "), (uint16_t)matchSide);
    result += printValue(p, F("combination = "), (uint16_t)buoysCombination);
    for (int i = 0; i < ARRAY_SIZE(robots); i++)
    {
        result += printValue(p, F("* robot "), i);
        result += robots[i].printTo(p);
    }
    return result;
}

void MatchStatus::switchBytesOrder()
{
    reverseBytes(timestamp);
    reverseBytes(matchElapsedSeconds);
    for (int i = 0; i < ARRAY_SIZE(robots); i++)
    {
        robots[i].switchBytesOrder();
    }
    uint16_t *side = (uint16_t *)&matchSide;
    reverseBytes(*side);
    uint16_t *combination = (uint16_t *)&buoysCombination;
    reverseBytes(*combination);
}

size_t RobotStatus::printTo(Print &p) const
{
    size_t result = printValue(p, F("robotId = "), robotId);
    result += printValue(p, F("robotTeam = "), robotTeam);
    result += printValue(p, F("robotType = "), robotType);
    result += printValue(p, F("robotSide = "), (uint16_t)robotSide);
    result += printValue(p, F("positionError = "), positionError);
    result += printValue(p, F("x = "), x);
    result += printValue(p, F("y = "), y);
    result += printValue(p, F("relativePositionError = "), relativePositionError);
    result += printValue(p, F("dx = "), dx);
    result += printValue(p, F("dy = "), dy);
    result += p.print(F("data = "));
    for (int i = 0; i < ARRAY_SIZE(data); i++)
    {
        result += p.print(data[i], HEX);
        result += p.print(' ');
    }
    result += p.println();
    return result;
}

void RobotStatus::switchBytesOrder()
{
    reverseBytes(robotId);
    reverseBytes(robotTeam);
    reverseBytes(robotType);
    uint16_t *side = (uint16_t *)&robotSide;
    reverseBytes(*side);
    reverseBytes((uint16_t &)positionError);
    reverseBytes((uint16_t &)x);
    reverseBytes((uint16_t &)y);
    reverseBytes((uint16_t &)relativePositionError);
    reverseBytes((uint16_t &)dx);
    reverseBytes((uint16_t &)dy);
}
