#include "CenBLEScanner.h"

#include "CenDebug.h"

CenBLEScanner::CenBLEScanner(CenBLEClient *const *clients, unsigned int clientsCount) : clients(clients), clientsCount(clientsCount)
{
}

void CenBLEScanner::start()
{
    active = true;
    DBG_skv(F("BLE"), F("clientsCount"), clientsCount);

    // Retrieve a Scanner and set the callback we want to use to be informed when we
    // have detected a new device.  Specify that we want active scanning and start the
    // scan to run for 5 seconds.
    DBG(F("scan started"));
    BLEScan *pBLEScan = BLEDevice::getScan();
    pBLEScan->setAdvertisedDeviceCallbacks(this);
    pBLEScan->setInterval(1349);
    pBLEScan->setWindow(449);
    pBLEScan->setActiveScan(true);
    pBLEScan->start(1, true);
    DBG(F("scan done"));
    active = false;
}

bool CenBLEScanner::isScanning() const
{
    return active;
}

void CenBLEScanner::onResult(BLEAdvertisedDevice advertisedDevice)
{
    DBG_skv(F("BLE"), F("scan"), advertisedDevice.toString().c_str());

    // We have found a device, let us now see if it contains the service we are looking for.
    for (int i = 0; i < clientsCount; i++)
    {
        CenBLEClient *client = clients[i];
        if (client->isConnected())
        {
            continue;
        }
        const BLEUUID &uuid = client->getServiceUUID();
        if (advertisedDevice.haveServiceUUID() && advertisedDevice.isAdvertisingService(uuid))
        {
            DBG_skv(F("BLE"), F("service"), BLEUUID(uuid).toString().c_str());
            client->handleDevice(advertisedDevice);
        }
    }
}

bool CenBLEScanner::areAllClientsConnected() const
{
    bool result = true;
    for (int i = 0; i < clientsCount; i++)
    {
        CenBLEClient *client = clients[i];
        result &= client->isConnected();
        if (!result)
        {
            break;
        }
    }
    return result;
}

void CenBLEScanner::loop()
{
    if (!areAllClientsConnected())
    {
        start();
    }
}

CenBLEClient *CenBLEScanner::findClient(BLERemoteCharacteristic *characteristic) const
{
    for (int i = 0; i < clientsCount; i++)
    {
        CenBLEClient *client = clients[i];
        BLEUUID uuid = characteristic->getUUID();
        if (uuid.equals(client->getCharacteristicUUID()))
        {
            return client;
        }
    }
    return nullptr;
}

void CenBLEScanner::connectClients()
{
    for (int i = 0; i < clientsCount; i++)
    {
        CenBLEClient *client = clients[i];
        client->connect();
    }
}