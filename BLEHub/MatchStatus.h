#ifndef MATCHSTATUS_H
#define MATCHSTATUS_H

#include <inttypes.h>
#include <Printable.h>

enum class WeatherVane : uint8_t
{
    Unknown,
    North,
    South
};

enum class MatchSide : uint16_t
{
    Unknown,
    Yellow,
    Blue
};

enum class BuoysCombination : uint16_t
{
    Unknown,
    RGG_RRG,
    GRG_RGR,
    GGR_GRR
};

#pragma pack(1)

struct RobotStatus
{
    uint16_t robotId = 0;
    uint16_t robotTeam = 0;
    uint16_t robotType = 0;
    MatchSide robotSide = MatchSide::Unknown;
    int16_t positionError = 0;
    int16_t x = 0;
    int16_t y = 0;
    int16_t relativePositionError = 0;
    int16_t dx = 0;
    int16_t dy = 0;
    uint8_t data[16] = {0};
    size_t printTo(Print &p) const;
    void switchBytesOrder();
};

struct MatchStatus
{
    uint8_t emitter = 0;
    uint64_t timestamp = 0;
    uint16_t matchElapsedSeconds = 0;
    WeatherVane weatherVane = WeatherVane::Unknown;
    struct RobotStatus robots[4];
    MatchSide matchSide;
    BuoysCombination buoysCombination;
    // Fin de la structure distante, champs supplémentaires pour la validation locale
    bool valid = false;
    unsigned long validTimestamp = 0;
    size_t printTo(Print &p) const;
    void switchBytesOrder();
};

struct LightHouseStatus
{
    bool activated = false;
};

struct RobotData
{
    int16_t score = 0;
    bool matchStarted = false;
};

#define SIGNATURE 0x12345678

struct DataBusStatus
{
    int signature = SIGNATURE;
    MatchStatus matchStatus;
    LightHouseStatus lightHouseStatus;
    RobotData bigRobot;
    RobotData smallRobot;
    unsigned long elementsMask = 0;
    bool pushMode = false;
    int bootCount = 0;
};

#endif