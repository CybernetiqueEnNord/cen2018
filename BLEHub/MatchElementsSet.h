#ifndef MATCH_ELEMENTS_H
#define MATCH_ELEMENTS_H

#include <bitset>

#include "Commons/databus/DataBus2020Commands.h"

class MatchElementsSet
{
private:
    std::bitset<(size_t)MatchElements2020::Count> set;

public:
    MatchElementsSet();
    unsigned long getMaskValue() const;
    bool isDone(MatchElements2020 element) const;
    void setDone(MatchElements2020 element);
    void setMaskValue(unsigned long value);
};

#endif