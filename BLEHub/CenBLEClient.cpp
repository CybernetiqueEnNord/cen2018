#include "CenBLEClient.h"

#include <algorithm>
#include "arrays.h"
#include "CenDebug.h"
#include "CenBLEConfig.h"

notify_callback CenBLEClient::callback = nullptr;

CenBLEClient::CenBLEClient(const String &baseUUID)
{
    String uuid = baseUUID;
    uuid[7] = '0';
    service = BLEUUID(uuid.c_str());
    uuid[7] = '1';
    writeCharacteristic = BLEUUID(uuid.c_str());
    uuid[7] = '2';
    readCharacteristic = BLEUUID(uuid.c_str());
}

bool CenBLEClient::isConnected() const
{
    return connected;
}

void CenBLEClient::handleDevice(BLEAdvertisedDevice &device)
{
    this->device = device;
}

void CenBLEClient::connect()
{
    if (connected || !isPeerDeviceKnown())
    {
        return;
    }

    DBG_skv(F("BLE"), F("server"), device.getAddress().toString().c_str());

    pClient = BLEDevice::createClient();
    pClient->setClientCallbacks(this);

    // Connect to the remove BLE Server.
    // if you pass BLEAdvertisedDevice instead of address, it will be recognized type of peer device address (public or private)
    pClient->connect(&device);

    // Obtain a reference to the service we are after in the remote BLE server.
    BLERemoteService *pRemoteService = pClient->getService(service);
    if (pRemoteService == nullptr)
    {
        DBG_skv(F("BLE"), F("missing service"), service.toString().c_str());
        pClient->disconnect();
        return;
    }

    // Obtain a reference to the characteristic in the service of the remote BLE server.
    pRemoteReadCharacteristic = pRemoteService->getCharacteristic(readCharacteristic);
    if (pRemoteReadCharacteristic == nullptr)
    {
        DBG_skv(F("BLE"), F("missing characteristic"), readCharacteristic.toString().c_str());
        pClient->disconnect();
        return;
    }

    pRemoteWriteCharacteristic = pRemoteService->getCharacteristic(writeCharacteristic);
    if (pRemoteWriteCharacteristic == nullptr)
    {
        DBG_skv(F("BLE"), F("missing characteristic"), writeCharacteristic.toString().c_str());
        pClient->disconnect();
        return;
    }

    // Read the value of the characteristic.
    if (pRemoteReadCharacteristic->canRead())
    {
        std::string value = pRemoteReadCharacteristic->readValue();
        DBG_skv(F("BLE"), F("value"), value.c_str());
    }

    if (pRemoteReadCharacteristic->canNotify())
    {
        pRemoteReadCharacteristic->registerForNotify(notifyCallback);
        DBG_skv(F("BLE"), F("notify"), pRemoteReadCharacteristic->getUUID().toString().c_str());
    }

    connected = true;
    return;
}

void CenBLEClient::onConnect(BLEClient *pClient)
{
    DBG_skv(F("BLE"), F("connect"), pClient->getPeerAddress().toString().c_str());
}

void CenBLEClient::onDisconnect(BLEClient *pClient)
{
    DBG_skv(F("BLE"), F("disconnect"), pClient->getPeerAddress().toString().c_str());
    pClient->disconnect();
    BLEAddress address = device.getAddress();
    BLEDevice::getScan()->erase(address);
    device = BLEAdvertisedDevice();
    connected = false;
}

void CenBLEClient::notifyCallback(BLERemoteCharacteristic *pBLERemoteCharacteristic, uint8_t *pData, size_t length, bool isNotify)
{
    DBG_skv(F("BLE"), F("notify"), pBLERemoteCharacteristic->getUUID().toString().c_str());
    DBG_skv(F("BLE"), F("notify value"), std::string((const char *)pData, length).c_str());
    if (callback != nullptr)
    {
        callback(pBLERemoteCharacteristic, pData, length, isNotify);
    }
}

const BLEUUID &CenBLEClient::getServiceUUID() const
{
    return service;
}

const BLEUUID &CenBLEClient::getCharacteristicUUID() const
{
    return readCharacteristic;
}

bool CenBLEClient::isPeerDeviceKnown()
{
    static BLEAddress nullAddress((uint8_t *)"\0\0\0\0\0\0");
    BLEAddress address = device.getAddress();
    return !address.equals(nullAddress);
}

void CenBLEClient::send(const char *data, size_t length)
{
    if (length == 0)
    {
        length = strlen(data);
    }
    DBG_skv(F("BLE"), F("send"), length);
    if (pRemoteWriteCharacteristic != nullptr)
    {
        pRemoteWriteCharacteristic->writeValue((uint8_t *)data, length, false);
    }
    DBG_skv(F("BLE"), F("send"), F("done"));
}

void CenBLEClient::setNotifyCallback(notify_callback callback)
{
    CenBLEClient::callback = callback;
}

bool CenBLEClient::hasInput() const
{
    return inputLength > 0;
}

void CenBLEClient::setInput(const char *data, size_t length)
{
    inputLength = length;
    if (data != nullptr)
    {
        memcpy(input, data, std::min(ARRAY_SIZE(input), length));
    }
}

const char *CenBLEClient::getInput(size_t &length) const
{
    length = inputLength;
    return input;
}

void CenBLEClient::clearInput()
{
    setInput(nullptr, 0);
}
