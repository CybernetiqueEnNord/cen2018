#include "MatchElementsSet.h"

MatchElementsSet::MatchElementsSet()
{
}

bool MatchElementsSet::isDone(MatchElements2020 element) const
{
    unsigned int index = static_cast<unsigned int>(element);
    bool value = set[index];
    return value;
}

void MatchElementsSet::setDone(MatchElements2020 element)
{
    unsigned int index = static_cast<unsigned int>(element);
    set[index] = true;
}

unsigned long MatchElementsSet::getMaskValue() const
{
    return set.to_ulong();
}

void MatchElementsSet::setMaskValue(unsigned long value)
{
    std::bitset<(size_t)MatchElements2020::Count> tmp(value);
    set = tmp;
}