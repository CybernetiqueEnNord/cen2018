#ifndef CENBLECLIENT_H
#define CENBLECLIENT_H

#include "BLEDevice.h"
#include "BLERemoteCharacteristic.h"
#include "WString.h"

class CenBLEScanner;

class CenBLEClient : public BLEClientCallbacks
{
private:
    BLEUUID service;
    BLEUUID readCharacteristic;
    BLEUUID writeCharacteristic;
    bool connected = false;
    BLERemoteCharacteristic *pRemoteReadCharacteristic = nullptr;
    BLERemoteCharacteristic *pRemoteWriteCharacteristic = nullptr;
    BLEClient *pClient = nullptr;
    BLEAdvertisedDevice device;

    char input[20];
    size_t inputLength = 0;

    static notify_callback callback;

    static void notifyCallback(BLERemoteCharacteristic *pBLERemoteCharacteristic, uint8_t *pData, size_t length, bool isNotify);

protected:
    friend class CenBLEScanner;
    void onConnect(BLEClient *pclient);
    void onDisconnect(BLEClient *pclient);
    void handleDevice(BLEAdvertisedDevice &device);

public:
    CenBLEClient(const String &baseUUID);
    void connect();
    void clearInput();
    bool isConnected() const;
    bool isPeerDeviceKnown();
    const BLEUUID &getCharacteristicUUID() const;
    const char *getInput(size_t &length) const;
    const BLEUUID &getServiceUUID() const;
    bool hasInput() const;
    void send(const char *data, size_t length = 0);
    void setInput(const char *data, size_t length);

    static void setNotifyCallback(notify_callback callback);
};

#endif
