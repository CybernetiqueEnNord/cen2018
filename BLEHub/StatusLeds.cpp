#include "StatusLeds.h"

StatusLeds::StatusLeds()
{
    FastLED.addLeds<WS2812, PIN_STRIP, GRB>(leds, LEDS_COUNT);
    FastLED.setBrightness(1);
}

void StatusLeds::setMatchStatus(const MatchStatus &status)
{
    if (!status.valid)
    {
        return;
    }
    setMatchSide(status.matchSide);
    setWeatherVane(status.weatherVane);
    setCombination(status.buoysCombination);
}

void StatusLeds::setMatchSide(MatchSide side)
{
    uint32_t color;
    switch (side)
    {
    case MatchSide::Blue:
        color = CRGB::Blue;
        break;

    case MatchSide::Yellow:
        color = CRGB::Yellow;
        break;

    default:
        color = CRGB::Black;
        break;
    }
    leds[LED_MATCHSIDE].setColorCode(color);
}

void StatusLeds::setWeatherVane(WeatherVane vane)
{
    uint32_t color;
    switch (vane)
    {
    case WeatherVane::North:
        color = CRGB::DarkViolet;
        break;

    case WeatherVane::South:
        color = CRGB::White;
        break;

    default:
        color = CRGB::Red;
        break;
    }
    leds[LED_WEATHERVANE].setColorCode(color);
}

void StatusLeds::update()
{
    FastLED.setBrightness(2);
    FastLED.show();
}

void StatusLeds::setCombination(BuoysCombination combination)
{
    for (int i = LED_COMBINATION; i < LED_COMBINATION + 3; i++)
    {
        leds[i].setColorCode(CRGB::DarkGreen);
    }
    int value = static_cast<int>(combination) - 1;
    if (value >= 0 && value <= 2)
    {
        leds[LED_COMBINATION + value].setColorCode(CRGB::Red);
    }
}

void StatusLeds::setBLEClientConnected(int index, bool connected)
{
    if (index < 0 || index > 2)
    {
        return;
    }
    leds[LED_BLE_CLIENT + index] = hubConnected ? (connected ? CRGB::Yellow : CRGB::DarkBlue) : CRGB::Red;
    if (index == 2 && lightHouseActivated)
    {
        leds[LED_BLE_CLIENT + index] = CRGB::DarkGreen;
    }
}

void StatusLeds::displayInitialization()
{
    int ledCounter = 0;
    while (ledCounter <= 3)
    {
        setMatchSide(MatchSide::Blue);
        update();
        delay(200);
        setMatchSide(MatchSide::Yellow);
        update();
        delay(200);
        ledCounter++;
    }
    setMatchSide(MatchSide::Unknown);
    update();
}

void StatusLeds::setLightHouse(bool activated)
{
    lightHouseActivated = activated;
    if (lightHouseActivated)
    {
        leds[LED_BLE_CLIENT + 2] = CRGB::DarkGreen;
    }
}

void StatusLeds::setHubConnected(bool value)
{
    hubConnected = value;
}
