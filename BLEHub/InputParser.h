#ifndef INPUT_PARSER_H
#define INPUT_PARSER_H

#include "MatchStatus.h"
#include <Stream.h>

class InputParser
{
private:
    Stream &inputStream;
    int state = 0;
    struct MatchStatus status;

    uint8_t getExpectedInput() const;

public:
    InputParser(Stream &inputStream);
    void parse(uint8_t input);
    const MatchStatus &getStatus() const;
    void loop();
};

#endif