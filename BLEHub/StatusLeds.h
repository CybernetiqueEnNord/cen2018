#ifndef STATUSLEDS_H
#define STATUSLEDS_H

#include <FastLed.h>
#include "MatchStatus.h"

#define PIN_STRIP D3

#define LEDS_COUNT 8

#define LED_MATCHSIDE 0
#define LED_WEATHERVANE 1
#define LED_COMBINATION 2
#define LED_BLE_CLIENT 5

class StatusLeds
{
private:
    CRGB leds[LEDS_COUNT];
    bool lightHouseActivated = false;
    bool hubConnected = false;

    void setWeatherVane(WeatherVane vane);
    void setCombination(BuoysCombination combination);

public:
    StatusLeds();
    void displayInitialization();
    void setBLEClientConnected(int index, bool connected);
    void setHubConnected(bool value);
    void setMatchStatus(const MatchStatus &status);
    void setMatchSide(MatchSide side);
    void setLightHouse(bool activated);
    void update();
};

#endif
