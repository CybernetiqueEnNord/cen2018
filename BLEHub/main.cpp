#include <Arduino.h>

#include "arrays.h"
#include "Commons/databus/DataBus2020Commands.h"
#include "CenDebug.h"
//#include "ESP32WatchDog.h"
#include "InputParser.h"
#include "MatchElementsSet.h"
#include "StatusLeds.h"
#include "TestInputParser.h"

#ifdef CENBLE
#include "CenBLEClient.h"
#include "CenBLEConfig.h"
#include "CenBLEScanner.h"
#include "CenBLESecurity.h"
#endif

#ifdef CENWIFI
#include <WiFi.h>
#include "CenUdpClient.h"
#include "CenUdpServer.h"
#include "CenWifiScanner.h"
#endif

#include <FreeRTOS.h>

#define BLE_NOTIFICATION_ACTIVATED "Activated"
#define BLE_NOTIFICATION_READY "Ready"

#ifdef CENOTA
#include "CenOTA.h"
CenOTA ota;
#endif

StatusLeds leds;
InputParser parser(Serial);
MatchElementsSet elements;
DataBusStatus dataBusStatus;
RTC_NOINIT_ATTR int rtcRam;
DataBusStatus *rtcDataBusStatus = (DataBusStatus *)&rtcRam;
//ESP32WatchDog watchdog;

#ifdef CENBLE
#define CenClient CenBLEClient
CenBLEClient bigClient("c23d8e05-6da1-46d5-8cdc-dcd389eaf069");
CenBLEClient smallClient("3c0d4d73-0ccb-495a-bee0-2372d276943c");
CenBLEClient lightHouseClient("bf63f140-1251-4def-b3c2-bc2765fa16fe");
CenBLEClient *clients[] = {&bigClient, &smallClient, &lightHouseClient};
CenBLEScanner scanner(clients, ARRAY_SIZE(clients));
CenBLESecurityCallbacks bleSecurity;
#endif

#ifdef CENWIFI
#define CenClient CenUdpClient
WiFiUDP udp;
CenUdpClient bigClient(udp, IPAddress(192, 168, 66, 11));
CenUdpClient smallClient(udp, IPAddress(192, 168, 66, 21));
CenUdpClient lightHouseClient(udp, IPAddress(192, 168, 66, 31));
CenUdpClient *clients[] = {&bigClient, &smallClient, &lightHouseClient};
CenWifiScanner scanner;
CenUdpServer udpServer(udp);
#endif

#include "CenConfig.h"

unsigned long oldValidStatusTimestamp = 0;
unsigned long scannerTimestamp = 0;

void pushUpdatedData(const MatchStatus &status);

#define sendToTower(message) \
  do                         \
  {                          \
    Serial.print('>');       \
    Serial.println(message); \
  } while (0)

void persistData()
{
  *rtcDataBusStatus = dataBusStatus;
}

void runInputParser(void *data)
{
  DBG(F("task started"));
  DBG_skv(F("parser"), F("core"), xPortGetCoreID());

  while (true)
  {
    parser.loop();
    auto &status = parser.getStatus();
    if (status.valid && status.validTimestamp > dataBusStatus.matchStatus.validTimestamp)
    {
      leds.setMatchStatus(status);
      pushUpdatedData(status);
      dataBusStatus.matchStatus = status;
      persistData();
#ifdef CENDEBUG
      if (oldValidStatusTimestamp != status.validTimestamp)
      {
        oldValidStatusTimestamp = status.validTimestamp;
        status.printTo(Serial);
      }
#endif
    }
  }
  vTaskDelete(NULL);
}

void buildResponse(char command, char *buffer, const MatchStatus &status)
{
  buffer[0] = command;
  switch (command)
  {
  case DATABUS_COMMAND_COMBINATION:
    buffer[1] = '0' + (int)status.buoysCombination;
    break;

  case DATABUS_COMMAND_MATCH_SIDE:
    buffer[1] = '0' + (int)status.matchSide;
    break;

  case DATABUS_COMMAND_ANCHOR_AREA:
    switch (status.weatherVane)
    {
    case WeatherVane::North:
      buffer[1] = DATABUS_RESULT_ANCHOR_AREA_NORTH;
      break;

    case WeatherVane::South:
      buffer[1] = DATABUS_RESULT_ANCHOR_AREA_SOUTH;
      break;

    default:
      buffer[1] = DATABUS_RESULT_UNDEFINED;
      break;
    }
    break;
  }
  if (!status.valid)
  {
    buffer[1] = DATABUS_RESULT_UNDEFINED;
  }
}

void pushData(const char *data, size_t length)
{
  if (!dataBusStatus.pushMode)
  {
    return;
  }
  smallClient.send(data, length);
  bigClient.send(data, length);
}

void pushData(char command, const MatchStatus &status)
{
  char c[2];
  buildResponse(command, c, status);
  pushData(c, ARRAY_SIZE(c));
}

void pushUpdatedData(const MatchStatus &status)
{
  if (!dataBusStatus.pushMode)
  {
    return;
  }

  if (status.buoysCombination != dataBusStatus.matchStatus.buoysCombination)
  {
    pushData(DATABUS_COMMAND_COMBINATION, status);
  }
  if (status.weatherVane != dataBusStatus.matchStatus.weatherVane)
  {
    pushData(DATABUS_COMMAND_ANCHOR_AREA, status);
  }
}

void handleLightHouse(CenClient *client, const char *pData, size_t length)
{
  bool activated = (length == strlen(BLE_NOTIFICATION_ACTIVATED)) && (strncmp(pData, BLE_NOTIFICATION_ACTIVATED, length) == 0);
  if (activated && !dataBusStatus.lightHouseStatus.activated)
  {
    dataBusStatus.lightHouseStatus.activated = activated;
    persistData();
    leds.setLightHouse(true);
    char c[] = {DATABUS_COMMAND_LIGHTHOUSE, activated ? DATABUS_RESULT_LIGHTHOUSE_ACTIVE : DATABUS_RESULT_LIGHTHOUSE_INACTIVE};
    pushData(c, ARRAY_SIZE(c));
  }

  // Réinitialisation de l'état si le phare a été redémarré
  bool ready = (length == strlen(BLE_NOTIFICATION_READY)) && (strncmp(pData, BLE_NOTIFICATION_READY, length) == 0);
  if (ready && dataBusStatus.lightHouseStatus.activated)
  {
    dataBusStatus.lightHouseStatus.activated = false;
    persistData();
    leds.setLightHouse(false);
  }
}

RobotData &getRobotData(CenClient *client)
{
  if (client == &smallClient)
  {
    return dataBusStatus.smallRobot;
  }
  else if (client == &bigClient)
  {
    return dataBusStatus.bigRobot;
  }
  else
  {
    DBG(F("invalid client"));
    static RobotData dummy;
    return dummy;
  }
}

void setRobotScore(CenClient *client, int score)
{
  RobotData &data = getRobotData(client);
  data.score = score;
  persistData();
  DBG_skv(client == &smallClient ? F("small") : F("big"), F("score"), score);
}

void setElementDone(CenClient *client, int elementId)
{
  MatchElements2020 element = static_cast<MatchElements2020>(elementId);
  elements.setDone(element);
  // persistance RTC RAM
  dataBusStatus.elementsMask = elements.getMaskValue();
  persistData();
}

bool isElementDone(int elementId)
{
  MatchElements2020 element = static_cast<MatchElements2020>(elementId);
  bool value = elements.isDone(element);
  return value;
}

void setMatchStarted(CenClient *client)
{
  RobotData &data = getRobotData(client);
  data.matchStarted = true;
  // Dès le début du match, on bascule en mode push
  dataBusStatus.pushMode = true;
  persistData();
  sendToTower(F("started"));
  DBG_skv(client == &smallClient ? F("small") : F("big"), F("match"), F("started"));
}

void handleRobot(CenClient *client, const char *pData, size_t length)
{
  if (length > 0)
  {
    char c[2] = {pData[0], '0'};
    switch (pData[0])
    {
    case DATABUS_COMMAND_LIGHTHOUSE:
      c[1] = dataBusStatus.lightHouseStatus.activated ? DATABUS_RESULT_LIGHTHOUSE_ACTIVE : DATABUS_RESULT_LIGHTHOUSE_INACTIVE;
      break;

    case DATABUS_COMMAND_COMBINATION:
    case DATABUS_COMMAND_MATCH_SIDE:
    case DATABUS_COMMAND_ANCHOR_AREA:
      buildResponse(pData[0], c, dataBusStatus.matchStatus);
      break;

    case DATABUS_COMMAND_SCORE:
    {
      std::string value = std::string(&pData[1], length - 1);
      int score = atoi(value.c_str());
      setRobotScore(client, score);
      return;
    }

    case DATABUS_COMMAND_ELEMENT_DONE:
    {
      std::string value = std::string(&pData[1], length - 1);
      int id = atoi(value.c_str());
      setElementDone(client, id);
      return;
    }

    case DATABUS_COMMAND_ELEMENT_QUERY:
    {
      std::string value = std::string(&pData[1], length - 1);
      int id = atoi(value.c_str());
      c[1] = isElementDone(id) ? DATABUS_RESULT_ELEMENT_DONE : DATABUS_RESULT_ELEMENT_NOT_DONE;
      break;
    }

    case DATABUS_COMMAND_ELEMENT_STATE_QUERY:
    {
      int value = elements.getMaskValue();
      char response[14];
      response[0] = DATABUS_COMMAND_ELEMENT_STATE_QUERY;
      itoa(value, &response[1], 16);
      DBG_skv(F("client"), F("send"), response);
      client->send(response);
      return;
    }

    case DATABUS_COMMAND_MATCH_STARTED:
      setMatchStarted(client);
      return;

    default:
      return;
    }
    // Si l'état du match n'est pas valide, on renvoie toujours la réponse "indéfini"
    if (pData[0] != DATABUS_COMMAND_LIGHTHOUSE && !dataBusStatus.matchStatus.valid)
    {
      c[1] = DATABUS_RESULT_UNDEFINED;
    }
    DBG_skv(F("client"), F("send"), std::string(c, 2).c_str());
    client->send(c, 2);
  }
}

void handleClient(CenClient *client, const char *pData, size_t length)
{
  DBG_skv(F("client"), F("handle"), std::string((const char *)pData, length).c_str());
  if (client == &lightHouseClient)
  {
    handleLightHouse(client, pData, length);
  }
  else
  {
    handleRobot(client, pData, length);
  }
}

void handleClients()
{
#ifdef CENWIFI
  udpServer.handleInput();
#endif

  bool inScan = scanner.isScanning();
  for (int i = 0; i < ARRAY_SIZE(clients); i++)
  {
    CenClient *client = clients[i];
    leds.setBLEClientConnected(i, client->isConnected());
    if (!inScan && !client->isConnected())
    {
      client->connect();
    }
    else if (client->hasInput())
    {
      size_t length;
      const char *data = client->getInput(length);
      handleClient(client, data, length);
      client->clearInput();
    }
  }
}

void handleWifi()
{
#ifdef CENWIFI
#ifndef CENWIFIAP
  {
    static bool wifiConnected = false;
    bool tmpConnected = WiFi.isConnected();
    if (tmpConnected != wifiConnected)
    {
      DBG_skv(F("wifi"), F("connected"), tmpConnected ? F("YES") : F("NO"));
      leds.setHubConnected(tmpConnected);
    }
    wifiConnected = tmpConnected;
  }
#endif
#endif
}

#ifdef CENBLE
void BLENotifyCallback(BLERemoteCharacteristic *pBLERemoteCharacteristic, uint8_t *pData, size_t length, bool isNotify)
{
  CenClient *client = scanner.findClient(pBLERemoteCharacteristic);
  client->setInput((const char *)pData, length);
}
#endif

void sendAlive()
{
#ifdef CENDEBUG
  static unsigned long timestamp = 0;
  unsigned long now = millis();
  if (now > timestamp + 5000)
  {
    DBG(F("alive"));
    timestamp = now;
  }
#endif
}

void setupBLE()
{
#ifdef CENBLE
  CenBLEClient::setNotifyCallback(BLENotifyCallback);
  BLEDevice::setSecurityCallbacks(&bleSecurity);
  BLEDevice::setEncryptionLevel(ESP_BLE_SEC_ENCRYPT);
  BLEDevice::init("hub");
  leds.setHubConnected(true);
  DBG_skv(F("ble"), F("scanner"), F("started"));
#endif
}

void setupInputParser()
{
  DBG_skv(F("serial"), F("inputParser"), F("started"));
  TaskHandle_t taskInputParser = NULL;
  xTaskCreate(runInputParser, "inputParser", 4096, nullptr, 1, &taskInputParser);
}

void setupWifi()
{
#ifdef CENWIFI
  WiFi.persistent(false);

#ifdef CENWIFIAP
  WiFi.softAPConfig(apLocalIP, apGatewayIP, apMask);
  WiFi.softAP(apSSID, apPassword);
  DBG_skv(F("wifi"), F("mode"), F("AP"));
#else
  WiFi.mode(WIFI_MODE_STA);
  WiFi.config(apLocalIP, apGatewayIP, apMask);
  WiFi.begin(apSSID, apPassword);
  DBG_skv(F("wifi"), F("mode"), F("STA"));
#endif

  delay(200);
  udp.begin(udpPort);
  udpServer.setClients(clients, ARRAY_SIZE(clients));
  DBG_skv(F("wifi"), F("udpserver"), F("started"));
#endif
}

void runTests()
{
#ifdef CENDEBUG
  runTest(Serial);
#endif
}

void setupData()
{
  DBG_skv(F("setup"), F("rtcram"), sizeof dataBusStatus);

  // gestion des données persistantes
  if (rtcRam == SIGNATURE)
  {
    // cas d'un reboot
    DBG(F("warm boot"));
    dataBusStatus = *rtcDataBusStatus;
    dataBusStatus.matchStatus.validTimestamp = 0;
    elements.setMaskValue(dataBusStatus.elementsMask);
  }
  else
  {
    DBG(F("cold boot"));
    persistData();
  }
  DBG_skv(F("setup"), F("boot"), ++rtcDataBusStatus->bootCount);
}

void setup()
{
  Serial.begin(115200);
  DBG(F("Hub " __DATE__ " " __TIME__));
  DBG_skv(F("main"), F("setup"), F("started"));
  DBG_skv(F("main"), F("core"), xPortGetCoreID());

#ifdef CENOTA
  ota.setupOTA(171);
#endif

  leds.displayInitialization();

#ifdef CENBLE
  BLEClient::checkCenVersion();
#endif

  setupData();

  runTests();

  setupBLE();
  setupWifi();
  setupInputParser();

#ifdef CENOTA
  ota.beginOTA();
#endif

  DBG_skv(F("main"), F("setup"), F("done"));
}

void loop()
{
#ifdef CENOTA
  ota.handle();
#endif

  //watchdog.update();
  unsigned long now = millis();
  if (now - scannerTimestamp > 3000)
  {
    scannerTimestamp = now;
    scanner.loop();
  }

  sendAlive();
  handleClients();
  leds.update();
  handleWifi();
}
