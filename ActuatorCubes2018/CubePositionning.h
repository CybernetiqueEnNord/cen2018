/*
 * CubePositionning.h
 *
 *  Created on: 18 avr. 2018
 *      Author: Anastaszor
 */

#ifndef CUBEPOSITIONNING_H_
#define CUBEPOSITIONNING_H_

#include "Program.h"	// CubeColor is there

/**
 * This class defines the cubes positionning from the point of view of the
 * robot. From the point of view of the table, the set of cubes can be near
 * the green side or near the orange side, and the robot can come from each
 * of the four cardinal points (north, east, etc.)
 *
 * The cubes are defined from the nearest of the front of the robot, to the
 * farest. This class should not be used directly, and should be generated
 * from the CubePositionningFactory instead.
 * from the CubeLoader instead.
 */
class CubePositionning
{

private:

	/**
	 * The cube that is in the first row, on the middle position. This cube's
	 * color depends on the position of the robot and the position of the cube
	 * set on the table.
	 */
	CubeColor _nearest_cube;

	/**
	 * The cube that is in the middle row, on the left position. This cube's
	 * color depends of the position of the robot and the position of the cube
	 * set on the table.
	 */
	CubeColor _leftmost_cube;

	/**
	 * The cube that is in the middle row, on the center position. This cube's
	 * color does not depends of the position of the robot nor on the position
	 * of the table (and should always be yellow), but is there to be treated
	 * the same way as the other cubes.
	 */
	CubeColor _centered_cube;

	/**
	 * The cube that is in the middle row, on the right position. This cube's
	 * color depends of the position of the robot and the position of the cube
	 * set on the table.
	 */
	CubeColor _rightmost_cube;

	/**
	 * The cube that is in the third row, on the center position. This cube's
	 * color depends of the position of the robot and the position of the cube
	 * set on the table.
	 */
	CubeColor _farer_cube;

public:

	/**
	 * Gets the center cube on the first row from the robot's point of view.
	 */
	CubeColor getNearestCube();

	/**
	 * Sets the center cube on the first row from the robot's point of view.
	 */
	void setNearestCube(CubeColor nearest_cube);

	/**
	 * Gets the left cube on the second row from the robot's point of view.
	 */
	CubeColor getLeftmostCube();

	/**
	 * Sets the left cube on the second row from the robot's point of view.
	 */
	void setLeftmostCube(CubeColor leftmost_cube);

	/**
	 * Gets the center cube on the second row from the robot's point of view.
	 */
	CubeColor getCenteredCube();

	/**
	 * Sets the center cube on the second row from the robot's point of view.
	 */
	void setCenteredCube(CubeColor centered_cube);

	/**
	 * Gets the right cube on the second row from the robot's point of view.
	 */
	CubeColor getRightmostCube();

	/**
	 * Sets the right cube on the second row from the robot's point of view.
	 */
	void setRightmostCube(CubeColor rightmost_cube);

	/**
	 * Gets the center cube on the third row from the robot's point of view.
	 */
	CubeColor getFarerCube();

	/**
	 * Sets the center cube on the third row from the robot's point of view.
	 */
	void setFarerCube(CubeColor farer_cube);

};

#endif /* CUBEPOSITIONNING_H_ */
