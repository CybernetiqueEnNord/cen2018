#include <Servo.h>

// Motor steps per revolution. Most steppers are 200 steps or 1.8 degrees/step
/* @deprecated */
#define MOTOR_STEPS 200
// Target RPM for cruise speed
/* @deprecated */
#define RPM_LOW 40
/* @deprecated */
#define RPM_HIGH 200
// Acceleration and deceleration values are always in FULL steps / s^2
/* @deprecated */
#define MOTOR_ACCEL 2000
/* @deprecated */
#define MOTOR_DECEL 2000

// Microstepping mode. If you hardwired it to save pins, set to the same value here.
/* @deprecated */
#define MICROSTEPS 4

/* @deprecated */
#define DIR 45
/* @deprecated */
#define STEP 44
/* @deprecated */
#define ENABLE 43

#include "A4988.h"
/* @deprecated */
#define MS1 42
/* @deprecated */
#define MS2 41
/* @deprecated */
#define MS3 40

/* @deprecated */
#define PIN_LIMIT_SWITCH_LOW 46
/* @deprecated */
#define PIN_SIGNAL_NUCLEO 2

/* @deprecated */
#define PIN_SERVO_GAUCHE 6
/* @deprecated */
#define PIN_SERVO_DROIT 7
/* @deprecated */
#define PIN_SERVO_POIGNET 9

/* @deprecated */
#define servoGaucheOffset 85
/* @deprecated */
#define servoDroitOffset 140
/* @deprecated */
#define servoPoignetOffset 90

/* @deprecated */
void open();
/* @deprecated */
void positionPelle();
/* @deprecated */
void close();
/* @deprecated */
void centerPoignet();
/* @deprecated */
void droitePoignet();
/* @deprecated */
void gauchePoignet();

void homeSearch();
void setupActionneurCubes();
void miseEnConditionDepart();
void chargerCube();
void loop_old();
