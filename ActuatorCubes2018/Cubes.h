#ifndef CUBES_H
#define CUBES_H

#include "Program.h"
#include "SoftWire.h"
#include "Wire.h"
#include "CubeCommand.h"
#include "CubeLoader.h"
#include "../Commons/i2c/ActuatorCubes2018Commands.h"

#define slave Wire

enum class Action {
	None,
	SetSideOrange,
	SetSideGreen,
	SetInitialPosition,
	SetStrategy1,
	SetStrategy2,
	SetStrategy3,
	SetStrategy4,
	SetStrategy5,
	SetOrientationNorth,
	SetOrientationEast,
	SetOrientationSouth,
	SetOrientationWest,
	Load_Prepare,
	Load_Jocker,
	Unload_Jocker,
	Load_Lv1,
	Load_Lv2,
	Load_Lv3,
	Load_Lv4,
	Load_Lv5,
	ResetNewTower,
	Prepare_Unload_Lv1,
	Prepare_Unload_Lv2,
	Prepare_Unload_Lv3,
	Prepare_Unload_Lv4,
	Prepare_Unload_Lv5,
	Open,
	Close,
	ModePelle,
	ReadCombination,
};

class Cubes : public Program {
	private:
		static uint8_t slaveParametersCount;
		static uint8_t slaveParameters[8];
		static uint8_t slaveCommandResult;

		static void i2cHandleSlaveInput(int bytesCount);
		static void i2cHandleSlaveRequest();
		static void i2cHandleCommand();
		bool isBusy() const;
		void i2cSendStatus();

		uint8_t address;
		SoftWire& master;

		CubeLoader* _cubeLoader;
		int _currentPointsAcquired = 0;
		Orientation _currentOrientation = Orientation::North;
		CubeLocation _currentLocation;
		TakingPosition _currentTaking = TakingPosition::FrontCenter;
		MatchSide _matchSide = MatchSide::Undefined;
		Action _command = Action::None;
		UnloadOrdering* uo;

    void i2cScanMasterBus();
    void initializeMasterI2C();
    void initializeSlaveI2C();

	protected:
		virtual void enterDebug();
		virtual void runInitialization();
		virtual void runStart();
		virtual void runMain();
		virtual void runDebug();
		void handleAction();
    virtual void serialParseInput(const char *buffer, int length);

    virtual void testBonFonctionnement();
    virtual void setColorGreen();
    virtual void setColorOrange();
    virtual void testColorDependantAction();

	public:
		Cubes(uint8_t address, SoftWire& master);
};


extern Cubes cubes;

#endif
