/*
 * CubeCombination.h
 *
 *  Created on: 20 avr. 2018
 *      Author: Anastaszor
 */

#ifndef CUBECOMBINATION_H_
#define CUBECOMBINATION_H_

#include "Program.h"
#include "UnloadOrdering.h"
#include "Rack.h"

class CubeCombination
{

private:

	/**
	 * The color on the left of the sequence.
	 */
	CubeColor _color_left = CubeColor::Not_A_Cube;

	/**
	 * The color on the middle of the sequence.
	 */
	CubeColor _color_middle = CubeColor::Not_A_Cube;

	/**
	 * The color on the left of the sequence.
	 */
	CubeColor _color_right = CubeColor::Not_A_Cube;

public:

	/**
	 * Sets the colors of the sequence.
	 */
	void setCombination(CubeColor left, CubeColor middle, CubeColor right);

	/**
	 * Gets the color on the left of the sequence.
	 */
	CubeColor getColorLeft();

	/**
	 * Gets the color on the middle of the sequence.
	 */
	CubeColor getColorMiddle();

	/**
	 * Gets the color on the right of the sequence.
	 */
	CubeColor getColorRight();

	/**
	 * Gets whether the given rack can handle this combination, i.e. if it has
	 * currently a sufficient number of cubes and they are the right color to
	 * make the this combination.
	 */
	bool canBeHandledBy(Rack* rack);

	/**
	 * Gets an ordering to unload all of the cubes.
	 */
	UnloadOrdering* getUnloadOrderingFrom(Rack* rack);

};

#endif /* CUBECOMBINATION_H_ */
