/*
 * Glissiere.cpp
 *
 *  Created on: 19 avr. 2018
 *      Author: Anastaszor
 */

#include "Glissiere.h"
#include "A4988.h"


// Static Initialization. Initialization with new does not work.
A4988 _stepper(
	GLISSIERE_MOTOR_STEPS,
	GLISSIERE_MOTOR_PIN_DIR,
	GLISSIERE_MOTOR_PIN_STEP,
	GLISSIERE_MOTOR_PIN_ENABLE,
	GLISSIERE_MOTOR_PIN_MS1,
	GLISSIERE_MOTOR_PIN_MS2,
	GLISSIERE_MOTOR_PIN_MS3
);

bool Glissiere::isAtBottom()
{
	if(!this->_is_initialized)
		return true;	// paralyze the motor if something is not initialized

	return 1 == digitalRead(GLISSIERE_PIN_LIMIT_LOW);
}

bool Glissiere::init()
{

	pinMode(GLISSIERE_PIN_LIMIT_LOW, INPUT);

	pinMode(GLISSIERE_PIN_SIGNAL_NUCLEO, INPUT_PULLUP);

	_stepper.enable();

	_stepper.begin(GLISSIERE_MOTOR_RPM_HIGH, GLISSIERE_MOTOR_MICROSTEPS);

	_stepper.setSpeedProfile(A4988::LINEAR_SPEED, GLISSIERE_MOTOR_ACCEL, GLISSIERE_MOTOR_DECEL);

	this->_is_initialized = true;

	// force the setting of the actual position steps to the "right" zero.
	// this->moveToTheBottom();

	return true;
}

bool Glissiere::moveToTheBottom()
{
	if(!this->_is_initialized)
		return false;

	_stepper.startMove(-50000);	// suppose there we have enough
										// steps to cover the whole mvt
										// from the top of the glissiere
										// until the switch is pushed on

	unsigned long startTime = millis();
	// suppose there the 15 sec are enough to make the whole move
	while(!this->isAtBottom() && (millis() - startTime) < 15000)
	{
		if(0 == _stepper.nextAction())
		{
			// there we say that there is no more actions left,
			// we must leave the loop to not have the processor
			// heat as it does nothing
			break;
		}
	}

	_stepper.stop();

	this->_actual_position_steps = 0;

	return true;
}


bool Glissiere::moveToRackLevel1Left(TowerSubLevel level)
{
	if(!this->_is_initialized)
			return false;

	int position = 0;
	switch(level)
	{
		case TowerSubLevel::ON_TOP:
			position = GLISSIERE_RACK_LV1_POSITION_LEFT_ON_TOP_OF_THE_CUBE;
			break;
		case TowerSubLevel::PUSH:
			position = GLISSIERE_RACK_LV1_POSITION_LEFT_PUSH;
			break;
		case TowerSubLevel::RELEASE:
			position = GLISSIERE_RACK_LV1_POSITION_LEFT;
			break;
		case TowerSubLevel::TAKE_BACK:
			position = GLISSIERE_RACK_LV1_POSITION_LEFT_TAKE_BACK;
			break;
		default:
			// nothing to do
			break;
	}

	_stepper.move(position - this->_actual_position_steps);

	this->_actual_position_steps = position;

	return true;
}

bool Glissiere::moveToRackLevel1Right(TowerSubLevel level)
{
	if(!this->_is_initialized)
		return false;

	int position = 0;
	switch(level)
	{
		case TowerSubLevel::ON_TOP:
			position = GLISSIERE_RACK_LV1_POSITION_RIGHT_ON_TOP_OF_THE_CUBE;
			break;
		case TowerSubLevel::PUSH:
			position = GLISSIERE_RACK_LV1_POSITION_RIGHT_PUSH;
			break;
		case TowerSubLevel::RELEASE:
			position = GLISSIERE_RACK_LV1_POSITION_RIGHT;
			break;
		case TowerSubLevel::TAKE_BACK:
			position = GLISSIERE_RACK_LV1_POSITION_RIGHT_TAKE_BACK;
			break;
		default:
			// nothing to do
			break;
	}

	_stepper.move(position - this->_actual_position_steps);

	this->_actual_position_steps = position;

	return true;
}

bool Glissiere::moveToRackLevel2Left(TowerSubLevel level)
{
	if(!this->_is_initialized)
		return false;

	int position = 0;
	switch(level)
	{
		case TowerSubLevel::ON_TOP:
			position = GLISSIERE_RACK_LV2_POSITION_LEFT_ON_TOP_OF_THE_CUBE;
			break;
		case TowerSubLevel::PUSH:
			position = GLISSIERE_RACK_LV2_POSITION_LEFT_PUSH;
			break;
		case TowerSubLevel::RELEASE:
			position = GLISSIERE_RACK_LV2_POSITION_LEFT;
			break;
		case TowerSubLevel::TAKE_BACK:
			position = GLISSIERE_RACK_LV2_POSITION_LEFT_TAKE_BACK;
			break;
		default:
			// nothing to do
			break;
	}

	_stepper.move(position - this->_actual_position_steps);

	this->_actual_position_steps = position;

	return true;
}

bool Glissiere::moveToRackLevel2Right(TowerSubLevel level)
{
	if(!this->_is_initialized)
		return false;

	int position = 0;
	switch(level)
	{
		case TowerSubLevel::ON_TOP:
			position = GLISSIERE_RACK_LV2_POSITION_RIGHT_ON_TOP_OF_THE_CUBE;
			break;
		case TowerSubLevel::PUSH:
			position = GLISSIERE_RACK_LV2_POSITION_RIGHT_PUSH;
			break;
		case TowerSubLevel::RELEASE:
			position = GLISSIERE_RACK_LV2_POSITION_RIGHT;
			break;
		case TowerSubLevel::TAKE_BACK:
			position = GLISSIERE_RACK_LV2_POSITION_RIGHT_TAKE_BACK;
			break;
		default:
			// nothing to do
			break;
	}

	_stepper.move(position - this->_actual_position_steps);

	this->_actual_position_steps = position;

	return true;
}

bool Glissiere::moveToRackLevel3Left(TowerSubLevel level)
{
	if(!this->_is_initialized)
			return false;

	int position = 0;
	switch(level)
	{
		case TowerSubLevel::ON_TOP:
			position = GLISSIERE_RACK_LV3_POSITION_LEFT_ON_TOP_OF_THE_CUBE;
			break;
		case TowerSubLevel::PUSH:
			position = GLISSIERE_RACK_LV3_POSITION_LEFT_PUSH;
			break;
		case TowerSubLevel::RELEASE:
			position = GLISSIERE_RACK_LV3_POSITION_LEFT;
			break;
		case TowerSubLevel::TAKE_BACK:
			position = GLISSIERE_RACK_LV3_POSITION_LEFT_TAKE_BACK;
			break;
		case TowerSubLevel::JUST_STABILYZE_JOCKER_TOWER:
			position = GLISSIERE_RACK_LV3_POSITION_LEFT_STABILYZE_JOCKER_TOWER;
			break;
		default:
			// nothing to do
			break;
	}

	_stepper.move(position - this->_actual_position_steps);

	this->_actual_position_steps = position;

	return true;
}

bool Glissiere::moveToRackLevel3Right(TowerSubLevel level)
{
	if(!this->_is_initialized)
		return false;

	int position = 0;
	switch(level)
	{
		case TowerSubLevel::ON_TOP:
			position = GLISSIERE_RACK_LV3_POSITION_RIGHT_ON_TOP_OF_THE_CUBE;
			break;
		case TowerSubLevel::PUSH:
			position = GLISSIERE_RACK_LV3_POSITION_RIGHT_PUSH;
			break;
		case TowerSubLevel::RELEASE:
			position = GLISSIERE_RACK_LV3_POSITION_RIGHT;
			break;
		case TowerSubLevel::TAKE_BACK:
			position = GLISSIERE_RACK_LV3_POSITION_RIGHT_TAKE_BACK;
			break;
		default:
			// nothing to do
			break;
	}

	_stepper.move(position - this->_actual_position_steps);

	this->_actual_position_steps = position;

	return true;
}

bool Glissiere::moveToTowerLevel1()
{
	if(!this->_is_initialized)
		return false;

	_stepper.move(GLISSIERE_TOWER_LV1_POSITION - this->_actual_position_steps);

	this->_actual_position_steps = GLISSIERE_TOWER_LV1_POSITION;

	return true;
}

bool Glissiere::moveToTowerLevel2()
{
	if(!this->_is_initialized)
		return false;

	_stepper.move(GLISSIERE_TOWER_LV2_POSITION - this->_actual_position_steps);

	this->_actual_position_steps = GLISSIERE_TOWER_LV2_POSITION;

	return true;
}

bool Glissiere::moveToTowerLevel3()
{
	if(!this->_is_initialized)
		return false;

	_stepper.move(GLISSIERE_TOWER_LV3_POSITION - this->_actual_position_steps);

	this->_actual_position_steps = GLISSIERE_TOWER_LV3_POSITION;

	return true;
}

bool Glissiere::moveToTowerLevel4()
{
	if(!this->_is_initialized)
		return false;

	_stepper.move(GLISSIERE_TOWER_LV4_POSITION - this->_actual_position_steps);

	this->_actual_position_steps = GLISSIERE_TOWER_LV4_POSITION;

	return true;
}

bool Glissiere::moveToTowerLevel5()
{
	if(!this->_is_initialized)
		return false;

	_stepper.move(GLISSIERE_TOWER_LV5_POSITION - this->_actual_position_steps);

	this->_actual_position_steps = GLISSIERE_TOWER_LV5_POSITION;

	return true;
}


bool Glissiere::moveToArbitraryHeight(int height)
{
	if(!this->_is_initialized)
		return false;

	_stepper.move(height - this->_actual_position_steps);

	this->_actual_position_steps = height;

	return true;
}
