/*
 * UnloadOrdering.cpp
 *
 *  Created on: 20 avr. 2018
 *      Author: Anastaszor
 */

#include "UnloadOrdering.h"

RackEmplacement UnloadOrdering::getUnloadFirst()
{
	return this->_unload_first;
}

void UnloadOrdering::setUnloadFirst(RackEmplacement emplacement)
{
	this->_unload_first = emplacement;
	if(emplacement != RackEmplacement::Not_An_Emplacement)
	{
		this->_expected_points += 1;
	}
}

RackEmplacement UnloadOrdering::getUnloadSecond()
{
	return this->_unload_second;
}

void UnloadOrdering::setUnloadSecond(RackEmplacement emplacement)
{
	this->_unload_second = emplacement;
	if(emplacement != RackEmplacement::Not_An_Emplacement)
	{
		this->_expected_points += 2;
	}
}

RackEmplacement UnloadOrdering::getUnloadThird()
{
	return this->_unload_third;
}

void UnloadOrdering::setUnloadThird(RackEmplacement emplacement)
{
	this->_unload_third = emplacement;
	if(emplacement != RackEmplacement::Not_An_Emplacement)
	{
		this->_expected_points += 3;
	}
}

RackEmplacement UnloadOrdering::getUnloadFourth()
{
	return this->_unload_fourth;
}

void UnloadOrdering::setUnloadFourth(RackEmplacement emplacement)
{
	this->_unload_fourth = emplacement;
	if(emplacement != RackEmplacement::Not_An_Emplacement)
	{
		this->_expected_points += 4;
	}
}

RackEmplacement UnloadOrdering::getUnloadLast()
{
	return this->_unload_last;
}

void UnloadOrdering::setUnloadLast(RackEmplacement emplacement)
{
	this->_unload_last = emplacement;
	if(emplacement != RackEmplacement::Not_An_Emplacement)
	{
		this->_expected_points += 5;
	}
}

void UnloadOrdering::setUnloadOrderContainsCombination()
{
	if(this->_expected_points < 30)
	{
		this->_expected_points += 30;
	}
}

int UnloadOrdering::getExpectedPoints()
{
	return this->_expected_points;
}
