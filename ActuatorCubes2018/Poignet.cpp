/*
 * Poignet.cpp
 *
 *  Created on: 18 avr. 2018
 *      Author: Anastaszor
 */

#include <Servo.h>
#include <Arduino.h>
#include "Poignet.h"


bool Poignet::init()
{
	this->_servo_gauche = new Servo();
	this->_servo_centre = new Servo();
	this->_servo_droit = new Servo();

	pinMode(POIGNET_PIN_SERVO_GAUCHE, OUTPUT);
	pinMode(POIGNET_PIN_SERVO_CENTRE, OUTPUT);
	pinMode(POIGNET_PIN_SERVO_DROIT,  OUTPUT);

	this->_servo_gauche->attach(POIGNET_PIN_SERVO_GAUCHE);
	this->_servo_centre->attach(POIGNET_PIN_SERVO_CENTRE);
	this->_servo_droit->attach(POIGNET_PIN_SERVO_DROIT);

	this->rotateToMiddle();
	this->release();

	return true;
}

bool Poignet::rotateToLeftLoad()
{
	if(!this->_servo_gauche
		|| !this->_servo_centre || !this->_servo_droit)
		return false;

	this->_servo_centre->write(POIGNET_POSITION_MIDDLE + POIGNET_AMPLITUDE_LEFT_LOAD);

	delay(600);

	return true;
}

bool Poignet::rotateToLeftUnLoad()
{
	if(!this->_servo_gauche || !this->_servo_centre || !this->_servo_droit)
		return false;

	this->_servo_centre->write(POIGNET_POSITION_MIDDLE + POIGNET_AMPLITUDE_LEFT_UNLOAD);

	delay(600);

	return true;
}

bool Poignet::rotateToMiddleAndOpen()
{
	if(!this->_servo_gauche || !this->_servo_centre || !this->_servo_droit)
		return false;

	this->_servo_centre->write(POIGNET_POSITION_MIDDLE);
	this->_servo_gauche->write(POIGNET_SERVO_GAUCHE_OFFSET + POIGNET_TIGHTENING_OPEN_RELEASE);
	this->_servo_droit->write(POIGNET_SERVO_DROIT_OFFSET - POIGNET_TIGHTENING_OPEN_RELEASE);

	return true;
}

bool Poignet::rotateToMiddle()
{
	if(!this->_servo_gauche || !this->_servo_centre || !this->_servo_droit)
		return false;

	this->_servo_centre->write(POIGNET_POSITION_MIDDLE);

	delay(250);

	return true;
}

bool Poignet::rotateToRightUnLoad()
{
	if(!this->_servo_gauche || !this->_servo_centre || !this->_servo_droit)
		return false;

	this->_servo_centre->write(POIGNET_POSITION_MIDDLE - POIGNET_AMPLITUDE_RIGHT_UNLOAD);

	delay(600);

	return true;
}

bool Poignet::rotateToRightLoad()
{
	if(!this->_servo_gauche || !this->_servo_centre || !this->_servo_droit)
		return false;

	this->_servo_centre->write(POIGNET_POSITION_MIDDLE - POIGNET_AMPLITUDE_RIGHT_LOAD);

	delay(600);

	return true;
}

bool Poignet::hold()
{
	if(!this->_servo_gauche || !this->_servo_centre || !this->_servo_droit)
		return false;

	this->_servo_gauche->write(POIGNET_SERVO_GAUCHE_OFFSET + POIGNET_TIGHTENING_HOLD);
	this->_servo_droit->write(POIGNET_SERVO_DROIT_OFFSET - POIGNET_TIGHTENING_HOLD);

	delay(250);

	return true;
}

bool Poignet::modePelle()
{
	if(!this->_servo_gauche || !this->_servo_centre || !this->_servo_droit)
		return false;

	this->_servo_gauche->write(POIGNET_SERVO_GAUCHE_OFFSET + POIGNET_TIGHTENING_MODE_PELLE);
	this->_servo_droit->write(POIGNET_SERVO_DROIT_OFFSET - POIGNET_TIGHTENING_MODE_PELLE);

	delay(250);

	return true;
}






bool Poignet::release()
{
	if(!this->_servo_gauche || !this->_servo_centre || !this->_servo_droit)
		return false;

	this->_servo_gauche->write(POIGNET_SERVO_GAUCHE_OFFSET + POIGNET_TIGHTENING_RELEASE);
	this->_servo_droit->write(POIGNET_SERVO_DROIT_OFFSET - POIGNET_TIGHTENING_RELEASE);

	delay(100);

	return true;
}

bool Poignet::open()
{
	if(!this->_servo_gauche || !this->_servo_centre || !this->_servo_droit)
		return false;

	this->_servo_gauche->write(POIGNET_SERVO_GAUCHE_OFFSET + POIGNET_TIGHTENING_OPEN_RELEASE);
	this->_servo_droit->write(POIGNET_SERVO_DROIT_OFFSET - POIGNET_TIGHTENING_OPEN_RELEASE);

	delay(80);

	return true;
}

bool Poignet::takening()
{
	if(!this->_servo_gauche || !this->_servo_droit)
		return false;

	this->_servo_gauche->write(POIGNET_SERVO_GAUCHE_OFFSET + POIGNET_TIGHTENING_OPEN_TAKENING);
	this->_servo_droit->write(POIGNET_SERVO_DROIT_OFFSET - POIGNET_TIGHTENING_OPEN_TAKENING);

	delay(100);

	return true;
}


bool Poignet::folded()
{
	if(!this->_servo_gauche || !this->_servo_droit)
			return false;

	this->_servo_gauche->write(POIGNET_SERVO_GAUCHE_OFFSET + POIGNET_TIGHTENING_FOLDED);
	this->_servo_droit->write(POIGNET_SERVO_DROIT_OFFSET - POIGNET_TIGHTENING_FOLDED);

	return true;
}

bool Poignet::pushCube()
{
	if(!this->_servo_gauche || !this->_servo_droit)
			return false;

	this->_servo_gauche->write(POIGNET_SERVO_GAUCHE_OFFSET + POIGNET_TIGHTENING_PUSH_CUBE);
	this->_servo_droit->write(POIGNET_SERVO_DROIT_OFFSET - POIGNET_TIGHTENING_PUSH_CUBE);

	delay(500);

	return true;
}

bool Poignet::moveToArbitraryDirection(int direction)
{
	if(!this->_servo_centre)
		return false;

	this->_servo_centre->write(direction);

	delay(200);

	return true;
}

bool Poignet::openToArbitraryQuantity(int quantity)
{
	if(!this->_servo_gauche || !this->_servo_droit)
		return false;

	this->_servo_gauche->write(POIGNET_SERVO_GAUCHE_OFFSET + quantity);
	this->_servo_droit->write(POIGNET_SERVO_DROIT_OFFSET - quantity);

	return true;

}

bool Poignet::setToStartingPosition(MatchSide side)
{
	if(!this->_servo_centre || !this->_servo_gauche || !this->_servo_droit)
		return false;

	this->_servo_centre->write(POIGNET_POSITION_MIDDLE);
	this->_servo_gauche->write(POIGNET_SERVO_GAUCHE_OFFSET + POIGNET_TIGHTENING_COMPLETELY_OPEN);
	this->_servo_droit->write(POIGNET_SERVO_DROIT_OFFSET - POIGNET_TIGHTENING_COMPLETELY_OPEN);

	switch(side)
	{
		case MatchSide::Orange:
			this->_servo_centre->write(POIGNET_POSITION_MIDDLE + POIGNET_AMPLITUDE_LEFT_START_GAME);
			break;
		case MatchSide::Green:
			this->_servo_centre->write(POIGNET_POSITION_MIDDLE - POIGNET_AMPLITUDE_RIGHT_START_GAME);
			break;
		default:
			return false;
	}

	return true;
}
