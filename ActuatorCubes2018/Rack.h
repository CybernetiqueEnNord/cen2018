/*
 * Rack.h
 *
 *  Created on: 20 avr. 2018
 *      Author: Anastaszor
 */

#ifndef RACK_H_
#define RACK_H_

#include "Program.h"

/**
 * The rack represents the storage positions of the robot to place cubes.
 *
 * The level 1 is the bottomest and the level 3 the highest.
 * The left and right positions are determined naturally when you look at
 * the robot from its front.
 */
class Rack
{

private:

	/**
	 * The default strategy to unload without the exact color code.
	 */
	int _default_strategy = 1;

	/**
	 * The actual cube that is loaded on the level 1 left rack.
	 */
	CubeColor _level_1_left = CubeColor::Not_A_Cube;

	/**
	 * The actual cube that is loaded on the level 1 right rack.
	 */
	CubeColor _level_1_right = CubeColor::Not_A_Cube;

	/**
	 * The actual cube that is loaded on the level 2 left rack.
	 */
	CubeColor _level_2_left = CubeColor::Not_A_Cube;

	/**
	 * The actual cube that is loaded on the level 2 right rack.
	 */
	CubeColor _level_2_right = CubeColor::Not_A_Cube;

	/**
	 * The actual cube that is loaded on the level 3 left rack.
	 */
	CubeColor _level_3_left = CubeColor::Not_A_Cube;

	/**
	 * The actual cube that is loaded on the level 3 right rack.
	 */
	CubeColor _level_3_right = CubeColor::Not_A_Cube;

public:

	/**
	 * Returns true if the level 1 left rack is occupied by a cube.
	 */
	bool hasLevel1LeftOccupied();

	/**
	 * Returns true if the level 1 right rack is occupied by a cube.
	 */
	bool hasLevel1RightOccupied();

	/**
	 * Returns true if the level 2 left rack is occupied by a cube.
	 */
	bool hasLevel2LeftOccupied();

	/*
	 * Returns true if the level 2 right rack is occupied by a cube.
	 */
	bool hasLevel2RightOccupied();

	/**
	 * Returns true if the level 3 left rack is occupied by a cube.
	 */
	bool hasLevel3LeftOccupied();

	/**
	 * Returns true if the level 3 right rack is occupied by a cube.
	 */
	bool hasLevel3RightOccupied();

	/**
	 * Sets the strategy. Must be between 1 and 5, inclusive.
	 *
	 * Returns true if the strategy is acceptable, fallback on strategy 1 if
	 * 		strategy number is not acceptable.
	 */
	bool setStrategy(int strategy);

	/**
	 * Gets an emplacement that holds a cube of the given color or a
	 * compatible color (i.e. if there is no such color, the joker cube
	 * can do it).
	 */
	RackEmplacement getEmplacementForColor(CubeColor expected);

	/**
	 * Gets the next occupied emplacement in the rack. Retusn Not_An_Emplacement
	 * if there is no more emplacements available.
	 */
	RackEmplacement getNextOccupiedEmplacement();

	/**
	 * Gets the next free emplacement in the rack. Returns Not_An_Emplacement
	 * if there is no more emplacements available.
	 */
	RackEmplacement getNextFreeEmplacement();

	/**
	 * Stores the given cube at an arbitrary emplacement
	 */
	void loadEmplacement(RackEmplacement emplacement, CubeColor cube);

	/**
	 * Stores the given cube on level 1 left rack.
	 */
	void loadLevel1Left(CubeColor cube);

	/**
	 * Stores the given cube on level 1 right rack.
	 */
	void loadLevel1Right(CubeColor cube);

	/**
	 * Stores the given cube on level 2 left rack.
	 */
	void loadLevel2Left(CubeColor cube);

	/**
	 * Stores the given cube on level 2 right rack.
	 */
	void loadLevel2Right(CubeColor cube);

	/**
	 * Stores the given cube on level 3 left rack.
	 */
	void loadLevel3Left(CubeColor cube);

	/**
	 * Stores the given cube on level 3 right rack.
	 */
	void loadLevel3Right(CubeColor cube);

	/**
	 * Removes the cube at the given emplacement
	 */
	CubeColor unloadEmplacement(RackEmplacement emplacement);

	/**
	 * Removes the cube on level 1 left rack.
	 */
	CubeColor unloadLevel1Left();

	/**
	 * Removes the cube on level 1 right rack.
	 */
	CubeColor unloadLevel1Right();

	/**
	 * Removes the cube on level 2 left rack.
	 */
	CubeColor unloadLevel2Left();

	/**
	 * Removes the cube on level 2 right rack.
	 */
	CubeColor unloadLevel2Right();

	/**
	 * Removes the cube on level 3 left rack.
	 */
	CubeColor unloadLevel3Left();

	/**
	 * Removes the cube on level 3 right rack.
	 */
	CubeColor unloadLevel3Right();

};

#endif /* RACK_H_ */
