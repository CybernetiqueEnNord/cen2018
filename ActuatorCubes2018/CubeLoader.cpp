/*
 * CubeLoader.cpp
 *
 *  Created on: 20 avr. 2018
 *      Author: Aeon
 */

#include "CubeLoader.h"
#include "Rack.h"
#include "UnloadOrdering.h"

bool CubeLoader::loadCubeByColor(CubeColor color)
{
	if(color == CubeColor::Not_A_Cube)
		return false;

	RackEmplacement emplacement = this->_rack->getNextFreeEmplacement();
	if(emplacement == RackEmplacement::Not_An_Emplacement)
		return false;

	if(!this->_is_prepared_to_load)
		if(!this->prepareToLoad())
			return false;

	this->_is_prepared_to_load = false;
	this->_poignet->hold();
	switch(emplacement)
	{
		case RackEmplacement::Level1Left:
			this->_glissiere->moveToRackLevel1Left(TowerSubLevel::RELEASE);
			this->_poignet->rotateToLeftLoad();
			this->_poignet->release();
			this->_glissiere->moveToRackLevel1Left(TowerSubLevel::ON_TOP);
			this->_poignet->folded();
			this->_poignet->rotateToMiddle();
			this->_glissiere->moveToRackLevel1Left(TowerSubLevel::PUSH);
			this->_poignet->pushCube();
			this->_poignet->release();
			this->_rack->loadLevel1Left(color);
			return true;
			break;
		case RackEmplacement::Level1Right:
			this->_glissiere->moveToRackLevel1Right(TowerSubLevel::RELEASE);
			this->_poignet->rotateToRightLoad();
			this->_poignet->release();
			this->_glissiere->moveToRackLevel1Right(TowerSubLevel::ON_TOP);
			this->_poignet->folded();
			this->_poignet->rotateToMiddle();
			this->_glissiere->moveToRackLevel1Right(TowerSubLevel::PUSH);
			this->_poignet->pushCube();
			this->_poignet->release();
			this->_rack->loadLevel1Right(color);
			return true;
			break;
		case RackEmplacement::Level2Left:
			this->_glissiere->moveToRackLevel2Left(TowerSubLevel::RELEASE);
			this->_poignet->rotateToLeftLoad();
			this->_poignet->release();
			this->_glissiere->moveToRackLevel2Left(TowerSubLevel::ON_TOP);
			this->_poignet->folded();
			this->_poignet->rotateToMiddle();
			this->_glissiere->moveToRackLevel2Left(TowerSubLevel::PUSH);
			this->_poignet->pushCube();
			this->_poignet->release();
			this->_rack->loadLevel2Left(color);
			return true;
			break;
		case RackEmplacement::Level2Right:
			this->_glissiere->moveToRackLevel2Right(TowerSubLevel::RELEASE);
			this->_poignet->rotateToRightLoad();
			this->_poignet->release();
			this->_glissiere->moveToRackLevel2Right(TowerSubLevel::ON_TOP);
			this->_poignet->folded();
			this->_poignet->rotateToMiddle();
			this->_glissiere->moveToRackLevel2Right(TowerSubLevel::PUSH);
			this->_poignet->pushCube();
			this->_poignet->release();
			this->_rack->loadLevel2Right(color);
			return true;
			break;
		case RackEmplacement::Level3Left:
			this->_glissiere->moveToRackLevel3Left(TowerSubLevel::RELEASE);
			this->_poignet->rotateToLeftLoad();
			this->_poignet->release();
			this->_glissiere->moveToRackLevel3Left(TowerSubLevel::ON_TOP);
			this->_poignet->folded();
			this->_poignet->rotateToMiddle();
			this->_glissiere->moveToRackLevel3Left(TowerSubLevel::PUSH);
			this->_poignet->pushCube();
			this->_poignet->release();
			this->_rack->loadLevel3Left(color);
			return true;
			break;
		case RackEmplacement::Level3Right:
			this->_glissiere->moveToRackLevel3Right(TowerSubLevel::RELEASE);
			this->_poignet->rotateToRightLoad();
			this->_poignet->release();
			this->_glissiere->moveToRackLevel3Right(TowerSubLevel::ON_TOP);
			this->_poignet->folded();
			this->_poignet->rotateToMiddle();
			this->_glissiere->moveToRackLevel3Right(TowerSubLevel::PUSH);
			this->_poignet->pushCube();
			this->_poignet->release();
			this->_rack->loadLevel3Right(color);
			return true;
			break;
		case RackEmplacement::Not_An_Emplacement:
			// nothing to do
			break;
	}

	return false;
}

CubeColor CubeLoader::resolveColor(Orientation orientation, CubeLocation cube_location, TakingPosition taking_position)
{
	CubePositionning* cp = this->resolveCubePositionning(cube_location, orientation);
	switch(taking_position)
	{
		case TakingPosition::FrontCenter:
			return cp->getNearestCube();
		case TakingPosition:: MiddleLeft:
			return cp->getLeftmostCube();
		case TakingPosition::MiddleCenter:
			return cp->getCenteredCube();
		case TakingPosition::MiddleRight:
			return cp->getRightmostCube();
		case TakingPosition::AwayCenter:
			return cp->getFarerCube();
	}
	return CubeColor::Not_A_Cube;
}

CubePositionning* CubeLoader::resolveCubePositionning(
	CubeLocation nearest_side_of_the_cube_set,
	Orientation from_where_the_robot_attacks_the_set
) {
	CubePositionning* cp = new CubePositionning();

	cp->setCenteredCube(CubeColor::Yellow);

	switch(from_where_the_robot_attacks_the_set)
	{
		case Orientation::North:
			cp->setNearestCube(CubeColor::Black);
			cp->setFarerCube(CubeColor::Blue);
			switch(nearest_side_of_the_cube_set)
			{
				case CubeLocation::NearestOrange:
					cp->setLeftmostCube(CubeColor::Orange);
					cp->setRightmostCube(CubeColor::Green);
					break;
				case CubeLocation::NearestGreen:
					cp->setLeftmostCube(CubeColor::Green);
					cp->setRightmostCube(CubeColor::Orange);
					break;
			}
			break;

		case Orientation::South:
			cp->setNearestCube(CubeColor::Blue);
			cp->setFarerCube(CubeColor::Black);
			switch(nearest_side_of_the_cube_set)
			{
				case CubeLocation::NearestOrange:
					cp->setLeftmostCube(CubeColor::Green);
					cp->setRightmostCube(CubeColor::Orange);
					break;
				case CubeLocation::NearestGreen:
					cp->setLeftmostCube(CubeColor::Orange);
					cp->setRightmostCube(CubeColor::Green);
					break;
			}
			break;

		case Orientation::East:
			switch(nearest_side_of_the_cube_set)
			{
				case CubeLocation::NearestOrange:
					cp->setNearestCube(CubeColor::Green);
					cp->setFarerCube(CubeColor::Orange);
					break;
				case CubeLocation::NearestGreen:
					cp->setNearestCube(CubeColor::Orange);
					cp->setFarerCube(CubeColor::Green);
					break;
			}
			cp->setLeftmostCube(CubeColor::Blue);
			cp->setRightmostCube(CubeColor::Black);
			break;

		case Orientation::West:
			switch(nearest_side_of_the_cube_set)
			{
				case CubeLocation::NearestOrange:
					cp->setNearestCube(CubeColor::Orange);
					cp->setFarerCube(CubeColor::Green);
					break;
				case CubeLocation::NearestGreen:
					cp->setNearestCube(CubeColor::Green);
					cp->setFarerCube(CubeColor::Orange);
					break;
			}
			cp->setLeftmostCube(CubeColor::Black);
			cp->setRightmostCube(CubeColor::Blue);
			break;
	}

	return cp;
}

void CubeLoader::moveToTowerLevel(TowerLevel stage)
{
	switch(stage)
	{
		case TowerLevel::Level1:
			this->_glissiere->moveToTowerLevel1();
			break;
		case TowerLevel::Level2:
			this->_glissiere->moveToTowerLevel2();
			break;
		case TowerLevel::Level3:
			this->_glissiere->moveToTowerLevel3();
			break;
		case TowerLevel::Level4:
			this->_glissiere->moveToTowerLevel4();
			break;
		case TowerLevel::Level5:
			this->_glissiere->moveToTowerLevel5();
			break;
	}
}

CubeColor CubeLoader::unloadEmplacement(RackEmplacement emplacement, TowerLevel stage)
{
	if(emplacement == RackEmplacement::Not_An_Emplacement)
		return CubeColor::Not_A_Cube;

	this->_poignet->rotateToMiddle();
	this->_poignet->release();
	this->_poignet->hold();
	CubeColor color = this->_rack->unloadEmplacement(emplacement);
	switch(emplacement)
	{
		case RackEmplacement::Level1Left:
			this->_glissiere->moveToRackLevel1Left(TowerSubLevel::ON_TOP);
			this->_poignet->rotateToLeftUnLoad();
			this->_poignet->takening();
			this->_glissiere->moveToRackLevel1Left(TowerSubLevel::TAKE_BACK);
			this->_poignet->hold();
			this->_glissiere->moveToRackLevel1Left(TowerSubLevel::RELEASE);
			this->_poignet->rotateToMiddle();
			this->moveToTowerLevel(stage);
			return color;
			break;
		case RackEmplacement::Level1Right:
			this->_glissiere->moveToRackLevel1Right(TowerSubLevel::ON_TOP);
			this->_poignet->rotateToRightUnLoad();
			this->_poignet->takening();
			this->_glissiere->moveToRackLevel1Right(TowerSubLevel::TAKE_BACK);
			this->_poignet->hold();
			this->_glissiere->moveToRackLevel1Right(TowerSubLevel::RELEASE);
			this->_poignet->rotateToMiddle();
			this->moveToTowerLevel(stage);
			return color;
			break;
		case RackEmplacement::Level2Left:
			this->_glissiere->moveToRackLevel2Left(TowerSubLevel::ON_TOP);
			this->_poignet->rotateToLeftUnLoad();
			this->_poignet->takening();
			this->_glissiere->moveToRackLevel2Left(TowerSubLevel::TAKE_BACK);
			this->_poignet->hold();
			this->_glissiere->moveToRackLevel2Left(TowerSubLevel::RELEASE);
			this->_poignet->rotateToMiddle();
			this->moveToTowerLevel(stage);
			return color;
			break;
		case RackEmplacement::Level2Right:
			this->_glissiere->moveToRackLevel2Right(TowerSubLevel::ON_TOP);
			this->_poignet->rotateToRightUnLoad();
			this->_poignet->takening();
			this->_glissiere->moveToRackLevel2Right(TowerSubLevel::TAKE_BACK);
			this->_poignet->hold();
			this->_glissiere->moveToRackLevel2Right(TowerSubLevel::RELEASE);
			this->_poignet->rotateToMiddle();
			this->moveToTowerLevel(stage);
			return color;
			break;
		case RackEmplacement::Level3Left:
			this->_glissiere->moveToRackLevel3Left(TowerSubLevel::ON_TOP);
			this->_poignet->rotateToLeftUnLoad();
			this->_poignet->takening();
			this->_glissiere->moveToRackLevel3Left(TowerSubLevel::TAKE_BACK);
			this->_poignet->hold();
			this->_glissiere->moveToRackLevel3Left(TowerSubLevel::RELEASE);
			this->_poignet->rotateToMiddle();
			this->moveToTowerLevel(stage);
			return color;
			break;
		case RackEmplacement::Level3Right:
			this->_glissiere->moveToRackLevel3Right(TowerSubLevel::ON_TOP);
			this->_poignet->rotateToRightUnLoad();
			this->_poignet->takening();
			this->_glissiere->moveToRackLevel3Right(TowerSubLevel::TAKE_BACK);
			this->_poignet->hold();
			this->_glissiere->moveToRackLevel3Right(TowerSubLevel::RELEASE);
			this->_poignet->rotateToMiddle();
			this->moveToTowerLevel(stage);
			return color;
			break;
		case RackEmplacement::Not_An_Emplacement:
			// nothing to do
			break;
	}

	return CubeColor::Not_A_Cube;
}

void CubeLoader::setPoignet(Poignet* poignet)
{
	this->_poignet = poignet;
}

void CubeLoader::setGlissiere(Glissiere* glissiere)
{
	this->_glissiere = glissiere;
}

void CubeLoader::setCubeCombination(CubeCombination* cube_combination)
{
	this->_cube_combination = cube_combination;
}

bool CubeLoader::setDefaultStrategy(int strategy)
{
	if(!this->_rack)
		return false;

	return this->_rack->setStrategy(strategy);
}

bool CubeLoader::init()
{
	if(!this->_poignet || !this->_glissiere)
		return false;

	this->_rack = new Rack();
	this->_is_initialized = true;
	this->_end_match = false;

	return true;
}

bool CubeLoader::instantLoad()
{
	if(!this->_is_initialized)
		return false;
	if(this->_end_match)
		return false;

	RackEmplacement emplacement = RackEmplacement::Not_An_Emplacement;
	while((emplacement = this->_rack->getNextFreeEmplacement()) != RackEmplacement::Not_An_Emplacement)
	{
		this->_rack->loadEmplacement(emplacement, CubeColor::White);
	}
	return true;
}

bool CubeLoader::setInitialPositionRetractedAndColorDependant(MatchSide side)
{
	if(this->_end_match)
		return false;

	if(!this->_poignet->setToStartingPosition(side))
		return false;

	return true;
}

bool CubeLoader::releaseCube()
{
	if(!this->_is_initialized)
		return false;
	if(this->_end_match)
		return false;

	return this->_poignet->open();
}

bool CubeLoader::holdCube()
{
	if(!this->_is_initialized)
		return false;
	if(this->_end_match)
		return false;

	return this->_poignet->hold();
}

bool CubeLoader::modePelle()
{
	if(!this->_is_initialized)
		return false;
	if(this->_end_match)
		return false;

	this->prepareToLoad();

	return this->_poignet->modePelle();
}



bool CubeLoader::prepareToLoad()
{
	if(!this->_is_initialized)
		return false;
	if(this->_end_match)
		return false;
	if(!this->_poignet->rotateToMiddleAndOpen())
		return false;
	if(!this->_glissiere->moveToTheBottom())
		return false;

	this->_is_prepared_to_load = true;

	return true;
}

bool CubeLoader::loadInitialCube()
{
	if(!this->_is_initialized)
		return false;
	if(this->_end_match)
		return false;

	this->_poignet->hold();
	this->_glissiere->moveToRackLevel3Left(TowerSubLevel::RELEASE);
	this->_poignet->rotateToLeftUnLoad();
	this->_glissiere->moveToRackLevel3Left(TowerSubLevel::JUST_STABILYZE_JOCKER_TOWER);

	return true;
}

bool CubeLoader::unloadInitialCube()
{
	if(!this->_is_initialized)
		return false;
	if(this->_end_match)
		return false;

	this->_poignet->rotateToMiddle();
	this->moveToTowerLevel(TowerLevel::Level1);
//	this->_poignet->open();	// will be handled with new order open

	return true;
}


bool CubeLoader::loadCube(Orientation orientation, CubeLocation cube_location, TakingPosition taking_position)
{
	if(!this->_is_initialized)
		return false;
	if(this->_end_match)
		return false;

	return this->loadCubeByColor(this->resolveColor(orientation, cube_location, taking_position));
}

bool CubeLoader::loadCubeAndHold(TowerLevel level)
{
	if(!this->_is_initialized)
		return false;
	if(this->_end_match)
		return false;

	if(!this->_is_prepared_to_load)
		if(!this->prepareToLoad())
			return false;

	this->_is_prepared_to_load = false;
	this->_poignet->hold();
	this->moveToTowerLevel(level);

	return true;
}

void CubeLoader::prepareUnload()
{
	if(this->_end_match)
		return;
	// if there is no cube combination, just use the no-op cube combination
	// by default and not fail
	// this will have all the cubes unloaded as asked but the ordering will
	// be something like random and no points can be accounted for sequence
	if(!this->_cube_combination)
	{
		this->_cube_combination = new CubeCombination();
	}
	this->_unload_ordering = this->_cube_combination->getUnloadOrderingFrom(this->_rack);
}

CubeColor CubeLoader::unloadCubeAtLevel(TowerLevel level)
{
	if(!this->_unload_ordering)
		this->prepareUnload();
	if(this->_end_match)
		return CubeColor::Not_A_Cube;

	RackEmplacement emplacement = RackEmplacement::Not_An_Emplacement;
	switch(level)
	{
		case TowerLevel::Level1:
			emplacement = this->_unload_ordering->getUnloadFirst();
			break;
		case TowerLevel::Level2:
			emplacement = this->_unload_ordering->getUnloadSecond();
			break;
		case TowerLevel::Level3:
			emplacement = this->_unload_ordering->getUnloadThird();
			break;
		case TowerLevel::Level4:
			emplacement = this->_unload_ordering->getUnloadFourth();
			break;
		case TowerLevel::Level5:
			emplacement = this->_unload_ordering->getUnloadLast();
			break;
	}

	return this->unloadEmplacement(emplacement, level);
}

int CubeLoader::unloadTower()
{
	if(!this->_is_initialized)
		return 0;
	if(this->_end_match)
		return 0;

	this->prepareUnload();

	this->unloadCubeAtLevel(TowerLevel::Level1);
	this->unloadCubeAtLevel(TowerLevel::Level2);
	this->unloadCubeAtLevel(TowerLevel::Level3);
	this->unloadCubeAtLevel(TowerLevel::Level4);
	this->unloadCubeAtLevel(TowerLevel::Level5);

	return this->_unload_ordering->getExpectedPoints();
}

bool CubeLoader::unloadTowerAsIs()
{
	if(this->_end_match)
		return false;

	this->_poignet->open();
	return true;
}

bool CubeLoader::setArbitraryHeight(int height)
{
	if(this->_end_match)
		return false;

	return this->_glissiere->moveToArbitraryHeight(height);
}

bool CubeLoader::setArbitraryDirection(int direction)
{
	if(this->_end_match)
		return false;

	return this->_poignet->moveToArbitraryDirection(direction);
}

bool CubeLoader::openToArbitraryQuantity(int quantity)
{
	if(this->_end_match)
		return false;

	return this->_poignet->openToArbitraryQuantity(quantity);
}

void CubeLoader::handleEndMatch()
{
	this->_end_match = true;
}
