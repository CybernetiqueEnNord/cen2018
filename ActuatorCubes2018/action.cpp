#include <Servo.h>
#include "action.h"

/* @deprecated */
A4988 stepper(MOTOR_STEPS, DIR, STEP, ENABLE, MS1, MS2, MS3);

/* @deprecated */
Servo servoGauche;
/* @deprecated */
Servo servoDroit;
/* @deprecated */
Servo servoPoignet;

/* @deprecated */
void open() {
  int serrage = 40;
  servoGauche.write(servoGaucheOffset + serrage);
  servoDroit.write(servoDroitOffset - serrage);
}

/* @deprecated */
void positionPelle()
{
  int serrage = 0;
  servoGauche.write(servoGaucheOffset + serrage);
  servoDroit.write(servoDroitOffset - serrage);
}

/* @deprecated */
void close() {
  int serrage = 62;
  servoGauche.write(servoGaucheOffset + serrage);
  servoDroit.write(servoDroitOffset - serrage);
}

/* @deprecated */
void centerPoignet(){
  servoPoignet.write(90);
}

/* @deprecated */
void droitePoignet(){
  servoPoignet.write(5);
}

/* @deprecated */
void gauchePoignet(){
  servoPoignet.write(175);
}

void homeSearch(){

    stepper.begin(RPM_LOW, MICROSTEPS);
    stepper.setSpeedProfile(stepper.LINEAR_SPEED, MOTOR_ACCEL, MOTOR_DECEL);

    stepper.startMove(-50000);

    unsigned long startTime = millis();
    while (digitalRead(PIN_LIMIT_SWITCH_LOW) != 1 && (millis() - startTime) < 15000) {
    stepper.nextAction();
   }

   stepper.stop();

   stepper.begin(RPM_HIGH, MICROSTEPS);
   stepper.setSpeedProfile(stepper.LINEAR_SPEED, MOTOR_ACCEL, MOTOR_DECEL);

   stepper.rotate(360);
   stepper.rotate(-360);
}



void setupActionneurCubes() {
  // Sets the two pins as Outputs


  pinMode(PIN_SERVO_GAUCHE, OUTPUT);
  pinMode(PIN_SERVO_DROIT, OUTPUT);
  pinMode(PIN_SERVO_POIGNET, OUTPUT);

  servoGauche.attach(PIN_SERVO_GAUCHE);
  servoDroit.attach(PIN_SERVO_DROIT);
  servoPoignet.attach(PIN_SERVO_POIGNET);

  // make the pushbutton's pin an input:
  pinMode(PIN_LIMIT_SWITCH_LOW, INPUT);
  pinMode(PIN_SIGNAL_NUCLEO, INPUT_PULLUP);



  //stepper.setSpeedProfile(stepper.LINEAR_SPEED, MOTOR_ACCEL, MOTOR_DECEL);
}

void miseEnConditionDepart()
{
  close();
  centerPoignet();
  delay(1000);
  homeSearch();
  open();
}

void chargerCube()
{
  close();
  delay(50);
  stepper.rotate(1200);
  //delay(50);
  droitePoignet();
  delay(500);
}

void loop_old() {
  while (digitalRead(PIN_SIGNAL_NUCLEO) == 1) {
    delay(1);
  }
  centerPoignet();
  stepper.rotate(-610);
  positionPelle();
  while(digitalRead(PIN_SIGNAL_NUCLEO) == 0) {
    delay(1);
  }
  homeSearch();


}

