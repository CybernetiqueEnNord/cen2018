/*
 * Poignet.h
 *
 *  Created on: 18 avr. 2018
 *      Author: Anastaszor
 */

#ifndef POIGNET_H_
#define POIGNET_H_

#include <Servo.h>
#include "Constants.h"
#include "Program.h"

/**
 * This class represents the assembly of three servos that add mobility to
 * take the cubes and place them on the racks (etageres).
 *
 * The poignet resembles this schema (view from the top to bottom) :
 *
 *
 *                   FRONT OF THE ROBOT
 *
 *        <<<<<<= | <<<<<=  OPEN =>>>>> | =>>>>>>
 *
 *                | <==   RELEASE   ==> |
 *
 *          ==>   |         HOLD        |   <==
 *
 *               /-E                   3-\
 *         LEFT  | E                   3 | RIGHT
 *          ARM  | E                   3 | ARM
 *               | |                   | |
 *               | |                   | |
 *           +---| |----+----------+---| |----+
 *           |   /-\    |          |   /-\    |
 *           |   \-/    |          |   \-/    |
 * MOVE <=== |          |          |          |  ===> MOVE TO RIGHT
 * TO LEFT   |          |          |          |
 *           |          |          |          |
 *           |  LEFT    |  CENTER  |  RIGHT   |
 *           |  SERVO   |  SERVO   |  SERVO   |
 *           |          |          |          |
 *           |          |          |          |
 *           |          |          |          |
 *           |          |          |          |
 *           |          |   /-\    |          |
 *           |          |   \-/    |          |
 *           +----------+---| |----+----------+
 *                          | |
 *                          | |
 *                     ==>> | | <<==  MOVE TO CENTER
 *                          | |
 *                     TO THE ARMATURE
 *                      OF THE ROBOT
 *
 *
 *                     REAR OF THE ROBOT
 *
 */
class Poignet
{

private:

	/**
	 * The left servo that composes the poignet.
	 */
	Servo* _servo_gauche;

	/**
	 * The middle servo that composes the poignet.
	 */
	Servo* _servo_centre;

	/**
	 * The left servo that composes the poignet.
	 */
	Servo* _servo_droit;


public:

	/**
	 * Initializes the poignet by telling the arduino api which wiring
	 * to take account for.
	 *
	 * This method returns true if the wiring is successful, and false
	 * if it fails.
	 */
	bool init();

	/**
	 * Rotates the whole poignet to the left position. This position is made
	 * to put cubes on a rack on the left side of the robot, or to take a cube
	 * from a specific rack.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool rotateToLeft();
	bool rotateToLeftLoad();

	/**
	 * Rotates the whole poignet to the left position. This position is made
	 * to put cubes on a rack on the left side of the robot, or to take a cube
	 * from a specific rack.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool rotateToLeftUnLoad();


	/**
	 * Both method concatenated. Attention, non blocking on purpose.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool rotateToMiddleAndOpen();

	/**
	 * Rotates the whole poignet to the middle position. This position is made
	 * to takes cubes on the floor, or to put cubes on a tower.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool rotateToMiddle();

	/**
	 * Rotates the whole poignet to the right position. This position is made
	 * to put cubes on a rack on the right side of the robot, or to take a cube
	 * from a specific rack.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool rotateToRight();
	bool rotateToRightLoad();


	/**
	 * Rotates the whole poignet to the right position. This position is made
	 * to put cubes on a rack on the right side of the robot, or to take a cube
	 * from a specific rack.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool rotateToRightUnLoad();

	/**
	 * Holds something on the middle in the left and the right servos.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool hold();


	/**
	 * Set the mode pelle.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool modePelle();

	/**
	 * Releases the something that is hold on the middle of the servos.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool release();

	/**
	 * Releases the something that is hold and go puts the arms wide open.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool open();





	bool takening();
	bool folded();
	bool pushCube();

	/**
	 * DEBUG function. This sets the poignet to an arbitrary direction.
	 *
	 * Returns true if the move is a success, false else.
	 */
	bool moveToArbitraryDirection(int direction);

	/**
	 * DEBUG function. This Releases/Hold the something that is hold on the middle of the servos.
	 *
	 * Returns true if the move is a success, false else.
	 */
	bool openToArbitraryQuantity(int quantity);

	/**
	 * Set the poignet to show the selected side, and open the pince to the maximum.
	 */
	bool setToStartingPosition(MatchSide side);

};

#endif /* POIGNET_H_ */
