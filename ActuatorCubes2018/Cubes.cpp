#include "Cubes.h"
#include "Constants.h"

#include "Glissiere.h"

PROGMEM const char * const Program::versionString = "vCubes " __DATE__ " - " __TIME__;

Cubes::Cubes(uint8_t address, SoftWire& master) :
		Program(), address(address), master(master) {
}

uint8_t Cubes::slaveParametersCount = 0;

uint8_t Cubes::slaveParameters[8];

uint8_t Cubes::slaveCommandResult = 0;

void Cubes::initializeSlaveI2C() {
	slave.begin(address);
	slave.onRequest(i2cHandleSlaveRequest);
	slave.onReceive(i2cHandleSlaveInput);
}

void Cubes::initializeMasterI2C() {
	master.begin();
}

void Cubes::runInitialization() {
	Program::runInitialization();
	initializeSlaveI2C();
	//initializeMasterI2C();

	Poignet* p = new Poignet();
	p->init();

	Glissiere* g = new Glissiere();
	g->init();

	this->_cubeLoader = new CubeLoader();
	this->_cubeLoader->setGlissiere(g);
	this->_cubeLoader->setPoignet(p);
	if (this->_cubeLoader->init())
		Serial.write("\nIntialisation done.\n");
	else
		Serial.write("\nInitialization failed.\n");
}

void Cubes::runStart() {
	Program::runStart();
}

void Cubes::runMain() {
	Program::runMain();
	handleAction();
}

void Cubes::runDebug() {
	Program::runDebug();
}

void Cubes::i2cHandleSlaveInput(int bytesCount) {
	unsigned int i = 0;
	while (slave.available() > 0 && i < min(ARRAY_SIZE(slaveParameters), bytesCount)) {
		char c = slave.read();
		slaveParameters[i++] = c;
	}
	slaveParametersCount = i;
}

void Cubes::i2cHandleSlaveRequest() {
	if (slaveParametersCount != 0) {
		i2cHandleCommand();
		slaveParametersCount = 0;
	}
}

bool Cubes::isBusy() const {
	return _command != Action::None;
}

void Cubes::i2cSendStatus() {
	slave.write(_command == Action::None ? I2C_STATUS_READY : I2C_STATUS_BUSY);
}

void Cubes::i2cHandleCommand() {
	if (slaveParametersCount == 0) {
		cubes.i2cSendStatus();
	} else if (slaveParameters[0] == I2C_COMMAND_STATUS) {
		cubes.i2cSendStatus();
	} else {
		if (cubes.isBusy()) {
			return;
		}
		switch (slaveParameters[0]) {
			case I2C_COMMAND_KILL:
				cubes._cubeLoader->handleEndMatch();
				break;
			case I2C_COMMAND_SET_COLOR_GREEN:
				cubes._command = Action::SetSideGreen;
				break;
			case I2C_COMMAND_SET_COLOR_ORANGE:
				cubes._command = Action::SetSideOrange;
				break;
			case I2C_COMMAND_SET_INITIAL_POSITION:
				cubes._command = Action::SetInitialPosition;
				break;
			case I2C_COMMAND_SET_STRATEGY_1:
				cubes._command = Action::SetStrategy1;
				break;
			case I2C_COMMAND_SET_STRATEGY_2:
				cubes._command = Action::SetStrategy2;
				break;
			case I2C_COMMAND_SET_STRATEGY_3:
				cubes._command = Action::SetStrategy3;
				break;
			case I2C_COMMAND_SET_STRATEGY_4:
				cubes._command = Action::SetStrategy4;
				break;
			case I2C_COMMAND_SET_ORIENTATION_NORTH:
				cubes._command = Action::SetOrientationNorth;
				break;
			case I2C_COMMAND_SET_ORIENTATION_EAST:
				cubes._command = Action::SetOrientationEast;
				break;
			case I2C_COMMAND_SET_ORIENTATION_SOUTH:
				cubes._command = Action::SetOrientationSouth;
				break;
			case I2C_COMMAND_SET_ORIENTATION_WEST:
				cubes._command = Action::SetOrientationWest;
				break;
			case I2C_COMMAND_HANDLE_LOAD_JOCKER:
				cubes._command = Action::Load_Jocker;
				break;
			case I2C_COMMAND_HANDLE_UNLOAD_JOCKER:
				cubes._command = Action::Unload_Jocker;
				break;
			case I2C_COMMAND_HANDLE_LOAD_PREPARE:
				cubes._command = Action::Load_Prepare;
				break;
			case I2C_COMMAND_HANDLE_LOAD_1:
				cubes._command = Action::Load_Lv1;
				break;
			case I2C_COMMAND_HANDLE_LOAD_2:
				cubes._command = Action::Load_Lv2;
				break;
			case I2C_COMMAND_HANDLE_LOAD_3:
				cubes._command = Action::Load_Lv3;
				break;
			case I2C_COMMAND_HANDLE_LOAD_4:
				cubes._command = Action::Load_Lv4;
				break;
			case I2C_COMMAND_HANDLE_LOAD_5:
				cubes._command = Action::Load_Lv5;
				break;
			case I2C_COMMAND_HANDLE_RESET_NEW_TOWER:
				cubes._command = Action::ResetNewTower;
				break;
			case I2C_COMMAND_HANDLE_PREPARE_UNLOAD_1:
				cubes._command = Action::Prepare_Unload_Lv1;
				break;
			case I2C_COMMAND_HANDLE_PREPARE_UNLOAD_2:
				cubes._command = Action::Prepare_Unload_Lv2;
				break;
			case I2C_COMMAND_HANDLE_PREPARE_UNLOAD_3:
				cubes._command = Action::Prepare_Unload_Lv3;
				break;
			case I2C_COMMAND_HANDLE_PREPARE_UNLOAD_4:
				cubes._command = Action::Prepare_Unload_Lv4;
				break;
			case I2C_COMMAND_HANDLE_PREPARE_UNLOAD_5:
				cubes._command = Action::Prepare_Unload_Lv5;
				break;
			case I2C_COMMAND_HANDLE_OPEN:
				cubes._command = Action::Open;
				break;
			case I2C_COMMAND_HANDLE_CLOSE:
				cubes._command = Action::Close;
				break;
			case I2C_COMMAND_MODE_PELLE:
				cubes._command = Action::ModePelle;
				break;
			case I2C_COMMAND_READ_COMBINATION:
				cubes._command = Action::ReadCombination;
				break;
//				case SLAVE_READ_NB_POINTS:
//					//Cubes::slaveCommandResult = thizs->_currentPointsAcquired;
//					break;
		}
	}
}

void Cubes::enterDebug() {
	Program::enterDebug();
}

void Cubes::i2cScanMasterBus() {
	Serial.println(F(":scanning I2C bus"));
	for (uint8_t address = 1; address < 127; address++) {
		// The i2c_scanner uses the return value of
		// the Write.endTransmisstion to see if
		// a device did acknowledge to the address.
		master.beginTransmission(address);
		uint8_t error = master.endTransmission();

		if (error == 0) {
			Serial.print('D');
			serialPrintHex(address);
		}
	}
	serialPrintSuccess();
}

void Cubes::testBonFonctionnement() {
	Serial.println(F("Ca fonctionne !"));

}

void Cubes::handleAction() {
	if (_command == Action::None) {
		return;
	}

	Serial.print(F("Handling command: "));
	Serial.println((int) _command);
	switch (_command) {
		case Action::SetSideGreen:
			_matchSide = MatchSide::Green;
			this->_currentLocation = CubeLocation::NearestGreen;    //TODO Handle setting to be able to get oposite cubes
			Serial.println(F("GREEN"));
			break;

		case Action::SetSideOrange:
			_matchSide = MatchSide::Orange;
			this->_currentLocation = CubeLocation::NearestOrange;    //TODO Handle setting to be able to get oposite cubes
			Serial.println(F("ORANGE"));
			break;

		case Action::SetInitialPosition:
			this->_cubeLoader->prepareToLoad();
			this->_cubeLoader->setInitialPositionRetractedAndColorDependant(_matchSide);
			break;

		case Action::SetStrategy1:
			this->_cubeLoader->setDefaultStrategy(1);
			break;

		case Action::SetStrategy2:
			this->_cubeLoader->setDefaultStrategy(2);
			break;

		case Action::SetStrategy3:
			this->_cubeLoader->setDefaultStrategy(3);
			break;

		case Action::SetStrategy4:
			this->_cubeLoader->setDefaultStrategy(4);
			break;

		case Action::SetStrategy5:
			this->_cubeLoader->setDefaultStrategy(5);
			break;

		case Action::SetOrientationNorth:
			this->_currentOrientation = Orientation::North;
			break;

		case Action::SetOrientationEast:
			this->_currentOrientation = Orientation::East;
			break;

		case Action::SetOrientationSouth:
			this->_currentOrientation = Orientation::South;
			break;

		case Action::SetOrientationWest:
			this->_currentOrientation = Orientation::West;
			break;

		case Action::Load_Jocker:
			this->_cubeLoader->loadInitialCube();
			break;

		case Action::Unload_Jocker:
			this->_cubeLoader->unloadInitialCube();
			break;

		case Action::Load_Prepare:
			this->_cubeLoader->prepareToLoad();
			break;

		case Action::Load_Lv1:
			this->_cubeLoader->loadCube(this->_currentOrientation, this->_currentLocation, TakingPosition::FrontCenter);
			this->_cubeLoader->prepareToLoad();
			break;

		case Action::Load_Lv2:
			this->_cubeLoader->loadCube(this->_currentOrientation, this->_currentLocation, TakingPosition::MiddleCenter);
			this->_cubeLoader->prepareToLoad();
			break;

		case Action::Load_Lv3:
			this->_cubeLoader->loadCube(this->_currentOrientation, this->_currentLocation, TakingPosition::MiddleLeft);
			this->_cubeLoader->prepareToLoad();
			break;

		case Action::Load_Lv4:
			this->_cubeLoader->loadCube(this->_currentOrientation, this->_currentLocation, TakingPosition::MiddleRight);
			this->_cubeLoader->prepareToLoad();
			break;

		case Action::Load_Lv5:
			this->_cubeLoader->loadCube(this->_currentOrientation, this->_currentLocation, TakingPosition::AwayCenter);
			break;

		case Action::ResetNewTower:
			this->_cubeLoader->prepareUnload();
			break;

		case Action::Prepare_Unload_Lv1:
			this->_cubeLoader->unloadCubeAtLevel(TowerLevel::Level1);
			break;

		case Action::Prepare_Unload_Lv2:
			this->_cubeLoader->unloadCubeAtLevel(TowerLevel::Level2);
			break;

		case Action::Prepare_Unload_Lv3:
			this->_cubeLoader->unloadCubeAtLevel(TowerLevel::Level3);
			break;

		case Action::Prepare_Unload_Lv4:
			this->_cubeLoader->unloadCubeAtLevel(TowerLevel::Level4);
			break;

		case Action::Prepare_Unload_Lv5:
			this->_cubeLoader->unloadCubeAtLevel(TowerLevel::Level5);
			break;

		case Action::Open:
			this->_cubeLoader->releaseCube();
			break;

		case Action::Close:
			this->_cubeLoader->holdCube();
			break;

		case Action::ModePelle:
			this->_cubeLoader->modePelle();
			break;

		case Action::ReadCombination:
			//TODO Manu
			break;

		case Action::None:
			break;

		default:
			Serial.println(F("Error action not handled"));
			break;
	}

	// delay to permit the I2C to probe the bus and show a busy
	_command = Action::None;
	Serial.println(F("command done"));

	// temporisation pour faciliter le traitement de l'interruption I2C,
	// doit être fait après la réinitialisation de command à Action::None
	delay(200);
}

void Cubes::serialParseInput(const char *buffer, int length) {
	Serial.write(buffer);

	if (length == 0)
		return;

	// TODO how to read the total number of points gathered ?

	if (length == 1) {
		switch (buffer[0]) {
			case COMMAND_SERIAL_I2C_SCAN:
				i2cScanMasterBus();
				break;

			case COMMAND_CUBE_TEST_BON_FONCTIONNEMENT:
				testBonFonctionnement();
				break;

			case COMMAND_CUBE_MISE_EN_POSITION_DEPART:
				// TODO
				break;

			case COMMAND_SET_COLOR_GREEN:
				setColorGreen();
				break;

			case COMMAND_SET_COLOR_ORANGE:
				setColorOrange();
				break;

			case COMMAND_EXEC_COLOR_DEPENDANT_ACTION:
				testColorDependantAction();
				break;

			default:
				Program::serialParseInput(buffer, length);
				break;
		}
	}

	if (length == 2) {
		switch (buffer[0]) {
			case COMMAND_HANDLE_AS_I2C:	// i
				slaveParameters[0] = buffer[1];
				slaveParametersCount = 1;
				Serial.write("Transmiting RS232 to i2C Command:");
				Serial.write(slaveParameters[0]);
				this->i2cHandleCommand();
				this->handleAction();
				this->handleAction();
				this->handleAction();
				break;
			case COMMAND_HANDLE_COMBINATION:	// c

				// needed to be on 2 lines because of cross initialization
				// https://stackoverflow.com/questions/12992108/crosses-initialization-of-variable-only-when-initialization-combined-with-decl
				CubeCombination* cc;
				cc = new CubeCombination();
				switch (buffer[1]) {
					case COMMAND_HANDLE_COMBINATION_OBG:	// ca
						cc->setCombination(CubeColor::Orange, CubeColor::Black, CubeColor::Green);
						break;
					case COMMAND_HANDLE_COMBINATION_YBU:	// cb
						cc->setCombination(CubeColor::Yellow, CubeColor::Black, CubeColor::Blue);
						break;
					case COMMAND_HANDLE_COMBINATION_UGO:	// cc
						cc->setCombination(CubeColor::Blue, CubeColor::Green, CubeColor::Orange);
						break;
					case COMMAND_HANDLE_COMBINATION_YGB:	// cd
						cc->setCombination(CubeColor::Yellow, CubeColor::Green, CubeColor::Black);
						break;
					case COMMAND_HANDLE_COMBINATION_BYO:	// ce
						cc->setCombination(CubeColor::Black, CubeColor::Yellow, CubeColor::Orange);
						break;
					case COMMAND_HANDLE_COMBINATION_GYU:	// cf
						cc->setCombination(CubeColor::Green, CubeColor::Yellow, CubeColor::Blue);
						break;
					case COMMAND_HANDLE_COMBINATION_UOB:	// cg
						cc->setCombination(CubeColor::Blue, CubeColor::Orange, CubeColor::Black);
						break;
					case COMMAND_HANDLE_COMBINATION_GOY:	// ch
						cc->setCombination(CubeColor::Green, CubeColor::Orange, CubeColor::Yellow);
						break;
					case COMMAND_HANDLE_COMBINATION_BUG:	// ci
						cc->setCombination(CubeColor::Black, CubeColor::Blue, CubeColor::Green);
						break;
					case COMMAND_HANDLE_COMBINATION_OUY:	// cj
						cc->setCombination(CubeColor::Orange, CubeColor::Blue, CubeColor::Yellow);
						break;
					default:
						Program::serialParseInput(buffer, length);
						break;
				}
				this->_cubeLoader->setCubeCombination(cc);
				break;

			case COMMAND_SET:	// s

				switch (buffer[1]) {
					case COMMAND_SET_ORIENTATION_NORTH:				// sn
						this->_currentOrientation = Orientation::North;
						break;
					case COMMAND_SET_ORIENTATION_WEST:				// sw
						this->_currentOrientation = Orientation::West;
						break;
					case COMMAND_SET_ORIENTATION_SOUTH:				// ss
						this->_currentOrientation = Orientation::South;
						break;
					case COMMAND_SET_ORIENTATION_EAST:				// se
						this->_currentOrientation = Orientation::East;
						break;
					case COMMAND_SET_LOCATION_NEAREST_GREEN:		// sg
						this->_currentLocation = CubeLocation::NearestGreen;
						break;
					case COMMAND_SET_LOCATION_NEAREST_ORANGE:		// so
						this->_currentLocation = CubeLocation::NearestOrange;
						break;
					case COMMAND_SET_CURRENT_TAKING_FRONTCENTER:	// sx
						this->_currentTaking = TakingPosition::FrontCenter;
						break;
					case COMMAND_SET_CURRENT_TAKING_MIDDLELEFT:		// sl
						this->_currentTaking = TakingPosition::MiddleLeft;
						break;
					case COMMAND_SET_CURRENT_TAKING_MIDDLECENTER:	// sy
						this->_currentTaking = TakingPosition::MiddleCenter;
						break;
					case COMMAND_SET_CURRENT_TAKING_MIDDLERIGHT:	// sr
						this->_currentTaking = TakingPosition::MiddleRight;
						break;
					case COMMAND_SET_CURRENT_TAKING_AWAYCENTER:		// sz
						this->_currentTaking = TakingPosition::AwayCenter;
						break;
					default:
						Program::serialParseInput(buffer, length);
						break;
				}
				break;

			case COMMAND_LOADING:	// l

				switch (buffer[1]) {
					case COMMAND_FULL_INSTANT_LOAD: // lf
						Serial.write(">>> Command DEBUG INSTANT LOAD\n");
						if (!this->_cubeLoader->instantLoad())
							Serial.write("\tINSTANT LOAD failed.\n");
						break;
					case COMMAND_PREPARE_TO_LOAD:	// lp
						Serial.write(">>> Command PREPARE_TO_LOAD\n");
						if (!this->_cubeLoader->prepareToLoad())
							Serial.write("\tPREPARE_TO_LOAD failed.\n");
						break;
					case COMMAND_LOAD_INITIAL_CUBE:	// li
						Serial.write(">>> Command LOAD_INITIAL_CUBE\n");
						if (!this->_cubeLoader->loadInitialCube())
							Serial.write("\tLOAD_INITIAL_CUBE failed.\n");
						break;
					case COMMAND_LOAD_CUBE: // ll
						Serial.write(">>> Command LOAD_CUBE\n");
						if (!this->_cubeLoader->loadCube(this->_currentOrientation, this->_currentLocation, this->_currentTaking))
							Serial.write("\tLOAD_CUBE failed.\n");
						break;

					case COMMAND_LOAD_CUBE_AND_HOLD_LV1:
						Serial.write(">>> Command LOAD CUBE AND HOLD LV1\n");
						if (!this->_cubeLoader->loadCubeAndHold(TowerLevel::Level1))
							Serial.write("\tLOAD_CUBE_AND_HOLD LV1 failed.\n");
						break;
					case COMMAND_LOAD_CUBE_AND_HOLD_LV2:
						Serial.write(">>> Command LOAD CUBE AND HOLD LV2\n");
						if (!this->_cubeLoader->loadCubeAndHold(TowerLevel::Level2))
							Serial.write("\tLOAD_CUBE_AND_HOLD LV2 failed.\n");
						break;
					case COMMAND_LOAD_CUBE_AND_HOLD_LV3:
						Serial.write(">>> Command LOAD CUBE AND HOLD LV3\n");
						if (!this->_cubeLoader->loadCubeAndHold(TowerLevel::Level3))
							Serial.write("\tLOAD_CUBE_AND_HOLD LV3 failed.\n");
						break;
					case COMMAND_LOAD_CUBE_AND_HOLD_LV4:
						Serial.write(">>> Command LOAD CUBE AND HOLD LV4\n");
						if (!this->_cubeLoader->loadCubeAndHold(TowerLevel::Level4))
							Serial.write("\tLOAD_CUBE_AND_HOLD LV4 failed.\n");
						break;
					case COMMAND_LOAD_CUBE_AND_HOLD_LV5:
						Serial.write(">>> Command LOAD CUBE AND HOLD LV5\n");
						if (!this->_cubeLoader->loadCubeAndHold(TowerLevel::Level5))
							Serial.write("\tLOAD_CUBE_AND_HOLD LV5 failed.\n");
						break;
					default:
						Serial.write(">> Command unknown\n");
						Program::serialParseInput(buffer, length);
						break;
				}
				break;

			case COMMAND_UNLOADING:	// u

				switch (buffer[1]) {
					case COMMAND_UNLOAD_TOWER:	// ut
						this->_currentPointsAcquired += this->_cubeLoader->unloadTower();
						break;

					case COMMMAND_UNHOLD_HERE_AS_IS:	// uh
						this->_cubeLoader->unloadTowerAsIs();
						break;
					case COMMAND_UNLOAD_EMPLACEMENT_1:	// u1
						this->_cubeLoader->unloadEmplacement(RackEmplacement::Level1Left, TowerLevel::Level1);
						break;
					case COMMAND_UNLOAD_EMPLACEMENT_2:	// u2
						this->_cubeLoader->unloadEmplacement(RackEmplacement::Level1Right, TowerLevel::Level2);
						break;
					case COMMAND_UNLOAD_EMPLACEMENT_3:	// u3
						this->_cubeLoader->unloadEmplacement(RackEmplacement::Level2Left, TowerLevel::Level3);
						break;
					case COMMAND_UNLOAD_EMPLACEMENT_4:	// u4
						this->_cubeLoader->unloadEmplacement(RackEmplacement::Level2Right, TowerLevel::Level4);
						break;
					case COMMAND_UNLOAD_EMPLACEMENT_5:	// u5
						this->_cubeLoader->unloadEmplacement(RackEmplacement::Level3Left, TowerLevel::Level5);
						break;
					case COMMAND_UNLOAD_EMPLACEMENT_6:	// u6
						this->_cubeLoader->unloadEmplacement(RackEmplacement::Level3Right, TowerLevel::Level1);
						break;
					default:
						Program::serialParseInput(buffer, length);
						break;
				}
				break;

			default:
				Program::serialParseInput(buffer, length);
				break;
		}
	}

	if (length == 5) {
		switch (buffer[0]) {
			case COMMAND_HANDLE_ABRITRARY_HEIGHT:		// h
				// strtol c'est trop compliqu?, on va faire simple
				// range ["h0000", "h4500"]
				int height;	// cannot init and assign in the same instruction
				height = (1000 * (buffer[1] - '0')) + (100 * (buffer[2] - '0')) + (10 * (buffer[3] - '0')) + (buffer[4] - '0');
				if (this->_cubeLoader->setArbitraryHeight(height))
					Serial.write(">>> Set successful.");
				else
					Serial.write(">>> Set failed.");
				break;
			case COMMAND_HANDLE_ARBITRARY_DIRECTION:	// d
				// strtol c'est trop compliqu?, on va faire simple
				// range ["d-180", "d+180"]
//				char* pEnd = NULL;
//				long direction = strtol(buffer[1], &pEnd, 10);
				int direction;	// cannot init and assign in the same instruction
				direction = (100 * (buffer[2] - '0')) + (10 * (buffer[3] - '0')) + (buffer[4] - '0');
				if (buffer[1] == '-')
					direction = -direction;
				if (this->_cubeLoader->setArbitraryDirection(direction))
					Serial.write(">>> Set successful.");
				else
					Serial.write(">>> Set failed.");
				break;
			case COMMAND_HANDLE_ARBITRARY_TIGHTENING:	// t
				// strtol c'est trop compliqu?, on va faire simple
				// range ["d-180", "d+180"]
//				char* pEnd = NULL;
//				long direction = strtol(buffer[1], &pEnd, 10);
				int quantity;	// cannot init and assign in the same instruction
				quantity = (100 * (buffer[2] - '0')) + (10 * (buffer[3] - '0')) + (buffer[4] - '0');
				if (buffer[1] == '-')
					quantity = -quantity;
				if (this->_cubeLoader->openToArbitraryQuantity(quantity))
					Serial.write(">>> Set successful.");
				else
					Serial.write(">>> Set failed.");
				break;

			default:
				Program::serialParseInput(buffer, length);
				break;
		}
	}

}

void Cubes::setColorGreen() {
	this->_matchSide = MatchSide::Green;
	this->_currentLocation = CubeLocation::NearestGreen;
}

void Cubes::setColorOrange() {
	this->_matchSide = MatchSide::Orange;
	this->_currentLocation = CubeLocation::NearestOrange;
}

void Cubes::testColorDependantAction() {
	switch (this->_matchSide) {
		case MatchSide::Green:
			// TODO change
			break;
		case MatchSide::Orange:
			// TODO change
			break;
		case MatchSide::Undefined:
			for (int i = 0; i < 10; i++) {
				// TODO change
			}
			break;
			// no default, we are in an enum
	}
}
