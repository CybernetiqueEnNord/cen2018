// I2C

// adresse I2C de la caméra (0x54 par défaut)
#define I2C_ADDR_PIXY PIXY_I2C_DEFAULT_ADDR
#ifndef I2C_ADDRESS_CUBES
#define I2C_ADDRESS_CUBES 0x62
#endif

//

#include "SoftWire.h"
#include "PixySoftI2C.h"
#include "Cubes.h"
#include "Program.h"
#include "Wire.h"
#include "../Commons/i2c/ActuatorCubes2018Commands.h"

SoftWire masterSoftWire;
PixySoftI2C pixy(I2C_ADDR_PIXY);
Cubes cubes(I2C_ADDRESS_CUBES, masterSoftWire);

void setupPins() {
}

void setup() {
  setupPins();

  Serial.begin(115200);
  Serial.setTimeout(3);

  //pixy.init();
  cubes.init();
}

void loop() {
  cubes.loop();
}

