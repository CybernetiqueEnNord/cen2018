/*
 * CubeLoader.h
 *
 *  Created on: 20 avr. 2018
 *      Author: Aeon
 */

#ifndef CUBELOADER_H_
#define CUBELOADER_H_

#include "Program.h"
#include "Rack.h"
#include "Poignet.h"
#include "Glissiere.h"
#include "CubeCombination.h"
#include "CubePositionning.h"

/**
 * The Cube loader represents the full mechanism of physical loading of the
 * cube and the logic to handle empty and full places in the rack, storing
 * and resolving the colors of the cube according to the location and the
 * moves of the robots, and the discharge ordering according to the build
 * plan that is to be scanned.
 */
class CubeLoader
{

private:

	Rack* _rack;
	Poignet* _poignet;
	Glissiere* _glissiere;

	CubeCombination* _cube_combination;
	UnloadOrdering* _unload_ordering;

	bool _is_initialized = false;
	bool _is_prepared_to_load = false;
	bool _end_match = false;

protected:
	bool loadCubeByColor(CubeColor cube);
	CubeColor resolveColor(Orientation orientation, CubeLocation cube_location, TakingPosition taking_position);
	CubePositionning* resolveCubePositionning(CubeLocation nearest_side_of_the_cube_set, Orientation from_where_the_robot_attacks_the_set);
	void moveToTowerLevel(TowerLevel stage);


public:

	CubeColor unloadEmplacement(RackEmplacement emplacement, TowerLevel stage);

	/**
	 * Sets the poignet that will be used by the cube loader to physically
	 * load the cubes into the rack.
	 */
	void setPoignet(Poignet* poignet);

	/**
	 * Sets the glissiere that will be used by the cube loaded to physically
	 * load the cubes into the rack.
	 */
	void setGlissiere(Glissiere* glissiere);

	/**
	 * Sets the cube combination that will be used to unload all of the cubes
	 * on the construction play ground.
	 */
	void setCubeCombination(CubeCombination* cube_combination);

	/**
	 * Sets the default strategy that is used to unload the combination
	 * without the correct cube combination known.
	 */
	bool setDefaultStrategy(int strategy);

	/**
	 * Initializes the cube loader and the rack.
	 *
	 * Returns true if the initialization is successful.
	 */
	bool init();

	/**
	 * DO NOT USE IN PRODUCTION
	 *
	 * Considers the rack fully loaded with joker cubes.
	 */
	bool instantLoad();

	/**
	 * Prepares the actuator to be in position for starting the game.
	 *
	 * Returns true if the preparation is successful.
	 */
	bool setInitialPositionRetractedAndColorDependant(MatchSide side);

	/**
	 * Releases the cube that is placed at the current position of the poignet.
	 */
	bool releaseCube();

	/**
	 * Holds the cube that is placed at the current position of the poignet.
	 */
	bool holdCube();

	/**
	 * Open the pince devant to push cubes.
	 */
	bool modePelle();

	/**
	 * Prepares the actuator to be in position of loading.
	 *
	 * Returns true if the preparation is successful.
	 */
	bool prepareToLoad();

	/**
	 * Loads one joker cube.
	 *
	 * Returns true if the cube has been successfully loaded.
	 */
	bool loadInitialCube();

	/**
	 * UnLoads one joker cube.
	 *
	 * Returns true if the cube has been successfully loaded.
	 */
	bool unloadInitialCube();

	/**
	 * Loads a cube from the cross.
	 * The orientation of the cube is known by the trajectory.
	 * The cube location is known by the trajectory and the match side.
	 * The taking position is known by the order in which the robot makes
	 * 		moves to take individually each cube.
	 *
	 * 	Returns true if the cube has been successfully loaded.
	 */
	bool loadCube(Orientation orientation, CubeLocation cube_location, TakingPosition taking_position);

	/**
	 * @deprecated
	 */
	bool loadCubeAndHold(TowerLevel level);

	/**
	 * Prepares an unload ordering. If there is 5 cubes available in the rack,
	 * the resulting unload ordering will unload those 5 cubes. If there is less
	 * cubes, it will unload all the cubes, and if there is more than 5 cubes,
	 * it will unload 5 cubes maximum.
	 */
	void prepareUnload();

	/**
	 * Unloads the cube according to the previous ordering. If no ordering is
	 * defined, a new ordering is generated on the fly. This unloads the nth
	 * cube according to the requested tower level.
	 *
	 * Returns whether the unloading is successful.
	 */
	CubeColor unloadCubeAtLevel(TowerLevel level);

	/**
	 * Unloads a pile up to 5 cubes, if there is 5 cubes available in the rack.
	 * When there is less than 5 cubes available, this unloads all the cubes.
	 * This method tries to unload the cubes with the motives whenever possible.
	 *
	 * Returns the expected number of points such a tower makes according
	 * to the rules.
	 */
	int unloadTower();

	/**
	 * Discharges a cube in the position the poignet is in.
	 */
	bool unloadTowerAsIs();

	/**
	 * Handles the end of the match. This sets a flag which inhibits all the
	 * actions to be done.
	 */
	void handleEndMatch();

	/**
	 * DEBUG function. Sets the glissiere to an arbitrary height. Use to tune
	 * the action to the place they should be.
	 *
	 * Returns true if the move is a success, false else.
	 */
	bool setArbitraryHeight(int height);

	/**
	 * DEBUG function. Sets the poignet to an arbitrary direction. Use to tune
	 * the action to the place they should be.
	 *
	 * Returns true if the move is a success, false else.
	 */
	bool setArbitraryDirection(int direction);

	/**
	 * DEBUG function. Sets the pince to an arbitrary position. Use to tune
	 * the action to the place they should be.
	 *
	 * Returns true if the move is a success, false else.
	 */
	bool openToArbitraryQuantity(int quantity);

};

#endif /* CUBELOADER_H_ */
