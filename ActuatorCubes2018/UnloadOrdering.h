/*
 * UnloadOrdering.h
 *
 *  Created on: 20 avr. 2018
 *      Author: Anastaszor
 */

#ifndef UNLOADORDERING_H_
#define UNLOADORDERING_H_

#include "Program.h"

/**
 * This class describes the successive emplacements to unload in order
 * to make the tower with its required color combination fulfilled, as
 * long as possible.
 */
class UnloadOrdering
{

private:

	/**
	 * The first rack emplacement to unload.
	 */
	RackEmplacement _unload_first = RackEmplacement::Not_An_Emplacement;

	/**
	 * The second rack emplacement to unload.
	 */
	RackEmplacement _unload_second = RackEmplacement::Not_An_Emplacement;

	/**
	 * The third rack emplacement to unload.
	 */
	RackEmplacement _unload_third = RackEmplacement::Not_An_Emplacement;

	/**
	 * The fourth rack emplacement to unload.
	 */
	RackEmplacement _unload_fourth = RackEmplacement::Not_An_Emplacement;

	/**
	 * The fifth and last rack emplacement to unload.
	 */
	RackEmplacement _unload_last = RackEmplacement::Not_An_Emplacement;

	/**
	 * The expected number of points to make if this unload is done
	 * as described.
	 */
	int _expected_points = 0;

public:

	/**
	 * Getter for the first emplacement to unload.
	 */
	RackEmplacement getUnloadFirst();

	/**
	 * Setter for the first emplacement to unload.
	 */
	void setUnloadFirst(RackEmplacement emplacement);

	/**
	 * Getter for the second emplacement to unload.
	 */
	RackEmplacement getUnloadSecond();

	/**
	 * Setter for the second emplacement to unload.
	 */
	void setUnloadSecond(RackEmplacement emplacement);

	/**
	 * Getter for the third emplacement to unload.
	 */
	RackEmplacement getUnloadThird();

	/**
	 * Setter for the third emplacement to unload.
	 */
	void setUnloadThird(RackEmplacement emplacement);

	/**
	 * Getter for the fourth emplacement to unload.
	 */
	RackEmplacement getUnloadFourth();

	/**
	 * Setter for the fourth emplacement to unload.
	 */
	void setUnloadFourth(RackEmplacement emplacement);

	/**
	 * Getter for the fifth emplacement to unload.
	 */
	RackEmplacement getUnloadLast();

	/**
	 * Setter for the fifth emplacement to unload.
	 */
	void setUnloadLast(RackEmplacement emplacement);

	/**
	 * Sets whether the unload order contains the right sequence of colors
	 * to make the points available.
	 */
	void setUnloadOrderContainsCombination();

	/**
	 * Gets the expected reward in points if the tower is built as
	 * described.
	 */
	int getExpectedPoints();

};

#endif /* UNLOADORDERING_H_ */
