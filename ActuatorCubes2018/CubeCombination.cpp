/*
 * CubeCombination.cpp
 *
 *  Created on: 20 avr. 2018
 *      Author: Anastaszor
 */

#include "CubeCombination.h"
#include "Program.h"

void CubeCombination::setCombination(CubeColor left, CubeColor middle, CubeColor right)
{
	this->_color_left = left;
	this->_color_middle = middle;
	this->_color_right = right;
}

CubeColor CubeCombination::getColorLeft()
{
	return this->_color_left;
}

CubeColor CubeCombination::getColorMiddle()
{
	return this->_color_middle;
}

CubeColor CubeCombination::getColorRight()
{
	return this->_color_right;
}

bool CubeCombination::canBeHandledBy(Rack* rack)
{
	RackEmplacement first = rack->getEmplacementForColor(this->getColorLeft());
	RackEmplacement second = rack->getEmplacementForColor(this->getColorMiddle());
	RackEmplacement third = rack->getEmplacementForColor(this->getColorRight());

	if(    first  == RackEmplacement::Not_An_Emplacement
		|| second == RackEmplacement::Not_An_Emplacement
		|| third  == RackEmplacement::Not_An_Emplacement
		|| first  == second
		|| second == third
		|| third  == first
	) {
		return false;
	}
	return true;
}

UnloadOrdering* CubeCombination::getUnloadOrderingFrom(Rack* rack)
{
	UnloadOrdering* uo = new UnloadOrdering();

	RackEmplacement e1, e2, e3, e4, e5;
	CubeColor cc1, cc2, cc3, cc4, cc5;

	// if possible to have the combination, unload it first
	// this lowers the chances to have it collapse
	if(this->canBeHandledBy(rack))
	{
		e1 = rack->getEmplacementForColor(this->getColorLeft());
		cc1 = rack->unloadEmplacement(e1);
		uo->setUnloadFirst(e1);
		e2 = rack->getEmplacementForColor(this->getColorMiddle());
		cc2 = rack->unloadEmplacement(e2);
		uo->setUnloadSecond(e2);
		e3 = rack->getEmplacementForColor(this->getColorRight());
		cc3 = rack->unloadEmplacement(e3);
		uo->setUnloadThird(e3);
		uo->setUnloadOrderContainsCombination();
	}
	else // else just unload random cubes, but not the joker if > 5 available
	{
		e1 = rack->getNextOccupiedEmplacement();
		cc1 = rack->unloadEmplacement(e1);
		uo->setUnloadFirst(e1);
		e2 = rack->getNextOccupiedEmplacement();
		cc2 = rack->unloadEmplacement(e2);
		uo->setUnloadSecond(e2);
		e3 = rack->getNextOccupiedEmplacement();
		cc3 = rack->unloadEmplacement(e3);
		uo->setUnloadThird(e3);
	}

	// those slots are optional, but in case the next occupied element
	// is an Not_An_Emplacement, it should be handled properly when
	// unloading the rack with the given
	e4 = rack->getNextOccupiedEmplacement();
	cc4 = rack->unloadEmplacement(e4);
	uo->setUnloadFourth(e4);
	e5 = rack->getNextOccupiedEmplacement();
	cc5 = rack->unloadEmplacement(e5);
	uo->setUnloadLast(e5);

	// the unloads are for the simulation, now load them back
	rack->loadEmplacement(e1, cc1);
	rack->loadEmplacement(e2, cc2);
	rack->loadEmplacement(e3, cc3);
	rack->loadEmplacement(e4, cc4);
	rack->loadEmplacement(e5, cc5);

	return uo;
}
