/*
 * Glissiere.h
 *
 *  Created on: 19 avr. 2018
 *      Author: Anastaszor
 */

#ifndef GLISSIERE_H_
#define GLISSIERE_H_

#include "A4988.h"
#include "Constants.h"
#include "Program.h"



class Glissiere
{

private:



	/**
	 * Whether the glissiere is properly initialized. Most of the functions
	 * that are useful for moving parts will do nothing if the initialization
	 * was not done.
	 *
	 * The initialization needs the glissiere config object to be properly
	 * filled of data.
	 */
	bool _is_initialized = false;

	/**
	 * The actual position the glissiere thinks the stepper motor is in.
	 * This position is made from calculations from expected movements and
	 * may or may not be accurate from reality. This position is reset to zero
	 * each time the stepper motor performs a move to the bottom (and 0 is
	 * the bottom position).
	 */
	int _actual_position_steps = 0;


protected:

	/**
	 * Gets whether the motor is at the bottom of the glissiere.
	 * The check is done by looking at the bottom switch and checking
	 * if there is information in it.
	 */
	bool isAtBottom();


public:

	/**
	 * Initializes the glissiere by telling the arduino api which wiring
	 * to take account for. It initializes as well the mechanical constants
	 * of the stepper motor to use.
	 *
	 * This method returns true if the wiring is successful, and false if
	 * it fails.
	 */
	bool init();

	/**
	 * Moves the poignet position to the bottomest position. This position
	 * is made to charge cubes to the racks.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool moveToTheBottom();


	bool moveToRackLevel1Left(TowerSubLevel level);

	/**
	 * Moves the poignet position to the rack level 1. This position is made
	 * to release a cube into one of the two racks at level 1. If on top of the
	 * cube is false, then it moves at the height to load or unload the cube.
	 * If on top of the cube is true, it moves just up from the cube, so that
	 * rotations of the poignet do not carry the cube with it.
	 *
	 * Level 1 is the bottomest level for the racks.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool moveToRackLevel1Right(TowerSubLevel level);


	bool moveToRackLevel2Left(TowerSubLevel level);

	/**
	 * Moves the poignet position to the rack level 2. This position is made
	 * to release a cube into one of the two racks at level 2. If on top of the
	 * cube is false, then it moves at the height to load or unload the cube.
	 * If on top of the cube is true, it moves just up from the cube, so that
	 * rotations of the poignet do not carry the cube with it.
	 *
	 * Level 2 is the middle level for the racks.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool moveToRackLevel2Right(TowerSubLevel level);


	bool moveToRackLevel3Left(TowerSubLevel level);

	/**
	 * Moves the poignet position to the rack level 3. This position is made to
	 * release a cube into one of the two racks at level 3. If on top of the
	 * cube is false, then it moves at the height to load or unload the cube.
	 * If on top of the cube is true, it moves just up from the cube, so that
	 * rotations of the poignet do not carry the cube with it.
	 *
	 * Level 3 is the upper level for the racks.
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool moveToRackLevel3Right(TowerSubLevel level);

	/**
	 * Moves the poignet position to the tower level 1. This position is made
	 * to release a cube into the bottomest position for a tower.
	 *
	 * Level 1 is the floor level for cubes. (= 1 point / cube)
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool moveToTowerLevel1();

	/**
	 * Moves the poignet position to the tower level 2. This position is made
	 * to release a cube on top of an existing cube at level 1.
	 *
	 * Level 2 is the next floor level for cubes (=2 points / cube)
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool moveToTowerLevel2();

	/**
	 * Moves the poignet position to the tower level 3. This position is made
	 * to release a cube on top of an existing cube at level 2.
	 *
	 * Level 3 is the middle floor level for cubes (=3 points / cube)
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool moveToTowerLevel3();

	/**
	 * Moves the poignet position to the tower level 4. This position is made
	 * to release a cube on top of an existing cube at level 3.
	 *
	 * Level 4 is the middle next floor level for cubes (=4 points / cube)
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool moveToTowerLevel4();

	/**
	 * Moves the poignet position to the tower level 5. This position is made
	 * to release a cube on top of an existing cube at level 4.
	 *
	 * Level 5 is the upper floor for cubes (=5 points / cube)
	 *
	 * This method returns true if the order is passed, and false if it fails.
	 */
	bool moveToTowerLevel5();



	/**
	 * DEBUG function. Sets the glissiere height to an arbitrary value.
	 *
	 * Returns true if the move is a success, false else.
	 */
	bool moveToArbitraryHeight(int height);

};

#endif /* GLISSIERE_H_ */
