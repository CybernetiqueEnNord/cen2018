/*
 * Rack.cpp
 *
 *  Created on: 20 avr. 2018
 *      Author: Anastaszor
 */

#include "Rack.h"

bool Rack::hasLevel1LeftOccupied()
{
	return CubeColor::Not_A_Cube == this->_level_1_left;
}

bool Rack::hasLevel1RightOccupied()
{
	return CubeColor::Not_A_Cube == this->_level_1_right;
}

bool Rack::hasLevel2LeftOccupied()
{
	return CubeColor::Not_A_Cube == this->_level_2_left;
}

bool Rack::hasLevel2RightOccupied()
{
	return CubeColor::Not_A_Cube == this->_level_2_right;
}

bool Rack::hasLevel3LeftOccupied()
{
	return CubeColor::Not_A_Cube == this->_level_3_left;
}

bool Rack::hasLevel3RightOccupied()
{
	return CubeColor::Not_A_Cube == this->_level_3_right;
}

bool Rack::setStrategy(int strategy)
{
	if(strategy > 0 && strategy < 6)
	{
		this->_default_strategy = strategy;
		return true;
	}
	return false;
}

RackEmplacement Rack::getEmplacementForColor(CubeColor expected)
{
	if(expected == CubeColor::Not_A_Cube)
		return RackEmplacement::Not_An_Emplacement;

	// first round, search for a cube of the given color
	if(this->_level_1_left == expected)
		return RackEmplacement::Level1Left;
	if(this->_level_1_right == expected)
		return RackEmplacement::Level1Right;
	if(this->_level_2_left == expected)
		return RackEmplacement::Level2Left;
	if(this->_level_2_right == expected)
		return RackEmplacement::Level2Right;
	if(this->_level_3_left == expected)
		return RackEmplacement::Level3Left;
	if(this->_level_3_right == expected)
		return RackEmplacement::Level3Right;

	// second round, search for a cube of the joker color
	if(this->_level_1_left == CubeColor::White)
		return RackEmplacement::Level1Left;
	if(this->_level_1_right == CubeColor::White)
		return RackEmplacement::Level1Right;
	if(this->_level_2_left == CubeColor::White)
		return RackEmplacement::Level2Left;
	if(this->_level_2_right == CubeColor::White)
		return RackEmplacement::Level2Right;
	if(this->_level_3_left == CubeColor::White)
		return RackEmplacement::Level3Left;
	if(this->_level_3_right == CubeColor::White)
		return RackEmplacement::Level3Right;

	// not found
	return RackEmplacement::Not_An_Emplacement;
}

RackEmplacement Rack::getNextOccupiedEmplacement()
{
	// first round, search for a cube not joker
	// ordering : suppose all the cubes are loaded, unload by probability
	// that they make a combination and that they are well loaded
	// TakingPosition : FrontCenter, MiddleCenter, AwayCenter, MiddleLeft, MiddleRight
	// translated     : level_3_left, level_2_left, level_1_left, level_2_right, level_1_right
	// UNLOAD ORDER   : B Y R G U

	switch(this->_default_strategy)
	{
		case 4: // tower D [GYU, YUR]
			// BGYUR
			if(this->_level_1_left != CubeColor::Not_A_Cube && this->_level_1_left != CubeColor::White)
				return RackEmplacement::Level1Left;
			if(this->_level_2_right != CubeColor::Not_A_Cube && this->_level_2_right != CubeColor::White)
				return RackEmplacement::Level2Right;
			if(this->_level_1_right != CubeColor::Not_A_Cube && this->_level_1_right != CubeColor::White)
				return RackEmplacement::Level1Right;
			if(this->_level_3_left != CubeColor::Not_A_Cube && this->_level_3_left != CubeColor::White)
				return RackEmplacement::Level3Left;
			if(this->_level_2_left != CubeColor::Not_A_Cube && this->_level_2_left != CubeColor::White)
				return RackEmplacement::Level2Left;
			break;

		case 3:	// tower C [YGB, GBR, BRU]
			// YGBRU
			if(this->_level_1_right != CubeColor::Not_A_Cube && this->_level_1_right != CubeColor::White)
				return RackEmplacement::Level1Right;
			if(this->_level_2_right != CubeColor::Not_A_Cube && this->_level_2_right != CubeColor::White)
				return RackEmplacement::Level2Right;
			if(this->_level_1_left != CubeColor::Not_A_Cube && this->_level_1_left != CubeColor::White)
				return RackEmplacement::Level1Left;
			if(this->_level_2_left != CubeColor::Not_A_Cube && this->_level_2_left != CubeColor::White)
				return RackEmplacement::Level2Left;
			if(this->_level_3_left != CubeColor::Not_A_Cube && this->_level_3_left != CubeColor::White)
				return RackEmplacement::Level3Left;
			break;

		case 2:	// tower B [YBU, BUG]
			// YBUGR
			if(this->_level_1_right != CubeColor::Not_A_Cube && this->_level_1_right != CubeColor::White)
				return RackEmplacement::Level1Right;
			if(this->_level_1_left != CubeColor::Not_A_Cube && this->_level_1_left != CubeColor::White)
				return RackEmplacement::Level1Left;
			if(this->_level_3_left != CubeColor::Not_A_Cube && this->_level_3_left != CubeColor::White)
				return RackEmplacement::Level3Left;
			if(this->_level_2_right != CubeColor::Not_A_Cube && this->_level_2_right != CubeColor::White)
				return RackEmplacement::Level2Right;
			if(this->_level_2_left != CubeColor::Not_A_Cube && this->_level_2_left != CubeColor::White)
				return RackEmplacement::Level2Left;
			break;

		case 1:	// tower A [BYR, YRG, RGU]
		default:
			// BYRGU
			if(this->_level_1_left != CubeColor::Not_A_Cube && this->_level_1_left != CubeColor::White)
				return RackEmplacement::Level1Left;
			if(this->_level_1_right != CubeColor::Not_A_Cube && this->_level_1_right != CubeColor::White)
				return RackEmplacement::Level1Right;
			if(this->_level_2_left != CubeColor::Not_A_Cube && this->_level_2_left != CubeColor::White)
				return RackEmplacement::Level2Left;
			if(this->_level_2_right != CubeColor::Not_A_Cube && this->_level_2_right != CubeColor::White)
				return RackEmplacement::Level2Right;
			if(this->_level_3_left != CubeColor::Not_A_Cube && this->_level_3_left != CubeColor::White)
				return RackEmplacement::Level3Left;
			//if(this->_level_3_right != CubeColor::Not_A_Cube && this->_level_3_right != CubeColor::White)
			//	return RackEmplacement::Level3Right;
			break;
	}

	// second round, search for a cube of the joker color
	if(this->_level_1_left == CubeColor::White)
		return RackEmplacement::Level1Left;
	if(this->_level_1_right == CubeColor::White)
		return RackEmplacement::Level1Right;
	if(this->_level_2_left == CubeColor::White)
		return RackEmplacement::Level2Left;
	if(this->_level_2_right == CubeColor::White)
		return RackEmplacement::Level2Right;
	if(this->_level_3_left == CubeColor::White)
		return RackEmplacement::Level3Left;
	if(this->_level_3_right == CubeColor::White)
		return RackEmplacement::Level3Right;

	return RackEmplacement::Not_An_Emplacement;
}

RackEmplacement Rack::getNextFreeEmplacement()
{
	if(this->hasLevel1LeftOccupied())
		return RackEmplacement::Level1Left;
	if(this->hasLevel1RightOccupied())
		return RackEmplacement::Level1Right;
	if(this->hasLevel2LeftOccupied())
		return RackEmplacement::Level2Left;
	if(this->hasLevel2RightOccupied())
		return RackEmplacement::Level2Right;
	if(this->hasLevel3LeftOccupied())
		return RackEmplacement::Level3Left;
	//if(this->hasLevel3RightOccupied())
	//	return RackEmplacement::Level3Right;

	return RackEmplacement::Not_An_Emplacement;
}

void Rack::loadEmplacement(RackEmplacement emplacement, CubeColor cube)
{
	switch(emplacement)
	{
		case RackEmplacement::Level1Left:
			this->loadLevel1Left(cube);
			break;
		case RackEmplacement::Level1Right:
			this->loadLevel1Right(cube);
			break;
		case RackEmplacement::Level2Left:
			this->loadLevel2Left(cube);
			break;
		case RackEmplacement::Level2Right:
			this->loadLevel2Right(cube);
			break;
		case RackEmplacement::Level3Left:
			this->loadLevel3Left(cube);
			break;
		case RackEmplacement::Level3Right:
			this->loadLevel3Right(cube);
			break;
		case RackEmplacement::Not_An_Emplacement:
			// nothing to do
			break;
	}
}

void Rack::loadLevel1Left(CubeColor cube)
{
	this->_level_1_left = cube;
}

void Rack::loadLevel1Right(CubeColor cube)
{
	this->_level_1_right = cube;
}

void Rack::loadLevel2Left(CubeColor cube)
{
	this->_level_2_left = cube;
}

void Rack::loadLevel2Right(CubeColor cube)
{
	this->_level_2_right = cube;
}

void Rack::loadLevel3Left(CubeColor cube)
{
	this->_level_3_left = cube;
}

void Rack::loadLevel3Right(CubeColor cube)
{
	this->_level_3_right = cube;
}

CubeColor Rack::unloadEmplacement(RackEmplacement emplacement)
{
	switch(emplacement)
	{
		case RackEmplacement::Level1Left:
			return this->unloadLevel1Left();
		case RackEmplacement::Level1Right:
			return this->unloadLevel1Right();
		case RackEmplacement::Level2Left:
			return this->unloadLevel2Left();
		case RackEmplacement::Level2Right:
			return this->unloadLevel2Right();
		case RackEmplacement::Level3Left:
			return this->unloadLevel3Left();
		case RackEmplacement::Level3Right:
			return this->unloadLevel3Right();
		default:
			return CubeColor::Not_A_Cube;
	}
	return CubeColor::Not_A_Cube;
}

CubeColor Rack::unloadLevel1Left()
{
	CubeColor cc = this->_level_1_left;
	this->_level_1_left = CubeColor::Not_A_Cube;
	return cc;
}

CubeColor Rack::unloadLevel1Right()
{
	CubeColor cc = this->_level_1_right;
	this->_level_1_right = CubeColor::Not_A_Cube;
	return cc;
}

CubeColor Rack::unloadLevel2Left()
{
	CubeColor cc = this->_level_2_left;
	this->_level_2_left = CubeColor::Not_A_Cube;
	return cc;
}

CubeColor Rack::unloadLevel2Right()
{
	CubeColor cc = this->_level_2_right;
	this->_level_2_right = CubeColor::Not_A_Cube;
	return cc;
}

CubeColor Rack::unloadLevel3Left()
{
	CubeColor cc = this->_level_3_left;
	this->_level_3_left = CubeColor::Not_A_Cube;
	return cc;
}

CubeColor Rack::unloadLevel3Right()
{
	CubeColor cc = this->_level_3_right;
	this->_level_3_right = CubeColor::Not_A_Cube;
	return cc;
}
