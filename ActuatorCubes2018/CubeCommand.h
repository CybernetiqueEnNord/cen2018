

// Commands from the arduino

#define COMMAND_SERIAL_I2C_SCAN                 's'
#define COMMAND_CUBE_TEST_BON_FONCTIONNEMENT    'b'
#define COMMAND_CUBE_MISE_EN_POSITION_DEPART    'd'
#define COMMAND_SET_COLOR_GREEN                 'g'
#define COMMAND_SET_COLOR_ORANGE                'o'
#define COMMAND_EXEC_COLOR_DEPENDANT_ACTION     'c'

#define COMMAND_HANDLE_AS_I2C					'i'
#define COMMAND_HANDLE_COMBINATION              'c'
#define COMMAND_HANDLE_COMBINATION_OBG          'a'
#define COMMAND_HANDLE_COMBINATION_YBU          'b'
#define COMMAND_HANDLE_COMBINATION_UGO          'c'
#define COMMAND_HANDLE_COMBINATION_YGB          'd'
#define COMMAND_HANDLE_COMBINATION_BYO          'e'
#define COMMAND_HANDLE_COMBINATION_GYU          'f'
#define COMMAND_HANDLE_COMBINATION_UOB          'g'
#define COMMAND_HANDLE_COMBINATION_GOY          'h'
#define COMMAND_HANDLE_COMBINATION_BUG          'i'
#define COMMAND_HANDLE_COMBINATION_OUY          'j'

#define COMMAND_SET                             's'
#define COMMAND_SET_ORIENTATION_NORTH           'n'
#define COMMAND_SET_ORIENTATION_WEST            'w'
#define COMMAND_SET_ORIENTATION_SOUTH           's'
#define COMMAND_SET_ORIENTATION_EAST            'e'
#define COMMAND_SET_LOCATION_NEAREST_GREEN      'g'
#define COMMAND_SET_LOCATION_NEAREST_ORANGE     'o'
#define COMMAND_SET_CURRENT_TAKING_FRONTCENTER  'x'
#define COMMAND_SET_CURRENT_TAKING_MIDDLELEFT   'l'
#define COMMAND_SET_CURRENT_TAKING_MIDDLECENTER 'y'
#define COMMAND_SET_CURRENT_TAKING_MIDDLERIGHT  'r'
#define COMMAND_SET_CURRENT_TAKING_AWAYCENTER   'z'

#define COMMAND_LOADING                         'l'
#define COMMAND_FULL_INSTANT_LOAD               'f'
#define COMMAND_PREPARE_TO_LOAD                 'p'
#define COMMAND_LOAD_INITIAL_CUBE               'i'
#define COMMAND_LOAD_CUBE                       'l'
#define COMMAND_LOAD_CUBE_AND_HOLD_LV1          '1'
#define COMMAND_LOAD_CUBE_AND_HOLD_LV2          '2'
#define COMMAND_LOAD_CUBE_AND_HOLD_LV3          '3'
#define COMMAND_LOAD_CUBE_AND_HOLD_LV4          '4'
#define COMMAND_LOAD_CUBE_AND_HOLD_LV5          '5'

#define COMMAND_HANDLE_ABRITRARY_HEIGHT         'h'
#define COMMAND_HANDLE_ARBITRARY_DIRECTION      'd'
#define COMMAND_HANDLE_ARBITRARY_TIGHTENING     't'


#define COMMAND_UNLOADING                       'u'
#define COMMAND_UNLOAD_TOWER                    't'
#define COMMAND_UNLOAD_EMPLACEMENT_1            '1'
#define COMMAND_UNLOAD_EMPLACEMENT_2            '2'
#define COMMAND_UNLOAD_EMPLACEMENT_3            '3'
#define COMMAND_UNLOAD_EMPLACEMENT_4            '4'
#define COMMAND_UNLOAD_EMPLACEMENT_5            '5'
#define COMMAND_UNLOAD_EMPLACEMENT_6            '6'
#define COMMMAND_UNHOLD_HERE_AS_IS              'h'


// Slave readings

//#define SLAVE_READ_NB_POINTS                    'n'

