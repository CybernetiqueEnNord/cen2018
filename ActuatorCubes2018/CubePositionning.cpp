/*
 * CubePositionning.cpp
 *
 *  Created on: 18 avr. 2018
 *      Author: Anastaszor
 */

#include "CubePositionning.h"

CubeColor CubePositionning::getNearestCube()
{
	return this->_nearest_cube;
}

void CubePositionning::setNearestCube(CubeColor nearest_cube)
{
	this->_nearest_cube = nearest_cube;
}

CubeColor CubePositionning::getLeftmostCube()
{
	return this->_leftmost_cube;
}

void CubePositionning::setLeftmostCube(CubeColor leftmost_cube)
{
	this->_leftmost_cube = leftmost_cube;
}

CubeColor CubePositionning::getCenteredCube()
{
	return this->_centered_cube;
}

void CubePositionning::setCenteredCube(CubeColor centered_cube)
{
	this->_centered_cube = centered_cube;
}

CubeColor CubePositionning::getRightmostCube()
{
	return this->_rightmost_cube;
}

void CubePositionning::setRightmostCube(CubeColor rightmost_cube)
{
	this->_rightmost_cube = rightmost_cube;
}

CubeColor CubePositionning::getFarerCube()
{
	return this->_farer_cube;
}

void CubePositionning::setFarerCube(CubeColor farer_cube)
{
	this->_farer_cube = farer_cube;
}
