/*
 * MainMotor.h
 *
 *  Created on: 6 d�c. 2014
 *      Author: Emmanuel
 */

#ifndef DEVICES_MOTORS_MOTOR_H_
#define DEVICES_MOTORS_MOTOR_H_

/**
 * \brief Moteur g�n�rique
 *
 * Classe abstraite de gestion d'un moteur.
 */
class Motor {
protected:
	/**
	 * Constructeur.
	 */
	Motor();
public:
	/**
	 * \brief Freinage
	 *
	 * Freine le moteur en utilisant le freinage actif s'il est disponible.
	 */
	virtual void brake() = 0;
	/**
	 * \brief Charge �lectrique consomm�e (mC)
	 *
	 * Renvoie la charge �lectrique consomm�e par le moteur.
	 * Renvoie 0 si la mesure n'est pas prise en charge.
	 * @return la charge consomm�e en mC
	 */
	virtual int getCharge() const;
	/**
	 * \brief Intensit� du courant �lectrique (mA)
	 *
	 * Renvoie la valeur instantan�e du courant.
	 * Renvoie 0 si la mesure n'est pas prise en charge.
	 * @return la valeur du courant en mA
	 */
	virtual int getCurrent() const;
	/**
	 * \brief Activation / d�sactivation
	 *
	 * D�finit l'�tat du moteur.
	 * Activ�, la vitesse est contrainte. D�sactiv�, le moteur est en roue libre.
	 * @param value [in] true pour contraindre la vitesse de rotation, false pour basculer en roue libre
	 */
	virtual void setEnabled(bool value) = 0;
	/**
	 * \brief Sens et vitesse
	 *
	 * D�finit le sens et la vitesse de rotation par rapport � la valeur neutre.
	 * @param value [in] la vitesse et le sens de rotation
	 */
	virtual void setSpeed(int value) = 0;
};

#endif /* DEVICES_MOTORS_MOTOR_H_ */
