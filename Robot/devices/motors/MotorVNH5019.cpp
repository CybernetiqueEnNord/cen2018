/*
 * MotorVNH5019.cpp
 *
 *  Created on: 23 ao�t 2017
 *      Author: Emmanuel
 */

#include "devices/motors/MotorVNH5019.h"

#include <stdlib.h>

MotorVNH5019::MotorVNH5019(MotorVNH5019Config &config) :
		Motor(), config(config) {
}

void MotorVNH5019::setSpeed(int value) {
	// d�termine la direction
	bool forward = (value >= 0) ^ config.reversed;
	palWritePad(config.portDirectionA, config.padDirectionA, forward ? PAL_HIGH : PAL_LOW);
	palWritePad(config.portDirectionB, config.padDirectionB, !forward ? PAL_HIGH : PAL_LOW);

	value = abs(value) + config.neutralValue;
	pwmEnableChannel(config.pwmDriver, config.pwmChannel, value);

	updateCurrent();
}

void MotorVNH5019::brake() {
	palWritePad(config.portDirectionA, config.padDirectionA, PAL_LOW);
	palWritePad(config.portDirectionB, config.padDirectionB, PAL_LOW);
}

void MotorVNH5019::setEnabled(bool value) {
	palSetPadMode(config.portEnable, config.padEnable, value ? PAL_MODE_INPUT_PULLUP : PAL_MODE_INPUT_PULLDOWN);
}

void MotorVNH5019::initialize() {
	// d�sactive la carte avant de configurer le reste (mode roue libre)
	setEnabled(false);

	// configuration de la direction
	palSetPadMode(config.portDirectionA, config.padDirectionA, PAL_MODE_OUTPUT_PUSHPULL);
	palSetPadMode(config.portDirectionB, config.padDirectionB, PAL_MODE_OUTPUT_PUSHPULL);

	// PWM
	pwmStart(config.pwmDriver, config.pwmConfig);
	palSetPadMode(config.portPwm, config.padPwm, PAL_MODE_ALTERNATE(config.modePwm));

	// Mesure analogique
	palSetPadMode(config.portAnalog, config.padAnalog, PAL_MODE_INPUT_ANALOG);

	// active le PWM avec la valeur neutre (roue libre)
	setSpeed(0);
}

void MotorVNH5019::updateCurrent() {
	AnalogReader &reader = AnalogReader::getInstance();
	// courant (mA)
	current = ADC_TO_MILLIAMPERES(reader.getValue(config.analogChannel));
	systime_t now = chVTGetSystemTime();
	// Calcul de la consommation moyenne depuis la derni�re mesure (mA.s = mC)
	charge += 1e-6 * ST2US(now - timestamp) * current;
	timestamp = now;
}

int MotorVNH5019::getCharge() const {
	return (int) charge;
}

int MotorVNH5019::getCurrent() const {
	return current;
}
