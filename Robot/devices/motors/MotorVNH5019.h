/*
 * MotorVNH5019.h
 *
 *  Created on: 23 ao�t 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_MOTORS_MOTORVNH5019_H_
#define DEVICES_MOTORS_MOTORVNH5019_H_

#include "ch.h"
#include "hal.h"
#include "hal_pwm.h"

#include "devices/io/AnalogReader.h"
#include "devices/motors/Motor.h"

#define MILLIVOLTS_PER_AMPERE 140
#define ADC_RATIO ((ADC_VOLTAGE_REF * 1000) / MILLIVOLTS_PER_AMPERE)
#define ADC_TO_MILLIAMPERES(x) ((x * ADC_RATIO) >> ADC_RESOLUTION)

/**
 * \brief Configuration du p�riph�rique VNH5019
 *
 * Structure d�finissant les param�tre de configuration du p�riph�rique VNH5019.
 */
struct MotorVNH5019Config {
	/** Pilote PWM */
	PWMDriver *pwmDriver;
	/** Configuration du pilote PWM */
	PWMConfig *pwmConfig;
	/** Canal PWM */
	uint8_t pwmChannel;
	/** Port de la commande PWM */
	stm32_gpio_t *portPwm;
	/** Indice dans le port de la commande PWM */
	uint8_t padPwm;
	/** Mode de fonctionnement de la commande PWM */
	uint8_t modePwm;
	/** Port de la commande de direction A */
	stm32_gpio_t *portDirectionA;
	/** Indice dans le port de la commande de direction A */
	uint8_t padDirectionA;
	/** Port de la commande de direction B */
	stm32_gpio_t *portDirectionB;
	/** Indice dans le port de la commande de direction B */
	uint8_t padDirectionB;
	/** Port de la commande d'activation */
	stm32_gpio_t *portEnable;
	/** Indice dans le port de la commande d'activation */
	uint8_t padEnable;
	/** Port de l'acquisition analogique */
	stm32_gpio_t *portAnalog;
	/** Indice dans le port de l'acquisition analogique */
	uint8_t padAnalog;
	/** Canal analogique de la sonde de courant */
	int analogChannel;
	/** Valeur interne de la position neutre du moteur. */
	unsigned int neutralValue;
	/** Drapeau indiquant si le sens de rotation est invers�. */
	bool reversed;
};

/**
 * \brief Moteur VNH5019
 *
 * Impl�mentation de la classe Motor sp�cifique au composant VNH5019.
 */
class MotorVNH5019 : public Motor {
private:
	MotorVNH5019Config &config;
	float charge = 0;
	int current = 0;
	systime_t timestamp = 0;
	void updateCurrent();
public:
	/**
	 * Constructeur.
	 * @param config [in] param�tre de configuration
	 */
	MotorVNH5019(MotorVNH5019Config &config);
	/**
	 * \brief Initialisation
	 *
	 * Initialise l'objet � partir de la configuration.
	 */
	void initialize();
	virtual void brake();
	virtual int getCharge() const;
	virtual int getCurrent() const;
	virtual void setEnabled(bool value);
	virtual void setSpeed(int value);
};

#endif /* DEVICES_MOTORS_MOTORVNH5019_H_ */
