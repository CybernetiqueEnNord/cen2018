/*
 * MatchTimer.cpp
 *
 *  Created on: 27 oct. 2017
 *      Author: Emmanuel
 */

#include "MatchTimer.h"

MatchTimer::MatchTimer() {
	chVTObjectInit(&timer);
	durationMS = 0;
}

void MatchTimer::initialize(int durationMS) {
	this->durationMS = durationMS;
}

void MatchTimer::start() {
	this->startTimestamp = chVTGetSystemTime();
	chVTSet(&timer, MS2ST(durationMS), onTimer, this);
}

int MatchTimer::getElapsedTimeMS() const {
	if (startTimestamp == 0) {
		return 0;
	}
	systime_t now = chVTGetSystemTime();
	int elapsed = ST2MS(now - startTimestamp);
	return elapsed;
}

int MatchTimer::getRemainingTimeMS() const {
	int remaining = durationMS - getElapsedTimeMS();
	if (remaining < 0) {
		remaining = 0;
	}
	return remaining;
}

void MatchTimer::onTimeElapsed() {
	if (listener != NULL) {
		listener->onMatchTimeElapsed(*this);
	}
}
