/*
 * MatchTimer.h
 *
 *  Created on: 27 oct. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_TIMER_MATCHTIMER_H_
#define DEVICES_TIMER_MATCHTIMER_H_

#include "ch.h"

class MatchTimer;

class MatchTimerListener {
	friend class MatchTimer;

protected:
	virtual void onMatchTimeElapsed(const MatchTimer &timer) = 0;
};

class MatchTimer {
private:
	systime_t startTimestamp = 0;
	int durationMS = 0;
	virtual_timer_t timer;

	static void onTimer(void *matchTimer) {static_cast<MatchTimer *>(matchTimer)->onTimeElapsed();}

protected:
	virtual void onTimeElapsed();

public:
	MatchTimerListener *listener = NULL;

	MatchTimer();
	void initialize(int durationMS);
	void start();
	int getElapsedTimeMS() const;
	int getRemainingTimeMS() const;
};

#endif /* DEVICES_TIMER_MATCHTIMER_H_ */
