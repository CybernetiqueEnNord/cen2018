/*
 * LCDBigFont.h
 *
 *  Created on: 4 nov. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_LCD_LCDBIGFONT_H_
#define DEVICES_LCD_LCDBIGFONT_H_

#include "devices/lcd/LCD.h"
#include "devices/lcd/Print.h"

class LCDBigFont : public Print {
private:
	static const uint8_t customChars[][8];
	static const uint8_t bigChars[][8];

	LCD &lcd;
	int col = 0, row = 0;
	bool extraSpace = false;

protected:
    virtual size_t write(uint8_t);

public:
	LCDBigFont(LCD &lcd);
	void setCursor(int x, int y);
	void setExtraSpace(bool value);
};

#endif /* DEVICES_LCD_LCDBIGFONT_H_ */
