/*
 * LCDQY2004A.cpp
 *
 *  Created on: 24 oct. 2017
 *      Author: Emmanuel
 */

#include "devices/lcd/LCDQY2004A.h"

LCD_QY2004A::LCD_QY2004A() :
		LCD_QY2004A(0x27) {
}

LCD_QY2004A::LCD_QY2004A(uint8_t i2cAddress) :
		LiquidCrystal_I2C(i2cAddress, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE) {
}

void LCD_QY2004A::begin() {
	LiquidCrystal_I2C::begin(20, 4);
}
