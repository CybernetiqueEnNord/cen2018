/*
 * LCDQY2004A.h
 *
 *  Created on: 24 oct. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_LCD_LCDQY2004A_H_
#define DEVICES_LCD_LCDQY2004A_H_

#include "LiquidCrystal_I2C.h"

class LCD_QY2004A : public LiquidCrystal_I2C {
public:
	LCD_QY2004A();
	LCD_QY2004A(uint8_t i2cAddress);
	void begin();
};

#endif /* DEVICES_LCD_LCDQY2004A_H_ */
