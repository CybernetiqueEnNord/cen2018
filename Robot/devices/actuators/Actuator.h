/*
 * Actuator.h
 *
 *  Created on: 22 avr. 2018
 *      Author: Emmanuel
 */

#ifndef DEVICES_ACTUATORS_ACTUATOR_H_
#define DEVICES_ACTUATORS_ACTUATOR_H_

/**
 * Classe abstraite de base des objets actionneurs.
 */
class Actuator {
public:
	/**
	 * D�termine si l'actionneur est pr�t.
	 * @return true si l'actionneur est pr�t, false s'il est occup�
	 */
	virtual bool isReady() const = 0;
	/**
	 * \brief Attente de l'actionneur
	 *
	 * Attends que l'actionneur soit pr�t, au maximum le temps sp�cifi�.
	 * @param maxMilliseconds le nombre maximum de millisondes � attendre l'actionneur
	 * @return true si l'actionneur est pr�t, false si le temps maximum a �t� atteint
	 *  sans que l'actionneur soit pr�t
	 */
	bool waitForReady(unsigned int maxMilliseconds) const;
};

#endif /* DEVICES_ACTUATORS_ACTUATOR_H_ */
