/*
 * I2CActuator.cpp
 *
 *  Created on: 8 mai 2018
 *      Author: Emmanuel
 */

#include "devices/actuators/I2CActuator.h"

#include "../Commons/i2c/CommonCommands.h"
#include <cstring>

I2CActuator::I2CActuator(I2CDeviceConfig &config) :
		I2CDevice(config) {
}

bool I2CActuator::isPresent() const {
	i2cAcquireBus();
	bool result = isDevicePresent();
	i2cReleaseBus();
	return result;
}

bool I2CActuator::isReady() const {
	uint8_t command = I2C_COMMAND_STATUS;
	uint8_t in;
	i2cAcquireBus();
	bool result = i2cWrite(&command, sizeof(command), &in, sizeof(in));
	i2cReleaseBus();
	result &= in == I2C_STATUS_READY;
	return result;
}

bool I2CActuator::kill() const {
	bool result = false;
	int retry = 10;
	while (retry > 0) {
		result |= i2cSendCommand(I2C_COMMAND_KILL, false);
		retry--;
	}
	return result;
}

bool I2CActuator::i2cSendCommand(uint8_t command, bool waitBusy) const {
	uint8_t in;
	bool result = false;
	do {
		i2cAcquireBus();
		result = i2cWrite(&command, sizeof(command), &in, sizeof(in));
		i2cReleaseBus();
		chThdSleepMilliseconds(10);
		result &= !isReady();
	} while (!result && waitBusy);
	return result;
}

bool I2CActuator::i2cSendCommand(const char * command, bool waitBusy) const {
	uint8_t in;
	bool result = false;
	do {
		i2cAcquireBus();
		result = i2cWrite((uint8_t*) command, strlen(command), &in, sizeof(in));
		i2cReleaseBus();
		chThdSleepMilliseconds(10);
		result &= !isReady();
	}
	while(!result && waitBusy);
	return result;
}
