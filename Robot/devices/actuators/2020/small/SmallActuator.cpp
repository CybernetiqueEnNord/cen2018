/*
 * smallActuator.cpp
 *
 *  Created on: 20 janv. 2019
 *      Author: emmanuel
 */

#include "../small/SmallActuator.h"

SmallActuatorConfig::SmallActuatorConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address) :
		I2CDeviceConfig(i2cDriver, i2cConfig, address) {
}

SmallActuator::SmallActuator(SmallActuatorConfig &config) :
		I2CActuator(config) {

}

bool SmallActuator::setMatchSide(bool reversed) {
	matchSide = reversed;
	bool result = i2cSendCommand(reversed ? I2C_COMMAND_SET_COLOR_YELLOW : I2C_COMMAND_SET_COLOR_BLUE);
	return result;
}

bool SmallActuator::startPosition() {
	bool result = i2cSendCommand(I2C_COMMAND_START_POSITION, true);
	result &= waitForReady(3000);
	return result;
}

bool SmallActuator::testAllServoAreWorking() {
	bool result = i2cSendCommand(I2C_COMMAND_TEST_ALL_SERVO_ARE_WORKING, true);
	result &= waitForReady(10000);
	return result;
}

bool SmallActuator::setMancheAAirArm(bool out) {
	char command[3];
	command[0] = COMMAND_SET_ARM;
	command[1] = (matchSide ? '0' : '1'); //0 is arm left, 1 is arm right, 2 is arm front,3 is rear.
	command[2] = (out ? '0' + static_cast<char>(ServoPosition::Down) : '0' + static_cast<char>(ServoPosition::Up));
	bool result = i2cSendCommand(command);
	return result;
}

bool SmallActuator::setPhareArm(bool out) {
	char command[3];
	command[0] = COMMAND_SET_ARM;
	command[1] = (matchSide ? '0' : '1'); //0 is arm left, 1 is arm right, 2 is arm front,3 is rear.
	command[2] = (out ? '0' + static_cast<char>(ServoPosition::Down) : '0' + static_cast<char>(ServoPosition::Up));
	bool result = i2cSendCommand(command);
	return result;
}

bool SmallActuator::setArmLeft_asBlue(ServoPosition position) {
	char command[3];
	command[0] = COMMAND_SET_ARM;
	command[1] = (matchSide ? '1' : '0'); //0 is arm left, 1 is arm right, 2 is arm front,3 is rear.
	command[2] = ('0' + static_cast<char>(position));
	bool result = i2cSendCommand(command);
	result &= waitForReady(2000);
	return result;
}
bool SmallActuator::setArmRight_asBlue(ServoPosition position) {
	char command[3];
	command[0] = COMMAND_SET_ARM;
	command[1] = (matchSide ? '0' : '1'); //0 is arm left, 1 is arm right, 2 is arm front,3 is rear.
	command[2] = ('0' + static_cast<char>(position));
	bool result = i2cSendCommand(command);
	result &= waitForReady(2000);
	return result;
}

bool SmallActuator::setPumpRearRight_asBlue(bool on) {

	char command[3];
	command[0] = COMMAND_SET_PUMP;
	command[1] = (matchSide ? '0' : '1');
	command[2] = (on ? '1' : '0');
	return i2cSendCommand(command);
}

bool SmallActuator::setPumpRearLeft_asBlue(bool on) {

	char command[3];
	command[0] = COMMAND_SET_PUMP;
	command[1] = (matchSide ? '1' : '0');
	command[2] = (on ? '1' : '0');
	bool result = i2cSendCommand(command, true);
	return result;
}


bool SmallActuator::setPumpFrontRight_asBlue(bool on) {
	char command[3];
	command[0] = COMMAND_SET_PUMP;
	command[1] = (matchSide ? '5' : '4'); //0:AR Gauche, 1:AR Droit, 2: Centre Gauche, 3: Centre Droit, 4: Avant Droit, 5: Avant Gauche, 6: Ariere centre
	command[2] = (on ? '1' : '0');
	return i2cSendCommand(command);
}

bool SmallActuator::setPumpFrontLeft_asBlue(bool on) {
	char command[3];
	command[0] = COMMAND_SET_PUMP;
	command[1] = (matchSide ? '4' : '5'); //0:AR Gauche, 1:AR Droit, 2: Centre Gauche, 3: Centre Droit, 4: Avant Droit, 5: Avant Gauche, 6: Ariere centre
	command[2] = (on ? '1' : '0');
	bool result = i2cSendCommand(command, true);
	return result;
}

bool SmallActuator::setPumpRearTrio(bool on) {

	char command[3];
	command[0] = COMMAND_SET_PUMP;
	command[1] = '2'; //0:AR Gauche, 1:AR Droit, 2: Centre Gauche, 3: Centre Droit, 4: Avant Droit, 5: Avant Gauche, 6: Ariere centre
	command[2] = (on ? '1' : '0');
	bool result = i2cSendCommand(command, true);
	result &= waitForReady(2000);

	command[0] = COMMAND_SET_PUMP;
	command[1] = '6'; //0:AR Gauche, 1:AR Droit, 2: Centre Gauche, 3: Centre Droit, 4: Avant Droit, 5: Avant Gauche, 6: Ariere centre
	command[2] = (on ? '1' : '0');
	result &= i2cSendCommand(command, true);
	result &= waitForReady(2000);

	command[0] = COMMAND_SET_PUMP;
	command[1] = '3'; //0:AR Gauche, 1:AR Droit, 2: Centre Gauche, 3: Centre Droit, 4: Avant Droit, 5: Avant Gauche, 6: Ariere centre
	command[2] = (on ? '1' : '0');
	result &= i2cSendCommand(command, true);
	result &= waitForReady(2000);
	return result;
}

bool SmallActuator::setPumpTrioRearLeft_asBlue(bool on) {

	char command[3];
	command[0] = COMMAND_SET_PUMP;
	command[1] = (matchSide ? '3' : '2'); //0:AR Gauche, 1:AR Droit, 2: Centre Gauche, 3: Centre Droit, 4: Avant Droit, 5: Avant Gauche, 6: Ariere centre
	command[2] = (on ? '1' : '0');
	bool result = i2cSendCommand(command, true);
	result &= waitForReady(2000);
	return result;
}

bool SmallActuator::setPumpTrioRearRight_asBlue(bool on) {
	char command[3];
	command[0] = COMMAND_SET_PUMP;
	command[1] = (matchSide ? '2' : '3'); //0:AR Gauche, 1:AR Droit, 2: Centre Gauche, 3: Centre Droit, 4: Avant Droit, 5: Avant Gauche, 6: Ariere centre
	command[2] = (on ? '1' : '0');
	bool result = i2cSendCommand(command, true);
	result &= waitForReady(2000);
	return result;
}

bool SmallActuator::setPumpRearCenter(bool on) {
	char command[3];
	command[0] = COMMAND_SET_PUMP;
	command[1] = '6'; //0:AR Gauche, 1:AR Droit, 2: Centre Gauche, 3: Centre Droit, 4: Avant Droit, 5: Avant Gauche, 6: Ariere centre
	command[2] = (on ? '1' : '0');
	bool result = i2cSendCommand(command, true);
	result &= waitForReady(2000);
	return result;
}

bool SmallActuator::setFrontLeftLifter_asBlue(ServoPosition pos) {
	char command[3];
	command[0] = COMMAND_SET_ARM;
	command[1] = (matchSide ? '2' : '3');
	command[2] = '0' + static_cast<char>(pos);
	bool result = i2cSendCommand(command, true);
	result &= waitForReady(2000);
	return result;
}

bool SmallActuator::setFrontRightLifter_asBlue(ServoPosition pos) {

	char command[3];
	command[0] = COMMAND_SET_ARM;
	command[1] = (matchSide ? '3' : '2');
	command[2] = '0' + static_cast<char>(pos);
	bool result = i2cSendCommand(command, true);
	result &= waitForReady(3000);
	return result;
}

bool SmallActuator::setFrontBothLifter(ServoPosition pos) {

	char command[2];
	command[0] = COMMAND_SET_BOTH_FRONT_ARM;
	command[1] = '0' + static_cast<char>(pos);
	bool result = i2cSendCommand(command, true);
	result &= waitForReady(3000);
	return result;
}

bool SmallActuator::setRearLifter(ServoPosition pos) {

	char command[3];
	command[0] = COMMAND_SET_ARM;
	command[1] = '4';
	command[2] = '0' + static_cast<char>(pos);
	bool result = i2cSendCommand(command, true);
	result &= waitForReady(3000);
	return result;
}

bool SmallActuator::raiseFlags() {
	return i2cSendCommand(COMMAND_FLAGS);
}

