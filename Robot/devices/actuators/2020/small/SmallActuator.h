/*
 * SuctionActuator.h
 *
 *  Created on: 20 janv. 2020
 *      Author: emmanuel
 */

#ifndef DEVICES_ACTUATORS_2020_SMALL_SMALLACTUATOR_H_
#define DEVICES_ACTUATORS_2020_SMALL_SMALLACTUATOR_H_

#include "devices/actuators/I2CActuator.h"
#include "devices/i2c/I2CDevice.h"
#include "../Commons/i2c/ActuatorSmall2020Commands.h"

struct SmallActuatorConfig : I2CDeviceConfig {
	SmallActuatorConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address);
};

class SmallActuator : public I2CActuator {
private:
	bool matchSide=0;
public:
	SmallActuator(SmallActuatorConfig &config);
	bool setMatchSide(bool reversed);
	bool startPosition();
	bool testAllServoAreWorking();
	bool setMancheAAirArm(bool out);
	bool setPhareArm(bool out);
	bool setPumpFrontRight_asBlue(bool on);
	bool setPumpFrontLeft_asBlue(bool on);
	bool setPumpRearRight_asBlue(bool on);
	bool setPumpRearLeft_asBlue(bool on);
	bool setPumpTrioRearRight_asBlue(bool on);
	bool setPumpTrioRearLeft_asBlue(bool on);
	bool setPumpRearTrio(bool on);
	bool setPumpRearCenter(bool on);
	bool setFrontLeftLifter_asBlue(ServoPosition position);
	bool setFrontRightLifter_asBlue(ServoPosition position);
	bool setFrontBothLifter(ServoPosition position);
	bool setRearLifter(ServoPosition position);
	bool setArmLeft_asBlue(ServoPosition position);
	bool setArmRight_asBlue(ServoPosition position);
	bool raiseFlags();

};

#endif /* DEVICES_ACTUATORS_2020_SMALL_SMALLACTUATOR_H_ */
