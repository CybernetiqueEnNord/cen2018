/*
 * ActuatorsTest.cpp
 *
 *  Created on: 10 d�c. 2018
 *      Author: Emmanuel
 */

#include "ActuatorsTest.h"

#include "devices/host/HostCapabilities2020.h"
#include "devices/host/HostDevice.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/actuators/2020/small/SmallActuator.h"

void ActuatorsTest::run(Console &console, int &step) {
	const HostData &data = HostDevice::getHostData();
	unsigned int capabilities = data.getCapabilities();

	if ((capabilities & CAPABILITY_SMALL) != 0) {
		testSmallActuator(console, step);
	}
	if ((capabilities & CAPABILITY_BIG) != 0) {
		testBigActuator(console, step);
	}
}

bool ActuatorsTest::checkActuators(int capabilities) {
	if ((capabilities & CAPABILITY_SMALL) != 0) {
		// TODO
//		extern SmallActuator smallActuator;
//		if (!smallActuator.isPresent()) {
//			return false;
//		}
	}
	if ((capabilities & CAPABILITY_BIG) != 0) {
		// TODO
//		extern BigActuator bigActuator;
//		if (!bigActuator.isPresent()) {
//			return false;
//		}
	}
	return true;
}

void ActuatorsTest::testBigActuator(Console &console, int &step) {
	(void) console;
	(void) step;
	// TODO
}

void ActuatorsTest::testSmallActuator(Console &console, int &step) {
	(void) console;
	(void) step;
	// TODO
}
