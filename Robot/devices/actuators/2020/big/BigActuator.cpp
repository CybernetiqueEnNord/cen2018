/*
 * smallActuator.cpp
 *
 *  Created on: 20 janv. 2020
 *      Author: emmanuel
 */

#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/actuators/I2CActuator.h"
#include "devices/io/Console.h"

#include "../Commons/i2c/ActuatorBig2020Commands.h"
#include "match/2020/Match2020Enums.h"
#include "devices/i2c/I2CDevice.h"

BigActuatorConfig::BigActuatorConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address) :
		I2CDeviceConfig(i2cDriver, i2cConfig, address) {

}

bool BigActuator::setMatchSide(bool reversed) {
	matchSide = reversed;
	bool result = i2cSendCommand(reversed ? I2C_COMMAND_SET_COLOR_YELLOW : I2C_COMMAND_SET_COLOR_BLUE, true);
	return result;
}

BigActuator::BigActuator(BigActuatorConfig &config) :
		I2CActuator(config) {
}

bool BigActuator::startPosition() {
	bool result = i2cSendCommand(I2C_COMMAND_START_POSITION, true);
	result &= waitForReady(3000);
	return result;
}

bool BigActuator::testAllServoAreWorking() {
	bool result = i2cSendCommand(I2C_COMMAND_TEST_ALL_SERVO_ARE_WORKING, true);
	result &= waitForReady(10000);
	return result;
}

bool BigActuator::setCode(Code code) {
	this->code = code;
	char command[4] = { 'r', ';', 0, 0 };
	command[2] = static_cast<char>(this->code) + '0';
	bool result = i2cSendCommand(command, true);
	result &= waitForReady(10000);
	return result;
}

Code BigActuator::getCode() {
	return this->code;
}

int BigActuator::computeOffset(FrontRear frontOrRear, FirstOrComplement firstOrComplement) {

	bool blue = false;
	bool yellow = true;

	switch (this->code) {
		case Code::RVRVR:
			return 0;
			break;

		case Code::VVVRR_VVRRR_3:
			// normal -> bleu, reversed => yellow
			if (this->matchSide == blue) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue FirstOponent=Right side of the field: 		_VVRRR
						//								servo index front :  	  		 43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						return 0;
					} else {
						//VVVRR_VVRRR_3 Blue FirstOponent=Right side of the field: 		_VVRRR
						//								servo index rear :  	 		-98765
						//blue + rear, we take the green
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue ComplementOurSide=Left side of the field: 	 VVVRR_
						//								    servo index front :             43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						return 2;
					} else {
						//VVVRR_VVRRR_3 Blue FirstOponent=Left side of the field: 		   VVVRR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						return -2;
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue FirstOurSide=Left side of the field: 	     VVVRR_
						//								    servo index front :          43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						return 0;
					} else {
						//VVVRR_VVRRR_3 Blue FirstOurSide=Left side of the field: 		 VVVRR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue ComplementOponent=Right side of the field: 	_VVRRR
						//									servo index front :  	  		   43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						return 2;
					} else {
						//VVVRR_VVRRR_3 Blue ComplementOponent=Right side of the field: 	   _VVRRR
						//									servo index rear :  	 		-98765
						//blue + rear, we take the green
						return -2;
					}
				}
			} else if (this->matchSide == yellow) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Left side of the field: 		VVVRR_
						//										servo index front :  	43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						return 0;
					} else {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Left side of the field: 		 VVVRR_
						//										servo index rear :  	-98765
						//yellow + rear, we take the red
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Right side of the field: 		    _VVRRR
						//								servo index front :  	  		  43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						return +2;
					} else {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Right side of the field: 		_VVRRR
						//								servo index front : 	      	  -98765
						//yellow + rear, we take the red
						return -2;
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow FirstOurSide=Right side of the field: 		 _VVRRR
						//								servo index front :  	  		  43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						return 0;
					} else {
						//VVVRR_VVRRR_3 Yellow FirstOurSide=Right side of the field: 		_VVRRR
						//								servo index front : 	      	-98765
						//yellow + rear, we take the red
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow ComplementOponent=Left side of the field: 		VVVRR_
						//										servo index front :  	  43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						return 2;
					} else {
						//VVVRR_VVRRR_3 Yellow ComplementOponent=Left side of the field: 		 VVVRR_
						//											servo index rear :  	   -98765
						//yellow + rear, we take the red
						return -2;
					}
				}
			}
			break;
		case Code::VVRVR_VRVRR_2:

			if (this->matchSide == blue) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Blue FirstOponent=Right side of the field: 		_VRVRR
						//								servo index front :  	  		 43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						return 0;
					} else {
						//VVRVR_VRVRR_2 Blue FirstOponent=Right side of the field: 		  _VRVRR
						//								servo index rear :  	 		-98765
						//blue + rear, we take the green
						return -2;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Blue ComplementOurSide=Left side of the field: 	  VVRVR_
						//								    servo index front :             43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						return 2;
					} else {
						//VVRVR_VRVRR_2 Blue FirstOponent=Left side of the field: 	     VVRVR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Blue FirstOurSide=Left side of the field: 	     VVRVR_
						//								    servo index front :            43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						return 2;
					} else {
						//VVRVR_VRVRR_2 Blue FirstOurSide=Left side of the field: 		 VVRVR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Blue ComplementOponent=Right side of the field: 	  _VRVRR
						//								servo index front :  	  			   43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						return 0;
					} else {
						//VVRVR_VRVRR_2 Blue ComplementOponent=Right side of the field: 	   _VRVRR
						//								servo index rear :  	 		 	 -98765
						//blue + rear, we take the green
						return -2;
					}
				}
			} else if (this->matchSide == yellow) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Yellow FirstOponent=Left side of the field: 		VVRVR_
						//										servo index front :  		43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						return 0;
					} else {
						//VVRVR_VRVRR_2 Yellow FirstOponent=Left side of the field: 		 VVRVR_
						//										servo index rear :  	  	  -98765
						//yellow + rear, we take the red
						return -2;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Yellow FirstOponent=Right side of the field: 		    _VRVRR
						//								servo index front :  	  		   	   43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						return 2;
					} else {
						//VVRVR_VRVRR_2 Yellow FirstOponent=Right side of the field: 		 _VRVRR
						//								servo index front : 	      	 	 -98765
						//yellow + rear, we take the red
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Yellow FirstOurSide=Right side of the field: 		 _VRVRR
						//								servo index front :  	  			43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						return 2;
					} else {
						//VVRVR_VRVRR_2 Yellow FirstOurSide=Right side of the field: 		_VRVRR
						//								servo index front : 	      		-98765
						//yellow + rear, we take the red
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Yellow ComplementOponent=Left side of the field: 		VVRVR_
						//										servo index front :  	    	43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						return 0;
					} else {
						//VVRVR_VRVRR_2 Yellow ComplementOponent=Left side of the field: 		  VVRVR_
						//											servo index rear :  	   	   -98765
						//yellow + rear, we take the red
						return -2;
					}
				}
			}

			break;

		case Code::VRVVR_VRRVR_1:

			if (this->matchSide == blue) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Blue FirstOponent=Right side of the field: 		_VRRVR
						//								servo index front :  	  		 43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						return 0;
					} else {
						//VRVVR_VRRVR_1 Blue FirstOponent=Right side of the field: 		  _VRRVR
						//								servo index rear :  	 		 -98765
						//blue + rear, we take the green
						return -1;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Blue ComplementOurSide=Left side of the field: 	  VRVVR_
						//								    servo index front :            43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						return 1;
					} else {
						//VRVVR_VRRVR_1 Blue FirstOponent=Left side of the field: 	     VRVVR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Blue FirstOurSide=Left side of the field: 	     VRVVR_
						//								    servo index front :           43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						return 1;
					} else {
						//VRVVR_VRRVR_1 Blue FirstOurSide=Left side of the field: 		 VRVVR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Blue ComplementOponent=Right side of the field: 	  _VRRVR
						//								servo index front :  	  		   	   43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						return 0;
					} else {
						//VRVVR_VRRVR_1 Blue ComplementOponent=Right side of the field: 	   _VRRVR
						//								servo index rear :  	 		  	  -98765
						//blue + rear, we take the green
						return -1;
					}
				}
			} else if (this->matchSide == yellow) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Yellow FirstOponent=Left side of the field: 		VRVVR_
						//										servo index front :  	    43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						return 0;
					} else {
						//VRVVR_VRRVR_1 Yellow FirstOponent=Left side of the field: 		 VRVVR_
						//										servo index rear :  	     -98765
						//yellow + rear, we take the red
						return -1;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Yellow FirstOponent=Right side of the field: 		    _VRRVR
						//								servo index front :  	  		        43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						return 1;
					} else {
						//VRVVR_VRRVR_1 Yellow FirstOponent=Right side of the field: 		 _VRRVR
						//								servo index front : 	      	     -98765
						//yellow + rear, we take the red
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Yellow FirstOurSide=Right side of the field: 		 _VRRVR
						//								servo index front :  	  		     43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						return 1;
					} else {
						//VRVVR_VRRVR_1 Yellow FirstOurSide=Right side of the field: 		_VRRVR
						//								servo index front : 	      	    -98765
						//yellow + rear, we take the red
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Yellow ComplementOponent=Left side of the field: 		VRVVR_
						//										servo index front :  	        43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						return 0;
					} else {
						//VRVVR_VRRVR_1 Yellow ComplementOponent=Left side of the field: 		  VRVVR_
						//											servo index rear :  	      -98765
						//yellow + rear, we take the red
						return -1;
					}
				} else {
					//ERROR
					return 0;
				}
			}
			break;

		default:
			if (this->matchSide == blue) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue FirstOponent=Right side of the field:      _VRRRR
						//                              servo index front :              43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la d�pose)
						return 0;
					} else {
						//VVVRR_VVRRR_3 Blue FirstOponent=Right side of the field:      _VRRRR
						//                              servo index rear :              -98765
						//blue + rear, we take the green
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue ComplementOurSide=Left side of the field:        VVVVR_
						//                                  servo index front :              43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la d�pose)
						return 4;
					} else {
						//VVVRR_VVRRR_3 Blue FirstOponent=Left side of the field:          VVVVR_
						//                                  servo index rear :           -98765
						//blue + rear, we take the green
						return -1;
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue FirstOurSide=Left side of the field:          VVVVR_
						//                                  servo index front :            43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la d�pose)
						return 0;
					} else {
						//VVVRR_VVRRR_3 Blue FirstOurSide=Left side of the field:        VVVVR_
						//                                  servo index rear :          -98765
						//blue + rear, we take the green
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue ComplementOponent=Right side of the field:     _VRRRR
						//                                  servo index front :               43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la d�pose)
						return 1;
					} else {
						//VVVRR_VVRRR_3 Blue ComplementOponent=Right side of the field:    _VRRRR
						//                                  servo index rear :         -98765
						//blue + rear, we take the green
						return 4;
					}
				}
			} else {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Left side of the field:    VVVVR_
						//                                      servo index front :    43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la d�pose)
						return 0;
					} else {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Left side of the field:     VVVVR_
						//                                       servo index rear :    -98765
						//yellow + rear, we take the red
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Right side of the field:      _VRRRR
						//                                       servo index front :   43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la d�pose)
						return 4;
					} else {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Right side of the field:     _VRRRR
						//                                       servo index front :      -98765
						//yellow + rear, we take the red
						return -1;
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow FirstOurSide=Right side of the field:   _VRRRR
						//                                       servo index front :    43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la d�pose)
						return 0;
					} else {
						//VVVRR_VVRRR_3 Yellow FirstOurSide=Right side of the field:    _VRRRR
						//                                       servo index front :    -98765
						//yellow + rear, we take the red
						return 0;
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow ComplementOponent=Left side of the field:    VVVVR_
						//                                           servo index front :   43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la d�pose)
						return -1;
					} else {
						//VVVRR_VVRRR_3 Yellow ComplementOponent=Left side of the field:    VVVVR_
						//                                            servo index rear :       -98765
						//yellow + rear, we take the red
						return 4;
					}
				}
			}
			break;

	}
	return 0;
}

bool BigActuator::readyToTake(FrontRear frontOrRear, FirstOrComplement firstOrComplement) {
	char command[6] = { 't', ';', 0, ';', 0, 0 };
	command[2] = static_cast<char>(frontOrRear) + '0';
	command[4] = static_cast<char>(firstOrComplement) + '0';
	bool result = i2cSendCommand(command, true);
	return result;
}

//Arm 0 to 4 for front, 5 to 9 on rear
bool BigActuator::readyToTakeIndividualArmFront(bool arm0, bool arm1, bool arm2, bool arm3, bool arm4) {
	char command[7];
	command[0] = COMMAND_READY_TO_TAKE_INDIVIDUAL; //0 for front
	command[1] = '0'; //0 for front
	command[2] = (arm0 ? '1' : '0');
	command[3] = (arm1 ? '1' : '0');
	command[4] = (arm2 ? '1' : '0');
	command[5] = (arm3 ? '1' : '0');
	command[6] = (arm4 ? '1' : '0');
	return i2cSendCommand(command, true);
}
//Arm 0 to 4 for front, 5 to 9 on rear
bool BigActuator::readyToTakeIndividualArmRear(bool arm5, bool arm6, bool arm7, bool arm8, bool arm9) {
	char command[7];
	command[0] = COMMAND_READY_TO_TAKE_INDIVIDUAL; //0 for front
	command[1] = '0'; //0 for front
	command[2] = (arm5 ? '1' : '0');
	command[3] = (arm6 ? '1' : '0');
	command[4] = (arm7 ? '1' : '0');
	command[5] = (arm8 ? '1' : '0');
	command[6] = (arm9 ? '1' : '0');
	return i2cSendCommand(command, true);
}

bool BigActuator::hold() {
	i2cSendCommand(COMMAND_HOLD, true);
	return waitForReady(2000);
}

bool BigActuator::raise(bool waitDone) {
	bool result = i2cSendCommand(COMMAND_RAISE, true);
	if (waitDone) {
		result &= waitForReady(2000);
	}
	return result;
}

bool BigActuator::setDownTopBuoyLifter(FrontRear frontOrRear) {
	bool result = false;
	if (frontOrRear == FrontRear::Front) {
		result = i2cSendCommand(COMMAND_FRONT_DOWN, true);
	} else {
		result = i2cSendCommand(COMMAND_REAR_DOWN, true);
	}
	result &= waitForReady(5000);
	return result;
}

bool BigActuator::dance() {
	return i2cSendCommand(I2C_COMMAND_DANCE, true);
}

bool BigActuator::releaseTopBuoyLifter(FrontRear frontOrRear) {
	bool result = false;
	if (frontOrRear == FrontRear::Front) {
		result = i2cSendCommand(COMMAND_FRONT_RELEASE, true);
	} else {
		result = i2cSendCommand(COMMAND_REAR_RELEASE, true);
	}
	result &= waitForReady(5000);
	return result;
}

bool BigActuator::releaseBottomBuoyCatcher(FrontRear frontOrRear) {
	bool result = false;
	if (frontOrRear == FrontRear::Front) {
		result = i2cSendCommand(COMMAND_BOTTOM_BOTTOM_BUOY_FRONT_RELEASE_DISARM_AND_RESET, true);
	} else {
		result = i2cSendCommand(COMMAND_BOTTOM_BOTTOM_BUOY_REAR_RELEASE_DISARM_AND_RESET, true);
	}
	result &= waitForReady(5000);
	return result;
}

bool BigActuator::armBottomBuoyCatcher(FrontRear frontOrRear) {
	bool result = false;
	if (frontOrRear == FrontRear::Front) {
		result = i2cSendCommand(COMMAND_BOTTOM_BOTTOM_BUOY_FRONT_ARM, true);
	} else {
		result = i2cSendCommand(COMMAND_BOTTOM_BOTTOM_BUOY_REAR_ARM, true);
	}
	result &= waitForReady(5000);
	return result;
}

bool BigActuator::setArmEndAtBay(bool out) {
	bool result = i2cSendCommand(out ? COMMAND_LATERAL_ARM_MANCHE_A_AIR_OUT : COMMAND_LATERAL_ARM_MANCHE_A_AIR_RETRACTED, true);
	return result;
}

bool BigActuator::setArmPushBuoy(bool out) {
	bool result = i2cSendCommand(out ? COMMAND_LATERAL_ARM_PUSH_BUOY : COMMAND_LATERAL_ARM_PUSH_BUOY_RETRACTED, true);
	return result;
}

bool BigActuator::setArmLighthouse(bool out) {
	bool result = i2cSendCommand(out ? COMMAND_LATERAL_ARM_PHARE_OUT : COMMAND_LATERAL_ARM_PHARE_RETRACTED, true);
	return result;
}

bool BigActuator::setCleanerArmInBlue(bool out, char arm) {
	ServoPosition position = (out ? ServoPosition::Working : ServoPosition::Up);
	char command[3];
	command[0] = COMMAND_SET_ARM;
	command[1] = arm;
	command[2] = ('0' + static_cast<char>(position));
	bool result = i2cSendCommand(command);
	// pas d'attente, on commence la trajectoire sans.
	return result;
}

bool BigActuator::setCleanerArmRightInBlue(bool out) {
	//1 is arm right
	char arm = matchSide ? '0' : '1';
	return setCleanerArmInBlue(out, arm);
}

bool BigActuator::setCleanerArmLeftInBlue(bool out) {
	//0 is arm left
	char arm = matchSide ? '1' : '0';
	return setCleanerArmInBlue(out, arm);
}

bool BigActuator::raiseFlags() {
	return i2cSendCommand(COMMAND_FLAGS, true);
}

BigBuoySensorData BigActuator::getBuoyData(uint8_t command, int count) {
	uint8_t in[2];
	bool result = false;
	int max = 5;
	do {
		i2cAcquireBus();
		result = i2cWrite(&command, sizeof(command), &in[0], sizeof(in));
		i2cReleaseBus();
	} while (!result && max-- > 0);

	if (!result) {
		in[0] = 0;
		in[1] = 0;
	}
	BigBuoySensorData buoyData(count, in[0], in[1]);
	return buoyData;
}

BigBuoySensorData BigActuator::getTopBuoyData() {
	BigBuoySensorData result = getBuoyData(COMMAND_GET_TOP_BUOY_DATA, 5);
	return result;
}

BigBuoySensorData BigActuator::getBottomBuoyData() {
	BigBuoySensorData result = getBuoyData(COMMAND_GET_BOTTOM_BUOY_DATA, 4);
	return result;
}

bool BigActuator::testWaitForReady() {
	bool result = i2cSendCommand(I2C_COMMAND_10_SECOND_WAIT_READY_TEST, true);
	result &= waitForReady(1000);
	return result;
}
