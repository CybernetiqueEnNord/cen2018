/*
 * Cen2020BigBottomBuoySensorData.cpp
 *
 *  Created on: 7 juin 2020
 *      Author: anastaszor
 */

#include "BigBuoySensorData.h"

BigBuoySensorData::BigBuoySensorData() {
}

BigBuoySensorData::BigBuoySensorData(int count, int front, int back) :
		count(count), front(front), back(back) {
	// nothing to do
}

bool BigBuoySensorData::isPresent(int index) {
	if ((index < 0) || (index >= count * 2))
		return false;
	int mask = (index >= count) ? back : front;
	int offset = (index >= count) ? count : 0;
	bool result = 0 != (mask & (1 << (index - offset)));
	return result;
}

int BigBuoySensorData::countFront() {
	int c = 0;
	for (int i = 0; i < count; i++) {
		if (isPresent(i)) {
			c++;
		}
	}
	return c;
}

int BigBuoySensorData::countBack() {
	int c = 0;
	for (int i = count; i < count * 2; i++) {
		if (isPresent(i)) {
			c++;
		}
	}
	return c;
}
