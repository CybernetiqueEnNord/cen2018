/*
 * SuctionActuator.h
 *
 *  Created on: 20 janv. 2019
 *      Author: emmanuel
 */

#ifndef DEVICES_ACTUATORS_2020_BIG_BIGACTUATOR_H_
#define DEVICES_ACTUATORS_2020_BIG_BIGACTUATOR_H_

#include "./../Commons/i2c/ActuatorBig2020Commands.h"
#include "BigBuoySensorData.h"
#include "devices/actuators/I2CActuator.h"
#include "devices/i2c/I2CDevice.h"
#include "match/2020/Match2020Enums.h"

struct BigActuatorConfig : I2CDeviceConfig {
	BigActuatorConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address);
};

class BigActuator : public I2CActuator {
private:
	Code code=Code::Undefined;
	bool matchSide=0;

	BigBuoySensorData getBuoyData(uint8_t command, int count);
	bool setCleanerArmInBlue(bool out, char arm);

public:
	BigActuator(BigActuatorConfig &config);

	bool setMatchSide(bool reversed);
	bool setCode(Code code);
	Code getCode();

	bool startPosition();
	bool testAllServoAreWorking();

	bool readyToTake(FrontRear frontOrReae, FirstOrComplement firstOrComplement);
	//  -2 to 2, -2 is closer to the starting zone.
	int computeOffset(FrontRear frontOrReae, FirstOrComplement firstOrComplement);

	//Arm 0 to 4 for front, 5 to 9 on rear
	bool readyToTakeIndividualArmFront(bool arm0, bool arm1, bool arm2, bool arm3, bool arm4);
	//Arm 0 to 4 for front, 5 to 9 on rear
	bool readyToTakeIndividualArmRear(bool arm5, bool arm6, bool arm7, bool arm8, bool arm9);
	bool hold();
	bool raise(bool waitDone = true);
	bool dance();
	bool releaseTopBuoyLifter(FrontRear frontOrRear);
	bool setDownTopBuoyLifter(FrontRear frontOrRear);
	bool releaseBottomBuoyCatcher(FrontRear frontOrRear);
	bool armBottomBuoyCatcher(FrontRear frontOrRear);
	bool setArmEndAtBay(bool out);
	bool setArmLighthouse(bool out);
	bool setArmPushBuoy(bool out);
	bool setCleanerArmRightInBlue(bool out);
	bool setCleanerArmLeftInBlue(bool out);
	bool raiseFlags();
	bool testWaitForReady();

	BigBuoySensorData getBottomBuoyData();
	BigBuoySensorData getTopBuoyData();
};

#endif /* DEVICES_ACTUATORS_2020_BIG_BIGACTUATOR_H_ */
