/*
 * Cen2020BigBottomBuoySensorData.h
 *
 *  Created on: 7 juin 2020
 *      Author: anastaszor
 */

#ifndef DEVICES_ACTUATORS_2020_BIG_BIGBUOYSENSORDATA_H_
#define DEVICES_ACTUATORS_2020_BIG_BIGBUOYSENSORDATA_H_

/**
 * Classe qui permet de stocker le r�sultat du sondage des capteurs dans la
 * structure haute des pinces.
 */
class BigBuoySensorData
{
private:
	/**
	 * Nombre de valeurs dans l'ensemble.
	 */
	int count = 0;
	/**
	 * Representation binaire du chargement front. 0x---43210
	 */
	int front = 0;
	/**
	 * Representation binaire du chargement back. 0x---98765
	 */
	int back = 0;

public:

	BigBuoySensorData();

	/**
	 * @param [in] count le nombre d'�l�ments
	 * @param [in] front le masque avant
	 * @param [in] back le masque arri�re
	 */
	BigBuoySensorData(int count, int front, int back);

	/**
	 * Gets whether the sensor at index detects something.
	 * indexes 0..count-1 are front
	 * indexes count..count*2 are back
	 *
	 * @return boolean
	 */
	bool isPresent(int index);

	/**
	 * Counts the number of something present on the front line.
	 *
	 * @return integer
	 */
	int countFront();

	/**
	 * Counts the number of something present on the back line.
	 *
	 * @return integer
	 */
	int countBack();

};

#endif /* DEVICES_ACTUATORS_2020_BIG_BIGBUOYSENSORDATA_H_ */
