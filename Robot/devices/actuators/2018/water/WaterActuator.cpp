/*
 * WaterActuator.cpp
 *
 *  Created on: 21 avr. 2018
 *      Author: Emmanuel
 */

#include "WaterActuator.h"

#include "i2c/ActuatorWater2018Commands.h"

WaterActuatorConfig::WaterActuatorConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address) :
		I2CDeviceConfig(i2cDriver, i2cConfig, address) {
}

WaterActuator::WaterActuator(WaterActuatorConfig &config) :
		I2CActuator(config) {
}

bool WaterActuator::handleBee() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_BEE);
	return result;
}

bool WaterActuator::handleCleanWaterDispenser() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_TAP);
	return result;
}

bool WaterActuator::setMatchSide(bool reversed) {
	bool result = i2cSendCommand(reversed ? I2C_COMMAND_SET_COLOR_ORANGE : I2C_COMMAND_SET_COLOR_GREEN);
	return result;
}

bool WaterActuator::handleOpenDispenser() {
	bool result = i2cSendCommand(I2C_COMMAND_OPEN_DISPENSER);
	return result;
}
