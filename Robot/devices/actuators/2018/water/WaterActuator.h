/*
 * WaterActuator.h
 *
 *  Created on: 21 avr. 2018
 *      Author: Emmanuel
 */

#ifndef DEVICES_ACTUATORS_WATER_WATERACTUATOR_H_
#define DEVICES_ACTUATORS_WATER_WATERACTUATOR_H_

#include "devices/actuators/I2CActuator.h"
#include "devices/i2c/I2CDevice.h"

struct WaterActuatorConfig : I2CDeviceConfig {
	WaterActuatorConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address);
};

class WaterActuator : public I2CActuator {
public:
	WaterActuator(WaterActuatorConfig &config);

	bool handleBee();
	bool handleCleanWaterDispenser();
	bool handleOpenDispenser();
	bool setMatchSide(bool reversed);
};

#endif /* DEVICES_ACTUATORS_WATER_WATERACTUATOR_H_ */
