/*
 * ActuatorsTest.cpp
 *
 *  Created on: 10 d�c. 2018
 *      Author: Emmanuel
 */

#include "ActuatorsTest.h"

#include "devices/actuators/2018/cubes/CubesActuator.h"
#include "devices/actuators/2018/water/WaterActuator.h"
#include "devices/host/HostCapabilities2018.h"
#include "devices/host/HostDevice.h"

void ActuatorsTest::run(Console &console, int &step) {
	const HostData &data = HostDevice::getHostData();
	unsigned int capabilities = data.getCapabilities();

	if ((capabilities & CAPABILITY_CUBES) != 0) {
		testCubesActuator(console, step);
	}

	if ((capabilities & CAPABILITY_WATER) != 0) {
		testWaterActuator(console, step);
	}
}

void ActuatorsTest::testCubesActuator(Console &console, int &step) {
	extern CubesActuator cubesActuator;

	if (step == 0) {
		console.sendData("cubes actuator test");
		if (cubesActuator.isPresent()) {
			console.sendData("present");
			step++;
		} else {
			console.sendData("not present");
		}
		step++;
	}

	switch (step) {
		case 1:
			return;

		case 200:
			console.sendData("test prepare");
			if (!cubesActuator.handleLoadPrepare()) {
				goto error;
			}
			step++;
			break;

		case 202:
			console.sendData("test load lv1");
			if (!cubesActuator.handleLoad1()) {
				goto error;
			}
			step++;
			break;

		case 204:
			console.sendData("test unload");
			if (!cubesActuator.handlePrepareUnLoad1()) {
				goto error;
			}
			step++;
			break;

		case 206:
			step = 1;
			break;

		default:
			if (step > 200) {
				if (cubesActuator.isReady()) {
					console.sendData("ok");
					step++;
				}
			} else {
				step++;
			}
			break;
	}
	return;

	error: console.sendData("failure");
	step = 1;
}

void ActuatorsTest::testWaterActuator(Console &console, int &step) {
	extern WaterActuator waterActuator;

	if (step == 0) {
		console.sendData("water actuator test");
		if (waterActuator.isPresent()) {
			console.sendData("present");
			step++;
		} else {
			console.sendData("not present");
		}
		step++;
	}

	switch (step) {
		case 1:
			return;

		case 200:
			console.sendData("test vert");
			if (!waterActuator.setMatchSide(false)) {
				goto error;
			}
			step++;
			break;

		case 600:
			console.sendData("test orange");
			if (!waterActuator.setMatchSide(true)) {
				goto error;
			}
			step++;
			break;

		case 1000:
			console.sendData("test abeille");
			if (!waterActuator.handleBee()) {
				goto error;
			}
			step++;
			break;

		case 1002:
			console.sendData("test distributeur");
			if (!waterActuator.handleCleanWaterDispenser()) {
				goto error;
			}
			step++;
			break;

		case 1004:
			step = 1;
			break;

		default:
			if (step > 1000) {
				if (waterActuator.isReady()) {
					console.sendData("ok");
					step++;
				}
			} else {
				step++;
			}
			break;
	}
	return;

	error: console.sendData("failure");
	step = 1;
}

bool ActuatorsTest::checkActuators(int capabilities) {
	if ((capabilities & CAPABILITY_WATER) != 0) {
		extern WaterActuator waterActuator;
		if (!waterActuator.isPresent()) {
			return false;
		}
	}

	if ((capabilities & CAPABILITY_CUBES) != 0) {
		extern CubesActuator cubesActuator;
		if (!cubesActuator.isPresent()) {
			return false;
		}
	}

	return true;
}
