/*
 * CubesActuator.cpp
 *
 *  Created on: 21 avr. 2018
 *      Author: Emmanuel
 */

#include "CubesActuator.h"

#include "i2c/ActuatorCubes2018Commands.h"

CubesActuatorConfig::CubesActuatorConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address) :
		I2CDeviceConfig(i2cDriver, i2cConfig, address) {
}

CubesActuator::CubesActuator(CubesActuatorConfig& config) :
		I2CActuator(config) {
}

int CubesActuator::readCombination() {
	uint8_t command = I2C_COMMAND_READ_COMBINATION;
	uint8_t in = 0;
	i2cAcquireBus();
	bool result = i2cWrite(&command, sizeof(command), &in, sizeof(in));
	i2cReleaseBus();
	if (!result) {
		return -1;
	}
	return in;
}

bool CubesActuator::setMatchSide(bool reversed) {
	bool result = i2cSendCommand(reversed ? I2C_COMMAND_SET_COLOR_ORANGE : I2C_COMMAND_SET_COLOR_GREEN);
	return result;
}

bool CubesActuator::setOrientationNorth() {
	bool result = i2cSendCommand(I2C_COMMAND_SET_ORIENTATION_NORTH);
	return result;
}

bool CubesActuator::setOrientationEast() {
	bool result = i2cSendCommand(I2C_COMMAND_SET_ORIENTATION_EAST);
	return result;
}

bool CubesActuator::setOrientationSouth() {
	bool result = i2cSendCommand(I2C_COMMAND_SET_ORIENTATION_SOUTH);
	return result;
}

bool CubesActuator::setOrientationWest() {
	bool result = i2cSendCommand(I2C_COMMAND_SET_ORIENTATION_WEST);
	return result;
}

bool CubesActuator::setInitialPosition() {
	bool result = i2cSendCommand(I2C_COMMAND_SET_INITIAL_POSITION);
	return result;
}

bool CubesActuator::handleLoadJocker() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_LOAD_JOCKER);
	return result;
}

bool CubesActuator::handleUnLoadJocker() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_UNLOAD_JOCKER);
	return result;
}

bool CubesActuator::handleUnLoadJockerLevel2() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_UNLOAD_JOCKER_LEVEL2);
	return result;
}

bool CubesActuator::handleOpen() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_OPEN);
	return result;
}

bool CubesActuator::handleClose() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_CLOSE);
	return result;
}

bool CubesActuator::handleLoadPrepare() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_LOAD_PREPARE);
	return result;
}

bool CubesActuator::handleLoad1() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_LOAD_1);
	return result;
}

bool CubesActuator::handleLoad2() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_LOAD_2);
	return result;
}

bool CubesActuator::handleLoad3() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_LOAD_3);
	return result;
}

bool CubesActuator::handleLoad4() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_LOAD_4);
	return result;
}

bool CubesActuator::handleLoad5() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_LOAD_5);
	return result;
}

bool CubesActuator::handleResetTower() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_RESET_NEW_TOWER);
	return result;
}

bool CubesActuator::handlePrepareUnLoad1() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_PREPARE_UNLOAD_1);
	return result;
}

bool CubesActuator::handlePrepareUnLoad2() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_PREPARE_UNLOAD_2);
	return result;
}

bool CubesActuator::handlePrepareUnLoad3() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_PREPARE_UNLOAD_3);
	return result;
}

bool CubesActuator::handlePrepareUnLoad4() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_PREPARE_UNLOAD_4);
	return result;
}

bool CubesActuator::handlePrepareUnLoad5() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_PREPARE_UNLOAD_5);
	return result;
}

bool CubesActuator::setUnloadCombination(int index) {
	bool result = i2cSendCommand(I2C_COMMAND_SET_UNLOAD_COMBINATION + (index - 1));
	return result;
}

bool CubesActuator::handlePush() {
	bool result = i2cSendCommand(I2C_COMMAND_HANDLE_PUSH);
	return result;
}
