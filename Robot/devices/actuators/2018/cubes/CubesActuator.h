/*
 * CubesActuator.h
 *
 *  Created on: 21 avr. 2018
 *      Author: Emmanuel
 */

#ifndef DEVICES_ACTUATORS_CUBES_CUBESACTUATOR_H_
#define DEVICES_ACTUATORS_CUBES_CUBESACTUATOR_H_

#include "devices/actuators/I2CActuator.h"
#include "devices/i2c/I2CDevice.h"

struct CubesActuatorConfig : I2CDeviceConfig {
	CubesActuatorConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address);
};

class CubesActuator : public I2CActuator {
private:
	int combination = 0;

public:
	CubesActuator(CubesActuatorConfig& config);
	/**
	 * \brief Lecture de la combinaison de couleurs
	 *
	 * Lit et renvoie la combinaison de couleurs.
	 * @return -1 en cas d'�chec de communication, sinon la valeur de la combinaison (0-255)
	 */
	int readCombination();

	bool handleLoadJocker();
	bool handleUnLoadJocker();
	bool handleUnLoadJockerLevel2();
	bool handleOpen();
	bool handleClose();
	bool handleLoadPrepare();
	bool handleLoad1();
	bool handleLoad2();
	bool handleLoad3();
	bool handleLoad4();
	bool handleLoad5();
	bool handleResetTower();
	bool handlePrepareUnLoad1();
	bool handlePrepareUnLoad2();
	bool handlePrepareUnLoad3();
	bool handlePrepareUnLoad4();
	bool handlePrepareUnLoad5();
	bool handlePush();
	bool setInitialPosition();
	bool setMatchSide(bool reversed);
	bool setOrientationNorth();
	bool setOrientationEast();
	bool setOrientationSouth();
	bool setOrientationWest();
	bool setUnloadCombination(int index);
};

#endif /* DEVICES_ACTUATORS_CUBES_CUBESACTUATOR_H_ */
