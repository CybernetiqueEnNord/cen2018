/*
 * SuctionActuator.h
 *
 *  Created on: 20 janv. 2019
 *      Author: emmanuel
 */

#ifndef DEVICES_ACTUATORS_2019_BIG_BIGACTUATOR_H_
#define DEVICES_ACTUATORS_2019_BIG_BIGACTUATOR_H_

#include "devices/actuators/I2CActuator.h"
#include "devices/i2c/I2CDevice.h"

#define TRIO_LEFT_PUMP 0
#define TRIO_CENTER_PUMP 1
#define TRIO_RIGHT_PUMP 2

struct BigActuatorConfig : I2CDeviceConfig {
	BigActuatorConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address);
};

class BigActuator : public I2CActuator {
public:
	BigActuator(BigActuatorConfig &config);
	bool setLifterBack(int value);
	bool setLifterFront(int value);
	bool setPumpBackTrio(bool value);
	bool setPumpBackOff(int pump);
	bool setPumpFrontTrio(bool value);
	bool setPumpFront(int pump);
	bool setPumpFrontOff(int pump);
	bool setFlapClosed();
	bool setFlapPickup();
	bool setFlapSustain();
	bool setMatchSide(bool reversed);
};

#endif /* DEVICES_ACTUATORS_2019_BIG_BIGACTUATOR_H_ */
