/*
 * smallActuator.cpp
 *
 *  Created on: 20 janv. 2019
 *      Author: emmanuel
 */

#include "devices/actuators/2019/big/BigActuator.h"
#include "devices/actuators/I2CActuator.h"

#include "../Commons/i2c/ActuatorBig2019Commands.h"

BigActuatorConfig::BigActuatorConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address) :
		I2CDeviceConfig(i2cDriver, i2cConfig, address) {
}

BigActuator::BigActuator(BigActuatorConfig &config) :
		I2CActuator(config) {
}

bool BigActuator::setPumpBackTrio(bool value) {
	bool result = i2cSendCommand(value ? I2C_COMMAND_TRIO_PUMP_AR_ON : I2C_COMMAND_TRIO_PUMP_AR_OFF, true);
	return result;
}
bool BigActuator::setPumpFrontTrio(bool value) {
	bool result = i2cSendCommand(value ? I2C_COMMAND_TRIO_PUMP_AV_ON : I2C_COMMAND_TRIO_PUMP_AV_OFF, true);
	return result;
}

bool BigActuator::setPumpFrontOff(int pump) {
	bool result = true;
	switch (pump) {
		case TRIO_LEFT_PUMP:
			result = i2cSendCommand(I2C_COMMAND_TRIO_PUMP_OFF_AV_LEFT, true);
			break;
		case TRIO_CENTER_PUMP:
			result = i2cSendCommand(I2C_COMMAND_TRIO_PUMP_OFF_AV_CENTER, true);
			break;
		case TRIO_RIGHT_PUMP:
			result = i2cSendCommand(I2C_COMMAND_TRIO_PUMP_OFF_AV_RIGHT, true);
			break;
	}
	return result;
}

bool BigActuator::setPumpBackOff(int pump) {
	bool result = true;
	switch (pump) {
		case TRIO_LEFT_PUMP:
			result = i2cSendCommand(I2C_COMMAND_TRIO_PUMP_OFF_AR_LEFT, true);
			break;
		case TRIO_CENTER_PUMP:
			result = i2cSendCommand(I2C_COMMAND_TRIO_PUMP_OFF_AR_CENTER, true);
			break;
		case TRIO_RIGHT_PUMP:
			result = i2cSendCommand(I2C_COMMAND_TRIO_PUMP_OFF_AR_RIGHT, true);
			break;
	}
	return result;
}

bool BigActuator::setLifterBack(int value) {
	//TODO add enum
	//4: down
	//3: depose down
	//2: depose center
	//1: depose up
	//0: Up
	bool result = true;
	switch (value) {
		case 0:
			result = i2cSendCommand(I2C_COMMAND_AR_UP, true);
			break;
		case 1:
			result = i2cSendCommand(I2C_COMMAND_AR_DEPOSE_HAUT, true);
			break;
		case 2:
			result = i2cSendCommand(I2C_COMMAND_AR_DEPOSE_CENTER, true);
			break;
		case 3:
			result = i2cSendCommand(I2C_COMMAND_AR_DEPOSE_BAS, true);
			break;
		case 4:
			result = i2cSendCommand(I2C_COMMAND_AR_DOWN, true);
			break;
	}
	return result;
}

bool BigActuator::setLifterFront(int value) {
	//TODO add enum
	//4: down
	//3: depose down
	//2: depose center
	//1: depose up
	//0: Up
	bool result = true;
	switch (value) {
		case 0:
			result = i2cSendCommand(I2C_COMMAND_AV_UP, true);
			break;
		case 1:
			result = i2cSendCommand(I2C_COMMAND_AV_DEPOSE_HAUT, true);
			break;
		case 2:
			result = i2cSendCommand(I2C_COMMAND_AV_DEPOSE_CENTER, true);
			break;
		case 3:
			result = i2cSendCommand(I2C_COMMAND_AV_DEPOSE_BAS, true);
			break;
		case 4:
			result = i2cSendCommand(I2C_COMMAND_AV_DOWN, true);
			break;
	}
	return result;
}

bool BigActuator::setFlapClosed() {
	bool result = i2cSendCommand(I2C_COMMAND_PELLE_RENTREE, true);
	return result;
}

bool BigActuator::setFlapPickup() {
	bool result = i2cSendCommand(I2C_COMMAND_PELLE_PRISE, true);
	return result;
}

bool BigActuator::setFlapSustain() {
	bool result = i2cSendCommand(I2C_COMMAND_PELLE_DEBLAYAGE, true);
	return result;
}

bool BigActuator::setMatchSide(bool reversed) {
	bool result = i2cSendCommand(reversed ? I2C_COMMAND_SET_COLOR_BLUE : I2C_COMMAND_SET_COLOR_YELLOW, true);
	return result;
}
