/*
 * SuctionActuator.h
 *
 *  Created on: 20 janv. 2019
 *      Author: emmanuel
 */

#ifndef DEVICES_ACTUATORS_2019_SMALL_SMALLACTUATOR_H_
#define DEVICES_ACTUATORS_2019_SMALL_SMALLACTUATOR_H_

#include "devices/actuators/I2CActuator.h"
#include "devices/i2c/I2CDevice.h"

#define TRIO_LEFT_PUMP 0
#define TRIO_CENTER_PUMP 1
#define TRIO_RIGHT_PUMP 2

struct SmallActuatorConfig : I2CDeviceConfig {
	SmallActuatorConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address);
};

class SmallActuator : public I2CActuator {
public:
	SmallActuator(SmallActuatorConfig &config);
	bool guideGoldenium(bool value);
	bool setArm(bool value);
	bool setFlapClosed();
	bool setFlapPickup();
	bool setFlapDropGoldenium();
	bool setMatchSide(bool reversed);
	bool setPumpGoldenium(bool value);
	bool setPumpTrio(bool value);
	bool setPumpOff(int pump);
	bool checkGoldenium();
	bool queryGoldeniumDoorOpen();
};

#endif /* DEVICES_ACTUATORS_2019_SMALL_SMALLACTUATOR_H_ */
