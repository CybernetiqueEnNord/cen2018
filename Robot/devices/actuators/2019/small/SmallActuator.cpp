/*
 * smallActuator.cpp
 *
 *  Created on: 20 janv. 2019
 *      Author: emmanuel
 */

#include "../small/SmallActuator.h"

#include "i2c/ActuatorSmall2019Commands.h"

SmallActuatorConfig::SmallActuatorConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address) :
		I2CDeviceConfig(i2cDriver, i2cConfig, address) {
}

SmallActuator::SmallActuator(SmallActuatorConfig &config) :
		I2CActuator(config) {
}

bool SmallActuator::setPumpGoldenium(bool value) {
	bool result = i2cSendCommand(value ? I2C_COMMAND_GOLDENIUM_PUMP_ON : I2C_COMMAND_GOLDENIUM_PUMP_OFF, true);
	return result;
}

bool SmallActuator::setPumpTrio(bool value) {
	bool result = i2cSendCommand(value ? I2C_COMMAND_TRIO_PUMP_ON : I2C_COMMAND_TRIO_PUMP_OFF, true);
	return result;
}

bool SmallActuator::setPumpOff(int pump) {
	bool result = true;
	switch (pump) {
		case TRIO_LEFT_PUMP:
			result = i2cSendCommand(I2C_COMMAND_TRIO_PUMP_OFF_AV_LEFT, true);
			break;
		case TRIO_CENTER_PUMP:
			result = i2cSendCommand(I2C_COMMAND_TRIO_PUMP_OFF_AV_CENTER, true);
			break;
		case TRIO_RIGHT_PUMP:
			result = i2cSendCommand(I2C_COMMAND_TRIO_PUMP_OFF_AV_RIGHT, true);
			break;
	}
	return result;
}

bool SmallActuator::setArm(bool value) {
	bool result = i2cSendCommand(value ? I2C_COMMAND_ARM_DOWN : I2C_COMMAND_ARM_UP, true);
	return result;
}

bool SmallActuator::setFlapClosed() {

	bool result = i2cSendCommand(I2C_COMMAND_PELLE_RENTREE, true);
	return result;
}
bool SmallActuator::setFlapPickup() {

	bool result = i2cSendCommand(I2C_COMMAND_PELLE_PRISE, true);
	return result;
}
bool SmallActuator::setFlapDropGoldenium() {

	bool result = i2cSendCommand(I2C_COMMAND_PELLE_DEPOSE_GOLDENIUM, true);
	return result;
}

bool SmallActuator::setMatchSide(bool reversed) {
	bool result = i2cSendCommand(reversed ? I2C_COMMAND_SET_COLOR_BLUE : I2C_COMMAND_SET_COLOR_YELLOW, true);
	return result;
}

bool SmallActuator::checkGoldenium() {
	bool result = i2cSendCommand(I2C_COMMAND_CHECK_GOLDENIUM, true);
	return result;
}

bool SmallActuator::queryGoldeniumDoorOpen() {
	uint8_t in;
	uint8_t command = COMMAND_GET_BOTTOM_BUOY_DATA;
	bool result = false;
	do {
		i2cAcquireBus();
		result = i2cWrite(&command, sizeof(command), &in, sizeof(in));
		i2cReleaseBus();
	} while (!result);
	bool value = in != 0;
	return value;
}

bool SmallActuator::guideGoldenium(bool value) {
	bool result = i2cSendCommand(value ? I2C_COMMAND_SET_SERVO_GUIDE : I2C_COMMAND_UNSET_SERVO_GUIDE);
	return result;
}
