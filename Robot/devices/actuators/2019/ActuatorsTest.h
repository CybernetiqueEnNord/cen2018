/*
 * ActuatorsTest.h
 *
 *  Created on: 10 d�c. 2018
 *      Author: Emmanuel
 */

#ifndef DEVICES_ACTUATORS_2019_ACTUATORSTEST_H_
#define DEVICES_ACTUATORS_2019_ACTUATORSTEST_H_

#include "devices/io/Console.h"

class ActuatorsTest {
private:
	static void testBigActuator(Console &console, int &step);
	static void testSmallActuator(Console &console, int &step);

public:
	static void run(Console &console, int &step);
	static bool checkActuators(int capabilities);
};

#endif /* DEVICES_ACTUATORS_2018_ACTUATORSTEST_H_ */
