/*
 * ActuatorsTest.cpp
 *
 *  Created on: 10 d�c. 2018
 *      Author: Emmanuel
 */

#include "ActuatorsTest.h"

#include "devices/host/HostCapabilities2019.h"
#include "devices/host/HostDevice.h"
#include "devices/actuators/2019/big/BigActuator.h"
#include "devices/actuators/2019/small/SmallActuator.h"

void ActuatorsTest::run(Console &console, int &step) {
	const HostData &data = HostDevice::getHostData();
	unsigned int capabilities = data.getCapabilities();

	if ((capabilities & CAPABILITY_SMALL) != 0) {
		testSmallActuator(console, step);
	}
	if ((capabilities & CAPABILITY_BIG) != 0) {
		testBigActuator(console, step);
	}
}

bool ActuatorsTest::checkActuators(int capabilities) {
	if ((capabilities & CAPABILITY_SMALL) != 0) {
		extern SmallActuator smallActuator;
		if (!smallActuator.isPresent()) {
			return false;
		}
	}
	if ((capabilities & CAPABILITY_BIG) != 0) {
		extern BigActuator bigActuator;
		if (!bigActuator.isPresent()) {
			return false;
		}
	}
	return true;
}

void ActuatorsTest::testBigActuator(Console &console, int &step) {
	(void) console;
	(void) step;
	// TODO
}

void ActuatorsTest::testSmallActuator(Console &console, int &step) {
	extern SmallActuator smallActuator;

	if (step == 0) {
		console.sendData("small actuator test");
		if (smallActuator.isPresent()) {
			console.sendData("present");
			step++;
		} else {
			console.sendData("not present");
		}
		step++;
	}

	switch (step) {
		case 1:
			return;

		case 200:
			console.sendData("pump on");
			if (!smallActuator.setPumpGoldenium(true)) {
				goto error;
			}
			step++;
			break;

		case 202:
			console.sendData("pump off");
			if (!smallActuator.setPumpGoldenium(false)) {
				goto error;
			}
			step++;
			break;

		case 204:
			console.sendData("arm down");
			if (!smallActuator.setArm(true)) {
				goto error;
			}
			step++;
			break;

		case 206:
			console.sendData("arm up");
			if (!smallActuator.setArm(false)) {
				goto error;
			}
			step++;
			break;

		case 208:
			console.sendData("check goldenium");
			if (!smallActuator.checkGoldenium()) {
				goto error;
			}
			step++;
			break;

		case 210: {
			console.sendData("query goldenium");
			bool value = smallActuator.queryGoldeniumDoorOpen();
			console.sendData(value ? "open" : "closed");
			step++;
			break;
		}

		case 212:
			console.sendData("set color");
			if (!smallActuator.setMatchSide(false)) {
				goto error;
			}
			step++;
			break;

		case 214:
			console.sendData("goldenium guide set");
			if (!smallActuator.guideGoldenium(true)) {
				goto error;
			}
			step++;
			break;

		case 216:
			console.sendData("goldenium guide unset");
			if (!smallActuator.guideGoldenium(false)) {
				goto error;
			}
			step++;
			break;

		case 218:
			console.sendData("set color");
			if (!smallActuator.setMatchSide(true)) {
				goto error;
			}
			step++;
			break;

		case 220:
			console.sendData("goldenium guide set");
			if (!smallActuator.guideGoldenium(true)) {
				goto error;
			}
			step++;
			break;

		case 222:
			console.sendData("goldenium guide unset");
			if (!smallActuator.guideGoldenium(false)) {
				goto error;
			}
			step++;
			break;

		case 224:
			break;

		default:
			if (step > 200) {
				chThdSleepSeconds(1);
				if (smallActuator.isReady()) {
					console.sendData("ok");
					step++;
				}
			} else {
				step++;
			}
			break;
	}
	return;

	error: console.sendData("failure");
	step = 1;
}
