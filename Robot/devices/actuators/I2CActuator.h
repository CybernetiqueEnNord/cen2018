/*
 * I2CActuator.h
 *
 *  Created on: 8 mai 2018
 *      Author: Emmanuel
 */

#ifndef DEVICES_ACTUATORS_I2CACTUATOR_H_
#define DEVICES_ACTUATORS_I2CACTUATOR_H_

#include "Actuator.h"

#include "devices/i2c/I2CDevice.h"
#include <inttypes.h>

class I2CActuator : public Actuator, public I2CDevice {
protected:
	bool i2cSendCommand(uint8_t command, bool waitBusy = true) const;
	bool i2cSendCommand(const char* command, bool waitBusy = true) const;

public:
	I2CActuator(I2CDeviceConfig &config);

	bool isPresent() const;
	virtual bool isReady() const;
	bool kill() const;
};

#endif /* DEVICES_ACTUATORS_I2CACTUATOR_H_ */
