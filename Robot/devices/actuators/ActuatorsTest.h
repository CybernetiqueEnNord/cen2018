/*
 * ActuatorsTest.h
 *
 *  Created on: 10 d�c. 2018
 *      Author: Emmanuel
 */

#ifndef DEVICES_ACTUATORS_ACTUATORSTEST_H_
#define DEVICES_ACTUATORS_ACTUATORSTEST_H_

#if CEN_EDITION == 2018
#include "devices/actuators/2018/ActuatorsTest.h"
#elif CEN_EDITION == 2019
#include "devices/actuators/2019/ActuatorsTest.h"
#elif CEN_EDITION == 2020
#include "devices/actuators/2020/ActuatorsTest.h"
#endif

#endif /* DEVICES_ACTUATORS_ACTUATORSTEST_H_ */
