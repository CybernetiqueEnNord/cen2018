/*
 * Actuator.cpp
 *
 *  Created on: 22 avr. 2018
 *      Author: Emmanuel
 */

#include "Actuator.h"

#include "ch.h"

bool Actuator::waitForReady(unsigned int maxMilliseconds) const {
#ifdef _X86_
	return true;
#endif
	bool result = isReady();
	if (result) {
		return result;
	}
	systime_t time = chVTGetSystemTime();
	time += MS2ST(maxMilliseconds);
	while (!result) {
		chThdSleepMilliseconds(20);
		result = isReady();
		systime_t now = chVTGetSystemTime();
		if (now > time) {
			break;
		}
	}
	return result;
}
