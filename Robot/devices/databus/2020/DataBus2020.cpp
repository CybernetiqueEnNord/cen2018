/*
 * DataBus2020.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: emmanuel
 */

#include "DataBus2020.h"

#include <stdlib.h>

#include "../../../../Commons/databus/DataBusCommands.h"
#include "../../../utils/Text.h"
#include "../../io/Console.h"

DataBus2020::DataBus2020(Console &console) :
		DataBus(console) {
	setTimeout(DATABUS2020_TIMEOUT);
}

bool DataBus2020::queryCombination(Code &combination) {
	bool success = true;
	if (!push || (buoysCombination == Code::Undefined)) {
		success &= query(DATABUS_COMMAND_COMBINATION);
	}
	combination = buoysCombination;
	success &= combination != Code::Undefined;
	return success;
}

bool DataBus2020::queryLightHouse(bool &activated) {
	bool success = true;
	if (!push || (lightHouseStatus == LightHouseStatus::UNKNOWN)) {
		success &= query(DATABUS_COMMAND_LIGHTHOUSE);
	}
	activated = lightHouseStatus == LightHouseStatus::ACTIVE;
	success &= lightHouseStatus != LightHouseStatus::UNKNOWN;
	return success;
}

bool DataBus2020::isPushEnabled() const {
	return push;
}

bool DataBus2020::queryMatchSide(bool &reversed) {
	bool success = true;
	if (!push || (matchSide == MatchSide::UNKNOWN)) {
		success &= query(DATABUS_COMMAND_MATCH_SIDE);
	}
	reversed = matchSide == MatchSide::REVERSED;
	success &= matchSide != MatchSide::UNKNOWN;
	return success;
}

void DataBus2020::handleInput() {
	// Tous les messages font au moins 2 octets
	if (inputDataLength < 2) {
		return;
	}
	switch (inputData[0]) {
		case DATABUS_COMMAND_COMBINATION:
			handleCombination();
			break;

		case DATABUS_COMMAND_ANCHOR_AREA:
			handleAnchorArea();
			break;

		case DATABUS_COMMAND_MATCH_SIDE:
			handleMatchSide();
			break;

		case DATABUS_COMMAND_LIGHTHOUSE:
			handleLightHouse();
			break;

		case DATABUS_COMMAND_ELEMENT_QUERY:
		case DATABUS_COMMAND_ELEMENT_STATE_QUERY:
			handleElementQuery();
			break;
	}
}

void DataBus2020::handleAnchorArea() {
	switch (inputData[1]) {
		case DATABUS_RESULT_ANCHOR_AREA_NORTH:
			anchorArea = Match2020AnchorArea::NORTH;
			break;

		case DATABUS_RESULT_ANCHOR_AREA_SOUTH:
			anchorArea = Match2020AnchorArea::SOUTH;
			break;

		default:
			anchorArea = Match2020AnchorArea::UNKNOWN;
			break;
	}

	extern Console console;
	const char *area = getAreaDescription();
	console.sendMessage("anchor", area);
}

const char* DataBus2020::getAreaDescription() const {
	switch (anchorArea) {
		case Match2020AnchorArea::NORTH:
			return "north";

		case Match2020AnchorArea::SOUTH:
			return "south";

		default:
			return "unknown";
	}
}

const char* DataBus2020::getLighthouseDescription() const {
	switch (lightHouseStatus) {
		case LightHouseStatus::INACTIVE:
			return "inactive";

		case LightHouseStatus::ACTIVE:
			return "active";

		default:
			return "unknown";
	}
}

const char* DataBus2020::getMatchSideDescription() const {
	switch (matchSide) {
		case MatchSide::DIRECT:
			return "bleu";

		case MatchSide::REVERSED:
			return "jaune";

		default:
			return "unknown";
	}
}

void DataBus2020::handleCombination() {
	switch (inputData[1]) {
		case '1' ... '3':
			buoysCombination = static_cast<Code>(inputData[1] - '0');
			break;

		default:
			buoysCombination = Code::Undefined;
			break;
	}
}

void DataBus2020::handleLightHouse() {
	switch (inputData[1]) {
		case DATABUS_RESULT_LIGHTHOUSE_INACTIVE:
			lightHouseStatus = LightHouseStatus::INACTIVE;
			break;

		case DATABUS_RESULT_LIGHTHOUSE_ACTIVE:
			lightHouseStatus = LightHouseStatus::ACTIVE;
			break;

		default:
			lightHouseStatus = LightHouseStatus::UNKNOWN;
			break;
	}

	extern Console console;
	const char *status = getLighthouseDescription();
	console.sendMessage("lighthouse", status);
}

void DataBus2020::handleMatchSide() {
	switch (inputData[1]) {
		case DATABUS_RESULT_MATCHSIDE_DIRECT:
			matchSide = MatchSide::DIRECT;
			break;

		case DATABUS_RESULT_MATCHSIDE_REVERSED:
			matchSide = MatchSide::REVERSED;
			break;

		default:
			matchSide = MatchSide::UNKNOWN;
			break;
	}

	extern Console console;
	const char *status = getMatchSideDescription();
	console.sendMessage("side", status);
}

bool DataBus2020::queryAnchorArea(Match2020AnchorArea &area) {
	bool success = true;
	if (!push || (anchorArea == Match2020AnchorArea::UNKNOWN)) {
		success &= query(DATABUS_COMMAND_ANCHOR_AREA);
	}
	area = anchorArea;
	success &= area != Match2020AnchorArea::UNKNOWN;
	return success;
}

bool DataBus2020::query(char command) {
	const char outputData[] = { DATABUS_COMMAND_HEADER, command, '\0' };
	sendCommand(outputData);
	bool success = waitForInput();
	if (!success) {
		console.sendMessage("query timeout");
	}
	success &= inputData[0] == command;
	return success;
}

void DataBus2020::notifyMatchStart() {
	char outputData[] = { DATABUS_COMMAND_HEADER, DATABUS_COMMAND_MATCH_STARTED, '\0' };
	sendCommand(outputData);
	// Une fois le match d�marr�, on attend les donn�es en mode push
	setPushEnabled(true);
}

void DataBus2020::executeTestSequence() {
	console.sendData("\nquery match side");
	bool success, b;
	success = queryMatchSide(b);
	console.sendData(success ? "OK" : "KO");
	console.sendData(b ? "reversed" : "direct");

	console.sendData("\nquery lighthouse");
	success = queryLightHouse(b);
	console.sendData(success ? "OK" : "KO");
	console.sendData(b ? "activated" : "inactive");

	console.sendData("\nquery combination");
	Code combination;
	success = queryCombination(combination);
	console.sendData(success ? "OK" : "KO");
	const char *combinationName = "unknown";
	switch (combination) {
		case Code::VRVVR_VRRVR_1:
			combinationName = "1";
			break;

		case Code::VVRVR_VRVRR_2:
			combinationName = "2";
			break;

		case Code::VVVRR_VVRRR_3:
			combinationName = "3";
			break;

		default:
			break;
	}
	console.sendData(combinationName);

	console.sendData("\nquery anchor area");
	Match2020AnchorArea area;
	success = queryAnchorArea(area);
	console.sendData(success ? "OK" : "KO");
	const char *areaName = "unknown";
	switch (area) {
		case Match2020AnchorArea::NORTH:
			areaName = "north";
			break;

		case Match2020AnchorArea::SOUTH:
			areaName = "south";
			break;

		default:
			break;
	}
	console.sendData(areaName);
}

void DataBus2020::notifyElementDone(MatchElements2020 element) {
	int elementId = static_cast<int>(element);
	sendNumericCommand(DATABUS_COMMAND_ELEMENT_DONE, elementId);
}

void DataBus2020::sendNumericCommand(char command, int value) {
	char outputData[14];
	outputData[0] = DATABUS_COMMAND_HEADER;
	outputData[1] = command;
	itoa(value, &outputData[2], 10);
	sendCommand(outputData);
}

bool DataBus2020::queryElementDone(MatchElements2020 element, bool &done) {
	int elementId = static_cast<int>(element);
	done = elementsCache[elementId];
	if (done) {
		// si la valeur est dans le cache, pas d'interrogation distante
		return true;
	}

	char command = DATABUS_COMMAND_ELEMENT_QUERY;
	sendNumericCommand(command, elementId);
	bool success = waitForInput();
	if (!success) {
		console.sendMessage("query timeout");
	}
	success &= inputData[0] == command;
	done = success && (inputData[1] == DATABUS_RESULT_ELEMENT_DONE);
	if (done) {
		// ajout dans le cache si l'�l�ment a �t� trait�, sinon il peut �tre trait� ult�rieurement
		elementsCache[elementId] = done;
	}
	return success;
}

void DataBus2020::handleElementQuery() {
// Mise en cache
	if (inputData[0] == DATABUS_COMMAND_ELEMENT_STATE_QUERY) {
		int maskValue = strtol(&inputData[1], nullptr, 16);
		MatchElementsCache tmp(maskValue);
		elementsCache = tmp;
	}
}

void DataBus2020::setPushEnabled(bool value) {
	push = value;
}

void DataBus2020::notifyScore(int score) {
	sendNumericCommand(DATABUS_COMMAND_SCORE, score);
}
