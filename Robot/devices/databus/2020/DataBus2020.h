/*
 * DataBus2020.h
 *
 *  Created on: 8 f�vr. 2020
 *      Author: emmanuel
 */

#ifndef DEVICES_DATABUS_2020_DATABUS2020_H_
#define DEVICES_DATABUS_2020_DATABUS2020_H_

#include <stddef.h>
#include <bitset>

#include "../../../../Commons/databus/DataBus2020Commands.h"
#include "../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "../../../match/2020/Match2020Enums.h"
#include "../DataBus.h"

#define DATABUS2020_TIMEOUT 400

enum class MatchSide {
	UNKNOWN,
	DIRECT,
	REVERSED
};

enum class LightHouseStatus {
	UNKNOWN,
	INACTIVE,
	ACTIVE
};

typedef std::bitset<(size_t)MatchElements2020::Count> MatchElementsCache;

class DataBus2020 : public DataBus {
private:
	bool push = false;
	MatchSide matchSide = MatchSide::UNKNOWN;
	Code buoysCombination = Code::Undefined;
	Match2020AnchorArea anchorArea = Match2020AnchorArea::UNKNOWN;
	LightHouseStatus lightHouseStatus = LightHouseStatus::UNKNOWN;
	MatchElementsCache elementsCache;

	const char* getAreaDescription() const;
	const char* getLighthouseDescription() const;
	const char* getMatchSideDescription() const;
	void handleAnchorArea();
	void handleCombination();
	void handleElementQuery();
	void handleLightHouse();
	void handleMatchSide();
	bool query(char command);
	void sendNumericCommand(char command, int value);

protected:
	virtual void executeTestSequence() override;
	virtual void handleInput() override;

public:
	DataBus2020(Console &console);
	bool isPushEnabled() const;
	void notifyMatchStart();
	void notifyScore(int score);
	void notifyElementDone(MatchElements2020 element);
	bool queryAnchorArea(Match2020AnchorArea &area);
	bool queryCombination(Code &combination);
	bool queryElementDone(MatchElements2020 element, bool &done);
	bool queryLightHouse(bool& activated);
	bool queryMatchSide(bool &reversed);
	void setPushEnabled(bool value);
};

#endif /* DEVICES_DATABUS_DATABUS2020_H_ */
