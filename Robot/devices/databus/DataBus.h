/*
 * DataBus.h
 *
 *  Created on: 8 f�vr. 2020
 *      Author: emmanuel
 */

#ifndef DEVICES_DATABUS_DATABUS_H_
#define DEVICES_DATABUS_DATABUS_H_

// Temps d'attente avant �chec en millisecondes
#define DATABUS_TIMEOUT 2000
// Taille du tampon limit� � la taille BLE de 20 octets
#define DATABUS_BUFFER_SIZE 20

#include "devices/io/Console.h"

class InputParser;

class DataBus {
private:
	unsigned int timeoutMS = DATABUS_TIMEOUT;
	bool inputHandled = false;

	void clearInput();
	bool hasInput() const;

protected:
	Console &console;
	char inputData[DATABUS_BUFFER_SIZE];
	size_t inputDataLength = 0;

	friend class InputParser;
	friend class Console;
	virtual void executeTestSequence();
	virtual void handleInput();
	void setInput(const char *data, size_t dataLength);
	void sendCommand(const char *data);
	bool waitForInput(unsigned int timeoutMS = 0);

public:
	DataBus(Console &console);
	void setTimeout(unsigned int timeoutMS);
};

#endif /* DEVICES_DATABUS_DATABUS_H_ */
