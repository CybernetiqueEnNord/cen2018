/*
 * DataBus.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: emmanuel
 */

#include "DataBus.h"

#include "utils/Arrays.h"
#include <algorithm>
#include <string.h>

DataBus::DataBus(Console &console) :
		console(console) {
}

void DataBus::setInput(const char* data, size_t dataLength) {
	inputHandled = false;
	memcpy(this->inputData, data, std::min(ARRAY_SIZE(this->inputData), dataLength));
	this->inputDataLength = dataLength;
	handleInput();
	inputHandled = true;
}

bool DataBus::hasInput() const {
	return (inputDataLength > 0) && inputHandled;
}

bool DataBus::waitForInput(unsigned int timeoutMS) {
	if (timeoutMS == 0) {
		timeoutMS = this->timeoutMS;
	}
	systime_t now = chVTGetSystemTime();
	systime_t maxTime = now + MS2ST(timeoutMS);
	while (!hasInput() && now < maxTime) {
		chThdSleep(100);
		now = chVTGetSystemTime();
	}
	bool success = inputDataLength > 0;
	return success;
}

void DataBus::setTimeout(unsigned int timeoutMS) {
	this->timeoutMS = timeoutMS;
}

void DataBus::clearInput() {
	inputDataLength = 0;
	inputHandled = false;
}

void DataBus::sendCommand(const char* data) {
	clearInput();
	console.sendData(data);
}

void DataBus::handleInput() {
}

void DataBus::executeTestSequence() {
}
