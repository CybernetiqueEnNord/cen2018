/*
 * I2CDevice.cpp
 *
 *  Created on: 31 d�c. 2017
 *      Author: Emmanuel
 */

#include "devices/i2c/I2CDevice.h"

I2CDeviceConfig::I2CDeviceConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address) :
		i2cDriver(i2cDriver), i2cConfig(i2cConfig), address(address) {
}

I2CDevice::I2CDevice(I2CDeviceConfig &config) :
		config(config) {
}

bool I2CDevice::i2cWrite(uint8_t *out, unsigned int outSize, uint8_t *in, unsigned int inSize) const {
	msg_t result = i2cMasterTransmitTimeout(config.i2cDriver, config.address, out, outSize, in, inSize, I2C_TIMEOUT);
	if (result == MSG_TIMEOUT) {
		i2cReset();
	}
	return result == MSG_OK ;
}

bool I2CDevice::i2cRead(uint8_t *in, unsigned int inSize) const {
	msg_t result = i2cMasterReceiveTimeout(config.i2cDriver, config.address, in, inSize, I2C_TIMEOUT);
	if (result == MSG_TIMEOUT) {
		i2cReset();
	}
	return result == MSG_OK ;
}

void I2CDevice::i2cAcquireBus() const {
	::i2cAcquireBus(config.i2cDriver);
}

void I2CDevice::i2cReleaseBus() const {
	::i2cReleaseBus(config.i2cDriver);
}

void I2CDevice::i2cReset() const {
	i2cStart(config.i2cDriver, config.i2cConfig);
}

bool I2CDevice::isDevicePresent() const {
	uint8_t in;
	bool result = i2cRead(&in, sizeof(in));
	return result;
}
