/*
 * I2CDevice.h
 *
 *  Created on: 31 d�c. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_I2C_I2CDEVICE_H_
#define DEVICES_I2C_I2CDEVICE_H_

#include "hal.h"

struct I2CDeviceConfig {
	I2CDriver *i2cDriver;
	I2CConfig *i2cConfig;
	uint8_t address;
	I2CDeviceConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address);
};

class I2CDevice {
private:
	I2CDeviceConfig &config;

protected:
	void i2cAcquireBus() const;
	bool i2cRead(uint8_t *in, unsigned int inSize) const;
	void i2cReleaseBus() const;
	void i2cReset() const;
	bool i2cWrite(uint8_t *out, unsigned int outSize, uint8_t *in, unsigned int inSize) const;
	bool isDevicePresent() const;

public:
	I2CDevice(I2CDeviceConfig &config);
};

#endif /* DEVICES_I2C_I2CDEVICE_H_ */
