/*
 * Coder.cpp
 *
 *  Created on: 6 d�c. 2014
 *      Author: Emmanuel
 */

#include "Coder.h"

Coder::Coder(const CoderConfig &config) :
		config(config) {
}

void Coder::initialize(){
	qeiStart(config.qeiDriver, config.qeiConfig);
	qeiEnable (config.qeiDriver);

	palSetPadMode(config.portChannelA, config.padChannelA, PAL_MODE_ALTERNATE(config.mode));
	palSetPadMode(config.portChannelB, config.padChannelB, PAL_MODE_ALTERNATE(config.mode));
}

int Coder::getValue() const {
	return value;
}

int Coder::update() {
	int d = qeiUpdate(config.qeiDriver);
	if (config.reversed) {
		d = -d;
	}
	value += d;
	return d;
}

int Coder::updateI() {
	int d = qeiUpdateI(config.qeiDriver);
	if (config.reversed) {
		d = -d;
	}
	value += d;
	return d;
}

void Coder::reset() {
	qeiUpdate(config.qeiDriver);
	value = 0;
}
