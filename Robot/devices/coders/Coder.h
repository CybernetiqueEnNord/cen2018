/*
 * Coder.h
 *
 *  Created on: 6 d�c. 2014
 *      Author: Emmanuel
 */

#ifndef DEVICES_CODERS_CODER_H_
#define DEVICES_CODERS_CODER_H_

#include "hal.h"
#include "hal_qei.h"

/**
 * \brief Configuration des codeurs
 *
 * Structure de configuration des codeurs.
 */
struct CoderConfig {
	/** Pilote du p�ripg�rique QEI. */
	QEIDriver *qeiDriver;
	/** Configuration du pilote du p�ripg�rique QEI. */
	QEIConfig *qeiConfig;
	/** Port du canal A */
	stm32_gpio_t *portChannelA;
	/** Indice dans le port du canal A */
	uint8_t padChannelA;
	/** Port du canal B */
	stm32_gpio_t *portChannelB;
	/** Indice dans le port du canal B */
	uint8_t padChannelB;
	/** Mode de fonctionnement des canaux A et B */
	uint8_t mode;
	/** Drapeau indiquant si le sens de la mesure doit �tre invers�. */
	bool reversed;
};

/**
 * \brief Codeurs
 *
 * Gestion des codeurs incr�mentaux.
 */
class Coder {
private:
	const CoderConfig &config;
	int value = 0;

public:
	/**
	 * Constructeur.
	 * @param [in] config la configuration du codeur
	 */
	Coder(const CoderConfig &config);
	/**
	 * \brief Valeur
	 *
	 * Renvoie la valeur courante du codeur.
	 * @return la valeur courante du codeur
	 */
	int getValue() const;
	/**
	 * \brief Initialisation
	 *
	 * Initialise l'objet � partir de la configuration.
	 */
	void initialize();
	/**
	 * \brief R�initialisation
	 *
	 * R�initialise le gestionnaire.
	 */
	void reset();
	/**
	 * \brief Mise � jour
	 *
	 * Acquiert la mesure courante et met � jour la valeur.
	 * @return la nouvelle valeur du codeur
	 */
	int update();
	/**
	 * \brief Mise � jour
	 *
	 * Acquiert la mesure courante et met � jour la valeur.
	 * Cette m�thode est compatible avec un appel dans un gestionnaire d'interruption.
	 * @return la nouvelle valeur du codeur
	 */
	int updateI();
};

#endif /* DEVICES_CODERS_CODER_H_ */
