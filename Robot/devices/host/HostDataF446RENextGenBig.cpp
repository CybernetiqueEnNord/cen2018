/*
 * HostDataF446RENextGenBig.cpp
 *
 *  Created on: 21 oct. 2018
 *      Author: zoro
 */

#include "HostDataF446RENextGenBig.h"

#include "devices/host/HostCapabilities2019.h"
#include "init/Peripherals.h"

HostData_F446RE_NextGen_Big::HostData_F446RE_NextGen_Big() :
		HostData() {
	setName("F446RE Gros NG");
	architecture = HostArchitecture::STM32F446;
	leftCoderConfig.reversed = true;
	rightCoderConfig.reversed = true;
	controlConfig.stepsByMeter = 51129; // 3 51129  -16 51215  0 51133  -16 51213
	controlConfig.stepsByTurn = 30088;
	leftVsRightCorrectionRatio = -1500;
	capabilities = 0;
	// 1154 mm < 1200 OK perimetre non deploye
	// 1258 mm < 1300 OK perimetre deploye
	// 352 mm de large non deploye
	// 352 mm de large deploye
	// 225 mm de long non deploye
	// 277 mm de long deploye
	geometry.rearWidth = 113; //225
	geometry.frontWidth = 113;
	geometry.lateralWidth = 176; // largeur totale /2
	geometry.beaconDiameter = 98; // 9,8 cm de diam�tre pour les capteurs
	geometry.distanceXBeaconCenterFromRobotCenter = 0; // centr�e gauche droite
	geometry.distanceYBeaconCenterFromRobotCenter = 50; // 50mm en avant
	angleParameters = ControlParameters( { .P = 3, .D = 10, .weight = 16 });
	distanceParameters = ControlParameters( { .P = 6, .D = 10, .weight = 16 });
	robotType = RobotType::Big;
#if CEN_EDITION == 2019 || CEN_EDITION == 2020
	capabilities = CAPABILITY_BIG;
#endif
}

const DeviceId& HostData_F446RE_NextGen_Big::getDeviceId() {
	static DeviceId id = DeviceId( { 0x3a001e, 0x30385104, 0x30333332 });
	return id;
}
