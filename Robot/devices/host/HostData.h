/*
 * HostData.h
 *
 *  Created on: 27 nov. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_HOST_HOSTDATA_H_
#define DEVICES_HOST_HOSTDATA_H_

#include "control/PositionControl.h"
#include "devices/coders/Coder.h"
#include "devices/host/RobotGeometry.h"

class HostDevice;

enum class HostArchitecture {
	Unknown,
	STM32F411,
	STM32F446
};

enum class RobotType {
	Undefined,
	Small,
	Big
};

class HostData {
	friend class HostDevice;

protected:
	char name[20];
	HostArchitecture architecture = HostArchitecture::Unknown;
	CoderConfig leftCoderConfig;
	CoderConfig rightCoderConfig;
	ControlConfig controlConfig;
	ControlParameters angleParameters;
	ControlParameters distanceParameters;
	unsigned int capabilities = 0;
	int leftVsRightCorrectionRatio = 0;
	RobotGeometry geometry;
	RobotType robotType = RobotType::Undefined;

	HostData();
	void setName(const char *name);

public:
	const HostArchitecture& getArchitecture() const;
	unsigned int getCapabilities() const;
	const ControlConfig& getControlConfig() const;
	const ControlParameters& getControlParametersAngle() const;
	const ControlParameters& getControlParametersDistance() const;
	const RobotGeometry& getGeometry() const;
	const CoderConfig& getLeftCoderConfig() const;
	int getLeftVsRightCorrectionRatio() const;
	const char* getName() const;
	const CoderConfig& getRightCoderConfig() const;
	RobotType getRobotType() const;
	/**
	 * \brief Pr�sence des fonctionnalit�s
	 *
	 * D�termine si l'h�te poss�de toutes les fonctionnalit�s sp�cifi�es.
	 * @return TRUE si l'h�te poss�de toutes les fonctionnalit�s
	 */
	bool hasCapabilities(unsigned int capability) const;
};

#endif /* DEVICES_HOST_HOSTDATA_H_ */
