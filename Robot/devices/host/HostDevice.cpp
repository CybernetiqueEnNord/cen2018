/*
 * HostDevice.cpp
 *
 *  Created on: 27 nov. 2017
 *      Author: Emmanuel
 */

#include "ch.h"

#include "HostDataF446REBig.h"
#include "HostDataF446RENextGenBig.h"
#include "HostDataF446RENextGenSmall.h"
#include "HostDataF446RESmall.h"
#include "HostDataF411RETest.h"
#include "HostDevice.h"

Pointer<HostData> HostDevice::hostData = Pointer<HostData>();

const DeviceId& HostDevice::getDeviceId() {
#ifdef _X86_
	static DeviceId deviceId;
	DeviceId *ptr = &deviceId;
#else
	DeviceId *ptr = (DeviceId *) 0x1FFF7A10;
#endif
	return *ptr;
}

bool DeviceId::operator==(const DeviceId &id) const {
	bool result = value1 == id.value1 && value2 == id.value2 && value3 == id.value3;
	return result;
}

const HostData& HostDevice::getHostData() {
	if (!hostData.isValid()) {
		static HostData data;
		const DeviceId &id = getDeviceId();
		if (HostData_F411RE_Test::getDeviceId() == id) {
			data = HostData_F411RE_Test();
		} else if (HostData_F446RE_Big::getDeviceId() == id) {
			data = HostData_F446RE_Big();
		} else if (HostData_F446RE_Small::getDeviceId() == id) {
			data = HostData_F446RE_Small();
		} else if (HostData_F446RE_NextGen_Big::getDeviceId() == id) {
			data = HostData_F446RE_NextGen_Big();
		} else if (HostData_F446RE_NextGen_Small::getDeviceId() == id) {
			data = HostData_F446RE_NextGen_Small();
		}
		hostData = &data;
	}
	return *hostData;
}

const char * HostDevice::getVersion() {
	return "v0.2." __DATE__ "." __TIME__ "." BOARD_NAME;
}

bool HostDevice::isBig() {
	const HostData &hostData = getHostData();
	bool value = hostData.getRobotType() == RobotType::Big;
	return value;
}

bool HostDevice::isSmall() {
	const HostData &hostData = getHostData();
	bool value = hostData.getRobotType() == RobotType::Small;
	return value;
}
