/*
 * HostDevice.h
 *
 *  Created on: 27 nov. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_HOST_HOSTDEVICE_H_
#define DEVICES_HOST_HOSTDEVICE_H_

#include <stdint.h>

#include "devices/host/HostData.h"
#include "utils/Pointer.h"

struct DeviceId {
public:
	uint32_t value1;
	uint32_t value2;
	uint32_t value3;

	bool operator==(const DeviceId &id) const;
};

class HostDevice {
private:
	static Pointer<HostData> hostData;

public:
	static const DeviceId& getDeviceId();
	static const HostData& getHostData();
	static const char * getVersion();
	static bool isBig();
	static bool isSmall();
};

#endif /* DEVICES_HOST_HOSTDEVICE_H_ */
