/*
 * HostDataF411RETest.h
 *
 *  Created on: 27 nov. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_HOST_HOSTDATAF411RETEST_H_
#define DEVICES_HOST_HOSTDATAF411RETEST_H_

#include "HostData.h"
#include "HostDevice.h"

class HostData_F411RE_Test : public HostData {
friend class HostDevice;

protected:
	HostData_F411RE_Test();

public:
	static const DeviceId& getDeviceId();
};

#endif /* DEVICES_HOST_HOSTDATAF411RETEST_H_ */
