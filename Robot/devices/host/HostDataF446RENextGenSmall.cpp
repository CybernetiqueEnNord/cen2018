/*
 * HostDataF446RENextGenSmall.cpp
 *
 *  Created on: 21 oct. 2018
 *      Author: zoro
 */

#include "HostDataF446RENextGenSmall.h"

#include "devices/host/HostCapabilities2019.h"
#include "init/Peripherals.h"

HostData_F446RE_NextGen_Small::HostData_F446RE_NextGen_Small() :
		HostData() {
	setName("F446RE Petit NG");
	leftCoderConfig.reversed = true;
	rightCoderConfig.reversed = true;
	architecture = HostArchitecture::STM32F446;
	controlConfig.stepsByMeter = 51230; // -9 51195   -13 51217  -4 51230   -4 51230 0 51225  1 51224
	controlConfig.stepsByTurn = 29226; // 29212 // 29194 + 316
	leftVsRightCorrectionRatio = 1100;
	capabilities = 0;
	// 774 mm < 896 mm OK perimetre non deploye
	// ??? < 942 mm OK perimetre deploye
	// 270 mm largeur arriere
	// 72 mm largeur chassis
	// 130 mm largeur avant
	// 115 mm diagonale cote
	geometry.rearWidth = 36;
	geometry.frontWidth = 97 + 35;
	geometry.lateralWidth = 138; // largeur totale 276 mm
	geometry.beaconDiameter = 98; // 9,8 cm de diam�tre pour les capteurs
	geometry.distanceXBeaconCenterFromRobotCenter = 0; // centr�e gauche droite
	geometry.distanceYBeaconCenterFromRobotCenter = 50; // 50mm en avant
	angleParameters = ControlParameters( { .P = 12, .D = 10, .weight = 16 });
	distanceParameters = ControlParameters( { .P = 6, .D = 8, .weight = 16 });
	robotType = RobotType::Small;
#if CEN_EDITION == 2019 || CEN_EDITION == 2020
	capabilities = CAPABILITY_SMALL;
#endif
}

const DeviceId& HostData_F446RE_NextGen_Small::getDeviceId() {
	static DeviceId id = DeviceId( { 0x4a0048, 0x30385104, 0x30333332 });
	return id;
}
