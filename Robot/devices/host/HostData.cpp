/*
 * HostData.cpp
 *
 *  Created on: 27 nov. 2017
 *      Author: Emmanuel
 */

#include "HostData.h"

#include "devices/host/HostDevice.h"
#include "devices/host/RobotGeometry.h"
#include "init/Peripherals.h"
#include "utils/Arrays.h"
#include "utils/Base64.h"

#include <cstring>

HostData::HostData() {
	DeviceId id = HostDevice::getDeviceId();
	Base64::encode((unsigned char*) &id, sizeof(DeviceId), (unsigned char*) name);
	name[(sizeof(DeviceId) * 4) / 3] = 0;

	extern QEIConfig qeiLeftConfig;
	leftCoderConfig = {.qeiDriver = &QEI_LEFT, .qeiConfig = &qeiLeftConfig, .portChannelA = PORT_QEIM1A, .padChannelA = PAD_QEIM1A, .portChannelB = PORT_QEIM1B, .padChannelB = PAD_QEIM1B, .mode = MODE_QEIM1, .reversed = false};

	extern QEIConfig qeiRightConfig;
	rightCoderConfig = {.qeiDriver = &QEI_RIGHT, .qeiConfig = &qeiRightConfig, .portChannelA = PORT_QEIM2A, .padChannelA = PAD_QEIM2A, .portChannelB = PORT_QEIM2B, .padChannelB = PAD_QEIM2B, .mode = MODE_QEIM2, .reversed = false};

	angleParameters = ControlParameters( { .P = 1, .D = 0, .weight = 16 });
	distanceParameters = ControlParameters( { .P = 1, .D = 0, .weight = 16 });
}

const char* HostData::getName() const {
	return name;
}

void HostData::setName(const char *newName) {
	strncpy(name, newName, ARRAY_SIZE(name));
	name[ARRAY_SIZE(name) - 1] = 0;
}

const HostArchitecture& HostData::getArchitecture() const {
	return architecture;
}

const CoderConfig& HostData::getLeftCoderConfig() const {
	return leftCoderConfig;
}

const CoderConfig& HostData::getRightCoderConfig() const {
	return rightCoderConfig;
}

const ControlConfig& HostData::getControlConfig() const {
	return controlConfig;
}

unsigned int HostData::getCapabilities() const {
	return capabilities;
}

int HostData::getLeftVsRightCorrectionRatio() const {
	return leftVsRightCorrectionRatio;
}

const RobotGeometry& HostData::getGeometry() const {
	return geometry;
}

const ControlParameters& HostData::getControlParametersAngle() const {
	return angleParameters;
}

const ControlParameters& HostData::getControlParametersDistance() const {
	return distanceParameters;
}

bool HostData::hasCapabilities(unsigned int capability) const {
	bool result = (capabilities & capability) != capability;
	return result;
}

RobotType HostData::getRobotType() const {
	return robotType;
}
