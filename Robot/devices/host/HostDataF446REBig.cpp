/*
 * HostDataF446RE.cpp
 *
 *  Created on: 27 d�c. 2017
 *      Author: Emmanuel
 */

#include "devices/host/HostCapabilities2018.h"
#include "devices/host/HostDataF446REBig.h"
#include "init/Peripherals.h"

HostData_F446RE_Big::HostData_F446RE_Big() :
		HostData() {
	setName("F446RE Gros");
	architecture = HostArchitecture::STM32F446;
	leftCoderConfig.reversed = true;
	rightCoderConfig.reversed = true;
	controlConfig.stepsByMeter = 68821;
	controlConfig.stepsByTurn = 50360;
	robotType = RobotType::Big;
#if CEN_EDITION == 2018
	capabilities = CAPABILITY_CUBES;
#endif
}

const DeviceId& HostData_F446RE_Big::getDeviceId() {
	static DeviceId id = DeviceId( { 0x00380054, 0x30355112, 0x37313835 });
	return id;
}
