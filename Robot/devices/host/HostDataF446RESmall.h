/*
 * HostDataF446RESmall.h
 *
 *  Created on: 17 f�vr. 2018
 *      Author: Emmanuel
 */

#ifndef DEVICES_HOST_HOSTDATAF446RESMALL_H_
#define DEVICES_HOST_HOSTDATAF446RESMALL_H_

#include "HostData.h"
#include "HostDevice.h"

class HostData_F446RE_Small : HostData {
	friend class HostDevice;

protected:
	HostData_F446RE_Small();

public:
	static const DeviceId& getDeviceId();
};

#endif /* DEVICES_HOST_HOSTDATAF446RESMALL_H_ */
