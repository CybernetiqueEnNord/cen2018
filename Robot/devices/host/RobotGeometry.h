/*
 * RobotGeometry.h
 *
 *  Created on: 3 mars 2019
 *      Author: emmanuel
 */

#ifndef DEVICES_HOST_ROBOTGEOMETRY_H_
#define DEVICES_HOST_ROBOTGEOMETRY_H_

struct RobotGeometry
{
	/**
	 * \brief Dimension arri�re
	 *
	 * Distance en millim�tres entre l'axe des roues et la partie arri�re en
	 * contact avec la bordure.
	 */
	unsigned int rearWidth = 0;
	/**
	 * \brief Dimension avant
	 *
	 * Distance en millim�tres entre l'axe des roues et la partie avant en
	 * contact avec la bordure.
	 */
	unsigned int frontWidth = 0;
	/**
	 * \brief Demi-distance lat�rale
	 *
	 * Distance en millim�tres entre l'axe de d�placement du robot (au centre
	 * des roues) et la bord droit (ou gauche, supposant que le robot est
	 * sym�trique)
	 */
	unsigned int lateralWidth = 0;
	/**
	 * \brief Distance de d�gagement
	 *
	 * Distance minimale � parcourir en millim�tres pour d�gager le robot de
	 * tout obstacle.
	 */
	unsigned int clearanceDistance = 0;
	/**
	 * \brief Ecart balise-robot en X
	 *
	 * Distance en mm entre le centre de la balise et le centre du robot sur
	 * l'axe gauche-droite du robot (on d�finit "devant" comme �tant la
	 * direction dans laquelle le robot avance quand il est en marche avant, et
	 * on regarde de l'arri�re vers l'avant).
	 * Si la balise est plus � gauche alors distance < 0.
	 * Si la balise est plus � droite alors distance > 0.
	 */
	int distanceXBeaconCenterFromRobotCenter = 0;
	/**
	 * \brief Ecart balise-robot en Y
	 *
	 * Distance en mm entre le centre de la balise et le centre du robot sur
	 * l'axe avant-arri�re du robot (on d�finit "devant" comme �tant la
	 * direction dans laquelle le robot avance quand il est en marche avant).
	 * Si la balise est plus en avant alors distance > 0.
	 * Si la balise est plus en arri�re, alors distance < 0.
	 */
	int distanceYBeaconCenterFromRobotCenter = 0;
	/**
	 * \brief Diam�tre de la balise
	 *
	 * Distance en mm entre les parties ext�rieures des capteurs de la balise
	 * (en supposant que la balise est circulaire, et que les capteurs sont
	 * plac�s tous sur le m�me cercle qui a le m�me diam�tre depuis le centre
	 * pour chaque capteur).
	 */
	unsigned int beaconDiameter = 0;

	RobotGeometry();
	RobotGeometry(unsigned int rearWidth);
};

#endif /* DEVICES_HOST_ROBOTGEOMETRY_H_ */
