/*
 * HostDataF446RENextGenBig.h
 *
 *  Created on: 21 oct. 2018
 *      Author: zoro
 */

#ifndef DEVICES_HOST_HOSTDATAF446RENEXTGENBIG_H_
#define DEVICES_HOST_HOSTDATAF446RENEXTGENBIG_H_

#include "HostData.h"
#include "HostDevice.h"

class HostData_F446RE_NextGen_Big : public HostData {
	friend class HostDevice;

protected:
	HostData_F446RE_NextGen_Big();

public:
	static const DeviceId& getDeviceId();
};

#endif /* DEVICES_HOST_HOSTDATAF446RENEXTGENBIG_H_ */
