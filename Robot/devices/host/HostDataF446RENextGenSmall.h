/*
 * HostDataF446RENextGenSmall.h
 *
 *  Created on: 21 oct. 2018
 *      Author: zoro
 */

#ifndef DEVICES_HOST_HOSTDATAF446RENEXTGENSMALL_H_
#define DEVICES_HOST_HOSTDATAF446RENEXTGENSMALL_H_

#include "HostData.h"
#include "HostDevice.h"

class HostData_F446RE_NextGen_Small : public HostData {
	friend class HostDevice;

protected:
	HostData_F446RE_NextGen_Small();

public:
	static const DeviceId& getDeviceId();
};

#endif /* DEVICES_HOST_HOSTDATAF446RENEXTGENSMALL_H_ */
