/*
 * HostDataF446RE.h
 *
 *  Created on: 27 d�c. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_HOST_HOSTDATAF446REBIG_H_
#define DEVICES_HOST_HOSTDATAF446REBIG_H_

#include "HostData.h"
#include "HostDevice.h"

class HostData_F446RE_Big : public HostData {
friend class HostDevice;

protected:
	HostData_F446RE_Big();

public:
	static const DeviceId& getDeviceId();
};

#endif /* DEVICES_HOST_HOSTDATAF446REBIG_H_ */
