/*
 * HostDataF446RESmall.cpp
 *
 *  Created on: 17 f�vr. 2018
 *      Author: Emmanuel
 */

#include "devices/host/HostCapabilities2018.h"
#include "devices/host/HostDataF446RESmall.h"

HostData_F446RE_Small::HostData_F446RE_Small() :
		HostData() {
	setName("F446RE Petit");
	architecture = HostArchitecture::STM32F446;
	controlConfig.stepsByTurn = 44682;
	controlConfig.stepsByMeter = 76081;
	capabilities = CAPABILITY_WATER;
	robotType = RobotType::Small;
}

const DeviceId& HostData_F446RE_Small::getDeviceId() {
	static DeviceId id = DeviceId( { 0x3b005d, 0x30355112, 0x37313835 });
	return id;
}
