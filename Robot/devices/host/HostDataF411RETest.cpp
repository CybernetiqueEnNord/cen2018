/*
 * HostDataF411RETest.cpp
 *
 *  Created on: 27 nov. 2017
 *      Author: Emmanuel
 */

#include "HostDataF411RETest.h"

HostData_F411RE_Test::HostData_F411RE_Test() : HostData() {
	setName("F411RE Test");
	architecture = HostArchitecture::STM32F411;
	leftCoderConfig.reversed = true;
	rightCoderConfig.reversed = true;
	controlConfig.stepsByTurn = 44682;
	controlConfig.stepsByMeter = 76081;
}

const DeviceId& HostData_F411RE_Test::getDeviceId() {
	static DeviceId id = DeviceId( { 0x0031006d, 0x30345110, 0x37363035 });
	return id;
}
