/*
 * Beacon.h
 *
 *  Created on: 9 d�c. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_BEACON_BEACON_H_
#define DEVICES_BEACON_BEACON_H_

#include "devices/io/ObstacleDetector.h"

class Beacon : public ObstacleDetector {
public:
	Beacon();
};

#endif /* DEVICES_BEACON_BEACON_H_ */
