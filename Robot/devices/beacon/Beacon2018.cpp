/*
 * Beacon2018.cpp
 *
 *  Created on: 26 d�c. 2017
 *      Author: Emmanuel
 */

#include "devices/beacon/Beacon2018.h"

#include "ch.h"
#include "utils/Arrays.h"

#include "i2c/BeaconCommands.h"
#include "math/cenMath.h"

Beacon2018Config::Beacon2018Config(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address, const Area &obstacleArea) :
		I2CDeviceConfig(i2cDriver, i2cConfig, address), obstacleArea(obstacleArea) {
}

Beacon2018::Beacon2018(Beacon2018Config &config) :
		I2CDevice(config), config(config) {
}

void Beacon2018::initialize() {
	i2cAcquireBus();
	readVersion();
	available = data.version[0] != 0;
	if (available) {
		readConfiguration();
	}
	i2cReleaseBus();
}

void Beacon2018::initializeBeaconVars(int dx, int dy, unsigned diameter) {
	distanceXBeaconCenterFromRobotCenter = dx;
	distanceYBeaconCenterFromRobotCenter = dy;
	beaconDiameter = diameter;
}

bool Beacon2018::readBuffer(uint8_t command, uint8_t *buffer, unsigned int size) {
	return i2cWrite(&command, sizeof(command), buffer, size);
}

void Beacon2018::readVersion() {
	if (!readBuffer(COMMAND_I2C_VERSION, (uint8_t*) data.version, sizeof data.version)) {
		data.version[0] = 0;
	}
}

const char* Beacon2018::getVersion() const {
	return data.version;
}

bool Beacon2018::isAvailable() const {
	return available;
}

const Beacon2018Data& Beacon2018::getData() const {
	return data;
}

void Beacon2018::readHMI() {
	uint8_t values[2];
	if (!readBuffer(COMMAND_I2C_HMI, (uint8_t*) values, sizeof values)) {
		return;
	}
	data.hmiValue1 = values[0];
	data.hmiValue2 = values[1];
}

void Beacon2018::readConfiguration() {
	struct Config {
		uint8_t index;
		uint8_t angle;
	};

	uint8_t buffer[sizeof(Config) * (ARRAY_SIZE(sensors) + 1)];
	if (!readBuffer(COMMAND_I2C_CONFIG, (uint8_t*) buffer, sizeof buffer)) {
		return;
	}

	unsigned int i = 0;
	Config *config = (Config*) buffer;
	while (config->index != 0xFF && i < ARRAY_SIZE(sensors)) {
		unsigned int index = config->index;
		if (index < ARRAY_SIZE(sensors)) {
			sensors[index].configured = true;
			sensors[index].angleDegrees = (config->angle * 360) / 256;
		}
		config++;
		i++;
	}
}

void Beacon2018::readMeasures() {
	struct Measure {
		uint8_t index;
		uint8_t distance;
	};

	uint8_t buffer[sizeof(Measure) * (ARRAY_SIZE(sensors) + 1)];
	if (!readBuffer(COMMAND_I2C_MEASURES, (uint8_t*) buffer, sizeof buffer)) {
		return;
	}

	unsigned int i = 0;
	Measure *measure = (Measure*) buffer;
	while (measure->index != 0xFF && i < ARRAY_SIZE(sensors)) {
		unsigned int index = measure->index;
		if (index < ARRAY_SIZE(sensors)) {
			sensors[index].distance = measure->distance * 32;
		}
		measure++;
		i++;
	}
}

void Beacon2018::readStatus() {
	uint8_t status;
	if (!readBuffer(I2C_COMMAND_STATUS, &status, sizeof(status))) {
		return;
	}
	data.obstacle = static_cast<Obstacle>((status & MASK_STATUS_OBSTACLE) >> SHIFT_STATUS_OBSTACLE);
	data.started = static_cast<bool>((status & MASK_STATUS_START) >> SHIFT_STATUS_START);
}

void Beacon2018::updateHMIData() {
	update(Beacon2018DataType::HMIData);
}

void Beacon2018::updateMeasures() {
	update(Beacon2018DataType::Measures);
}

void Beacon2018::updateStatus() {
	update(Beacon2018DataType::Status);
}

void Beacon2018::update(Beacon2018DataType mask) {
	i2cAcquireBus();
	if (hasFlag(mask, Beacon2018DataType::HMIData)) {
		readHMI();
	}
	if (hasFlag(mask, Beacon2018DataType::Measures)) {
		readMeasures();
	}
	if (hasFlag(mask, Beacon2018DataType::Status)) {
		readStatus();
	}
	i2cReleaseBus();
}

bool Beacon2018::setThreshold(int highDistanceMM, int lowDistanceMM) {
	highDistanceThreshold = highDistanceMM;
	lowDistanceThreshold = lowDistanceMM;
	if (lowDistanceMM == INT_MAX) {
		lowDistanceThreshold = highDistanceMM;
	}

	distanceThreshold = highDistanceMM;
	state = BeaconState::STATE_HIGH_THRESHOLD;
	lastTimestamp = chVTGetSystemTime();

	uint8_t buffer[2];
	buffer[0] = COMMAND_I2C_THRESHOLD;
	buffer[1] = highDistanceMM / 8;
	uint8_t in;

	i2cAcquireBus();
	bool result = i2cWrite(buffer, sizeof buffer, &in, sizeof(in));
	i2cReleaseBus();
	return result;
}

bool Beacon2018::isObstacleDetected(const Position &position, bool movingBackward, Point &obstaclePosition) {
	if (!isAdvancedMode()) {
		// Mode standard : on utilise le flag d�tection de la balise
		return hasFlag(data.obstacle, movingBackward ? Obstacle::Back : Obstacle::Front);
	}

	// Mode avanc�, on utilise les informations d'angle et de distance que l'on proj�te sur la carte
	bool result = false;
	for (unsigned int i = 0; i < ARRAY_SIZE(sensors); i++) {
		const BeaconSensorData &data = sensors[i];

		// V�rifie que le capteur est configur�
		if (!data.configured) {
			continue;
		}

		// V�rifie que le capteur est dans la direction de d�placement
		bool backObstacle = (data.angleDegrees > 90) && (data.angleDegrees < 270);
		if (movingBackward != backObstacle) {
			continue;
		}

		// V�rifie que l'obstacle est dans l'intervalle de d�tection
		if (data.distance > distanceThreshold || data.distance == 0) {
			continue;
		}

		float angleRadians = ANGLE_TO_RADIANS(data.angleDegrees);
		if (reversed) {
			angleRadians = M_2PI - angleRadians;
		}

		// on corrige la position calcul�e du point en ajoutant deux offsets
		// premier offset de decalage lineaire du centre de la balise par
		//    rapport au centre du robot
		// second offset, radial, du rayon de la balise dans la direction du
		//    capteur par rapport au centre de la balise
		//Point delta = Point(distanceXBeaconCenterFromRobotCenter, distanceYBeaconCenterFromRobotCenter);
		Point p = position.getPoint(angleRadians, data.distance + (beaconDiameter / 2));
		//Point pcorr = Point(delta.x + p.x, delta.y + p.y);
		result = config.obstacleArea.contains(p);
		if (result) {
			obstaclePosition = p;
			break;
		}
	}
	updateState(result);
	return result;
}

void Beacon2018::updateState(bool obstacle) {
	systime_t now = chVTGetSystemTime();
	switch (state) {
		case BeaconState::STATE_HIGH_THRESHOLD:
			if (obstacle) {
				long elapsed = ST2MS(now - lastTimestamp);
				if (elapsed >= highThresholdTimeMs) {
					state = BeaconState::STATE_LOW_THRESHOLD;
					lastTimestamp = now;
					distanceThreshold = lowDistanceThreshold;
				}
			} else {
				lastTimestamp = now;
			}
			break;

		case BeaconState::STATE_LOW_THRESHOLD:
			if (!obstacle) {
				long elapsed = ST2MS(now - lastTimestamp);
				if (elapsed >= lowThresholdTimeMs) {
					state = BeaconState::STATE_HIGH_THRESHOLD;
					lastTimestamp = now;
					distanceThreshold = highDistanceThreshold;
				}
			}
			break;
	}
}

bool Beacon2018::isPresent() {
	i2cAcquireBus();
	bool result = isDevicePresent();
	i2cReleaseBus();
	return result;
}

bool Beacon2018::isStarted() {
	return data.started;
}

bool Beacon2018::setLedBlinking(unsigned int onDuration, unsigned int offDuration, unsigned int count) {
	uint8_t buffer[4];
	buffer[0] = COMMAND_I2C_LED;
	buffer[1] = onDuration / 16;
	buffer[2] = offDuration / 16;
	buffer[3] = count;
	uint8_t in;

	i2cAcquireBus();
	bool result = i2cWrite(buffer, sizeof buffer, &in, sizeof(in));
	i2cReleaseBus();
	return result;
}

bool Beacon2018::setBeepEnabled(bool value) {
	uint8_t buffer[2];
	buffer[0] = COMMAND_I2C_BUZZER;
	buffer[1] = value;
	uint8_t in;

	i2cAcquireBus();
	bool result = i2cWrite(buffer, sizeof buffer, &in, sizeof(in));
	i2cReleaseBus();
	return result;
}

bool Beacon2018::isAdvancedMode() const {
	bool result = advancedMode && sensors[0].configured;
	return result;
}

const BeaconSensorData* Beacon2018::getSensorData() const {
	return sensors;
}

void Beacon2018::setReversed(bool value) {
	reversed = value;
}
