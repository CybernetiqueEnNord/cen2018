/*
 * Beacon2018.h
 *
 *  Created on: 26 d�c. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_BEACON_BEACON2018_H_
#define DEVICES_BEACON_BEACON2018_H_

#include "configuration/Beacon2018Configuration.h"
#include "devices/beacon/Beacon.h"
#include "devices/i2c/I2CDevice.h"
#include "devices/io/StartDetector.h"
#include "geometry/Area.h"
#include "utils/Booleans.h"

#include <inttypes.h>
#include <limits.h>

#define SENSORS_COUNT BEACON2018_SENSORS_COUNT

#define MASK_STATUS_OBSTACLE 0x3
#define SHIFT_STATUS_OBSTACLE 0
#define MASK_STATUS_START 0x4
#define SHIFT_STATUS_START 2

enum class Obstacle {
	None = 0,
	Front = 1,
	Back = 2
};

FLAGS(Obstacle);

enum class Beacon2018DataType {
	None = 0,
	HMIData = 1,
	Measures = 2,
	Status = 4,
	All = ~None
};

FLAGS(Beacon2018DataType)

struct Beacon2018Config : I2CDeviceConfig {
	const Area &obstacleArea;
	Beacon2018Config(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address, const Area &obstacleArea);
};

struct Beacon2018Data {
	Obstacle obstacle = Obstacle::None;
	bool started = false;
	int hmiValue1 = 0;
	int hmiValue2 = 0;
	char version[20] = { 0 };
};

struct BeaconSensorData {
	/**
	 * \brief Drapeau de configuration
	 *
	 * Drapeau indiquant si le capteur est configur�
	 */
	bool configured = false;
	/**
	 * \brief Angle du capteur
	 *
	 * L'angle du capteur en degr�s
	 */
	int angleDegrees = 0;
	/**
	 * \brief Distance de l'obstacle
	 *
	 * La distance de l'obstacle en mm
	 */
	int distance = 0;
};

enum BeaconState {
	STATE_HIGH_THRESHOLD, STATE_LOW_THRESHOLD
};

class Beacon2018 : public Beacon, public I2CDevice, public StartDetector {
private:
	Beacon2018Config &config;
	Beacon2018Data data;
	BeaconSensorData sensors[SENSORS_COUNT];

	/**
	 * @see RobotGeometry.distanceXBeaconCenterFromRobotCenter
	 */
	int distanceXBeaconCenterFromRobotCenter = 0;
	/**
	 * @see RobotGeometry.distanceYBeaconCenterFromRobotCenter
	 */
	int distanceYBeaconCenterFromRobotCenter = 0;
	/**
	 * @see RobotGeometry.beaconDiameter
	 */
	unsigned int beaconDiameter = 0;

	int highThresholdTimeMs = 2000;
	int lowThresholdTimeMs = 2000;
	systime_t lastTimestamp = 0;
	BeaconState state = BeaconState::STATE_HIGH_THRESHOLD;

	bool available = false;
	bool reversed = false;
	int distanceThreshold = INT_MAX;
	int highDistanceThreshold = INT_MAX;
	int lowDistanceThreshold = INT_MAX;

	bool readBuffer(uint8_t command, uint8_t *buffer, unsigned int size);
	void readConfiguration();
	void readHMI();
	void readMeasures();
	void readStatus();
	void readVersion();
	void reset();
	void updateState(bool obstacle);

public:
	bool advancedMode = true;

	Beacon2018(Beacon2018Config &config);
	const Beacon2018Data& getData() const;
	const BeaconSensorData* getSensorData() const;
	const char* getVersion() const;
	void initialize();
	void initializeBeaconVars(int dx, int dy, unsigned diameter);
	bool isAdvancedMode() const;
	bool isAvailable() const;
	virtual bool isObstacleDetected(const Position &position, bool movingBackward, Point &obstaclePosition) override;
	bool isPresent();
	virtual bool isStarted();
	bool setBeepEnabled(bool value);
	bool setLedBlinking(unsigned int onDuration, unsigned int offDuration, unsigned int count);
	void setReversed(bool value);
	bool setThreshold(int maxValue, int minValue = INT_MAX);
	void updateHMIData();
	void updateMeasures();
	void updateStatus();
	void update(Beacon2018DataType mask);
};

#endif /* DEVICES_BEACON_BEACON2018_H_ */
