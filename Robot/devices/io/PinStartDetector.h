/*
 * PinStartDetector.h
 *
 *  Created on: 4 oct. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_IO_PINSTARTDETECTOR_H_
#define DEVICES_IO_PINSTARTDETECTOR_H_

#include "devices/io/StartDetector.h"

#include "hal.h"

/**
 * \brief Configuration de la d�tection de d�but de match
 *
 * Structure de configuration du p�riph�rique de d�tection de d�but de match.
 */
struct PinStartDetectorConfig {
	/** Port de l'entr�e. */
	stm32_gpio_t *port;
	/** Broche de l'entr�e. */
	int pad;
	/** Signal de d�part � d�tecter. */
	unsigned int startSignal;
	/** Seuil de d�tection en nombre de d�tections successives. */
	int threshold;
};

/**
 * \brief D�tection du d�but de match
 *
 * D�tection du d�but de match bas� sur l'�tat d'une entr�e.
 */
class PinStartDetector : public StartDetector {
private:
	int startCounter = 0;
	bool started = false;
	PinStartDetectorConfig &config;

	void checkStarted();
public:
	/**
	 * Constructeur.
	 * @param [in] config la configuration du p�riph�rique de d�tection de d�but de match
	 */
	PinStartDetector(PinStartDetectorConfig &config);
	virtual bool isStarted();
};

#endif /* DEVICES_IO_PINSTARTDETECTOR_H_ */
