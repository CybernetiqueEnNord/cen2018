/*
 * EmergencyStop.h
 *
 *  Created on: 25 d�c. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_EMERGENCYSTOP_H_
#define DEVICES_EMERGENCYSTOP_H_

class EmergencyStop {
protected:
	EmergencyStop();

public:
	virtual bool isPressed() const;
};

#endif /* DEVICES_EMERGENCYSTOP_H_ */
