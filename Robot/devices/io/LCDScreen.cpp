/*
 * LCDScreen.cpp
 *
 *  Created on: 26 oct. 2017
 *      Author: Emmanuel
 */

#include "LCDScreen.h"

#include "devices/lcd/LCDBigFont.h"
#include "match/MatchData.h"
#include "math/cenMath.h"
#include "utils/Text.h"

#include <algorithm>

LCDScreen::LCDScreen(LCD_QY2004A &lcd) :
		Screen(), lcd(lcd) {
	chMtxObjectInit(&mutex);
}

void LCDScreen::acquire() {
	chMtxLock(&mutex);
}

void LCDScreen::release() {
	chMtxUnlock(&mutex);
}

void LCDScreen::initialize() {
	lcd.begin();
}

void LCDScreen::lockedEraseLine(int count) {
	while (count-- > 0) {
		lcd.print(' ');
	}
}

void LCDScreen::lockedEraseLineRemaining(int start) {
	int n = lcd.getColsCount() - start;
	lockedEraseLine(n);
}

void LCDScreen::lockedDisplayMessage(const char *message, int x) {
	lcd.setCursor(x, 2);
	int n = lcd.print(message);
	n = lcd.getColsCount() - n - x;
	lockedEraseLine(n);
}

void LCDScreen::displayMessage(const char *message, int x) {
	acquire();
	lockedDisplayMessage(message, x);
	release();
}

void LCDScreen::displayTest(int step, const char *message) {
	acquire();
	lockedDisplayTest(step, message);
	release();
}

void LCDScreen::lockedDisplayTest(int step, const char *message) {
	lockedDisplayTestStep(step);
	lockedDisplayMessage(message);
	lockedEraseLineFrom(0, 4);
}

void LCDScreen::lockedEraseLineFrom(int col, int row, int count) {
	lcd.setCursor(col, row);
	if (count == 0) {
		count = lcd.getColsCount();
	}
	lockedEraseLine(count);
}

void LCDScreen::displayTestStatus(const char *status, const char *message) {
	acquire();
	lockedDisplayTestStatus(status, message);
	release();
}

void LCDScreen::lockedDisplayTestStatus(const char *status, const char *message) {
	int cols = lcd.getColsCount();
	int l = strlen(status);
	int n = cols - l;
	lcd.setCursor(n, 2);
	lcd.write(status, l);
	if (message != NULL) {
		lcd.setCursor(0, 3);
		l = strlen(message);
		l = std::min(l, cols);
		n = lcd.write(message, l);
		lockedEraseLine(cols - n);
	}
}

void LCDScreen::startTests(int stepsCount) {
	acquire();
	lcd.clear();
	lcd.home();
	lcd.print("Running tests");
	this->stepsCount = stepsCount;
	lockedDisplayTestStep(0);
	release();
}

void LCDScreen::stopTests() {
	acquire();
	lcd.clear();
	release();
}

void LCDScreen::lockedDisplayTestStep(int step) {
	lcd.setCursor(15, 0);
	int n = lcd.print(step);
	n += lcd.print('/');
	n += lcd.print(stepsCount);
	lockedEraseLineRemaining(n);
}

void LCDScreen::update() {
	acquire();
	lockedDisplayScore();
	lockedDisplayTimer();
	lockedDisplayPosition();
	lockedDisplayObstacle();
	release();
}

void LCDScreen::lockedDisplayObstacle() {
	if (obstacle == obstacleDisplayed) {
		return;
	}
	if (obstacle) {
		lockedDisplayMessage("OBSTACLE");
		if (obstaclePosition.x != 0 || obstaclePosition.y != 0) {
			char buffer[9];
			itoa((int) obstaclePosition.x, buffer, 10);
			lockedDisplayMessage(buffer, 10);
			itoa((int) obstaclePosition.y, buffer, 10);
			lockedDisplayMessage(buffer, 16);
		}
	} else {
		lockedDisplayMessage("");
	}
	obstacleDisplayed = obstacle;
}

void LCDScreen::displayPosition(const Position &position) {
	acquire();
	lockedDisplayPosition(position);
	release();
}

void LCDScreen::lockedDisplayPosition(const Position &position) {
	int x = (int) position.x;
	int y = (int) position.y;
	lcd.setCursor(0, 3);
	int n = lcd.print(x);
	n += lcd.print(',');
	n += lcd.print(y);
	n += lcd.print(',');
	n += lcd.print(ANGLE_TO_DEGREES(position.angle));
	lockedEraseLineRemaining(n + 3);
}

void LCDScreen::lockedDisplayPosition() {
	if (!matchData.isValid()) {
		return;
	}
	const Position &position = matchData->getPosition();
	lockedDisplayPosition(position);
}

void LCDScreen::lockedDisplayScore() {
	if (!matchData.isValid()) {
		return;
	}
	int score = matchData->getScore();
	int x = score < 1000 ? (score < 100 ? (score < 10 ? 8 : 7) : 5) : 3;
	LCDBigFont lcdBigFont(lcd);
	lcdBigFont.setExtraSpace(true);
	lcdBigFont.setCursor(x, 0);
	lcdBigFont.print(score);
}

void LCDScreen::lockedDisplayTimer() {
	if (!matchData.isValid()) {
		return;
	}
	int remaining = matchData->getTimer().getRemainingTimeMS() / 1000;
	lcd.setCursor(17, 3);
	if (remaining < 100) {
		lcd.print(' ');
	}
	if (remaining < 10) {
		lcd.print(' ');
	}
	lcd.print(remaining);
}

void LCDScreen::setObstacle(bool value, const Point &position) {
	obstacle = value;
	obstaclePosition = position;
}

void LCDScreen::displayObstacle() {
	acquire();
	lockedDisplayObstacle();
	release();
}
