/*
 * AnalogEmergencyStop.h
 *
 *  Created on: 25 d�c. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_IO_ANALOGEMERGENCYSTOP_H_
#define DEVICES_IO_ANALOGEMERGENCYSTOP_H_

#include "devices/io/AnalogSensor.h"
#include "devices/io/EmergencyStop.h"

struct AnalogEmergencyStopConfig : AnalogSensorConfig {
	/** Seuil de d�tection en mV. */
	int thresholdMilliVolts;
	/** R�sistance R1 du diviseur de tension en amont (c�t� VCC) de l'entr�e. */
	int dividerR1;
	/** R�sistance R2 du diviseur de tension en aval (c�t� GND) de l'entr�e. */
	int dividerR2;
	AnalogEmergencyStopConfig(int analogChannel, int thresholdMilliVolts, int dividerR1, int dividerR2);
};

class AnalogEmergencyStop : public EmergencyStop, AnalogSensor {
private:
	AnalogEmergencyStopConfig& config;
	int getRawValue() const;

public:
	AnalogEmergencyStop(AnalogEmergencyStopConfig &config);
	virtual int getMilliVolts() const;
	virtual bool isPressed() const;
};

#endif /* DEVICES_IO_ANALOGEMERGENCYSTOP_H_ */
