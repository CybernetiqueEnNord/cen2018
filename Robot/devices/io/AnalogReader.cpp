/*
 * AnalogReader.cpp
 *
 *  Created on: 26 ao�t 2017
 *      Author: Emmanuel
 */

#include "AnalogReader.h"
#include "init/Peripherals.h"

#define ADC_GRP1_NUM_CHANNELS 4
// taille du tampon (puissance de 2 pour performance)
#define ADC_GRP1_BUFFER_LENGTH 16

const ADCConversionGroup AnalogReader::config = {
	.circular = TRUE,
	.num_channels = ADC_GRP1_NUM_CHANNELS,
	/* callbacks */
	.end_cb = AnalogReader::endReadCallback, .error_cb = NULL,
	/* hardware registers */
	.cr1 = 0,
	.cr2 = ADC_CR2_SWSTART,
	.smpr1 = 0,
	.smpr2 = ADC_SMPR2_SMP_AN0(ADC_SAMPLE_480) | ADC_SMPR2_SMP_AN1(ADC_SAMPLE_480) | ADC_SMPR2_SMP_AN2(ADC_SAMPLE_480) | ADC_SMPR2_SMP_AN3(ADC_SAMPLE_480),
	.sqr1 = ADC_SQR1_NUM_CH(ADC_GRP1_NUM_CHANNELS),
	.sqr2 = 0,
	.sqr3 = ADC_SQR3_SQ1_N(ADC_CHANNEL_M1) | ADC_SQR3_SQ2_N(ADC_CHANNEL_M2) | ADC_SQR3_SQ3_N(ADC_CHANNEL_POWER_MOTORS) | ADC_SQR3_SQ4_N(ADC_CHANNEL_POWER_ELECTRONIC)
};

adcsample_t AnalogReader::samples[ADC_GRP1_BUFFER_LENGTH * ADC_GRP1_NUM_CHANNELS];

int AnalogReader::values[ADC_GRP1_NUM_CHANNELS];

AnalogReader::AnalogReader() {
}

AnalogReader& AnalogReader::getInstance() {
	static AnalogReader instance;
	return instance;
}

void AnalogReader::endReadCallback(ADCDriver *adcDriver, adcsample_t *buffer, size_t count) {
	(void) adcDriver;

	// sommation
	int values[ADC_GRP1_NUM_CHANNELS] = { };
	for (unsigned int k = 0; k < count; k++) {
		for (int i = 0; i < ADC_GRP1_NUM_CHANNELS; i++) {
			values[i] += *buffer;
			buffer++;
		}
	}

	// valeur moyenne
	for (int i = 0; i < ADC_GRP1_NUM_CHANNELS; i++) {
		AnalogReader::values[i] = values[i] / count;
	}
}

void AnalogReader::start() {
	adcStartConversion(&ADCD1, &config, samples, ADC_GRP1_BUFFER_LENGTH);
}

void AnalogReader::init() {
	adcStart(&ADCD1, NULL);
}

int AnalogReader::getValue(int channel) const {
	return values[channel];
}
