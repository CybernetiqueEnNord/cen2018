/*
 * ObstacleDetector.h
 *
 *  Created on: 9 d�c. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_IO_OBSTACLEDETECTOR_H_
#define DEVICES_IO_OBSTACLEDETECTOR_H_

#include "odometry/Position.h"

class ObstacleDetector {
public:
	ObstacleDetector();
	virtual bool isObstacleDetected(const Position& position, bool movingBackward, Point& obstaclePosition);
};

#endif /* DEVICES_IO_OBSTACLEDETECTOR_H_ */
