/*
 * Console.h
 *
 *  Created on: 23 juil. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_IO_CONSOLE_H_
#define DEVICES_IO_CONSOLE_H_

#include "ch.h"
#include "hal.h"
#include "control/Navigation.h"

/**
 * Mode courant de la console.
 */
enum class ConsoleMode {
	/**
	 * Mode �mission � la demande.
	 */
	OnDemandSend,
	/**
	 * Mode �mission cyclique.
	 */
	CyclicSend,
	/**
	 * Mode TEST en attente de commande.
	 */
	Test,
	/**
	 * Mode TEST des codeurs.
	 */
	TestCoders,
	/**
	 * Mode TEST des moteurs.
	 */
	TestMotors,
	/**
	 * Mode TEST en lecture du NXT.
	 */
	TestNXTRead,
	/**
	 * Mode TEST en �criture du NXT.
	 */
	TestNXTWrite,
	/**
	 * Mode TEST de la balise.
	 */
	TestBeacon,
	/**
	 * Mode TEST des actionneurs.
	 */
	TestActuators,
	/**
	 * Mode TEST du bus de donn�es.
	 */
	TestDataBus
};

#define CONSOLE_MODE_INITIAL ConsoleMode::CyclicSend

/**
 * \brief Console d'interfa�age
 *
 * Console d'interfa�age et de commande externe.
 */
class Console {
private:
	int step = 0;
	ch_mutex mutex;
	ConsoleMode mode = CONSOLE_MODE_INITIAL;
	BaseSequentialStream *stream;
	Navigation &navigation;

	void beginSend();
	void endSend();
	void handleTest();
	void lockedSendBuffer();
	void lockedSendCalibrationParameters();
	void lockedSendControlParameters();
	void lockedSendCurrentPosition();
	void lockedSendData(const char *data);
	void lockedSendDebug();
	void lockedSendDetection();
	void lockedSendEnergyUsage();
	void lockedSendK();
	void lockedSendMotorCommands();
	void lockedSendObstaclePosition();
	void lockedSendParameters();
	void lockedSendPosition(const Position &p, bool separator);
	void lockedSendState();
	void lockedWriteBool(bool b);
	void lockedWriteChar(char c);
	void lockedWriteIntParameter(int value, bool separator);
	void lockedWriteText(const char *data);
	void testActuators();
	void testBeacon();
	void testCoders();
	void testCubesActuator();
	void testDataBus();
	void testMotors();
	void testNXTRead();
	void testNXTWrite();
	void testWaterActuator();
	bool tryBeginSend();

public:
	/**
	 * Constructeur.
	 * @param [in] stream pointeur sur la flux de donn�es
	 * @param [in] navigation gestionnaire de la navigation
	 */
	Console(BaseSequentialStream *stream, Navigation &navigation);
	/**
	 * Effectue l'envoi des donn�es de mani�re cyclique.
	 * Chaque appel effectue une it�ration dans le cycle.
	 * Le cycle est automatiquement red�marr� lorsque toutes les �tapes ont �t� effectu�es.
	 */
	void cyclicSend();
	/**
	 * Entre en mode TEST.
	 */
	void enterTestMode();
	/**
	 * Sort du mode TEST.
	 */
	void exitTestMode();
	/**
	 * D�termine si la console est en mode TEST.
	 *
	 * @return true si la console est en mode TEST, false sinon
	 */
	bool isInTest() const;
	/**
	 * Affiche les commandes utilisables en mode TEST.
	 */
	void printTestCommands();
	/**
	 * Effectue le balayage du bus I2C.
	 */
	void scanI2CBus();
	/**
	 * Envoie les param�tres de calibration de l'asservissement.
	 */
	void sendCalibrationParameters();
	/**
	 * Envoie les param�tres d'asservissement sur la console.
	 */
	void sendControlParameters();
	/**
	 * Envoie les donn�es sp�cifi�es sur la console.
	 * Le caract�re LF est automatiquement �mis apr�s les donn�es.
	 * @param [in] data les donn�es � envoyer
	 */
	void sendData(const char *data);
	/**
	 * Envoie le nom du p�riph�rique
	 * @param [in] name le nom � envoyer
	 */
	void sendDeviceName(const char *name);
	/**
	 * Envoie les donn�es de d�bogage.
	 */
	void sendDebug();
	/**
	 * Envoie les donn�es de d�tection d'obstacle.
	 */
	void sendDetection();
	/**
	 * Envoie les param�tres de simulation de l'asservissement.
	 */
	void sendK();
	/**
	 * Envoie les donn�es sp�cifi�es sous la forme clef=valeur
	 * @param [in] key le nom de la clef
	 * @param [in] value la valeur associ�e � la clef
	 */
	void sendKeyValue(const char *key, const char *value);
	/**
	 * Envoie les donn�es sp�cifi�es sous la forme clef=valeur
	 * @param [in] key le nom de la clef
	 * @param [in] value la valeur associ�e � la clef
	 */
	void sendKeyValue(const char *key, int value);
	/**
	 * Envoie le message textuel sp�cifi�.
	 * @param [in] message le message � envoyer
	 * @param [in] data les donn�es � envoyer
	 */
	void sendMessage(const char *message, const char *data = nullptr);
	/**
	 * Envoie les param�tres des commandes moteur.
	 */
	void sendMotorCommands();
	/**
	 * Envoie la position courante de l'obstacle.
	 */
	void sendObstaclePosition();
	/**
	 * Envoie la position courante.
	 */
	void sendPosition();
	/**
	 * Envoie le r�sultat du repositionnement.
	 * @param [in] oldPosition la position avant recallage
	 */
	void sendRepositionStatus(const Position &oldPosition);
	/**
	 * Envoie les options de simulation.
	 */
	void sendSimulationFlags(int value);
	/**
	 * Envoie l'�tat de l'asservissement.
	 */
	void sendState();
	/**
	 * Active l'envoi cyclique.
	 */
	void setModeCyclic();
	/**
	 * Active l'envoi � la demande d'un cycle.
	 */
	void setModeOnDemand();
	/**
	 * D�finit le mode TEST courant.
	 * @param [in] testMode le mode test courant (doit �tre > ConsoleMode::Test)
	 */
	void setTest(ConsoleMode testMode);
	/**
	 * D�finit les param�tres de calibration de l'asservissement.
	 * @param [in] stepsByTurn nombre de pas par tour
	 * @param [in] stepsByMeter nombre de pas par m�tre
	 */
	void setCalibrationParameters(int stepsByTurn, int stepsByMeter);
	/**
	 * D�finit les param�tres d'asservissement.
	 * @param [in] linearP coefficient P pour l'asservissement lin�aire
	 * @param [in] linearD coefficient D pour l'asservissement lin�aire
	 * @param [in] linearWeight poids total des coefficients P et D de l'asservissement lin�aire
	 * @param [in] angularP coefficient P pour l'asservissement angulaire
	 * @param [in] angularD coefficient D pour l'asservissement angulaire
	 * @param [in] angularWeight poids total des coefficients P et D de l'asservissement angulaire
	 */
	void setControlParameters(int linearP, int linearD, int linearWeight, int angularP, int angularD, int angularWeight);
	/**
	 * Effectue un reset logiciel.
	 */
	void systemReset();
};

#endif /* DEVICES_IO_CONSOLE_H_ */
