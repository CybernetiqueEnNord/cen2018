/*
 * LCDScreen.h
 *
 *  Created on: 26 oct. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_IO_LCDSCREEN_H_
#define DEVICES_IO_LCDSCREEN_H_

#include "Screen.h"

#include <climits>

#include "devices/lcd/LCDQY2004A.h"

class LCDScreen : public Screen {
private:
	LCD_QY2004A &lcd;
	unsigned int stepsCount = 0;
	bool obstacle = false;
	bool obstacleDisplayed = false;
	Point obstaclePosition;

	ch_mutex mutex;
	unsigned int mutexCount = 0;

	void acquire();
	void release();

	void lockedDisplayObstacle();
	void lockedDisplayPosition();
	void lockedDisplayPosition(const Position &position);
	void lockedDisplayScore();
	void lockedDisplayTestStep(int step);
	void lockedDisplayTimer();
	void lockedEraseLine(int count);
	void lockedEraseLineFrom(int col, int row, int count = 0);
	void lockedEraseLineRemaining(int start = 0);

	virtual void lockedDisplayMessage(const char *message, int x = 0);
	virtual void lockedDisplayTest(int step, const char *name);
	virtual void lockedDisplayTestStatus(const char *status, const char *message = NULL);

protected:
	virtual void displayObstacle();

public:
	LCDScreen(LCD_QY2004A &lcd);
	void initialize();
	virtual void displayMessage(const char *message, int x = 0);
	virtual void displayPosition(const Position &position);
	virtual void displayTest(int step, const char *name);
	virtual void displayTestStatus(const char *status, const char *message = NULL);
	virtual void setObstacle(bool value, const Point &position);
	virtual void startTests(int stepsCount);
	virtual void stopTests();
	virtual void update();
};

#endif /* DEVICES_IO_LCDSCREEN_H_ */
