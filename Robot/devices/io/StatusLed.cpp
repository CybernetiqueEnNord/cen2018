/*
 * StatusLed.cpp
 *
 *  Created on: 22 juil. 2017
 *      Author: Emmanuel
 */

#include "devices/io/StatusLed.h"

#include "hal.h"

StatusLed::StatusLed() {
	chSemObjectInit(&lock, 0);
}

StatusLed& StatusLed::getInstance() {
	static StatusLed instance;
	return instance;
}

void StatusLed::blink(unsigned int msOn, unsigned int msOff, unsigned int count, bool wait) {
	this->msOn = msOn;
	this->msOff = msOff;
	this->count = count;
	if (current) {
		chSemReset(&lock, 0);
	}
	if (wait) {
		while (count-- > 0) {
			chThdSleepMilliseconds(msOn + msOff);
		}
	}
}

void StatusLed::blinkContinuously(unsigned int msOn, unsigned int msOff) {
	blink(msOn, msOff, 0);
}

bool StatusLed::updateLed() {
	palSetPad(GPIOA, GPIOA_LED_GREEN);
	if (chSemWaitTimeout(&lock, MS2ST(msOn)) != MSG_TIMEOUT) {
		return true;
	}

	palClearPad(GPIOA, GPIOA_LED_GREEN);
	if (chSemWaitTimeout(&lock, MS2ST(msOff)) != MSG_TIMEOUT) {
		return true;
	}

	return false;
}

void StatusLed::run() {
	current = chThdGetSelfX();
	for (;;) {
		bool interrupted = updateLed();
		if (interrupted) {
			// si un appel � blink est survenu, on boucle sur les nouveaux param�tres
			continue;
		}

		switch (count) {
			case 0:
				// clignotement continu, on boucle
				break;

			case 1:
				// clignotements effectu�s, on se met attente
				chSemWait(&lock);
				break;

			default:
				count--;
		}
	}
	current = NULL;
}
