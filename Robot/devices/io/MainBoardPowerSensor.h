/*
 * MainBoardPowerSensor.h
 *
 *  Created on: 26 f�vr. 2018
 *      Author: Emmanuel
 */

#ifndef DEVICES_IO_MAINBOARDPOWERSENSOR_H_
#define DEVICES_IO_MAINBOARDPOWERSENSOR_H_

#include "devices/io/AnalogSensor.h"

class MainBoardPowerSensor : public AnalogSensor {
public:
	MainBoardPowerSensor(const AnalogSensorConfig &config);
};

#endif /* DEVICES_IO_MAINBOARDPOWERSENSOR_H_ */
