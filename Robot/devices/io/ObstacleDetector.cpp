/*
 * ObstacleDetector.cpp
 *
 *  Created on: 9 d�c. 2017
 *      Author: Emmanuel
 */

#include "devices/io/ObstacleDetector.h"

ObstacleDetector::ObstacleDetector() {
}

bool ObstacleDetector::isObstacleDetected(const Position& position, bool movingBackward, Point& obstaclePosition) {
	(void) position;
	(void) movingBackward;
	obstaclePosition = Point(0, 0);
	return false;
}
