/*
 * StartDetector.h
 *
 *  Created on: 4 oct. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_IO_STARTDETECTOR_H_
#define DEVICES_IO_STARTDETECTOR_H_

/**
 * \brief D�tection du d�but de match
 *
 * Classe de base de la d�tection du d�but de match.
 */
class StartDetector {
public:
	/**
	 * Constructeur.
	 */
	StartDetector();
	/**
	 * \brief Indicateur de d�marrage du match
	 *
	 * D�termine si le match a d�marr�.
	 * @return true si le match a d�marr�, false sinon
	 */
	virtual bool isStarted();
};

#endif /* DEVICES_IO_STARTDETECTOR_H_ */
