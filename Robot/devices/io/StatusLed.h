/*
 * StatusLed.h
 *
 *  Created on: 22 juil. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_IO_STATUSLED_H_
#define DEVICES_IO_STATUSLED_H_

#include "ch.h"

/**
 * \brief LED de statut
 *
 * Gestionnaire de la LED de statut.
 * Permet de faire clignoter la LED selon l'�tat courant.
 * L'objet est un singleton.
 */
class StatusLed {
private:
	ch_semaphore lock;
	thread_t *current = NULL;

	unsigned int count = 0;
	unsigned int msOn = 500;
	unsigned int msOff = 500;

	bool updateLed();
protected:
	/**
	 * Constructeur.
	 */
	StatusLed();
public:
	StatusLed(const StatusLed&) = delete;
	/**
	 * Renvoie l'instance du singleton.
	 * @return une r�f�rence � l'instance
	 */
	static StatusLed& getInstance();
	void operator= (const StatusLed&) = delete;
	/**
	 * Fait clignoter la LED de statut le nombre de fois sp�cifi� avec les dur�es d'allumage et d'extinction sp�cifi�es.
	 * L'appel peut �tre synchrone ou asynchrone.
	 * @param [in] msOn la dur�e d'allumage en millisecondes
	 * @param [in] msOff la dur�e d'extinction en millisecondes
	 * @param [in] count le nombre de clignotements
	 * @param [in] wait drapeau indiquant si l'appel est synchrone (true) ou asynchrone (false)
	 */
	void blink(unsigned int msOn, unsigned int msOff, unsigned int count, bool wait = false);
	/**
	 * Fait clignoter la LED de statut en continu avec les dur�es d'allumage et d'extinction sp�cifi�es.
	 * L'appel est asynchrone et retourne imm�diatement.
	 * @param [in] msOn la dur�e d'allumage en millisecondes
	 * @param [in] msOff la dur�e d'extinction en millisecondes
	 */
	void blinkContinuously(unsigned int msOn, unsigned int msOff);
	/**
	 * Ex�cute le thread de gestion du clignotement de la LED de statut.
	 */
	void run();
};

#endif /* DEVICES_IO_STATUSLED_H_ */
