/*
 * Screen.h
 *
 *  Created on: 26 oct. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_IO_SCREEN_H_
#define DEVICES_IO_SCREEN_H_

#include "match/MatchData.h"
#include "utils/Pointer.h"

#include <cstddef>

class Screen {
protected:
	Screen();

public:
	Pointer<const MatchData> matchData;

	virtual void displayMessage(const char *message, int x = 0) = 0;
	virtual void displayObstacle() = 0;
	virtual void displayPosition(const Position &position) = 0;
	virtual void displayTest(int step, const char *name) = 0;
	virtual void displayTestStatus(const char *status, const char *message = NULL) = 0;
	virtual void setObstacle(bool value, const Point &position) = 0;
	virtual void startTests(int stepsCount) = 0;
	virtual void stopTests() = 0;
	virtual void update() = 0;
};

#endif /* DEVICES_IO_SCREEN_H_ */
