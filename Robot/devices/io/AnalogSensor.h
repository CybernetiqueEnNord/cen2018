/*
 * AnalogSensor.h
 *
 *  Created on: 26 f�vr. 2018
 *      Author: Emmanuel
 */

#ifndef DEVICES_IO_ANALOGSENSOR_H_
#define DEVICES_IO_ANALOGSENSOR_H_

struct AnalogSensorConfig {
	int analogChannel;
	int multiplier;
	int divider;
	AnalogSensorConfig(int analogChannel, int multiplier = 1, int divider = 1);
};

class AnalogSensor {
protected:
	const AnalogSensorConfig &config;
	AnalogSensor(const AnalogSensorConfig &config);
	int getRawValue() const;

public:
	virtual int getMilliVolts() const;
};

#endif /* DEVICES_IO_ANALOGSENSOR_H_ */
