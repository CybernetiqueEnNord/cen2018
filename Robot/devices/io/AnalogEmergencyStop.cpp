/*
 * AnalogEmergencyStop.cpp
 *
 *  Created on: 25 d�c. 2017
 *      Author: Emmanuel
 */

#include "devices/io/AnalogEmergencyStop.h"

#include "ch.h"

AnalogEmergencyStopConfig::AnalogEmergencyStopConfig(int analogChannel, int thresholdMilliVolts, int dividerR1, int dividerR2) :
		AnalogSensorConfig(analogChannel), thresholdMilliVolts(thresholdMilliVolts), dividerR1(dividerR1), dividerR2(dividerR2) {
}

AnalogEmergencyStop::AnalogEmergencyStop(AnalogEmergencyStopConfig &config) :
		AnalogSensor(config), config(config) {
}

bool AnalogEmergencyStop::isPressed() const {
	int value = getMilliVolts();
	int count = 10;
	while (value < config.thresholdMilliVolts) {
		if (--count == 0) {
			return true;
		}
		chThdSleepMilliseconds(1);
		value = getMilliVolts();
	}
	return false;
}

int AnalogEmergencyStop::getMilliVolts() const {
	int value = AnalogSensor::getMilliVolts();
	value = (value * (config.dividerR1 + config.dividerR2)) / config.dividerR2;
	return value;
}
