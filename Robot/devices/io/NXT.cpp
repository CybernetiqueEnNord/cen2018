/*
 * NXT.cpp
 *
 *  Created on: 31 d�c. 2017
 *      Author: Emmanuel
 */

#include "devices/io/NXT.h"

NXTConfig::NXTConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address) :
		I2CDeviceConfig(i2cDriver, i2cConfig, address) {
}

NXT::NXT(NXTConfig &config) :
		I2CDevice(config), config(config) {
}

bool NXT::sendAction(uint8_t command) {
	uint8_t in;
	i2cAcquireBus();
	bool result = i2cWrite(&command, sizeof(command), &in, sizeof(in));
	i2cReleaseBus();
	return result;
}

int NXT::readData() {
	uint8_t in;
	i2cAcquireBus();
	bool result = i2cRead(&in, sizeof(in));
	i2cReleaseBus();
	return result ? in : -1;
}
