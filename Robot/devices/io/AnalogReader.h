/*
 * AnalogReader.h
 *
 *  Created on: 26 ao�t 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_IO_ANALOGREADER_H_
#define DEVICES_IO_ANALOGREADER_H_

#include "hal.h"

#define ADC_VOLTAGE_REF 3300
#define ADC_RESOLUTION 12
#define ADC_TO_MILLIVOLTS(x) ((x * ADC_VOLTAGE_REF) >> ADC_RESOLUTION)

/**
 * \brief Acquisition analogique
 *
 * Gestionnaire de l'acquisition des signaux analogiques.
 * L'objet est un singleton.
 */
class AnalogReader {
private:
	static const ADCConversionGroup config;
	static adcsample_t samples[];
	static int values[];

	static void endReadCallback(ADCDriver *adcp, adcsample_t *buffer, size_t n);
protected:
	/**
	 * Constructeur.
	 */
	AnalogReader();
public:
	AnalogReader(const AnalogReader&) = delete;
	/**
	 * Renvoie l'instance du singleton.
	 * @return une r�f�rence � l'instance
	 */
	static AnalogReader& getInstance();
	void operator= (const AnalogReader&) = delete;
	/**
	 * Initialisation
	 */
	void init();
	/**
	 * D�marre l'acquisition.
	 */
	void start();
	/**
	 * Renvoie la valeur de la mesure associ�e au canal sp�cifi�.
	 * @param channel [in] le canal
	 * @return la valeur de la mesure associ�e au canal sp�cifi�
	 */
	int getValue(int channel) const;
};

#endif /* DEVICES_IO_ANALOGREADER_H_ */
