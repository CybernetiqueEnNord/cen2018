/*
 * AnalogSensor.cpp
 *
 *  Created on: 26 f�vr. 2018
 *      Author: Emmanuel
 */

#include "devices/io/AnalogSensor.h"

#include "devices/io/AnalogReader.h"

AnalogSensorConfig::AnalogSensorConfig(int analogChannel, int multiplier, int divider) :
		analogChannel(analogChannel), multiplier(multiplier), divider(divider) {
}

AnalogSensor::AnalogSensor(const AnalogSensorConfig &config) :
		config(config) {
}

int AnalogSensor::getRawValue() const {
	AnalogReader &reader = AnalogReader::getInstance();
	int value = reader.getValue(config.analogChannel);
	return value;
}

int AnalogSensor::getMilliVolts() const {
	int value = getRawValue();
	value = ADC_TO_MILLIVOLTS(value);
	value *= config.multiplier;
	value /= config.divider;
	return value;
}
