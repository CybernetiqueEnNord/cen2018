/*
 * Console.cpp
 *
 *  Created on: 23 juil. 2017
 *      Author: Emmanuel
 */

#include "devices/io/Console.h"

#include <hal_streams.h>
#include <stdlib.h>
#include <cstring>

#include "devices/actuators/ActuatorsTest.h"
#include "devices/beacon/Beacon2018.h"
#include "devices/databus/DataBus.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"
#include "devices/io/NXT.h"
#include "devices/io/StatusLed.h"
#include "math/cenMath.h"
#include "odometry/Position.h"
#include "utils/Text.h"

Console::Console(BaseSequentialStream *stream, Navigation &navigation) :
		stream(stream), navigation(navigation) {
	chMtxObjectInit(&mutex);
}

void Console::lockedSendData(const char *data) {
	lockedWriteText(data);
	lockedSendBuffer();
}

void Console::lockedSendBuffer() {
	lockedWriteChar('\n');
}

void Console::lockedWriteText(const char *data) {
	size_t n = strlen(data);
	if (n > 0) {
		streamWrite(stream, (const unsigned char* ) data, n);
	}
}

void Console::lockedSendDetection() {
	//'F'
	//Appel d'une Macro: commWriteChar('F')
	lockedWriteChar('F');

	//Appel d'une Macro
	//Appel d'une Macro: commWriteBool(flag_emergencyBrake)
	PositionControl &control = navigation.getControl();
	lockedWriteBool(control.obstacleAvoidance);

	//Appel d'une Macro
	//Appel d'une Macro: commWriteBool(in_obstacle != 0)
	lockedWriteBool(control.isObstacleDetected());
	lockedSendBuffer();
}

void Console::lockedSendObstaclePosition() {
	//'F'
	//Appel d'une Macro: commWriteChar('F')
	lockedWriteChar('F');

	//Appel d'une Macro
	//Appel d'une Macro: commWriteBool(flag_emergencyBrake)
	PositionControl &control = navigation.getControl();
	lockedWriteBool(control.obstacleAvoidance);

	//Appel d'une Macro
	//Appel d'une Macro: commWriteBool(in_obstacle != 0)
	lockedWriteBool(control.isObstacleDetected());

	const Point &p = control.getObstaclePosition();
	lockedWriteIntParameter((int) p.x, true);
	lockedWriteIntParameter((int) p.y, true);

	lockedSendBuffer();
}

void Console::lockedSendCurrentPosition() {
	PositionControl &control = navigation.getControl();
	const Position &p = control.getPosition();
	lockedWriteChar('p');
	lockedSendPosition(p, false);
	lockedSendBuffer();
}

void Console::lockedSendPosition(const Position &p, bool separator) {
	lockedWriteIntParameter((int) p.x, separator);
	lockedWriteIntParameter((int) p.y, true);
	lockedWriteIntParameter((int) (ANGLE_TO_DEGREES(p.angle) * 10), true);
}

void Console::lockedWriteChar(char c) {
	streamWrite(stream, (unsigned char* ) &c, 1);
}

void Console::lockedWriteBool(bool b) {
	lockedWriteChar(b ? '1' : '0');
}

void Console::lockedWriteIntParameter(int value, bool parameterSeparator) {
	//D�cision
	//D�cision: .separator?
	if (parameterSeparator) {
		//Appel d'une Macro
		//Appel d'une Macro: commWriteChar(';')
		lockedWriteChar(';');
	}

	//Code C
	//Code C:
	char data[12];
	itoa(value, data, 10);

	//Code C
	//Code C:
	lockedWriteText(data);
}

void Console::lockedSendK() {
	//Appel d'une Macro
	//Appel d'une Macro: commWriteChar('K')
	lockedWriteChar('K');

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(k, false)
	PositionControl &control = navigation.getControl();
	lockedWriteIntParameter(control.k, false);
	lockedSendBuffer();
}

void Console::lockedSendCalibrationParameters() {
	//Appel d'une Macro
	//Appel d'une Macro: commWriteChar('Q')
	lockedWriteChar('Q');

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(pas_par_m32, false)
	PositionControl &control = navigation.getControl();
	lockedWriteIntParameter(control.stepsByMeter, false);

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(pas_par_tour32, true)
	lockedWriteIntParameter(control.stepsByTurn, true);
	lockedSendBuffer();
}

void Console::lockedSendState() {

	enum class GlobalState {
		Init, Ready, Busy, Error
	};

	//D�finitions des variables locales
	GlobalState state = GlobalState::Ready;

	//D�cision
	//D�cision: etat_deplacement > 0 || command != COMMAND_MOVE?
	PositionControl &control = navigation.getControl();
	if (control.getState() > MoveState::StandBy || navigation.getCommand() != MoveCommand::Idle) {
		//Calcul
		//Calcul:
		//  .state = STATE_BUSY
		state = GlobalState::Busy;
	} else {
		//D�cision
		//D�cision: echec_deplacement?
		if (control.hasMoveFailed()) {
			//Calcul
			//Calcul:
			//  .state = STATE_ERROR
			state = GlobalState::Error;
		}
	}

	//Appel d'une Macro
	//Appel d'une Macro: commWriteChar('S')
	lockedWriteChar('S');

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(.state, false)
	int value = static_cast<int>(state);
	lockedWriteIntParameter(value, false);
	lockedSendBuffer();
}

void Console::lockedSendMotorCommands() {
	//Appel d'une Macro
	//Appel d'une Macro: commWriteChar(MSG_CODERS)
	lockedWriteChar('C');

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(out_correctionGauche, false)
	int leftCommand;
	int rightCommand;
	PositionControl &control = navigation.getControl();
	control.getMotorsCommands(leftCommand, rightCommand);
	lockedWriteIntParameter(leftCommand, false);

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(out_correctionDroite, true)
	lockedWriteIntParameter(rightCommand, true);

	//Appel d'une Macro
	//Appel d'une Macro: commSendBuffer()
	lockedSendBuffer();
}

void Console::lockedSendDebug() {
	//Appel d'une Macro
	//Appel d'une Macro: commWriteChar('D')
	lockedWriteChar('D');

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(etat_deplacement, false)
	PositionControl &control = navigation.getControl();
	MoveState moveState = control.getState();
	int value = static_cast<int>(moveState);
	lockedWriteIntParameter(value, false);

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(vitesse, true)
	unsigned int speed = control.getSpeedIndex();
	lockedWriteIntParameter(speed, true);

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(errorState, true)
	MoveError error = control.getError();
	value = static_cast<int>(error);
	lockedWriteIntParameter(value, true);

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(command, true)
	MoveCommand command = navigation.getCommand();
	value = static_cast<int>(command);
	lockedWriteIntParameter(value, true);

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(consigneDistance16, true)
	int distanceOrder = control.getDistanceOrder();
	lockedWriteIntParameter(distanceOrder, true);

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(mesureDistance32, true)
	int distanceTravelled = control.getDistanceTravelled();
	lockedWriteIntParameter(distanceTravelled, true);

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(debugValue1, true)
	lockedWriteIntParameter(0, true);

	//Appel d'une Macro
	//Appel d'une Macro: commWriteIntParameter(debugValue2, true)
	lockedWriteIntParameter(0, true);

	int cpuLoad = control.getCPULoad();
	lockedWriteIntParameter(cpuLoad, true);

	//Appel d'une Macro
	//Appel d'une Macro: commSendBuffer()
	lockedSendBuffer();
}

void Console::cyclicSend() {
	if (mode >= ConsoleMode::Test) {
		handleTest();
		return;
	}

	// si un envoi est d�j� en cours, on ne bloque pas et on reporte l'envoi
	if (!tryBeginSend()) {
		return;
	}

	step++;
	switch (step) {
		case 1:
			lockedSendCurrentPosition();
			break;

		case 3:
			lockedSendState();
			break;

		case 5:
			lockedSendMotorCommands();
			break;

		case 7:
			lockedSendDebug();
			break;

		case 9:
			lockedSendDetection();
			break;

		case 11:
			lockedSendEnergyUsage();
			break;

		case 12:
			if (mode == ConsoleMode::CyclicSend) {
				step = 0;
			}
			break;

		case 13:
			// Mode � la demande
			step = 12;
			break;
	}

	endSend();
}

bool Console::tryBeginSend() {
	return chMtxTryLock(&mutex);
}

void Console::beginSend() {
	chMtxLock(&mutex);
}

void Console::endSend() {
	chMtxUnlock(&mutex);
}

void Console::sendData(const char *data) {
	beginSend();
	lockedSendData(data);
	endSend();
}

void Console::sendKeyValue(const char *key, const char *value) {
	beginSend();
	lockedWriteText(key);
	lockedWriteChar('=');
	lockedWriteText(value);
	lockedSendBuffer();
	endSend();
}

void Console::sendKeyValue(const char *key, int value) {
	beginSend();
	lockedWriteText(key);
	lockedWriteChar('=');
	lockedWriteIntParameter(value, false);
	lockedSendBuffer();
	endSend();
}

void Console::sendDebug() {
	beginSend();
	lockedSendDebug();
	endSend();
}

void Console::sendDetection() {
	beginSend();
	lockedSendDetection();
	endSend();
}

void Console::sendK() {
	beginSend();
	lockedSendK();
	endSend();
}

void Console::sendMotorCommands() {
	beginSend();
	lockedSendMotorCommands();
	endSend();
}

void Console::sendPosition() {
	beginSend();
	lockedSendCurrentPosition();
	endSend();
}

void Console::sendObstaclePosition() {
	beginSend();
	lockedSendCurrentPosition();
	lockedSendObstaclePosition();
	endSend();
}

void Console::sendState() {
	beginSend();
	lockedSendState();
	endSend();
}

void Console::sendSimulationFlags(int value) {
	beginSend();
	lockedWriteChar('O');
	lockedWriteIntParameter(value, false);
	lockedSendBuffer();
	endSend();
}

void Console::enterTestMode() {
	setTest(ConsoleMode::Test);

	beginSend();
	lockedWriteText("Test mode");
	lockedSendBuffer();
	endSend();

	StatusLed &statusLed = StatusLed::getInstance();
	statusLed.blinkContinuously(1000, 1000);
}

void Console::exitTestMode() {
	step = 0;
	mode = CONSOLE_MODE_INITIAL;
	PositionControl &control = navigation.getControl();
	control.setOverrideMotorsCommands(false, 0, 0);

	StatusLed &statusLed = StatusLed::getInstance();
	statusLed.blinkContinuously(50, 50);
}

void Console::printTestCommands() {
	beginSend();
	lockedWriteText("A - test actuators\n");
	lockedWriteText("B - test beacon\n");
	lockedWriteText("C - test coders\n");
	lockedWriteText("D - test databus\n");
	lockedWriteText("M - test motors\n");
	lockedWriteText("NR - test NXT read\n");
	lockedWriteText("NW - test NXT write\n");
	lockedWriteText("U - unit tests\n");
	lockedWriteText("i - scan i2c bus\n");
	lockedWriteChar('\n');
	lockedWriteText("s - stop running test\n");
	lockedWriteText("x - exit test mode\n");
	lockedSendBuffer();
	endSend();
}

void Console::handleTest() {
	switch (mode) {
		case ConsoleMode::TestCoders:
			testCoders();
			break;

		case ConsoleMode::TestMotors:
			testMotors();
			break;

		case ConsoleMode::TestNXTRead:
			testNXTRead();
			break;

		case ConsoleMode::TestNXTWrite:
			testNXTWrite();
			break;

		case ConsoleMode::TestBeacon:
			testBeacon();
			break;

		case ConsoleMode::TestActuators:
			testActuators();
			break;

		case ConsoleMode::TestDataBus:
			testDataBus();
			break;

		default:
			break;
	}
}

void Console::testActuators() {
	ActuatorsTest::run(*this, step);
}

void Console::testBeacon() {
	extern Beacon2018 beacon;

	if (step == 0) {
		sendData("beacon test");
		if (beacon.isPresent()) {
			sendData("present");
		} else {
			sendData("not present");
		}
		beacon.initialize();
		if (beacon.isAvailable()) {
			const char *version = beacon.getVersion();
			sendData(version);
			step++;
		} else {
			sendData("unavailable");
		}
		step++;
	}

	if (step == 1) {
		return;
	}

	if (step-- == 2) {
		beacon.update(Beacon2018DataType::All);
		const Beacon2018Data &data = beacon.getData();
		sendData("beacon data");
		sendKeyValue("obstacle", static_cast<int>(data.obstacle));
		sendKeyValue("started", data.started ? "yes" : "no");
		sendKeyValue("value1", data.hmiValue1);
		sendKeyValue("value2", data.hmiValue2);
		sendData("");

		const BeaconSensorData *sensorData = beacon.getSensorData();
		sendData("sensor data");
		int i = 0;
		while (sensorData[i].configured && i < SENSORS_COUNT) {
			sendKeyValue("index", i);
			sendKeyValue("angle", sensorData[i].angleDegrees);
			sendKeyValue("distance", sensorData[i].distance);
			i++;
		}
		sendData("");

		step = 100;
	}
}

void Console::testCoders() {
	if (++step > 100) {
		PositionControl &control = navigation.getControl();
		Coder &leftCoder = control.getLeftCoder();
		Coder &rightCoder = control.getRightCoder();
		leftCoder.update();
		rightCoder.update();
		int left = leftCoder.getValue();
		int right = rightCoder.getValue();

		beginSend();
		lockedWriteText("coders: ");
		lockedWriteIntParameter(left, false);
		lockedWriteIntParameter(right, true);
		lockedSendBuffer();
		endSend();

		step = 0;
	}
}

void Console::testMotors() {
	PositionControl &control = navigation.getControl();
	switch (step++) {
		case 0:
			sendData("left forward");
			control.setOverrideMotorsCommands(true, 200, 0);
			break;

		case 1000:
			sendData("right forward");
			control.setOverrideMotorsCommands(true, 0, 200);
			break;

		case 2000:
			sendData("left backward");
			control.setOverrideMotorsCommands(true, -200, 0);
			break;

		case 3000:
			sendData("right backward");
			control.setOverrideMotorsCommands(true, 0, -200);
			break;

		case 4000:
			step = 0;
			break;
	}
}

void Console::testNXTWrite() {
	if (step == 0) {
		sendData("sending commands to NXT");
	}

	if (step % 200 == 0) {
		uint8_t command = step / 200;

		beginSend();
		lockedWriteText("command: ");
		lockedWriteIntParameter(command, false);
		lockedSendBuffer();
		endSend();

		extern NXT nxt;
		nxt.sendAction(command);
	}

	step++;
}

void Console::testNXTRead() {
	if (step == 0) {
		sendData("reading commands from NXT");
	}

	if (step % 200 == 0) {
		extern NXT nxt;
		int command = nxt.readData();

		beginSend();
		lockedWriteText("command: ");
		lockedWriteIntParameter(command, false);
		lockedSendBuffer();
		endSend();
	}

	step++;
}

void Console::testDataBus() {
	switch (step) {
		case 0:
			sendData("testing data bus");
			break;

		case 100: {
			extern DataBus dataBus;
			// appel de la m�thode virtuelle
			DataBus *vDataBus = &dataBus;
			vDataBus->executeTestSequence();
			break;
		}

		case 5000:
			step = 0;
			break;
	}
	step++;
}

void Console::setTest(ConsoleMode testMode) {
	if (testMode < ConsoleMode::Test) {
		return;
	}

	mode = testMode;
	step = 0;
	PositionControl &control = navigation.getControl();
	control.setOverrideMotorsCommands(true, 0, 0);
}

void Console::lockedSendEnergyUsage() {
	PositionControl &control = navigation.getControl();
	MotorsEnergyUsage usage = control.getEnergyUsage();
	lockedWriteChar('E');
	lockedWriteIntParameter(usage.leftCurrent, false);
	lockedWriteIntParameter(usage.leftCharge, true);
	lockedWriteIntParameter(usage.rightCurrent, true);
	lockedWriteIntParameter(usage.rightCharge, true);
	lockedSendBuffer();
}

void Console::scanI2CBus() {
	extern I2CConfig i2cConfig;
	sendData("i2c bus scan");
	i2cAcquireBus (&I2CD1);
	uint8_t in;
	for (uint8_t addr = 1; addr < 127; addr++) {
		msg_t error = i2cMasterReceiveTimeout(&I2CD1, addr, &in, 1, I2C_TIMEOUT);
		if (error == MSG_OK) {
			char c[5] = "0x";
			itoa(addr, &c[2], 16);
			sendKeyValue("address", c);
		} else if (error == MSG_TIMEOUT) {
			i2cStart(&I2CD1, &i2cConfig);
		}
	}
	i2cReleaseBus(&I2CD1);
	sendData("scan done");
}

void Console::setControlParameters(int linearP, int linearD, int linearWeight, int angularP, int angularD, int angularWeight) {
	PositionControl &control = navigation.getControl();
	control.distanceParameters.P = linearP;
	control.distanceParameters.D = linearD;
	control.distanceParameters.weight = linearWeight;
	control.angleParameters.P = angularP;
	control.angleParameters.D = angularD;
	control.angleParameters.weight = angularWeight;
	sendControlParameters();
}

void Console::setCalibrationParameters(int stepsByTurn, int stepsByMeter) {
	PositionControl &control = navigation.getControl();
	ControlConfig config(stepsByTurn, stepsByMeter);
	control.setControlConfig(config);
	sendCalibrationParameters();
}

void Console::sendControlParameters() {
	beginSend();
	lockedSendControlParameters();
	endSend();
}

void Console::sendCalibrationParameters() {
	beginSend();
	lockedSendCalibrationParameters();
	endSend();
}

void Console::lockedSendControlParameters() {
	PositionControl &control = navigation.getControl();
	lockedWriteChar('H');
	lockedWriteIntParameter(control.distanceParameters.P, false);
	lockedWriteIntParameter(control.distanceParameters.D, true);
	lockedWriteIntParameter(control.distanceParameters.weight, true);
	lockedWriteIntParameter(control.angleParameters.P, true);
	lockedWriteIntParameter(control.angleParameters.D, true);
	lockedWriteIntParameter(control.angleParameters.weight, true);
}

void Console::setModeOnDemand() {
	mode = ConsoleMode::OnDemandSend;
	step = 0;
}

void Console::setModeCyclic() {
	mode = ConsoleMode::CyclicSend;
	step = 0;
}

void Console::sendMessage(const char *message, const char *data) {
	beginSend();
	lockedWriteChar(':');
	lockedWriteText(message);
	if (data != nullptr) {
		lockedWriteChar(' ');
		lockedWriteText(data);
	}
	lockedSendBuffer();
	endSend();
}

bool Console::isInTest() const {
	bool result = mode >= ConsoleMode::Test;
	return result;
}

void Console::sendDeviceName(const char *name) {
	beginSend();
	lockedWriteChar('n');
	lockedSendData(name);
	endSend();
}

void Console::systemReset() {
	sendMessage("RESET");
	chThdSleepMilliseconds(200);
	NVIC_SystemReset();
}

void Console::sendRepositionStatus(const Position &oldPosition) {
	beginSend();
	lockedWriteChar('r');
	lockedSendPosition(oldPosition, false);
	PositionControl &control = navigation.getControl();
	const Position &p = control.getPosition();
	lockedSendPosition(p, true);
	lockedSendBuffer();
	endSend();
}
