/*
 * NXT.h
 *
 *  Created on: 31 d�c. 2017
 *      Author: Emmanuel
 */

#ifndef DEVICES_IO_NXT_H_
#define DEVICES_IO_NXT_H_

#include "devices/i2c/I2CDevice.h"

struct NXTConfig : I2CDeviceConfig {
	NXTConfig(I2CDriver *i2cDriver, I2CConfig *i2cConfig, uint8_t address);
};

class NXT : public I2CDevice {
private:
	NXTConfig &config;

public:
	NXT(NXTConfig &config);
	bool sendAction(uint8_t command);
	int readData();
};

#endif /* DEVICES_IO_NXT_H_ */
