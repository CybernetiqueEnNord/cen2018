/*
 * PinStartDetector.cpp
 *
 *  Created on: 4 oct. 2017
 *      Author: Emmanuel
 */

#include "devices/io/PinStartDetector.h"

#include "hal.h"
#include "init/Peripherals.h"

PinStartDetector::PinStartDetector(PinStartDetectorConfig &config) :
		StartDetector(), config(config) {
}

bool PinStartDetector::isStarted() {
	if (!started) {
		checkStarted();
	}
	return started;
}

void PinStartDetector::checkStarted() {
	if (palReadPad(config.port, config.pad) == config.startSignal) {
		startCounter++;
		if (startCounter >= config.threshold) {
			started = true;
		}
	} else {
		startCounter = 0;
	}
}
