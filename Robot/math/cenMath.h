/*
 * cenMath.h
 *
 *  Created on: 1 d�c. 2014
 *      Author: Emmanuel
 */

#ifndef MATH_CENMATH_H_
#define MATH_CENMATH_H_

#include <math.h>

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

#ifndef M_2PI
	#define M_2PI (M_PI * 2)
#endif

#ifndef M_PI_2
	#define M_PI_2 (M_PI / 2)
#endif

#define EPSILON 1e-4

#define ANGLE_TO_MILLI(x) ((int) (0.5 + (x) * 10000))
#define ANGLE_TO_DECIDEGREES(x) ((int) (0.5 + (x) / M_PI * 1800.0))
#define ANGLE_TO_DEGREES(x) ((x) / M_PI * 180.0)
#define ANGLE_TO_RADIANS(x) ((x) * M_PI / 180.0)

template <typename T> int signum(T val) {
	return (T(0) < val) - (val < T(0));
}

/**
 * \brief Diff�rence d'angles
 *
 * Renvoie la variation d'angle n�cessaire pour passer de l'orientation a � l'orientation b.
 * @param [in] a angle a en radians
 * @param [in] b angle b en radians
 * @return l'angle b - a normalis� en radians
 */
float diffAngle(float a, float b);
/**
 * Normalise l'angle dans l'intervalle [-PI ; PI[
 * @param [in] angle l'angle � normaliser
 */
float normalizeAngle(float angle);

bool equalsf(float a, float b);

#endif /* MATH_CENMATH_H_ */
