/*
 * cenMath.cpp
 *
 *  Created on: 3 d�c. 2014
 *      Author: Emmanuel
 */

#include <math.h>

#include "cenMath.h"

float normalizeAngle(float angle) {
	angle = fmodf(angle + M_PI, M_2PI);
	if (angle < 0) {
		angle += M_2PI;
	}
	return angle - M_PI;
}

float diffAngle(float a, float b) {
	float diff = fmodf(b - a + M_PI, M_2PI);
	if (diff < 0) {
		diff += M_2PI;
	}
	return diff - M_PI;
}

bool equalsf(float a, float b) {
	bool result = fabsf(a - b) < EPSILON;
	return result;
}
