/*
 * crt.cpp
 *
 *  Created on: 25 janv. 2015
 *      Author: Emmanuel
 */

extern "C" {

int _getpid() __attribute__((used));
int _kill(int, int) __attribute__((used));
void _exit(int) __attribute__((used));

int _getpid() {
	return 1;
}

int _kill(int, int) {
	return (-1);
}

void _exit(int) {
	while (1) {
	}
}

void __cxa_pure_virtual() {
	return;
}

}
