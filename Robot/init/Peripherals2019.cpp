#include "devices/actuators/2019/big/BigActuator.h"
#include "devices/actuators/2019/small/SmallActuator.h"
#include "i2c/ActuatorBig2019Commands.h"
#include "i2c/ActuatorSmall2019Commands.h"
#include "init/Peripherals.h"

#include "geometry/Rectangle.h"

extern I2CConfig i2cConfig;

SmallActuatorConfig smallActuatorConfig(&I2CD1, &i2cConfig, I2C_ADDRESS_ACTUATORSMALL2019);
SmallActuator smallActuator(smallActuatorConfig);

BigActuatorConfig bigActuatorConfig(&I2CD1, &i2cConfig, I2C_ADDRESS_ACTUATORBIG2019);
BigActuator bigActuator(bigActuatorConfig);

const Area& Peripherals::getObstaclesDetectionArea() {
	static Rectangle board = Rectangle(150, 150, 1280, 2400);
	return board;
}
