/*
 * Initialization.cpp
 *
 *  Created on: 29 d�c. 2017
 *      Author: Emmanuel
 */

#include "init/Initialization.h"

#include "control/PositionControl.h"
#include "devices/actuators/ActuatorsTest.h"
#include "devices/beacon/Beacon2018.h"
#include "devices/host/HostCapabilities2018.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"
#include "devices/io/AnalogEmergencyStop.h"
#include "devices/io/LCDScreen.h"
#include "devices/io/MainBoardPowerSensor.h"
#include "devices/io/StatusLed.h"
#include "init/Peripherals.h"
#include "utils/Text.h"

#define MIN_POWER_LOADED 16000
#define MIN_POWER_LOADED_ALARM 16500
#define MIN_POWER_UNLOADED 19000
#define MIN_POWER_UNLOADED_ALARM 19800
#define MIN_POWER_MAINBOARD 5000
#define MIN_POWER_MAINBOARD_ALARM 5800

#define BEACON_DISTANCE_THRESHOLD 600

// S�quence de test � partir de 1, 0 pour les tests � d�sactiver
#define TEST_VERSION 1
#define TEST_DEVICE 2
#define TEST_ARCHITECTURE 3
#define TEST_MAINBOARD_POWER 4
#define TEST_BEACON 5
#define TEST_BEACON_MODE 6
#define TEST_WATER_ACTUATOR 7
#define TEST_MOTOR_POWER_UNLOADED 8
#define TEST_START_PRESENCE 9
#define TEST_MOTOR_POWER_LOADED 0

#define test(name) do { if (query) { step++; return; } start(name); } while (false)
#define runTest(name, function) if (name != 0 && name == step) { function(); } else

Initialization::Initialization() {
}

void Initialization::checkArchitecture() {
#ifdef STM32F411xE
	HostArchitecture expected = HostArchitecture::STM32F411;
#endif
#ifdef STM32F446xx
	HostArchitecture expected = HostArchitecture::STM32F446;
#endif

	test("Architecture");
	const HostData &hostData = HostDevice::getHostData();
	if (hostData.getArchitecture() != expected) {
		fail();
		return;
	}
	success();
}

void Initialization::checkDevice() {
	test("Device");
	const HostData &hostData = HostDevice::getHostData();
	info(hostData.getName());
}

void Initialization::checkVersion() {
	test("Version");
	const char *version = HostDevice::getVersion();
	info(version);
}

void Initialization::checkPowerUnloaded() {
	test("Power");
	extern AnalogEmergencyStop emergencyStop;
	int value = emergencyStop.getMilliVolts();
	char buffer[16];
	itoa(value, buffer, 10);
	if (value > MIN_POWER_UNLOADED) {
		success();
		return;
	}
	fail(buffer);
}

void Initialization::checkPowerLoaded() {
	test("Power loaded");

	extern AnalogEmergencyStop emergencyStop;
	extern PositionControl control;

	// test de charge
	systime_t end = chVTGetSystemTime() + MS2ST(100);
	while (chVTGetSystemTime() < end) {
		control.setOverrideMotorsCommands(true, 250, 250);
		chThdSleepMilliseconds(1);
		control.setOverrideMotorsCommands(true, -250, -250);
		chThdSleepMilliseconds(1);
	}
	int loadedValue = emergencyStop.getMilliVolts();

	// d�sactivation + attente de stabilisation
	control.setOverrideMotorsCommands(true, 0, 0);
	chThdSleepMilliseconds(50);

	// r�initialisation et activation de l'asservissement
	control.reset();
	control.setOverrideMotorsCommands(false, 0, 0);

	char buffer[16];
	itoa(loadedValue, buffer, 10);
	if (loadedValue > MIN_POWER_LOADED) {
		success();
		return;
	}
	if (loadedValue > MIN_POWER_LOADED_ALARM) {
		warning(buffer);
		return;
	}
	fail(buffer);
}

void Initialization::checkMainBoardPower() {
	test("Main board power");

	extern MainBoardPowerSensor mainBoardPowerSensor;
	int value = mainBoardPowerSensor.getMilliVolts();
	char buffer[16];
	itoa(value, buffer, 10);
	if (value > MIN_POWER_MAINBOARD) {
		success();
		return;
	}
	if (value > MIN_POWER_MAINBOARD_ALARM) {
		warning(buffer);
		return;
		}
	fail(buffer);
}

void Initialization::checkBeacon() {
	test("Beacon");

	extern Beacon2018 beacon;
	if (!beacon.isPresent()) {
		fail("Absente");
		return;
	}
	success();
}

void Initialization::checkBeaconMode() {
	test("Beacon mode");

	extern Beacon2018 beacon;
	beacon.initialize();
	bool advanced = beacon.isAdvancedMode();
	// beep activ� en mode standard
	beacon.setBeepEnabled(!advanced);
	beacon.setThreshold(BEACON_DISTANCE_THRESHOLD);
	if(advanced)
	{
		success();
	}
	else
	{
		warning("Degraded");
	}
}

void Initialization::checkActuators() {
	test("Actionneurs");

	const HostData &hostData = HostDevice::getHostData();
	unsigned int capabilities = hostData.getCapabilities();

	if (!ActuatorsTest::checkActuators(capabilities)) {
		fail("Absents");
		return;
	}

	success();
}

void Initialization::checkStartPresence() {
	test("Start");

	extern Beacon2018 beacon;
	beacon.updateStatus();
	const Beacon2018Data &data = beacon.getData();
	if (!data.started) {
		fail("Retirer tirette");
	} else {
		success();
	}
}

int Initialization::getTestsCount() {
	step = 1;
	query = true;
	runSequence();
	query = false;
	int result = step - 1;
	step = 1;
	return result;
}

void Initialization::executeTests() {
	extern LCDScreen screen;
	screen.initialize();

	// r�cup�ration du nombre de tests
	int testsCount = getTestsCount();

	// ex�cution de la s�quence
	screen.startTests(testsCount);
	runSequence();

	screen.stopTests();
}

void Initialization::runSequence() {
	while (true) {
		runTest(TEST_VERSION, checkVersion)
		runTest(TEST_DEVICE, checkDevice)
		runTest(TEST_ARCHITECTURE, checkArchitecture)
		runTest(TEST_MAINBOARD_POWER, checkMainBoardPower)
		runTest(TEST_MOTOR_POWER_UNLOADED, checkPowerUnloaded)
		runTest(TEST_MOTOR_POWER_LOADED, checkPowerLoaded)
		runTest(TEST_BEACON, checkBeacon)
		runTest(TEST_BEACON_MODE, checkBeaconMode)
		runTest(TEST_WATER_ACTUATOR, checkActuators)
		runTest(TEST_START_PRESENCE, checkStartPresence)
			return;
	}
}

void Initialization::initializePeripherals() {
	const HostData &hostData = HostDevice::getHostData();

	// Configuration des E/S
	Peripherals::initialize(hostData);
	Peripherals::mapPins();
}

void Initialization::fail(const char *message) {
	extern LCDScreen screen;
	screen.displayTestStatus(" FAILED", message);

	StatusLed& statusLed = StatusLed::getInstance();
	statusLed.blinkContinuously(950, 50);
	chThdSleepSeconds(1);
}

void Initialization::start(const char *name) {
	extern LCDScreen screen;
	screen.displayTest(step, name);
}

void Initialization::info(const char *message) {
	extern LCDScreen screen;
	screen.displayTestStatus(" INFO", message);
	chThdSleepSeconds(1);
	step++;
}

void Initialization::warning(const char *message) {
	extern LCDScreen screen;
	screen.displayTestStatus(" WARN", message);
	chThdSleepSeconds(5);
	step++;
}

void Initialization::success() {
	extern LCDScreen screen;
	screen.displayTestStatus(" OK");
	chThdSleepMilliseconds(250);
	step++;
}
