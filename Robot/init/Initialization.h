/*
 * Initialization.h
 *
 *  Created on: 29 d�c. 2017
 *      Author: Emmanuel
 */

#ifndef INIT_INITIALIZATION_H_
#define INIT_INITIALIZATION_H_

#include <cstddef>
#include <stdint.h>

class Initialization {
private:
	uint8_t step = 0;
	bool query = false;

	void checkArchitecture();
	void checkBeacon();
	void checkBeaconMode();
	void checkDevice();
	void checkMainBoardPower();
	void checkPowerLoaded();
	void checkPowerUnloaded();
	void checkStartPresence();
	void checkVersion();
	void checkActuators();
	void fail(const char *message = NULL);
	int getTestsCount();
	void runSequence();
	void start(const char *name);
	void success();
	void warning(const char *message = NULL);
	void info(const char *message = NULL);

public:
	Initialization();
	void executeTests();
	void initializePeripherals();
};

#endif /* INIT_INITIALIZATION_H_ */
