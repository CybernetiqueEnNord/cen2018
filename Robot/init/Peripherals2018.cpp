#include "init/Peripherals.h"

#include "devices/actuators/2018/cubes/CubesActuator.h"
#include "devices/actuators/2018/water/WaterActuator.h"
#include "geometry/AreaDifference.h"
#include "geometry/Rectangle.h"

extern I2CConfig i2cConfig;

WaterActuatorConfig waterActuatorConfig(&I2CD1, &i2cConfig, 0x61);
WaterActuator waterActuator(waterActuatorConfig);

CubesActuatorConfig cubesActuatorConfig(&I2CD1, &i2cConfig, 0x62);
CubesActuator cubesActuator(cubesActuatorConfig);

const Area& Peripherals::getObstaclesDetectionArea() {
	int dispenserSize = 400;
	static Rectangle board = Rectangle(200, 200, 1600, 2600);
	static Rectangle dispenser1 = Rectangle(840 - dispenserSize / 2, 0, dispenserSize, dispenserSize);
	static Rectangle dispenser2 = Rectangle(2000 - dispenserSize, 610 - dispenserSize / 2, dispenserSize, dispenserSize);
	static Rectangle dispenser3 = Rectangle(840 - dispenserSize / 2, 3000 - dispenserSize, dispenserSize, dispenserSize);
	static Rectangle dispenser4 = Rectangle(2000 - dispenserSize, 2390 - dispenserSize / 2, dispenserSize, dispenserSize);
	static AreaDifference area1 = AreaDifference(board, dispenser1);
	static AreaDifference area2 = AreaDifference(area1, dispenser2);
	static AreaDifference area3 = AreaDifference(area2, dispenser3);
	static AreaDifference area = AreaDifference(area3, dispenser4);
	return area;
}
