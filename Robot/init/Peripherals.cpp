#include "init/Peripherals.h"

#include "devices/beacon/Beacon2018.h"
#include "devices/databus/DataBus.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"
#include "devices/io/AnalogEmergencyStop.h"
#include "devices/io/Console.h"
#include "devices/io/LCDScreen.h"
#include "devices/io/MainBoardPowerSensor.h"
#include "devices/io/NXT.h"
#include "devices/io/ObstacleDetector.h"
#include "devices/io/StatusLed.h"
#include "devices/lcd/LCDQY2004A.h"
#include "devices/motors/MotorVNH5019.h"
#include "parser/InputParser.h"

const HostData &data = HostDevice::getHostData();

// Moteurs : PWM de 120 KHz, paliers de 512
PWMConfig pwmMotorConfig = { 120000, 512, NULL, { { PWM_OUTPUT_ACTIVE_HIGH, NULL }, { PWM_OUTPUT_ACTIVE_HIGH, NULL }, { PWM_OUTPUT_DISABLED, NULL }, { PWM_OUTPUT_DISABLED, NULL }, }, 0, 0 };

MotorVNH5019Config motorM1Config = { .pwmDriver = &PWM_M1, .pwmConfig = &pwmMotorConfig, .pwmChannel = PWM_M1_CHANNEL, .portPwm = PORT_M1PWM, .padPwm = PAD_M1PWM, .modePwm = MODE_M1PWM, .portDirectionA = PORT_M1INA, .padDirectionA = PAD_M1INA, .portDirectionB = PORT_M1INB, .padDirectionB = PAD_M1INB, .portEnable = PORT_M1EN, .padEnable = PAD_M1EN, .portAnalog = PORT_M1CS, .padAnalog = PAD_M1CS, .analogChannel = ANALOG_CHANNEL_M1, .neutralValue = 0, .reversed = true };
MotorVNH5019Config motorM2Config = { .pwmDriver = &PWM_M2, .pwmConfig = &pwmMotorConfig, .pwmChannel = PWM_M2_CHANNEL, .portPwm = PORT_M2PWM, .padPwm = PAD_M2PWM, .modePwm = MODE_M2PWM, .portDirectionA = PORT_M2INA, .padDirectionA = PAD_M2INA, .portDirectionB = PORT_M2INB, .padDirectionB = PAD_M2INB, .portEnable = PORT_M2EN, .padEnable = PAD_M2EN, .portAnalog = PORT_M2CS, .padAnalog = PAD_M2CS, .analogChannel = ANALOG_CHANNEL_M2, .neutralValue = 0, .reversed = false };

MotorVNH5019 leftMotor(motorM1Config);
MotorVNH5019 rightMotor(motorM2Config);

CoderConfig leftCoderConfig = data.getLeftCoderConfig();
CoderConfig rightCoderConfig = data.getRightCoderConfig();

Coder leftCoder(leftCoderConfig);
Coder rightCoder(rightCoderConfig);

LCD_QY2004A lcd(HostDevice::isBig() ? 0x3F : 0x27);

//PinStartDetectorConfig startDetectorConfig = { .port = PORT_START, .pad = PAD_START, .startSignal = PAL_HIGH, .threshold = 5 };
//PinStartDetector startDetector(startDetectorConfig);

I2CConfig i2cConfig = { OPMODE_I2C, I2C_CLOCK_SPEED, FAST_DUTY_CYCLE_2 };

Beacon2018Config beaconConfig(&I2CD1, &i2cConfig, 0x25, Peripherals::getObstaclesDetectionArea());
Beacon2018 beacon(beaconConfig);

NXTConfig nxtConfig(&I2CD1, &i2cConfig, 0x20);
NXT nxt(nxtConfig);

AnalogEmergencyStopConfig emergencyStopConfig = { .analogChannel = ANALOG_CHANNEL_STOP, .thresholdMilliVolts = 5000, .dividerR1 = 12, .dividerR2 = 1 };
AnalogEmergencyStop emergencyStop(emergencyStopConfig);

AnalogSensorConfig mainBoardSensorConfig = { .analogChannel = ANALOG_CHANNEL_VIN, .multiplier = 13, .divider = 1 };
MainBoardPowerSensor mainBoardPowerSensor(mainBoardSensorConfig);

PositionControl control(leftCoder, rightCoder, leftMotor, rightMotor, beacon);
Navigation navigation(control, beacon, emergencyStop);
Console console((BaseSequentialStream *) &SD2, navigation);
LCDScreen screen(lcd);
extern DataBus dataBus;
InputParser inputParser((BaseChannel *) &SD2, console, navigation, screen, dataBus);

QEIConfig qeiLeftConfig = { .mode = QEI_MODE_QUADRATURE, .resolution = QEI_BOTH_EDGES, .dirinv = QEI_DIRINV_TRUE, .overflow = QEI_OVERFLOW_WRAP, .min = 0, .max = 0, .notify_cb = NULL, .overflow_cb = NULL };
QEIConfig qeiRightConfig = { .mode = QEI_MODE_QUADRATURE, .resolution = QEI_BOTH_EDGES, .dirinv = QEI_DIRINV_FALSE, .overflow = QEI_OVERFLOW_WRAP, .min = 0, .max = 0, .notify_cb = NULL, .overflow_cb = NULL };

void Peripherals::initialize(const HostData &data) {
	(void) data;

	sdStart(&SD2, NULL);
	i2cStart(&I2CD1, &i2cConfig);

	qeiStart(&QEI_LEFT, &qeiLeftConfig);
	qeiStart(&QEI_RIGHT, &qeiRightConfig);
	qeiEnable (&QEI_LEFT);
	qeiEnable (&QEI_RIGHT);

	leftMotor.initialize();
	rightMotor.initialize();

	leftMotor.setEnabled(true);
	rightMotor.setEnabled(true);

	beacon.initializeBeaconVars(
		data.getGeometry().distanceXBeaconCenterFromRobotCenter,
		data.getGeometry().distanceYBeaconCenterFromRobotCenter,
		data.getGeometry().beaconDiameter
	);
	beacon.initialize();
	beacon.setThreshold(500);
}

void Peripherals::mapPins() {
	// Mappage des pins

	// QEI1
	palSetPadMode(PORT_QEIM1A, PAD_QEIM1A, PAL_MODE_ALTERNATE(MODE_QEIM1));
	palSetPadMode(PORT_QEIM1B, PAD_QEIM1B, PAL_MODE_ALTERNATE(MODE_QEIM1));

	// QEI2
	palSetPadMode(PORT_QEIM2A, PAD_QEIM2A, PAL_MODE_ALTERNATE(MODE_QEIM2));
	palSetPadMode(PORT_QEIM2B, PAD_QEIM2B, PAL_MODE_ALTERNATE(MODE_QEIM2));

	// Tirette
	palSetPadMode(PORT_START, PAD_START, MODE_START);

	// I2C
	palSetPadMode(PORT_I2C_SDA, PAD_I2C_SDA, PAL_MODE_ALTERNATE(MODE_I2C_SDA) | PAL_STM32_OTYPE_OPENDRAIN);
	palSetPadMode(PORT_I2C_SCL, PAD_I2C_SCL, PAL_MODE_ALTERNATE(MODE_I2C_SCL) | PAL_STM32_OTYPE_OPENDRAIN);

	// Arr�t d'urgence
	palSetPadMode(PORT_STOP, PAD_STOP, PAL_MODE_INPUT_ANALOG);

	// VIN
	palSetPadMode(PORT_VIN, PAD_VIN, PAL_MODE_INPUT_ANALOG);

	// TTL
	palSetPad(GPIOC, GPIOC_PIN8);
	palSetPadMode(GPIOC, GPIOC_PIN8, PAL_MODE_OUTPUT_PUSHPULL);
}
