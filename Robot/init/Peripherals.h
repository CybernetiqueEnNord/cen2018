/*
 * peripherals.h
 *
 *  Created on: 29 ao�t 2017
 *      Author: Emmanuel
 */

#ifndef PERIPHERALS_H_
#define PERIPHERALS_H_

#include "devices/host/HostData.h"
#include "geometry/Area.h"
#include "init/PeripheralsConfig.h"

/**
 * \brief P�riph�riques
 *
 * Configuration et gestion des p�riph�riques.
 */
class Peripherals {
public:
	/**
	 * \brief Gestion des obstacles
	 *
	 * Renvoie l'aire de d�tection des obstacles.
	 * @return l'aire de d�tection des obstacles
	 */
	static const Area& getObstaclesDetectionArea();
	/**
	 * \brief Initialisation
	 *
	 * Initialise les p�riph�riques.
	 * @param [in] data donn�es relatives � l'h�te
	 */
	static void initialize(const HostData &data);
	/**
	 * \brief Mappage des broches
	 *
	 * D�finit les fonctions des diff�rentes broches.
	 */
	static void mapPins();
};

#endif /* PERIPHERALS_H_ */
