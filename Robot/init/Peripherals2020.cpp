#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/actuators/2020/small/SmallActuator.h"
#include "devices/databus/2020/DataBus2020.h"
#include "devices/io/Console.h"
#include "i2c/ActuatorBig2020Commands.h"
#include "i2c/ActuatorSmall2020Commands.h"
#include "init/Peripherals.h"

#include "geometry/AreaDifference.h"
#include "geometry/Rectangle.h"
#define OUTER_EXCLUSION_MM 70

extern I2CConfig i2cConfig;
extern Console console;

SmallActuatorConfig smallActuatorConfig(&I2CD1, &i2cConfig, I2C_ADDRESS_ACTUATORSMALL2020);
SmallActuator smallActuator(smallActuatorConfig);

BigActuatorConfig bigActuatorConfig(&I2CD1, &i2cConfig, I2C_ADDRESS_ACTUATORBIG2020);
BigActuator bigActuator(bigActuatorConfig);

DataBus2020 dataBus(console);

const Area& Peripherals::getObstaclesDetectionArea() {
	static Rectangle board = Rectangle(OUTER_EXCLUSION_MM, OUTER_EXCLUSION_MM, 2000 - (2 * OUTER_EXCLUSION_MM), 3000 - (OUTER_EXCLUSION_MM * 2));
	// Retire le port adverse de la zone de détection puisqu'on ne s'y rend jamais
	static Rectangle opponentPort = Rectangle(1600, 800, 400, 800);
	static Rectangle opponentArea = Rectangle(1700, 2000, 300, 500);
	static AreaDifference area1 = AreaDifference(board, opponentPort);
	static AreaDifference area2 = AreaDifference(area1, opponentArea);
	return area2;
}
