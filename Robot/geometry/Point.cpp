/*
 * Point.cpp
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#include "Point.h"

#include "math/cenMath.h"

#include <math.h>

Point::Point() {
}

Point::Point(float x, float y) :
		x(x), y(y) {
}

void Point::getLengthAndOriantation(const Point &p, float &length, float &orientation) const {
	float dx = p.x - x;
	float dy = p.y - y;
	length = sqrtf(dx * dx + dy * dy);
	orientation = atan2f(dy, dx);
}

float Point::getDistance(const Point &p) const {
	float length, orientation;
	getLengthAndOriantation(p, length, orientation);
	return length;
}

float Point::getOrientationTo(const Point &p) const {
	float length, orientation;
	getLengthAndOriantation(p, length, orientation);
	return orientation;
}

bool Point::operator==(const Point &p) const {
	return equalsf(p.x, x) && equalsf(p.y, y);
}

bool Point::operator!=(const Point &p) const {
	return !equalsf(p.x, x) || !equalsf(p.y, y);
}
