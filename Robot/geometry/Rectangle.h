/*
 * Rectangle.h
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#ifndef GEOMETRY_RECTANGLE_H_
#define GEOMETRY_RECTANGLE_H_

#include "Area.h"

class Rectangle : public Area {
private:
	int x1, x2, y1, y2;

public:
	Rectangle(int x, int y, int width, int height);
	virtual bool contains(const Point &p) const;
};

#endif /* GEOMETRY_RECTANGLE_H_ */
