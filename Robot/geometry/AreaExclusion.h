/*
 * AreaExclusion.h
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#ifndef GEOMETRY_AREAEXCLUSION_H_
#define GEOMETRY_AREAEXCLUSION_H_

#include "Area.h"

class AreaExclusion : public Area {
private:
	Area &area1;
	Area &area2;

public:
	AreaExclusion(Area &area1, Area &area2);
	virtual bool contains(const Point &p) const;
};

#endif /* GEOMETRY_AREAEXCLUSION_H_ */
