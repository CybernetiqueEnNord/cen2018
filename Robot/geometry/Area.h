/*
 * Area.h
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#ifndef GEOMETRY_AREA_H_
#define GEOMETRY_AREA_H_

#include "geometry/Point.h"

class Area {
protected:
	Area();

public:
	virtual bool contains(const Point &p) const = 0;
};

#endif /* GEOMETRY_AREA_H_ */
