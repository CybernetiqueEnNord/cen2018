/*
 * Rectangle.cpp
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#include "Rectangle.h"

Rectangle::Rectangle(int x, int y, int width, int height) {
	x1 = x;
	y1 = y;
	x2 = x1 + width;
	y2 = y1 + height;
}

bool Rectangle::contains(const Point &p) const {
	bool result = (p.x >= x1) && (p.x < x2) && (p.y >= y1) && (p.y < y2);
	return result;
}
