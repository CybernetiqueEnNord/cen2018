/*
 * AreaIntersection.h
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#ifndef GEOMETRY_AREAINTERSECTION_H_
#define GEOMETRY_AREAINTERSECTION_H_

#include "Area.h"

class AreaIntersection : public Area {
private:
	Area &area1;
	Area &area2;

public:
	AreaIntersection(Area &area1, Area &area2);
	virtual bool contains(const Point &p) const;
};

#endif /* GEOMETRY_AREAINTERSECTION_H_ */
