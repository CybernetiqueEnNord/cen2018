/*
 * AreaUnion.h
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#ifndef GEOMETRY_AREAUNION_H_
#define GEOMETRY_AREAUNION_H_

#include "Area.h"

class AreaUnion : public Area {
private:
	Area &area1;
	Area &area2;

public:
	AreaUnion(Area &area1, Area &area2);
	virtual bool contains(const Point &p) const;
};

#endif /* GEOMETRY_AREAUNION_H_ */
