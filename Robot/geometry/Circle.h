/*
 * Circle.h
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#ifndef GEOMETRY_CIRCLE_H_
#define GEOMETRY_CIRCLE_H_

#include "Area.h"

class Circle : public Area {
private:
	Point center;
	int radius;

public:
	Circle(int x, int y, int radius);
	virtual bool contains(const Point &p) const;
};

#endif /* GEOMETRY_CIRCLE_H_ */
