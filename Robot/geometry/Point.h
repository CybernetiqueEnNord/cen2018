/*
 * Point.h
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#ifndef GEOMETRY_POINT_H_
#define GEOMETRY_POINT_H_

/**
 * \brief Coordonn�es d'un point
 *
 * Structure d�finissant les coordonn�es d'un point dans un plan.
 */
struct Point {
	/** Position sur l'axe X (mm). */
	float x = 0;
	/** Position sur l'axe Y (mm). */
	float y = 0;
	/** Constructeur par d�faut. */
	Point();
	/**
	 * Constructeur avec coordonn�es.
	 * @param [in] x coordonn�e sur l'axe X (mm)
	 * @param [in] y coordonn�e sur l'axe Y (mm)
	 */
	Point(float x, float y);
	/**
	 * Renvoie la distance de ce point au point sp�cifi�.
	 * @param [in] p le point cible
	 */
	float getDistance(const Point &p) const;
	/**
	 * Renvoie la longueur et l'orientation du segment constitu� par le receveur et le point cible sp�cifi�.
	 * @param [in] p le point cible
	 * @param [out] length la longueur du segment
	 * @param [out] orientation l'orientation du segment
	 */
	void getLengthAndOriantation(const Point &p, float &length, float &orientation) const;
	/**
	 * Renvoie l'orientation du segment allant du point repr�sent�
	 * par le receveur au point de destination sp�cifi�.
	 * @param [in] p le point cible
	 * @return l'orientation du segment
	 */
	float getOrientationTo(const Point &p) const;
	bool operator==(const Point &p) const;
	bool operator!=(const Point &p) const;
};

#endif /* GEOMETRY_POINT_H_ */
