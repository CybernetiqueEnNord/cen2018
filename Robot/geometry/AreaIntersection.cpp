/*
 * AreaIntersection.cpp
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#include "AreaIntersection.h"

AreaIntersection::AreaIntersection(Area &area1, Area &area2) :
		area1(area1), area2(area2) {
}

bool AreaIntersection::contains(const Point &p) const {
	bool result = area1.contains(p) && area2.contains(p);
	return result;
}
