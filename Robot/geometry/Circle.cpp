/*
 * Circle.cpp
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#include "Circle.h"

Circle::Circle(int x, int y, int radius) :
		center(x, y), radius(radius) {
}

bool Circle::contains(const Point &p) const {
	bool result = p.getDistance(center) < radius;
	return result;
}
