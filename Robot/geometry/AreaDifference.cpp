/*
 * AreaDifference.cpp
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#include "AreaDifference.h"

AreaDifference::AreaDifference(Area &included, Area &excluded) :
		included(included), excluded(excluded) {
}

bool AreaDifference::contains(const Point &p) const {
	bool result = included.contains(p) && !excluded.contains(p);
	return result;
}
