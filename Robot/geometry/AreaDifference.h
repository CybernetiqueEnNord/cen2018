/*
 * AreaDifference.h
 *
 *  Created on: 4 janv. 2018
 *      Author: Emmanuel
 */

#ifndef GEOMETRY_AREADIFFERENCE_H_
#define GEOMETRY_AREADIFFERENCE_H_

#include "Area.h"

class AreaDifference : public Area {
private:
	Area &included;
	Area &excluded;

public:
	AreaDifference(Area &included, Area &excluded);
	virtual bool contains(const Point &p) const;
};

#endif /* GEOMETRY_AREADIFFERENCE_H_ */
