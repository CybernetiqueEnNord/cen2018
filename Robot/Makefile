##############################################################################
# Build global options
# NOTE: Can be overridden externally.
#

TARGET = F446
MODE = DEBUG
EDITION = 2020

# Compiler options here.
ifeq ($(USE_OPT),)
  ifeq ($(MODE),DEBUG)
    USE_OPT = -O0 -ggdb -falign-functions=16
  else
    USE_OPT = -O2 -fomit-frame-pointer -falign-functions=16
  endif
endif

# C specific options here (added to USE_OPT).
ifeq ($(USE_COPT),)
  USE_COPT = 
endif

# C++ specific options here (added to USE_OPT).
ifeq ($(USE_CPPOPT),)
  USE_CPPOPT = -fno-rtti -std=c++14 -fno-threadsafe-statics -fno-use-cxa-atexit -fno-exceptions
endif

# Enable this if you want the linker to remove unused code and data
ifeq ($(USE_LINK_GC),)
  USE_LINK_GC = yes
endif

# Linker extra options here.
ifeq ($(USE_LDOPT),)
  USE_LDOPT = 
endif

# Enable this if you want link time optimizations (LTO)
ifeq ($(USE_LTO),)
  ifeq ($(MODE),DEBUG)
    USE_LTO = no
  else
    USE_LTO = yes
  endif
endif

# If enabled, this option allows to compile the application in THUMB mode.
ifeq ($(USE_THUMB),)
  USE_THUMB = yes
endif

# Enable this if you want to see the full log while compiling.
ifeq ($(USE_VERBOSE_COMPILE),)
  USE_VERBOSE_COMPILE = no
endif

# If enabled, this option makes the build process faster by not compiling
# modules not used in the current configuration.
ifeq ($(USE_SMART_BUILD),)
  USE_SMART_BUILD = yes
endif

#
# Build global options
##############################################################################

##############################################################################
# Architecture or project specific options
#

# Stack size to be allocated to the Cortex-M process stack. This stack is
# the stack used by the main() thread.
ifeq ($(USE_PROCESS_STACKSIZE),)
  USE_PROCESS_STACKSIZE = 0x800
endif

# Stack size to the allocated to the Cortex-M main/exceptions stack. This
# stack is used for processing interrupts and exceptions.
ifeq ($(USE_EXCEPTIONS_STACKSIZE),)
  USE_EXCEPTIONS_STACKSIZE = 0x400
endif

# Enables the use of FPU (no, softfp, hard).
ifeq ($(USE_FPU),)
  USE_FPU = hard
endif

#
# Architecture or project specific options
##############################################################################

##############################################################################
# Project, sources and paths
#

# Define project name here
PROJECT = ch

# Imported source files and paths
CHIBIOS = ../ChibiOS_17.6.0
CHIBIOS_CONTRIB = ../ChibiOS_17.6.0/community
COMMON = ../Commons
# Startup files.
include $(CHIBIOS)/os/common/startup/ARMCMx/compilers/GCC/mk/startup_stm32f4xx.mk
# HAL-OSAL files (optional).
include $(CHIBIOS_CONTRIB)/os/hal/hal.mk
include $(CHIBIOS_CONTRIB)/os/hal/ports/STM32/STM32F4xx/platform.mk
ifeq ($(TARGET), F446)
  include $(CHIBIOS)/os/hal/boards/ST_NUCLEO64_F446RE/board.mk
endif
ifeq ($(TARGET), F411)
  include $(CHIBIOS)/os/hal/boards/ST_NUCLEO64_F411RE/board.mk
endif
include $(CHIBIOS)/os/hal/osal/rt/osal.mk
# RTOS files (optional).
include $(CHIBIOS)/os/rt/rt.mk
include $(CHIBIOS)/os/common/ports/ARMCMx/compilers/GCC/mk/port_v7m.mk
# Other files (optional).
include $(CHIBIOS)/test/rt/test.mk
include $(CHIBIOS)/os/hal/lib/streams/streams.mk

# Define linker script file here
ifeq ($(TARGET), F446)
LDSCRIPT = $(STARTUPLD)/STM32F446xE.ld
endif
ifeq ($(TARGET), F411)
LDSCRIPT = $(STARTUPLD)/STM32F411xE.ld
endif

# C sources that can be compiled in ARM or THUMB mode depending on the global
# setting.
CSRC = $(STARTUPSRC) \
       $(KERNSRC) \
       $(PORTSRC) \
       $(OSALSRC) \
       $(HALSRC) \
       $(PLATFORMSRC) \
       $(BOARDSRC) \
       $(TESTSRC)

# C++ sources that can be compiled in ARM or THUMB mode depending on the global
# setting.

ifeq ($(EDITION), 2018)
EDITIONCPPSRC = devices/actuators/2018/ActuatorsTest.cpp \
				devices/actuators/2018/cubes/CubesActuator.cpp \
				devices/actuators/2018/water/WaterActuator.cpp \
				init/Peripherals2018.cpp \
				match/2018/Match2018.cpp
endif

ifeq ($(EDITION), 2019)
EDITIONCPPSRC = control/map/2019/NavigationMap2019.cpp \
				devices/actuators/2019/ActuatorsTest.cpp \
				devices/actuators/2019/big/BigActuator.cpp \
				devices/actuators/2019/small/SmallActuator.cpp \
				init/Peripherals2019.cpp \
				match/2019/MatchTaskList2019.cpp \
				match/2019/actions/BlueiumAction.cpp \
				match/2019/actions/GoldeniumAction.cpp \
				match/2019/actions/GoldeniumAndDispenserAction.cpp \
				match/2019/actions/SingleActionList.cpp \
				match/2019/actions/SmallAtomsDispenserAction.cpp \
				match/2019/actions/Target1ActionList.cpp \
				match/2019/actions/big/BigAccelerator3DropAction.cpp \
				match/2019/actions/big/BigAcceleratorDropAction.cpp \
				match/2019/actions/big/BigDispenserPickupAction.cpp \
				match/2019/actions/big/BigGoldeniumDropAction.cpp \
				match/2019/actions/big/BigGoldeniumPickupAction.cpp \
				match/2019/actions/big/BigOpponentChaosAtoms.cpp \
				match/2019/actions/big/BigOpponentDispenserPickupAction.cpp \
				match/2019/actions/big/BigPrimaryTaskList.cpp \
				match/2019/actions/big/BigSideDispenserPickupAction.cpp \
				match/2019/actions/big/BigStartAllGroundAtomsAction.cpp \
				match/2019/actions/big/BigStartDropAction.cpp \
				match/2019/actions/big/BigWeighingTrayBlueDropAction.cpp \
				match/2019/actions/small/SmallAggressiveTaskList.cpp \
				match/2019/actions/small/SmallAlternateBlueiumAction.cpp \
				match/2019/actions/small/SmallDeliverBlueiumInBalance.cpp \
				match/2019/actions/small/SmallDeliverGoldeniumAction.cpp \
				match/2019/actions/small/SmallDeliverTrioInTable.cpp \
				match/2019/actions/small/SmallPrimaryTaskList.cpp \
				match/2019/actions/small/SmallRollBlueiumAction.cpp \
				match/2019/actions/small/SmallSafeTaskList.cpp \
				match/2019/actions/small/SmallOurReservedRackAction.cpp \
				match/2019/actions/small/SmallSideDispenserAction.cpp \
				match/2019/actions/small/SmallSpeedStart.cpp \
				match/2019/actions/small/SmallTakeGoldeniumAction.cpp \
				match/2019/actions/small/SmallTakeOpponentChaosAction.cpp \
				match/2019/actions/small/SmallTakeOppRack3NearStartAction.cpp \
				match/2019/actions/small/SmallTakeOppRack3NearBalanceAction.cpp \
				match/2019/actions/small/SmallTakeOurChaosAction.cpp \
				match/2019/actions/small/SmallTakeOurRack3NearBalanceAction.cpp \
				match/2019/actions/small/SmallTakeOurRack3NearStartAction.cpp \
				match/2019/Match2019.cpp \
				match/2019/Match2019BigTridentBackAdapter.cpp \
				match/2019/Match2019BigTridentFrontAdapter.cpp \
				match/2019/Match2019SmallTridentAdapter.cpp \
				match/2019/Match2019TridentStorage.cpp \
				match/2019/Match2019TridentToActuatorAdapter.cpp \
				match/2019/MatchCalibration2019.cpp \
				match/2019/MatchData2019.cpp
endif

ifeq ($(EDITION), 2020)
EDITIONCPPSRC = devices/actuators/2020/ActuatorsTest.cpp \
				devices/actuators/2020/big/BigActuator.cpp \
				devices/actuators/2020/big/BigBuoySensorData.cpp \
				devices/actuators/2020/small/SmallActuator.cpp \
				devices/databus/2020/DataBus2020.cpp \
				init/Peripherals2020.cpp \
				match/2020/Match2020.cpp \
				match/2020/Match2020AnchorAreaTimerListener.cpp \
				match/2020/Match2020FlagsTimerListener.cpp \
				match/2020/MatchCalibration2020.cpp \
				match/2020/MatchActionUtils2020.cpp \
				match/2020/MatchData2020.cpp \
				match/2020/actions/big/BigAggrOppSideRackAction.cpp \
				match/2020/actions/big/BigAggrOurSideRackAction.cpp \
				match/2020/actions/big/BigEndAtBayAction.cpp \
				match/2020/actions/big/BigGatherAroundSmallPortAction.cpp \
				match/2020/actions/big/BigGatherAroundSmallPortRearAction.cpp \
				match/2020/actions/big/BigGatherOurSideTableAction.cpp \
				match/2020/actions/big/BigKickstartLighthouseAction.cpp \
				match/2020/actions/big/BigNominalTaskList.cpp \
				match/2020/actions/big/BigSafeTaskList.cpp \
				match/2020/actions/big/BigOurReservedRackAction.cpp \
				match/2020/actions/big/BigSafeOppSideRackAction.cpp \
				match/2020/actions/big/BigSafeOurSideRackAction.cpp \
				match/2020/actions/big/BigUnloadAllOurPortAction.cpp \
				match/2020/actions/big/BigUnloadAllSmallPortAction.cpp \
				match/2020/actions/big/BigUnloadSafeOurPortAction.cpp \
				match/2020/actions/small/SmallDraftTaskList.cpp \
				match/2020/actions/small/SmallEndAtBayAction.cpp \
				match/2020/actions/small/SmallNominalTaskList.cpp \
				match/2020/actions/small/SmallLighthouseAction.cpp \
				match/2020/actions/small/SmallGetFirstBuoyAction.cpp \
				match/2020/actions/small/SmallOurReservedRackAction.cpp \
				match/2020/actions/small/SmallSafeTaskList.cpp \
				match/2020/actions/small/SmallTrivialAction.cpp \
				match/2020/actions/small/SmallUnloadAllSmallPortAction.cpp \
				match/2020/actions/small/SmallUnloadAllSmallPortAlternativeAction.cpp \
				match/2020/actions/small/SmallWindsockAction.cpp \
				match/2020/choices/MatchChoice2020.cpp \
				match/2020/choices/MatchChoice2020BigDraft.cpp \
				match/2020/choices/MatchChoice2020BigHomologation.cpp \
				match/2020/choices/MatchChoice2020BigNominal.cpp \
				match/2020/choices/MatchChoice2020BigSafe.cpp \
				match/2020/choices/MatchChoice2020BigFast.cpp \
				match/2020/choices/MatchChoice2020SmallDraft.cpp \
				match/2020/choices/MatchChoice2020SmallHomologation.cpp \
				match/2020/choices/MatchChoice2020SmallNominal.cpp \
				match/2020/choices/MatchChoice2020SmallSafe.cpp \
				match/2020/points/Match2020BouyRackConfiguration.cpp \
				match/2020/points/Match2020BouyState.cpp \
				match/2020/points/Match2020Dock.cpp \
				tests/testsScore.cpp
endif

CPPSRC = main.cpp \
         crt.cpp \
         control/AbstractMove.cpp \
         control/Navigation.cpp \
         control/PositionControl.cpp \
         control/map/NavigationArc.cpp \
         control/map/NavigationMap.cpp \
         control/map/NavigationNode.cpp \
         control/map/NavigationService.cpp \
         control/moves/OrientationMove.cpp \
         control/moves/RotationMove.cpp \
         control/moves/StraightMove.cpp \
         control/moves/XYMove.cpp \
         devices/actuators/Actuator.cpp \
         devices/actuators/I2CActuator.cpp \
         devices/beacon/Beacon.cpp \
         devices/beacon/Beacon2018.cpp \
         devices/coders/Coder.cpp \
         devices/databus/DataBus.cpp \
         devices/host/HostDevice.cpp \
         devices/host/HostData.cpp \
         devices/host/HostDataF411RETest.cpp \
         devices/host/HostDataF446REBig.cpp \
         devices/host/HostDataF446RENextGenBig.cpp \
         devices/host/HostDataF446RENextGenSmall.cpp \
         devices/host/HostDataF446RESmall.cpp \
         devices/host/RobotGeometry.cpp \
         devices/i2c/I2CDevice.cpp \
         devices/io/AnalogEmergencyStop.cpp \
         devices/io/AnalogReader.cpp \
         devices/io/AnalogSensor.cpp \
         devices/io/Console.cpp \
         devices/io/EmergencyStop.cpp \
         devices/io/LCDScreen.cpp \
         devices/io/MainBoardPowerSensor.cpp \
         devices/io/NXT.cpp \
         devices/io/ObstacleDetector.cpp \
         devices/io/PinStartDetector.cpp \
         devices/io/Screen.cpp \
         devices/io/StartDetector.cpp \
         devices/io/StatusLed.cpp \
         devices/lcd/I2CIO.cpp \
         devices/lcd/LCD.cpp \
         devices/lcd/LCDBigFont.cpp \
         devices/lcd/LCDQY2004A.cpp \
         devices/lcd/LiquidCrystal_I2C.cpp \
         devices/lcd/Print.cpp \
         devices/motors/Motor.cpp \
         devices/motors/MotorVNH5019.cpp \
         devices/timer/MatchTimer.cpp \
         geometry/Area.cpp \
         geometry/AreaDifference.cpp \
         geometry/AreaExclusion.cpp \
         geometry/AreaIntersection.cpp \
         geometry/AreaUnion.cpp \
         geometry/Circle.cpp \
         geometry/Point.cpp \
         geometry/Rectangle.cpp \
         init/Initialization.cpp \
         init/Peripherals.cpp \
         match/Match.cpp \
         match/MatchAction.cpp \
         match/MatchActionList.cpp \
         match/MatchChoice.cpp \
         match/MatchChoiceCollection.cpp \
         match/MatchData.cpp \
         match/MatchElement.cpp \
         match/MatchExecutor.cpp \
         match/MatchTarget.cpp \
         match/MatchTask.cpp \
         match/MatchTaskList.cpp \
         match/MatchTest.cpp \
         match/SimpleFallbackAction.cpp \
         match/SingleActionList.cpp \
         math/cenMath.cpp \
         odometry/Odometry.cpp \
         odometry/Position.cpp \
         parser/InputParser.cpp \
         tests/tests.cpp \
         tests/testsArea.cpp \
         tests/testsMath.cpp \
         tests/testsNavigationService.cpp \
         tests/testsPosition.cpp \
         tests/testsVNH5019.cpp \
         utils/Base64.cpp \
         utils/Text.cpp \
         $(EDITIONCPPSRC)

# C sources to be compiled in ARM mode regardless of the global setting.
# NOTE: Mixing ARM and THUMB mode enables the -mthumb-interwork compiler
#       option that results in lower performance and larger code size.
ACSRC =

# C++ sources to be compiled in ARM mode regardless of the global setting.
# NOTE: Mixing ARM and THUMB mode enables the -mthumb-interwork compiler
#       option that results in lower performance and larger code size.
ACPPSRC =

# C sources to be compiled in THUMB mode regardless of the global setting.
# NOTE: Mixing ARM and THUMB mode enables the -mthumb-interwork compiler
#       option that results in lower performance and larger code size.
TCSRC =

# C sources to be compiled in THUMB mode regardless of the global setting.
# NOTE: Mixing ARM and THUMB mode enables the -mthumb-interwork compiler
#       option that results in lower performance and larger code size.
TCPPSRC =

# List ASM source files here
ASMSRC =
ASMXSRC = $(STARTUPASM) $(PORTASM) $(OSALASM)

INCDIR = $(CHIBIOS)/os/license \
         $(STARTUPINC) $(KERNINC) $(PORTINC) $(OSALINC) \
         $(HALINC) $(PLATFORMINC) $(BOARDINC) $(TESTINC) \
         $(COMMON)

#
# Project, sources and paths
##############################################################################

##############################################################################
# Compiler settings
#

MCU  = cortex-m4

#TRGT = arm-elf-
TRGT = arm-none-eabi-
CC   = $(TRGT)gcc
CPPC = $(TRGT)g++
# Enable loading with g++ only if you need C++ runtime support.
# NOTE: You can use C++ even without C++ support if you are careful. C++
#       runtime support makes code size explode.
LD   = $(TRGT)gcc
#LD   = $(TRGT)g++
CP   = $(TRGT)objcopy
AS   = $(TRGT)gcc -x assembler-with-cpp
AR   = $(TRGT)ar
OD   = $(TRGT)objdump
SZ   = $(TRGT)size
HEX  = $(CP) -O ihex
BIN  = $(CP) -O binary

# ARM-specific options here
AOPT =

# THUMB-specific options here
TOPT = -mthumb -DTHUMB

# Define C warning options here
CWARN = -Wall -Wextra -Wundef -Wstrict-prototypes

# Define C++ warning options here
CPPWARN = -Wall -Wextra -Wundef

#
# Compiler settings
##############################################################################

##############################################################################
# Start of user section
#

# List all user C define here, like -D_DEBUG=1
UDEFS = -DCEN_EDITION=$(EDITION)

# Define ASM defines here
UADEFS =

# List all user directories here
UINCDIR =

# List the user directory to look for the libraries here
ULIBDIR =

# List all user libraries here
ULIBS = -lm

#
# End of user defines
##############################################################################

RULESPATH = $(CHIBIOS)/os/common/startup/ARMCMx/compilers/GCC
include $(RULESPATH)/rules.mk

parser/InputParser.cpp: parser/*.inc

parser/*.inc: parser/*.rl
	$(MAKE) -C parser
