#include "tests/testsVNH5019.h"

#include "devices/motors/MotorVNH5019.h"
#include "tests/tests.h"

const char * test_vnh5019_1() {
	test_assert("test_vnh5019_1", ADC_TO_MILLIAMPERES(4095) == 23565);
	return 0;
}
