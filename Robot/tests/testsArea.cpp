/*
 * testsArea.cpp
 *
 *  Created on: 12 f�vr. 2018
 *      Author: Emmanuel
 */

#include "tests.h"

#include "geometry/AreaDifference.h"
#include "geometry/Circle.h"
#include "geometry/Rectangle.h"

const char * test_area1() {
	Rectangle area = Rectangle(0, 0, 2000, 3000);
	Point p1(1000, 1500);
	test_assert("area1.p1", area.contains(p1));
	Point p2(-1000, 1500);
	test_assert("area1.p2", !area.contains(p2));
	Point p3(3000, 1500);
	test_assert("area1.p3", !area.contains(p3));
	Point p4(1000, 4500);
	test_assert("area1.p4", !area.contains(p4));
	Point p5(1000, -1500);
	test_assert("area1.p5", !area.contains(p5));
	return 0;
}

const char * test_area2() {
	Circle area = Circle(1000, 1500, 500);
	Point p1(1000, 1500);
	test_assert("area2.p1", area.contains(p1));
	Point p2(-1000, 1500);
	test_assert("area2.p2", !area.contains(p2));
	Point p3(3000, 1500);
	test_assert("area2.p3", !area.contains(p3));
	Point p4(1000, 4500);
	test_assert("area2.p4", !area.contains(p4));
	Point p5(1000, -1500);
	test_assert("area2.p5", !area.contains(p5));
	return 0;
}
const char * test_area_board() {
	int dispenserSize = 400;
	static Rectangle board = Rectangle(200, 200, 1600, 2600);
	static Rectangle dispenser1 = Rectangle(840 - dispenserSize / 2, 0, dispenserSize, dispenserSize);
	static Rectangle dispenser2 = Rectangle(2000 - dispenserSize, 610 - dispenserSize / 2, dispenserSize, dispenserSize);
	static Rectangle dispenser3 = Rectangle(840 - dispenserSize / 2, 3000 - dispenserSize, dispenserSize, dispenserSize);
	static Rectangle dispenser4 = Rectangle(2000 - dispenserSize, 2390 - dispenserSize / 2, dispenserSize, dispenserSize);
	static AreaDifference area1 = AreaDifference(board, dispenser1);
	static AreaDifference area2 = AreaDifference(area1, dispenser2);
	static AreaDifference area3 = AreaDifference(area2, dispenser3);
	static AreaDifference area = AreaDifference(area3, dispenser4);

	Point p1(840, 250);
	test_assert("area_board.p1", !area.contains(p1));
	Point p2(840, dispenserSize + 10);
	test_assert("area_board.p2", area.contains(p2));

	Point p3(1750, 610);
	test_assert("area_board.p3", !area.contains(p3));
	Point p4(2000 - dispenserSize - 10, 610);
	test_assert("area_board.p4", area.contains(p4));

	Point p5(840, 2750);
	test_assert("area_board.p5", !area.contains(p5));
	Point p6(840, 3000 - dispenserSize - 10);
	test_assert("area_board.p6", area.contains(p6));

	Point p7(1750, 2390);
	test_assert("area_board.p7", !area.contains(p7));
	Point p8(2000 - dispenserSize - 10, 2390);
	test_assert("area_board.p8", area.contains(p8));

	Point p9(975, 322);
	test_assert("area_board.p9", !area.contains(p9));

	return 0;
}
