/*
 * testsBezier.h
 *
 *  Created on: 14 d�c. 2014
 *      Author: Emmanuel
 */

#ifndef TESTS_TESTSBEZIER_H_
#define TESTS_TESTSBEZIER_H_

const char * test_bezier1();
const char * test_bezier2();
const char * test_bezier3();

#endif /* TESTS_TESTSBEZIER_H_ */
