/*
 * tests.cpp
 *
 *  Created on: 14 d�c. 2014
 *      Author: Emmanuel
 */

#include "tests.h"

#include "devices/io/Console.h"
#include "tests/testsArea.h"
//#include "tests/testsBezier.h"
#include "tests/testsMath.h"
#include "tests/testsNavigationService.h"
#include "tests/testsPosition.h"
#include "tests/testsPositionControl.h"
#include "tests/testsVNH5019.h"
#include "tests/testsScore.h"

extern Console console;

int tests_run = 0;

static const char * all_tests() {
	test_run(test_getOrientedDistance1);
	test_run(test_getOrientedDistance2);
	test_run(test_getOrientedDistance3);
	test_run(test_getOrientedDistance4);
	test_run(test_getOrientedDistance5);
	test_run(test_getOrientedDistance6);

//	test_run(test_bezier1);
//	test_run(test_bezier2);
//	test_run(test_bezier3);

//	test_run(test_positionControl1);
//	test_run(test_positionControl2);
//	test_run(test_positionControl3);
//	test_run(test_positionControl4);

	test_run(test_vnh5019_1);

	test_run(test_area1);
	test_run(test_area2);
	test_run(test_area_board);

	test_run(test_position1);

	test_run(test_NavigationService1);
	test_run(test_NavigationService2);

	test_run(test_ArcDistance1);
	test_run(test_ArcDistance2);
	test_run(test_ArcDistance3);

	test_run(test_Unload1);

	return 0;
}

void runAllTests() {
	tests_run = 0;
	const char *result = all_tests();
	if (result != 0) {
		console.sendKeyValue("TEST FAILURE", result);
	} else {
		console.sendData("ALL TESTS PASSED");
	}
	console.sendKeyValue("Tests run", tests_run);
}
