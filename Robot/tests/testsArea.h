/*
 * testArea.h
 *
 *  Created on: 12 f�vr. 2018
 *      Author: Emmanuel
 */

#ifndef TESTS_TESTSAREA_H_
#define TESTS_TESTSAREA_H_

const char * test_area1();
const char * test_area2();
const char * test_area3();
const char * test_area4();
const char * test_area_board();

#endif /* TESTS_TESTSAREA_H_ */
