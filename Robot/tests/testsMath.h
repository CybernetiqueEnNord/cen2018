/*
 * testsMath.h
 *
 *  Created on: 14 d�c. 2014
 *      Author: Emmanuel
 */

#ifndef TESTS_TESTSMATH_H_
#define TESTS_TESTSMATH_H_

const char * test_getOrientedDistance1();
const char * test_getOrientedDistance2();
const char * test_getOrientedDistance3();
const char * test_getOrientedDistance4();
const char * test_getOrientedDistance5();
const char * test_getOrientedDistance6();

#endif /* TESTS_TESTSMATH_H_ */
