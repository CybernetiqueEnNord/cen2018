/*
 * testsScore.cpp
 *
 *  Created on: 5 juil. 2020
 *      Author: emmanuel
 */

#include "testsScore.h"

#include "match/2020/MatchData2020.h"
#include "devices/actuators/2020/big/BigBuoySensorData.h"
#include "tests.h"

const char* test_Unload1() {
	MatchTimer timer;
	MatchData2020 data(timer);

	BigBuoySensorData sensors(5, 31, 31);
	data.unloadPattern(Code::VVVVR_VRRRR_4, sensors, FrontRear::Front, Match2020Chenal::STARTING_PORT, Match2020ChenalColor::CHENAL_RED);
	test_assert("red", data.getScore() == 9);

	sensors = BigBuoySensorData(4, 0, 31);
	data.unloadPattern(Code::VVVVR_VRRRR_4, sensors, FrontRear::Rear, Match2020Chenal::STARTING_PORT, Match2020ChenalColor::CHENAL_GREEN);
	test_assert("green", data.getScore() == 26);

	sensors = BigBuoySensorData(4, 15, 15);
	data.unload(sensors, FrontRear::Front, Match2020Chenal::STARTING_PORT);
	test_assert("front", data.getScore() == 30);

	sensors = BigBuoySensorData(4, 0, 15);
	data.unload(sensors, FrontRear::Front, Match2020Chenal::STARTING_PORT);
	test_assert("rear", data.getScore() == 34);
	return 0;
}
