/*
 * testsNavigationService.cpp
 *
 *  Created on: 29 d�c. 2018
 *      Author: emmanuel
 */

#include "testsNavigationService.h"

#include "tests.h"

#include "control/map/NavigationArc.h"
#include "control/map/NavigationMap.h"
#include "control/map/2019/NavigationMap2019.h"
#include "control/map/NavigationNode.h"
#include "control/map/NavigationService.h"
#include "geometry/Point.h"
#include "math/cenMath.h"
#include "utils/Arrays.h"

const char * test_NavigationService1() {
	NavigationNode n0_0(Point(0, 0));
	NavigationNode n100_0(Point(100, 0));
	NavigationNode n200_0(Point(200, 0));
	NavigationNode n300_0(Point(300, 0));

	NavigationNode n0_100(Point(0, 100));
	NavigationNode n100_100(Point(100, 100));
	NavigationNode n200_100(Point(200, 100));
	NavigationNode n300_100(Point(300, 100));

	NavigationNode n0_200(Point(0, 200));
	NavigationNode n100_200(Point(100, 200));
	NavigationNode n200_200(Point(200, 200));
	NavigationNode n300_200(Point(300, 200));

	NavigationNode n0_300(Point(0, 300));
	NavigationNode n100_300(Point(100, 300));
	NavigationNode n200_300(Point(200, 300));
	NavigationNode n300_300(Point(300, 300));

	NavigationArc a0_0_0_100(n0_0, n0_100, 0);
	NavigationArc a0_100_0_200(n0_100, n0_200, 0);
	NavigationArc a0_200_0_300(n0_200, n0_300, 0);

	NavigationArc a0_0_100_0(n0_0, n100_0, 0);
	NavigationArc a0_100_100_100(n0_100, n100_100, 0);
	NavigationArc a0_200_100_200(n0_200, n100_200, 0);
	NavigationArc a0_300_100_300(n0_300, n100_300, 0);

	NavigationArc a100_0_100_100(n100_0, n100_100, 0);
	NavigationArc a100_100_100_200(n100_100, n100_200, 0);
	NavigationArc a100_200_100_300(n100_200, n100_300, 0);

	NavigationArc a100_0_200_0(n100_0, n200_0, 0);
	NavigationArc a100_100_200_100(n100_100, n200_100, 0);
	NavigationArc a100_200_200_200(n100_200, n200_200, 0);
	NavigationArc a100_300_200_300(n100_300, n200_300, 0);

	NavigationArc a200_0_200_100(n200_0, n200_100, 0);
	NavigationArc a200_100_200_200(n200_100, n200_200, 0);
	NavigationArc a200_200_200_300(n200_200, n200_300, 0);

	NavigationArc a200_0_300_0(n200_0, n300_0, 0);
	NavigationArc a200_100_300_100(n200_100, n300_100, 0);
	NavigationArc a200_200_300_200(n200_200, n300_200, 0);
	NavigationArc a200_300_300_300(n200_300, n300_300, 0);

	NavigationArc a300_0_300_100(n300_0, n300_100, 0);
	NavigationArc a300_100_300_200(n300_100, n300_200, 0);
	NavigationArc a300_200_300_300(n300_200, n300_300, 0);

	NavigationNode *nodes[] = { &n0_0, &n100_0, &n200_0, &n300_0, &n0_100, &n100_100, &n200_100, &n300_100, &n0_200, &n100_200, &n200_200, &n300_200, &n0_300, &n100_300, &n200_300, &n300_300 };
	NavigationArc *arcs[] = { &a0_0_0_100, &a0_100_0_200, &a0_200_0_300, &a0_0_100_0, &a0_100_100_100, &a0_200_100_200, &a0_300_100_300, &a100_0_100_100, &a100_100_100_200, &a100_200_100_300, &a100_0_200_0, &a100_100_200_100, &a100_200_200_200, &a100_300_200_300, &a200_0_200_100, &a200_100_200_200, &a200_200_200_300, &a200_0_300_0, &a200_100_300_100, &a200_200_300_200, &a200_300_300_300, &a300_0_300_100, &a300_100_300_200, &a300_200_300_300 };
	NavigationMap map(nodes, ARRAY_SIZE(nodes), arcs, ARRAY_SIZE(arcs));
	NavigationService service(map);
	const Point* path[10];
	int pathSize = service.computePath(Point(0, 0), Point(400, 400), path, ARRAY_SIZE(path));

	test_assert("test_NavigationService1", pathSize == 6);
	return 0;
}

const char * test_NavigationService2() {
#if CEN_EDITION == 2019
	auto &map = NavigationMap2019::getMap();
	test_assert("test_NavigationService2", map.isValid());
#endif
	return 0;
}

float getArcDistanceSquared(const Point &s, const Point &e, const Point &p) {
	NavigationNode start(s);
	NavigationNode end(e);
	NavigationArc arc(start, end);
	float d = arc.getDistanceSquared(p);
	return d;
}

const char * test_ArcDistance1() {
	float d = getArcDistanceSquared(Point(1, 1), Point(5, 5), Point(2, 3));
	float expected = 0.707106781f;
	test_assert("test_ArcDistance1", equalsf(expected * expected, d));
	return 0;
}

const char * test_ArcDistance2() {
	float d = getArcDistanceSquared(Point(2, 3), Point(7, 8), Point(2, 0));
	float expected = 3.0f;
	test_assert("test_ArcDistance2", equalsf(expected * expected, d));
	return 0;
}

const char * test_ArcDistance3() {
	float d = getArcDistanceSquared(Point(12, 8), Point(18, 3), Point(5, 7));
	float expected = 7.07106781187f;
	test_assert("test_ArcDistance3", equalsf(expected * expected, d));
	return 0;
}
