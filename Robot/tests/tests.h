/*
 * tests.h
 *
 *  Created on: 3 d�c. 2014
 *      Author: Emmanuel
 */

#ifndef TESTS_TESTS_H_
#define TESTS_TESTS_H_

#define test_assert(message, test) do { if (!(test)) return message; } while (0)
#define test_run(test) do { const char *message = test(); tests_run++; if (message) return message;} while (0)

void runAllTests();

#endif /* TESTS_TESTS_H_ */
