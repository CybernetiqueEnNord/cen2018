/*
 * testsPosition.cpp
 *
 *  Created on: 17 f�vr. 2018
 *      Author: Emmanuel
 */

#include "math/cenMath.h"
#include "odometry/Position.h"
#include "tests/testsPosition.h"
#include "tests.h"

const char * test_position1() {
	Position start(840, 520, 0);
	Point destination(840, 850);

	float angle, distance;
	start.getAngleAndDistance(destination, angle, distance);

	test_assert("test_position1.angle", equalsf(angle, ANGLE_TO_RADIANS(90)));
	test_assert("test_position1.distance", equalsf(distance, 850 - 520));
	return 0;
}
