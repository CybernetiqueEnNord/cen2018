/*
 * testsBezier.cpp
 *
 *  Created on: 14 d�c. 2014
 *      Author: Emmanuel
 */

#include "tests.h"

#include <math.h>
#include "trajectory/Bezier.h"

const char * test_bezier1() {
	Bezier bezier({0, 0}, {1000, 1000}, {0, 100}, {1000, 900});
	Position p = bezier.getPosition(0);
	test_assert("bezier1.x", p.x == 0);
	test_assert("bezier1.y", p.y == 0);
	test_assert("bezier1.angle", p.angle == M_PI_2);
	return 0;
}

const char * test_bezier2() {
	Bezier bezier({0, 0}, {1000, 1000}, {0, 100}, {1000, 900});
	Position p = bezier.getPosition(1);
	test_assert("bezier2.x", p.x == 1000);
	test_assert("bezier2.y", p.y == 1000);
	test_assert("bezier2.angle", p.angle == M_PI_2);
	return 0;
}

const char * test_bezier3() {
	Bezier bezier({0, 0}, {1000, 1100}, {0, 100}, {1000, 1000});
	Position p = bezier.getPosition(0.5);
	test_assert("bezier3.x", p.x == 500);
	test_assert("bezier3.y", p.y == 550);
	test_assert("bezier3.angle", p.angle == M_PI_4);
	return 0;
}
