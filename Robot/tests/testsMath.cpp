/*
 * testsMath.cpp
 *
 *  Created on: 3 d�c. 2014
 *      Author: Emmanuel
 */

#include "testsMath.h"

#include "math/cenMath.h"
#include "odometry/Odometry.h"
#include "tests.h"

const char * test_getOrientedDistance1() {
	Position o = { 0, 0, 0 };
	Position p = { 30, 40, 0 };
	test_assert("getOrientedDistance1", o.getOrientedDistance(p) == 50);
	return 0;
}

const char * test_getOrientedDistance2() {
	Position o = { 0, 0, 0 };
	Position p = { 30, 40, 0 };
	test_assert("getOrientedDistance2", p.getOrientedDistance(o) == -50);
	return 0;
}

const char * test_getOrientedDistance3() {
	Position o = { 0, 0, ANGLE_TO_RADIANS(90) };
	Position p = { 30, 40, 0 };
	test_assert("getOrientedDistance3", o.getOrientedDistance(p) == 50);
	return 0;
}

const char * test_getOrientedDistance4() {
	Position o = { 0, 0, ANGLE_TO_RADIANS(90) };
	Position p = { 30, 40, 0 };
	test_assert("getOrientedDistance4", p.getOrientedDistance(o) == -50);
	return 0;
}

const char * test_getOrientedDistance5() {
	Position o = { 0, 0, ANGLE_TO_RADIANS(-90) };
	Position p = { 30, 40, 0 };
	test_assert("getOrientedDistance5", o.getOrientedDistance(p) == -50);
	return 0;
}

const char * test_getOrientedDistance6() {
	Position o = { 0, 0, ANGLE_TO_RADIANS(-90) };
	Position p = { 30, 40, 0 };
	test_assert("getOrientedDistance6", p.getOrientedDistance(o) == -50);
	return 0;
}
