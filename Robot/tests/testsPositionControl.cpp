/*
 * testsPositionControl.cpp
 *
 *  Created on: 25 janv. 2015
 *      Author: Emmanuel
 */

#include "testsPositionControl.h"

#include "control/PositionControl.h"
#include "math/cenMath.h"
#include "tests.h"

const char * test_positionControl1() {
	extern PositionControl control;
	Position p(0, 0, 0);
	p = control.moveStraight(p, 150);
	test_assert("test_positionControl1", equalsf(p.x, 150));
	test_assert("test_positionControl1", equalsf(p.y, 0));
	test_assert("test_positionControl1", equalsf(p.angle, 0));
	return 0;
}

const char * test_positionControl2() {
	extern PositionControl control;
	Position p(0, 0, ANGLE_TO_RADIANS(90));
	p = control.moveStraight(p, 150);
	test_assert("test_positionControl2", equalsf(p.x, 0));
	test_assert("test_positionControl2", equalsf(p.y, 150));
	test_assert("test_positionControl2", equalsf(p.angle, ANGLE_TO_RADIANS(90)));
	return 0;
}

const char * test_positionControl3() {
	extern PositionControl control;
	Position p(0, 0, ANGLE_TO_RADIANS(180));
	p = control.moveStraight(p, 150);
	test_assert("test_positionControl3", equalsf(p.x, -150));
	test_assert("test_positionControl3", equalsf(p.y, 0));
	test_assert("test_positionControl3", equalsf(p.angle, ANGLE_TO_RADIANS(180)));
	return 0;
}

const char * test_positionControl4() {
	extern PositionControl control;
	Position p(0, 0, ANGLE_TO_RADIANS(-90));
	p = control.moveStraight(p, 150);
	test_assert("test_positionControl3", equalsf(p.x, 0));
	test_assert("test_positionControl3", equalsf(p.y, -150));
	test_assert("test_positionControl3", equalsf(p.angle, ANGLE_TO_RADIANS(-90)));
	return 0;
}
