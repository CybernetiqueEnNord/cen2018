/*
 * testsPositionControl.h
 *
 *  Created on: 25 janv. 2015
 *      Author: Emmanuel
 */

#ifndef TESTS_TESTSPOSITIONCONTROL_H_
#define TESTS_TESTSPOSITIONCONTROL_H_

const char * test_positionControl1();
const char * test_positionControl2();
const char * test_positionControl3();
const char * test_positionControl4();

#endif /* TESTS_TESTSPOSITIONCONTROL_H_ */
