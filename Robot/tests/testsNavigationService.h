/*
 * testsNavigationService.h
 *
 *  Created on: 29 d�c. 2018
 *      Author: emmanuel
 */

#ifndef TESTS_TESTSNAVIGATIONSERVICE_H_
#define TESTS_TESTSNAVIGATIONSERVICE_H_

const char * test_NavigationService1();
const char * test_NavigationService2();

const char * test_ArcDistance1();
const char * test_ArcDistance2();
const char * test_ArcDistance3();

#endif /* TESTS_TESTSNAVIGATIONSERVICE_H_ */
