/*
 * Pointer.h
 *
 *  Created on: 5 nov. 2017
 *      Author: Emmanuel
 */

#ifndef UTILS_CONSTPOINTER_H_
#define UTILS_CONSTPOINTER_H_

#include <cstddef>

template<typename T>
class ConstPointer {
private:
	const T* pointer = NULL;

public:
	ConstPointer() {}
	ConstPointer(const T* value) : pointer(value) {}
	const T* operator->() const {return pointer;}
	const T& operator*() const {return *pointer;}
	void operator=(ConstPointer<T> value) {pointer = value.pointer;}
	bool operator==(const ConstPointer<T> value) const {return pointer == value.pointer;}
	bool isValid() const {return pointer != NULL;}
};

#endif /* UTILS_CONSTPOINTER_H_ */
