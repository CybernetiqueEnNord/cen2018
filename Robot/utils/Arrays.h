/*
 * Arrays.h
 *
 *  Created on: 9 d�c. 2017
 *      Author: Emmanuel
 */

#ifndef UTILS_ARRAYS_H_
#define UTILS_ARRAYS_H_

#define ARRAY_SIZE(x) (sizeof x / sizeof(x[0]))

#endif /* UTILS_ARRAYS_H_ */
