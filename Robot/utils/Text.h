/*
 * Text.h
 *
 *  Created on: 17 f�vr. 2018
 *      Author: Emmanuel
 */

#ifndef UTILS_TEXT_H_
#define UTILS_TEXT_H_

int atoi(char *text);
char* itoa(int num, char* str, unsigned char base);

#endif /* UTILS_TEXT_H_ */
