/*
 * Base64.h
 *
 *  Created on: 27 nov. 2017
 *      Author: Emmanuel
 */

#ifndef UTILS_BASE64_H_
#define UTILS_BASE64_H_

class Base64 {
private:
	static const char cb64[];
	static void decodeBlock(unsigned char *in, unsigned char *out);
	static void encodeBlock(unsigned char *in, unsigned char *out, int len);

public:
	static void decode(unsigned char *in, int len, unsigned char *out);
	static void encode(unsigned char *in, int len, unsigned char *out);
};

#endif /* UTILS_BASE64_H_ */
