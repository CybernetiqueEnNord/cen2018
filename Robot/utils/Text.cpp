/*
 * Text.cpp
 *
 *  Created on: 17 f�vr. 2018
 *      Author: Emmanuel
 */

#include "utils/Text.h"

#include <stdlib.h>

void reverse(char *str, unsigned char length) {
	char *end = str + length - 1;
	while (str < end) {
		char tmp = *str;
		*str = *end;
		*end = tmp;
		str++;
		end--;
	}
}

char* itoa(int num, char* str, unsigned char base) {
	unsigned char i = 0;
	bool isNegative = false;

	/* Handle 0 explicitely, otherwise empty string is printed for 0 */
	if (num == 0) {
		str[i++] = '0';
		str[i] = '\0';
		return str;
	}

	// In standard itoa(), negative numbers are handled only with
	// base 10. Otherwise numbers are considered unsigned.
	if (num < 0 && base == 10) {
		isNegative = true;
		num = -num;
	}

	// Process individual digits
	while (num != 0) {
		int rem = num % base;
		str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
		num = num / base;
	}

	// If number is negative, append '-'
	if (isNegative)
		str[i++] = '-';

	// Append string terminator
	str[i] = '\0';

	// Reverse the string
	reverse(str, i);

	return str;
}

int atoi(char *s) {
	int result = strtol(s, nullptr, 10);
	return result;

	/*
	 int c = 1, a = 0, sign, start, end, base = 1;
	 // Determine if the number is negative or positive
	 if (s[0] == '-') {
	 sign = -1;
	 } else if (s[0] <= '9' && s[0] >= '0') {
	 sign = 1;
	 } else if (s[0] == '+') {
	 sign = 2;
	 // No further processing if it starts with a letter
	 } else {
	 return 0;
	 }

	 // Scanning the string to find the position of the last consecutive number
	 while (s[c] != '\n' && s[c] <= '9' && s[c] >= '0') {
	 c++;
	 }

	 // Index of the last consecutive number from beginning
	 start = c - 1;
	 // Based on sign, index of the 1st number is set
	 if (sign == -1) {
	 end = 1;
	 } else if (sign == 1) {
	 end = 0;
	 } else {
	 // When it starts with +, it is actually positive but with a different index
	 // for the 1st number
	 end = 1;
	 sign = 1;
	 }

	 // This the main loop of algorithm which generates the absolute value of the
	 // number from consecutive numerical characters.
	 for (int i = start; i >= end; i--) {
	 a += (s[i] - '0') * base;
	 base *= 10;
	 }

	 // The correct sign of generated absolute value is applied
	 return sign * a;
	 */
}
