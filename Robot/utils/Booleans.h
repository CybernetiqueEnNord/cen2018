/*
 * Booleans.h
 *
 *  Created on: 19 mars 2018
 *      Author: Emmanuel
 */

#ifndef UTILS_BOOLEANS_H_
#define UTILS_BOOLEANS_H_

#define FLAGS(T) \
inline T operator~ (T a) {return (T)~(int)a;} \
inline T operator| (T a, T b) {return (T)((int)a | (int)b);} \
inline T operator& (T a, T b) {return (T)((int)a & (int)b);} \
inline T operator^ (T a, T b) {return (T)((int)a ^ (int)b);} \
inline T& operator|= (T& a, T b) {return (T&)((int&)a |= (int)b);} \
inline T& operator&= (T& a, T b) {return (T&)((int&)a &= (int)b);} \
inline T& operator^= (T& a, T b) {return (T&)((int&)a ^= (int)b);} \
inline bool hasFlag (T a, T b) {return ((int)a & (int)b) == (int)b;}

#endif /* UTILS_BOOLEANS_H_ */
