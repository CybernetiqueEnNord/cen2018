/*
 * Pointer.h
 *
 *  Created on: 5 nov. 2017
 *      Author: Emmanuel
 */

#ifndef UTILS_POINTER_H_
#define UTILS_POINTER_H_

#include <cstddef>

template<typename T>
class Pointer {
private:
	T* pointer = NULL;

public:
	Pointer() {}
	Pointer(T* value) : pointer(value) {}
	Pointer(T& value): pointer(&value) {}
	void clear() {pointer = NULL;}
	const T* operator->() const {return pointer;}
	T* operator->() {return pointer;}
	T& operator*() {return *pointer;}
	void operator=(Pointer<T> value) {pointer = value.pointer;}
	bool operator==(const Pointer<T> value) const {return pointer == value.pointer;}
	bool isValid() const {return pointer != NULL;}
};

#endif /* UTILS_POINTER_H_ */
