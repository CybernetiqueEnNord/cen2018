/*
 * Base64.cpp
 *
 *  Created on: 27 nov. 2017
 *      Author: Emmanuel
 */

#include "Base64.h"

const char Base64::cb64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void Base64::encodeBlock(unsigned char *in, unsigned char *out, int len) {
	out[0] = (unsigned char) cb64[(int) (in[0] >> 2)];
	out[1] = (unsigned char) cb64[(int) (((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4))];
	out[2] = (unsigned char) (len > 1 ? cb64[(int) (((in[1] & 0x0f) << 2) | ((in[2] & 0xc0) >> 6))] : '=');
	out[3] = (unsigned char) (len > 2 ? cb64[(int) (in[2] & 0x3f)] : '=');
}

void Base64::decodeBlock(unsigned char *in, unsigned char *out) {
	out[0] = (unsigned char) (in[0] << 2 | in[1] >> 4);
	out[1] = (unsigned char) (in[1] << 4 | in[2] >> 2);
	out[2] = (unsigned char) (((in[2] << 6) & 0xc0) | in[3]);
}

void Base64::decode(unsigned char *in, int len, unsigned char *out) {
	while (len > 0) {
		decodeBlock(in, out);
		len -= 4;
		in += 4;
		out += 3;
	}
}

void Base64::encode(unsigned char *in, int len, unsigned char *out) {
	while (len > 3) {
		encodeBlock(in, out, 3);
		len -= 3;
		in += 3;
		out += 4;
	}
	if (len > 0) {
		encodeBlock(in, out, len);
	}
}
