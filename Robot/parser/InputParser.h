/*
 * InputParser.h
 *
 *  Created on: 23 juil. 2017
 *      Author: Emmanuel
 */

#ifndef PARSER_INPUTPARSER_H_
#define PARSER_INPUTPARSER_H_

#include "devices/databus/DataBus.h"
#include "devices/io/Console.h"
#include "devices/io/Screen.h"
#include "control/Navigation.h"
#include "match/Match.h"

#define ERROR_DATABUS_OVERFLOW 1

/**
 * \brief Analyseur syntaxique
 *
 * Analyseur syntaxique associ� � la console d'interfa�age externe.
 * Analyse et traite les commandes externes.
 */
class InputParser {
private:
	BaseChannel *channel;
	Console &console;
	Navigation &navigation;
	Screen &screen;
	DataBus &dataBus;
	Pointer<Match> match;

	int lastError = 0;

	char parserState;
	int number;
	signed char numberSign;
	int numberIndex = -1;
	int numbers[6];
	char dataBuffer[32];
	unsigned int dataBufferPosition = 0;

	void handleInput(char c);
	void initialize();
	void parserNumberReset();
	void parserNumberAdd(char c);
	void parserNumberPush();
	void reverse(char *str, char length);
	void setError(int error);

public:
	/**
	 * Constructeur.
	 * @param [in] channel le flux entrant des donn�es � traiter
	 * @param [in] console l'objet console
	 * @param [in] navigation le gestionnaire de la navigation
	 * @param [in] screen interface d'affichage
	 * @param [in] dataBus interface du bus de donn�es
	 */
	InputParser(BaseChannel *channel, Console &console, Navigation &navigation, Screen &screen, DataBus &dataBus);
	/**
	 * Effectue le traitement des donn�es.
	 */
	void run();
	/**
	 * D�finit le match.
	 * La m�thode update() de l'objet Match est invoqu�e p�riodiquement pour effectuer les traitements asynchrones.
	 * @param [in] match l'objet match � invoquer p�riodiquement
	 * @see Match::update
	 */
	void setMatch(Match &match);
};

#endif /* PARSER_INPUTPARSER_H_ */
