/*
 * InputParser.cpp
 *
 *  Created on: 23 juil. 2017
 *      Author: Emmanuel
 */

#include "InputParser.h"

#include "ch.h"
#include "tests/tests.h"
#include "devices/host/HostDevice.h"
#include "utils/Arrays.h"

#include "static.inc"

InputParser::InputParser(BaseChannel *channel, Console &console, Navigation &navigation, Screen &screen, DataBus &dataBus) :
		channel(channel), console(console), navigation(navigation), screen(screen), dataBus(dataBus) {
	initialize();
}

void InputParser::initialize() {
#include "initializeParser.inc"
}

void InputParser::handleInput(char c) {
	PositionControl &control = navigation.getControl();
#include "handleInput.inc"
}

void InputParser::reverse(char *str, char length) {
	char *end = str + length - 1;
	while (str < end) {
		char tmp = *str;
		*str = *end;
		*end = tmp;
		str++;
		end--;
	}
}

void InputParser::parserNumberReset() {
	numberSign = 1;
	number = 0;
}

void InputParser::parserNumberAdd(char c) {
	number *= 10;
	number += c - '0';
}

void InputParser::parserNumberPush() {
	number *= numberSign;
	if (numberIndex >= 0) {
		numbers[numberIndex] = number;
		numberIndex++;
	}
}

void InputParser::run() {
	int n = 0;
	bool inObstacle = false;
	while (TRUE) {
		// Traite les entr�es, si pas d'entr�e, traite la navigation
		msg_t msg = chnGetTimeout(channel, MS2ST(10));
		if (msg == STM_TIMEOUT) {
			n++;
			// Mise � jour de la navigation
			navigation.update();

			// Mise � jour de l'objet match
			if (match.isValid()) {
				match->update();
			}

			// Journalisation de l'obstacle
			PositionControl &control = navigation.getControl();
			MoveState state = control.getState();
			bool obstacle = state == MoveState::EmergencyBrake;
			if (obstacle != inObstacle) {
				inObstacle = obstacle;
				if (obstacle) {
					console.sendObstaclePosition();
				} else {
					console.sendDetection();
				}
			}

			// Mise � jour de l'�cran (500 ms)
			if (n % 50 == 0) {
				const Point& obstacle = control.getObstaclePosition();
				screen.setObstacle(state == MoveState::EmergencyBrake, obstacle);
				screen.update();
			}
		} else {
			handleInput((char) msg);
		}
	}
}

void InputParser::setMatch(Match& match) {
	this->match = Pointer<Match>(&match);
}

void InputParser::setError(int error) {
	lastError = error;
}
