%%{
	machine parser;
	
	action about {
		const char *version = HostDevice::getVersion();
		console.sendData(version);
		const HostData &data = HostDevice::getHostData();
		const char *name = data.getName();
		console.sendDeviceName(name);
	}
	
	action position_send {
		console.sendPosition();
	}
	
	action number_reset {
		parserNumberReset();
	}
	
	action number_add {
		parserNumberAdd(fc);
	}
	
	action number_send {
		char buf[32];
		itoa(number, buf, 10);
		console.sendData(buf);
	}
	
	action number_negate {
		numberSign = -1;
	}
	
	action move_rotation {
		navigation.rotateDeciDegrees(number);
	}
	
	action move_stop {
		control.abort();
	}
	
	action move_reset {
		control.reset();
	}
	
	action move_straight {
		navigation.moveMM(number);
	}
	
	action move_point {
		navigation.moveTo(Point(numbers[0], numbers[1]));
	}
	
	action number_invert {
		number = ~number;
	}
	
	action simulation_get {
		number = control.simulateCoders ? FLAG_SIMULATE_CODERS : 0;
	}
	
	action simulation_set {
		control.simulateCoders = (number & FLAG_SIMULATE_CODERS) == FLAG_SIMULATE_CODERS;
	}
	
	action simulation_coders {
		number ^= FLAG_SIMULATE_CODERS;
	}
	
	action simulation_send {
		console.sendSimulationFlags(number);
	}
	
	action coders_set {
		control.codersInputAddSteps(numbers[0], numbers[1]);
	}
	
	action numbers_clear {
		numberIndex = 0;
	}
	
	action numbers_push {
		parserNumberPush();
	}
	
	action state_send {
		console.sendState();
	}
	
	action k_query {
		console.sendK();
	}
	
	action k_set {
		control.k = number;
	}
	
	action brake_on {
		control.obstacleAvoidance = true;
	}
	
	action brake_off {
		control.obstacleAvoidance = false;
	}
	
	action detection_query {
		console.sendDetection();
	}
	
	action position_set {
		control.setPosition(numbers[0], numbers[1], numbers[2]);
	}
	
	action wait_start {
		navigation.waitForStart();
	}
	
	action move_speed {
		control.setSpeedIndex(number);
	}
	
	action test_enter {
		console.enterTestMode();
		fgoto test;
	}
	
	action test_exit {
		console.exitTestMode();
		fgoto main;
	}
	
	action test_info {
		console.printTestCommands();
	}
	
	action test_actuators {
		console.setTest(ConsoleMode::TestActuators);
	}
	
	action test_beacon {
		console.setTest(ConsoleMode::TestBeacon);
	}
	
	action test_coders {
		console.setTest(ConsoleMode::TestCoders);
	}
	
	action test_databus {
		console.setTest(ConsoleMode::TestDataBus);
	}
	
	action test_motors {
		console.setTest(ConsoleMode::TestMotors);
	}
	
	action test_nxt_read {
		console.setTest(ConsoleMode::TestNXTRead);
	}
	
	action test_nxt_write {
		console.setTest(ConsoleMode::TestNXTWrite);
	}
	
	action test_error {
		// ignore l'erreur et retourne en mode test
		fgoto test;
	}
	
	action main_error {
		// ignore le symbole courant 
		fhold;
		// consomme la ligne courante et retourne dans main
		fgoto line;
	}
	
	action test_stop {
		console.setTest(ConsoleMode::Test);
	}
	
	action test_unit {
		runAllTests();
	}
	
	action test_i2c {
		console.scanI2CBus();
	}
	
	action control_parameters_send {
		console.sendControlParameters();
	}
	
	action control_parameters_set {
		console.setControlParameters(numbers[0], numbers[1], numbers[2], numbers[3], numbers[4], numbers[5]);
	}
	
	action calibration_parameters_send {
		console.sendCalibrationParameters();
	}
	
	action calibration_parameters_set {
		console.setCalibrationParameters(numbers[1], numbers[0]);
	}
	
	action on_demand_query {
		console.setModeOnDemand();
	}
	
	action cyclic_query {
		console.setModeCyclic();
	}
	
	action system_reset {
		console.systemReset();
	}
	
	action data_clear {
		dataBufferPosition = 0;
	}
	
	action data_add {
		if (dataBufferPosition >= ARRAY_SIZE(dataBuffer)) {
			setError(ERROR_DATABUS_OVERFLOW);
		} else { 
			dataBuffer[dataBufferPosition++] = fc;
		}
	}
	
	action data_push {
		dataBus.setInput(dataBuffer, dataBufferPosition);
	}
	
	number = ('-' @number_negate)? >number_reset digit+ $number_add %numbers_push;
	
	about = '?' %about;
	position = 'P' %position_send | 'P' >numbers_clear (number ';'){2} number %position_set;
	move_rotation = 'R' number %move_rotation;
	move_reset = '^' %move_reset;
	move_speed = 'V' number %move_speed;
	move_stop = 'Z' %move_stop;
	move_straight = 'S' number %move_straight;
	move_point = 'G' number ';' number %move_point; 
	test_number = 'TN' number %number_send;
	simulation_options_set = ('+' @number_reset | '-' @number_reset @number_invert | '~' @simulation_get) ('C' @simulation_coders)+ %simulation_set;
	simulation_options = 'O' (simulation_options_set | '?' @simulation_get %simulation_send);
	coders = 'C' >numbers_clear number ';' number %coders_set;
	state = 'S' %state_send;
	k = 'K' ('?' %k_query | number %k_set);
	brake = 'B' ('0' %brake_off | '1' %brake_on | '?' %detection_query);
	wait_start = 'W' %wait_start;
	control_parameters = ('H?' %control_parameters_send | 'H' >numbers_clear (number ';'){5} number %control_parameters_set);
	calibration_parameters = ('Q?' %calibration_parameters_send | 'Q' >numbers_clear number ';' number %calibration_parameters_set);
	on_demand_query = 'Y' %on_demand_query | 'YC' %cyclic_query;
	system_reset = '&R' %system_reset;
	databus_input = 'x' >data_clear print+ $data_add %data_push;

	test_mode = 'TEST' %test_enter;
	test_actuators = 'A' %test_actuators;
	test_beacon = 'B' %test_beacon;
	test_coders = 'C' %test_coders;
	test_databus = 'D' %test_databus;
	test_motors = 'M' %test_motors;
	test_nxt = 'N' ('R' %test_nxt_read | 'W' %test_nxt_write);
	test_unit = 'U' %test_unit;
	test_exit = 'x' %test_exit;
	test_help = '?' %test_info;
	test_stop = 's' %test_stop;
	test_i2c = 'i' %test_i2c;

	command = (about | brake | coders | calibration_parameters | control_parameters | databus_input | k | on_demand_query | position | state | move_point | move_reset | move_rotation | move_speed | move_stop | move_straight | simulation_options | system_reset | test_mode | wait_start) '\n';
	test_command = (test_actuators | test_beacon | test_coders | test_databus | test_exit | test_help | test_i2c | test_motors | test_nxt | test_stop | test_unit) '\n';

	main := command* $err(main_error);
	test := test_command* $err(test_error);
	line := [^\n]* '\n' @{ fgoto main; };
}%%
