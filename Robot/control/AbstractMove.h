/*
 * AbstractMove.h
 *
 *  Created on: 16 nov. 2017
 *      Author: Emmanuel
 */

#ifndef CONTROL_ABSTRACTMOVE_H_
#define CONTROL_ABSTRACTMOVE_H_

#include "utils/Pointer.h"

class Navigation;

class AbstractMove {
private:
	Pointer<AbstractMove> nextMove;

public:
	AbstractMove();
	void clearNext();
	virtual void execute(Navigation &navigation);
	void setNext(AbstractMove &move);
	const Pointer<AbstractMove>& getNextMove() const;
};

#endif /* CONTROL_ABSTRACTMOVE_H_ */
