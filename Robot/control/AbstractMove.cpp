/*
 * AbstractMove.cpp
 *
 *  Created on: 16 nov. 2017
 *      Author: Emmanuel
 */

#include "control/AbstractMove.h"

AbstractMove::AbstractMove() {
}

void AbstractMove::execute(Navigation &navigation) {
	(void) navigation;
}

void AbstractMove::clearNext() {
	nextMove = NULL;
}

void AbstractMove::setNext(AbstractMove &move) {
	if (nextMove.isValid()) {
		nextMove->setNext(move);
	} else {
		nextMove = &move;
	}
}

const Pointer<AbstractMove>& AbstractMove::getNextMove() const {
	return nextMove;
}
