/*
 * PositionControl.cpp
 *
 *  Created on: 21 juil. 2017
 *      Author: Emmanuel
 */

#include "PositionControl.h"

#include "chvt.h"

#include "control/Navigation.h"
#include "devices/host/HostDevice.h"
#include "math/cenMath.h"

#include <algorithm>

#define DIVIDER_SPEED 32

#define WHEELS_DISTANCE_STEPS(stepsByTurn) (1.0 * stepsByTurn / M_2PI)
#define WHEELS_DISTANCE_MM(stepsByMeter, stepsByTurn) (1000.0 * WHEELS_DISTANCE_STEPS(stepsByTurn) / stepsByMeter)
#define MM_PER_STEP(stepsByMeter) (1000.0 / stepsByMeter)

#define LOOPS_ON_TARGET 3

ControlConfig::ControlConfig(int stepsByTurn, int stepsByMeter) :
		stepsByTurn(stepsByTurn), stepsByMeter(stepsByMeter) {
}

PositionControl::PositionControl(Coder &leftCoder, Coder &rightCoder, Motor &leftMotor, Motor &rightMotor, ObstacleDetector &obstacleDetector) :
		odometry(createOdometry()), leftCoder(leftCoder), rightCoder(rightCoder), leftMotor(leftMotor), rightMotor(rightMotor), obstacleDetector(obstacleDetector) {
	speed = 0;

	const HostData &data = HostDevice::getHostData();
	const ControlConfig &config = data.getControlConfig();
	setControlConfig(config);

	errorRatioLeftVsRight = data.getLeftVsRightCorrectionRatio();

	maxPower = maxPowerInit;
	maxError = maxErrorInit;

	angleParameters = data.getControlParametersAngle();
	distanceParameters = data.getControlParametersDistance();
}

Odometry& PositionControl::createOdometry() {
	static Odometry odometry;
	return odometry;
}

void PositionControl::run() {
	// mise � jour depuis les codeurs
	updatePosition();
	// mise � jour des consignes moteur
	updateMoveState();
	// callback
	if (callback != NULL) {
		callback();
	}
}

void PositionControl::readCoders(int &inMeasureLeft, int &inMeasureRight) {
	inMeasureLeft = leftCoder.update();
	inMeasureRight = rightCoder.update();
}

int PositionControl::restrict(int value) const {
	return std::max(-maxPower, std::min(maxPower, value));
}

void PositionControl::updatePosition() {
	//Commentaire:
	//Lecture des codeurs/
	//HCTL2032

	//Appel d'une Macro
	//Appel d'une Macro: HCTL_GET_DATA()

	int inStepsLeft;
	int inStepsRight;
	if (simulateCoders) {
		codersInputAddSteps(inStepsLeft, inStepsRight);
	} else {
		readCoders(inStepsLeft, inStepsRight);
	}

	inMeasureLeftRaw += inStepsLeft;
	inMeasureRightRaw += inStepsRight;

	int inStepsLeftCorrected = inMeasureLeft;
	int inStepsRightCorrected = inMeasureRight;

	if (errorRatioLeftVsRight != 0) {
		int errLeftMeasure = inMeasureLeftRaw / errorRatioLeftVsRight;
		int errRightMeasure = inMeasureRightRaw / errorRatioLeftVsRight;

		inMeasureLeft = inMeasureLeftRaw - errLeftMeasure;
		inMeasureRight = inMeasureRightRaw + errRightMeasure;
	} else {
		inMeasureLeft = inMeasureLeftRaw;
		inMeasureRight = inMeasureRightRaw;
	}

	inStepsLeftCorrected = inMeasureLeft - inStepsLeftCorrected;
	inStepsRightCorrected = inMeasureRight - inStepsRightCorrected;

	updateOdometry(inStepsLeftCorrected, inStepsRightCorrected);

	int angleMeasured;
	int distanceMeasured;
	int angleError;
	int distanceCorrection;
	int angleCorrection;

	//Commentaire:
	//Calcul de la d�rive des
	//codeurs droits et gauche
	//depuis le dernier calcul

	//D�cision
	//D�cision: couleur?
	if (reversedRotations) {
		//Calcul
		//Calcul:
		//  mesureAngle32 = in_mesureGauche32 - in_mesureDroite32
		angleMeasured = inMeasureLeft - inMeasureRight;
	} else {
		//Calcul
		//Calcul:
		//  mesureAngle32 = in_mesureDroite32 - in_mesureGauche32
		angleMeasured = inMeasureRight - inMeasureLeft;
	}

	//virtualisation a/d
	//Calcul:
	//  mesureDistance32 = in_mesureGauche32 + in_mesureDroite32
	distanceMeasured = inMeasureLeft + inMeasureRight;

	//Calcul de l'erreur
	//Calcul:
	//  aLong = consigneDistance16
	//  aLong = aLong + consigne_distante_deja_parcouru
	//  erreurDistance = (aLong * DIVIDER_16_32_LIN) - mesureDistance32
	//  aLong = tour
	//  aLong = aLong * pas_par_tour16
	//  aLong = aLong + consigneAngle16
	//  erreurAngle = (aLong * DIVIDER_16_32_ANGLE) - mesureAngle32
	distanceError = distanceOrder + distanceTravelled - distanceMeasured;
	angleError = turns * stepsByTurn + angleOrder - angleMeasured;

	//deverouillage_asserv_angle
	//D�cision: deverouillage_asserv_angle <> 0?
	if (unlockedAngleControl) {
		//Calcul
		//Calcul:
		//  erreurAngle = 0
		angleError = 0;
	}

	//Calcul PD
	//Calcul:
	//  correctionDistance = ((coefProportionelAsservDistance * erreurDistance)) + (coefDerivAsservDistance * (erreurDistance - ancienneErreurDistance))
	//  correctionDistance = correctionDistance / 8
	//  correctionAngle = (coefProportionelAsservAngle * erreurAngle) + (coefDerivAsservAngle * (erreurAngle - ancienneErreurAngle))
	//  correctionAngle = correctionAngle / 8
	//  ancienneErreurDistance = erreurDistance
	//  ancienneErreurAngle = erreurAngle
	distanceCorrection = (distanceParameters.P * distanceError) + (distanceParameters.D * (distanceError - previousDistanceError));
	distanceCorrection /= distanceParameters.weight;
	angleCorrection = (angleParameters.P * angleError) + (angleParameters.D * (angleError - previousAngleError));
	angleCorrection /= angleParameters.weight;
	previousDistanceError = distanceError;
	previousAngleError = angleError;

	//D�cision
	//D�cision: couleur?
	if (reversedRotations) {
		//de-virtualisation a/d
		//Calcul:
		//  out_rawLeftCommand = correctionDistance + correctionAngle
		//  out_rawRightCommand = correctionDistance - correctionAngle
		outRawLeftCommand = distanceCorrection + angleCorrection;
		outRawRightCommand = distanceCorrection - angleCorrection;
	} else {
		//de-virtualisation a/d
		//Calcul:
		//  out_rawLeftCommand = correctionDistance - correctionAngle
		//  out_rawRightCommand = correctionDistance + correctionAngle
		outRawLeftCommand = distanceCorrection - angleCorrection;
		outRawRightCommand = distanceCorrection + angleCorrection;
	}

	//protection PWM
	//Appel d'une Macro: out_correctionGauche=MAX(480, out_rawLeftCommand)
	outLeftCorrection = restrict(outRawLeftCommand);

	//protection PWM
	//Appel d'une Macro: out_correctionDroite=MAX(480, out_rawRightCommand)
	outRightCorrection = restrict(outRawRightCommand);

	//Envoi des consignes
	//Appel d'une Macro: motorsSendPwmCommands()
	setMotorsCommands(outLeftCorrection, outRightCorrection);

	//Commentaire:
	//Allumage de la led pour dire "on est au max de correction"

#if 0 // Disabled code
	//D�cision
	//D�cision: out_correctionGauche > max_puissance?
	if (outLeftCorrection > FCV_MAX_PUISSANCE)
	{

		//Sortie
		//Sortie: 1 -> E2
		trise = trise & 0xFB;
		if ((1))
		porte = (porte & 0xFB) | 0x04;
		else
		porte = porte & 0xFB;

	} else {

		//D�cision
		//D�cision: out_correctionGauche < (-1) * max_puissance?
		if (outLeftCorrection < (-1) * FCV_MAX_PUISSANCE)
		{

			//Sortie
			//Sortie: 1 -> E2
			trise = trise & 0xFB;
			if ((1))
			porte = (porte & 0xFB) | 0x04;
			else
			porte = porte & 0xFB;

		} else {

			//D�cision
			//D�cision: out_correctionDroite > max_puissance?
			if (outRightCorrection > FCV_MAX_PUISSANCE)
			{

				//Sortie
				//Sortie: 1 -> E2
				trise = trise & 0xFB;
				if ((1))
				porte = (porte & 0xFB) | 0x04;
				else
				porte = porte & 0xFB;

			} else {

				//D�cision
				//D�cision: out_correctionDroite < (-1) * max_puissance?
				if (outRightCorrection < (-1) * FCV_MAX_PUISSANCE)
				{

					//Sortie
					//Sortie: 1 -> E2
					trise = trise & 0xFB;
					if ((1))
					porte = (porte & 0xFB) | 0x04;
					else
					porte = porte & 0xFB;

				} else {

					//Sortie
					//Sortie: 0 -> E2
					trise = trise & 0xFB;
					if ((0))
					porte = (porte & 0xFB) | 0x04;
					else
					porte = porte & 0xFB;

				}

			}

		}

	}

#endif // #if 0: Disabled code
	//Commentaire:
	//Protection, si l'erreur est trop grande, on est soit rentr� dans l'adversaire, soit le robot est soulev�, soit un moteur � cram�....
	//Donc on coupe tout

	//D�cision
	//D�cision: erreurDistance > MAX_ERREUR || erreurDistance < -1 * MAX_ERREUR || erreurAngle > MAX_ERREUR || erreurAngle < -1 * MAX_ERREUR?
	if (distanceError > maxError || distanceError < -maxError || angleError > maxError || angleError < -maxError) {
		//Appel d'une Macro
		motorsEnabled = false;

		//Calcul
		//Calcul:
		//  errorState = ERROR_OVERRUN
		errorState = overrunAllowed ? MoveError::CalibrationOverrun : MoveError::Overrun;
	}

	//Calcul
	//Calcul:
	//  consigneDistance16 = consigneDistance16 + (consigneVitesseCurviligne / DIVIDER_VITESSE)
	//  consigneAngle16 = consigneAngle16 + (consigneVitesseAngle / DIVIDER_VITESSE)
	distanceOrder += curvilinearSpeedOrder / DIVIDER_SPEED;
	if (circleArcFlag) {
		int totalQuantityOfAngleToMove = angleTarget - circleArcStartingAngleSteps;
		float actualCurvilinearRatio = (float) distanceOrder / (float) curvilinearTarget;
		float actualAngleQuantity = (float) totalQuantityOfAngleToMove * (float) actualCurvilinearRatio;
		angleOrder = circleArcStartingAngleSteps + actualAngleQuantity;
	} else {
		angleOrder += angleSpeedOrder / DIVIDER_SPEED;
	}
}

void PositionControl::updateMoveState() {
	//D�finitions des variables locales
	int linearAcceleration;
	int maxLinearSpeed;
	int linearDeceleration;
	int brakingDistance;
	int direction;
	int angularAcceleration;
	int maxAngularSpeed;
	int angularDeceleration;
	int minLinearSpeedBeforeTargetControl;
	int minAngularSpeedBeforeTargetControl;

	obstacleDetected = isObstacleDetected();

	//Multi-D�cision
	//Multi-D�cision: vitesse?
	switch (speed) {
		case SPEED_BOTH_RECALLAGE: {
			angularAcceleration = 30;
			linearAcceleration = 30;
			angularDeceleration = 30;
			linearDeceleration = 30;
			minAngularSpeedBeforeTargetControl = 1;
			minLinearSpeedBeforeTargetControl = 100;
			maxAngularSpeed = 800;
			maxLinearSpeed = 800;
			break;
		}
		default:
		case SPEED_SMALL_SAFE: { // Safe value
			angularAcceleration = 10;
			linearAcceleration = 8;
			angularDeceleration = 10;
			linearDeceleration = 10;
			minAngularSpeedBeforeTargetControl = 55;
			minLinearSpeedBeforeTargetControl = 55;
			maxAngularSpeed = 500;
			maxLinearSpeed = 900;

			break;
		}
		case SPEED_SMALL_INITIALISATION: {
			angularAcceleration = 10;
			linearAcceleration = 15;
			angularDeceleration = 10;
			linearDeceleration = 15;
			minAngularSpeedBeforeTargetControl = 55;
			minLinearSpeedBeforeTargetControl = 55;
			maxAngularSpeed = 1200;
			maxLinearSpeed = 1500;
			break;
		}
		case SPEED_SMALL_NOMINAL: {
			angularAcceleration = 15;
			linearAcceleration = 25;
			angularDeceleration = 25;
			linearDeceleration = 30;
			minAngularSpeedBeforeTargetControl = 55;
			minLinearSpeedBeforeTargetControl = 55;
			maxAngularSpeed = 1700;
			maxLinearSpeed = 3100;

			break;
		}
		case SPEED_SMALL_FAST: {
			angularAcceleration = 20;
			linearAcceleration = 40;
			angularDeceleration = 26;
			linearDeceleration = 35;
			minAngularSpeedBeforeTargetControl = 100;
			minLinearSpeedBeforeTargetControl = 100;
			maxAngularSpeed = 1800;
			maxLinearSpeed = 9000;

			break;
		}
		case SPEED_SMALL_FAST_STARTING_ROTATION: {
			angularAcceleration = 15;
			linearAcceleration = 35;
			angularDeceleration = 25;
			linearDeceleration = 35;
			minAngularSpeedBeforeTargetControl = 55;
			minLinearSpeedBeforeTargetControl = 55;
			maxAngularSpeed = 1700;
			maxLinearSpeed = 4000;
			break;
		}
		case SPEED_SMALL_MAX_FAST_LINEAR: {
			angularAcceleration = 15;
			linearAcceleration = 45;
			angularDeceleration = 25;
			linearDeceleration = 45;
			minAngularSpeedBeforeTargetControl = 55;
			minLinearSpeedBeforeTargetControl = 55;
			maxAngularSpeed = 1700;
			maxLinearSpeed = 11000;
			break;
		}
		case SPEED_SMALL_TEST: {
			angularAcceleration = 15;
			linearAcceleration = 45;
			angularDeceleration = 25;
			linearDeceleration = 45;
			minAngularSpeedBeforeTargetControl = 55;
			minLinearSpeedBeforeTargetControl = 55;
			maxAngularSpeed = 1700;
			maxLinearSpeed = 11000;
			break;
		}
		case SPEED_SMALL_MAX_STARTING_FAST_LINEAR: {
			angularAcceleration = 15;
			linearAcceleration = 45;
			angularDeceleration = 25;
			linearDeceleration = 45;
			minAngularSpeedBeforeTargetControl = 55;
			minLinearSpeedBeforeTargetControl = 55;
			maxAngularSpeed = 1700;
			maxLinearSpeed = 6000;
			break;
		}

		case SPEED_BIG_INITIALISATION: {
			angularAcceleration = 10;
			linearAcceleration = 15;
			angularDeceleration = angularAcceleration;
			linearDeceleration = linearAcceleration;
			minAngularSpeedBeforeTargetControl = 55;
			minLinearSpeedBeforeTargetControl = 55;
			maxAngularSpeed = 1200;
			maxLinearSpeed = 1800;
			break;
		}
		case SPEED_BIG_SLOW: {
			angularAcceleration = 2;
			linearAcceleration = 2;
			angularDeceleration = angularAcceleration;
			linearDeceleration = linearAcceleration;
			minAngularSpeedBeforeTargetControl = 10;
			minLinearSpeedBeforeTargetControl = 10;
			maxAngularSpeed = 1200;
			maxLinearSpeed = 1000;
			break;
		}
		case SPEED_BIG_NOMINAL: {
			angularAcceleration = 20;
			linearAcceleration = 25;
			angularDeceleration = 20;
			linearDeceleration = 25;
			minAngularSpeedBeforeTargetControl = 55;
			minLinearSpeedBeforeTargetControl = 55;
			maxAngularSpeed = 1700;
			maxLinearSpeed = 5000;
			break;
		}
		case SPEED_BIG_FAST: {
			angularAcceleration = 15;
			linearAcceleration = 35;
			angularDeceleration = 25;
			linearDeceleration = 35;
			minAngularSpeedBeforeTargetControl = 55;
			minLinearSpeedBeforeTargetControl = 55;
			maxAngularSpeed = 2000;
			maxLinearSpeed = 10000;
			break;
		}
	}

	//etat_deplacement
	//Multi-D�cision: etat_deplacement?
	switch (moveState) {
		case MoveState::ClothoidAcceleration: {
			//Commentaire:
			//Acceleration en clotoide

			//D�cision
			//D�cision: cibleAngle >= consigneAngle16?
			if (angleTarget >= angleOrder) {
				//Calcul
				//Calcul:
				//  consigneVitesseAngle = consigneVitesseAngle + .ACCELERATION_ANGULAIRE
				angleSpeedOrder += angularAcceleration;
			} else {
				//Calcul
				//Calcul:
				//  consigneVitesseAngle = consigneVitesseAngle - .ACCELERATION_ANGULAIRE
				angleSpeedOrder -= angularAcceleration;
			}

			//D�cision
			//D�cision: consigneVitesseAngle >= .VITESSE_ANGULAIRE_MAX || consigneVitesseAngle <= -1 * .VITESSE_ANGULAIRE_MAX?
			if (angleSpeedOrder >= maxAngularSpeed || angleSpeedOrder <= -maxAngularSpeed) {
				//Calcul
				//Calcul:
				//  etat_deplacement = 12
				moveState = MoveState::ClothoidArc;
			}

			//Calcul
			//Calcul:
			//  .distanceFreinage = ((consigneVitesseAngle) / (2 * .DECELERATION_ANGULAIRE)) * (consigneVitesseAngle / DIVIDER_VITESSE)
			brakingDistance = (angleSpeedOrder / (2 * angularDeceleration)) * (angleSpeedOrder / DIVIDER_SPEED);

			//D�cision
			//D�cision: cibleAngle >= consigneAngle16?
			if (angleTarget >= angleOrder) {
				//D�cision
				//D�cision: consigneAngle16 >= (cibleAngle - .distanceFreinage)?
				if (angleOrder >= (angleTarget - brakingDistance)) {
					//Calcul
					//Calcul:
					//  etat_deplacement = 13
					moveState = MoveState::ClothoidDeceleration;
				}
			} else {
				//D�cision
				//D�cision: consigneAngle16 <= (cibleAngle + .distanceFreinage)?
				if (angleOrder <= (angleTarget + brakingDistance)) {
					//Calcul
					//Calcul:
					//  etat_deplacement = 13
					moveState = MoveState::ClothoidDeceleration;
				}
			}
			break;
		}
		case MoveState::ClothoidArc: {
			//Commentaire:
			//Arc de cercle entre clotoide

			//D�cision
			//D�cision: cibleAngle >= consigneAngle16?
			//Calcul
			//Calcul:
			//  .sens = 1
			//Calcul
			//Calcul:
			//  .sens = -1

			bool backward = angleTarget < angleOrder;
			if (backward) {
				direction = -1;
			} else {
				direction = 1;
			}

			//Calcul
			//Calcul:
			//  .distanceFreinage = ((consigneVitesseAngle) / (2 * .DECELERATION_ANGULAIRE)) * (consigneVitesseAngle / DIVIDER_VITESSE)
			brakingDistance = (angleSpeedOrder / (2 * angularDeceleration)) * (angleSpeedOrder / DIVIDER_SPEED);

			//D�cision
			//D�cision: .sens * consigneAngle16 >= .sens * (cibleAngle - .sens * .DISTANCE_FREINAGE_MAX_ANGLE)?
			//if (direction * angleOrder >= direction * (targetAngle - direction * maxBrakingDistanceAngle)) {
			if (direction * angleOrder >= direction * (angleTarget - direction * brakingDistance)) {
				//Calcul
				//Calcul:
				//  etat_deplacement = 13
				moveState = MoveState::ClothoidDeceleration;
			}
			break;
		}
		case MoveState::ClothoidDeceleration: {
			//Commentaire:
			//Decceleration en clotoide

			//D�cision
			//D�cision: cibleAngle >= consigneAngle16?
			if (angleTarget >= angleOrder) {
				//Calcul
				//Calcul:
				//  consigneVitesseAngle = consigneVitesseAngle - .DECELERATION_ANGULAIRE
				angleSpeedOrder -= angularDeceleration;

				//D�cision
				//D�cision: consigneVitesseAngle <= .VIT_MIN_AVT_ASV_CIBLE_ANGLE?
				if (angleSpeedOrder <= minAngularSpeedBeforeTargetControl) {

					//Calcul
					//Calcul:
					//  etat_deplacement = 3
					angleSpeedOrder = 0;
					angleOrder = angleTarget;
					moveState = MoveState::StraightConstant;
				}
			} else {
				//Calcul
				//Calcul:
				//  consigneVitesseAngle = consigneVitesseAngle + .DECELERATION_ANGULAIRE
				angleSpeedOrder += angularDeceleration;

				//D�cision
				//D�cision: consigneVitesseAngle >= -1 * .VIT_MIN_AVT_ASV_CIBLE_ANGLE?
				if (angleSpeedOrder >= -minAngularSpeedBeforeTargetControl) {
					//Calcul
					//Calcul:
					//  etat_deplacement = 3
					angleSpeedOrder = 0;
					angleOrder = angleTarget;
					moveState = MoveState::StraightConstant;
				}
			}
			break;
		}
		case MoveState::StandBy: {
			//Commentaire:
			//STandby

			break;
		}
		case MoveState::TargetPosition: {
			//Commentaire:
			//Asservissement en position sur la cible
			//Angle:     Vitesse nulle
			//Distance : Vitesse nulle

			//Calcul
			//Calcul:
			//  nbPeriodeAsservissementSurCible = nbPeriodeAsservissementSurCible - 1
			nbLoopsOnTarget--;

			//D�cision
			//D�cision: nbPeriodeAsservissementSurCible <= 0?
			if (nbLoopsOnTarget <= 0) {
				//Calcul
				//Calcul:
				//  etat_deplacement = 0
				moveState = MoveState::StandBy;

				//Calcul
				//Calcul:
				//  nbPeriodeAsservissementSurCible = NBPERIODASSERVSURCIBLE
				//  //RQ: attention, changer aussi cette valeur dans l'initialisation
				nbLoopsOnTarget = LOOPS_ON_TARGET;

				adjustAngle();
				adjustDistance();
			} else {
				//D�cision
				//D�cision: recallage?
#if 0 // Disabled code
				if (adjustPositionning) {
					//Calcul
					//Calcul:
					//  recallage = false
					adjustPositionning = FCV_FALSE;

					//Calcul
					//Calcul:
					//  consigneVitesseAngle = 0
					//  consigneVitesseCurviligne = 0
					//  consigne_distante_deja_parcouru = (mesureDistance32 / DIVIDER_16_32_LIN)
					//  cibleCurviligne16 = 0
					//  consigneDistance16 = 0
					//  //cibleAngle = mesureAngle32 / DIVIDER_16_32_ANGLE
					//  consigneAngle16 = cibleAngle
					//  consigneDistance16 = cibleCurviligne16
					angleSpeedOrder = 0;
					curvilinearSpeedOrder = 0;
					distanceTravelled = (measuredDistance / FCV_DIVIDER_16_32_LIN);
					curvilinearTarget = 0;
					distanceOrder = 0;
					angleOrder = angleTarget;
					distanceOrder = curvilinearTarget;
				} else {
#endif // #if 0: Disabled code
				//Calcul
				//Calcul:
				//  consigneVitesseAngle = 0
				//  consigneVitesseCurviligne = 0
				//  consigneAngle16 = cibleAngle
				//  consigneDistance16 = cibleCurviligne16
				angleSpeedOrder = 0;
				curvilinearSpeedOrder = 0;
				angleOrder = angleTarget;
				circleArcStartingAngleSteps = 0;
				circleArcFlag = false;
				distanceOrder = curvilinearTarget;
#if 0 // Disabled code
			}
#endif // #if 0: Disabled code
			}
			break;
		}
		case MoveState::StraightAcceleration: {
			//Commentaire:
			//Angle:     Vitesse nulle
			//Distance : Acc�l�ration

			//Calcul
			//Calcul:
			//  echec_deplacement = false
			//moveFailed = false;

			//D�cision
			//D�cision: in_obstacle == 1 && flag_emergencyBrake?
			if (obstacleDetected && obstacleAvoidance) {
				//Commentaire:
				//Freinage d'urgence

				//Calcul
				//Calcul:
				//  etat_deplacement = 9
				moveState = MoveState::EmergencyBrake;

				//D�cision
				//D�cision: cibleCurviligne16 >= consigneDistance16?
				if (curvilinearTarget >= distanceOrder) {
					//Calcul
					//Calcul:
					//  consigneVitesseAngle = 0
					//  consigneVitesseCurviligne = consigneVitesseCurviligne - .DECELERATION_LINEAIRE
					angleSpeedOrder = 0;
					curvilinearSpeedOrder -= linearDeceleration;
				} else {
					//Calcul
					//Calcul:
					//  consigneVitesseAngle = 0
					//  consigneVitesseCurviligne = consigneVitesseCurviligne + .DECELERATION_LINEAIRE
					angleSpeedOrder = 0;
					curvilinearSpeedOrder += linearDeceleration;
				}
			} else {
				//D�cision
				//D�cision: cibleCurviligne16 >= consigneDistance16?
				if (curvilinearTarget >= distanceOrder) {
					//Calcul
					//Calcul:
					//  consigneVitesseAngle = 0
					//  consigneVitesseCurviligne = consigneVitesseCurviligne + .ACCELERATION_LINEAIRE
					angleSpeedOrder = 0;
					curvilinearSpeedOrder += linearAcceleration;
				} else {
					//Calcul
					//Calcul:
					//  consigneVitesseAngle = 0
					//  consigneVitesseCurviligne = consigneVitesseCurviligne - .ACCELERATION_LINEAIRE
					angleSpeedOrder = 0;
					curvilinearSpeedOrder -= linearAcceleration;
				}

				//D�cision
				//D�cision: consigneVitesseCurviligne >= .VITESSE_LINEAIRE_MAX || consigneVitesseCurviligne <= -1 * .VITESSE_LINEAIRE_MAX?
				if (curvilinearSpeedOrder >= maxLinearSpeed || curvilinearSpeedOrder <= -maxLinearSpeed) {
					//Calcul
					//Calcul:
					//  etat_deplacement = 3
					moveState = MoveState::StraightConstant;
				}

				//Calcul
				//Calcul:
				//  .distanceFreinage = ((consigneVitesseCurviligne) / (2 * .DECELERATION_LINEAIRE)) * (consigneVitesseCurviligne / DIVIDER_VITESSE)
				brakingDistance = (curvilinearSpeedOrder / (2 * linearDeceleration)) * (curvilinearSpeedOrder / DIVIDER_SPEED);

				//D�cision
				//D�cision: cibleCurviligne16 >= consigneDistance16?
				if (curvilinearTarget >= distanceOrder) {
					//D�cision
					//D�cision: consigneDistance16 >= (cibleCurviligne16 - .distanceFreinage)?
					if (distanceOrder >= (curvilinearTarget - brakingDistance)) {
						//Calcul
						//Calcul:
						//  etat_deplacement = 4
						moveState = MoveState::StraightDeceleration;
					}
				} else {
					//D�cision
					//D�cision: consigneDistance16 <= (cibleCurviligne16 + .distanceFreinage)?
					if (distanceOrder <= (curvilinearTarget + brakingDistance)) {
						//Calcul
						//Calcul:
						//  etat_deplacement = 4
						moveState = MoveState::StraightDeceleration;
					}
				}
			}
			break;
		}
		case MoveState::StraightConstant: {
			//Commentaire:
			//Angle:     Vitesse nulle
			//Distance : Vitesse constante

			//D�cision
			//D�cision: cibleCurviligne16 >= consigneDistance16?
			//Calcul
			//Calcul:
			//  .sens = 1
			//Calcul
			//Calcul:
			//  .sens = -1

			bool backward = curvilinearTarget < distanceOrder;
			if (backward) {
				direction = -1;
			} else {
				direction = 1;
			}

			//Calcul
			//Calcul:
			//  .distanceFreinage = ((consigneVitesseCurviligne) / (2 * .DECELERATION_LINEAIRE)) * (consigneVitesseCurviligne / DIVIDER_VITESSE)
			brakingDistance = (curvilinearSpeedOrder / (2 * linearDeceleration)) * (curvilinearSpeedOrder / DIVIDER_SPEED);

			//D�cision
			//D�cision: .sens * consigneDistance16 >= .sens * (cibleCurviligne16 - .sens * .DISTANCE_FREINAGE_MAX)?
			//if (direction * distanceOrder >= direction * (curvilinearTarget - direction * maxBrakingDistance)) {
			if (direction * distanceOrder >= direction * (curvilinearTarget - direction * brakingDistance)) {
				//Commentaire:
				//Freinage

				//Calcul
				//Calcul:
				//  etat_deplacement = 4
				moveState = MoveState::StraightDeceleration;
			}

#if 0 // Disabled code
			//Appel de la Routine Composant
			//Appel de la Routine Composant: in_obstacle=Lire_�tat()
			obstacleDetected = FCD_SWITCH2_ReadState();

#endif // #if 0: Disabled code

			//D�cision
			//D�cision: in_obstacle == 1 && flag_emergencyBrake?
			if (obstacleDetected && obstacleAvoidance) {
				//Commentaire:
				//Freinage d'urgence

				//Calcul
				//Calcul:
				//  etat_deplacement = 9
				moveState = MoveState::EmergencyBrake;

				//D�cision
				//D�cision: cibleCurviligne16 >= consigneDistance16?
				if (curvilinearTarget >= distanceOrder) {
					//Calcul
					//Calcul:
					//  consigneVitesseAngle = 0
					//  consigneVitesseCurviligne = consigneVitesseCurviligne - .DECELERATION_LINEAIRE
					angleSpeedOrder = 0;
					curvilinearSpeedOrder -= linearDeceleration;
				} else {
					//Calcul
					//Calcul:
					//  consigneVitesseAngle = 0
					//  consigneVitesseCurviligne = consigneVitesseCurviligne + .DECELERATION_LINEAIRE
					angleSpeedOrder = 0;
					curvilinearSpeedOrder += linearDeceleration;
				}
			}
			break;
		}
		case MoveState::StraightDeceleration: {
			//Commentaire:
			//Angle:     Vitesse nulle
			//Distance : D�c�l�ration

			//D�cision
			//D�cision: cibleCurviligne16 >= consigneDistance16?
			if (curvilinearTarget >= distanceOrder) {
				//Calcul
				//Calcul:
				//  consigneVitesseAngle = 0
				//  consigneVitesseCurviligne = consigneVitesseCurviligne - .DECELERATION_LINEAIRE
				angleSpeedOrder = 0;
				curvilinearSpeedOrder -= linearDeceleration;

				//D�cision
				//D�cision: consigneVitesseCurviligne <= .VIT_MIN_AVT_ASV_CIBLE_LIN?
				if (curvilinearSpeedOrder <= minLinearSpeedBeforeTargetControl) {
					//Calcul
					//Calcul:
					//  etat_deplacement = 1
					moveState = MoveState::TargetPosition;
				}
			} else {
				//Calcul
				//Calcul:
				//  consigneVitesseAngle = 0
				//  consigneVitesseCurviligne = consigneVitesseCurviligne + .DECELERATION_LINEAIRE
				angleSpeedOrder = 0;
				curvilinearSpeedOrder += linearDeceleration;

				//D�cision
				//D�cision: consigneVitesseCurviligne >= - .VIT_MIN_AVT_ASV_CIBLE_LIN?
				if (curvilinearSpeedOrder >= -minLinearSpeedBeforeTargetControl) {
					//Calcul
					//Calcul:
					//  etat_deplacement = 1
					moveState = MoveState::TargetPosition;
				}
			}
			break;
		}
		case MoveState::RotationAcceleration: {
			//Commentaire:
			//Angle:     Acc�l�ration
			//Distance : Vitesse nulle

			//Calcul
			//Calcul:
			//  echec_deplacement = false
			//moveFailed = false;

			//D�cision
			//D�cision: cibleAngle >= consigneAngle16?
			if (angleTarget >= angleOrder) {
				//Calcul
				//Calcul:
				//  consigneVitesseAngle = consigneVitesseAngle + .ACCELERATION_ANGULAIRE
				//  consigneVitesseCurviligne = 0
				angleSpeedOrder += angularAcceleration;
				curvilinearSpeedOrder = 0;

			} else {
				//Calcul
				//Calcul:
				//  consigneVitesseAngle = consigneVitesseAngle - .ACCELERATION_ANGULAIRE
				//  consigneVitesseCurviligne = 0
				angleSpeedOrder -= angularAcceleration;
				curvilinearSpeedOrder = 0;
			}

			//Commentaire:
			//Fin d'acc�l�ration

			//D�cision
			//D�cision: consigneVitesseAngle >= .VITESSE_ANGULAIRE_MAX || consigneVitesseAngle <= -1 * .VITESSE_ANGULAIRE_MAX?
			if (angleSpeedOrder >= maxAngularSpeed || angleSpeedOrder <= -maxAngularSpeed) {
				//Calcul
				//Calcul:
				//  etat_deplacement = 7
				moveState = MoveState::RotationConstant;
			}

			//Commentaire:
			//D�c�l�ration

			//Calcul
			//Calcul:
			//  .distanceFreinage = ((consigneVitesseAngle) / (2 * .DECELERATION_ANGULAIRE)) * (consigneVitesseAngle / DIVIDER_VITESSE)
			brakingDistance = (angleSpeedOrder / (2 * angularDeceleration)) * (angleSpeedOrder / DIVIDER_SPEED);

			//D�cision
			//D�cision: cibleAngle >= consigneAngle16?
			if (angleTarget >= angleOrder) {
				//D�cision
				//D�cision: consigneAngle16 >= (cibleAngle - .distanceFreinage)?
				if (angleOrder >= (angleTarget - brakingDistance)) {
					//Calcul
					//Calcul:
					//  etat_deplacement = 8
					moveState = MoveState::RotationDeceleration;
				}
			} else {
				//D�cision
				//D�cision: consigneAngle16 <= (cibleAngle + .distanceFreinage)?
				if (angleOrder <= (angleTarget + brakingDistance)) {
					//Calcul
					//Calcul:
					//  etat_deplacement = 8
					moveState = MoveState::RotationDeceleration;
				}
			}
			break;
		}
		case MoveState::RotationConstant: {
			//Commentaire:
			//Angle:     Vitesse constante
			//Distance : Vitesse nulle

			//D�cision
			//D�cision: cibleAngle >= consigneAngle16?
			//Calcul
			//Calcul:
			//  .sens = 1
			//Calcul
			//Calcul:
			//  .sens = -1

			bool backward = angleTarget < angleOrder;
			if (backward) {
				direction = -1;
			} else {
				direction = 1;
			}

			//Calcul
			//Calcul:
			//  consigneVitesseCurviligne = 0
			curvilinearSpeedOrder = 0;

			//Calcul
			//Calcul:
			//  .distanceFreinage = ((consigneVitesseAngle) / (2 * .DECELERATION_ANGULAIRE)) * (consigneVitesseAngle / DIVIDER_VITESSE)
			brakingDistance = (angleSpeedOrder / (2 * angularDeceleration)) * (angleSpeedOrder / DIVIDER_SPEED);

			//D�cision
			//D�cision: .sens * consigneAngle16 >= .sens * (cibleAngle - .sens * .DISTANCE_FREINAGE_MAX_ANGLE)?
			//if (direction * angleOrder >= direction * (targetAngle - direction * maxBrakingDistanceAngle)) {
			if (direction * angleOrder >= direction * (angleTarget - direction * brakingDistance)) {
				//Calcul
				//Calcul:
				//  etat_deplacement = 8
				moveState = MoveState::RotationDeceleration;
			}
			break;
		}
		case MoveState::RotationDeceleration: {
			//Commentaire:
			//Angle:     D�c�l�ration
			//Distance : Vitesse nulle

			//D�cision
			//D�cision: cibleAngle >= consigneAngle16?
			if (angleTarget >= angleOrder) {
				//Calcul
				//Calcul:
				//  consigneVitesseAngle = consigneVitesseAngle - .DECELERATION_ANGULAIRE
				//  consigneVitesseCurviligne = 0
				angleSpeedOrder -= angularDeceleration;
				curvilinearSpeedOrder = 0;

				//D�cision
				//D�cision: consigneVitesseAngle <= .VIT_MIN_AVT_ASV_CIBLE_ANGLE?
				if (angleSpeedOrder <= minAngularSpeedBeforeTargetControl) {
					//Calcul
					//Calcul:
					//  etat_deplacement = 1
					moveState = MoveState::TargetPosition;
				}
			} else {
				//Calcul
				//Calcul:
				//  consigneVitesseAngle = consigneVitesseAngle + .DECELERATION_ANGULAIRE
				//  consigneVitesseCurviligne = 0
				angleSpeedOrder += angularDeceleration;
				curvilinearSpeedOrder = 0;

				//D�cision
				//D�cision: consigneVitesseAngle >= -1 * .VIT_MIN_AVT_ASV_CIBLE_ANGLE?
				if (angleSpeedOrder >= -minAngularSpeedBeforeTargetControl) {
					//Calcul
					//Calcul:
					//  etat_deplacement = 1
					moveState = MoveState::TargetPosition;
				}
			}
			break;
		}
		case MoveState::EmergencyBrake: {
			//Commentaire:
			//Freinage d'urgence (detection de la balise)

			//D�cision
			//D�cision: in_obstacle == 0?
			if (!obstacleDetected) {
				//Calcul
				//Calcul:
				//  etat_deplacement = 2
				moveState = MoveState::StraightAcceleration;
				// R�initialisation du d�compte d'attente d'obstacle
				obstacleCount = 0;
			} else {
				//D�cision
				//D�cision: cibleCurviligne16 >= consigneDistance16?
				if (curvilinearTarget >= distanceOrder) {
					//Calcul
					//Calcul:
					//  consigneVitesseAngle = 0
					//  consigneVitesseCurviligne = consigneVitesseCurviligne - .DECELERATION_LINEAIRE
					angleSpeedOrder = 0;
					curvilinearSpeedOrder -= linearDeceleration + linearDeceleration / 2;

					//D�cision
					//D�cision: consigneVitesseCurviligne <= .VIT_MIN_AVT_ASV_CIBLE_LIN?
					if (curvilinearSpeedOrder <= minLinearSpeedBeforeTargetControl) {
						//Calcul
						//Calcul:
						//  consigneVitesseAngle = 0
						//  consigneVitesseCurviligne = 0
						//  //echec_deplacement = true
						//  //etat_deplacement = 1
						angleSpeedOrder = 0;
						curvilinearSpeedOrder = 0;
					}
				} else {
					//Calcul
					//Calcul:
					//  consigneVitesseAngle = 0
					//  consigneVitesseCurviligne = consigneVitesseCurviligne + .DECELERATION_LINEAIRE
					angleSpeedOrder = 0;
					curvilinearSpeedOrder += linearDeceleration + linearDeceleration / 2;

					//D�cision
					//D�cision: consigneVitesseCurviligne >= -1 * .VIT_MIN_AVT_ASV_CIBLE_LIN?
					if (curvilinearSpeedOrder >= -minLinearSpeedBeforeTargetControl) {
						//Calcul
						//Calcul:
						//  consigneVitesseAngle = 0
						//  consigneVitesseCurviligne = 0
						//  //echec_deplacement = true
						//  //etat_deplacement = 1
						angleSpeedOrder = 0;
						curvilinearSpeedOrder = 0;
					}
				}
				// Gestion de l'attente sur obstacle
				bool timeoutEnabled = obstacleTimeout > 0 && angleSpeedOrder == 0 && curvilinearSpeedOrder == 0;
				if (timeoutEnabled && obstacleCount++ > obstacleTimeout) {
					// abandon du mouvement et r�initialisation
					adjustDistance();
					errorState = MoveError::ObstacleTimeout;
					moveState = MoveState::StandBy;
					obstacleCount = 0;
				}
			}
			break;
		}
	}
}

void PositionControl::setMotorsCommands(int outLeftCorrection, int outRightCorrection) {
	// Sorties forc�es : on ignore l'�tat de l'asservissement
	if (overrideEnabled) {
		rightMotor.setSpeed(outOverrideRightCommand);
		leftMotor.setSpeed(outOverrideLeftCommand);
		return;
	}

	//D�cision
	//D�cision: flag_simulateCoders?
	if (!simulateCoders && motorsEnabled) {
		//Appel de la Routine Composant
		//Appel de la Routine Composant: SetDutyCycle10bit(512 + out_correctionDroite)
		rightMotor.setSpeed(outRightCorrection);

		//Appel de la Routine Composant
		//Appel de la Routine Composant: SetDutyCycle10bit(512 + out_correctionGauche)
		leftMotor.setSpeed(outLeftCorrection);
	} else {
		rightMotor.setSpeed(0);
		leftMotor.setSpeed(0);
	}
}

void PositionControl::start() {
	if (running) {
		return;
	}

	running = true;
	reset();

	time_measurement_t cpuStats;
	chTMObjectInit(&cpuStats);

	while (running) {
		chTMStartMeasurementX(&cpuStats);
		volatile systime_t time = chVTGetSystemTime();
		run();
		// temps �coul� pour le calcul de la charge CPU
		chTMStopMeasurementX(&cpuStats);
		cpuConsumed = cpuStats.last;
		chThdSleepUntilWindowed(time, time + MS2ST(interval));
	}
	// arr�t moteurs
	setMotorsCommands(0, 0);
}

void PositionControl::stop() {
	running = false;
}

unsigned int PositionControl::getInterval() const {
	return interval;
}

void PositionControl::setInterval(unsigned int value) {
	interval = value;
}

void PositionControl::reset() {
	distanceOrder = 0;
	distanceTravelled = 0;
	turns = 0;
	angleOrder = 0;
	circleArcStartingAngleSteps = 0;
	circleArcFlag = false;
	nbLoopsOnTarget = LOOPS_ON_TARGET;
	angleTarget = 0;
	angleSpeedOrder = 0;
	curvilinearTarget = 0;
	curvilinearSpeedOrder = 0;
	movingBackward = false;
	moveState = MoveState::StandBy;
	errorState = MoveError::None;
	obstacleAvoidance = false;
	overrunAllowed = false;
	previousDistanceError = 0;
	previousAngleError = 0;
	outRawLeftCommand = 0;
	outRawRightCommand = 0;
	outLeftCorrection = 0;
	outRightCorrection = 0;
	inMeasureLeftRaw = 0;
	inMeasureLeft = 0;
	inMeasureRightRaw = 0;
	inMeasureRight = 0;
	distanceError = 0;
	odometry.setPosition( { 0, 0, 0 });
	motorsEnabled = true;
	unlockedAngleControl = false;
	maxPower = maxPowerInit;
	maxError = maxErrorInit;
}

void PositionControl::codersInputAddSteps(int &inMeasureLeft, int &inMeasureRight) {
	if (motorsEnabled) {
		int n = k - 1;
		inMeasureRight = (outRightCorrection + (outRightCorrection >= 0 ? n : -n)) / k;
		inMeasureLeft = (outLeftCorrection + (outLeftCorrection >= 0 ? n : -n)) / k;
	} else {
		inMeasureLeft = 0;
		inMeasureRight = 0;
	}
}

bool PositionControl::isObstacleDetected() {
	// Si la trajectoire est simul�e, on ne tient pas compte des mesures
	if (simulateCoders) {
		return false;
	}
	const Position &position = getPosition();
	bool value = obstacleDetector.isObstacleDetected(position, movingBackward, obstaclePosition);
	return value;
}

const Position& PositionControl::getPosition() const {
	return odometry.getPosition();
}

void PositionControl::addCorrectionToCurvilinearTarget(int value) {
	curvilinearTarget += value;
}

void PositionControl::setMoveState(MoveState ms) {
	moveState = ms;
}

MoveState PositionControl::getMoveState() {
	return moveState;
}

int PositionControl::getDistanceError() {
	return distanceError;
}

void PositionControl::unlockAngleControl() {
	unlockedAngleControl = true;
}

void PositionControl::lockAngleControl() {
	unlockedAngleControl = false;
}

void PositionControl::setMaxPower(int value) {
	maxPower = value;
}

int PositionControl::getAngleMeasured() {
	return angleMeasured;
}

void PositionControl::abort() {
// TODO
	Position tmp(getPosition());
	reset();
	odometry.setPosition(tmp);
}

bool PositionControl::hasMoveFailed() const {
	bool result = (moveState == MoveState::StandBy) && (errorState != MoveError::None);
	return result;
}

MoveError PositionControl::getError() const {
	return errorState;
}

MoveState PositionControl::getState() const {
	return moveState;
}

void PositionControl::setPosition(float x, float y, float angle) {
	odometry.setPosition( { x, y, angle });
}

void PositionControl::updateOdometry(int inMeasureLeft, int inMeasureRight) {
	odometry.update(inMeasureLeft, inMeasureRight);
}

void PositionControl::getMotorsCommands(int &leftCommand, int &rightCommand) const {
	leftCommand = outRawLeftCommand;
	rightCommand = outRawRightCommand;
}

int PositionControl::getDistanceOrder() const {
	return distanceOrder;
}

int PositionControl::getDistanceTravelled() const {
	return distanceTravelled;
}

Coder& PositionControl::getLeftCoder() const {
	return leftCoder;
}

Coder& PositionControl::getRightCoder() const {
	return rightCoder;
}

void PositionControl::setOverrideMotorsCommands(bool enabled, int leftCommand, int rightCommand) {
	overrideEnabled = enabled;
	outOverrideLeftCommand = leftCommand;
	outOverrideRightCommand = rightCommand;
}

const MotorsEnergyUsage PositionControl::getEnergyUsage() const {
	MotorsEnergyUsage usage;
	usage.leftCharge = leftMotor.getCharge();
	usage.leftCurrent = leftMotor.getCurrent();
	usage.rightCharge = rightMotor.getCharge();
	usage.rightCurrent = rightMotor.getCurrent();
	return usage;
}

int PositionControl::getCPULoad() const {
	// mesure en �s, intervalle en ms -> charge CPU en �
	return RTC2US(STM32_SYSCLK, cpuConsumed) / interval;
}

void PositionControl::move(int steps) {
	if (moveState != MoveState::StandBy) {
		return;
	}
	clearError();
	movingBackward = steps < 0;
	curvilinearTarget += steps;
	moveState = MoveState::StraightAcceleration;
}

void PositionControl::rotate(int steps) {
	if (moveState != MoveState::StandBy) {
		return;
	}
	clearError();
	//Calcul
	//Calcul:
	//  cibleAngle = cibleAngle + .angle_pas
	//  etat_deplacement = 6
	angleTarget += steps;
	moveState = MoveState::RotationAcceleration;
}

void PositionControl::circleArc(int distanceSteps, int arcSteps) {
	if (moveState != MoveState::StandBy) {
		return;
	}
	clearError();
	movingBackward = distanceSteps < 0;

	circleArcStartingAngleSteps = angleOrder;

	curvilinearTarget += distanceSteps;
	angleTarget += arcSteps;

	circleArcFlag = true;
	moveState = MoveState::StraightAcceleration;
}

void PositionControl::straitArcStrait(int totalDistanceSteps, int firstStraightSteps, int arcSteps) {
	if (moveState != MoveState::StandBy) {
		return;
	}
	clearError();

	movingBackward = totalDistanceSteps < 0;

	curvilinearTarget += totalDistanceSteps;

	moveState = MoveState::StraightAcceleration;

	if (!movingBackward) {
		while (distanceOrder < firstStraightSteps) {
			// doing nothing -> going straight
		}
	} else {
		while (distanceOrder > firstStraightSteps) {
			// doing nothing
		}
	}

	angleTarget = arcSteps;
	moveState = MoveState::ClothoidAcceleration;

	// TODO ligne droite -> arc -> ligne droite
}

int PositionControl::getStepsByMeter() const {
	return stepsByMeter;
}

int PositionControl::getStepsByTurn() const {
	return stepsByTurn;
}

void PositionControl::rotateToOrientation(int steps) {
	const Position &position = getPosition();
	int orientation = ANGLE_TO_DECIDEGREES(position.angle) * stepsByTurn / 3600;

	//Calcul
	//Calcul:
	//  .angle_todo = (.angleAAteindre_pas) - cibleAngle
	int angle = steps - orientation;

	//D�cision
	//D�cision: .angle_todo < 0?
	if (angle < 0) {
		//Boucle
		//Boucle: Tant que .angle_todo < -1 * (pas_par_tour16 / 2)
		while (angle < -(stepsByTurn / 2)) {
			//Calcul
			//Calcul:
			//  .angle_todo = .angle_todo + pas_par_tour16
			angle += stepsByTurn;
		}
	} else {
		//Boucle
		//Boucle: Tant que .angle_todo > pas_par_tour16 / 2
		while (angle > stepsByTurn / 2) {
			//Calcul
			//Calcul:
			//  .angle_todo = .angle_todo - pas_par_tour16
			angle -= stepsByTurn;
		}
	}

	//Calcul
	//Calcul:
	//  cibleAngle = cibleAngle + .angle_todo
	angleTarget += angle;

#if 0 // Disabled code
//Calcul
//Calcul:
//  cibleAngle = .angleAAteindre_pas
	FCV_CIBLEANGLE = FCL_XCALL_ATTEINDRE_ANGLE_PAS_ANGLEAATEINDRE_PAS;

#endif // #if 0: Disabled code
	//Calcul
	//Calcul:
	//  etat_deplacement = 6
	moveState = MoveState::RotationAcceleration;
}

void PositionControl::setSpeedIndex(unsigned int index) {
	speed = index;
}

unsigned int PositionControl::getSpeedIndex() const {
	return speed;
}

void PositionControl::setReversed(bool value) {
	reversedRotations = value;
	odometry.reversed = value;
}

bool PositionControl::isReversed() const {
	return reversedRotations;
}

const Point& PositionControl::getObstaclePosition() const {
	return obstaclePosition;
}

const Position PositionControl::reposition(Navigation &navigation, int steps, int newXMM, int newYMM, int newAngleDeciDegrees) {
	const int thresholdErrorLinear = 800;
	const int thresholdErrorAngular = 1000;
	const int maxPower = 100;

	Position pOrg = getPosition();

	//D�finitions des variables locales
	int tmpSpeed = getSpeedIndex();
	bool tmpOverrunAllowed = overrunAllowed;
	bool tmpObstacleAvoidance = obstacleAvoidance;

	reset();
	setPosition(pOrg.x, pOrg.y, pOrg.angle);

	setSpeedIndex(SPEED_BOTH_RECALLAGE);

	overrunAllowed = true;

	addCorrectionToCurvilinearTarget(steps);
	setMoveState(MoveState::StraightAcceleration);

	//Lorsque que l'erreur deviens trop grande, arreter le robot et prendre les valeurs actuelles comme d�fault
	setMaxPower(maxPower);

	while ((getDistanceError() > -1 * thresholdErrorLinear) && (getDistanceError() < thresholdErrorLinear) && (getMoveState() != MoveState::StandBy)) {
		navigation.waitForDelay(10);
	}

	unlockAngleControl();

	while ((getDistanceError() > (-1 * thresholdErrorAngular)) && (getDistanceError() < thresholdErrorAngular) && (getMoveState() != MoveState::StandBy)) {
		navigation.waitForDelay(10);
	}

	Position pCorrected = getPosition();


	int newX = newXMM;
	int newY = newYMM;
	float newA = ANGLE_TO_RADIANS(0.1f * newAngleDeciDegrees);

	float errorX = pCorrected.x - newXMM;
	float errorY = pCorrected.y - newYMM;
	float errorA = normalizeAngle(pCorrected.angle) - newA;



	if (newX <= CURRENT) {
		newX = pCorrected.x;
		errorX = 0;
	}
	if (newY <= CURRENT) {
		newY = pCorrected.y;
		errorY = 0;
	}
	if (newA <= CURRENT) {
		newA = pCorrected.angle;
		errorA = 0;
	}

	int errorDistance = (errorX * errorX) + (errorY * errorY);

	if ((!ignoreRepositionError) && (errorDistance >= (maxRepositionErrorDistanceMM * maxRepositionErrorDistanceMM) || fabsf(ANGLE_TO_DECIDEGREES(errorA)) >= maxRepositionErrorAngleDeciDegree)) {
		//Keep actual measurment. Dont use updated value proposed by the user.
		newX = pCorrected.x;
		newY = pCorrected.y;
		newA = pCorrected.angle;
		//set all to zero to indicate to log the ignore recallage feature has been used.
		errorX = 0;
		errorY = 0;
		errorA = 0;
	}

	reset();

	overrunAllowed = tmpOverrunAllowed;
	obstacleAvoidance = tmpObstacleAvoidance;

	// TODO changer tmp pour injecter la nouvelle position recalcul�e.
	setPosition(newX, newY, newA);
	setSpeedIndex(tmpSpeed);
	return Position(errorX, errorY, errorA);
}

void PositionControl::setErrorRatioLeftVsRight(int value) {
	errorRatioLeftVsRight = value;
}

void PositionControl::setControlConfig(const ControlConfig &config) {
	if (config.stepsByTurn != 0) {
		stepsByTurn = config.stepsByTurn;
	}
	if (config.stepsByMeter != 0) {
		stepsByMeter = config.stepsByMeter;
	}

	odometry.setConfiguration(WHEELS_DISTANCE_MM(stepsByMeter, stepsByTurn) * 2, MM_PER_STEP(stepsByMeter) * 2);
}

void PositionControl::setObstacleTimeout(unsigned int value) {
	obstacleTimeout = value / interval;
	obstacleCount = 0;
}

void PositionControl::adjustAngle() {
	//Commentaire:
	//On rabat l'angle dans une valeur entre Pi et -Pi

	//D�cision
	//D�cision: consigneAngle16 < 0?
	if (angleOrder < 0) {
		//Boucle
		//Boucle: Tant que consigneAngle16 < -1 * (pas_par_tour16 / 2)
		while (angleOrder < -(stepsByTurn / 2)) {
			//Calcul
			//Calcul:
			//  consigneAngle16 = consigneAngle16 + pas_par_tour16
			//  cibleAngle = cibleAngle + pas_par_tour16
			//  tour = tour - 1
			angleOrder += stepsByTurn;
			angleTarget += stepsByTurn;
			turns--;
		}
	} else {
		//Boucle
		//Boucle: Tant que consigneAngle16 > pas_par_tour16 / 2
		while (angleOrder > stepsByTurn / 2) {
			//Calcul
			//Calcul:
			//  consigneAngle16 = consigneAngle16 - pas_par_tour16
			//  cibleAngle = cibleAngle - pas_par_tour16
			//  tour = tour + 1
			angleOrder -= stepsByTurn;
			angleTarget -= stepsByTurn;
			turns++;
		}
	}
}

void PositionControl::adjustDistance() {
	//Commentaire:
	//On accumule la distance et on repart � 0 pour ne jamais d�border des 16 bits

	//D�cision
	//D�cision: consigneDistance16 <> 0?
	if (distanceOrder != 0) {
		//Calcul
		//Calcul:
		//  consigne_distante_deja_parcouru = consigne_distante_deja_parcouru + consigneDistance16
		//  cibleCurviligne16 = 0
		//  consigneDistance16 = 0
		distanceTravelled += distanceOrder;
		curvilinearTarget = 0;
		distanceOrder = 0;
		circleArcStartingAngleSteps = 0;
		circleArcFlag = false;
	}
}

void PositionControl::clearError() {
	errorState = MoveError::None;
}
