/*
 * Navigation.h
 *
 *  Created on: 12 sept. 2017
 *      Author: Emmanuel
 */

#ifndef CONTROL_NAVIGATION_H_
#define CONTROL_NAVIGATION_H_

#include "control/PositionControl.h"
#include "control/AbstractMove.h"
#include "devices/io/EmergencyStop.h"
#include "devices/io/StartDetector.h"
#include "utils/Pointer.h"

/**
 * Distance minimum (en millim�tres) d'un mouvement de point � point.
 */
#define MIN_MOVETO_DISTANCE 5.0f

/**
 * �tat de navigation.
 */
enum class MoveCommand {
	/** En attente. */
	Idle,
	/** En d�placement. */
	Moving,
	/** En attente du signal de d�part. */
	WaitingForStart,
	/** En attente d'une dur�e. */
	WaitingForDelay,
	/** Match termin�. */
	Terminated,
	/** Arr�t d'urgence press�. */
	EmergencyStop
};

class Navigation;

/**
 * \brief �v�nements de navigation
 *
 * Interface de gestion des �v�nements de navigation
 */
class NavigationListener {
	friend class Navigation;

protected:
	/**
	 * R�ception des �v�nements de navigation.
	 * @param [in] l'objet navigation � l'origine de l'�v�nement
	 */
	virtual void onNavigationEvent(const Navigation& navigation) = 0;
};

/**
 * \brief �v�nements conditionnels de navigation
 *
 * Interface de gestion conditionnelle des �v�nements de navigation
 */
class NavigationConditionListener : NavigationListener {
	friend class Navigation;

private:
	bool executeOnce;
	bool done = false;
	int lastAction = 0;

	virtual void onNavigationEvent(const Navigation& navigation);

protected:
	/**
	 * \brief Condition
	 *
	 * �value la condition.
	 * @param [in] l'objet navigation � l'origine de l'�v�nement
	 * @return l'identifiant de l'action � ex�cuter, 0 si aucune condition n'est remplie
	 */
	virtual int onCheckCondition(const Navigation& navigation) = 0;
	/**
	 * \brief Action
	 *
	 * Ex�cute l'action lorsque la condition est remplie.
	 * @param [in] l'objet navigation � l'origine de l'�v�nement
	 * @param [in] action l'identifiant de l'action � ex�cuter
	 * @return true si l'action a �t� ex�cut�e
	 */
	virtual bool onConditionMet(const Navigation& navigation, int action) = 0;
	/**
	 * Marque le traitement comme termin�.
	 * Une fois le traitement termin�, la condition n'est plus jamais �valu�e.
	 */
	void setDone();

public:
	/**
	 * Constructeur.
	 * @param [in] executeOnce drapeau indiquant si la condition est �valu�e jusqu'� �tre vraie (true) ou en continu (false)
	 */
	NavigationConditionListener(bool executeOnce = true);
	/**
	 * D�termine si l'action est termin�e.
	 * @return true si l'action est termin�e
	 */
	bool isDone() const;
};

/**
 * \brief Navigation
 *
 * Gestionnaire de la navigation.
 */
class Navigation {
	friend class AbstractMove;

private:
	Pointer<AbstractMove> nextMove;
	PositionControl &control;
	StartDetector &startDetector;
	EmergencyStop &emergencyStop;
	MoveCommand moveCommand = MoveCommand::Idle;
	systime_t waitTimeout;
	thread_t *updater;

	void addMove(AbstractMove &move);
	void addMoveTo(const Point &destination, bool backwards);
	void addOrientationMove(float angle);
	void addRotation(float angle);
	void addStraightMove(int distance);
	void addXYMove(float angle, float distance);
	void asynchronousWait(Pointer<NavigationListener> callback = Pointer<NavigationListener>());
	void checkDelay();
	void checkEmergencyStop();
	void checkStanding();
	void checkStarted();
	void checkStatus();
	void executeNextMove();
	bool isUpdater() const;
	void moveTo(const Point &destination, bool backwards);
	void moveToArc(const Point &destination, int angleDeciDegrees, bool backwards);
	void moveToArcWithOrientation(const Point &destination, int orientationDeciDegrees, bool backwards);
	void moveToWithOrientation(const Point &destination, float angle, bool backwards);
	void setCommand(MoveCommand command);
	void setStarted();
	void synchronousWait(Pointer<NavigationListener> callback = Pointer<NavigationListener>());

public:
	/**
	 * Drapeau indiquant si les commandes sont ex�cut�es de mani�re asynchrone (true) ou synchrone (false)
	 */
	bool asynchronous = true;

	/**
	 * Constructeur.
	 * @param [in] control gestionnaire de l'asservissement
	 * @param [in] startDetector dispositif de d�tection de d�but de match
	 * @param [in] emergencyStop dispositif d'arr�t d'urgence
	 */
	Navigation(PositionControl &control, StartDetector &startDetector, EmergencyStop &emergencyStop);
	/**
	 * \brief Commande courante
	 *
	 * Renvoie la commande courante.
	 * @return la commande courante
	 */
	MoveCommand getCommand() const;
	/**
	 * \brief Asservissement
	 *
	 * Renvoie le gestionnaire de l'asservissement.
	 * @return le gestionnaire de l'asservissement
	 */
	PositionControl& getControl() const;
	/**
	 * \brief Conversion
	 *
	 * Convertit un angle en d�ci-degr�s vers un angle en incr�ments.
	 * @param [in] angle angle � convertir (d�)
	 * @return angle converti (incr�ments)
	 */
	int convertDeciDegreesToSteps(int angle) const;
	/**
	 * \brief Conversion
	 *
	 * Convertit une distance en millim�tres vers une distance en incr�ments.
	 * @param [in] distance distance � convertir (mm)
	 * @return distance convertie (incr�ments)
	 */
	int convertMMToSteps(int distance) const;
	/**
	 * \brief Conversion
	 *
	 * Convertit une distance en incr�ments vers une distance en millim�tres.
	 * @param [in] distance distance � convertir (incr�ments)
	 * @return distance convertie (mm)
	 */
	int convertStepsToMM(int distance) const;
	/**
	 * \brief Conversion
	 *
	 * Convertit un angle en incr�ments vers un angle en d�ci-degr�s.
	 * @param [in] angle angle � convertir (incr�ments)
	 * @return angle converti (d�)
	 */
	int convertStepsToDeciDegrees(int angle) const;
	/**
	 * \brief D�placement rectiligne
	 *
	 * Effectue un d�placement rectligne.
	 * @param [in] value la distance � parcourir (mm)
	 */
	void moveMM(int value);
	/**
	 * \brief D�placement rectiligne
	 *
	 * Effectue un d�placement rectiligne.
	 * @param [in] value la distance � parcourir (incr�ments)
	 */
	void moveSteps(int value);
	/**
	 * \brief D�placement en arc de cercle
	 *
	 * Effectue un d�placement en arc de cercle.
	 * @param [in] distance la distance rectiligne � parcourir (mm)
	 * @param [in] arc l'arc � effectuer (decidegree)
	 */
	void moveMMCircleDeciDegrees(int distance, int arcDecidegree);
	/**
	 * \brief D�placement en arc de cercle, with units in steps
	 *
	 * Effectue un d�placement en arc de cercle.
	 * @param [in] distance la distance rectiligne � parcourir (steps)
	 * @param [in] arc l'arc � effectuer (steps)
	 */
	void moveStepsCircleSteps(int distanceSteps, int arcSteps);
	/**
	 * \brief D�placement vers une position absolue
	 *
	 * Effectue un d�placement vers une position absolue en effectuant successivement
	 * une rotation puis un d�placement rectligne.
	 * @param [in] destination la position � atteindre
	 */
	void moveTo(const Point &destination);
	/**
	 * \brief D�placement vers une position absolue en arc de cercle
	 *
	 * Effectue un d�placement vers une position absolue en effectuant un arc de cercle
	 * @param [in] destination la position � atteindre
	 * @param [in] angleDeciDegrees l'angle de rotation en d�ci-degr�s (d�) correspondant
	 * au diff�rentiel d'orientation entre la position de d�part et la position finale
	 */
	void moveToArc(const Point &destination, int angleDeciDegrees);
	/**
	 * \brief D�placement vers une position absolue en arc de cercle en marche arri�re
	 *
	 * Effectue un d�placement vers une position absolue en effectuant un arc de cercle en marche arri�re
	 * @param [in] destination la position � atteindre
	 * @param [in] angleDeciDegrees l'angle de rotation en d�ci-degr�s (d�) correspondant
	 * au diff�rentiel d'orientation entre la position de d�part et la position finale
	 */
	void moveToArcBackwards(const Point &destination, int angleDeciDegrees);
	/**
	 * \brief D�placement vers une position absolue et une orientation absolue en arc de cercle
	 *
	 * Effectue un d�placement vers une position absolue et une orientation absolue en effectuant un arc de cercle
	 * @param [in] destination la position � atteindre
	 * @param [in] orientationDeciDegrees l'orientation finale en d�ci-degr�s (d�)
	 */
	void moveToArcWithOrientation(const Point &destination, int orientationDeciDegrees);
	/**
	 * \brief D�placement vers une position absolue et une orientation absolue en arc de cercle en marche arri�re
	 *
	 * Effectue un d�placement en marche arri�re vers une position absolue et une orientation absolue en effectuant un arc de cercle
	 * @param [in] destination la position � atteindre
	 * @param [in] orientationDeciDegrees l'orientation finale en d�ci-degr�s (d�)
	 */
	void moveToArcWithOrientationBackwards(const Point &destination, int orientationDeciDegrees);
	/**
	 * \brief D�placement vers une position absolue en marche arri�re
	 *
	 * Effectue un d�placement vers une position absolue en effectuant successivement
	 * une rotation puis un d�placement rectligne en marche arri�re.
	 * @param [in] destination la position � atteindre
	 */
	void moveToBackwards(const Point &destination);
	/**
	 * R�initialise la navigation � l'�tat Idle.
	 */
	void reset();
	/**
	 * \brief Recalage
	 *
	 * Effectue le recalage en utilisant la distance sp�cifi�e.
	 * @param [in] distanceMM la distance maximale de recalage (mm)
	 * @param [in] newXMM la nouvelle coordonn�e X (mm)
	 * @param [in] newYMM la nouvelle coordonn�e Y (mm)
	 * @param [in] newAngleDeciDegrees la nouvelle valeur angulaire de l'orientation (d�)
	 * @return l'erreur de position apr�s recalage
	 */
	const Position repositionMM(int distanceMM, int newXMM, int newYMM, int newAngleDeciDegrees);
	/**
	 * \brief Recalage
	 *
	 * Effectue le recalage en utilisant la distance sp�cifi�e.
	 * @param [in] distanceMM la distance maximale de recalage (mm)
	 * @param [in] newXMM la nouvelle coordonn�e X (mm) ou CURRENT pour conserver la valeur
	 * @param [in] newYMM la nouvelle coordonn�e Y (mm) ou CURRENT pour conserver la valeur
	 * @param [in] newAngleDeciDegrees la nouvelle valeur angulaire de l'orientation (d�) ou CURRENT pour conserver la valeur
	 * @return l'erreur de position apr�s recalage
	 */
	const Position repositionSteps(int distanceSteps, int newXMM, int newYMM, int newAngleDeciDegrees);
	/**
	 * \brief Rotation
	 *
	 * Effectue une rotation sur place.
	 * @param [in] value l'angle de rotation (d�)
	 */
	void rotateDeciDegrees(int value);
	/**
	 * \brief Rotation
	 *
	 * Effectue une rotation sur place de sorte � faire face au point sp�cifi�.
	 * @param [in] point le point vers lequel s'orienter
	 */
	void rotateToOrientation(const Point &point);
	/**
	 * \brief Rotation
	 *
	 * Effectue une rotation sur place pour atteindre l'orientation sp�cifi�e.
	 * @param [in] value l'angle � atteindre (d�)
	 */
	void rotateToOrientationDeciDegrees(int value);
	/**
	 * \brief Rotation
	 *
	 * Effectue une rotation sur place pour atteindre l'orientation sp�cifi�e.
	 * @param [in] value l'angle � atteindre (incr�ments)
	 */
	void rotateToOrientationSteps(int value);

//	void straitArcStraitTriangle(const Point &sommet, const Point &destination);

	void straitMMArcDecidegreeStraitMM(int distanceMMinit, int arcDecidegree, int distanceMMfinal);

	void straitArcStrait(int distanceStepInit, int arcStepInit, int distanceStepFinal);
	/**
	 * \brief Rotation
	 *
	 * Effectue une rotation sur place.
	 * @param [in] value l'angle de rotation (incr�ments)
	 */
	void rotateSteps(int value);
	/**
	 * \brief Activation/d�sactivation de la navigation.
	 *
	 * D�finit l'�tat d'activation de la navigation.
	 * @param [in] value l'�tat d'activation (true) ou de d�sactivation (false) de la navigation
	 */
	void setTerminated(bool value);
	/**
	 * \brief Mise � jour
	 *
	 * Met � jour l'�tat interne.
	 */
	void update();
	/**
	 * \brief Attente conditionnelle
	 *
	 * Attend la fin d'un d�placement en r�alisant �ventuellement une action.
	 * @param [in] le gestionnaire d'�v�nement
	 * @return true si l'action a �t� ex�cut�e
	 */
	bool waitForCondition(NavigationConditionListener &listener);
	/**
	 * \brief Attente
	 *
	 * Attend la dur�e sp�cifi�e.
	 * @param [in] value la dur�e d'attente (ms)
	 */
	void waitForDelay(unsigned int value);
	/**
	 * \brief Attente de fin de mouvement
	 *
	 * Attend la fin du mouvement en cours. Appelle le callback sp�cifi� durant le mouvement.
	 * @param [in] callback le callback � appeler durant le mouvement
	 */
	void waitForMove(Pointer<NavigationListener> callback = Pointer<NavigationListener>());
	/**
	 * \brief Attente de d�part.
	 *
	 * Attend le signal de d�part.
	 * @param [in] r�f�rence optionnelle � un �couteur appel� p�riodiquement durant l'attente
	 */
	void waitForStart(const Pointer<NavigationListener> &callback = Pointer<NavigationListener>());
};

/**
 * \brief Recalage
 *
 * Valeur utilis�e dans le recalage pour indiquer que la coordonn�e avant recalage est conserv�e
 */
#define CURRENT (-1e6f)

#endif /* CONTROL_NAVIGATION_H_ */
