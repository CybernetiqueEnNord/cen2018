/*
 * PositionControl.h
 *
 *  Created on: 21 juil. 2017
 *      Author: Emmanuel
 */

#ifndef CONTROL_POSITIONCONTROL_H_
#define CONTROL_POSITIONCONTROL_H_

#include "devices/coders/Coder.h"
#include "devices/io/ObstacleDetector.h"
#include "devices/motors/Motor.h"
#include "odometry/Odometry.h"

#define SPEED_BOTH_RECALLAGE 0

#define SPEED_SMALL_SAFE 1
#define SPEED_SMALL_INITIALISATION 2
#define SPEED_SMALL_NOMINAL 3
#define SPEED_SMALL_FAST 4
#define SPEED_SMALL_MAX_FAST_LINEAR 6
#define SPEED_SMALL_FAST_STARTING_ROTATION 5
#define SPEED_SMALL_MAX_STARTING_FAST_LINEAR 7

#define SPEED_SMALL_TEST 99

#define SPEED_BIG_INITIALISATION 11
#define SPEED_BIG_SLOW 12
#define SPEED_BIG_NOMINAL 13
#define SPEED_BIG_FAST 14

class Navigation;

/**
 * \brief Erreur de d�placement.
 *
 * Type d'erreur de d�placement.
 */
enum class MoveError {
	/** Aucune erreur. */
	None,
	/** D�passement du maximum d'erreur. */
	Overrun,
	/** D�passement du maximum d'erreur en mode calibration. */
	CalibrationOverrun,
	/** D�passement de la dur�e d'attente sur la d�tection d'un obstacle. */
	ObstacleTimeout
};

/**
 * �tat du d�placement.
 */
enum class MoveState {
	/** En attente. */
	StandBy = 0,
	/** Asservissement sur position cible. */
	TargetPosition = 1,
	/** Acc�l�ration droite. */
	StraightAcceleration = 2,
	/** Vitesse lin�aire constante. */
	StraightConstant = 3,
	/** D�c�l�ration droite. */
	StraightDeceleration = 4,
	/** Acc�l�ration en rotation. */
	RotationAcceleration = 6,
	/** Vitesse de rotation constante. */
	RotationConstant = 7,
	/** D�c�l�ration en rotation. */
	RotationDeceleration = 8,
	/** Freinage d� � un obstacle. */
	EmergencyBrake = 9,
	/** Acc�l�ration en clotho�de. */
	ClothoidAcceleration = 11,
	/** Arc de clotho�de. */
	ClothoidArc = 12,
	/** D�c�l�ration en clotho�de. */
	ClothoidDeceleration = 13
};

/**
 * \brief Param�tres de l'asservissement
 *
 * Param�tre de cont�le de l'asservissement.
 */
struct ControlParameters {
	/** Param�tre proportionnelle. */
	int P = 1;
	/** Param�tre d�riv�e. */
	int D = 0;
	/** Poids. */
	int weight = 1;
};

struct ControlConfig {
	int stepsByTurn = 1;
	int stepsByMeter = 1;
	ControlConfig(int stepsByTurn = 1, int stepsByMeter = 1);
};

/**
 * \brief Consommation �nerg�tique
 *
 * Structure relative � la consommation �nerg�tique des moteurs.
 */
struct MotorsEnergyUsage {
	/** Courant du moteur gauche (mA). */
	int leftCurrent;
	/** Courant du moteur droit (mA). */
	int rightCurrent;
	/** Charge consomm�e par le moteur gauche (mC). */
	int leftCharge;
	/** Charge consomm�e par le moteur droit (mC). */
	int rightCharge;
};

/**
 * \brief Asservissement
 *
 * Gestionnaire de l'asservissement.
 */
class PositionControl {
private:
	Odometry &odometry;
	Coder &leftCoder;
	Coder &rightCoder;
	Motor &leftMotor;
	Motor &rightMotor;
	ObstacleDetector &obstacleDetector;
	Point obstaclePosition = {0, 0};

	bool running = false;
	MoveState moveState = MoveState::StandBy;
	MoveError errorState = MoveError::None;
	int distanceError = 0;
	bool unlockedAngleControl = false;
	bool motorsEnabled = true;
	bool obstacleDetected = false;
	bool overrunAllowed = false;

	bool movingBackward = false;

	int distanceOrder = 0;
	int distanceTravelled = 0;
	int turns = 0;
	int angleOrder = 0;
	int nbLoopsOnTarget = 0;
	int angleTarget = 0;
	int angleSpeedOrder = 0;
	int curvilinearTarget = 0;
	int curvilinearSpeedOrder = 0;
	int previousDistanceError = 0;
	int previousAngleError = 0;
	int outRawLeftCommand = 0;
	int outRawRightCommand = 0;
	int outLeftCorrection = 0;
	int outRightCorrection = 0;
	int inMeasureLeft = 0;
	int inMeasureRight = 0;
	int inMeasureLeftRaw = 0;
	int inMeasureRightRaw = 0;
	int angleMeasured = 0;
	int circleArcStartingAngleSteps = 0;
	bool circleArcFlag = false;
	int errorRatioLeftVsRight = 0;

	bool overrideEnabled = false;
	int outOverrideLeftCommand = 0;
	int outOverrideRightCommand = 0;

	int maxPowerInit = 480;
	int maxErrorInit = 5000;
	int maxPower;
	int maxError;

	unsigned int obstacleTimeout = 0;
	unsigned int obstacleCount = 0;

	rtcnt_t cpuConsumed;

	/** Vitesse. */
	unsigned int speed = 0;

	/** Drapeau indiquant si les rotations se font en sens inverse. */
	bool reversedRotations = false;

	static Odometry& createOdometry();

	void adjustAngle();
	void adjustDistance();
	void clearError();
	void readCoders(int &inMeasureLeft, int &inMeasureRight);
	int restrict(int value) const;
	void run();
	void setMotorsCommands(int outLeftCorrection, int outRightCorrection);
	void updateMoveState();
	void updateOdometry(int inMeasureLeft, int inMeasureRight);
	void updatePosition();

	// TODO Recalage
	void addCorrectionToCurvilinearTarget(int value);
	int getDistanceError();
	void lockAngleControl();
	void unlockAngleControl();
	void setMaxPower(int value);
	void setMoveState(MoveState state);
	int getAngleMeasured();

public:

	MoveState getMoveState();

	/** Param�tre d'asservissement en angle. */
	ControlParameters angleParameters;
	/** Param�tre d'asservissement rectiligne. */
	ControlParameters distanceParameters;

	/** Intervalle de calcul de l'asservissement (ms). */
	unsigned int interval = 5;
	/** Drapeau indiquant si le retour des codeurs doit �tre simul�. */
	bool simulateCoders = false;
	/** Drapeau indiquant si la gestion des obstacles est active. */
	bool obstacleAvoidance = false;
	/** Constante de simulation des codeurs. */
	int k = 3;

	/** Nombre d'incr�ments par tour. */
	int stepsByTurn;
	/** Nombre d'incr�ments par m�tre. */
	int stepsByMeter;

	/*Distance max pour discr�diter un recallage sur un �l�ment de jeux*. */
	int maxRepositionErrorDistanceMM = 1000;
	/** Angle max pour discr�diter un recallage sur un �l�ment de jeux. */
	float maxRepositionErrorAngleDeciDegree = 900;
	/** Boolean to activate the above feature. */
	bool ignoreRepositionError = true;

	/**
	 * Callback invoqu� apr�s chaque it�ration.
	 */
	void (*callback)() = NULL;

	/**
	 * Constructeur.
	 * @param [in] leftCoder codeur gauche
	 * @param [in] rightCoder codeur droit
	 * @param [in] leftMotor moteur gauche
	 * @param [in] rightMotor moteur droit
	 * @param [in] obstacleDetector dispositif de d�tection d'obstacles
	 */
	PositionControl(Coder &leftCoder, Coder &rightCoder, Motor &leftMotor, Motor &rightMotor, ObstacleDetector &obstacleDetector);
	/**
	 * \brief Interruption du d�placement
	 *
	 * Interrompt le d�placement en cours.
	 */
	void abort();
	/**
	 * \brief Simulation des codeurs
	 *
	 * Effectue la simulation du mouvement par les codeurs.
	 * @param [out] inMeasureLeft mesure gauche
	 * @param [out] inMeasureRight mesure droite
	 */
	void codersInputAddSteps(int &inMeasureLeft, int &inMeasureRight);
	/**
	 * \brief Codeur gauche
	 *
	 * Renvoie le codeur gauche.
	 * @return le codeur gauche
	 */
	Coder& getLeftCoder() const;
	/**
	 * \brief Codeur droit
	 *
	 * Renvoie le codeur droit.
	 * @return le codeur droit
	 */
	Coder& getRightCoder() const;
	/**
	 * \brief Charge CPU
	 *
	 * Renvoie la charge CPU du calcul de l'asservissement.
	 * @return la charge CPU (�)
	 */
	int getCPULoad() const;
	/**
	 * \brief Consigne de distance
	 *
	 * Renvoie la consigne de distance courante.
	 * @return la consigne de distance
	 */
	int getDistanceOrder() const;
	/**
	 * \brief Distance parcourue
	 *
	 * Renvoie la distance parcourue.
	 * @return la distance parcourue (en incr�ments)
	 */
	int getDistanceTravelled() const;
	/**
	 * \brief Consommation �nerg�tique
	 *
	 * Renvoie la consommation �nerg�tique.
	 * @return la consommation �nerg�tique
	 */
	const MotorsEnergyUsage getEnergyUsage() const;
	/**
	 * \brief Erreur de d�placement
	 *
	 * Renvoie la nature de l'erreur courante.
	 * @return la nature de l'erreur courante
	 */
	MoveError getError() const;
	/**
	 * \brief Intervalle d'asservissement
	 *
	 * Renvoie la valeur de l'intervalle d'asservissement.
	 * @return la valeur de l'intervalle d'asservissement (ms)
	 */
	unsigned int getInterval() const;
	/**
	 * \brief Commandes moteur
	 *
	 * Renvoie la valeur des commandes des moteurs.
	 * @param [out] leftCommand commande du moteur gauche
	 * @param [out] rightCommand commande du moteur droit
	 */
	void getMotorsCommands(int &leftCommand, int &rightCommand) const;
	/**
	 * \brief Position de l'obstacle
	 *
	 * Renvoie la position du dernier obstacle d�tect�.
	 * @return la position du dernier obstacle d�tect�
	 */
	const Point& getObstaclePosition() const;
	/**
	 * \brief Position
	 *
	 * Renvoie la position courante.
	 * @return la position courante
	 */
	const Position& getPosition() const;
	/**
	 * \brief Vitesse
	 *
	 * Renvoie l'indice de la vitesse courante.
	 * @return l'indice de la vitesse courante.
	 */
	unsigned int getSpeedIndex() const;
	/**
	 * \brief �tat du d�placement
	 *
	 * Renvoie l'�tat du d�placement.
	 * @return l'�tat du d�placement
	 */
	MoveState getState() const;
	/**
	 * \brief Pas par tour
	 *
	 * Renvoie le nombre de pas pour parcourir un m�tre en d�placement rectiligne.
	 * @return le nombre de pas par m�tre
	 */
	int getStepsByMeter() const;
	/**
	 * \brief Pas par tour
	 *
	 * Renvoie le nombre de pas n�cessaires pour effectuer un tour sur soi-m�me.
	 * @return le nombre de pas par tour
	 */
	int getStepsByTurn() const;
	/**
	 * \brief �chec du d�placement
	 *
	 * Indique si le dernier d�placement a �chou�.
	 * @return true si le dernier d�placement a �chou�
	 */
	bool hasMoveFailed() const;
	/**
	 * \brief D�tection d'obstacle
	 *
	 * Indique si un obstacle est en cours de d�tection.
	 * @return true si un obstacle est actuellement d�tect�
	 */
	bool isObstacleDetected();
	/**
	 * \brief Inversion du rep�re
	 *
	 * D�termine si le rep�re est direct ou indirect.
	 * @return true si le rep�re est indirect, false s'il est direct
	 */
	bool isReversed() const;

	/**
	 * \brief D�placement rectiligne
	 *
	 * Effectue un d�placement rectiligne.
	 * @param [in] steps la distance � parcourir (incr�ments)
	 */
	void move(int steps);

	/**
	 * \brief D�placement en arc de cercle
	 *
	 * Effectue un d�placement rectiligne.
	 * @param [in] distance la distance rectiligne � parcourir (steps)
	 * @param [in] arc l'arc � effectuer (steps)
	 */
	void circleArc(int distanceSteps, int arcSteps);

	void straitArcStrait(int totalDistanceSteps, int firstStraightSteps, int arcSteps);

	/**
	 * \brief Recalage
	 *
	 * Effectue le recalage en utilisant la distance sp�cifi�e.
	 * @param [in] navigation l'objet navigation permettant la gestion de l'attente asynchrone
	 * @param [in] distanceMM la distance maximale de recalage (mm)
	 * @param [in] newXMM la nouvelle coordonn�e X (mm) ou CURRENT pour conserver la valeur
	 * @param [in] newYMM la nouvelle coordonn�e Y (mm) ou CURRENT pour conserver la valeur
	 * @param [in] newAngleDeciDegrees la nouvelle valeur angulaire de l'orientation (d�) ou CURRENT pour conserver la valeur
	 * @return l'erreur de position apr�s recalage
	 */
	const Position reposition(Navigation &navigation, int steps, int newXMM, int newYMM, int newAngleDeciDegrees);
	/**
	 * \brief R�initialisation
	 *
	 * R�initialise l'asservissement et l'odom�trie.
	 */
	void reset();
	/**
	 * \brief Rotation
	 *
	 * Effectue une rotation sur place.
	 * @param [in] steps l'angle de rotation � effectuer (incr�ments)
	 */
	void rotate(int steps);
	/**
	 * \brief Rotation
	 *
	 * Effectue une rotation sur place pour atteindre l'orientation sp�cifi�e.
	 * @param [in] steps l'angle � atteindre (incr�ments)
	 */
	void rotateToOrientation(int steps);
	/**
	 * \brief Calibration
	 *
	 * D�finit les facteurs de calibration pas par tour et pas par m�tre.
	 * @param [in] config les param�tres de calibration
	 */
	void setControlConfig(const ControlConfig &config);
	/**
	 * \brief Odom�trie
	 *
	 * D�finit le coefficient de correction de la mesure d'odom�trie de la
	 * roue gauche par rapport � la droite en fraction de la valeur mesur�e (1 / value).
	 * @param [in] value la valeur du coefficient de correction, 0 pour d�sactiver la correction
	 */
	void setErrorRatioLeftVsRight(int value);
	/**
	 * \brief Intervalle d'asservissement
	 *
	 * D�finit l'intervalle de calcul de l'asservissement.
	 * @param [in] value la valeur de l'intervalle d'asservissement (ms)
	 */
	void setInterval(unsigned int value);
	/**
	 * \brief Temps d'attente sur obstacle
	 *
	 * D�finit le temps d'attente maximal lorsqu'un obstacle est d�tect� pour
	 * consid�rer que le d�placement a �chou�.
	 * @param [in] value la dur�e du temps d'attente (ms), 0 pour d�sactiver la fonctionnalit�
	 */
	void setObstacleTimeout(unsigned int value);
	/**
	 * \brief Commandes moteur
	 *
	 * Surcharge la valeur courante des commandes moteur.
	 * @param [in] enabled true pour indiquer que les commandes de l'asservissement doivent
	 *             �tre remplac�es par les commandes sp�cifi�es,
	 *             false pour r�tablir les valeurs calcul�es par l'asservissement
	 * @param [in] leftCommand commande du moteur gauche
	 * @param [in] rightCommand commande du moteur droit
	 */
	void setOverrideMotorsCommands(bool enabled, int leftCommand, int rightCommand);
	/**
	 * \brief Position
	 *
	 * D�finit la position courante.
	 * @param [in] x la position selon l'axe X (mm)
	 * @param [in] y la position selon l'axe Y (mm)
	 * @param [in] angle l'angle correspondant � l'orientation (radians)
	 */
	void setPosition(float x, float y, float angle);
	/**
	 * \brief Inversion du rep�re
	 *
	 * D�termine si le rep�re est direct ou indirect.
	 * @param [in] value drapeau indiquant si le rep�re est direct (false) ou indirect (true)
	 */
	void setReversed(bool value);
	/**
	 * \brief Vitesse
	 *
	 * D�finit la vitesse.
	 * @param [in] index l'indice de la vitesse � d�finir
	 */
	void setSpeedIndex(unsigned int index);
	/**
	 * \brief D�marrage
	 *
	 * D�marre l'asservissement.
	 */
	void start();
	/**
	 * \brief Arr�t
	 *
	 * Arr�te l'asservissement.
	 */
	void stop();
};

#endif /* CONTROL_POSITIONCONTROL_H_ */
