/*
 * Navigation.cpp
 *
 *  Created on: 12 sept. 2017
 *      Author: Emmanuel
 */

#include "control/Navigation.h"
#include "control/moves/OrientationMove.h"
#include "control/moves/RotationMove.h"
#include "control/moves/StraightMove.h"
#include "control/moves/XYMove.h"
#include "devices/io/Console.h"
#include "devices/io/StatusLed.h"
#include "math/cenMath.h"

Navigation::Navigation(PositionControl &control, StartDetector &startDetector, EmergencyStop &emergencyStop) :
		control(control), startDetector(startDetector), emergencyStop(emergencyStop) {
}

MoveCommand Navigation::getCommand() const {
	return moveCommand;
}

void Navigation::setCommand(MoveCommand command) {
	moveCommand = command;
}

void Navigation::setStarted() {
	StatusLed &led = StatusLed::getInstance();
	led.blinkContinuously(50, 500);
	moveCommand = MoveCommand::Idle;
}

void Navigation::reset() {
	control.setOverrideMotorsCommands(true, 0, 0);
	// attente de stabilisation de l'asservissement
	chThdSleepMilliseconds(250);
	control.reset();
	moveCommand = MoveCommand::Idle;
	control.setOverrideMotorsCommands(false, 0, 0);
}

void Navigation::waitForStart(const Pointer<NavigationListener> &callback) {
	moveCommand = MoveCommand::WaitingForStart;
	StatusLed &led = StatusLed::getInstance();
	led.blinkContinuously(50, 50);
	synchronousWait(callback);
}

PositionControl& Navigation::getControl() const {
	return control;
}

void Navigation::moveMM(int value) {
	value = convertMMToSteps(value);
	moveSteps(value);
}

void Navigation::moveSteps(int value) {
	if (moveCommand != MoveCommand::Idle) {
		return;
	}
	moveCommand = MoveCommand::Moving;
	control.move(value);
	synchronousWait();
}

void Navigation::moveToArcBackwards(const Point &destination, int angleDeciDegrees) {
	moveToArc(destination, angleDeciDegrees, true);
}

void Navigation::moveToArc(const Point &destination, int angleDeciDegrees) {
	moveToArc(destination, angleDeciDegrees, false);
}

void Navigation::moveToArcWithOrientation(const Point &destination, int orientationDeciDegrees) {
	moveToArcWithOrientation(destination, orientationDeciDegrees, false);
}

void Navigation::moveToArcWithOrientationBackwards(const Point &destination, int orientationDeciDegrees) {
	moveToArcWithOrientation(destination, orientationDeciDegrees, true);
}

void Navigation::moveToArcWithOrientation(const Point &destination, int orientationDeciDegrees, bool backwards) {
	// calcul du diff�rentiel d'angle
	float finalOrientation = ANGLE_TO_RADIANS(0.1f * orientationDeciDegrees);
	const Position &current = control.getPosition();
	float angle = finalOrientation - current.angle;
	int angleDeciDegrees = ANGLE_TO_DECIDEGREES(angle);
	moveToArc(destination, angleDeciDegrees, backwards);
}

void Navigation::moveToArc(const Point &destination, int angleDeciDegrees, bool backwards) {
	// Rayon de courbure minimum. Doit �tre au moins >= demi-distance des roues pour satisfaire les conditions d'acc�l�ration constante
	const float RADIUS_ARC_MIN = 30.0f;

	if (angleDeciDegrees == 0) {
		moveTo(destination);
		return;
	} else if (angleDeciDegrees < -1800 || angleDeciDegrees > 1800) {
		// On limite la plage d'angle � [-180� ; +180�]
		float angle = ANGLE_TO_RADIANS(0.1f * angleDeciDegrees);
		angle = normalizeAngle(angle);
		angleDeciDegrees = (int) ANGLE_TO_DECIDEGREES(angle);
	}

	// Calcul de la longueur de l'arc
	const Position &current = control.getPosition();
	float semiStraightDistance = destination.getDistance(current) / 2;
	float semiAngle = fabsf(ANGLE_TO_RADIANS(0.1f * angleDeciDegrees) / 2);
	float radius = semiStraightDistance / sinf(semiAngle);

	// Si l'arc est trop serr�, on remplace par un mouvement droit + rotation
	if (radius < RADIUS_ARC_MIN) {
		float angle = ANGLE_TO_RADIANS(0.1f * angleDeciDegrees);
		angle += current.angle;
		moveToWithOrientation(destination, angle, backwards);
		return;
	}

	int arcLength = (int) (semiAngle * 2 * radius);
	if (backwards) {
		arcLength = -arcLength;
	}
	moveMMCircleDeciDegrees(arcLength, angleDeciDegrees);
}

void Navigation::moveToWithOrientation(const Point &destination, float angle, bool backwards) {
	addMoveTo(destination, backwards);
	addOrientationMove(angle);
	if (moveCommand == MoveCommand::Idle) {
		executeNextMove();
	}
}

void Navigation::moveMMCircleDeciDegrees(int distanceMM, int arcDeciDegrees) {
	distanceMM = convertMMToSteps(distanceMM);
	arcDeciDegrees = convertDeciDegreesToSteps(arcDeciDegrees);
	moveStepsCircleSteps(distanceMM, arcDeciDegrees); //distance in steps and arcDecidegree is in steps, but we dont want to create a temps variavle just for here.
}

void Navigation::moveStepsCircleSteps(int distanceSteps, int arcSteps) {
	if (moveCommand != MoveCommand::Idle) {
		return;
	}
	moveCommand = MoveCommand::Moving;
	control.circleArc(distanceSteps, arcSteps);
	synchronousWait();
}

//void Navigation::straitArcStraitTriangle(const Point &sommet, const Point &destination)
//{
//	Point &currentPos = control.getPosition();
//	int firstLength = sommet.getDistance(currentPos);
//	int secondLength = destination.getDistance(sommet);
//	int rayon = firstLength > secondLength ? secondLength : firstLength; // min
//	int distanceMMinit = firstLength - rayon;
//	int distanceMMfinal = secondLength - rayon;
//// TODO calculer l'angle
//	straitMMArcDecidegreeStraitMM(distanceMMinit, arcDecidegree, distanceMMfinal);
//}

void Navigation::straitMMArcDecidegreeStraitMM(int distanceMMinit, int arcDecidegree, int distanceMMfinal) {
	straitArcStrait(convertMMToSteps(distanceMMinit), convertDeciDegreesToSteps(arcDecidegree), convertMMToSteps(distanceMMfinal));
}

void Navigation::straitArcStrait(int distanceStepInit, int arcStepInit, int distanceStepFinal) {
	// | arcstep missing
	// V
	control.straitArcStrait(distanceStepInit + distanceStepFinal + 0, distanceStepInit, arcStepInit);
}

void Navigation::rotateSteps(int value) {
	if (moveCommand != MoveCommand::Idle) {
		return;
	}
	moveCommand = MoveCommand::Moving;
	control.rotate(value);
	synchronousWait();
}

void Navigation::rotateToOrientation(const Point &point) {
	const Position &current = control.getPosition();
	float angle, distance;
	// calcul du diff�rentiel d'angle entre la position courante et le point
	current.getAngleAndDistance(point, angle, distance);
	int angleDeciDegrees = ANGLE_TO_DECIDEGREES(angle);
	rotateDeciDegrees(angleDeciDegrees);
}

void Navigation::rotateToOrientationDeciDegrees(int value) {
	value = convertDeciDegreesToSteps(value);
	rotateToOrientationSteps(value);
}

void Navigation::rotateToOrientationSteps(int value) {
	if (moveCommand != MoveCommand::Idle) {
		return;
	}
	moveCommand = MoveCommand::Moving;
	control.rotateToOrientation(value);
	synchronousWait();
}

void Navigation::rotateDeciDegrees(int value) {
	value = convertDeciDegreesToSteps(value);
	rotateSteps(value);
}

int Navigation::convertDeciDegreesToSteps(int angle) const {
	int stepsByTurn = control.getStepsByTurn();
	return angle * stepsByTurn / 3600;
}

int Navigation::convertMMToSteps(int distance) const {
	int stepsByMeter = control.getStepsByMeter();
	return distance * stepsByMeter / 1000;
}

int Navigation::convertStepsToMM(int distance) const {
	int stepsByMeter = control.getStepsByMeter();
	return distance * 1000 / stepsByMeter;
}

int Navigation::convertStepsToDeciDegrees(int angle) const {
	int stepsByTurn = control.getStepsByTurn();
	return angle * 3600 / stepsByTurn;
}

void Navigation::synchronousWait(Pointer<NavigationListener> callback) {
	checkEmergencyStop();
	// le thread de mise � jour (qui entre par update() est toujours asynchrone)
	if (asynchronous || isUpdater()) {
		return;
	}
	while (moveCommand != MoveCommand::Idle) {
		if (callback.isValid()) {
			callback->onNavigationEvent(*this);
		}
		chThdSleepMilliseconds(10);
		checkStatus();
	}
}

void Navigation::asynchronousWait(Pointer<NavigationListener> callback) {
	checkEmergencyStop();
	if (!asynchronous) {
		return;
	}
	while (moveCommand != MoveCommand::Idle || nextMove.isValid()) {
		if (callback.isValid()) {
			callback->onNavigationEvent(*this);
		}
		chThdSleepMilliseconds(5);
		checkStatus();
	}
}

void Navigation::checkEmergencyStop() {
	if (moveCommand != MoveCommand::EmergencyStop && emergencyStop.isPressed()) {
		control.setOverrideMotorsCommands(true, 0, 0);
		moveCommand = MoveCommand::EmergencyStop;
	}
}

void Navigation::checkStatus() {
	checkEmergencyStop();
	switch (moveCommand) {
		case MoveCommand::Moving:
			checkStanding();
			break;

		case MoveCommand::WaitingForStart:
			checkStarted();
			break;

		case MoveCommand::WaitingForDelay:
			checkDelay();
			break;

		default:
			break;
	}
}

void Navigation::update() {
	if (!asynchronous || updater != NULL) {
		// un autre thread execute d�j� une mise � jour
		return;
	}

	updater = chThdGetSelfX();
	checkStatus();
	updater = NULL;
}

void Navigation::checkDelay() {
	systime_t time = chVTGetSystemTime();
	if (time >= waitTimeout) {
		moveCommand = MoveCommand::Idle;
	}
}

void Navigation::checkStanding() {
	MoveState state = control.getState();
	if (state == MoveState::StandBy) {
		moveCommand = MoveCommand::Idle;
		if (asynchronous) {
			executeNextMove();
		}
	}
}

void Navigation::checkStarted() {
	if (startDetector.isStarted()) {
		setStarted();
	}
}

void Navigation::waitForDelay(unsigned int value) {
	if (moveCommand != MoveCommand::Idle) {
		return;
	}
	moveCommand = MoveCommand::WaitingForDelay;
	systime_t time = chVTGetSystemTime();
	waitTimeout = time + MS2ST(value);
	if (!asynchronous) {
		while (time < waitTimeout) {
			checkEmergencyStop();
			if (moveCommand == MoveCommand::EmergencyStop) {
				return;
			}
			chThdSleepMilliseconds(10);
			time = chVTGetSystemTime();
		}
		moveCommand = MoveCommand::Idle;
	}
}

void Navigation::setTerminated(bool value) {
	switch (moveCommand) {
		case MoveCommand::Terminated:
			if (!value) {
				control.abort();
				moveCommand = MoveCommand::Idle;
			}
			break;

		default:
			if (value) {
				control.abort();
				moveCommand = MoveCommand::Terminated;
			}
			break;
	}
}

void Navigation::moveTo(const Point &destination, bool backwards) {
	addMoveTo(destination, backwards);
	if (moveCommand == MoveCommand::Idle) {
		executeNextMove();
	}
}

void Navigation::addMoveTo(const Point &destination, bool backwards) {
	const Position &current = control.getPosition();
	float angle, distance;
	current.getAngleAndDistance(destination, angle, distance);
	if (distance < MIN_MOVETO_DISTANCE) {
		return;
	}
	if (backwards) {
		angle = normalizeAngle(angle + M_PI);
		distance = -distance;
	}
	addXYMove(angle, distance);
}

void Navigation::moveTo(const Point &destination) {
	moveTo(destination, false);
}

void Navigation::moveToBackwards(const Point &destination) {
	moveTo(destination, true);
}

void Navigation::addXYMove(float angle, float distance) {
	static XYMove move;
	move.setAngleAndDistance(ANGLE_TO_DEGREES(angle) * 10, distance);
	addMove(move);
}

void Navigation::addOrientationMove(float angle) {
	static OrientationMove move;
	move.setAngle(ANGLE_TO_DEGREES(angle) * 10);
	addMove(move);
}

void Navigation::addRotation(float angle) {
	static RotationMove move;
	move.setAngle(ANGLE_TO_DEGREES(angle) * 10);
	addMove(move);
}

void Navigation::addStraightMove(int distance) {
	static StraightMove move;
	move.setDistance(distance);
	addMove(move);
}

void Navigation::addMove(AbstractMove &move) {
	if (nextMove.isValid()) {
		nextMove->setNext(move);
	} else {
		nextMove = &move;
	}
}

void Navigation::executeNextMove() {
	while (nextMove.isValid()) {
		nextMove->execute(*this);
		nextMove = nextMove->getNextMove();
		if (asynchronous) {
			break;
		}
	}
}

bool Navigation::isUpdater() const {
	thread_t *self = chThdGetSelfX();
	return self == updater;
}

const Position Navigation::repositionMM(int distanceMM, int newXMM, int newYMM, int newAngleDeciDegrees) {
	int value = convertMMToSteps(distanceMM);
	return repositionSteps(value, newXMM, newYMM, newAngleDeciDegrees);
}

const Position Navigation::repositionSteps(int distanceSteps, int newXMM, int newYMM, int newAngleDeciDegrees) {
	Position old = control.reposition(*this, distanceSteps, newXMM, newYMM, newAngleDeciDegrees);
	extern Console console;
	console.sendRepositionStatus(old);
	return old;
}

bool Navigation::waitForCondition(NavigationConditionListener &listener) {
	Pointer<NavigationListener> callback = Pointer<NavigationListener>(&listener);
	asynchronousWait(callback);
	bool result = listener.isDone();
	return result;
}

void Navigation::waitForMove(Pointer<NavigationListener> callback) {
	synchronousWait(callback);
}

NavigationConditionListener::NavigationConditionListener(bool executeOnce) :
		NavigationListener(), executeOnce(executeOnce) {
}

void NavigationConditionListener::onNavigationEvent(const Navigation &navigation) {
	if (done) {
		return;
	}
	int action = onCheckCondition(navigation);
	if (action != 0 && (!executeOnce || action != lastAction)) {
		onConditionMet(navigation, action);
	}
	lastAction = action;
}

bool NavigationConditionListener::isDone() const {
	return done;
}

void NavigationConditionListener::setDone() {
	done = true;
}
