/*
 * XYMove.h
 *
 *  Created on: 25 nov. 2017
 *      Author: Emmanuel
 */

#ifndef CONTROL_MOVES_XYMOVE_H_
#define CONTROL_MOVES_XYMOVE_H_

#include "control/AbstractMove.h"
#include "control/Navigation.h"

class XYMove : public AbstractMove {
private:
	int angleDeciDegrees;
	int distanceMM;

public:
	XYMove();
	virtual void execute(Navigation &navigation);
	void setAngleAndDistance(int angleDeciDegrees, int distanceMM);
};

#endif /* CONTROL_MOVES_XYMOVE_H_ */
