/*
 * OrientationMove.h
 *
 *  Created on: 29 mai 2019
 *      Author: emmanuel
 */

#ifndef CONTROL_MOVES_ORIENTATIONMOVE_H_
#define CONTROL_MOVES_ORIENTATIONMOVE_H_

#include "control/AbstractMove.h"
#include "control/Navigation.h"

class OrientationMove : public AbstractMove {
private:
	int angleDeciDegrees;

public:
	OrientationMove();
	OrientationMove(int angleDeciDegrees);
	void setAngle(int angleDeciDegrees);
	virtual void execute(Navigation &navigation);
};

#endif /* CONTROL_MOVES_ORIENTATIONMOVE_H_ */
