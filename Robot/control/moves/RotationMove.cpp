/*
 * RotationMove.cpp
 *
 *  Created on: 16 nov. 2017
 *      Author: Emmanuel
 */

#include "RotationMove.h"

RotationMove::RotationMove() :
		RotationMove(0) {
}

RotationMove::RotationMove(int angleDeciDegrees) :
		angleDeciDegrees(angleDeciDegrees) {
}

void RotationMove::execute(Navigation &navigation) {
	navigation.rotateDeciDegrees(angleDeciDegrees);
}

void RotationMove::setAngle(int angleDeciDegrees) {
	this->angleDeciDegrees = angleDeciDegrees;
}
