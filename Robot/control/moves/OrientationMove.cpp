/*
 * OrientationMove.cpp
 *
 *  Created on: 29 mai 2019
 *      Author: emmanuel
 */

#include "OrientationMove.h"

OrientationMove::OrientationMove() :
		OrientationMove(0) {
}

OrientationMove::OrientationMove(int angleDeciDegrees) :
		angleDeciDegrees(angleDeciDegrees) {
}

void OrientationMove::execute(Navigation &navigation) {
	navigation.rotateToOrientationDeciDegrees(angleDeciDegrees);
}

void OrientationMove::setAngle(int angleDeciDegrees) {
	this->angleDeciDegrees = angleDeciDegrees;
}
