/*
 * StraightMove.cpp
 *
 *  Created on: 16 nov. 2017
 *      Author: Emmanuel
 */

#include "StraightMove.h"

StraightMove::StraightMove() :
		StraightMove(0) {
}

StraightMove::StraightMove(int distanceMM) :
		distanceMM(distanceMM) {
}

void StraightMove::execute(Navigation &navigation) {
	navigation.moveMM(distanceMM);
}

void StraightMove::setDistance(int distanceMM) {
	this->distanceMM = distanceMM;
}
