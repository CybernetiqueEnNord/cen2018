/*
 * XYMove.cpp
 *
 *  Created on: 25 nov. 2017
 *      Author: Emmanuel
 */

#include "control/moves/StraightMove.h"
#include "control/moves/XYMove.h"

XYMove::XYMove() :
		angleDeciDegrees(0), distanceMM(0) {
}

void XYMove::execute(Navigation &navigation) {
	if (angleDeciDegrees != 0) {
		static StraightMove move;
		move.setDistance(distanceMM);
		setNext(move);
		navigation.rotateDeciDegrees(angleDeciDegrees);
	} else {
		navigation.moveMM(distanceMM);
	}
}

void XYMove::setAngleAndDistance(int angleDeciDegrees, int distanceMM) {
	this->angleDeciDegrees = angleDeciDegrees;
	this->distanceMM = distanceMM;
	clearNext();
}
