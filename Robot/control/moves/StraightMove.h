/*
 * StraightMove.h
 *
 *  Created on: 16 nov. 2017
 *      Author: Emmanuel
 */

#ifndef CONTROL_MOVES_STRAIGHTMOVE_H_
#define CONTROL_MOVES_STRAIGHTMOVE_H_

#include "control/AbstractMove.h"
#include "control/Navigation.h"

class StraightMove : public AbstractMove {
private:
	int distanceMM;

public:
	StraightMove();
	StraightMove(int distanceMM);
	void setDistance(int distanceMM);
	virtual void execute(Navigation &navigation);
};

#endif /* CONTROL_MOVES_STRAIGHTMOVE_H_ */
