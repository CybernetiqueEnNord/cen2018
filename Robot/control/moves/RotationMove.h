/*
 * RotationMove.h
 *
 *  Created on: 16 nov. 2017
 *      Author: Emmanuel
 */

#ifndef CONTROL_MOVES_ROTATIONMOVE_H_
#define CONTROL_MOVES_ROTATIONMOVE_H_

#include "control/AbstractMove.h"
#include "control/Navigation.h"

class RotationMove : public AbstractMove {
private:
	int angleDeciDegrees;

public:
	RotationMove();
	RotationMove(int angleDeciDegrees);
	void setAngle(int angleDeciDegrees);
	virtual void execute(Navigation &navigation);
};

#endif /* CONTROL_MOVES_ROTATIONMOVE_H_ */
