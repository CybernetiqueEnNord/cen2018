/*
 * NavigationMap.h
 *
 *  Created on: 27 d�c. 2018
 *      Author: emmanuel
 */

#ifndef CONTROL_MAP_NAVIGATIONMAP_H_
#define CONTROL_MAP_NAVIGATIONMAP_H_

#include "ch.h"
#include "NavigationArc.h"
#include "NavigationNode.h"

// Rayon de l'obstacle (mm)
#define OBSTACLE_SIZE 200
// surco�t de navigation en pr�sence d'obstacle
#define OBSTACLE_COST 10000
// diminution du surco�t (par seconde)
#define OBSTACLE_DECAY -1000

/**
 * \brief Carte de navigation
 *
 * Objet repr�sentant le graphe de navigation compos� des noeuds de navigation encapsulant les positions,
 * et des arcs de navigation encapsulant le co�t de d�placement.
 */
class NavigationMap {
private:
	NavigationNode * const * const nodes;
	const int nodesCount;
	NavigationArc * const * const arcs;
	const int arcsCount;
	bool valid = false;
	systime_t lastUpdate = 0;

	bool prepareMap();

public:
	/**
	 * Constructeur.
	 * @param [in] nodes liste des noeud du graphe de navigation
	 * @param [in] nodesCount nombre de noeuds dans la liste
	 * @param [in] arcs liste des arcs du graphe de navigation
	 * @param [in] arcsCount nombre d'arcs dans la liste
	 */
	NavigationMap(NavigationNode * const *nodes, int nodesCount, NavigationArc * const *arcs, int arcsCount);
	/**
	 * Ajoute un obstacle � la position sp�cifi�e.
	 * Augmente le co�t de navigation des arcs en intersection avec la zone d�finie par l'obstacle.
	 * @param [in] position la position de l'obstacle
	 */
	void addObstacle(const Point &position);
	/**
	 * R�duit les co�t de navigation li�s � la pr�sence ant�rieure d'obstacles.
	 */
	void decayCosts();
	/**
	 * \brief Liste des arcs
	 *
	 * Renvoie la liste des arcs.
	 * @param [out] arcsCount le nombre d'arcs dans la liste
	 * @return la liste des arcs du graphe.
	 */
	const NavigationArc * const * getArcs(int &arcsCount) const;
	/**
	 * \brief Liste des noeuds
	 *
	 * Renvoie la liste des noeuds.
	 * @param [out] nodesCount le nombre de noeuds dans la liste
	 * @return la liste des noeuds du graphe.
	 */
	NavigationNode * const * getNodes(int &nodesCount) const;
	/**
	 * Renvoie le nombre de noeuds dans le graphe de navigation.
	 * @return le nombre de noeuds
	 */
	int getNodesCount() const;
	/**
	 * D�termine si le graphe de navigation est valide.
	 * Cette m�thode doit �tre appel�e avant d'effectuer un calcul d'itin�raire.
	 * @return true si le graphe est valide pour le calcul d'itin�raire
	 */
	bool isValid() const;
};

#endif /* CONTROL_MAP_NAVIGATIONMAP_H_ */
