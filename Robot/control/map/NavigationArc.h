/*
 * NavigationPath.h
 *
 *  Created on: 27 d�c. 2018
 *      Author: emmanuel
 */

#ifndef CONTROL_MAP_NAVIGATIONARC_H_
#define CONTROL_MAP_NAVIGATIONARC_H_

#include "NavigationNode.h"

/**
 * \brief Arc de navigation
 *
 * Repr�sente un chemin de navigation entre deux noeuds.
 * L'arc est dot� d'un co�t de navigation qui est le somme :
 * - d'un co�t statique correspondant au co�t r�el de passage sur l'arc
 * - d'un co�t dynamique permettant de favoriser ou de p�naliser son usage (gestion des obstacles)
 */
class NavigationArc {
private:
	NavigationNode &start;
	NavigationNode &end;
	int staticCost;
	int dynamicCost;

public:
	/**
	 * Constructeur.
	 * @param [in] start le noeud de d�part
	 * @param [in] end le noeud d'arriv�e
	 * @param [in] staticCost le co�t statique de navigation
	 */
	NavigationArc(NavigationNode &start, NavigationNode &end, int staticCost = 0);
	/**
	 * \brief Ajustement du co�t
	 *
	 * Ajuste le co�t de navigation de cet arc en ajoutant la valeur sp�cifi�e au co�t dynamique.
	 * @param [in] la valeur � ajouter au co�t dynamique
	 */
	void adjustCost(int cost);
	/**
	 * D�termine si cet arc a pour extr�mit� le noeud sp�cifi�.
	 * @param [in] node un noeud
	 * @return true si le noeud sp�cifi� est une des extr�mit�s de cet arc
	 */
	bool contains(const NavigationNode &node) const;
	/**
	 * \brief Co�t de navigation
	 *
	 * Renvoie le co�t total de navigation de cet arc.
	 * @return le co�t total de navigation
	 */
	int getCost() const;
	/**
	 * Renvoie la distance au carr� du segment repr�sent� par l'arc au point sp�cifi�.
	 * @param [in] un point
	 * @return la distance au carr� de l'arc au point
	 */
	float getDistanceSquared(const Point &point) const;
	/**
	 * \brief Orientation
	 *
	 * Renvoie l'orientation de l'arc � partir des coordonn�es de ses extr�mit�s.
	 * @return l'orientation de l'arc en radians
	 */
	float getOrientation() const;
	/**
	 * \brief Orientation
	 *
	 * Renvoie l'orientation de l'arc en prenant le noeud sp�cifi� comme point d'arriv�e.
	 * @param [in] node le noeud d'arriv�e
	 * @return l'orientation de l'arc en radians
	 */
	float getOrientationAtEndNode(const NavigationNode &node) const;
	/**
	 * Renvoie l'autre extr�mit� de cet arc � partir d'une extr�mit�.
	 * @param [in] node un noeud
	 * @return l'autre extr�mit� de l'arc si le noeud sp�cifi� est bien une extr�mit� de cet arc, sinon le noeud sp�cifi�
	 */
	NavigationNode& getOtherEnd(NavigationNode &node) const;
	/**
	 * D�termine si l'arc a une intersection avec la zone circulaire d�finie par le point et le rayon sp�cifi�s.
	 * @param [in] point le centre de la zone circulaire
	 * @oaram [in] size le rayon de la zone circulaire en millim�tres
	 */
	bool intersects(const Point &point, int size) const;
	/**
	 * \brief Suppression du co�t dynamique
	 *
	 * Annule la valeur du co�t dynamique de cet arc.
	 */
	void resetCost();
};

#endif /* CONTROL_MAP_NAVIGATIONARC_H_ */
