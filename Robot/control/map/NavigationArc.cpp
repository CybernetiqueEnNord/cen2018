/*
 * NavigationPath.cpp
 *
 *  Created on: 27 d�c. 2018
 *      Author: emmanuel
 */

#include "NavigationArc.h"

#include "math/cenMath.h"

NavigationArc::NavigationArc(NavigationNode &start, NavigationNode &end, int staticCost) :
		start(start), end(end), staticCost(staticCost), dynamicCost(0) {
}

bool NavigationArc::contains(const NavigationNode &node) const {
	return node == start || node == end;
}

void NavigationArc::adjustCost(int cost) {
	dynamicCost += cost;
	if (dynamicCost < 0) {
		dynamicCost = 0;
	}
}

void NavigationArc::resetCost() {
	dynamicCost = 0;
}

int NavigationArc::getCost() const {
	return staticCost + dynamicCost;
}

NavigationNode& NavigationArc::getOtherEnd(NavigationNode &node) const {
	if (node == start) {
		return end;
	} else if (node == end) {
		return start;
	} else {
		return node;
	}
}

float NavigationArc::getOrientation() const {
	const Point &s = start.getPoint();
	const Point &e = end.getPoint();
	float dx = e.x - s.x;
	float dy = e.y - s.y;
	float o = atan2f(dy, dx);
	return o;
}

float NavigationArc::getOrientationAtEndNode(const NavigationNode &node) const {
	// si le noeud d'arriv�e est le noeud de fin, on a parcouru l'arc dans le sens de son orientation, l'orientation finale est donc celle de l'arc
	float o = getOrientation();
	if (node == start) {
		// si le noeud d'arriv�e est le noeud de d�but, on a parcouru l'arc en sens inverse, l'orientation finale est donc l'orientation de l'arc + 180�
		o = normalizeAngle(o + M_PI);
	} else if (node != end) {
		// le noeud n'appartient pas � l'arc
		o = 0;
	}
	return o;
}

float NavigationArc::getDistanceSquared(const Point &point) const {
	const Point &s = start.getPoint();
	const Point &e = end.getPoint();
	float a = point.x - s.x;
	float b = point.y - s.y;
	float c = e.x - s.x;
	float d = e.y - s.y;

	float dot = a * c + b * d;
	float lengthSquared = c * c + d * d;
	float param = fminf(1.0f, fmaxf(0.0f, (lengthSquared != 0.0f) ? dot / lengthSquared : 0.0f));

	float xx = s.x + param * c;
	float yy = s.y + param * d;

	float dx = point.x - xx;
	float dy = point.y - yy;
	float distance = dx * dx + dy * dy;
	return distance;
}

bool NavigationArc::intersects(const Point &point, int size) const {
	float d = getDistanceSquared(point);
	bool result = d < size * size;
	return result;
}
