/*
 * NavigationError.h
 *
 *  Created on: 27 d�c. 2018
 *      Author: emmanuel
 */

#ifndef CONTROL_MAP_NAVIGATIONERROR_H_
#define CONTROL_MAP_NAVIGATIONERROR_H_

enum class NavigationError {
	NeighboursOverflow,
	InvalidArc
};

#endif /* CONTROL_MAP_NAVIGATIONERROR_H_ */
