/*
 * NavigationService.cpp
 *
 *  Created on: 28 d�c. 2018
 *      Author: emmanuel
 */

#include "NavigationService.h"

#include <climits>
#include "devices/io/Console.h"
#include "math/cenMath.h"
#include "NavigationNode.h"

NavigationService::NavigationService(NavigationMap &map) :
		map(map) {
}

unsigned int NavigationService::computePath(const Point &source, const Point &destination, const Point *path[], unsigned int pathSize) const {
	if (!map.isValid()) {
		extern Console console;
		console.sendMessage("invalid map");
		return 0;
	}

	clearNodes();

	NavigationNode &sourceNode = getNearestNode(source);
	NavigationNode &destinationNode = getNearestNode(destination);

	// source
	sourceNode.distance = 0;
	NavigationNode *node = &sourceNode;

	// visit every node
	int n = map.getNodesCount();
	for (int i = 0; i < n; i++) {
		// loop around the edges of current node
		int arcsCount;
		auto arcs = node->getArcs(arcsCount);

		for (int j = 0; j < arcsCount; j++) {
			const NavigationArc &arc = *arcs[j];
			NavigationNode &neighbour = arc.getOtherEnd(*node);

			// only if not visited
			if (!neighbour.visited) {
				int tentative = node->distance + arc.getCost() + getRotationCost(*node, neighbour, arc);
				if (tentative < neighbour.distance) {
					neighbour.distance = tentative;
					neighbour.previous = node;
					neighbour.arcFromPrevious = &arc;
				}
			}
		}

		// all neighbours checked so node visited
		node->visited = true;

		// next node must be with shortest distance
		node = &getNodeShortestDistance();
	}

	// best path
	unsigned int i = 0;
	node = &destinationNode;
	while (node != NULL && node != &sourceNode && i < pathSize) {
		path[i++] = &node->getPoint();
		node = node->previous;
	}

	// reverse
	for (unsigned int j = 0; j < i / 2; j++) {
		const Point *p = path[j];
		path[j] = path[i - j - 1];
		path[i - j - 1] = p;
	}

	return i;
}

NavigationNode& NavigationService::getNodeShortestDistance() const {
	int index = 0;
	int min = INT_MAX;

	int nodesCount;
	auto nodes = map.getNodes(nodesCount);
	for (int i = 0; i < nodesCount; i++) {
		int d = nodes[i]->distance;
		if (!nodes[i]->visited && d < min) {
			min = d;
			index = i;
		}
	}
	return *nodes[index];
}

NavigationNode& NavigationService::getNearestNode(const Point &point) const {
	int nodesCount;
	auto nodes = map.getNodes(nodesCount);
	int min = INT_MAX;
	NavigationNode *result;
	for (int i = 0; i < nodesCount; i++) {
		auto node = nodes[i];
		int d = node->getDistance(point);
		if (d < min) {
			min = d;
			result = node;
		}
	}
	return *result;
}

void NavigationService::clearNodes() const {
	int nodesCount;
	auto nodes = map.getNodes(nodesCount);
	for (int i = 0; i < nodesCount; i++) {
		nodes[i]->clear();
	}
}

int NavigationService::getRotationCost(const NavigationNode &origin, const NavigationNode &destination, const NavigationArc &arc) const {
	if (!useRotationCost || origin.arcFromPrevious == NULL) {
		return 0;
	}
	const NavigationArc &previous = *origin.arcFromPrevious;
	float o1 = previous.getOrientationAtEndNode(origin);
	float o2 = arc.getOrientationAtEndNode(destination);
	float d = fabsf(diffAngle(o1, o2));
	int cost = ANGLE_TO_DEGREES(d) / 10;
	return cost;
}

void NavigationService::setUseRotationCost(bool value) {
	useRotationCost = value;
}

NavigationMap& NavigationService::getMap() const {
	return map;
}
