/*
 * NavigationMap2019.cpp
 *
 *  Created on: 25 mars 2019
 *      Author: Emmanuel
 */

#include "NavigationMap2019.h"

#include "utils/Arrays.h"

#define COL1 300.0f
#define COL2 725.0f
#define COL3 1325.0f

#define ROW1 725.0f
#define ROW2 1300.0f
#define ROW3 1700.0f
#define ROW4 2275.0f

NavigationMap& NavigationMap2019::getMap() {
	// noeuds
	static NavigationNode a1(Point(COL1, ROW1));
	static NavigationNode a2(Point(COL2, ROW1));
	static NavigationNode a3(Point(COL3, ROW1));

	static NavigationNode b1(Point(COL1, ROW2));
	static NavigationNode b2(Point(COL2, ROW2));
	static NavigationNode b3(Point(COL3, ROW2));

	static NavigationNode c1(Point(COL1, ROW3));
	static NavigationNode c2(Point(COL2, ROW3));
	static NavigationNode c3(Point(COL3, ROW3));

	static NavigationNode d1(Point(COL1, ROW4));
	static NavigationNode d2(Point(COL2, ROW4));
	static NavigationNode d3(Point(COL3, ROW4));

	static NavigationNode m(Point(0.5f * (COL1 + COL3), 1500));

	// points sur la diagonale
	static NavigationNode i1(Point(600.4, 1010.8));
	static NavigationNode i2(Point(904.5, 1300));
	static NavigationNode i3(Point(1114.7, 1500));

	// arcs grille
	static NavigationArc arc_a1_a2(a1, a2);
	static NavigationArc arc_a2_a3(a2, a3);

	static NavigationArc arc_a1_b1(a1, b1);
	static NavigationArc arc_a2_b2(a2, b2);
	static NavigationArc arc_a3_b3(a3, b3);

	static NavigationArc arc_b1_b2(b1, b2);
	static NavigationArc arc_b2_b3(b2, b3);

	static NavigationArc arc_b1_c1(b1, c1);
	static NavigationArc arc_b2_c2(b2, c2);
	static NavigationArc arc_b3_c3(b3, c3);

	static NavigationArc arc_c1_c2(c1, c2);
	static NavigationArc arc_c2_c3(c2, c3);

	static NavigationArc arc_c1_d1(c1, d1);
	static NavigationArc arc_c2_d2(c2, d2);
	static NavigationArc arc_c3_d3(c3, d3);

	static NavigationArc arc_d1_d2(d1, d2);
	static NavigationArc arc_d2_d3(d2, d3);

	// diagonales
	static NavigationArc arc_a1_c3(a1, c3);
	static NavigationArc arc_d1_b3(d1, b3);

	static NavigationArc arc_b1_m(b1, m);
	static NavigationArc arc_m_c3(m, c3);
	static NavigationArc arc_c1_m(c1, m);
	static NavigationArc arc_m_b3(m, b3);

	static NavigationArc arc_b1_d2(b1, d2);
	static NavigationArc arc_c1_a2(c1, a2);

	static NavigationNode *nodes[] = { &a1, &a2, &a3, &b1, &b2, &b3, &c1, &c2, &c3, &d1, &d2, &d3 };
	static NavigationArc *arcs[] = { &arc_a1_a2, &arc_a2_a3, &arc_a1_b1, &arc_a2_b2, &arc_a3_b3, &arc_b1_b2, &arc_b2_b3, &arc_b1_c1, &arc_b2_c2, &arc_b3_c3, &arc_c1_c2, &arc_c2_c3, &arc_c1_d1, &arc_c2_d2, &arc_c3_d3, &arc_d1_d2, &arc_d2_d3, &arc_a1_c3, &arc_d1_b3, &arc_b1_m, &arc_m_c3, &arc_c1_m, &arc_m_b3, &arc_b1_d2, &arc_c1_a2 };
	static NavigationMap map(nodes, ARRAY_SIZE(nodes), arcs, ARRAY_SIZE(arcs));

	return map;
}
