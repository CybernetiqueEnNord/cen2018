/*
 * NavigationMap2019.h
 *
 *  Created on: 25 mars 2019
 *      Author: Emmanuel
 */

#ifndef CONTROL_MAP_NAVIGATIONMAP2019_H_
#define CONTROL_MAP_NAVIGATIONMAP2019_H_

#include "control/map/NavigationMap.h"

class NavigationMap2019 {
public:
	static NavigationMap& getMap();
};

#endif /* CONTROL_MAP_NAVIGATIONMAP2019_H_ */
