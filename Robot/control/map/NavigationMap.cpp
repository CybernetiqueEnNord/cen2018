/*
 * NavigationMap.cpp
 *
 *  Created on: 27 d�c. 2018
 *      Author: emmanuel
 */

#include "NavigationMap.h"

NavigationMap::NavigationMap(NavigationNode * const *nodes, int nodesCount, NavigationArc * const *arcs, int arcsCount) :
		nodes(nodes), nodesCount(nodesCount), arcs(arcs), arcsCount(arcsCount) {
	valid = prepareMap();
}

bool NavigationMap::prepareMap() {
	bool success = true;
	for (int i = 0; i < nodesCount; i++) {
		NavigationNode *node = nodes[i];
		for (int j = 0; j < arcsCount; j++) {
			NavigationArc *arc = arcs[j];
			if (arc->contains(*node)) {
				success &= node->addArc(*arc);
			}
			if (!success) {
				return false;
			}
		}
	}
	return success;
}

const NavigationArc * const * NavigationMap::getArcs(int &arcsCount) const {
	arcsCount = this->arcsCount;
	return arcs;
}

NavigationNode * const * NavigationMap::getNodes(int &nodesCount) const {
	nodesCount = this->nodesCount;
	return nodes;
}

int NavigationMap::getNodesCount() const {
	return nodesCount;
}

bool NavigationMap::isValid() const {
	return valid;
}

void NavigationMap::addObstacle(const Point &position) {
	decayCosts();
	lastUpdate = chVTGetSystemTime();
	for (int i = 0; i < arcsCount; ++i) {
		NavigationArc *arc = arcs[i];
		if (arc->intersects(position, OBSTACLE_SIZE)) {
			arc->adjustCost(OBSTACLE_COST);
		}
	}
}

void NavigationMap::decayCosts() {
	if (lastUpdate == 0) {
		return;
	}

	int elapsed = ST2S(chVTGetSystemTime() - lastUpdate);
	int value = elapsed * OBSTACLE_DECAY;
	for (int i = 0; i < arcsCount; ++i) {
		NavigationArc *arc = arcs[i];
		arc->adjustCost(value);
	}
}
