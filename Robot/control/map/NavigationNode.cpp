/*
 * NavigationNode.cpp
 *
 *  Created on: 27 d�c. 2018
 *      Author: emmanuel
 */

#include "NavigationArc.h"
#include "NavigationNode.h"

#include "devices/io/Console.h"

#include <climits>

NavigationNode::NavigationNode(const Point &point) :
		point(point) {
}

float NavigationNode::getDistance(const Point &point) const {
	return point.getDistance(this->point);
}

bool NavigationNode::operator==(const NavigationNode &node) const {
	return node.point == point;
}

bool NavigationNode::operator!=(const NavigationNode &node) const {
	return node.point != point;
}

bool NavigationNode::addArc(const NavigationArc &arc) {
	if (!arc.contains(*this)) {
		error(NavigationError::InvalidArc);
		return false;
	}
	for (int i = 0; i < arcsCount; i++) {
		if (arcs[i] == &arc) {
			return true;
		}
	}
	if (arcsCount >= MAX_ARCS) {
		error(NavigationError::NeighboursOverflow);
		return false;
	}
	arcs[arcsCount++] = &arc;
	return true;
}

void NavigationNode::error(NavigationError code) const {
	extern Console console;
	console.sendKeyValue("navigation error", static_cast<int>(code));
}

const NavigationArc * const * NavigationNode::getArcs(int &count) const {
	count = arcsCount;
	return arcs;
}

int NavigationNode::getNeighbours(NavigationNode * (&results)[MAX_ARCS]) {
	int resultsCount = 0;
	for (int i = 0; i < arcsCount; i++) {
		const NavigationArc *arc = arcs[i];
		NavigationNode &node = arc->getOtherEnd(*this);
		results[resultsCount++] = &node;
	}
	return resultsCount;
}

void NavigationNode::clear() {
	distance = INT_MAX;
	visited = false;
	previous = NULL;
}

const Point& NavigationNode::getPoint() const {
	return point;
}
