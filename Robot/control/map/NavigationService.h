/*
 * NavigationService.h
 *
 *  Created on: 28 d�c. 2018
 *      Author: emmanuel
 */

#ifndef CONTROL_MAP_NAVIGATIONSERVICE_H_
#define CONTROL_MAP_NAVIGATIONSERVICE_H_

#include "NavigationMap.h"

/**
 * \brief Calcul d'itin�raire
 *
 * Impl�mentation du calcul d'itin�raire selon l'algorithme de Dijkstra.
 */
class NavigationService {
private:
	NavigationMap &map;
	bool useRotationCost = false;

	void clearNodes() const;
	NavigationNode& getNearestNode(const Point &point) const;
	NavigationNode& getNodeShortestDistance() const;
	int getRotationCost(const NavigationNode &origin, const NavigationNode &destination, const NavigationArc &arc) const;

public:
	/**
	 * Constructeur.
	 * @param [in] map la carte de navigation
	 */
	NavigationService(NavigationMap &map);
	/**
	 * \brief Calcul de l'itin�raire
	 *
	 * Calcule l'itin�raire entre le point source et le point destination fournis.
	 * L'appelant doit passer en param�tre un tableau suffisamment grand pour stocker l'itin�raire calcul�.
	 * La m�thode retourne le nombre de points que comporte l'itin�raire calcul� et plac�s dans le tableau.
	 * @param [in] source la coordonn�e d'origine
	 * @param [in] destination la coordonn�e de destination
	 * @param [in] path un tableau de type const Point* destin� au stockage de l'itin�raire calcul�
	 * @param [in] pathSize la taille du tableau
	 * @return le nombre de positions dans l'itin�raire calcul�
	 */
	unsigned int computePath(const Point &source, const Point &destination, const Point *path[], unsigned int pathSize) const;
	/**
	 * Renvoie la carte de navigation.
	 * @return la carte de navigation
	 */
	NavigationMap& getMap() const;
	/**
	 * \brief Co�ts de rotation
	 *
	 * D�finit si les co�ts de rotation doivent �tre pris en compte.
	 * @param [in] value la valeur bool�enne indiquant si les co�ts de rotation doivent �tre pris en compte
	 */
	void setUseRotationCost(bool value);
};

#endif /* CONTROL_MAP_NAVIGATIONSERVICE_H_ */
