/*
 * NavigationNode.h
 *
 *  Created on: 27 d�c. 2018
 *      Author: emmanuel
 */

#ifndef CONTROL_MAP_NAVIGATIONNODE_H_
#define CONTROL_MAP_NAVIGATIONNODE_H_

#include <cstddef>
#include "geometry/Point.h"
#include "NavigationError.h"

#define MAX_ARCS 5

class NavigationArc;

/**
 * \brief Noeud de navigation
 *
 * Objet repr�sentant un noeud dans le graphe de navigation.
 * Le noeud comprend une position et la liste des arcs menant aux noeuds voisins.
 * Le noeud stocke �galement les informations n�cessaires au calcul de distance.
 */
class NavigationNode {
private:
	friend class NavigationService;
	const Point &point;
	const NavigationArc *arcs[MAX_ARCS] = {NULL};
	int arcsCount = 0;

	int distance = 0;
	bool visited = false;
	NavigationNode *previous = NULL;
	const NavigationArc *arcFromPrevious = NULL;

	void error(NavigationError code) const;
	void clear();

public:
	/**
	 * Constructeur.
	 * @param [in] point la position de navigation repr�sent�e par ce noeud
	 */
	NavigationNode(const Point &point);
	/**
	 * Ajoute un arc vers un noeud voisin.
	 * Le noeud doit faire partie des extr�mit�s de l'arc.
	 * @param [in] arc l'arc � ajouter
	 * @return true si l'arc a �t� ajout�, false en cas d'erreur
	 */
	bool addArc(const NavigationArc &arc);
	/**
	 * \brief Liste de arcs
	 *
	 * Renvoie la liste des arcs. Le noeud doit avoir �t� ajout� � une carte pour
	 * que la liste des noeuds ait �t� remplie.
	 * @param [out] le nombre d'arcs dans la liste
	 * @return la liste des arcs de ce noeud
	 */
	const NavigationArc * const * getArcs(int &count) const;
	/**
	 * Renvoie la distance du point repr�sent� par ce noeud de navigation au point sp�cifi�.
	 * @param [in] point un point
	 * @return la distance du point de ce noeud au noeud sp�cifi�
	 */
	float getDistance(const Point &point) const;
	/**
	 * \brief Liste des noeuds voisins
	 *
	 * Renvoie les noeuds voisins de ce noeud, c'est � dire les noeuds situ�s �
	 * l'autre extr�mit� des arcs issus de ce noeud.
	 * @param [in] le tableau � remplir par les noeud voisins
	 * @return le nombre de neouds ajout�s au tableau
	 */
	int getNeighbours(NavigationNode * (&results)[MAX_ARCS]);
	/**
	 * \brief Position
	 *
	 * Renvoie la position repr�sent�e par ce noeud de navigation.
	 * @return la position du noeud
	 */
	const Point& getPoint() const;
	bool operator==(const NavigationNode &node) const;
	bool operator!=(const NavigationNode &node) const;
};

#endif /* CONTROL_MAP_NAVIGATIONNODE_H_ */
