/*
 * Odometry.h
 *
 *  Created on: 26 nov. 2014
 *      Author: Emmanuel
 */

#ifndef ODOMETRY_ODOMETRY_H_
#define ODOMETRY_ODOMETRY_H_

#include "Position.h"

/**
 * \brief Donn�es d'odom�trie
 *
 * Donn�es brutes de l'odom�trie.
 */
struct OdometryRawData {
	/** Distance parcourue par la roue gauche. */
	int leftDistance;
	/** Distance parcourue par la roue droite. */
	int rightDistance;
	/** Vitesse de d�placement rectiligne. */
	int moveSpeed;
	/** Vitesse de rotation. */
	int rotationSpeed;
};

/**
 * \brief Odom�trie
 *
 * Gestionnaire de l'odom�trie.
 */
class Odometry {
private:
	float wheelsDistance = 0;
	float scale = 1.0f;
	float angleOffset = 0;
	float getAngle();
	OdometryRawData raw = { .leftDistance = 0, .rightDistance = 0, .moveSpeed = 0, .rotationSpeed = 0 };
	Position position = { .x = 0, .y = 0, .angle = 0 };

public:
	/**
	 * Drapeau indiquant si le rep�re est invers� (indirect)
	 */
	bool reversed = false;

	/**
	 * Constructeur.
	 */
	Odometry();
	/**
	 * Convertit un angle de rotation sur place en incr�ments.
	 * @param [in] angle l'angle de rotation sur place en radians
	 * @return le nombre d'incr�ments correspondant � l'angle de rotation
	 */
	float angleToWheelDistance(float angle) const;
	/**
	 * Convertit une distance droite en incr�ments.
	 * @param [in] distance la distance droite en millim�tres
	 * @return le nombre d'incr�ments correspondant � la distance droite
	 */
	float distanceToWheelDistance(float distance) const;
	/**
	 * \brief Position
	 *
	 * Renvoie la position courante.
	 * @return la position courante
	 */
	const Position& getPosition() const;
	/**
	 * \brief Donn�es brutes d'odom�trie
	 *
	 * Renvoie les donn�es brutes d'odom�trie.
	 * @return les donn�es brutes d'odom�trie
	 */
	const OdometryRawData& getRawData() const;
	/**
	 * \brief Demi-distance entre les roues
	 *
	 * Renvoie la demi-distance entre les roues en millim�tres.
	 * @return la demi-distance entre les roues (mm)
	 */
	int getWheelsSemiDistance() const;
	/**
	 * \brief Configuration
	 *
	 * D�finit les param�tres de configuration.
	 * @param [in] wheelsDistance la distance entre les roues (mm)
	 * @param [in] millimetersPerIncrement le nombre de millim�tres par incr�ment de codeur
	 */
	void setConfiguration(float wheelsDistance, float millimetersPerIncrement);
	/**
	 * \brief Position
	 *
	 * D�finit la position courante.
	 * @param [in] newPosition la nouvelle valeur de la position
	 */
	void setPosition(const Position &newPosition);
	/**
	 * \brief Mise � jour
	 *
	 * Met � jour la position courante � partir des derni�res mesures issues des codeurs.
	 * @param [in] leftIncrement le nombre d'incr�ments de la roue gauche
	 * @param [in] rightIncrement le nombre d'incr�ments de la roue droite
	 */
	void update(int leftIncrement, int rightIncrement);
	/**
	 * Convertit les incr�ments en angle de rotation sur place.
	 * @param [in] increments le nombre d'incr�ments
	 * @return la valeur de l'angle de rotation sur place en radians
	 */
	float wheelDistanceToAngle(int increments) const;
};

#endif /* ODOMETRY_ODOMETRY_H_ */
