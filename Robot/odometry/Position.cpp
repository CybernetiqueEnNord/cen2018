/*
 * Position.cpp
 *
 *  Created on: 12 d�c. 2014
 *      Author: Emmanuel
 */

#include "Position.h"

#include "math/cenMath.h"

#include <math.h>

float Position::getOrientedDistance(const Point &p) const {
	float angle, distance;
	getAngleAndDistance(p, angle, distance);
	if (angle > M_PI_2 || angle < -M_PI_2) {
		distance = -distance;
	}
	return distance;
}

void Position::getAngleAndDistance(const Point &p, float &angle, float &distance) const {
	float dx = p.x - x;
	float dy = p.y - y;
	angle = atan2f(dy, dx);
	angle = diffAngle(this->angle, angle);
	distance = sqrtf(dx * dx + dy * dy);
}

Point Position::getPoint(float angle, float distance) const {
	angle += this->angle;
	float x = this->x + cosf(angle) * distance;
	float y = this->y + sinf(angle) * distance;
	return Point(x, y);
}
