/*
 * Position2.h
 *
 *  Created on: 12 d�c. 2014
 *      Author: Emmanuel
 */

#ifndef ODOMETRY_POSITION_H_
#define ODOMETRY_POSITION_H_

#include "geometry/Point.h"

/**
 * \brief Coordonn�es orient�es.
 *
 * Structure d�finissant les coordonn�es d'un point dans un plan et d'un angle d'orientation.
 */
struct Position : Point {
	/** Angle d'orientation (radians). */
	float angle = 0;
	/** Constructeur. */
	Position() {};
	/**
	 * Constructeur.
	 * @param [in] x coordonn�e sur l'axe X (mm)
	 * @param [in] y coordonn�e sur l'axe Y (mm)
	 * @param [in] angle angle d'orientation (radians)
	 */
	Position(float x, float y, float angle) : Point(x, y), angle(angle) {};
	/**
	 * Renvoie l'angle et la distance de cette position � la position sp�cifi�e.
	 * @param [in] p la position cible
	 * @param [out] angle l'angle orient� entre la position courante et la destination (radians)
	 * @param [out] distance la distance au point de destination (mm)
	 */
	void getAngleAndDistance(const Point &p, float &angle, float &distance) const;
	/**
	 * Renvoie le point situ� relativement � la position repr�sent�e par le receveur et les
	 * param�tres angle relatif et distance sp�cifi�s.
	 * @param [in] angle l'angle relatif (radians)
	 * @param [in] distance la distance (mm)
	 * @return le point correspondant aux param�tres sp�cifi�s
	 */
	Point getPoint(float angle, float distance) const;
	/**
	 * Renvoie la distance orient�e de cette position � la position sp�cifi�e.
	 * @param [in] p la position cible
	 */
	float getOrientedDistance(const Point &p) const;
};

#endif /* ODOMETRY_POSITION_H_ */
