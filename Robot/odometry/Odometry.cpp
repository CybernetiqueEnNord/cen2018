/*
 * Odometry.cpp
 *
 *  Created on: 26 nov. 2014
 *      Author: Emmanuel
 */

#include "Odometry.h"

#include <math.h>

#include "math/cenMath.h"

#define ZERO 1e-7f

Odometry::Odometry() {
}

void Odometry::update(int leftIncrement, int rightIncrement) {
	// wheels distances
	raw.leftDistance += leftIncrement;
	raw.rightDistance += rightIncrement;
	int distanceIncrement = leftIncrement + rightIncrement;
	raw.moveSpeed = distanceIncrement / 2;
	raw.rotationSpeed = (rightIncrement - leftIncrement) / 2;

	// orientation
	float oldAngle = position.angle;
	float angle = getAngle() + angleOffset;
	float angleIncrement = angle - oldAngle;

	// straight distance from arc
	float straightDistance;
	if (fabsf(angleIncrement) < ZERO) {
		straightDistance = 0.5f * distanceIncrement;
	} else {
		float arcRadius = (float) distanceIncrement / angleIncrement;
		straightDistance = arcRadius * sinf(angleIncrement / 2);
	}

	// projection of distance following the mean angle of the move
	float meanAngle = (angle + oldAngle) / 2;
	float dx = straightDistance * cosf(meanAngle);
	float dy = straightDistance * sinf(meanAngle);

	// new position
	position.x += dx * scale;
	position.y += dy * scale;
	position.angle = angle;
}

const Position& Odometry::getPosition() const {
	return position;
}

const OdometryRawData& Odometry::getRawData() const {
	return raw;
}

float Odometry::angleToWheelDistance(float angle) const {
	return angle * this->wheelsDistance / 2;
}

float Odometry::wheelDistanceToAngle(int increments) const {
	return (float) increments * 2 / this->wheelsDistance;
}

float Odometry::distanceToWheelDistance(float distance) const {
	return distance / scale;
}

int Odometry::getWheelsSemiDistance() const {
	return (int) (wheelsDistance / 2);
}

float Odometry::getAngle() {
	int orientation = raw.rightDistance - raw.leftDistance;
	if (reversed) {
		orientation = -orientation;
	}
	float angle = fmodf((float) orientation / wheelsDistance, M_2PI);
	return angle;
}

void Odometry::setPosition(const Position &newPosition) {
	float angle = getAngle();
	angleOffset = normalizeAngle(newPosition.angle - angle);
	position = newPosition;
}

void Odometry::setConfiguration(float wheelsDistance, float millimetersPerIncrement) {
	this->wheelsDistance = wheelsDistance / millimetersPerIncrement;
	this->scale = millimetersPerIncrement;
}
