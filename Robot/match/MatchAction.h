/*
 * MatchAction.h
 *
 *  Created on: 23 janv. 2019
 *      Author: Emmanuel
 */

#ifndef MATCHACTION_H
#define MATCHACTION_H

#include "control/Navigation.h"
#include "geometry/Point.h"

#define STEP_FINALIZATION 9999

class MatchAction {
private:
	const char *label;
	const Point start;
	bool aborted = false;
	bool failed = false;

	void finalize(Navigation &navigation);

protected:
	bool terminated = false;

public:
	static MatchAction &none;

	MatchAction(const char* label);
	MatchAction(const char* label, const Point &start);
	void abort();
	void execute(Navigation &navigation);
	virtual void executeStep(Navigation &navigation, unsigned int step);
	const char* getLabel() const;
	const Point& getStart() const;
	bool hasFailed() const;
	bool isAborted() const;
	bool isTerminated() const;
	void reset();
};

#endif /* MATCHACTION_H */
