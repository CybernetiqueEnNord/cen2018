/*
 * MatchChoice.cpp
 *
 *  Created on: 21 d�c. 2019
 *      Author: anastaszor
 */

#include "MatchChoice.h"

MatchChoice::MatchChoice(Navigation &navigation, MatchData &matchData) :
		executor(navigation), navigation(navigation), matchData(matchData) {
	name = "UNKNOWN";
}

const char* MatchChoice::getName() const {
	return name;
}

void MatchChoice::prepare() {
	// must be overridden
}

void MatchChoice::execute() {
	// must be overridden
}

void MatchChoice::finish() {
	// must be overridden
}

void MatchChoice::abort() {
	executor.stop();
}
