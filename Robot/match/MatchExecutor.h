/*
 * MatchExecutor.h
 *
 *  Created on: 18 f�vr. 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_MATCHEXECUTOR_H_
#define MATCH_MATCHEXECUTOR_H_

#include "control/Navigation.h"
#include "match/MatchTaskList.h"
#include "control/map/NavigationService.h"
#include "utils/ConstPointer.h"
#include "utils/Pointer.h"

// dur�e (secondes) temporaire d'indisponibilit� d'une t�che apr�s un �chec
#define TASK_UNAVAILABILITY 15

enum class MatchExecutorResult {
	ActionDone,
	ActionNotReached,
	ActionFailed,
	ActionAborted,
	Aborted,
	Finished
};

class MatchExecutor {
private:
	Navigation &navigation;
	ConstPointer<MatchTaskList> tasks;
	bool started = false;
	bool aborted = false;
	bool finished = false;
	Pointer<MatchAction> currentAction;
	Pointer<MatchTask> currentTask;
	Pointer<MatchTarget> currentTarget;
	unsigned int obstacleTimeout;
	Pointer<NavigationService> navigationService;

	void checkCompletableTasks();
	void clearCurrentTarget(bool unlockTarget);
	void displayTarget(MatchAction &action) const;
	MatchExecutorResult executeNext();
	MatchAction& findNearestAction() const;
	Pointer<MatchAction> findNextAction();
	Pointer<MatchTask> findNextTask();
	float getTaskCost(int expectedScore, int expectedCost) const;
	void handleMoveToActionFailure();
	void handleObstacle();
	bool moveToAction(MatchAction &action);
	bool navigateToAction(const Point &point);
	void setCurrentTarget(MatchTarget& target);
	void updateMapCosts();

public:
	MatchExecutor(Navigation &navigation, unsigned int obstacleTimeout = 5000);
	bool isAborted() const;
	bool isFinished() const;
	void run();
	void setNavigationService(NavigationService &service);
	/**
	 * \brief D�lai d'attente sur obstacle
	 *
	 * D�finit le temps maximal d'attente sur un obstacle avant d'abandonner la trajectoire courante.
	 * @param [in] value le d�lai d'attente en millisecondes (ms)
	 */
	void setObstacleTimeout(unsigned int value);
	void setTasksList(MatchTaskList &tasks);
	void start();
	void stop();
};

#endif /* MATCH_MATCHEXECUTOR_H_ */
