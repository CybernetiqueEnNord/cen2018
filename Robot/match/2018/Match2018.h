/*
 * Match2018.h
 *
 *  Created on: 6 mai 2018
 *      Author: Emmanuel
 */

#ifndef MATCH_MATCH2018_H_
#define MATCH_MATCH2018_H_

#include "match/Match.h"

#define TRAJECTORY_HOMOLOGATION 0
#define TRAJECTORY_MATCH_1 1
#define TRAJECTORY_MATCH_6 6
#define TRAJECTORY_TEST 7

class Match2018 : public Match, public NavigationListener {
private:
	void changeMatchSide(bool newSide);
	void changeTrajectory(int trajectoryIndex);
	void displayMatchSide();
	void displayTrajectory();
	void updateMatchSide();
	void updateTrajectory();

	void executeBigHomologation();
	void executeSmallHomologation();
	void prepareBigHomologation();
	void prepareSmallHomologation();

	void executeBigMatch1();
	void executeSmallMatch1();
	void prepareBigMatch1();
	void prepareSmallMatch1();

	void executeSmallMatch2();
	void prepareSmallMatch2();

	void executeBigMatch6();
	void prepareBigMatch6();

	void executeBigTest();
	void executeSmallTest();
	void prepareBigTest();
	void prepareSmallTest();

	void executeBigLoadCubes();

protected:
	bool hasCapability(unsigned int capability) const;

	virtual void executeBig();
	virtual void executeSmall();
	virtual void prepareBig();
	virtual void prepareSmall();

	virtual void execute();
	virtual void initialize();
	virtual void prepare();
	virtual void onNavigationEvent(const Navigation &navigation);

public:
	int trajectoryIndex = TRAJECTORY_HOMOLOGATION;
	bool trajectorySelectable = true;

	Match2018(Navigation &navigation);
	virtual void terminate();
};

#endif /* MATCH_MATCH2018_H_ */
