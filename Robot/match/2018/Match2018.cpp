/*
 * Match2018.cpp
 *
 *  Created on: 6 mai 2018
 *      Author: Emmanuel
 */

#include "devices/actuators/2018/cubes/CubesActuator.h"
#include "devices/actuators/2018/water/WaterActuator.h"
#include "devices/beacon/Beacon2018.h"
#include "devices/host/HostCapabilities2018.h"
#include "devices/host/HostDevice.h"
#include "devices/host/HostDataF446REBig.h"
#include "devices/host/HostDataF446RESmall.h"
#include "devices/io/LCDScreen.h"
#include "match/2018/Match2018.h"
#include "math/cenMath.h"
#include "utils/Text.h"

Match2018::Match2018(Navigation &navigation) :
		Match(navigation) {
}

void Match2018::execute() {
	extern LCDScreen screen;
	screen.displayMessage("C'est parti !");

	if (HostDevice::getDeviceId() == HostData_F446RE_Small::getDeviceId()) {
		executeSmall();
	} else if (HostDevice::getDeviceId() == HostData_F446RE_Big::getDeviceId()) {
		executeBig();
	}

	setActionResult(ActionResult::Next);
}

void Match2018::prepare() {
	extern Beacon2018 beacon;
	beacon.setLedBlinking(100, 100, 0);

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = false;

	displayMatchSide();
	displayTrajectory();

	if (HostDevice::getDeviceId() == HostData_F446RE_Small::getDeviceId()) {
		prepareSmall();
	} else if (HostDevice::getDeviceId() == HostData_F446RE_Big::getDeviceId()) {
		prepareBig();
	}

	navigation.waitForStart(this);
}

void Match2018::initialize() {
	Match::initialize();

	displayMatchSide();
	displayTrajectory();

	extern Beacon2018 beacon;
	beacon.setLedBlinking(100, 400, 0);

	const Beacon2018Data &data = beacon.getData();
	while (data.started) {
		beacon.updateHMIData();
		updateMatchSide();
		if (trajectorySelectable) {
			updateTrajectory();
		}
		chThdSleep(50);
	}
}

void Match2018::onNavigationEvent(const Navigation &navigation) {
	(void) navigation;
}

void Match2018::terminate() {
	Match::terminate();

	if (hasCapability(CAPABILITY_CUBES)) {
		extern CubesActuator cubesActuator;
		cubesActuator.kill();
	}

	if (hasCapability(CAPABILITY_WATER)) {
		extern WaterActuator waterActuator;
		waterActuator.kill();
	}

	const PositionControl &control = navigation.getControl();
	MoveError error = control.getError();

	extern LCDScreen screen;
	if (error == MoveError::Overrun) {
		screen.displayMessage("      OVERRUN");
	} else {
		screen.displayMessage("   MATCH TERMINE");
	}
}

void Match2018::displayMatchSide() {
	bool side = navigation.getControl().isReversed();
	extern LCDScreen screen;
	screen.displayMessage(side ? "Orange" : "Vert");
}

void Match2018::displayTrajectory() {
	extern LCDScreen screen;
	char buffer[9];
	itoa(trajectoryIndex, buffer, 10);
	screen.displayMessage(buffer, 15);
}

void Match2018::changeMatchSide(bool newSide) {
	if (hasCapability(CAPABILITY_WATER)) {
		extern WaterActuator waterActuator;
		waterActuator.setMatchSide(newSide);
	}

	PositionControl &control = navigation.getControl();
	control.setReversed(newSide);
	displayMatchSide();
}

void Match2018::changeTrajectory(int trajectoryIndex) {
	this->trajectoryIndex = trajectoryIndex;
	displayTrajectory();
}

void Match2018::updateMatchSide() {
	extern Beacon2018 beacon;
	const Beacon2018Data &data = beacon.getData();
	bool side = data.hmiValue1 < 128;
	PositionControl &control = navigation.getControl();
	if (side != control.isReversed()) {
		changeMatchSide(side);
	}
}

void Match2018::updateTrajectory() {
	extern Beacon2018 beacon;
	const Beacon2018Data &data = beacon.getData();
	int index = data.hmiValue2 / 36;
	if (index != trajectoryIndex) {
		changeTrajectory(index);
	}
}

bool Match2018::hasCapability(unsigned int capability) const {
	const HostData &data = HostDevice::getHostData();
	unsigned int capabilities = data.getCapabilities();
	bool result = (capabilities & capability) != 0;
	return result;
}

void Match2018::executeBig() {
	switch (trajectoryIndex) {
		case TRAJECTORY_MATCH_1:
		case 2:
		case 3:
		case 4:
			executeBigMatch1();
			break;

		case TRAJECTORY_MATCH_6:
			executeBigMatch6();
			break;

		case TRAJECTORY_TEST:
			executeBigTest();
			break;

		default:
			executeBigHomologation();
			break;
	}
}

void Match2018::executeSmall() {
	switch (trajectoryIndex) {
		case TRAJECTORY_MATCH_1:
			executeSmallMatch1();
			break;

		case TRAJECTORY_TEST:
			executeSmallTest();
			break;

		default:
			executeSmallHomologation();
			break;
	}
}

void Match2018::prepareBig() {
	switch (trajectoryIndex) {
		case TRAJECTORY_MATCH_1:
		case 2:
		case 3:
		case 4:
			prepareBigMatch1();
			break;

		case TRAJECTORY_MATCH_6:
			prepareBigMatch6();
			break;

		case TRAJECTORY_TEST:
			prepareBigTest();
			break;

		default:
			prepareBigHomologation();
			break;
	}
}

void Match2018::prepareSmall() {
	// c�t�
	PositionControl &control = navigation.getControl();
	bool side = control.isReversed();

	extern WaterActuator waterActuator;
	waterActuator.setMatchSide(side);

	switch (trajectoryIndex) {
		case TRAJECTORY_MATCH_1:
			prepareSmallMatch1();
			break;

		case TRAJECTORY_TEST:
			prepareSmallTest();
			break;

		default:
			prepareSmallHomologation();
			break;
	}
}

// Homologation Gros

void Match2018::prepareBigHomologation() {
	navigation.moveMM(70);

	extern CubesActuator cubesActuator;
	cubesActuator.setInitialPosition();
	cubesActuator.waitForReady(20000);
}

void Match2018::executeBigHomologation() {
	MatchData &data = getMatchData();

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	control.setPosition(210, 128 + 70, ANGLE_TO_RADIANS(90));
	control.setSpeedIndex(1);
	data.addScore(5);

	navigation.waitForDelay(500);

	// Interrupteur
	navigation.moveTo(Point(210, 1130));
	navigation.rotateDeciDegrees(-900);
	navigation.moveMM(-50);
	data.addScore(25);
}

// Homologation Petit

void Match2018::prepareSmallHomologation() {
	PositionControl &control = navigation.getControl();
	control.setPosition(540, 115, ANGLE_TO_RADIANS(90));

	navigation.moveMM(30);
	navigation.moveTo(Point(500, 250));
	navigation.rotateDeciDegrees(-877);
}

void Match2018::executeSmallHomologation() {
	MatchData &data = getMatchData();

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	control.setSpeedIndex(2);
	data.addScore(5);

	navigation.waitForDelay(500);

	extern WaterActuator waterActuator;

	// Distributeur d'eau propre
	control.setSpeedIndex(2);
	navigation.moveTo(Point(810, 280));
	control.setSpeedIndex(1);
	navigation.rotateToOrientationDeciDegrees(0);
	navigation.moveMM(-140);
	waterActuator.handleOpenDispenser();
	navigation.waitForDelay(2000);
	// ouverture
	data.addScore(10);
	navigation.moveMM(150);

	// Abeille
	/*
	 navigation.moveTo(Point(1200, 550));
	 waterActuator.handleBee();
	 navigation.moveTo(Point(1820, 120));
	 control.setSpeedIndex(1);
	 navigation.moveTo(Point(1820, 170));
	 navigation.rotateDeciDegrees(4560);
	 data.addScore(50);
	 */
}

// Match 1 Gros

void Match2018::prepareBigMatch1() {
	navigation.waitForDelay(500);

	extern CubesActuator cubesActuator;
	PositionControl &control = navigation.getControl();
	control.setPosition(190, 128, ANGLE_TO_RADIANS(90));
	bool side = control.isReversed();

	navigation.moveMM(70);
	navigation.rotateDeciDegrees(-105);

	cubesActuator.setMatchSide(side);
	cubesActuator.setInitialPosition();
	cubesActuator.waitForReady(20000);

	navigation.waitForDelay(2000);

	cubesActuator.handleLoadPrepare();
	cubesActuator.waitForReady(20000);

	navigation.waitForDelay(3000);

	cubesActuator.handleLoadJocker();
	cubesActuator.waitForReady(20000);
}

void Match2018::executeBigMatch1() {
	extern Beacon2018 beacon;
	MatchData &data = getMatchData();
	beacon.setBeepEnabled(false);

	extern CubesActuator cubesActuator;

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	data.addScore(5);

	cubesActuator.handleUnLoadJocker();
	navigation.waitForDelay(500);

	// D�pose jokers
	control.setSpeedIndex(2);
	navigation.moveTo(Point(255, 550));
	control.setSpeedIndex(1);
	navigation.moveTo(Point(250, 550));
	cubesActuator.waitForReady(20000);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(20000);
	data.addScore(3);
	navigation.moveMM(-80);
	cubesActuator.setInitialPosition();

	// Interrupteur
	control.setSpeedIndex(2);
	navigation.moveTo(Point(210, 1130));
	control.setSpeedIndex(1);
	navigation.rotateToOrientationDeciDegrees(-900);
	navigation.moveMM(-50);
	data.addScore(25);
	navigation.moveMM(65);

	// Cam�ra
	/*
	 navigation.moveTo(Point(250, 1500));
	 navigation.rotateToOrientationDeciDegrees(-900);
	 navigation.waitForDelay(3000);
	 */

	// � c�t� du cube noir
	navigation.moveTo(Point(225, 850));
	navigation.moveTo(Point(540 - 190 - 90, 850));

	executeBigLoadCubes();

	// se d�placer jusqu'� proximit� de la tour
	navigation.moveTo(Point(250, 850));
	cubesActuator.waitForReady(20000);

	// D�pose cubes
	cubesActuator.setUnloadCombination(trajectoryIndex);
	cubesActuator.waitForReady(20000);
	cubesActuator.handlePrepareUnLoad1();
	cubesActuator.waitForReady(20000);

	cubesActuator.handleOpen();
	cubesActuator.waitForReady(500);
	data.addScore(1);

	navigation.moveMM(-80);
	cubesActuator.handlePrepareUnLoad2();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(80);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(500);
	data.addScore(2);

	navigation.moveMM(-80);
	cubesActuator.handlePrepareUnLoad3();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(80);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(500);
	data.addScore(3);

	navigation.moveMM(-80);
	cubesActuator.handlePrepareUnLoad4();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(80);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(500);
	data.addScore(4);

	navigation.moveMM(-80);
	cubesActuator.handlePrepareUnLoad5();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(80);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(500);

	navigation.moveMM(-80);

	control.setSpeedIndex(3);
	cubesActuator.handlePush();
	navigation.moveTo(Point(1600, 600));
	navigation.moveTo(Point(1500, 230));
	navigation.moveTo(Point(250, 500));
	control.setSpeedIndex(1);
	navigation.moveMM(-100);
	data.addScore(4);
}

// Match 1 Petit

void Match2018::prepareSmallMatch1() {
	PositionControl &control = navigation.getControl();
	bool side = control.isReversed();
	float angle = 90.0 + (side ? -2.0 : 2.0);
	control.setPosition(540, 115, ANGLE_TO_RADIANS(angle));

	navigation.moveMM(30);
	navigation.moveTo(Point(500, 250));
	navigation.rotateDeciDegrees(-877);
}

void Match2018::executeSmallMatch1() {
	MatchData &data = getMatchData();

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	control.setSpeedIndex(2);
	data.addScore(5);

	bool side = control.isReversed();

	navigation.waitForDelay(500);

	extern WaterActuator waterActuator;

	// Abeille
	navigation.moveTo(Point(1200, 550));
	waterActuator.handleBee();
	navigation.moveTo(Point(1820, 150));
	control.setSpeedIndex(1);
	navigation.rotateDeciDegrees(1800);
	data.addScore(50);
	//navigation.moveTo(Point(1820, 170));
	//navigation.rotateDeciDegrees(4560);

	// Distributeur d'eau propre
	control.setSpeedIndex(2);
	navigation.moveTo(Point(1250, 150));
	navigation.moveTo(Point(1100, 150));
	navigation.moveTo(Point(side ? 850 : 820, 280));
	control.setSpeedIndex(1);
	navigation.rotateToOrientationDeciDegrees(0);
	navigation.moveMM(side ? -155 : -70);
	navigation.rotateDeciDegrees(-30);
	waterActuator.handleOpenDispenser();
	navigation.waitForDelay(2000);
	// ouverture
	data.addScore(10);
	// premi�re it�ration
	navigation.moveMM(-20);
	waterActuator.handleCleanWaterDispenser();
	waterActuator.waitForReady(25000);
	// seconde it�ration
	navigation.moveMM(10);
	waterActuator.handleCleanWaterDispenser();
	waterActuator.waitForReady(25000);
	// balles
	data.addScore(35);
	waterActuator.handleOpenDispenser();
	navigation.moveMM(180);
	waterActuator.waitForReady(1000);
	waterActuator.handleBee();
	navigation.rotateDeciDegrees(3600);

	// Distributeur d'eau sale adverse
	control.setSpeedIndex(2);
	navigation.moveTo(Point(1190, 600));
	navigation.moveTo(Point(1190, 2370));
	if (side) {
		waterActuator.handleBee();
		control.setSpeedIndex(1);
		control.obstacleAvoidance = false;
		navigation.moveTo(Point(1520, 2320));
		navigation.rotateDeciDegrees(450);
		if (waterActuator.isReady()) {
			waterActuator.handleBee();
		}
		navigation.moveMM(50);
		navigation.rotateDeciDegrees(-450);
		if (waterActuator.isReady()) {
			waterActuator.handleBee();
		}
		navigation.moveMM(50);
		navigation.rotateDeciDegrees(450);
		if (waterActuator.isReady()) {
			waterActuator.handleBee();
		}
		navigation.moveMM(50);
		navigation.rotateDeciDegrees(-450);
		//data.addScore(10);
	} else {
		waterActuator.waitForReady(10000);
		waterActuator.setMatchSide(side);
		navigation.moveTo(Point(1720, 2370));
		control.setSpeedIndex(1);
		navigation.rotateDeciDegrees(1800);
		navigation.moveMM(-170);
		waterActuator.handleOpenDispenser();
		navigation.waitForDelay(2000);
		// ouverture
		//data.addScore(10);
		navigation.moveMM(50);
		waterActuator.handleBee();
		navigation.rotateDeciDegrees(3600);
	}

	// Interrupteur adverse
	/*
	 control.setSpeedIndex(2);
	 navigation.moveTo(Point(1190, 2390));
	 navigation.moveTo(Point(840, 1870));
	 navigation.moveTo(Point(210, 1870));
	 control.setSpeedIndex(1);
	 navigation.rotateDeciDegrees(1800);
	 navigation.moveMM(-60);

	 navigation.moveMM(100);
	 control.setSpeedIndex(2);
	 navigation.moveTo(Point(250, 2400));
	 navigation.moveTo(Point(620, 2400));
	 navigation.moveTo(Point(130, 1100));
	 */
}

// Match 2 Gros

void Match2018::prepareBigMatch6() {
	prepareBigMatch1();
}

void Match2018::executeBigMatch6() {
	extern Beacon2018 beacon;
	MatchData &data = getMatchData();
	beacon.setBeepEnabled(false);

	extern CubesActuator cubesActuator;

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	control.setPosition(210, 128 + 70, ANGLE_TO_RADIANS(90));
	control.setSpeedIndex(1);
	data.addScore(5);

	navigation.waitForDelay(500);

	// Interrupteur
	navigation.moveTo(Point(210, 1130));
	navigation.rotateDeciDegrees(-900);
	navigation.moveMM(-50);
	data.addScore(25);

	// Poussage du tas de cubes
	navigation.moveTo(Point(950, 1130));
	navigation.moveTo(Point(950, 850));
	navigation.moveTo(Point(330, 850));

	// D�pose jokers
	cubesActuator.handleUnLoadJockerLevel2();
	cubesActuator.waitForReady(20000);
	data.addScore(10);
	navigation.moveMM(-80);
}

// Test Petit

void Match2018::prepareSmallTest() {
}

void Match2018::executeSmallTest() {
	PositionControl &control = navigation.getControl();
	control.setSpeedIndex(1);
	navigation.moveMM(2000);
}

// Test Gros

void Match2018::prepareBigTest() {
}

void Match2018::executeBigTest() {
	PositionControl &control = navigation.getControl();
	control.setSpeedIndex(1);
	executeBigLoadCubes();
}

// Gros : prise des cubes en d�placements relatifs

void Match2018::executeBigLoadCubes() {
	extern CubesActuator cubesActuator;

	// Chargement cubes
	//chargement cube 1 :noir
	cubesActuator.handleLoadPrepare();
	cubesActuator.waitForReady(20000);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(2000);
	navigation.moveMM(30);
	cubesActuator.handleClose();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(-50);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(45);
	cubesActuator.handleLoad1();
	cubesActuator.waitForReady(20000);

	// chargement cube 2 : jaune
	navigation.moveMM(80);
	navigation.moveMM(-10);
	cubesActuator.handleClose();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(-80);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(50);
	cubesActuator.handleLoad2();
	navigation.waitForDelay(1000);
	navigation.rotateDeciDegrees(120);
	cubesActuator.waitForReady(20000);

	// chargement dube 3 : vert
	navigation.moveMM(90);
	navigation.moveMM(-10);
	cubesActuator.handleClose();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(-120);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(75);
	cubesActuator.handleLoad3();
	navigation.waitForDelay(500);
	navigation.moveMM(-90 + 10 + 120 - 75);
	navigation.rotateDeciDegrees(-240);
	cubesActuator.waitForReady(20000);

	// chargement cube 4 : orange
	navigation.moveMM(120);
	navigation.moveMM(-10);
	cubesActuator.handleClose();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(-150);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(75);
	cubesActuator.handleLoad4();
	navigation.waitForDelay(500);
	navigation.moveMM(-120 + 10 - 20 + 10 + 150 - 75);
	navigation.rotateDeciDegrees(120);
	navigation.moveMM(60);
	cubesActuator.waitForReady(20000);

	// chargement cube 5 : bleu
	navigation.moveMM(240);
	cubesActuator.handleLoad5();
	cubesActuator.waitForReady(1000);
}
