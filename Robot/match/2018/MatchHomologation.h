/*
 * MatchHomologation.h
 *
 *  Created on: 17 f�vr. 2018
 *      Author: Emmanuel
 */

#ifndef MATCH_MATCHHOMOLOGATION_H_
#define MATCH_MATCHHOMOLOGATION_H_

#include "match/Match2018.h"

class MatchHomologation : public Match2018 {
protected:
	virtual void executeBig();
	virtual void executeSmall();
	virtual void prepareBig();
	virtual void prepareSmall();

public:
	MatchHomologation(Navigation &navigation);
};

#endif /* MATCH_MATCHHOMOLOGATION_H_ */
