/*
 * MatchHomologation.h
 *
 *  Created on: 17 f�vr. 2018
 *      Author: Emmanuel
 */

#ifndef MATCH_MATCHV1_H_
#define MATCH_MATCHV1_H_

#include "match/Match.h"

class MatchV1 : public Match, public NavigationListener {
private:
	void changeMatchSide(bool newSide);
	void displayMatchSide();
	void executeBig();
	void executeSmall();
	void prepareBig();
	void prepareSmall();
	void updateMatchSide();

protected:
	virtual void prepare();
	virtual void execute();
	virtual void onNavigationEvent(const Navigation &navigation);

public:
	MatchV1(Navigation &navigation);
};

#endif /* MATCH_MATCHHOMOLOGATION_H_ */
