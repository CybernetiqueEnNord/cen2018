/*
 * MatchHomologation.cpp
 *
 *  Created on: 17 f�vr. 2018
 *      Author: Emmanuel
 */

#include "ch.h"

#include "devices/actuators/cubes/CubesActuator.h"
#include "devices/actuators/water/WaterActuator.h"
#include "devices/beacon/Beacon2018.h"
#include "devices/host/HostDevice.h"
#include "devices/host/HostDataF446REBig.h"
#include "devices/host/HostDataF446RESmall.h"
#include "devices/io/LCDScreen.h"
#include "match/MatchHomologation.h"
#include "math/cenMath.h"

MatchHomologation::MatchHomologation(Navigation &navigation) :
		Match2018(navigation) {
}

void MatchHomologation::executeBig() {
	extern Beacon2018 beacon;
	MatchData &data = getMatchData();
	beacon.setBeepEnabled(false);

	extern CubesActuator cubesActuator;

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	control.setPosition(210, 140 + 70, ANGLE_TO_RADIANS(90));
	control.setSpeedIndex(1);
	data.addScore(5);

	navigation.waitForDelay(500);

	// Interrupteur
//	cubesActuator.handleUnLoadJocker();
//
//	navigation.waitForDelay(8000);

	cubesActuator.handleLoadPrepare();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(30);
	cubesActuator.handleClose();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(-50);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(45);
	cubesActuator.handleLoad1();
	cubesActuator.waitForReady(20000);

	navigation.moveMM(60);
	navigation.moveMM(-10);
	cubesActuator.handleClose();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(-60);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(50);
	cubesActuator.handleLoad2();
	navigation.rotateDeciDegrees(100);
	cubesActuator.waitForReady(20000);


	// Cubes
	navigation.moveMM(60);
	navigation.moveMM(-10);
//	navigation.rotateDeciDegrees(20);
//	navigation.moveMM(20);
	cubesActuator.handleClose();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(-90);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(75);
	cubesActuator.handleLoad3();
	navigation.moveMM(-60+10+90-75);
	navigation.rotateDeciDegrees(-200);
	cubesActuator.waitForReady(20000);

	navigation.moveMM(70);
	navigation.moveMM(-10);
//	navigation.rotateDeciDegrees(-20);
//	navigation.moveMM(20);
//	navigation.moveMM(-10);
	cubesActuator.handleClose();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(-100);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(75);
	cubesActuator.handleLoad4();
	navigation.moveMM(-70+10-20+10+100-75);
	navigation.rotateDeciDegrees(100);
	navigation.moveMM(60);
	cubesActuator.waitForReady(20000);

	navigation.moveMM(120);
	cubesActuator.handleLoad5();
	cubesActuator.waitForReady(20000);


	// Dépose cubes
	navigation.waitForDelay(3000);

	cubesActuator.handleResetTower();
	cubesActuator.waitForReady(20000);

	cubesActuator.handlePrepareUnLoad1();
	// se déplacer jusqu'à proximité de la tour
	cubesActuator.waitForReady(20000);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(500);
	navigation.moveMM(-80);
	cubesActuator.handlePrepareUnLoad2();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(80);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(500);

	navigation.moveMM(-80);
	cubesActuator.handlePrepareUnLoad3();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(80);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(500);

	navigation.moveMM(-80);
	cubesActuator.handlePrepareUnLoad4();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(80);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(500);

	navigation.moveMM(-80);
	cubesActuator.handlePrepareUnLoad5();
	cubesActuator.waitForReady(20000);
	navigation.moveMM(80);
	cubesActuator.handleOpen();
	cubesActuator.waitForReady(500);

	navigation.moveMM(-80);


//	navigation.moveTo(Point(210, 1130));
//	navigation.rotateDeciDegrees(-900);
//	navigation.moveMM(-60);
//	data.addScore(25);
//
//	navigation.moveMM(60);
//	cubesActuator.handleLoadPrepare();
//	navigation.moveTo(Point(210, 850));
//	navigation.moveTo(Point(350, 850));
//	cubesActuator.handleLoadLv1();
//	cubesActuator.waitForReady(1000);
//	navigation.moveMM(-20);
//	navigation.rotateDeciDegrees(1800);
//	navigation.moveMM(60);
//	cubesActuator.handleUnloadAsIs();
//	cubesActuator.waitForReady(1000);
//
//	navigation.moveMM(-60);
//	navigation.rotateDeciDegrees(1800);
//	cubesActuator.handleLoadPrepare();
//	cubesActuator.waitForReady(2000);
//	navigation.moveMM(100);
//	cubesActuator.handleLoadLv2();
//	cubesActuator.waitForReady(1000);
//	navigation.moveMM(-100);
//	navigation.rotateDeciDegrees(1800);
//	navigation.moveMM(60);
//	cubesActuator.handleUnloadAsIs();
//	cubesActuator.waitForReady(1000);
//
//	navigation.moveMM(-60);
//	navigation.rotateDeciDegrees(1800);
//	cubesActuator.handleLoadPrepare();
//	cubesActuator.waitForReady(2000);
//	navigation.moveMM(200);
//	cubesActuator.handleLoadLv3();
//	cubesActuator.waitForReady(1000);
//	navigation.moveMM(-200);
//	navigation.rotateDeciDegrees(1800);
//	navigation.moveMM(60);
//	cubesActuator.handleUnloadAsIs();
//	cubesActuator.waitForReady(1000);

	/*
	 navigation.moveTo(Point(210, 520));
	 navigation.moveTo(Point(930, 520));
	 navigation.moveTo(Point(930, 850));
	 navigation.moveTo(Point(750, 850));

	 palClearPad(GPIOC, GPIOC_PIN8);
	 navigation.waitForDelay(8000);

	 navigation.moveMM(-100);
	 palSetPad(GPIOC, GPIOC_PIN8);
	 navigation.waitForDelay(5000);

	 control.obstacleAvoidance = false;
	 beacon.setBeepEnabled(false);
	 navigation.moveTo(Point(390, 850));
	 data.addScore(10);

	 navigation.moveMM(-100);
	 navigation.rotateDeciDegrees(-900);
	 control.obstacleAvoidance = true;
	 beacon.setBeepEnabled(true);
	 navigation.moveTo(Point(490, 1130));

	 navigation.rotateDeciDegrees(-900);
	 navigation.moveMM(-340);
	 data.addScore(25);
	 */
}

void MatchHomologation::executeSmall() {
	extern Beacon2018 beacon;
	MatchData &data = getMatchData();

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	control.setPosition(530, 115 + 10, ANGLE_TO_RADIANS(90));
	control.setSpeedIndex(1);
	data.addScore(5);

	navigation.waitForDelay(500);

	control.obstacleAvoidance = false;
	beacon.setBeepEnabled(false);
	/*
	 navigation.moveTo(Point(526, 220));
	 navigation.moveTo(Point(980, 220));
	 navigation.moveTo(Point(1200, 130));
	 navigation.moveTo(Point(1600, 250));
	 navigation.moveTo(Point(1870, 120));
	 navigation.rotateDeciDegrees(4760);
	 navigation.moveTo(Point(1870, 170));
	 navigation.rotateDeciDegrees(4390);
	 data.addScore(50);

	 control.obstacleAvoidance = true;
	 beacon.setBeepEnabled(true);
	 navigation.moveTo(Point(300, 475));
	 data.addScore(1);
	 */

	// Abeille
	extern WaterActuator waterActuator;
	navigation.moveTo(Point(530, 200));
	navigation.moveTo(Point(1200, 550));
	waterActuator.handleBee();
	navigation.moveTo(Point(1850, 120));
	navigation.moveTo(Point(1850, 170));
	navigation.rotateDeciDegrees(4560);

	// Distributeur d'eau propre
	navigation.moveTo(Point(1250, 100));
	navigation.moveTo(Point(1100, 100));
	navigation.moveTo(Point(810, 280));
	navigation.rotateToOrientationDeciDegrees(0);
	navigation.moveMM(-150);
	waterActuator.handleOpenDispenser();
	navigation.waitForDelay(2000);
	navigation.moveMM(-25);
	waterActuator.handleCleanWaterDispenser();
}

void MatchHomologation::prepareBig() {
	extern CubesActuator cubesActuator;
//	PositionControl &control = navigation.getControl();
//	bool side = control.isReversed();
//
//	cubesActuator.setMatchSide(side);
//	navigation.waitForDelay(1000);
//	cubesActuator.setInitialPosition();
//	navigation.waitForDelay(1000);


	/*cubesActuator.handleLoadPrepare();
	navigation.moveMM(70);
	navigation.waitForDelay(3000);
	cubesActuator.handleLoadJocker();*/
}

void MatchHomologation::prepareSmall() {
	navigation.moveMM(10);

	// Côté
	PositionControl &control = navigation.getControl();
	bool side = control.isReversed();

	extern WaterActuator waterActuator;
	waterActuator.setMatchSide(side);
}
