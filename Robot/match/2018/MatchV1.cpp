/*
 * MatchHomologation.cpp
 *
 *  Created on: 17 f�vr. 2018
 *      Author: Emmanuel / Robin
 */

#include "MatchV1.h"

#include "ch.h"

#include "devices/actuators/cubes/CubesActuator.h"
#include "devices/actuators/water/WaterActuator.h"
#include "devices/beacon/Beacon2018.h"
#include "devices/host/HostDevice.h"
#include "devices/host/HostDataF446REBig.h"
#include "devices/host/HostDataF446RESmall.h"
#include "devices/io/LCDScreen.h"
#include "math/cenMath.h"

MatchV1::MatchV1(Navigation &navigation) :
		Match(navigation) {
}

void MatchV1::execute() {
	extern LCDScreen screen;
	screen.displayMessage("C'est parti !");

	if (HostDevice::getDeviceId() == HostData_F446RE_Small::getDeviceId()) {
		executeSmall();
	} else if (HostDevice::getDeviceId() == HostData_F446RE_Big::getDeviceId()) {
		executeBig();
	}

	setActionResult(ActionResult::Next);
}

void MatchV1::executeBig() {
	extern Beacon2018 beacon;
	MatchData &data = getMatchData();

	extern CubesActuator cubesActuator;

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	control.setPosition(210, 140 + 70, ANGLE_TO_RADIANS(90));
	control.setSpeedIndex(1);
	data.addScore(5);

	navigation.waitForDelay(500);

	navigation.moveTo(Point(210, 1130));
	navigation.rotateDeciDegrees(-900);
	navigation.moveMM(-60);
	data.addScore(25);

	navigation.moveMM(60);
	cubesActuator.handleLoadPrepare();
	navigation.moveTo(Point(210, 850));
	navigation.moveTo(Point(350, 850));
	cubesActuator.handleLoadLv1();
	cubesActuator.waitForReady(1000);
	navigation.moveMM(-20);
	navigation.rotateDeciDegrees(1800);
	navigation.moveMM(60);
	cubesActuator.handleUnloadAsIs();
	cubesActuator.waitForReady(1000);

	navigation.moveMM(-60);
	navigation.rotateDeciDegrees(1800);
	cubesActuator.handleLoadPrepare();
	cubesActuator.waitForReady(2000);
	navigation.moveMM(100);
	cubesActuator.handleLoadLv2();
	cubesActuator.waitForReady(1000);
	navigation.moveMM(-100);
	navigation.rotateDeciDegrees(1800);
	navigation.moveMM(60);
	cubesActuator.handleUnloadAsIs();
	cubesActuator.waitForReady(1000);

	navigation.moveMM(-60);
	navigation.rotateDeciDegrees(1800);
	cubesActuator.handleLoadPrepare();
	cubesActuator.waitForReady(2000);
	navigation.moveMM(200);
	cubesActuator.handleLoadLv3();
	cubesActuator.waitForReady(1000);
	navigation.moveMM(-200);
	navigation.rotateDeciDegrees(1800);
	navigation.moveMM(60);
	cubesActuator.handleUnloadAsIs();
	cubesActuator.waitForReady(1000);

	/*
	 navigation.moveTo(Point(210, 520));
	 navigation.moveTo(Point(930, 520));
	 navigation.moveTo(Point(930, 850));
	 navigation.moveTo(Point(750, 850));

	 palClearPad(GPIOC, GPIOC_PIN8);
	 navigation.waitForDelay(8000);

	 navigation.moveMM(-100);
	 palSetPad(GPIOC, GPIOC_PIN8);
	 navigation.waitForDelay(5000);

	 control.obstacleAvoidance = false;
	 beacon.setBeepEnabled(false);
	 navigation.moveTo(Point(390, 850));
	 data.addScore(10);

	 navigation.moveMM(-100);
	 navigation.rotateDeciDegrees(-900);
	 control.obstacleAvoidance = true;
	 beacon.setBeepEnabled(true);
	 navigation.moveTo(Point(490, 1130));

	 navigation.rotateDeciDegrees(-900);
	 navigation.moveMM(-340);
	 data.addScore(25);
	 */
}

void MatchV1::executeSmall() {
	extern Beacon2018 beacon;
	MatchData &data = getMatchData();

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	control.setPosition(530, 115 + 10, ANGLE_TO_RADIANS(90));
	control.setSpeedIndex(1);
	data.addScore(5);

	navigation.waitForDelay(500);

	control.obstacleAvoidance = false;
	beacon.setBeepEnabled(false);
	/*
	 navigation.moveTo(Point(526, 220));
	 navigation.moveTo(Point(980, 220));
	 navigation.moveTo(Point(1200, 130));
	 navigation.moveTo(Point(1600, 250));
	 navigation.moveTo(Point(1870, 120));
	 navigation.rotateDeciDegrees(4760);
	 navigation.moveTo(Point(1870, 170));
	 navigation.rotateDeciDegrees(4390);
	 data.addScore(50);

	 control.obstacleAvoidance = true;
	 beacon.setBeepEnabled(true);
	 navigation.moveTo(Point(300, 475));
	 data.addScore(1);
	 */

	extern WaterActuator waterActuator;
	navigation.moveTo(Point(530, 200));
	navigation.moveTo(Point(1200, 550));
	waterActuator.handleBee();
	navigation.moveTo(Point(1850, 120));
	navigation.moveTo(Point(1850, 170));
	navigation.rotateDeciDegrees(4560);
	navigation.moveTo(Point(1250, 100));
	navigation.moveTo(Point(1100, 100));
	navigation.moveTo(Point(810, 280));
	navigation.rotateToOrientationDeciDegrees(0);
	navigation.moveMM(-150);
	waterActuator.handleOpenDispenser();
	navigation.waitForDelay(2000);
	navigation.moveMM(-25);
	waterActuator.handleCleanWaterDispenser();
}

void MatchV1::prepareBig() {
	navigation.moveMM(70);
}

void MatchV1::prepareSmall() {
	navigation.moveMM(10);

	// c�t�
	PositionControl &control = navigation.getControl();
	bool side = control.isReversed();

	extern WaterActuator waterActuator;
	waterActuator.setMatchSide(side);
}

void MatchV1::prepare() {
	extern Beacon2018 beacon;
	beacon.setLedBlinking(100, 100, 0);
	displayMatchSide();

	if (HostDevice::getDeviceId() == HostData_F446RE_Small::getDeviceId()) {
		prepareSmall();
	} else if (HostDevice::getDeviceId() == HostData_F446RE_Big::getDeviceId()) {
		prepareBig();
	}

	navigation.waitForStart(this);
}

void MatchV1::onNavigationEvent(const Navigation &navigation) {
	(void) navigation;
	updateMatchSide();
}

void MatchV1::displayMatchSide() {
	bool side = navigation.getControl().isReversed();
	extern LCDScreen screen;
	screen.displayMessage("Couleur :");
	screen.displayMessage(side ? "orange" : "vert", 14);
}

void MatchV1::changeMatchSide(bool newSide) {
	extern WaterActuator waterActuator;
	waterActuator.setMatchSide(newSide);

	PositionControl &control = navigation.getControl();
	control.setReversed(newSide);
	displayMatchSide();
}

void MatchV1::updateMatchSide() {
	extern Beacon2018 beacon;
	beacon.updateHMIData();
	const Beacon2018Data &data = beacon.getData();
	bool side = data.hmiValue1 < 128;
	PositionControl &control = navigation.getControl();
	if (side != control.isReversed()) {
		changeMatchSide(side);
	}
}
