/*
 * MatchHomologation22.h
 *
 *  Created on: 17 f�vr. 2018
 *      Author: Emmanuel
 */

#ifndef MATCH_MatchHomologation2_H_
#define MATCH_MatchHomologation2_H_

#include "match/Match.h"

class MatchHomologation2 : public Match, public NavigationListener {
private:
	void changeMatchSide(bool newSide);
	void displayMatchSide();
	void executeBig();
	void executeSmall();
	void prepareBig();
	void prepareSmall();
	void updateMatchSide();

protected:
	virtual void prepare();
	virtual void execute();
	virtual void onNavigationEvent(const Navigation &navigation);

public:
	MatchHomologation2(Navigation &navigation);
};

#endif /* MATCH_MatchHomologation2_H_ */
