/*
 * MatchChoice.h
 *
 *  Created on: 21 d�c. 2019
 *      Author: anastaszor
 */

#ifndef MATCH_MATCHCHOICE_H_
#define MATCH_MATCHCHOICE_H_

#include "control/Navigation.h"
#include "match/MatchData.h"
#include "match/MatchExecutor.h"

/**
 * MatchChoice is a class between the Match and the Matches Action. It takes
 * place once the choices has been made on board by the operator with the
 * strategy switch.
 */
class MatchChoice
{
protected:
	const char* name;
	MatchExecutor executor;
	Navigation &navigation;
	MatchData &matchData;

public:

	/**
	 * Constructor.
	 */
	MatchChoice(Navigation &navigation, MatchData &matchData);

	/**
	 * Aborts the tasks executor.
	 */
	void abort();

	/**
	 * Gets the name of the choice to be displayed on the lcd.
	 * Defaults to "UNKNOWN".
	 *
	 * @return string (8 char long max)
	 */
	virtual const char* getName() const;

	/**
	 * Executes the actions to be executed before the matches, to prepare for
	 * main session.
	 */
	virtual void prepare();

	/**
	 * Executes the actions to be executed during the matches main session.
	 */
	virtual void execute();

	/**
	 * Executes the action to be executed once the matches' timer has reach
	 * end time.
	 */
	virtual void finish();

};

#endif /* MATCH_MATCHCHOICE_H_ */
