/*
 * Match.h
 *
 *  Created on: 17 sept. 2017
 *      Author: Emmanuel
 */

#ifndef MATCH_MATCH_H_
#define MATCH_MATCH_H_

#include "control/Navigation.h"
#include "devices/timer/MatchTimer.h"
#include "match/MatchData.h"

#define MATCH_DURATION 100000

enum class MatchState {
	Initialization,
	Preparation,
	Starting,
	Started,
	Terminated,
	Finalization,
	Finalized
};

enum class ActionResult {
	Next,
	Loop
};

class Match : MatchTimerListener {
protected:
	Navigation &navigation;
	MatchData &matchData;

private:
	MatchState state;
	ActionResult result;

	void nextState();

protected:
	MatchTimer matchTimer;

	Match(Navigation &navigation);
	Match(Navigation &navigation, MatchData &matchData);

	virtual MatchData& createMatchData(const MatchTimer& timer);
	virtual void initialize();
	virtual void prepare();
	virtual void start();
	virtual void execute();
	virtual void finish();
	virtual void finalize();

	virtual void onMatchTimeElapsed(const MatchTimer &timer);

	void setActionResult(ActionResult value);
	virtual void terminateNavigation();

public:
	/**
	 * \brief Donn�es de match
	 *
	 * Renvoie les donn�es de match.
	 * @return les donn�es de match
	 */
	MatchData& getMatchData() const;
	/**
	 * Ex�cute les actions de match.
	 */
	void run();
	/**
	 * Ex�cute les actions de fin de match.
	 */
	virtual void terminate();
	/**
	 * M�thode d'actualisation pour les actions asynchrone.
	 */
	virtual void update();
};

#endif /* MATCH_MATCH_H_ */
