/*
 * MatchExecutor.cpp
 *
 *  Created on: 18 f�vr. 2019
 *      Author: Emmanuel
 */

#include <cfloat>

#include "MatchExecutor.h"

#include "devices/io/Console.h"
#include "devices/io/LCDScreen.h"
#include "utils/Arrays.h"

MatchExecutor::MatchExecutor(Navigation &navigation, unsigned int obstacleTimeout) :
		navigation(navigation), obstacleTimeout(obstacleTimeout) {
}

void MatchExecutor::start() {
	started = true;
	aborted = false;
}

bool MatchExecutor::isAborted() const {
	return aborted;
}

bool MatchExecutor::isFinished() const {
	return finished;
}

void MatchExecutor::stop() {
	aborted = true;
	if (currentAction.isValid()) {
		currentAction->abort();
	}
}

void MatchExecutor::run() {
	if (started || finished) {
		return;
	}
	start();
	PositionControl &control = navigation.getControl();
	control.setObstacleTimeout(obstacleTimeout);
	while (!aborted && !finished) {
		executeNext();
		chThdSleepMilliseconds(20);
	}
	control.setObstacleTimeout(0);
}

void MatchExecutor::handleMoveToActionFailure() {
	MatchTaskFailurePolicy policy = currentTask->getFailurePolicy();
	switch (policy) {
		case MatchTaskFailurePolicy::NextTask:
			// Marque la t�che comme �tant indisponible, une autre t�che sera choisie
			clearCurrentTarget(true);
			currentTask->setAvailable(false);
			currentTask.clear();

			// D�termine s'il reste des t�ches ex�cutables
			checkCompletableTasks();

			break;

		case MatchTaskFailurePolicy::NextAction:
			// conserve la cible active, une action disponible sera s�lectionn�e � l'it�ration suivante
			break;
	}
}

MatchExecutorResult MatchExecutor::executeNext() {
	extern Console console;
	if (aborted) {
		console.sendMessage("executor aborted");
		return MatchExecutorResult::Aborted;
	}
	if (finished) {
		console.sendMessage("executor finished");
		return MatchExecutorResult::Finished;
	}

	Pointer<MatchAction> action = findNextAction();
	if (!action.isValid()) {
		return MatchExecutorResult::ActionDone;
	}

	// Affichage de la cible
	displayTarget(*action);

	// Mouvement vers l'�l�ment
	bool success = moveToAction(*action);
	if (!success) {
		// �chec du mouvement
		// on n'a pas touch� � l'�l�ment cible, on le d�verrouille pour pouvoir l'utiliser plus tard
		handleMoveToActionFailure();

		return MatchExecutorResult::ActionNotReached;
	}

	// Ex�cution de la s�quence propre � l'action
	action->execute(navigation);

	// Marque la cible comme trait�e
	currentTarget->setDone();

	// Traitement du r�sultat
	// on garde l'�l�ment verrouill�, car l'action a �t� ex�cut�e
	clearCurrentTarget(false);
	if (action->hasFailed()) {
		handleObstacle();
		console.sendMessage("action failed");
		return MatchExecutorResult::ActionFailed;
	} else if (action->isAborted()) {
		console.sendMessage("action aborted");
		return MatchExecutorResult::ActionAborted;
	}

	// D�termine s'il reste des t�ches ex�cutables
	checkCompletableTasks();

	// mets � jour la carte de navigation
	updateMapCosts();

	return MatchExecutorResult::ActionDone;
}

void MatchExecutor::checkCompletableTasks() {
	if (tasks.isValid()) {
		unsigned int n = tasks->getCount();
		for (unsigned int i = 0; i < n; i++) {
			MatchTask &task = tasks->getTaskAt(i);
			task.updateAvailability(TASK_UNAVAILABILITY);
			if (!task.isDone() && task.isCompletable()) {
				return;
			}
		}
	}
	// aucune t�che achevable trouv�e
	finished = true;
}

Pointer<MatchAction> MatchExecutor::findNextAction() {
	Pointer<MatchAction> result;

	// V�rifie si la t�che active est toujours r�alisable
	bool valid = currentTask.isValid() && !currentTask->isDone() && currentTask->isCompletable();
	if (!valid) {
		// cherche une nouvelle t�che
		currentTask = findNextTask();
	}
	if (!currentTask.isValid()) {
		// �chec de la recherche d'une nouvelle t�che
		return result;
	}

	// Recherche de la cible suivante
	Pointer<MatchTarget> target = currentTask->getCurrentTarget();
	if (target.isValid()) {
		setCurrentTarget(*target);
		MatchAction &action = findNearestAction();
		result = Pointer<MatchAction>(&action);
	}
	return result;
}

Pointer<MatchTask> MatchExecutor::findNextTask() {
	extern Console console;
	console.sendMessage("findNextTask");

	// recherche de la t�che avec le meilleur rapport co�t / b�n�fice
	MatchTask *bestTask = NULL;
	if (tasks.isValid()) {
		const Position &p = navigation.getControl().getPosition();
		unsigned int n = tasks->getCount();
		float bestCost = FLT_MAX;
		for (unsigned int i = 0; i < n; i++) {
			MatchTask &task = tasks->getTaskAt(i);
			if (!task.isDone() && task.isAvailable() && task.isCompletable()) {
				int score = task.getExpectedScore();
				int cost = task.getEstimatedCost(p);
				float c = getTaskCost(score, cost);
				if (c < bestCost) {
					bestCost = c;
					bestTask = &task;
				}

				const char *label = task.getLabel();
				console.sendKeyValue(label, (int) c);
			}
		}
		console.sendKeyValue("best", bestTask == NULL ? "none" : bestTask->getLabel());
	}
	Pointer<MatchTask> result = Pointer<MatchTask>(bestTask);
	return result;
}

void MatchExecutor::setCurrentTarget(MatchTarget &target) {
	currentTarget = Pointer<MatchTarget>(&target);
	// Verrouillage de l'�l�ment cible
	MatchElement &element = currentTarget->getElement();
	element.setLockedByTask(*currentTask);
}

bool MatchExecutor::navigateToAction(const Point &point) {
	const Point* path[10];
	PositionControl &control = navigation.getControl();
	const Position &position = control.getPosition();
	unsigned int pathSize = navigationService->computePath(position, point, path, ARRAY_SIZE(path));
	if (pathSize > 0) {
		for (unsigned int i = 0; i < pathSize; i++) {
			navigation.moveTo(*path[i]);
			if (control.hasMoveFailed()) {
				return false;
			}
		}
	}
	return true;
}

bool MatchExecutor::moveToAction(MatchAction &action) {
	const Point &p = action.getStart();
	if (p == Point(0, 0)) {
		return true;
	}
	bool success;
	if (navigationService.isValid()) {
		// navigation en utilisant la carte
		success = navigateToAction(p);
	} else {
		// navigation en ligne droite
		navigation.moveTo(p);
		success = !navigation.getControl().hasMoveFailed();
	}
	if (!success) {
		handleObstacle();
	}
	return success;
}

MatchAction& MatchExecutor::findNearestAction() const {
	// V�rifie que la cible courante est valide
	if (!currentTarget.isValid()) {
		return MatchAction::none;
	}

	extern Console console;
	console.sendMessage("findNearestAction");

	// Recherche de l'action la plus proche en distance
	const MatchActionList &list = currentTarget->getAvailableActions();
	unsigned int n = list.getCount();
	const Position &position = navigation.getControl().getPosition();
	MatchAction *nearest = &MatchAction::none;
	float distance = FLT_MAX;
	for (unsigned int i = 0; i < n; i++) {
		MatchAction &action = list.getActionAt(i);
		// Ne traite pas les actions marqu�es �chou�es ou avort�es
		if (action.hasFailed() || action.isAborted()) {
			continue;
		}
		const Point &p = action.getStart();
		float d = position.getDistance(p);
		if (d < distance) {
			distance = d;
			nearest = &action;
		}

		const char *label = action.getLabel();
		console.sendKeyValue(label, (int) d);
	}

	console.sendKeyValue("nearest", nearest->getLabel());
	return *nearest;
}

void MatchExecutor::clearCurrentTarget(bool unlockTarget) {
	if (currentTarget.isValid()) {
		if (unlockTarget) {
			// lib�ration de l'�l�ment cible qui pourra �tre r�utilis� ult�rieurement
			MatchElement &element = currentTarget->getElement();
			element.unlock();
		}
		currentTarget.clear();
	}
}

void MatchExecutor::displayTarget(MatchAction &action) const {
	extern LCDScreen screen;
	const char *label = currentTask->getLabel();
	screen.displayMessage(label);
	label = action.getLabel();
	int x = 20 - strlen(label);
	screen.displayMessage(label, x);

	extern Console console;
	console.sendKeyValue("target", label);
}

float MatchExecutor::getTaskCost(int expectedScore, int expectedCost) const {
	return expectedCost - expectedScore;
}

void MatchExecutor::setObstacleTimeout(unsigned int value) {
	obstacleTimeout = value;
}

void MatchExecutor::setNavigationService(NavigationService &service) {
	navigationService = Pointer<NavigationService>(&service);
}

void MatchExecutor::handleObstacle() {
	if (!navigationService.isValid()) {
		// pas de carte d�finie
		return;
	}

	// ajoute l'obstacle sur la carte
	PositionControl &control = navigation.getControl();
	if (control.getError() == MoveError::ObstacleTimeout) {
		const Point &obstacle = control.getObstaclePosition();
		NavigationMap &map = navigationService->getMap();
		map.addObstacle(obstacle);
	}
}

void MatchExecutor::updateMapCosts() {
	if (!navigationService.isValid()) {
		return;
	}

	NavigationMap &map = navigationService->getMap();
	map.decayCosts();
}

void MatchExecutor::setTasksList(MatchTaskList &tasks) {
	this->tasks = ConstPointer<MatchTaskList>(&tasks);
}
