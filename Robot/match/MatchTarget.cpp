/*
 * MatchTarget.cpp
 *
 *  Created on: 23 janv. 2019
 *      Author: Emmanuel
 */

#include "MatchTarget.h"

MatchTarget::MatchTarget(const MatchActionList& actions, MatchElement &element) :
		actions(actions), element(element) {
}

Pointer<MatchTarget> MatchTarget::getNext() const {
	return next;
}

const MatchActionList& MatchTarget::getAvailableActions() const {
	return actions;
}

MatchElement& MatchTarget::getElement() const {
	return element;
}

bool MatchTarget::isAvailable() const {
	return true;
}

bool MatchTarget::isDone() const {
	return done;
}

void MatchTarget::setDone() {
	done = true;
}

void MatchTarget::setNext(MatchTarget &next) {
	this->next = Pointer<MatchTarget>(&next);
}
