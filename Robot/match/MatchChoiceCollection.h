/*
 * MatchChoiceCollection.h
 *
 *  Created on: 21 d�c. 2019
 *      Author: anastaszor
 */

#ifndef MATCH_MATCHCHOICECOLLECTION_H_
#define MATCH_MATCHCHOICECOLLECTION_H_

#include "MatchChoice.h"

/**
 * MatchChoiceCollection is a class that stores all the given match choices
 * that are available for a given robot switch.
 */
class MatchChoiceCollection
{
private:
	void ensureRange(int &index) const;

protected:
	Pointer<MatchChoice> choices[8];

public:

	/**
	 * Constructor.
	 */
	MatchChoiceCollection(MatchChoice &c1, MatchChoice &c2, MatchChoice &c3, MatchChoice &c4, MatchChoice &c5, MatchChoice &c6, MatchChoice &c7, MatchChoice &c8);

	const Pointer<MatchChoice>& getMatchChoice(int index) const;

	/**
	 * Gets the name of the choice to be displayed on the lcd.
	 * Defaults to "UNKNOWN".
	 *
	 * @return string (8 char long max)
	 * @see MatchChoice::getName()
	 */
	const char* getName(int index) const;

	/**
	 * Prepares the action at the given index.
	 *
	 * @see MatchChoice::prepare()
	 */
	void prepare(int index);

	/**
	 * Executes the action at the given index.
	 *
	 * @see MatchChoice::execute()
	 */
	void execute(int index);

	/**
	 * Finishes the action at the given index.
	 *
	 * @see MatchChoice::finish()
	 */
	void finish(int index);

};

#endif /* MATCH_MATCHCHOICECOLLECTION_H_ */
