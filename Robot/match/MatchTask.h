/*
 * MatchTask.h
 *
 *  Created on: 23 janv. 2019
 *      Author: Emmanuel
 */

#ifndef MATCHTASK_H
#define MATCHTASK_H

#include "match/MatchTarget.h"
#include "odometry/Position.h"
#include "utils/Pointer.h"

enum class MatchTaskFailurePolicy {
	NextTask,
	NextAction
};

class MatchTask {
private:
	const char* label;
	int expectedScore;
	int staticCost;
	Pointer<MatchTarget> firstTarget;
	systime_t unavailableTimestamp = 0;
	MatchTaskFailurePolicy failurePolicy = MatchTaskFailurePolicy::NextTask;

protected:
	virtual int computeDynamicCost(const Position& position) const;

public:
	MatchTask(const char* label, int expectedScore, int staticCost, Pointer<MatchTarget> firstTarget);
	int getExpectedScore() const;
	int getEstimatedCost(const Position &position) const;
	MatchTaskFailurePolicy getFailurePolicy() const;
	const char* getLabel() const;
	const Pointer<MatchTarget> getCurrentTarget() const;
	bool isAvailable() const;
	bool isCompletable() const;
	bool isDone() const;
	void resetActions();
	void setAvailable(bool value);
	void setDone();
	void updateAvailability(unsigned int seconds);
};

#endif /* MATCHTASK_H */
