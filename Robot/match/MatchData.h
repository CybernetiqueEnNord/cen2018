/*
 * MatchData.h
 *
 *  Created on: 26 oct. 2017
 *      Author: Emmanuel
 */

#ifndef MATCH_MATCHDATA_H_
#define MATCH_MATCHDATA_H_

#include "devices/timer/MatchTimer.h"
#include "odometry/Position.h"
#include "utils/ConstPointer.h"

class MatchData {
private:
	const MatchTimer &timer;
	ConstPointer<Position> position;

protected:
	int score = 0;
	virtual int getComputedScore() const;

public:
	MatchData(const MatchTimer& timer);
	void addScore(int value);
	const Position& getPosition() const;
	int getScore() const;
	const MatchTimer& getTimer() const;
	void setPosition(const Position &position);
	/**
	 * D�finit la valeur du score.
	 *
	 * @param [in] value la nouvelle valeur du score
	 */
	virtual void setScore(int value);
	/**
	 * Effectue le calcul du score et met � jour la valeur stock�e.
	 *
	 * @return la valeur du score calcul�
	 */
	void updateScore();
};

#endif /* MATCH_MATCHDATA_H_ */
