/*
 * SimpleFallbackAction.cpp
 *
 *  Created on: 30 mai 2019
 *      Author: Anastaszor
 */

#include "SimpleFallbackAction.h"
#include "MatchAction.h"

SimpleFallbackAction::SimpleFallbackAction(MatchAction &nominal, MatchAction &fallback) :
	MatchAction("Fallback", Point(0, 0)), nominal(nominal), fallback(fallback)
{
	doFallback = false;
	maxStepNominal = 0;
}

void SimpleFallbackAction::abort()
{
	if(doFallback)
	{
		fallback.abort();
	}
	else
	{
		nominal.abort();
		doFallback = true;
	}
}

bool SimpleFallbackAction::hasFailed()
{
	return (!doFallback && nominal.hasFailed()) || (doFallback && fallback.hasFailed());
}

bool SimpleFallbackAction::isAborted()
{
	return (!doFallback && nominal.isAborted()) || (doFallback && fallback.isAborted());
}

const Point& SimpleFallbackAction::getStart() const
{
	return nominal.getStart();
}

void SimpleFallbackAction::executeStep(Navigation &navigation, unsigned int step)
{
	doFallback &= !nominal.isAborted() && !nominal.isTerminated();
	if(doFallback)
	{
		nominal.executeStep(navigation, step);
		maxStepNominal = step;
	}
	else
	{
		fallback.executeStep(navigation, step - maxStepNominal);
	}
}

void SimpleFallbackAction::reset()
{
	nominal.reset();
	fallback.reset();
}

bool SimpleFallbackAction::isTerminated()
{
	return (!doFallback && nominal.isTerminated()) || (doFallback && fallback.isTerminated());
}
