/*
 * Match.cpp
 *
 *  Created on: 17 sept. 2017
 *      Author: Emmanuel
 */

#include "Match.h"

Match::Match(Navigation &navigation, MatchData &matchData) :
		navigation(navigation), matchData(matchData) {
	const Position &position = navigation.getControl().getPosition();
	matchData.setPosition(position);
	result = ActionResult::Next;
	state = MatchState::Initialization;
}

Match::Match(Navigation &navigation) :
		Match(navigation, createMatchData(matchTimer)) {
}

void Match::initialize() {
	matchTimer.initialize(MATCH_DURATION);
	matchTimer.listener = this;
}

void Match::prepare() {
	navigation.waitForStart();
}

void Match::start() {
	matchTimer.start();
}

void Match::execute() {
}

void Match::finish() {
	navigation.waitForDelay(MATCH_DURATION);
	terminate();
}

void Match::finalize() {
}

void Match::nextState() {
	switch (result) {
		case ActionResult::Next:
			state = static_cast<MatchState>(static_cast<int>(state) + 1);
			break;

		case ActionResult::Loop:
			break;
	}
}

void Match::run() {
	state = MatchState::Initialization;
	navigation.asynchronous = false;
	while (true) {
		result = ActionResult::Next;
		switch (state) {
			case MatchState::Initialization:
				initialize();
				break;

			case MatchState::Preparation:
				prepare();
				break;

			case MatchState::Starting:
				start();
				break;

			case MatchState::Started:
				execute();
				break;

			case MatchState::Terminated:
				finish();
				break;

			case MatchState::Finalization:
				finalize();
				break;

			case MatchState::Finalized:
				return;
				break;
		}
		nextState();
	}
}

void Match::setActionResult(ActionResult value) {
	result = value;
}

MatchData& Match::createMatchData(const MatchTimer& timer) {
	static MatchData data(timer);
	return data;
}

MatchData& Match::getMatchData() const {
	return matchData;
}

void Match::onMatchTimeElapsed(const MatchTimer &timer) {
	(void) timer;

	terminateNavigation();
}

void Match::terminate() {
	terminateNavigation();
}

void Match::terminateNavigation() {
	navigation.setTerminated(true);
	PositionControl &control = navigation.getControl();
	control.setOverrideMotorsCommands(true, 0, 0);
}

void Match::update() {
}
