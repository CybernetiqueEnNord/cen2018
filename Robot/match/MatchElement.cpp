/*
 * MatchElement.cpp
 *
 *  Created on: 26 janv. 2019
 *      Author: emmanuel
 */

#include "MatchElement.h"

MatchElement::MatchElement() {
}

bool MatchElement::isAvailableToTask(const MatchTask &task) const {
	if (lockingTask.isValid()) {
		bool result = lockingTask == ConstPointer<MatchTask>(&task);
		return result;
	}
	bool result = getState() == MatchElementState::Available;
	return result;
}

void MatchElement::setLockedByTask(const MatchTask &task) {
	lockingTask = ConstPointer<MatchTask>(&task);
}

MatchElementState MatchElement::getState() const {
	if (state == MatchElementState::Custom && callback.isValid()) {
		return callback->customGetState();
	}
	return state;
}

void MatchElement::unlock() {
	lockingTask = ConstPointer<MatchTask>();
}

void MatchElement::setState(MatchElementState state) {
	this->state = state;
}

void MatchElement::setCustomGetState(MatchElementCallback &callback) {
	this->callback = Pointer<MatchElementCallback>(&callback);
	state = MatchElementState::Custom;
}
