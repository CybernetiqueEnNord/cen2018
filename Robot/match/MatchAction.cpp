/*
 * MatchAction.cpp
 *
 *  Created on: 23 janv. 2019
 *      Author: Emmanuel
 */

#include "MatchAction.h"

class MatchActionNone: public MatchAction {
public:
	MatchActionNone() :
			MatchAction("none", Point(0, 0)) {
	}
};

static MatchActionNone actionNone;

MatchAction &MatchAction::none = actionNone;

MatchAction::MatchAction(const char *label) :
		label(label), start(Point(0, 0)) {
}

MatchAction::MatchAction(const char *label, const Point &start) :
		label(label), start(start) {
}

void MatchAction::execute(Navigation &navigation) {
	PositionControl &control = navigation.getControl();
	aborted = false;
	failed = false;
	terminated = false;
	unsigned int step = 0;
	while (!aborted) {
		executeStep(navigation, step);
		if (terminated) {
			goto end;
		}
		failed = control.hasMoveFailed();
		if (failed) {
			goto end;
		}
		step++;
	}

	end: finalize(navigation);
}

bool MatchAction::hasFailed() const {
	return failed;
}

bool MatchAction::isAborted() const {
	return aborted;
}

const Point& MatchAction::getStart() const {
	return start;
}

void MatchAction::executeStep(Navigation &navigation, unsigned int step) {
	(void) navigation;
	(void) step;
	terminated = true;
}

const char* MatchAction::getLabel() const {
	return label;
}

void MatchAction::finalize(Navigation &navigation) {
	executeStep(navigation, STEP_FINALIZATION);
}

void MatchAction::reset() {
	aborted = false;
	failed = false;
}

bool MatchAction::isTerminated() const {
	return terminated;
}

void MatchAction::abort() {
	aborted = true;
}
