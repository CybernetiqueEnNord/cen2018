/*
 * SingleActionList.h
 *
 *  Created on: 3 mars 2019
 *      Author: emmanuel
 */

#ifndef MATCH_SINGLEACTIONLIST_H_
#define MATCH_SINGLEACTIONLIST_H_

#include "MatchAction.h"
#include "MatchActionList.h"

class SingleActionList : public MatchActionList {
private:
	MatchAction &action;

public:
	SingleActionList(MatchAction &action);
	virtual MatchAction& getActionAt(unsigned int index) const;
	virtual unsigned int getCount() const;
};

#endif /* MATCH_SINGLEACTIONLIST_H_ */
