/*
 * MatchElement.h
 *
 *  Created on: 26 janv. 2019
 *      Author: emmanuel
 */

#ifndef MATCH_MATCHELEMENT_H_
#define MATCH_MATCHELEMENT_H_

#include "utils/ConstPointer.h"
#include "utils/Pointer.h"

enum class MatchElementState {
	Available,
	Unavailable,
	Locked,
	Custom
};

class MatchTask;
class MatchElement;

class MatchElementCallback {
protected:
	friend class MatchElement;
	virtual MatchElementState customGetState() const = 0;
};

class MatchElement {
private:
	ConstPointer<MatchTask> lockingTask;
	Pointer<MatchElementCallback> callback;
	MatchElementState state = MatchElementState::Available;

public:
	MatchElement();
	MatchElementState getState() const;
	bool isAvailableToTask(const MatchTask &task) const;
	void setCustomGetState(MatchElementCallback &callback);
	void setLockedByTask(const MatchTask &task);
	void setState(MatchElementState state);
	void unlock();
};

#endif /* MATCH_MATCHELEMENT_H_ */
