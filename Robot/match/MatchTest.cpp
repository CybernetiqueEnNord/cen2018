/*
 * MatchTest.cpp
 *
 *  Created on: 19 sept. 2017
 *      Author: Emmanuel
 */

#include "match/MatchTest.h"

MatchTest::MatchTest(Navigation &navigation) :
		Match(navigation) {
}

void MatchTest::execute() {
	MatchData &data = getMatchData();

	PositionControl &control = navigation.getControl();
	control.setSpeedIndex(1);

	navigation.moveTo(Point(500, 0));
	data.addScore(13);
	navigation.moveTo(Point(500, 500));
	data.addScore(13);
	navigation.moveTo(Point(0, 500));
	data.addScore(13);
	navigation.moveTo(Point(0, 0));
	data.addScore(13);
	navigation.rotateToOrientationDeciDegrees(0);

	setActionResult(ActionResult::Next);
}
