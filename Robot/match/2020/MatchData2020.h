/*
 * MatchData2020.h
 *
 *  Created on: 28 sep. 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_MATCHDATA2020_H_
#define MATCH_MATCHDATA2020_H_

#include "../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "../../devices/actuators/2020/big/BigBuoySensorData.h"
#include "../MatchData.h"
#include "Match2020Enums.h"
#include "points/Match2020BouyState.h"
#include "points/Match2020Dock.h"

class MatchData2020 : public MatchData {
private:
	Match2020BouyState tableState;
	Match2020Dock startingDock;
	Match2020Dock littleDock;

	bool nearestWindsockToggled;
	bool farestWindsockToggled;
	bool lighthousePresent;
	bool lighthouseUplifted;
	bool lodgesUplifted;
	bool flagRaised;
	Match2020AnchorArea expectedAnchoredArea;
	Match2020AnchorArea effectiveAnchoredArea;

protected:
	/**
	 * Gets the calculated current score for this match.
	 *
	 * @return integer the real-time calculated score
	 */
	virtual int getComputedScore() const override;

public:

	/**
	 * Constructor with the timer.
	 */
	MatchData2020(const MatchTimer& timer);

	/*
	 * Gets the current board state (for the bouys).
	 *
	 * @return Match2020BouyState the board state
	 */
	Match2020BouyState& getBoardState();

	/**
	 * Initializes which player we are playing.
	 *
	 * @param pcolor Match2020PlayerColor the player we play.
	 */
	void initializePlayer(Match2020PlayerColor pcolor);

	/**
	 * Initializes the random positions from the middle racks with the
	 * given pattern.
	 *
	 * @param pattern Code the random pattern to specify which bouy is where in
	 * 		the rack
	 */
	void initializeRandom(Code pattern);

	/**
	 * Initializes the anchor where the robots should be at the end of the
	 * match.
	 *
	 * @param area Match2020AnchorArea the expected anchor area
	 */
	void initializeAnchor(Match2020AnchorArea area);

	/**
	 * Asserts that the nearest windsock (manche a air) is uplifted.
	 */
	void toggleNearestWindsock();

	/**
	 * Asserts that the farest windsock (manche a air) is uplifted.
	 */
	void toggleFarestWindsock();

	/**
	 * Asserts that the lighthouse (phare) is setup.
	 */
	void setupLighthouse();

	/**
	 * Asserts that the lighthouse (phare) is uplifted.
	 */
	void upliftLighthouse();

	/**
	 * Asserts that the lodges (pavillons) were uplifted.
	 */
	void upliftLodges();

	/**
	 * At the end of the match, puts the anchor somewhere in any docking area.
	 *
	 * @param area Match2020AnchorArea the anchor area
	 */
	void anchorDown(Match2020AnchorArea area);

	/**
	 * At the end of the match, praise the flag.
	 *
	 */
	void raiseFlag();

	/**
	 * Updates the number of points to the match data
	 */
	void unload(Match2020BouyColor load, Match2020Chenal chenal, Match2020ChenalColor chenalColor);
	/**
	 * Updates the number of points to the match data
	 * @param [in] data sensors data
	 * @param [in] frontRear side to unload
	 * @param [in] chenal destination port
	 */
	void unload(BigBuoySensorData data, FrontRear frontRear, Match2020Chenal chenal);
	/**
	 * Updates the number of points to the match data assuming the given pattern
	 * is respected.
	 *
	 * @var Code the pattern to unload
	 * @var BigBottomBuoySensorData sensor data
	 * @var FrontRear whether to unload the front or the rear
	 * @var Match2020Chenal the chenal on which to unload
	 * @var Match2020ChenalColor the color of the chenal to unload
	 */
	void unloadPattern(Code pattern, BigBuoySensorData sensors, FrontRear frontOrRear, Match2020Chenal chenal, Match2020ChenalColor chenalColor);

	virtual void setScore(int value) override;
};

#endif /* MATCH_MATCHDATA2020_H_ */
