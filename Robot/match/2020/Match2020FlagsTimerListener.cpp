/*
 * Match2020FlagsTimerListener.cpp
 *
 *  Created on: 16 nov. 2019
 *      Author: emmanuel
 */

#include "Match2020FlagsTimerListener.h"

#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/io/LCDScreen.h"
#include "match/2020/Match2020.h"

Match2020FlagsTimerListener::Match2020FlagsTimerListener(Match2020 &match) :
		match(match) {
}

void Match2020FlagsTimerListener::onMatchTimeElapsed(const MatchTimer &timer) {
	(void) timer;

	match.setRaiseFlags();
}
