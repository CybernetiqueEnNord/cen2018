/*
 * Match2020AnchorAreaTimerListener.h
 *
 *  Created on: 30 mai 2020
 *      Author: emmanuel
 */

#ifndef MATCH_2020_MATCH2020ANCHORAREATIMERLISTENER_H_
#define MATCH_2020_MATCH2020ANCHORAREATIMERLISTENER_H_

#include "devices/timer/MatchTimer.h"

class Match2020;

class Match2020AnchorAreaTimerListener : public MatchTimerListener {
private:
	Match2020 &match;

protected:
	virtual void onMatchTimeElapsed(const MatchTimer &timer) override;

public:
	Match2020AnchorAreaTimerListener(Match2020 &match);
};

#endif /* MATCH_2020_MATCH2020ANCHORAREATIMERLISTENER_H_ */
