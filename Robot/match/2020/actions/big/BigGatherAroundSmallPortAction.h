/*
 * BigGatherAroundSmallPortAction.h
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_BIG_BIGGATHERAROUNDSMALLPORTACTION_H_
#define MATCH_2020_ACTIONS_BIG_BIGGATHERAROUNDSMALLPORTACTION_H_

#include "match/MatchAction.h"
#include "match/2020/MatchData2020.h"

class BigGatherAroundSmallPortAction : public MatchAction
{
private:
	MatchData2020 &matchData;

public:
	BigGatherAroundSmallPortAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_BIG_BIGGATHERAROUNDSMALLPORTACTION_H_ */
