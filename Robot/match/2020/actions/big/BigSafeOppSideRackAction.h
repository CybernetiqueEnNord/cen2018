/*
 * BigOppSideRackAction.h
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_BIG_BIGSAFEOPPSIDERACKACTION_H_
#define MATCH_2020_ACTIONS_BIG_BIGSAFEOPPSIDERACKACTION_H_

#include "match/MatchAction.h"
#include "match/2020/MatchData2020.h"

class BigSafeOppSideRackAction : public MatchAction
{
private:
	MatchData2020 &matchData;
	int frontTakeY = 2150;
	int rearTakeY = 2150;

public:
	BigSafeOppSideRackAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_BIG_BIGSAFEOPPSIDERACKACTION_H_ */
