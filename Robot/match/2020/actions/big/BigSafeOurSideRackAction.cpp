/*
 * BigOurSideRackAction.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "BigSafeOurSideRackAction.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"
#include "match/2020/MatchActionUtils2020.h"

BigSafeOurSideRackAction::BigSafeOurSideRackAction(MatchData2020 &matchData) :
	MatchAction("bgOurRack", Point(320, 850)), matchData(matchData)
{
	// nothing to do
}

void BigSafeOurSideRackAction::executeStep(Navigation &navigation, unsigned int step)
{
	extern BigActuator bigActuator;
	const RobotGeometry &robotGeometry = HostDevice::getHostData().getGeometry();

	switch(step)
	{
		case 0:
			bigActuator.setCode(Code::VVRVR_VRVRR_2); // TODO inject code
			break;

		case 1:
			frontTakeY = 850 + 75 * bigActuator.computeOffset(FrontRear::Front, FirstOrComplement::FirstOurSide);
			if(frontTakeY > 850)
			{
				navigation.rotateToOrientationDeciDegrees(900); // look at opponent
				navigation.moveTo(Point(260, frontTakeY));
			}
			else if(frontTakeY < 850)
			{
				navigation.rotateToOrientationDeciDegrees(900); // look at opponent
				navigation.moveToBackwards(Point(260, frontTakeY));
			}
			break;

		case 2:
			navigation.rotateToOrientationDeciDegrees(1800);
			break;

		case 3:
			bigActuator.readyToTake(FrontRear::Front, FirstOrComplement::FirstOurSide);
			break;

		case 4:
			navigation.repositionMM(300, robotGeometry.frontWidth, CURRENT, 1800);
			MatchActionUtils2020::waitForGobeletStabilisationInRack(navigation);
			break;

		case 5:
			bigActuator.hold();
			navigation.waitForDelay(200); // il faut prendre proprement
			navigation.moveMM(-20); // petite reculade pour laisser de la place
			bigActuator.raise();
			navigation.waitForDelay(200); // sinon ca touche a la reprise
			break;

		case 6:
			navigation.moveToBackwards(Point(260, frontTakeY));
			break;

		case 7:
			rearTakeY = 850 + 75 * bigActuator.computeOffset(FrontRear::Rear, FirstOrComplement::FirstOurSide);
			if(rearTakeY > frontTakeY)
			{
				navigation.rotateToOrientationDeciDegrees(900); // look at opponent
				navigation.moveTo(Point(260, rearTakeY));
			}
			else if(rearTakeY < frontTakeY)
			{
				navigation.rotateToOrientationDeciDegrees(900); // look at opponent
				navigation.moveToBackwards(Point(260, rearTakeY));
			}
			break;

		case 8:
			navigation.rotateToOrientationDeciDegrees(0);
			break;

		case 9:
			bigActuator.readyToTake(FrontRear::Rear, FirstOrComplement::FirstOurSide);
			break;

		case 10:
			navigation.repositionMM(-300, robotGeometry.rearWidth, CURRENT, 0);
			MatchActionUtils2020::waitForGobeletStabilisationInRack(navigation);
			break;

		case 11:
			bigActuator.hold();
			navigation.waitForDelay(200); // il faut prendre proprement
			navigation.moveMM(20); // petite reculade pour laisser de la place
			bigActuator.raise();
			navigation.waitForDelay(200); // sinon ca touche a la reprise
			break;

		case 12:
			navigation.moveTo(Point(320, rearTakeY));
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
