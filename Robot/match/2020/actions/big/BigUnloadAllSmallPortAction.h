/*
 * BigUnloadAllSmallPort.h
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_BIG_BIGUNLOADALLSMALLPORTACTION_H_
#define MATCH_2020_ACTIONS_BIG_BIGUNLOADALLSMALLPORTACTION_H_

#include "match/MatchAction.h"
#include "match/2020/MatchData2020.h"

class BigUnloadAllSmallPortAction : public MatchAction
{
private:
	MatchData2020 &matchData;

public:
	BigUnloadAllSmallPortAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_BIG_BIGUNLOADALLSMALLPORTACTION_H_ */
