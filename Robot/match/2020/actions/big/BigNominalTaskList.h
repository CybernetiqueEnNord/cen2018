/*
 * BigNominalTaskList.h
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_BIG_BIGNOMINALTASKLIST_H_
#define MATCH_2020_ACTIONS_BIG_BIGNOMINALTASKLIST_H_

#include "../../../MatchTask.h"
#include "../../../MatchTaskList.h"
#include "../../MatchData2020.h"

#define TASK_LIST_SIZE 6

class BigNominalTaskList : public MatchTaskList {
private:
	MatchData2020 &matchData;
	MatchTask (&tasks)[TASK_LIST_SIZE];
	bool fast = false;

	MatchTask (&createTasks())[TASK_LIST_SIZE];

public:
	BigNominalTaskList(MatchData2020 &matchData);
	virtual unsigned int getCount() const;
	virtual void setFast(bool fast);
	virtual MatchTask& getTaskAt(unsigned int index) const;
};

#endif /* MATCH_2020_ACTIONS_BIG_BIGNOMINALTASKLIST_H_ */
