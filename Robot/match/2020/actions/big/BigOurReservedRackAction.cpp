/*
 * BigOurReservedRackAction.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#include "BigOurReservedRackAction.h"

#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "BigSafeOppSideRackAction.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"
#include "match/2020/MatchActionUtils2020.h"

BigOurReservedRackAction::BigOurReservedRackAction(MatchData2020 &matchData) :
	MatchAction("bgResRack", Point(1600, 600)), matchData(matchData)
{
	// nothing to do
}

void BigOurReservedRackAction::executeStep(Navigation &navigation, unsigned int step)
{
	extern BigActuator bigActuator;
	const RobotGeometry &robotGeometry = HostDevice::getHostData().getGeometry();

	// d�sactiv�, le petit se charge de ce rack
	MatchAction::executeStep(navigation, step);
	return;

	switch(step)
	{
		case 0:
			bigActuator.setCode(Code::RVRVR); // TODO define our reserved side
			break;

		case 1:
			bigActuator.readyToTake(FrontRear::Front, FirstOrComplement::FirstOurSide);
			break;

		case 2:
			navigation.moveTo(Point(1600, robotGeometry.frontWidth + 20));
			break;

		case 3:
			navigation.repositionMM(200, CURRENT, robotGeometry.frontWidth, -900);
			MatchActionUtils2020::waitForGobeletStabilisationInRack(navigation);
			break;

		case 4:
			bigActuator.hold();
			MatchActionUtils2020::waitForHold(navigation);
			navigation.moveMM(-20);
			bigActuator.raise();
			navigation.moveMM(-200); // FIXME not needed
			break;

		case 5:
			navigation.moveToBackwards(Point(1600, 600));
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
