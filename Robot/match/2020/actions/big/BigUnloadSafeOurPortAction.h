/*
 * BigUnloadSafeOurPortAction.h
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_BIG_BIGUNLOADSAFEOURPORTACTION_H_
#define MATCH_2020_ACTIONS_BIG_BIGUNLOADSAFEOURPORTACTION_H_

#include "match/MatchAction.h"
#include "match/2020/MatchData2020.h"

class BigUnloadSafeOurPortAction : public MatchAction
{
private:
	MatchData2020 &matchData;

public:
	BigUnloadSafeOurPortAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_BIG_BIGUNLOADSAFEOURPORTACTION_H_ */
