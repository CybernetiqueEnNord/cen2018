/*
 * BigOppSideRackAction.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "BigSafeOppSideRackAction.h"
#include "BigActionsConstants.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"
#include "match/2020/MatchActionUtils2020.h"

BigSafeOppSideRackAction::BigSafeOppSideRackAction(MatchData2020 &matchData) :
		MatchAction("bgOppRack", POINT_OPPONENT_RACK), matchData(matchData) {
	//nothing to do
}

void BigSafeOppSideRackAction::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;
	const RobotGeometry &robotGeometry = HostDevice::getHostData().getGeometry();
	Point point;

	switch (step) {
		case 0:
			bigActuator.setCode(Code::VVRVR_VRVRR_2); // TODO inject code
			break;

		case 1:
			point = POINT_OPPONENT_RACK;
			frontTakeY = point.y + 75 * bigActuator.computeOffset(FrontRear::Front, FirstOrComplement::ComplementOponent);
			if (frontTakeY > point.y) {
				navigation.rotateToOrientationDeciDegrees(900); // look at opponent
				navigation.moveTo(Point(point.x, frontTakeY));
			} else if (frontTakeY < point.y) {
				navigation.rotateToOrientationDeciDegrees(900); // look at opponent
				navigation.moveToBackwards(Point(point.x, frontTakeY));
			}
			break;

		case 2:
			navigation.rotateToOrientationDeciDegrees(1800);
			break;

		case 3:
			bigActuator.readyToTake(FrontRear::Front, FirstOrComplement::ComplementOponent);
			break;

		case 4:
			navigation.repositionMM(300, robotGeometry.frontWidth, CURRENT, 1800);
			MatchActionUtils2020::waitForGobeletStabilisationInRack(navigation);
			break;

		case 5:
			bigActuator.hold();
			navigation.waitForDelay(200); // il faut prendre proprement
			navigation.moveMM(-20); // petite reculade pour laisser de la place
			bigActuator.raise();
			navigation.waitForDelay(200); // sinon ca touche a la reprise
			break;

		case 6:
			navigation.moveToBackwards(Point(260, frontTakeY));
			break;

		case 7:
			rearTakeY = 2150 + 75 * bigActuator.computeOffset(FrontRear::Rear, FirstOrComplement::ComplementOponent);
			if (rearTakeY > frontTakeY) {
				navigation.rotateToOrientationDeciDegrees(900); // look at opponent
				navigation.moveTo(Point(260, rearTakeY));
			} else if (rearTakeY < frontTakeY) {
				navigation.rotateToOrientationDeciDegrees(900); // look at opponent
				navigation.moveToBackwards(Point(260, rearTakeY));
			}
			break;

		case 8:
			navigation.rotateToOrientationDeciDegrees(0);
			break;

		case 9:
			bigActuator.readyToTake(FrontRear::Rear, FirstOrComplement::ComplementOponent);
			MatchActionUtils2020::waitForGobeletStabilisationInRack(navigation);
			break;

		case 10:
			navigation.repositionMM(-300, robotGeometry.rearWidth, CURRENT, 0);
			break;

		case 11:
			bigActuator.hold();
			navigation.waitForDelay(200); // il faut prendre proprement
			navigation.moveMM(20); // petite reculade pour laisser de la place
			bigActuator.raise();
			navigation.waitForDelay(200); // sinon ca touche a la reprise
			break;

		case 12:
			navigation.moveTo(Point(320, rearTakeY));
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
