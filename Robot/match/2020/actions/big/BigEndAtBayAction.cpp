/*
 * BigEndAtBayAction.cpp
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#include "BigEndAtBayAction.h"
#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"
#include "devices/databus/2020/DataBus2020.h"

BigEndAtBayAction::BigEndAtBayAction(MatchData2020 &matchData) :
		MatchAction("bgEndAtBay", Point(400, 1500)), matchData(matchData) {
	target = Match2020AnchorArea::UNKNOWN;
}

void BigEndAtBayAction::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;
	const RobotGeometry &robotGeometry = HostDevice::getHostData().getGeometry();

	extern DataBus2020 dataBus;
	if (step == 0) {
		if (!dataBus.queryAnchorArea(target)) {
			target = Match2020AnchorArea::UNKNOWN;
		}
		navigation.waitForDelay(matchData.getTimer().getRemainingTimeMS() - 10000);
	}

	if (step == 0) {
		bigActuator.releaseBottomBuoyCatcher(FrontRear::Front);
		navigation.waitForDelay(250);
		bigActuator.releaseBottomBuoyCatcher(FrontRear::Rear);
		navigation.waitForDelay(250);
		/*bigActuator.releaseTopBuoyLifter(FrontRear::Front);
		navigation.waitForDelay(250);
		bigActuator.releaseTopBuoyLifter(FrontRear::Rear);
		 navigation.waitForDelay(250);*/
	}

	if (target == Match2020AnchorArea::NORTH) {

		switch (step) {
			case 3:
				navigation.moveTo(Point(800, 460 + robotGeometry.lateralWidth));
				break;

			case 4:
				navigation.moveTo(Point(480, 460 + robotGeometry.lateralWidth));
				break;

			case 5:
				bigActuator.setArmLighthouse(true);
				break;
		}
	} else {
		switch (step) {
			case 3:
				navigation.moveTo(Point(800, 460 + robotGeometry.lateralWidth));
				break;

			case 4:
				navigation.moveTo(Point(1110, 460 + robotGeometry.lateralWidth));
				break;

			case 5:
				bigActuator.setArmEndAtBay(true);
				break;
		}
	}

	switch (step) {
		case 0:
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		case 5:
			break;
		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
