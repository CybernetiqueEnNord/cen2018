/*
 * BigAggrOppSideRackAction.h
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_BIG_BIGAGGROPPSIDERACKACTION_H_
#define MATCH_2020_ACTIONS_BIG_BIGAGGROPPSIDERACKACTION_H_

#include "../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "../../../MatchAction.h"
#include "../../MatchData2020.h"

class BigAggrOppSideRackAction : public MatchAction
{
private:
	MatchData2020 &matchData;
	int frontTakeY = 2150;
	int rearTakeY = 2150;
	Code pattern = Code::Undefined;

public:
	BigAggrOppSideRackAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_BIG_BIGAGGROPPSIDERACKACTION_H_ */
