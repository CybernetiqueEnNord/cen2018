/*
 * BigUnloadSafeOurPortAction.cpp
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#include "BigUnloadSafeOurPortAction.h"
#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"
#include "match/2020/MatchActionUtils2020.h"

BigUnloadSafeOurPortAction::BigUnloadSafeOurPortAction(MatchData2020 &matchData) :
		MatchAction("bgUnload2", Point(800, 700)), matchData(matchData) {
// nothing to do
}

void BigUnloadSafeOurPortAction::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;

	switch (step) {
		case 0:
			navigation.moveTo(Point(800, 450));
			break;

		case 1: {
			BigBuoySensorData sensors = bigActuator.getTopBuoyData();
			bigActuator.setDownTopBuoyLifter(FrontRear::Front);
			navigation.moveMM(75);
			bigActuator.releaseTopBuoyLifter(FrontRear::Front);
			matchData.unloadPattern(Code::RVRVR, sensors, FrontRear::Front, Match2020Chenal::STARTING_PORT, Match2020ChenalColor::CHENAL_NONE);
			sensors = bigActuator.getBottomBuoyData();
			bigActuator.releaseBottomBuoyCatcher(FrontRear::Front);
			matchData.unload(sensors, FrontRear::Front, Match2020Chenal::STARTING_PORT);
			break;
		}

		case 2:
			navigation.moveToBackwards(Point(800, 500));
			break;

		case STEP_FINALIZATION:
			bigActuator.armBottomBuoyCatcher(FrontRear::Front);
			bigActuator.armBottomBuoyCatcher(FrontRear::Rear);
			bigActuator.raise(false);
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}

