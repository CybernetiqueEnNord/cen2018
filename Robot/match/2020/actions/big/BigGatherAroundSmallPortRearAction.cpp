/*
 * BigGatherAroundSmallPortRearAction.cpp
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#include "BigGatherAroundSmallPortRearAction.h"
#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"


BigGatherAroundSmallPortRearAction::BigGatherAroundSmallPortRearAction(MatchData2020 &matchData) :
	MatchAction("bgGatherSPR", Point(1500, 1720)), matchData(matchData)
{
	// nothing to add
}

void BigGatherAroundSmallPortRearAction::executeStep(Navigation &navigation, unsigned int step)
{
	extern BigActuator bigActuator;
	const RobotGeometry &robotGeometry = HostDevice::getHostData().getGeometry();

	switch(step)
	{
		case 0:
			navigation.rotateToOrientationDeciDegrees(1800);
			break;

		case 1:
			bigActuator.armBottomBuoyCatcher(FrontRear::Front);
			navigation.waitForDelay(200);
			bigActuator.armBottomBuoyCatcher(FrontRear::Rear);
			navigation.waitForDelay(200);
			break;

		case 2:
			navigation.moveToBackwards(Point(1800, 1720));
			break;

		case 3:
			navigation.repositionMM(-100, robotGeometry.rearWidth, CURRENT, 1800);
			break;

		case 4:
			navigation.moveTo(Point(1500, 1720));
			break;

		case 5:
			navigation.moveTo(Point(1500, 1880));
			break;

		case 6:
			navigation.moveToBackwards(Point(1800, 1880));
			break;

		case 7:
			navigation.repositionMM(-100, robotGeometry.rearWidth, CURRENT, 1800);
			break;

		case 8:
			navigation.moveTo(Point(1500, 1720));
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
