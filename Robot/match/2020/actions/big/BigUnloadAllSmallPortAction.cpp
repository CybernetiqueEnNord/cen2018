/*
 * BigUnloadAllSmallPort.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#include "BigUnloadAllSmallPortAction.h"
#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"


BigUnloadAllSmallPortAction::BigUnloadAllSmallPortAction(MatchData2020 &matchData) :
	MatchAction("bgUnloadSm", Point(1600, 1800)), matchData(matchData)
{
	// nothing to do
}

void BigUnloadAllSmallPortAction::executeStep(Navigation &navigation, unsigned int step)
{
	extern BigActuator bigActuator;
	const RobotGeometry &robotGeometry = HostDevice::getHostData().getGeometry();

	switch(step)
	{
		case 0:
			navigation.moveTo(Point(1800, 1800));
			navigation.repositionMM(100, 2000 - robotGeometry.rearWidth, CURRENT, 0);
			break;

		case 1:
			bigActuator.releaseTopBuoyLifter(FrontRear::Front);
			navigation.waitForDelay(500);
			break;

		case 2:
			navigation.moveToBackwards(Point(1600, 1800));
			break;

		case 3:
			navigation.rotateToOrientationDeciDegrees(1800);
			break;

		case 4:
			navigation.moveToBackwards(Point(1700, 1800));
			break;

		case 5:
			bigActuator.releaseTopBuoyLifter(FrontRear::Rear);
			navigation.waitForDelay(500);
			bigActuator.releaseBottomBuoyCatcher(FrontRear::Rear);
			navigation.waitForDelay(500);
			break;

		case 6:
			navigation.moveTo(Point(1600, 1800));
			break;

		case 7:
			navigation.rotateToOrientationDeciDegrees(0);
			break;

		case 8:
			bigActuator.releaseBottomBuoyCatcher(FrontRear::Front);
			navigation.waitForDelay(500);
			break;

		case 9:
			navigation.moveToBackwards(Point(1500, 1800));
			break;

		case 10:
			bigActuator.armBottomBuoyCatcher(FrontRear::Front);
			navigation.waitForDelay(300);
			bigActuator.armBottomBuoyCatcher(FrontRear::Rear);
			navigation.waitForDelay(300);
			bigActuator.raise();
			navigation.waitForDelay(300);
			break;

		case STEP_FINALIZATION:
			bigActuator.armBottomBuoyCatcher(FrontRear::Front);
			navigation.waitForDelay(300);
			bigActuator.armBottomBuoyCatcher(FrontRear::Rear);
			navigation.waitForDelay(300);
			bigActuator.raise();
			navigation.waitForDelay(300);
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
