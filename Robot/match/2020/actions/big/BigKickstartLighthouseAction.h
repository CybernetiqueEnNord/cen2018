/*
 * BigKickstartLighthouseAction.h
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_BIG_BIGKICKSTARTLIGHTHOUSEACTION_H_
#define MATCH_2020_ACTIONS_BIG_BIGKICKSTARTLIGHTHOUSEACTION_H_

#include "match/MatchAction.h"
#include "match/2020/MatchData2020.h"

class BigKickstartLighthouseAction : public MatchAction
{
private:
	MatchData2020 &matchData;
	bool aggressive;
	bool fast;

public:
	BigKickstartLighthouseAction(MatchData2020 &matchData, bool aggressive = true, bool fast = true);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_BIG_BIGKICKSTARTLIGHTHOUSEACTION_H_ */
