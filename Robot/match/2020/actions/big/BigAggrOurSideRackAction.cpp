/*
 * BigAggrOurSideRackAction.cpp
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#include "BigAggrOurSideRackAction.h"
#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"
#include "match/2020/MatchActionUtils2020.h"

BigAggrOurSideRackAction::BigAggrOurSideRackAction(MatchData2020 &matchData) :
		MatchAction("bgOurRack", Point(320, 600)), matchData(matchData) {
	// nothing to do
}

void BigAggrOurSideRackAction::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;
	const RobotGeometry &robotGeometry = HostDevice::getHostData().getGeometry();

	switch (step) {
		case 0:
			//Code defined in Opp side action

			// d�placement pour d�gager le rack
			//navigation.moveTo(Point(210, 600));
			break;

		case 1:
			//navigation.moveTo(Point(210, 1100));
			break;

		case 2:
			// on est toujours au-del� du rack, on se place en marche arri�re
			frontTakeY = 850 + 75 * bigActuator.computeOffset(FrontRear::Front, FirstOrComplement::ComplementOurSide);
			navigation.moveToBackwards(Point(260, frontTakeY));
			break;

		case 3:
			navigation.rotateToOrientationDeciDegrees(1800);
			break;

		case 4:
			bigActuator.readyToTake(FrontRear::Front, FirstOrComplement::ComplementOurSide);
			break;

		case 5: {
			const Position &current = navigation.getControl().getPosition();
			navigation.moveTo(Point(robotGeometry.frontWidth + 20, current.y));
			break;
		}

		case 6:
			navigation.repositionMM(300, robotGeometry.frontWidth, CURRENT, 1800);
			MatchActionUtils2020::waitForGobeletStabilisationInRack(navigation);
			break;

		case 7:
			bigActuator.hold();
			MatchActionUtils2020::waitForHold(navigation);
			navigation.moveMM(-20); // petite reculade pour laisser de la place
			bigActuator.raise();
			break;

		case 8:
			navigation.moveToBackwards(Point(260, frontTakeY));
			break;

		case 9:
			rearTakeY = 850 + 75 * bigActuator.computeOffset(FrontRear::Rear, FirstOrComplement::ComplementOurSide);
			if (rearTakeY > frontTakeY) {
				navigation.rotateToOrientationDeciDegrees(900); // look at opponent
				navigation.moveTo(Point(260, rearTakeY));
			} else if (rearTakeY < frontTakeY) {
				navigation.rotateToOrientationDeciDegrees(900); // look at opponent
				navigation.moveToBackwards(Point(260, rearTakeY));
			}
			break;

		case 10:
			navigation.rotateToOrientationDeciDegrees(0);
			break;

		case 11:
			bigActuator.readyToTake(FrontRear::Rear, FirstOrComplement::ComplementOurSide);
			break;

		case 12: {
			const Position &current = navigation.getControl().getPosition();
			navigation.moveToBackwards(Point(robotGeometry.rearWidth + 20, current.y));
			break;
		}

		case 13:
			navigation.repositionMM(-300, robotGeometry.rearWidth, CURRENT, 0);
			MatchActionUtils2020::waitForGobeletStabilisationInRack(navigation);
			break;

		case 14:
			bigActuator.hold();
			MatchActionUtils2020::waitForHold(navigation);
			navigation.moveMM(20); // petite reculade pour laisser de la place
			bigActuator.raise();
			break;

		case 15:
			navigation.moveTo(Point(320, rearTakeY));
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
