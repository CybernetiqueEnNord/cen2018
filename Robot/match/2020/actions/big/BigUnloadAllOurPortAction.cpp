/*
 * BigUnloadAllOurPort.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "BigUnloadAllOurPortAction.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"
#include "devices/databus/2020/DataBus2020.h"
#include "match/2020/MatchActionUtils2020.h"

BigUnloadAllOurPortAction::BigUnloadAllOurPortAction(MatchData2020 &matchData) :
		MatchAction("bgUnload", Point(800, 700)), matchData(matchData) {
	// nothing to do
}

void BigUnloadAllOurPortAction::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;
	BigBuoySensorData sensors;
	const RobotGeometry &robotGeometry = HostDevice::getHostData().getGeometry();

	switch (step) {
		case 0:
			/*
			 // Poussage du gobelet laiss� au d�part avec le bras
			 navigation.rotateToOrientationDeciDegrees(-900);
			 bigActuator.setArmPushBuoy(true);
			 navigation.getControl().setSpeedIndex(SPEED_BIG_SLOW);
			 navigation.moveTo(Point(800,445));
			 bigActuator.setArmPushBuoy(false);
			 navigation.getControl().setSpeedIndex(SPEED_BIG_NOMINAL);
			 */
			navigation.moveTo(Point(800, 225));
			break;

		case 1:
			// d�sactivation de la d�tection dans le port
			navigation.getControl().obstacleAvoidance = false;
			navigation.rotateToOrientationDeciDegrees(0);
			break;

		case 2:
			navigation.moveTo(Point(900, 225));
			break;

		case 3:
			sensors = bigActuator.getTopBuoyData();
			bigActuator.releaseTopBuoyLifter(FrontRear::Front);
			matchData.unloadPattern(bigActuator.getCode(), sensors, FrontRear::Front, Match2020Chenal::STARTING_PORT, Match2020ChenalColor::CHENAL_RED);
			break;

		case 4:
			navigation.moveToBackwards(Point(700, 225));
			bigActuator.raise();
			navigation.waitForDelay(500);
			break;

		case 5:
			sensors = bigActuator.getTopBuoyData();
			bigActuator.releaseTopBuoyLifter(FrontRear::Rear);
			matchData.unloadPattern(bigActuator.getCode(), sensors, FrontRear::Rear, Match2020Chenal::STARTING_PORT, Match2020ChenalColor::CHENAL_GREEN);
			break;

		case 6:
			navigation.moveTo(Point(800, 225));
			bigActuator.raise();
			break;

		case 7:
			navigation.rotateToOrientationDeciDegrees(900);
			break;

		case 8:
			navigation.moveToBackwards(Point(800, robotGeometry.rearWidth + 10));
			navigation.repositionMM(-200, CURRENT, robotGeometry.rearWidth, 900);
			break;

		case 9:
			sensors = bigActuator.getBottomBuoyData();
			bigActuator.releaseBottomBuoyCatcher(FrontRear::Rear);
			matchData.unload(sensors, FrontRear::Rear, Match2020Chenal::STARTING_PORT);
			break;

		case 10:
			navigation.moveTo(Point(800, 540));
			break;

		case 11:
			navigation.rotateToOrientationDeciDegrees(-900);
			bigActuator.setArmPushBuoy(true);
			navigation.getControl().setSpeedIndex(SPEED_BIG_SLOW);
			navigation.moveTo(Point(800, 445));
			bigActuator.setArmPushBuoy(false);
			// Point du gobelet pouss�
			matchData.unload(Match2020BouyColor::BOUY_GREEN, Match2020Chenal::STARTING_PORT, Match2020ChenalColor::CHENAL_GREEN);
			navigation.getControl().setSpeedIndex(SPEED_BIG_NOMINAL);
			break;

		case 12:
			navigation.moveTo(Point(800, 175));
			break;

		case 13:
			sensors = bigActuator.getBottomBuoyData();
			bigActuator.releaseBottomBuoyCatcher(FrontRear::Front);
			matchData.unload(sensors, FrontRear::Front, Match2020Chenal::STARTING_PORT);
			break;

		case 14:
			navigation.getControl().obstacleAvoidance = true;
			navigation.moveToBackwards(Point(800, 700));
			break;

		case STEP_FINALIZATION:
			bigActuator.armBottomBuoyCatcher(FrontRear::Front);
			bigActuator.armBottomBuoyCatcher(FrontRear::Rear);
			bigActuator.raise(false);
			navigation.getControl().obstacleAvoidance = true;
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
