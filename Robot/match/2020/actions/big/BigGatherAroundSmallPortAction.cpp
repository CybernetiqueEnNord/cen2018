/*
 * BigGatherAroundSmallPortAction.cpp
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#include "BigGatherAroundSmallPortAction.h"
#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"

BigGatherAroundSmallPortAction::BigGatherAroundSmallPortAction(MatchData2020 &matchData) :
	MatchAction("bgGatherSP", Point(1500, 1720)), matchData(matchData)
{
	// nothing to add
}

void BigGatherAroundSmallPortAction::executeStep(Navigation &navigation, unsigned int step)
{
	extern BigActuator bigActuator;
	const RobotGeometry &robotGeometry = HostDevice::getHostData().getGeometry();

	switch(step)
	{
		case 0:
			bigActuator.armBottomBuoyCatcher(FrontRear::Front);
			navigation.waitForDelay(200);
			bigActuator.armBottomBuoyCatcher(FrontRear::Rear);
			navigation.waitForDelay(200);
			break;

		case 1:
			navigation.moveTo(Point(1800, 1720));
			break;

		case 2:
			navigation.repositionMM(100, robotGeometry.frontWidth, CURRENT, 0);
			break;

		case 3:
			navigation.moveToBackwards(Point(1500, 1720));
			break;

		case 4:
			navigation.moveTo(Point(1500, 1880));
			break;

		case 5:
			navigation.moveTo(Point(1800, 1880));
			break;

		case 6:
			navigation.repositionMM(100, robotGeometry.frontWidth, CURRENT, 0);
			break;

		case 7:
			navigation.moveToBackwards(Point(1500, 1880));
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
