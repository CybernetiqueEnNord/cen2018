/*
 * BigKickstartLighthouseAction.cpp
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#include "BigKickstartLighthouseAction.h"
#include "BigActionsConstants.h"
#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"

BigKickstartLighthouseAction::BigKickstartLighthouseAction(MatchData2020 &matchData, bool aggressive, bool fast) :
		MatchAction("bgKSLight", Point(0, 0)), matchData(matchData), aggressive(aggressive), fast(fast) {
	// nothing to add
}

void BigKickstartLighthouseAction::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;
	Point point;

	switch (step) {
		case 0:
			bigActuator.setArmLighthouse(true);
			navigation.moveMM(20);
			break;

		case 1:
			navigation.moveToArcWithOrientation(POINT_INITIAL_ARC_END, 700);
			break;

		case 2:
			matchData.upliftLighthouse();
			bigActuator.setArmLighthouse(false);
			break;

		case 3:
			// Trajectoire agressive
			if (!aggressive) {
				terminated = true;
				break;
			}
			// port de l'adversaire
			point = POINT_OPPONENT_RACK;
			// on s'�loigne du point d'arriv�e vers l'int�rieur de la table
			point.x += 500;
			if (!fast) {
				navigation.getControl().setSpeedIndex(SPEED_BIG_NOMINAL);
			}
			navigation.moveToArcWithOrientation(point, 1100);
			if (!fast) {
			navigation.getControl().setSpeedIndex(SPEED_BIG_FAST);
			}
			break;

		case 4:
			point = navigation.getControl().getPosition();
			navigation.moveTo(Point(220, point.y));
			break;

		case 5:
			navigation.rotateToOrientationDeciDegrees(900);
			break;

		case 6:
			navigation.moveMM(100);
			break;

		case 7:
			navigation.moveToBackwards(POINT_OPPONENT_RACK);
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
