/*
 * BigUnloadAllOurPort.h
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_BIG_BIGUNLOADALLOURPORTACTION_H_
#define MATCH_2020_ACTIONS_BIG_BIGUNLOADALLOURPORTACTION_H_

#include "match/MatchAction.h"
#include "match/2020/MatchData2020.h"
#include "../Commons/i2c/ActuatorBig2020Commands.h"

class BigUnloadAllOurPortAction : public MatchAction
{
private:
	MatchData2020 &matchData;
	Code pattern = Code::Undefined;

public:
	BigUnloadAllOurPortAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_BIG_BIGUNLOADALLOURPORTACTION_H_ */
