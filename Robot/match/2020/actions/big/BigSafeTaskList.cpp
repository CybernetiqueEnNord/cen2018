/*
 * BigSafeTaskList.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#include "BigSafeTaskList.h"

#include "../../../../utils/Arrays.h"
#include "../../../../utils/Pointer.h"
#include "BigAggrOppSideRackAction.h"
#include "BigAggrOurSideRackAction.h"
#include "BigEndAtBayAction.h"
#include "BigGatherAroundSmallPortAction.h"
#include "BigGatherAroundSmallPortRearAction.h"
#include "BigGatherOurSideTableAction.h"
#include "BigKickstartLighthouseAction.h"
#include "BigOurReservedRackAction.h"
#include "BigSafeOppSideRackAction.h"
#include "BigSafeOurSideRackAction.h"
#include "BigUnloadAllOurPortAction.h"
#include "BigUnloadAllSmallPortAction.h"
#include "BigUnloadSafeOurPortAction.h"
//#include "devices/actuators/2020/big/ActuatorSimpleWrist.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "match/SingleActionList.h"

BigSafeTaskList::BigSafeTaskList(MatchData2020 &matchData) :
		MatchTaskList(), matchData(matchData), tasks(createTasks()) {
	// nothing to do
}

unsigned int BigSafeTaskList::getCount() const {
	return ARRAY_SIZE(tasks);
}

MatchTask& BigSafeTaskList::getTaskAt(unsigned int index) const {
	return tasks[index];
}

MatchTask (& BigSafeTaskList::createTasks())[TASK_LIST_SIZE_SAFE] {

			static MatchElement lighthouse;
			static BigKickstartLighthouseAction klighthouseAction(matchData, false); //false: pas agressive
			static SingleActionList klighthouseActions(klighthouseAction);
			static MatchTarget klighthouseTarget(klighthouseActions, lighthouse);
			static MatchTask klighthouseTask("lighthouse", 15, 1, Pointer<MatchTarget>(&klighthouseTarget));

			static MatchElement oppRack;
			static MatchElement ourRack;

			static BigAggrOurSideRackAction aggrOurSideRackAction(matchData);
			static SingleActionList aggrOurSideRackActions(aggrOurSideRackAction);
			static MatchTarget aggrOurSideRackTarget(aggrOurSideRackActions, ourRack);
			static MatchTask aggrOurSideRackTask("AggrOurRack", 15, 7, Pointer<MatchTarget>(&aggrOurSideRackTarget));

			/*
			 static BigAggrOppSideRackAction aggrOppSideRackAction(matchData);
			 static SingleActionList aggrOppSideRackActions(aggrOppSideRackAction);
			 static MatchTarget aggrOppSideRackTarget(aggrOppSideRackActions, oppRack);
			 static MatchTask aggrOppSideRackTask("AggOppRack", 15, 10, Pointer<MatchTarget>(&aggrOppSideRackTarget));
			 */

			static MatchElement ourPort;
			static BigUnloadAllOurPortAction ourPortUnloadAction(matchData);
			static SingleActionList ourPortUnloadActions(ourPortUnloadAction);
			static MatchTarget ourPortUnloadTarget(ourPortUnloadActions, ourPort);
			static MatchTask ourPortUnloadTask("unload", 15, 30, Pointer<MatchTarget>(&ourPortUnloadTarget));

			static MatchElement middle;
			static BigGatherOurSideTableAction gatherOurSideTableAction(matchData);
			static SingleActionList gatherOurSideTableActions(gatherOurSideTableAction);
			static MatchTarget gatherOurSideTableTarget(gatherOurSideTableActions, middle);
			static MatchTask gatherOurSideTableTask("table", 15, 40, Pointer<MatchTarget>(&gatherOurSideTableTarget));

			static MatchTask tasks[TASK_LIST_SIZE_SAFE] = { klighthouseTask, aggrOurSideRackTask, /*aggrOppSideRackTask,*/ ourPortUnloadTask,
		gatherOurSideTableTask,
//		ourReservedRackTask,
//		unloadSafeOurPortTask,
//		gatherOurSideTableTask,
//		gatherSmallPortRearTask,
//		gatherSmallPortTask,
//		unloadSmallPortTask,
//		endAtBayTask,
					};

			return tasks;
		}
