/*
 * BigNominalTaskList.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#include "BigNominalTaskList.h"

#include "../../../../utils/Arrays.h"
#include "../../../../utils/Pointer.h"
#include "BigAggrOppSideRackAction.h"
#include "BigAggrOurSideRackAction.h"
#include "BigEndAtBayAction.h"
#include "BigGatherAroundSmallPortAction.h"
#include "BigGatherAroundSmallPortRearAction.h"
#include "BigGatherOurSideTableAction.h"
#include "BigKickstartLighthouseAction.h"
#include "BigOurReservedRackAction.h"
#include "BigSafeOppSideRackAction.h"
#include "BigSafeOurSideRackAction.h"
#include "BigUnloadAllOurPortAction.h"
#include "BigUnloadAllSmallPortAction.h"
#include "BigUnloadSafeOurPortAction.h"
//#include "devices/actuators/2020/big/ActuatorSimpleWrist.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "match/SingleActionList.h"

BigNominalTaskList::BigNominalTaskList(MatchData2020 &matchData) :
	MatchTaskList(), matchData(matchData), tasks(createTasks())
{
	// nothing to do
}

unsigned int BigNominalTaskList::getCount() const {
	return ARRAY_SIZE(tasks);
}

MatchTask& BigNominalTaskList::getTaskAt(unsigned int index) const {
	return tasks[index];
}

void BigNominalTaskList::setFast(bool fast) {
	this->fast = fast;
}

MatchTask (& BigNominalTaskList::createTasks())[TASK_LIST_SIZE] {
//	extern BigActuator bigActuator;

//	static ActuatorSimpleWrist wf1(Match2020Wrist::FRONT_WRIST_LEFTMOST,      Match2020ServoState::UP);
//	static ActuatorSimpleWrist wf2(Match2020Wrist::FRONT_WRIST_LEFT,          Match2020ServoState::UP);
//	static ActuatorSimpleWrist wf3(Match2020Wrist::FRONT_WRIST_CENTER,        Match2020ServoState::UP);
//	static ActuatorSimpleWrist wf4(Match2020Wrist::FRONT_WRIST_RIGHT,         Match2020ServoState::UP);
//	static ActuatorSimpleWrist wf5(Match2020Wrist::FRONT_WRIST_RIGHTMOST,     Match2020ServoState::UP);
//	static ActuatorSimpleWrist wb1(Match2020Wrist::REAR_WRIST_LEFTMOST,       Match2020ServoState::UP);
//	static ActuatorSimpleWrist wb2(Match2020Wrist::REAR_WRIST_LEFT,           Match2020ServoState::UP);
//	static ActuatorSimpleWrist wb3(Match2020Wrist::REAR_WRIST_CENTER,         Match2020ServoState::UP);
//	static ActuatorSimpleWrist wb4(Match2020Wrist::REAR_WRIST_RIGHT,          Match2020ServoState::UP);
//	static ActuatorSimpleWrist wb5(Match2020Wrist::REAR_WRIST_RIGHTMOST,      Match2020ServoState::UP);
//	static ActuatorSimpleWrist bf1(Match2020Wrist::FRONT_BOTTOM_LEFT,         Match2020ServoState::UP);
//	static ActuatorSimpleWrist bf2(Match2020Wrist::FRONT_BOTTOM_CENTER_LEFT,  Match2020ServoState::UP);
//	static ActuatorSimpleWrist bf3(Match2020Wrist::FRONT_BOTTOM_CENTER_RIGHT, Match2020ServoState::UP);
//	static ActuatorSimpleWrist bf4(Match2020Wrist::FRONT_BOTTOM_RIGHT,        Match2020ServoState::UP);
//	static ActuatorSimpleWrist bb1(Match2020Wrist::REAR_BOTTOM_LEFT,          Match2020ServoState::UP);
//	static ActuatorSimpleWrist bb2(Match2020Wrist::REAR_BOTTOM_CENTER_LEFT,   Match2020ServoState::UP);
//	static ActuatorSimpleWrist bb3(Match2020Wrist::REAR_BOTTOM_CENTER_RIGHT,  Match2020ServoState::UP);
//	static ActuatorSimpleWrist bb4(Match2020Wrist::REAR_BOTTOM_RIGHT,         Match2020ServoState::UP);

	// on est sur le gros, il faut donc d'abord avoir un modele d'actionneur
	// front et back

	static MatchElement lighthouse;
			static BigKickstartLighthouseAction klighthouseAction(matchData, true);
	static SingleActionList klighthouseActions(klighthouseAction);
	static MatchTarget klighthouseTarget(klighthouseActions, lighthouse);
	static MatchTask klighthouseTask("lighthouse", 15, 1, Pointer<MatchTarget>(&klighthouseTarget));

	static MatchElement oppRack;
	static MatchElement ourRack;

	static BigAggrOppSideRackAction aggrOppSideRackAction(matchData);
	static SingleActionList aggrOppSideRackActions(aggrOppSideRackAction);
	static MatchTarget aggrOppSideRackTarget(aggrOppSideRackActions, oppRack);
	static MatchTask aggrOppSideRackTask("AggOppRack", 15, 5, Pointer<MatchTarget>(&aggrOppSideRackTarget));

	static BigAggrOurSideRackAction aggrOurSideRackAction(matchData);
	static SingleActionList aggrOurSideRackActions(aggrOurSideRackAction);
	static MatchTarget aggrOurSideRackTarget(aggrOurSideRackActions, ourRack);
	static MatchTask aggrOurSideRackTask("AggrOurRack", 15, 7, Pointer<MatchTarget>(&aggrOurSideRackTarget));

	static BigSafeOurSideRackAction ourSideRackAction(matchData);
	static SingleActionList ourSideRackActions(ourSideRackAction);
	static MatchTarget ourSideRackTarget(ourSideRackActions, ourRack);
	static MatchTask ourSideRackTask("ourSideRack", 15, 15, Pointer<MatchTarget>(&ourSideRackTarget));

	static BigSafeOppSideRackAction oppSideRackAction(matchData);
	static SingleActionList oppSideRackActions(oppSideRackAction);
	static MatchTarget oppSideRackTarget(oppSideRackActions, oppRack);
	static MatchTask oppSideRackTask("oppSideRack", 15, 22, Pointer<MatchTarget>(&oppSideRackTarget));

	static MatchElement ourPort;
	static BigUnloadAllOurPortAction ourPortUnloadAction(matchData);
	static SingleActionList ourPortUnloadActions(ourPortUnloadAction);
	static MatchTarget ourPortUnloadTarget(ourPortUnloadActions, ourPort);
	static MatchTask ourPortUnloadTask("unload", 15, 30, Pointer<MatchTarget>(&ourPortUnloadTarget));

//			static MatchElement middle;
//			static BigGatherOurSideTableAction gatherOurSideTableAction(matchData);
//			static SingleActionList gatherOurSideTableActions(gatherOurSideTableAction);
//			static MatchTarget gatherOurSideTableTarget(gatherOurSideTableActions, middle);
//			static MatchTask gatherOurSideTableTask("table", 15, 40, Pointer<MatchTarget>(&gatherOurSideTableTarget));

//	static MatchElement reservedRack;
//	static BigOurReservedRackAction ourReservedRackAction(matchData);
//	static SingleActionList ourReservedRackActions(ourReservedRackAction);
//	static MatchTarget ourReservedRackTarget(ourReservedRackActions, reservedRack);
//	static MatchTask ourReservedRackTask("reservedRack", 5, 40, Pointer<MatchTarget>(&ourReservedRackTarget));
//
//	static MatchElement ourPort2;
//	static BigUnloadSafeOurPortAction unloadSafeOurPortAction(matchData);
//	static SingleActionList unloadSafeOurPortActions(unloadSafeOurPortAction);
//	static MatchTarget unloadSafeOurPortTarget(unloadSafeOurPortActions, ourPort2);
//	static MatchTask unloadSafeOurPortTask("unloadSafe", 5, 41, Pointer<MatchTarget>(&unloadSafeOurPortTarget));

//	static MatchElement ourBuysTable;
//	static BigGatherOurSideTableAction gatherOurSideTableAction(matchData);
//	static SingleActionList gatherOurSideTableActions(gatherOurSideTableAction);
//	static MatchTarget gatherOurSideTableTarget(gatherOurSideTableActions, ourBuysTable);
//	static MatchTask gatherOurSideTableTask("gatherTable1", 5, 42, Pointer<MatchTarget>(&gatherOurSideTableTarget));

//	static MatchElement buysSmallPort;
//	static BigGatherAroundSmallPortRearAction gatherSmallPortRearAction(matchData);
//	static SingleActionList gatherSmallPortRearActions(gatherSmallPortRearAction);
//	static MatchTarget gatherSmallPortRearTarget(gatherSmallPortRearActions, buysSmallPort);
//	static MatchTask gatherSmallPortRearTask("gatherSPR", 5, 44, Pointer<MatchTarget>(&gatherSmallPortRearTarget));
//
//	static BigGatherAroundSmallPortAction gatherSmallPortAction(matchData);
//	static SingleActionList gatherSmallPortActions(gatherSmallPortAction);
//	static MatchTarget gatherSmallPortTarget(gatherSmallPortActions, buysSmallPort);
//	static MatchTask gatherSmallPortTask("gatherSP", 5, 45, Pointer<MatchTarget>(&gatherSmallPortTarget));
//
//	static MatchElement smallPort;
//
//	static BigUnloadAllSmallPortAction unloadSmallPortAction(matchData);
//	static SingleActionList unloadSmallPortActions(unloadSmallPortActions);
//	static MatchTarget unloadSmallPortTarget(unloadSmallPortActions, smallPort);
//	static MatchTask unloadSmallPortTask("unloadSmall", 5, 49, Pointer<MatchTarget>(&unloadSmallPortTarget));

//	static MatchElement bay;
//	static BigEndAtBayAction endAtBayAction(matchData);
//	static SingleActionList endAtBayActions(endAtBayActions);
//	static MatchTarget endAtBayTarget(endAtBayActions, bay);
//	static MatchTask endAtBayTask("endAtBay", 5, 60, Pointer<MatchTarget>(&endAtBayTarget));

	static MatchTask tasks[TASK_LIST_SIZE] = {
		klighthouseTask,
		aggrOppSideRackTask,
		aggrOurSideRackTask,
		ourSideRackTask,
		oppSideRackTask,
		ourPortUnloadTask,
//		gatherOurSideTableTask,
//		ourReservedRackTask,
//		unloadSafeOurPortTask,
//		gatherOurSideTableTask,
//		gatherSmallPortRearTask,
//		gatherSmallPortTask,
//		unloadSmallPortTask,
//		endAtBayTask,
	};

	return tasks;
}
