/*
 * BigGatherOurSideTableAction.cpp
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#include "BigGatherOurSideTableAction.h"
#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"

BigGatherOurSideTableAction::BigGatherOurSideTableAction(MatchData2020 &matchData) :
	MatchAction("bgGatherSP"), matchData(matchData)
{
	// nothing to add
}

void BigGatherOurSideTableAction::executeStep(Navigation &navigation, unsigned int step)
{
	extern BigActuator bigActuator;
	BigBuoySensorData sensors;

	switch(step)
	{
		case 0:
			bigActuator.armBottomBuoyCatcher(FrontRear::Front);
			navigation.waitForDelay(300);
			bigActuator.armBottomBuoyCatcher(FrontRear::Rear);
			navigation.waitForDelay(300);
			bigActuator.raise();
			navigation.waitForDelay(300);
			break;

		case 1:
			navigation.moveTo(Point(800, 1850));
			break;

		case 2:
			navigation.getControl().setSpeedIndex(SPEED_BIG_SLOW);
			navigation.moveTo(Point(800, 1930));
			break;

		case 3:
			navigation.getControl().setSpeedIndex(SPEED_BIG_NOMINAL);
			navigation.rotateToOrientationDeciDegrees(1800);
			break;

		case 4:
			navigation.getControl().setSpeedIndex(SPEED_BIG_SLOW);
			navigation.moveTo(Point(350, 1930));
			break;

		case 5:
			navigation.getControl().setSpeedIndex(SPEED_BIG_NOMINAL);
			navigation.rotateToOrientationDeciDegrees(900);
			break;

		case 6:
			navigation.getControl().setSpeedIndex(SPEED_BIG_SLOW);
			navigation.moveToBackwards(Point(350, 1100));
			break;

		case 7:
			navigation.getControl().setSpeedIndex(SPEED_BIG_NOMINAL);
			navigation.moveToBackwards(Point(800, 670));
			break;

		case 8:
			// d�sactivation de la d�tection dans le port
			navigation.getControl().obstacleAvoidance = false;
			navigation.getControl().setSpeedIndex(SPEED_BIG_NOMINAL);
			navigation.moveToBackwards(Point(800, 250));
			break;

		case 9:
			sensors = bigActuator.getBottomBuoyData();
			bigActuator.releaseBottomBuoyCatcher(FrontRear::Rear);
			matchData.unload(sensors, FrontRear::Rear, Match2020Chenal::STARTING_PORT);
			break;

		case 10:
			navigation.moveTo(Point(800, 500));
			break;

		case 11:
			navigation.rotateToOrientationDeciDegrees(-900);
			break;

		case 12:
			navigation.moveTo(Point(800, 320));
			break;

		case 13:
			sensors = bigActuator.getBottomBuoyData();
			bigActuator.releaseBottomBuoyCatcher(FrontRear::Front);
			matchData.unload(sensors, FrontRear::Front, Match2020Chenal::STARTING_PORT);
			break;

		case 14:
			navigation.getControl().obstacleAvoidance = true;
			navigation.moveToBackwards(Point(800, 500));
			break;

		case STEP_FINALIZATION:
			bigActuator.armBottomBuoyCatcher(FrontRear::Front);
			bigActuator.armBottomBuoyCatcher(FrontRear::Rear);
			bigActuator.raise(false);
			navigation.getControl().obstacleAvoidance = true;
			navigation.getControl().setSpeedIndex(SPEED_BIG_NOMINAL);
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
