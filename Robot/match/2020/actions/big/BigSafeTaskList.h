/*
 * BigSafeTaskList.h
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_BIG_BIGSAFETASKLIST_H_
#define MATCH_2020_ACTIONS_BIG_BIGSAFETASKLIST_H_

#include "../../../MatchTask.h"
#include "../../../MatchTaskList.h"
#include "../../MatchData2020.h"

#define TASK_LIST_SIZE_SAFE 4

class BigSafeTaskList : public MatchTaskList {
private:
	MatchData2020 &matchData;
	MatchTask (&tasks)[TASK_LIST_SIZE_SAFE];

	MatchTask (&createTasks())[TASK_LIST_SIZE_SAFE];

public:
	BigSafeTaskList(MatchData2020 &matchData);
	virtual unsigned int getCount() const;
	virtual MatchTask& getTaskAt(unsigned int index) const;
};

#endif /* MATCH_2020_ACTIONS_BIG_BIGSAFETASKLIST_H_ */
