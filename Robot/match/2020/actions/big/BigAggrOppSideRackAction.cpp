/*
 * BigAggrOppSideRackAction.cpp
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#include "BigAggrOppSideRackAction.h"
#include "BigActionsConstants.h"
#include "../../../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "devices/host/HostDevice.h"
#include "devices/databus/2020/DataBus2020.h"
#include "match/2020/MatchActionUtils2020.h"
#include "math/cenMath.h"

BigAggrOppSideRackAction::BigAggrOppSideRackAction(MatchData2020 &matchData) :
		MatchAction("bgOppRack", POINT_OPPONENT_RACK), matchData(matchData) {
	//nothing to do
}

void BigAggrOppSideRackAction::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;
	extern DataBus2020 dataBus;
	const RobotGeometry &robotGeometry = HostDevice::getHostData().getGeometry();
	Point point;

	switch (step) {
		case 0:
			dataBus.queryCombination(pattern);
			bigActuator.setCode(pattern);
			break;

		case 1:
			navigation.moveTo(POINT_OPPONENT_RACK);
			break;

		case 2:
			navigation.rotateToOrientationDeciDegrees(1800);
			break;

		case 3:
			bigActuator.readyToTake(FrontRear::Front, FirstOrComplement::FirstOponent);
			break;

		case 4: {
			const Position &current = navigation.getControl().getPosition();
			navigation.moveTo(Point(robotGeometry.frontWidth + 20, current.y));
			break;
		}

		case 5:
			navigation.repositionMM(POINT_OPPONENT_RACK.x + 50, robotGeometry.frontWidth, CURRENT, 1800);
			MatchActionUtils2020::waitForGobeletStabilisationInRack(navigation);
			break;

		case 6:
			bigActuator.hold();
			MatchActionUtils2020::waitForHold(navigation);
			navigation.moveMM(-20); // petite reculade pour laisser de la place
			bigActuator.raise();
			break;

		case 7:
			navigation.moveToBackwards(Point(260, frontTakeY));
			break;

		case 8:
			rearTakeY = 2150 + 75 * bigActuator.computeOffset(FrontRear::Rear, FirstOrComplement::FirstOponent);
			if (rearTakeY > frontTakeY) {
				navigation.rotateToOrientationDeciDegrees(900); // look at opponent
				navigation.moveTo(Point(260, rearTakeY));
			} else if (rearTakeY < frontTakeY) {
				navigation.rotateToOrientationDeciDegrees(900); // look at opponent
				navigation.moveToBackwards(Point(260, rearTakeY));
			}
			break;

		case 9:
			navigation.rotateToOrientationDeciDegrees(0);
			break;

		case 10:
			bigActuator.readyToTake(FrontRear::Rear, FirstOrComplement::FirstOponent);
			break;

		case 11: {
			const Position &current = navigation.getControl().getPosition();
			navigation.moveToBackwards(Point(robotGeometry.rearWidth + 20, current.y));
			break;
		}

		case 12:
			navigation.repositionMM(-300, robotGeometry.rearWidth, CURRENT, 0);
			MatchActionUtils2020::waitForGobeletStabilisationInRack(navigation);
			break;

		case 13:
			bigActuator.hold();
			MatchActionUtils2020::waitForHold(navigation);
			navigation.moveMM(20); // petite reculade pour laisser de la place
			bigActuator.raise();
			break;

		case 14: {
			//navigation.moveTo(Point(320, rearTakeY));
			// bras en bas pour le balayage des gobelets
			bigActuator.setCleanerArmRightInBlue(true);
			const Position &current = navigation.getControl().getPosition();
			float radius = 320 - current.x;
			Point destination(260, 600);
			Point origin(320, rearTakeY - radius);
			float orientation = origin.getOrientationTo(destination);
			navigation.moveToArc(origin, ANGLE_TO_DECIDEGREES(orientation));
			break;
		}

		case 15:
			navigation.moveTo(Point(260, 600));
			break;

		case STEP_FINALIZATION:
			bigActuator.setCleanerArmRightInBlue(false);
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
