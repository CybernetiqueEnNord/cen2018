/*
 * BigOurSideRackAction.h
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_BIG_BIGSAFEOURSIDERACKACTION_H_
#define MATCH_2020_ACTIONS_BIG_BIGSAFEOURSIDERACKACTION_H_

#include "match/MatchAction.h"
#include "match/2020/MatchData2020.h"

class BigSafeOurSideRackAction : public MatchAction
{
private:
	MatchData2020 &matchData;
	int frontTakeY = 850;
	int rearTakeY = 850;

public:
	BigSafeOurSideRackAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_BIG_BIGSAFEOURSIDERACKACTION_H_ */
