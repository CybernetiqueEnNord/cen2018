/*
 * BigAggrOurSideRackAction.h
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_BIG_BIGAGGROURSIDERACKACTION_H_
#define MATCH_2020_ACTIONS_BIG_BIGAGGROURSIDERACKACTION_H_

#include "match/MatchAction.h"
#include "match/2020/MatchData2020.h"

class BigAggrOurSideRackAction : public MatchAction
{
private:
	MatchData2020 &matchData;
	int frontTakeY = 850;
	int rearTakeY = 850;

public:
	BigAggrOurSideRackAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_BIG_BIGAGGROURSIDERACKACTION_H_ */
