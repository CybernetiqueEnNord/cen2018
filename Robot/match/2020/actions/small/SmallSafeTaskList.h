/*
 * SmallSafeTaskList.h
 *
 *  Created on: 6 oct. 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2020_ACTIONS_SMALL_SMALLSAFETASKLIST_H_
#define MATCH_2020_ACTIONS_SMALL_SMALLSAFETASKLIST_H_

#include "../../../MatchTask.h"
#include "../../../MatchTaskList.h"
#include "../../MatchData2020.h"

class SmallSafeTaskList : public MatchTaskList {
private:
	MatchData2020 &matchData;
	MatchTask (&tasks)[1];

	MatchTask (&createTasks())[1];

public:
	SmallSafeTaskList(MatchData2020 &matchData);
	virtual unsigned int getCount() const;
	virtual MatchTask& getTaskAt(unsigned int index) const;
};

#endif /* MATCH_2020_ACTIONS_SMALL_SMALLNOMINALTASKLIST_H_ */
