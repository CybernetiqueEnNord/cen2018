/*
 * SmallUnloadAllSmallPort.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#include "SmallUnloadAllSmallPortAction.h"

#include "i2c/ActuatorSmall2020Commands.h"
#include "devices/actuators/2020/small/SmallActuator.h"
#include "devices/host/HostDevice.h"

SmallUnloadAllSmallPortAction::SmallUnloadAllSmallPortAction(MatchData2020 &matchData) :
		MatchAction("UnloadSmPort", Point(1450, 1550)), matchData(matchData) {
	// nothing to do
}

SmallUnloadAllSmallPortAction::SmallUnloadAllSmallPortAction(MatchData2020 &matchData, const char *name, Point origin) :
		MatchAction(name, origin), matchData(matchData) {
}

void SmallUnloadAllSmallPortAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();

	int oldSpeed = 0;

	switch (step) {

		case 0:
			break;

		case 1:
			// pousser le gobelet du centre vers le gobelet du bord de sorte que le robot passe
			// entre les 2 sans sortir de bras
			navigation.rotateToOrientationDeciDegrees(900);
			smallActuator.setArmRight_asBlue(ServoPosition::Working);
			navigation.moveTo(Point(1450, 1700));
			smallActuator.setArmRight_asBlue(ServoPosition::Up);
			break;

		case 2:
			// se positionner entre les 2 gobelets pour pouvoir avancer vers la zone en les poussant
			navigation.moveTo(Point(1450, 1845));
			break;

		case 3:
			// Pousser 2 gobelets
			// on ne peut pas les pousser par l'arriere si on a des gobelets ventous�s
			// on se place de sorte � prendre le gobelet droite (vert en bleu) dans la roue
			oldSpeed = navigation.getControl().getSpeedIndex();
			navigation.rotateToOrientation(Point(1800, 1800));
			navigation.getControl().setSpeedIndex(SPEED_SMALL_SAFE);
			// et on sort le bras de l'autre cot�
			navigation.moveTo(Point(1800, 1800));
			navigation.rotateToOrientationDeciDegrees(0);
			navigation.repositionMM(300, 2000 - geometry.frontWidth, CURRENT, 0);
			navigation.getControl().setSpeedIndex(oldSpeed);
			navigation.moveToBackwards(Point(1680, 1800));
			matchData.unload(Match2020BouyColor::BOUY_GREEN, Match2020Chenal::LITTLE_PORT, Match2020ChenalColor::CHENAL_GREEN);
			matchData.unload(Match2020BouyColor::BOUY_RED, Match2020Chenal::LITTLE_PORT, Match2020ChenalColor::CHENAL_RED);
			break;

		case 4:
			//Poser deux gobelets en ventouses
			smallActuator.setFrontBothLifter(ServoPosition::Down);
			navigation.moveTo(Point(2000 - 70 - geometry.frontWidth - 10, 1800));
			smallActuator.setPumpFrontLeft_asBlue(false);
			matchData.unload(Match2020BouyColor::BOUY_GREEN, Match2020Chenal::LITTLE_PORT, Match2020ChenalColor::CHENAL_GREEN);
			smallActuator.setPumpFrontRight_asBlue(false);
			matchData.unload(Match2020BouyColor::BOUY_RED, Match2020Chenal::LITTLE_PORT, Match2020ChenalColor::CHENAL_RED);
			smallActuator.setFrontBothLifter(ServoPosition::Up);
			break;

		case 5:
//			navigation.moveMM(-100);
			break;

		case 6:
//			navigation.rotateToOrientationDeciDegrees(-900);
			break;

		case 7:
			// recalage en X
//			navigation.repositionMM(300, CURRENT, 1511 + geometry.frontWidth, -900);
			break;

		case 8:
//			navigation.moveMM(-30);
			navigation.moveToBackwards(Point(1550, 1800));
			break;

		case 9:
			// attraper gobelet difficile 1
			oldSpeed = navigation.getControl().getSpeedIndex();
			navigation.getControl().setSpeedIndex(SPEED_SMALL_NOMINAL);
			navigation.moveTo(Point(1790, 1670)); // ancienne valeur 1660
			navigation.getControl().setSpeedIndex(oldSpeed);
			smallActuator.setPumpFrontLeft_asBlue(true);
			smallActuator.setFrontLeftLifter_asBlue(ServoPosition::Down);
			navigation.moveMM(-40);
			smallActuator.setFrontLeftLifter_asBlue(ServoPosition::Center);
			break;

		case 10:
			oldSpeed = navigation.getControl().getSpeedIndex();
			navigation.getControl().setSpeedIndex(SPEED_SMALL_NOMINAL);
			navigation.moveToBackwards(Point(1550, 1800));
			navigation.getControl().setSpeedIndex(oldSpeed);
			break;

		case 11:
			// attrapper gobelet difficile 2
			oldSpeed = navigation.getControl().getSpeedIndex();
			navigation.getControl().setSpeedIndex(SPEED_SMALL_NOMINAL);
			navigation.moveTo(Point(1800, 1940));
			navigation.getControl().setSpeedIndex(oldSpeed);
			smallActuator.setPumpFrontRight_asBlue(true);
			smallActuator.setFrontRightLifter_asBlue(ServoPosition::Down);
			navigation.moveMM(-40);
			smallActuator.setFrontRightLifter_asBlue(ServoPosition::Center);
			break;

		case 12:
			oldSpeed = navigation.getControl().getSpeedIndex();
			navigation.getControl().setSpeedIndex(SPEED_SMALL_NOMINAL);
			navigation.moveToBackwards(Point(1550, 1800));
			navigation.getControl().setSpeedIndex(oldSpeed);
			break;

		case 13:
			// on d�pose les gobelets difficiles
			navigation.rotateToOrientationDeciDegrees(0);
			smallActuator.setFrontBothLifter(ServoPosition::Down);
			navigation.moveTo(Point(1680, 1800));
			smallActuator.setPumpFrontRight_asBlue(false);
			matchData.unload(Match2020BouyColor::BOUY_GREEN, Match2020Chenal::LITTLE_PORT, Match2020ChenalColor::CHENAL_GREEN);
			smallActuator.setPumpFrontLeft_asBlue(false);
			matchData.unload(Match2020BouyColor::BOUY_RED, Match2020Chenal::LITTLE_PORT, Match2020ChenalColor::CHENAL_RED);
			smallActuator.setFrontBothLifter(ServoPosition::Up);
			break;

		case 14:
			navigation.moveMM(-100);
			// on d�pose les gobelets derri�re restants
			navigation.moveToBackwards(Point(1710, 1840));
			navigation.rotateToOrientationDeciDegrees(-1800);

			smallActuator.setRearLifter(ServoPosition::Working);
			navigation.moveMM(10);
			smallActuator.setPumpTrioRearLeft_asBlue(false);
			smallActuator.setPumpRearCenter(false);
			navigation.waitForDelay(500);
			matchData.unload(Match2020BouyColor::BOUY_GREEN, Match2020Chenal::LITTLE_PORT, Match2020ChenalColor::CHENAL_GREEN);
			matchData.unload(Match2020BouyColor::BOUY_RED, Match2020Chenal::LITTLE_PORT, Match2020ChenalColor::CHENAL_RED);

			// remonter le trio sinon hors p�rimetre
			smallActuator.setRearLifter(ServoPosition::Up);
			smallActuator.setArmLeft_asBlue(ServoPosition::Depose);
			navigation.moveMM(10);
			smallActuator.setPumpRearLeft_asBlue(false);
			navigation.waitForDelay(500);
			navigation.waitForDelay(200);
			matchData.unload(Match2020BouyColor::BOUY_RED, Match2020Chenal::LITTLE_PORT, Match2020ChenalColor::CHENAL_GREEN);
			smallActuator.setArmLeft_asBlue(ServoPosition::Up);

			navigation.moveMM(100);
			navigation.moveToBackwards(Point(1580, 1650));

			navigation.rotateToOrientationDeciDegrees(1800);
			smallActuator.setRearLifter(ServoPosition::Working);
			navigation.moveMM(-30); // positionnement
			navigation.moveMM(10);  // d�sangulation
			smallActuator.setPumpTrioRearRight_asBlue(false);
			navigation.waitForDelay(500);
			matchData.unload(Match2020BouyColor::BOUY_RED, Match2020Chenal::LITTLE_PORT, Match2020ChenalColor::CHENAL_RED);
			smallActuator.setRearLifter(ServoPosition::Up);
			navigation.moveMM(60);

			navigation.moveTo(Point(1580, 2000));
			smallActuator.setArmRight_asBlue(ServoPosition::Depose);
			// on ravance pour d�poser le reste sur le port et faire des points !
			navigation.moveMM(10);
			smallActuator.setPumpRearRight_asBlue(false);
			navigation.waitForDelay(500);
			matchData.unload(Match2020BouyColor::BOUY_GREEN, Match2020Chenal::LITTLE_PORT, Match2020ChenalColor::CHENAL_GREEN);
			smallActuator.setArmRight_asBlue(ServoPosition::Up);
			break;

		case 15:
			navigation.moveTo(Point(1400, 2000));
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
