/*
 * SmallLighthouseAction.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#include "SmallLighthouseAction.h"

#include "../../../../control/Navigation.h"
#include "../../../../devices/actuators/2020/small/SmallActuator.h"
#include "devices/databus/2020/DataBus2020.h"

SmallLighthouseAction::SmallLighthouseAction(MatchData2020 &matchData) :
		MatchAction("smLighthouse", Point(300, 700)), matchData(matchData) {
	// nothing to do
}

void SmallLighthouseAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;

	switch (step) {
		case 0:

			//Allready checked in customGetState of SmallNominalTaskList
			// on fait le phare si la communication fonctionne et que le phare n'est pas activ�

//			if (!dataBus.queryLightHouse(lightHouseDone) || lightHouseDone) {
//				MatchAction::executeStep(navigation, step);
//				return;
//			}

			navigation.moveTo(Point(190, 450));
			break;

		case 1:
			smallActuator.setPhareArm(true);
			break;

		case 2:
			navigation.moveTo(Point(190, 280));
			break;

		case 3:
			navigation.moveTo(Point(300, 720));
			smallActuator.setPhareArm(false);
			matchData.upliftLighthouse();
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
