/*
 * SmallGetFirstBuoyAction.h
 *
 *  Created on: 9 f�vr. 2020
 *      Author: zoro
 */

#ifndef MATCH_2020_ACTIONS_SMALL_SMALLGETFIRSTBUOYACTION_H_
#define MATCH_2020_ACTIONS_SMALL_SMALLGETFIRSTBUOYACTION_H_

#include "../../../MatchAction.h"
#include "../../MatchData2020.h"

class SmallGetFirstBuoyAction : public MatchAction
{
private:
	MatchData2020 &matchData;

public:
	SmallGetFirstBuoyAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);

};

#endif /* MATCH_2020_ACTIONS_SMALL_SMALLGETFIRSTBUOYACTION_H_ */
