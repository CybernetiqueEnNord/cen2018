/*
 * BigOurReservedRackAction.h
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_SMALL_SMALLOURRESERVEDRACKACTION_H_
#define MATCH_2020_ACTIONS_SMALL_SMALLOURRESERVEDRACKACTION_H_

#include "match/MatchAction.h"
#include "match/2020/MatchData2020.h"

class SmallOurReservedRackAction : public MatchAction
{
private:
	MatchData2020 &matchData;

public:
	SmallOurReservedRackAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_SMALL_SMALLOURRESERVEDRACKACTION_H_ */
