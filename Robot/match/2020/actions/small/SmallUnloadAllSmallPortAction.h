/*
 * SmallUnloadAllSmallPort.h
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_SMALL_BIGUNLOADALLSMALLPORTACTION_H_
#define MATCH_2020_ACTIONS_SMALL_BIGUNLOADALLSMALLPORTACTION_H_

#include "match/MatchAction.h"
#include "match/2020/MatchData2020.h"

class SmallUnloadAllSmallPortAction : public MatchAction
{
private:
	MatchData2020 &matchData;

protected:
	SmallUnloadAllSmallPortAction(MatchData2020 &matchData, const char *name, Point origin);

public:
	SmallUnloadAllSmallPortAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_SMALL_BIGUNLOADALLSMALLPORTACTION_H_ */
