/*
 * SmallNominalTaskList.h
 *
 *  Created on: 6 oct. 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2020_ACTIONS_SMALL_SMALLNOMINALTASKLIST_H_
#define MATCH_2020_ACTIONS_SMALL_SMALLNOMINALTASKLIST_H_

#include "match/MatchTask.h"
#include "match/MatchTaskList.h"
#include "match/2020/MatchData2020.h"

class SmallNominalTaskList : public MatchTaskList {
private:
	MatchData2020 &matchData;
	MatchTask (&tasks)[6];

	MatchTask (&createTasks())[6];

public:
	SmallNominalTaskList(MatchData2020 &matchData);
	virtual unsigned int getCount() const;
	virtual MatchTask& getTaskAt(unsigned int index) const;
};

#endif /* MATCH_2020_ACTIONS_SMALL_SMALLNOMINALTASKLIST_H_ */
