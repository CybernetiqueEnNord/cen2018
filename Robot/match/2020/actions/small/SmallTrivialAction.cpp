/*
 * SmallTrivialAction.cpp
 *
 *  Created on: 6 oct. 2019
 *      Author: Anastaszor
 */

#include "SmallTrivialAction.h"

#include "../../../../control/Navigation.h"
#include "../../../../devices/actuators/2020/small/SmallActuator.h"

SmallTrivialAction::SmallTrivialAction(MatchData2020 &matchData) :
		MatchAction("smTrivial", Point(1600, 300)), matchData(matchData) {
	// nothing to do
}

void SmallTrivialAction::executeStep(Navigation &navigation, unsigned int step) {

	switch (step) {
		case 0:
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
