/*
 * SmallTrivialAction.h
 *
 *  Created on: 6 oct. 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2020_ACTIONS_SMALL_SMALLTRIVIALACTION_H_
#define MATCH_2020_ACTIONS_SMALL_SMALLTRIVIALACTION_H_

#include "../../../MatchAction.h"
#include "../../MatchData2020.h"

class SmallTrivialAction : public MatchAction
{
private:
	MatchData2020 &matchData;

public:
	SmallTrivialAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_SMALL_SMALLTRIVIALACTION_H_ */
