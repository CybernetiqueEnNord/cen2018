/*
 * SmallLighthouseAction.h
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_SMALL_SMALLLIGHTHOUSEACTION_H_
#define MATCH_2020_ACTIONS_SMALL_SMALLLIGHTHOUSEACTION_H_

#include "../../../MatchAction.h"
#include "../../MatchData2020.h"

class SmallLighthouseAction : public MatchAction
{
private:
	MatchData2020 &matchData;

public:
	SmallLighthouseAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_SMALL_SMALLLIGHTHOUSEACTION_H_ */
