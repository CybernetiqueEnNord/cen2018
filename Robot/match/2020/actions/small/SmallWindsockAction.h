/*
 * SmallWindsockAction.h
 *
 *  Created on: 6 oct. 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2020_ACTIONS_SMALL_SMALLWINDSOCKACTION_H_
#define MATCH_2020_ACTIONS_SMALL_SMALLWINDSOCKACTION_H_

#include "../../../MatchAction.h"
#include "../../MatchData2020.h"

class SmallWindsockAction : public MatchAction
{
private:
	MatchData2020 &matchData;

public:
	SmallWindsockAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_SMALL_SMALLWINDSOCKACTION_H_ */
