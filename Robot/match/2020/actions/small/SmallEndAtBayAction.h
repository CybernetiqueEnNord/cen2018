/*
 * SmallEndAtBayAction.h
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_ACTIONS_SMALL_SMALLENDATBAYACTION_H_
#define MATCH_2020_ACTIONS_SMALL_SMALLENDATBAYACTION_H_

#include "../../../MatchAction.h"
#include "../../MatchData2020.h"

class SmallEndAtBayAction : public MatchAction
{
private:
	MatchData2020 &matchData;
	Match2020AnchorArea target;

public:
	SmallEndAtBayAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2020_ACTIONS_SMALL_SMALLENDATBAYACTION_H_ */
