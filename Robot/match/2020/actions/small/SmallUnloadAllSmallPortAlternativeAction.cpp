/*
 * SmallUnloadAllSmallPortAlternativeAction.cpp
 *
 *  Created on: 26 oct. 2020
 *      Author: Emmanuel
 */

#include "SmallUnloadAllSmallPortAlternativeAction.h"

SmallUnloadAllSmallPortAlternativeAction::SmallUnloadAllSmallPortAlternativeAction(MatchData2020 &matchData) :
		SmallUnloadAllSmallPortAction(matchData, "UnloadSmPortAlternative", Point(1000, 900)) {
}

void SmallUnloadAllSmallPortAlternativeAction::executeStep(Navigation &navigation, unsigned int step) {
	switch (step) {
		case 0:
			navigation.moveTo(Point(1000, 1550));
			break;

		default:
			SmallUnloadAllSmallPortAction::executeStep(navigation, step);
			break;
	}
}
