/*
 * SmallUnloadAllSmallPortAlternativeAction.h
 *
 *  Created on: 26 oct. 2020
 *      Author: Emmanuel
 */

#ifndef MATCH_2020_ACTIONS_SMALL_SMALLUNLOADALLSMALLPORTALTERNATIVEACTION_H_
#define MATCH_2020_ACTIONS_SMALL_SMALLUNLOADALLSMALLPORTALTERNATIVEACTION_H_

#include "SmallUnloadAllSmallPortAction.h"

class SmallUnloadAllSmallPortAlternativeAction : public SmallUnloadAllSmallPortAction {
public:
	SmallUnloadAllSmallPortAlternativeAction(MatchData2020 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step) override;
};

#endif /* MATCH_2020_ACTIONS_SMALL_SMALLUNLOADALLSMALLPORTALTERNATIVEACTION_H_ */
