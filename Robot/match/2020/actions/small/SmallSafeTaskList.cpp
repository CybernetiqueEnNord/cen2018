/*
 * SmallSafeTaskList.cpp
 *
 *  Created on: 6 oct. 2019
 *      Author: Anastaszor
 */

#include "SmallSafeTaskList.h"

#include "../../../../devices/actuators/2020/small/SmallActuator.h"
#include "../../../../utils/Arrays.h"
#include "../../../../utils/Pointer.h"
#include "../../../MatchElement.h"
#include "../../../MatchTarget.h"
#include "../../../SingleActionList.h"
#include "SmallGetFirstBuoyAction.h"
#include "devices/databus/2020/DataBus2020.h"


SmallSafeTaskList::SmallSafeTaskList(MatchData2020 &matchData) :
		MatchTaskList(), matchData(matchData), tasks(createTasks()) {
	// nothing to do
}

unsigned int SmallSafeTaskList::getCount() const {
	return ARRAY_SIZE(tasks);
}

MatchTask& SmallSafeTaskList::getTaskAt(unsigned int index) const {
	return tasks[index];
}

MatchTask (&SmallSafeTaskList::createTasks())[1] {

//			static MatchElement trivialMatchElement;
//			static SmallTrivialAction trivialAction(matchData);
//			static SingleActionList trivialActionList(trivialAction);
//			static MatchTarget trivialTarget(trivialActionList, trivialMatchElement);
//			static MatchTask smallTrivialTask("trivial", 4, -100, Pointer<MatchTarget>(&trivialTarget));

			static MatchElement getFirstsBuoy;
			static SmallGetFirstBuoyAction getFirstBuoyAction(matchData);
			static SingleActionList smallGetFirstBuoyActions(getFirstBuoyAction);
			static MatchTarget smallGetFirstBuoyTarget(smallGetFirstBuoyActions, getFirstsBuoy);
			static MatchTask smallGetFirstBuoyTask("firstBuoy", 4, -100, Pointer<MatchTarget>(&smallGetFirstBuoyTarget));

			static MatchTask tasks[1] = { smallGetFirstBuoyTask };

			return tasks;
		}
