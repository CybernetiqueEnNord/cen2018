/*
 * SmallDraftTaskList.cpp
 *
 *  Created on: 29 ao�t 2020
 *      Author: Robin
 */

#include "SmallDraftTaskList.h"

#include "../../../../utils/Arrays.h"
#include "../../../../utils/Pointer.h"
#include "../../../MatchElement.h"
#include "../../../MatchTarget.h"
#include "../../../MatchTask.h"
#include "../../../MatchTaskList.h"
#include "../../../SingleActionList.h"
#include "SmallOurReservedRackAction.h"

SmallDraftTaskList::SmallDraftTaskList(MatchData2020 &matchData) :
		MatchTaskList(), matchData(matchData), tasks(createTasks()) {
// nothing to do
}

unsigned int SmallDraftTaskList::getCount() const {
	return ARRAY_SIZE(tasks);
}

MatchTask& SmallDraftTaskList::getTaskAt(unsigned int index) const {
	return tasks[index];
}

MatchTask (& SmallDraftTaskList::createTasks())[1] {

			static MatchElement getFirstsBuoy;
			static SmallOurReservedRackAction getOurReservedRackAction(matchData);
			static SingleActionList smallOurReservedRackActions(getOurReservedRackAction);
			static MatchTarget smallOurReservedRackTarget(smallOurReservedRackActions, getFirstsBuoy);
			static MatchTask smallOurReservedRackTask("getOurRack", 4, -100, Pointer<MatchTarget>(&smallOurReservedRackTarget));

			static MatchTask tasks[1] = { smallOurReservedRackTask };

			return tasks;
		}
