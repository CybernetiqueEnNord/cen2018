/*
 * SmallGetFirstBuoyAction.cpp
 *
 *  Created on: 9 f�vr. 2020
 *      Author: zoro
 */

#include "SmallGetFirstBuoyAction.h"
#include "../../../../control/Navigation.h"
#include "../../../../devices/actuators/2020/small/SmallActuator.h"

SmallGetFirstBuoyAction::SmallGetFirstBuoyAction(MatchData2020 &matchData) :
		MatchAction("getFirstBuoy", Point(0, 0)), matchData(matchData) {
	// TODO Auto-generated constructor stub

}

void SmallGetFirstBuoyAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;

	switch (step) {
		case 0:
			navigation.rotateToOrientationDeciDegrees(450); //safety if we arent allready at this angle.
			smallActuator.setPumpFrontLeft_asBlue(true);
			navigation.moveMM(120);
			smallActuator.setFrontLeftLifter_asBlue(ServoPosition::Down);
			navigation.waitForDelay(500);
			break;

		case 1:
			navigation.rotateToOrientationDeciDegrees(100);
			navigation.moveMM(20);
			smallActuator.setPumpFrontRight_asBlue(true);
			smallActuator.setFrontRightLifter_asBlue(ServoPosition::Down);
			navigation.waitForDelay(500);
			break;

		case 2:
			smallActuator.setFrontBothLifter(ServoPosition::Center);
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
