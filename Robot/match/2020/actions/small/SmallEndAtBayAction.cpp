/*
 * SmallEndAtBayAction.cpp
 *
 *  Created on: 9 f�vr. 2020
 *      Author: anastaszor
 */

#include "SmallEndAtBayAction.h"
#include "devices/host/HostDevice.h"
#include "devices/databus/2020/DataBus2020.h"

SmallEndAtBayAction::SmallEndAtBayAction(MatchData2020 &matchData) :
		MatchAction("smEndAtBay", Point(400, 1500)), matchData(matchData) {
	target = Match2020AnchorArea::UNKNOWN;
}

void SmallEndAtBayAction::executeStep(Navigation &navigation, unsigned int step) {
	const RobotGeometry &robotGeometry = HostDevice::getHostData().getGeometry();

	extern DataBus2020 dataBus;


	if (step == 0) {
		target = Match2020AnchorArea::SOUTH;
		//if (!dataBus.queryAnchorArea(target)) {
		//	target = Match2020AnchorArea::UNKNOWN;
		//}
		//navigation.waitForDelay(matchData.getTimer().getRemainingTimeMS() - 45000);
	}

	if (target == Match2020AnchorArea::NORTH) {
		switch (step) {
			case 1:
				navigation.moveTo(Point(200, 650));
				break;

			case 2:
				navigation.moveTo(Point(200, 400));
				break;

			case 3:
				navigation.rotateToOrientationDeciDegrees(900);
				break;

			case 4:
				navigation.repositionMM(-200, CURRENT, robotGeometry.rearWidth, 900);
				matchData.anchorDown(Match2020AnchorArea::NORTH);
				break;
		}
	} else {
		switch (step) {
			case 1:
				navigation.moveTo(Point(1650, 450));
				break;

			case 2:
				navigation.moveTo(Point(1300, 300));
				matchData.anchorDown(Match2020AnchorArea::SOUTH);
				break;
		}
	}

	if (step >= 5) {
		MatchAction::executeStep(navigation, step);
	}
}
