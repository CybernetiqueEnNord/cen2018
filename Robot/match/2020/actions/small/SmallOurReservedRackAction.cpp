/*
 * SmallOurReservedRackAction.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#include "../../../../../../Commons/i2c/ActuatorSmall2020Commands.h"
#include "devices/actuators/2020/small/SmallActuator.h"
#include "devices/host/HostDevice.h"
#include "match/2020/MatchActionUtils2020.h"
#include "SmallOurReservedRackAction.h"

SmallOurReservedRackAction::SmallOurReservedRackAction(MatchData2020 &matchData) :
		MatchAction("smResRack", Point(1600, 670)), matchData(matchData) {
	// nothing to do
}

void SmallOurReservedRackAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;
	const RobotGeometry &robotGeometry = HostDevice::getHostData().getGeometry();

	switch (step) {
		case 0:
			break;

		case 1:
			navigation.moveToBackwards(Point(1600, robotGeometry.rearWidth + 15));
			break;

		case 2:
			navigation.repositionMM(-200, CURRENT, robotGeometry.rearWidth, 900);
			break;

		case 3:
			navigation.moveMM(60);
			break;

		case 4:
			smallActuator.setRearLifter(ServoPosition::Down);
			smallActuator.setPumpRearTrio(true);
			navigation.getControl().setSpeedIndex(SPEED_SMALL_SAFE);
			navigation.moveMM(-40);
			break;

		case 5:
			MatchActionUtils2020::waitForGobeletStabilisationInRack(navigation);
			smallActuator.setRearLifter(ServoPosition::Up);
			break;

		case 6:
			navigation.getControl().setSpeedIndex(SPEED_SMALL_NOMINAL);
			navigation.moveTo(Point(1600, robotGeometry.lateralWidth + 15));
			break;

		case 7:
			navigation.rotateToOrientationDeciDegrees(1800);
			break;

		case 8:
			smallActuator.setPumpRearLeft_asBlue(true);
			smallActuator.setArmLeft_asBlue(ServoPosition::Working);
			navigation.getControl().setSpeedIndex(SPEED_SMALL_SAFE);
			navigation.moveToBackwards(Point(1710, robotGeometry.lateralWidth + 15));
			break;

		case 9:
			smallActuator.setArmLeft_asBlue(ServoPosition::Center);
			break;

		case 10:
			navigation.getControl().setSpeedIndex(SPEED_SMALL_NOMINAL);
			navigation.rotateToOrientationDeciDegrees(0);
			break;

		case 11:
			smallActuator.setPumpRearRight_asBlue(true);
			smallActuator.setArmRight_asBlue(ServoPosition::Working);
			navigation.moveToBackwards(Point(1520, robotGeometry.lateralWidth + 15));
			navigation.getControl().setSpeedIndex(SPEED_SMALL_SAFE);
			navigation.moveToBackwards(Point(1490, robotGeometry.lateralWidth + 15));
			break;

		case 12:
			smallActuator.setArmRight_asBlue(ServoPosition::Center);
			navigation.getControl().setSpeedIndex(SPEED_SMALL_NOMINAL);
			break;

		case 13:
			navigation.rotateToOrientationDeciDegrees(900);
			break;

		case 14:
			navigation.moveToBackwards(Point(1490, robotGeometry.rearWidth + 10));
			navigation.repositionMM(-200, CURRENT, robotGeometry.rearWidth, 900);
			navigation.moveMM(150);
			break;

		case 15:
			navigation.moveTo(Point(1400, 670));
			break;

		case STEP_FINALIZATION:
			navigation.getControl().setSpeedIndex(SPEED_SMALL_NOMINAL);
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
