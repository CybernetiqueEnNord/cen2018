/*
 * SmallNominalTaskList.cpp
 *
 *  Created on: 6 oct. 2019
 *      Author: Anastaszor
 */

#include "SmallNominalTaskList.h"

#include "devices/actuators/2020/small/SmallActuator.h"
#include "utils/Arrays.h"
#include "utils/Pointer.h"
#include "match/MatchElement.h"
#include "match/MatchTarget.h"
#include "match/SingleActionList.h"
#include "SmallEndAtBayAction.h"
#include "SmallLighthouseAction.h"
#include "SmallWindsockAction.h"
#include "SmallGetFirstBuoyAction.h"
#include "SmallUnloadAllSmallPortAction.h"
#include "SmallUnloadAllSmallPortAlternativeAction.h"
#include "SmallOurReservedRackAction.h"
#include "devices/databus/2020/DataBus2020.h"

class LighthouseElementCallback: public MatchElementCallback {
private:
	MatchData2020 &matchData;

protected:
	virtual MatchElementState customGetState() const override {
		return MatchElementState::Unavailable;

		static bool activated = false;
		if (activated) {
			return MatchElementState::Unavailable;
		}
		// la t�che n'est jamais disponible pendant les premi�res secondes du match
		int elapsed = matchData.getTimer().getElapsedTimeMS();
		if (elapsed < 15000) {
			return MatchElementState::Unavailable;
		}
		extern DataBus2020 dataBus;
		bool comunicationSuccess = dataBus.queryLightHouse(activated);
		if (comunicationSuccess && !activated) {
			return MatchElementState::Available;
		} else {
			return MatchElementState::Unavailable;
		}
	}

public:
	LighthouseElementCallback(MatchData2020 &data) :
			matchData(data) {
	}
};

SmallNominalTaskList::SmallNominalTaskList(MatchData2020 &matchData) :
		MatchTaskList(), matchData(matchData), tasks(createTasks()) {
	// nothing to do
}

unsigned int SmallNominalTaskList::getCount() const {
	return ARRAY_SIZE(tasks);
}

MatchTask& SmallNominalTaskList::getTaskAt(unsigned int index) const {
	return tasks[index];
}

MatchTask (& SmallNominalTaskList::createTasks())[6] {

			static MatchElement getFirstsBuoy;
			static SmallGetFirstBuoyAction getFirstBuoyAction(matchData);
			static SingleActionList smallGetFirstBuoyActions(getFirstBuoyAction);
			static MatchTarget smallGetFirstBuoyTarget(smallGetFirstBuoyActions, getFirstsBuoy);
			static MatchTask smallGetFirstBuoyTask("firstBuoy", 4, -100, Pointer<MatchTarget>(&smallGetFirstBuoyTarget));

			static MatchElement windsocks;
			static SmallWindsockAction smallWindsockAction(matchData);
			static SingleActionList smallWindsockActions(smallWindsockAction);
			static MatchTarget smallWindsockTarget(smallWindsockActions, windsocks);
			static MatchTask smallWindsockTask("windsocks", 15, 0, Pointer<MatchTarget>(&smallWindsockTarget));

			static MatchElement reservedRack;
			static SmallOurReservedRackAction smallOurReservedRackAction(matchData);
			static SingleActionList smallOurReservedRackActions(smallOurReservedRackAction);
			static MatchTarget smallOurReservedRackTarget(smallOurReservedRackActions, reservedRack);
			static MatchTask smallOurReservedRackTask("reservedRack", 15, 10, Pointer<MatchTarget>(&smallOurReservedRackTarget));

			static MatchElement unloadSmallPort;
			static SmallUnloadAllSmallPortAction smallUnloadAllSmallPortAction(matchData);
			static SingleActionList smallUnloadAllSmallPortActions(smallUnloadAllSmallPortAction);
			static MatchTarget smallUnloadAllSmallPortTarget(smallUnloadAllSmallPortActions, unloadSmallPort);
			static MatchTask smallUnloadAllSmallPortTask("unloadSmall", 10, 20, Pointer<MatchTarget>(&smallUnloadAllSmallPortTarget));

			static MatchElement unloadSmallPortAlternative;
			static SmallUnloadAllSmallPortAlternativeAction smallUnloadAllSmallPortAlternativeAction(matchData);
			static SingleActionList smallUnloadAllSmallPortAlternativeActions(smallUnloadAllSmallPortAlternativeAction);
			static MatchTarget smallUnloadAllSmallPortAlternativeTarget(smallUnloadAllSmallPortAlternativeActions, unloadSmallPort);
			static MatchTask smallUnloadAllSmallPortAlternativeTask("unloadSmallAlternative", 10, 25, Pointer<MatchTarget>(&smallUnloadAllSmallPortAlternativeTarget));

			static LighthouseElementCallback lighthouseElementCallback(matchData);
			static MatchElement lighthouse;
			lighthouse.setCustomGetState(lighthouseElementCallback);
			static SmallLighthouseAction smallLighthouseAction(matchData);
			static SingleActionList smallLighthouseActions(smallLighthouseAction);
			static MatchTarget smallLighthouseTarget(smallLighthouseActions, lighthouse);
			static MatchTask smallLighthouseTask("lighthouse", 15, 5, Pointer<MatchTarget>(&smallLighthouseTarget));

//			static MatchElement bay;
//			static SmallEndAtBayAction smallEndAtBayAction(matchData);
//			static SingleActionList smallEndAtBayActions(smallEndAtBayAction);
//			static MatchTarget smallEndAtBayTarget(smallEndAtBayActions, bay);
//			static MatchTask smallEndAtBayTask("endAtBay", 5, 100, Pointer<MatchTarget>(&smallEndAtBayTarget));

			static MatchTask tasks[] = { smallGetFirstBuoyTask, smallWindsockTask, smallOurReservedRackTask, smallUnloadAllSmallPortTask, smallUnloadAllSmallPortAlternativeTask, smallLighthouseTask /*, smallEndAtBayTask*/};

			return tasks;
		}
