/*
 * SmallWindsockAction.cpp
 *
 *  Created on: 6 oct. 2019
 *      Author: Anastaszor
 */

#include "SmallWindsockAction.h"

#include "../../../../control/Navigation.h"
#include "../../../../devices/actuators/2020/small/SmallActuator.h"
#include "devices/host/HostDevice.h"

SmallWindsockAction::SmallWindsockAction(MatchData2020 &matchData) :
		MatchAction("smWindsock", Point(1600, 300)), matchData(matchData) {
	// nothing to do
}

void SmallWindsockAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;

	int oldSpeed = 0;
	switch (step) {
		case 0:
			smallActuator.setFrontBothLifter(ServoPosition::Center);
			navigation.moveToBackwards(Point(1840, 145));
			break;

		case 1:
			navigation.rotateToOrientationDeciDegrees(900);
			smallActuator.setMancheAAirArm(true);
			smallActuator.waitForReady(1000);
			break;

		case 2:
			oldSpeed = navigation.getControl().getSpeedIndex();
			navigation.getControl().setSpeedIndex(SPEED_SMALL_NOMINAL);
			navigation.moveTo(Point(1840, 670));
			navigation.waitForDelay(500);
			matchData.toggleNearestWindsock();
			navigation.getControl().setSpeedIndex(oldSpeed);
			break;

		case 3:
			oldSpeed = navigation.getControl().getSpeedIndex();
			navigation.getControl().setSpeedIndex(SPEED_SMALL_NOMINAL);
			navigation.rotateToOrientationDeciDegrees(1800);
			matchData.toggleFarestWindsock();
			navigation.getControl().setSpeedIndex(oldSpeed);
			break;

		case 4:
			smallActuator.setMancheAAirArm(false);
			navigation.waitForDelay(500);
			break;

		case 5: {
			// recalage arri�re en X
			const RobotGeometry &geometry = HostDevice::getHostData().getGeometry();
			navigation.moveMM(-geometry.rearWidth + 10);
			navigation.repositionMM(-300, 2000 - geometry.rearWidth, CURRENT, 1800);
			break;
		}

		case 6:
			navigation.moveTo(Point(1600, 670));
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}

}
