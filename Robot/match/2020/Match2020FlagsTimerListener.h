/*
 * Match2020FlagsTimerListener.h
 *
 *  Created on: 16 nov. 2019
 *      Author: emmanuel
 */

#ifndef MATCH_2020_MATCH2020FLAGSTIMERLISTENER_H_
#define MATCH_2020_MATCH2020FLAGSTIMERLISTENER_H_

#include "devices/timer/MatchTimer.h"

class Match2020;

class Match2020FlagsTimerListener : public MatchTimerListener {
private:
	Match2020 &match;

protected:
	virtual void onMatchTimeElapsed(const MatchTimer &timer) override;

public:
	Match2020FlagsTimerListener(Match2020 &match);
};

#endif /* MATCH_2020_MATCH2020FLAGSTIMERLISTENER_H_ */
