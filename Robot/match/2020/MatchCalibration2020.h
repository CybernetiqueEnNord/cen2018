/*
 * MatchCalibration.h
 *
 *  Created on: 28 sep. 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2020_MATCHCALIBRATION_H_
#define MATCH_2020_MATCHCALIBRATION_H_

#include "control/Navigation.h"
#include "devices/host/HostDataF446REBig.h"
#include "devices/host/HostDataF446RENextGenBig.h"
#include "devices/host/HostDataF446RENextGenSmall.h"
#include "devices/host/HostDataF446RESmall.h"
#include "match/2020/MatchData2020.h"

class MatchCalibration2020 {
private:
	Navigation &navigation;

	void prepareAngularCalibration();
	void prepareLinearCalibration();
	void prepareLinearTest();

	void executeAngularCalibration();
	void executeTestRecallage();
	void executeLinearCalibration();
	void executeLinearTest();

public:
	MatchCalibration2020(Navigation &navigation);
	void prepareCalibrationSmall(int trajectoryIndex);
	void executeCalibrationSmall(int trajectoryIndex);
	void prepareCalibrationBig(int trajectoryIndex);
	void executeCalibrationBig(int trajectoryIndex);

	void displayTrajectory(int trajectoryIndex);
};

#endif /* MATCH_2020_MATCHCALIBRATION_H_ */
