/*
 * MatchData2020.cpp
 *
 *  Created on: 28 sep. 2019
 *      Author: Emmanuel
 */

#include "MatchData2020.h"

#include "devices/databus/2020/DataBus2020.h"

MatchData2020::MatchData2020(const MatchTimer &timer) :
		MatchData(timer) {
	nearestWindsockToggled = false;
	farestWindsockToggled = false;
	lighthousePresent = false;
	lighthouseUplifted = false;
	lodgesUplifted = false;
	flagRaised = false;
	expectedAnchoredArea = Match2020AnchorArea::UNKNOWN;
	effectiveAnchoredArea = Match2020AnchorArea::UNKNOWN;
}

Match2020BouyState& MatchData2020::getBoardState() {
	return tableState;
}

void MatchData2020::initializePlayer(Match2020PlayerColor pcolor) {
	tableState.intializeColor(pcolor);
	updateScore();
}

void MatchData2020::initializeRandom(Code pattern) {
	tableState.initializeRandom(pattern);
	updateScore();
}

void MatchData2020::initializeAnchor(Match2020AnchorArea area) {
	expectedAnchoredArea = area;
	updateScore();
}

void MatchData2020::toggleNearestWindsock() {
	nearestWindsockToggled = true;
	updateScore();
}

void MatchData2020::toggleFarestWindsock() {
	farestWindsockToggled = true;
	updateScore();
}

void MatchData2020::setupLighthouse() {
	lighthousePresent = true;
	updateScore();
}

void MatchData2020::upliftLighthouse() {
	lighthouseUplifted = true;
	updateScore();
}

void MatchData2020::upliftLodges() {
	lodgesUplifted = true;
	updateScore();
}

void MatchData2020::anchorDown(Match2020AnchorArea area) {
	effectiveAnchoredArea = area;
	updateScore();
}

void MatchData2020::raiseFlag() {
	flagRaised = true;
	updateScore();
}

void MatchData2020::unload(BigBuoySensorData data, FrontRear frontRear, Match2020Chenal chenal) {
	int count = frontRear == FrontRear::Front ? data.countFront() : data.countBack();
	while (count-- > 0) {
		unload(Match2020BouyColor::BOUY_RED, chenal, Match2020ChenalColor::CHENAL_NONE);
	}
}

void MatchData2020::unload(Match2020BouyColor load, Match2020Chenal chenal, Match2020ChenalColor chenalColor) {
	switch (load) {
		case Match2020BouyColor::NOT_A_BOUY:
			break;
		case Match2020BouyColor::BOUY_GREEN:
			switch (chenalColor) {
				case Match2020ChenalColor::CHENAL_NONE:
					switch (chenal) {
						case Match2020Chenal::NOWHERE:
							break;
						case Match2020Chenal::STARTING_PORT:
							startingDock.putBouyOnDock();
							break;
						case Match2020Chenal::LITTLE_PORT:
							littleDock.putBouyOnDock();
							break;
					}
					break;
				case Match2020ChenalColor::CHENAL_RED:
					switch (chenal) {
						case Match2020Chenal::NOWHERE:
							break;
						case Match2020Chenal::STARTING_PORT:
							startingDock.putBouyOnDock();
							break;
						case Match2020Chenal::LITTLE_PORT:
							littleDock.putBouyOnDock();
							break;
					}
					break;
				case Match2020ChenalColor::CHENAL_GREEN:
					switch (chenal) {
						case Match2020Chenal::NOWHERE:
							break;
						case Match2020Chenal::STARTING_PORT:
							startingDock.putBouyGreenOnGreen();
							break;
						case Match2020Chenal::LITTLE_PORT:
							littleDock.putBouyGreenOnGreen();
							break;
					}
					break;
			}
			break;
		case Match2020BouyColor::BOUY_RED:
			switch (chenalColor) {
				case Match2020ChenalColor::CHENAL_NONE:
					switch (chenal) {
						case Match2020Chenal::NOWHERE:
							break;
						case Match2020Chenal::STARTING_PORT:
							startingDock.putBouyOnDock();
							break;
						case Match2020Chenal::LITTLE_PORT:
							littleDock.putBouyOnDock();
							break;
					}
					break;
				case Match2020ChenalColor::CHENAL_RED:
					switch (chenal) {
						case Match2020Chenal::NOWHERE:
							break;
						case Match2020Chenal::STARTING_PORT:
							startingDock.putBouyRedOnRed();
							break;
						case Match2020Chenal::LITTLE_PORT:
							littleDock.putBouyRedOnRed();
							break;
					}
					break;
				case Match2020ChenalColor::CHENAL_GREEN:
					switch (chenal) {
						case Match2020Chenal::NOWHERE:
							break;
						case Match2020Chenal::STARTING_PORT:
							startingDock.putBouyOnDock();
							break;
						case Match2020Chenal::LITTLE_PORT:
							littleDock.putBouyOnDock();
							break;
					}
					break;
			}
			break;
	}
	updateScore();
}

void MatchData2020::unloadPattern(Code pattern, BigBuoySensorData sensors, FrontRear frontOrRear, Match2020Chenal chenal, Match2020ChenalColor chenalColor) {
	int red = 0;
	int green = 0;
	switch (pattern) {
		case Code::VRVVR_VRRVR_1:
			if (frontOrRear == FrontRear::Front) {
				red = 0;
				green = sensors.countFront();
			} else {
				red = sensors.countBack();
				green = 0;
			}
			break;

		case Code::VVRVR_VRVRR_2:
			if (frontOrRear == FrontRear::Front) {
				red = 0;
				green = sensors.countFront();
			} else {
				red = sensors.countBack();
				green = 0;
			}
			break;

		case Code::VVVRR_VVRRR_3:
			if (frontOrRear == FrontRear::Front) {
				red = 0;
				green = sensors.countFront();
			} else {
				red = sensors.countBack();
				green = 0;
			}
			break;

		case Code::RVRVR:
			if (frontOrRear == FrontRear::Front) {
				for (int i = 0; i < 5; i++) {
					int present = sensors.isPresent(i) ? 1 : 0;
					if (i % 2 == 0) {
						red += present;
					} else {
						green += present;
					}
				}
			}
			break;

		case Code::Undefined:
		case Code::VVVVR_VRRRR_4:
		default:
			if (frontOrRear == FrontRear::Front) {
				red = sensors.countFront();
				green = 0;
			} else {
				red = 1;
				green = sensors.countBack() - 1; // random estimation
				if (green < 0) // estimation correction
					red = 0;
			}
			break;
	}
	for (int i = 0; i < red; i++) {
		this->unload(Match2020BouyColor::BOUY_RED, chenal, chenalColor);
	}
	for (int i = 0; i < green; i++) {
		this->unload(Match2020BouyColor::BOUY_GREEN, chenal, chenalColor);
	}
}

int MatchData2020::getComputedScore() const {
	int s = 0;
	if (nearestWindsockToggled || farestWindsockToggled)
		s += 5;
	if (nearestWindsockToggled && farestWindsockToggled)
		s += 10;
	if (lighthousePresent) {
		s += 1; //2point, but each robot display half.
		if (lighthouseUplifted) {
			s += 13;
		}
	}

	/*
	if (effectiveAnchoredArea != Match2020AnchorArea::UNKNOWN) {
		if (expectedAnchoredArea == Match2020AnchorArea::UNKNOWN) {
			//If unknown, 1 chance sur 2 que l'on ai les deux robots dans la bonne zone. donc, soit 10, soit 5. Moyenne 7.5
			//Divisé par deux: 3.75 points
			s += 4;
		} else {
			if (expectedAnchoredArea == effectiveAnchoredArea) {
				s += 5; //5 points for each robots.
			}
		}
	}
	*/

	if (effectiveAnchoredArea != Match2020AnchorArea::UNKNOWN) {
		s += 6;
		if (expectedAnchoredArea == Match2020AnchorArea::NORTH) {
			s += 1;
		}
	}

	if (flagRaised) {
		s += 5; //5 points for each robots.
	}

	s += startingDock.countPoints();
	s += littleDock.countPoints();
	return s;
}

void MatchData2020::setScore(int value) {
	MatchData::setScore(value);
	extern DataBus2020 dataBus;
	dataBus.notifyScore(value);
}
