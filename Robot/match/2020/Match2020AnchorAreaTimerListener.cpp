/*
 * Match2020AnchorAreaTimerListener.cpp
 *
 *  Created on: 30 mai 2020
 *      Author: emmanuel
 */

#include "Match2020AnchorAreaTimerListener.h"

#include "match/2020/Match2020.h"

Match2020AnchorAreaTimerListener::Match2020AnchorAreaTimerListener(Match2020 &match) :
		match(match) {
}

void Match2020AnchorAreaTimerListener::onMatchTimeElapsed(const MatchTimer& timer) {
	(void) timer;

	match.setMoveToAnchorArea();
}
