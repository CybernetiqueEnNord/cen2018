/*
 * MatchActionUtils2020.cpp
 *
 *  Created on: 6 juin 2020
 *      Author: Robin
 */

#include "MatchActionUtils2020.h"

void MatchActionUtils2020::waitForGobeletStabilisationInRack(Navigation &navigation)
{
	navigation.waitForDelay(500);
}

void MatchActionUtils2020::waitForHold(Navigation &navigation) {
	//After we ask to hold, wait before raising,to avoid miss gobelet
	navigation.waitForDelay(300);
}




