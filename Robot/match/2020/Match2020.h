/*
 * Match2020.h
 *
 *  Created on: 28 sep. 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_MATCH2020_H_
#define MATCH_MATCH2020_H_

#include "MatchCalibration2020.h"
#include "match/Match.h"
#include "match/MatchChoiceCollection.h"
#include "match/MatchChoice.h"
#include "match/MatchExecutor.h"
#include "match/2020/MatchData2020.h"
#include "match/2020/Match2020AnchorAreaTimerListener.h"
#include "match/2020/Match2020FlagsTimerListener.h"
#include "match/2020/Match2020StrategyIndex.h"
#include "control/Navigation.h"

class Match2020 : public Match, public NavigationListener {
private:
	bool calibration = false;
	MatchCalibration2020 calibrator;
	MatchExecutor executor;

	MatchTimer flagsTimer;
	Match2020FlagsTimerListener flagsTimerListener = Match2020FlagsTimerListener(*this);
	bool raiseFlags = false;

	MatchTimer anchorTimer;
	Match2020AnchorAreaTimerListener anchorTimerListener = Match2020AnchorAreaTimerListener(*this);
	bool moveToAnchorArea = false;

	void changeMatchSide(bool newSide);
	void changeTrajectory(int trajectoryIndex);
	void displayMatchSide();
	void displayTrajectory();
	void updateMatchSide();
	void updateTrajectory();

	MatchTaskList& getTestsTasksList();
	void executeTest(int taskIndex);

protected:
	MatchChoiceCollection &matchChoices;


	bool hasCapability(unsigned int capability) const;

	virtual MatchData& createMatchData(const MatchTimer& timer);
	virtual MatchChoiceCollection& createMatchChoices();
	virtual MatchChoiceCollection& createSmallMatchChoices();
	virtual MatchChoiceCollection& createBigMatchChoices();

	virtual void execute();
	virtual void finish() override;
	virtual void initialize();
	virtual void prepare();
	virtual void onNavigationEvent(const Navigation &navigation);

	virtual void terminateNavigation();
	virtual void update() override;

public:
	int trajectoryIndex = STRATEGY_INDEX_NOMINAL;
	bool trajectorySelectable = true;

	Match2020(Navigation &navigation);
	void setMoveToAnchorArea();
	void setRaiseFlags();
	virtual void terminate();
};

#endif /* MATCH_MATCH2020_H_ */
