/*
 * Match2020.cpp
 *
 *  Created on: 11 oct. 2020
 *      Author: Anastaszor
 */

#include "Match2020.h"

#include "../../../ChibiOS_17.6.0/os/rt/include/chthreads.h"
#include "../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "../../control/PositionControl.h"
#include "../../devices/actuators/2020/big/BigActuator.h"
#include "../../devices/actuators/2020/small/SmallActuator.h"
#include "../../devices/beacon/Beacon2018.h"
#include "../../devices/host/HostCapabilities2020.h"
#include "../../devices/host/HostDataF446REBig.h"
#include "../../devices/host/HostDataF446RENextGenBig.h"
#include "../../devices/host/HostDataF446RENextGenSmall.h"
#include "../../devices/host/HostDataF446RESmall.h"
#include "../../devices/host/HostDevice.h"
#include "../../devices/host/RobotGeometry.h"
#include "../../devices/io/LCDScreen.h"
#include "../../odometry/Position.h"
#include "../../utils/Pointer.h"
#include "../../utils/Text.h"
#include "../MatchTarget.h"
#include "../MatchTask.h"
#include "match/MatchChoice.h"
#include "devices/databus/2020/DataBus2020.h"
#include "choices/MatchChoice2020BigDraft.h"
#include "choices/MatchChoice2020BigHomologation.h"
#include "choices/MatchChoice2020BigNominal.h"
#include "choices/MatchChoice2020BigSafe.h"
#include "choices/MatchChoice2020BigFast.h"
#include "choices/MatchChoice2020SmallDraft.h"
#include "choices/MatchChoice2020SmallHomologation.h"
#include "choices/MatchChoice2020SmallNominal.h"
#include "choices/MatchChoice2020SmallSafe.h"
#include "points/Match2020BouyState.h"
#include "points/Match2020Dock.h"
#include "Match2020StrategyIndexCalibration.h"

Match2020::Match2020(Navigation &navigation) :
		Match(navigation, createMatchData(matchTimer)), calibrator(navigation), executor(navigation), /* TODO as the executor is in the matchChoice, is it needed here ? */
		matchChoices(createMatchChoices()) {
	flagsTimer.initialize(95000);
	flagsTimer.listener = &flagsTimerListener;

	anchorTimer.initialize(85000);
	anchorTimer.listener = &anchorTimerListener;
}

MatchChoiceCollection& Match2020::createBigMatchChoices() {
	static MatchChoice2020BigHomologation m1(navigation, matchData);
	static MatchChoice2020BigNominal m2(navigation, matchData);
	static MatchChoice2020BigSafe m3(navigation, matchData);
	static MatchChoice2020BigDraft m4(navigation, matchData);
	static MatchChoice2020BigFast m5(navigation, matchData);
	static MatchChoice2020BigNominal m6(navigation, matchData);
	static MatchChoice2020BigNominal m7(navigation, matchData);
	static MatchChoice2020BigNominal m8(navigation, matchData);
	static MatchChoiceCollection bmc(m1, m2, m3, m4, m5, m6, m7, m8);
	return bmc;
}

MatchChoiceCollection& Match2020::createSmallMatchChoices() {
	static MatchChoice2020SmallHomologation m1(navigation, matchData);
	static MatchChoice2020SmallNominal m2(navigation, matchData);
	static MatchChoice2020SmallSafe m3(navigation, matchData);
	static MatchChoice2020SmallDraft m4(navigation, matchData);
	static MatchChoice2020SmallNominal m5(navigation, matchData);
	static MatchChoice2020SmallNominal m6(navigation, matchData);
	static MatchChoice2020SmallNominal m7(navigation, matchData);
	static MatchChoice2020SmallNominal m8(navigation, matchData);
	static MatchChoiceCollection smc(m1, m2, m3, m4, m5, m6, m7, m8);
	return smc;
}

void Match2020::execute() {
	flagsTimer.start();
	anchorTimer.start();

	extern LCDScreen screen;
	screen.displayMessage("C'est parti !");

	PositionControl &control = navigation.getControl();
	// d�tection d'obstacle active par d�faut sauf pour la calibration
	control.obstacleAvoidance = !calibration;

	extern Beacon2018 beacon;
	beacon.setBeepEnabled(!control.simulateCoders);

	extern DataBus2020 dataBus;
	dataBus.notifyMatchStart();

	if (calibration) {
		bool test = trajectoryIndex >= CALIBRATION_STRATEGY_COUNT;
		if (test) {
			executeTest(trajectoryIndex - CALIBRATION_STRATEGY_COUNT);
		} else {
			if (HostDevice::isSmall()) {
				calibrator.executeCalibrationSmall(trajectoryIndex);
			} else if (HostDevice::isBig()) {
				calibrator.executeCalibrationBig(trajectoryIndex);
			} else {
				extern LCDScreen screen;
				screen.displayMessage("Calibration Hote non pris en charge");
			}
		}
	} else {

		control.maxRepositionErrorDistanceMM = 30;
		control.maxRepositionErrorAngleDeciDegree = 70;
		control.ignoreRepositionError = false;

		matchChoices.execute(trajectoryIndex);
	}

	setActionResult(ActionResult::Next);
}

void Match2020::prepare() {
	extern Beacon2018 beacon;
	beacon.setLedBlinking(100, 100, 0);

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = false;

	//Set threshold on all STRATEGY, and enable beep. => Beep to be tested during initialisation.
	beacon.setThreshold(500, 350);
	beacon.setBeepEnabled(false);

	displayMatchSide();
	displayTrajectory();

	// Initialisation match side sur les actionneurs
	bool side = control.isReversed();
	const HostData &data = HostDevice::getHostData();
	unsigned int capabilities = data.getCapabilities();

	if ((capabilities & CAPABILITY_SMALL) != 0) {
		extern SmallActuator smallActuator;
		smallActuator.setMatchSide(side);
	}
	if ((capabilities & CAPABILITY_BIG) != 0) {
		extern BigActuator bigActuator;
		bigActuator.setMatchSide(side);
	}

	if (calibration) {
		if (HostDevice::isSmall()) {
			calibrator.prepareCalibrationSmall(trajectoryIndex);
		} else if (HostDevice::isBig()) {
			calibrator.prepareCalibrationBig(trajectoryIndex);
		} else {
			extern LCDScreen screen;
			screen.displayMessage("Calibration Hote non pris en charge");
		}
	} else {
		MatchData2020 &mdata = (MatchData2020&) matchData;
		matchChoices.prepare(trajectoryIndex);
		mdata.setupLighthouse();
	}

	navigation.waitForStart(this);
}

void Match2020::executeTest(int taskIndex) {
	// Tests unitaires
	MatchTaskList &list = getTestsTasksList();
	for (int i = 0; i < (signed) list.getCount(); i++) {
		if (i != taskIndex) {
			MatchTask &task = list.getTaskAt(i);
			task.setDone();
		}
	}
	executor.setTasksList(list);
	executor.run();
}

void Match2020::initialize() {
	Match::initialize();

	displayMatchSide();
	displayTrajectory();

	extern Beacon2018 beacon;
	beacon.setLedBlinking(100, 400, 0);

//	extern BigActuator bigActuator;

	const Beacon2018Data &data = beacon.getData();
	do {
		beacon.updateHMIData();
		updateMatchSide();
		if (trajectorySelectable) {
			updateTrajectory();
		}
		chThdSleep(50);
	} while (data.started);
}

void Match2020::onNavigationEvent(const Navigation &navigation) {
	(void) navigation;
}

void Match2020::update() {
	MatchData2020 &mdata = (MatchData2020&) matchData;

	if (raiseFlags) {
		extern LCDScreen screen;
		screen.displayMessage("      DRAPEAUX");

		if (hasCapability(CAPABILITY_SMALL)) {
			extern SmallActuator smallActuator;
			smallActuator.raiseFlags();
			mdata.raiseFlag();
		}
		if (hasCapability(CAPABILITY_BIG)) {
			extern BigActuator bigActuator;
			bigActuator.raiseFlags();
			mdata.raiseFlag();
		}
		raiseFlags = false;
	}

	if (moveToAnchorArea) {
		Pointer<MatchChoice> choice = matchChoices.getMatchChoice(trajectoryIndex);
		// interrompt la liste de t�ches en cours
		if (choice.isValid()) {
			choice->abort();
		}
		moveToAnchorArea = false;
	}
}

void Match2020::setRaiseFlags() {
	raiseFlags = true;
}

void Match2020::finish() {
	matchChoices.finish(trajectoryIndex);
	Match::finish();
}

void Match2020::setMoveToAnchorArea() {
	moveToAnchorArea = true;
}

MatchChoiceCollection& Match2020::createMatchChoices() {
	if (HostDevice::isSmall()) {
		return createSmallMatchChoices();
	} else if (HostDevice::isBig()) {
		return createBigMatchChoices();
	} else {
		extern LCDScreen screen;
		screen.displayMessage("Hote non pris en charge");
		return createSmallMatchChoices();
	}
}

void Match2020::terminate() {
	const PositionControl &control = navigation.getControl();
	MoveError error = control.getError();

	// arr�t actionneurs
	if (hasCapability(CAPABILITY_SMALL)) {
		extern SmallActuator smallActuator;
		smallActuator.kill();
	}
	if (hasCapability(CAPABILITY_BIG)) {
		extern BigActuator bigActuator;
		bigActuator.kill();
	}

	Match::terminate();

	extern LCDScreen screen;
	if (navigation.getCommand() == MoveCommand::EmergencyStop) {
		screen.displayMessage("  ARRET D'URGENCE");
	} else if (error == MoveError::Overrun) {
		screen.displayMessage("      OVERRUN");
	} else {
		screen.displayMessage("   MATCH TERMINE");
	}
}

void Match2020::displayMatchSide() {
	extern LCDScreen screen;
	bool side = navigation.getControl().isReversed();
	if (calibration) {
		screen.displayMessage(side ? "CalibJ" : "CalibB");
	} else {
		screen.displayMessage(side ? "Jaune" : "Bleu");
	}
}

void Match2020::displayTrajectory() {
	extern LCDScreen screen;

	if (calibration) {
		if (trajectoryIndex < CALIBRATION_STRATEGY_COUNT) {
			calibrator.displayTrajectory(trajectoryIndex);
		} else {
			// Tests unitaires
			MatchTaskList &list = getTestsTasksList();
			unsigned int index = trajectoryIndex - CALIBRATION_STRATEGY_COUNT;
			if (index >= list.getCount()) {
				extern LCDScreen screen;
				screen.displayMessage("none", 8); //"!Act nbr:" + trajectoryIndex - STRATEGY_INDEX_NOMINAL_QUANTITY + " Mx:" + list.getCount());
			} else {
				MatchTask &task = list.getTaskAt(index);
				screen.displayMessage(task.getLabel(), 8);
			}
		}
	} else {
		screen.displayMessage(matchChoices.getName(trajectoryIndex), 8);
	}

}

void Match2020::changeMatchSide(bool newSide) {
	PositionControl &control = navigation.getControl();
	control.setReversed(newSide);
	extern Beacon2018 beacon;
	beacon.setReversed(newSide);
	displayMatchSide();
}

void Match2020::changeTrajectory(int trajectoryIndex) {
	this->trajectoryIndex = trajectoryIndex;
	displayTrajectory();
}

void Match2020::updateMatchSide() {
	extern Beacon2018 beacon;
	const Beacon2018Data &data = beacon.getData();
	bool side = data.hmiValue1 >= 128;
	bool calibration = (data.hmiValue1 > 96) && (data.hmiValue1 < 160);
	PositionControl &control = navigation.getControl();
	if (side != control.isReversed() || calibration != this->calibration) {
		this->calibration = calibration;
		changeMatchSide(side);
	}

}

void Match2020::updateTrajectory() {
	extern Beacon2018 beacon;
	const Beacon2018Data &data = beacon.getData();
	// 20 positions en calibration, 8 en match
	int index = data.hmiValue2 / (calibration ? 13 : 36);
	if (index != trajectoryIndex) {
		changeTrajectory(index);
	}
}

bool Match2020::hasCapability(unsigned int capability) const {
	const HostData &data = HostDevice::getHostData();
	unsigned int capabilities = data.getCapabilities();
	bool result = (capabilities & capability) != 0;
	return result;
}

void Match2020::terminateNavigation() {
	// Pas d'arret si procedure de recalage
	if (calibration) {
		return;
	}
	Match::terminateNavigation();
}

MatchData& Match2020::createMatchData(const MatchTimer &timer) {
	static MatchData2020 data(timer);
	return data;
}

MatchTaskList& Match2020::getTestsTasksList() {
	class EmptyTaskList: public MatchTaskList {
	protected:
		virtual unsigned int getCount() const {
			return 0;
		}
		virtual MatchTask& getTaskAt(unsigned int index) const {
			(void) index;
			static MatchTask none("none", 0, 0, Pointer<MatchTarget>());
			return none;
		}
	};
	static EmptyTaskList tasks;
	return tasks;
}
