/*
 * MatchDock.h
 *
 *  Created on: 29 sept. 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2020_MATCH2020DOCK_H_
#define MATCH_2020_MATCH2020DOCK_H_

/**
 * MatchDock class file.
 *
 * This class represents one of the two docks in the field where bouys can
 * be placed by the robots. Those fields are the final places of the bouys
 * and particular care should be taken when elements are placed on those
 * places.
 */
class Match2020Dock {
private:
	int nb_green_on_green;
	int nb_red_on_red;
	int nb_on_dock;

public:
	Match2020Dock();

	/**
	 * Puts one red bouy on red line if there is more space available.
	 * Maximum space is 6.
	 */
	void putBouyRedOnRed();

	/**
	 * Puts one green bouy on green line if there is more space available.
	 * Maximum space is 6.
	 */
	void putBouyGreenOnGreen();

	/**
	 * Puts a bouy of any color on the field. There is no maximum space here.
	 */
	void putBouyOnDock();

	/**
	 * Counts the points for any bouy placement. The discount is as follow :
	 * - 2 points for any green bouy on the green line
	 * - 2 points for any red bouy on the red line
	 * - 2 points for any red on red where there is an equivalent green on green
	 * - 1 point for any bouy on dock
	 *
	 * @return int the calculated number of points
	 */
	int countPoints() const;
	void sendToConsole() const;
};

#endif /* MATCH_2020_MATCH2020DOCK_H_ */
