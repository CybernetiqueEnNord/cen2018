/*
 * Match2020BouyRackConfiguration.h
 *
 *  Created on: 29 sept. 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2020_MATCH2020BOUYRACKCONFIGURATION_H_
#define MATCH_2020_MATCH2020BOUYRACKCONFIGURATION_H_

#include "../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "../Match2020Enums.h"

class Match2020BouyRackConfiguration {

private:

	/**
	 * The configuration of the rack of bouys that is on our side. The bouy
	 * at index 0 is the nearest from point (0,0) and the bouy at index 5
	 * is the nearest from point (2000,0)
	 *
	 * @var Match2020BouyColor[5]
	 */
	Match2020BouyColor our_side[5];

	/**
	 * The configuration GGGRR of the rack of bouys for the one rack that is
	 * randomized. The bouy at index 0 is the nearest from point (0,0) and the
	 * bouy at index 5 is the nearest from point (0,1000)
	 *
	 * @var Match2020BouyColor[5]
	 */
	Match2020BouyColor random_1_our_side[5];

	/**
	 * The configuration GGRGR of the rack of bouys for the one rack that is
	 * randomized. The bouy at index 0 is the nearest from point (0,0) and the
	 * bouy at index 5 is the nearest from point (0,1000)
	 *
	 * @var Match2020BouyColor[5]
	 */
	Match2020BouyColor random_2_middle[5];

	/**
	 * The configuration GRGGR of the rack of bouys for the one rack that is
	 * randomized. The bouy at index 0 is the nearest from point (0,0) and the
	 * bouy at index 5 is the nearest from point (0,1000)
	 *
	 * @var Match2020BouyColor[5]
	 */
	Match2020BouyColor random_3_opp_side[5];

	/**
	 * The configuration GRRRR of the rack of bouys for the one rack that is
	 * randomized. The bouy at index 0 is the nearest from point (0,0) and the
	 * bouy at index 5 is the nearest from point (0,1000)
	 *
	 * @var Match2020BouyColor[5]
	 */
	Match2020BouyColor known_one_error[5];

public:

	/**
	 * Contructor
	 */
	Match2020BouyRackConfiguration();

	/**
	 * Applies the configuration for the given playing player color.
	 *
	 * @param pcolor Match2020PlayerColor the color of the playing player.
	 */
	void applyPlayerColor(Match2020PlayerColor pcolor);

	/**
	 * Gets the real color of the bouy from the expected color when the player
	 * is the default player (blue) (i.e if the player color is blue, returns
	 * the expected color, and if yellow, inverts the bouy color)
	 *
	 * @return Match2020BouyColor
	 */
	Match2020BouyColor forColor(Match2020PlayerColor pcolor, Match2020BouyColor bcolor);

	/**
	 * Gets the color at the given index 0 <= i < 5 for the given pattern.
	 *
	 * @param pattern Code the pattern to use
	 * 		as configuration
	 * @return i int the number of the rack slot (must be < 5)
	 * @return Match2020BouyColor
	 */
	Match2020BouyColor getColor(Code pattern, int i);

};

#endif /* MATCH_2020_MATCH2020BOUYRACKCONFIGURATION_H_ */
