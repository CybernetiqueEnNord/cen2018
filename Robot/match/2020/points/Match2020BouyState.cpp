/*
 * Match2020BouyState.cpp
 *
 *  Created on: 29 sept. 2019
 *      Author: Anastaszor
 */

#include "Match2020BouyState.h"

#include "../../../../Commons/i2c/ActuatorBig2020Commands.h"

Match2020BouyState::Match2020BouyState() :
	config(Match2020BouyRackConfiguration())
{
	initialized = false;
	randomized = false;
	playercolor = Match2020PlayerColor::BLUE;
	n1_c = Match2020BouyColor::NOT_A_BOUY;
	n2_c = Match2020BouyColor::NOT_A_BOUY;
	n3_c = Match2020BouyColor::NOT_A_BOUY;
	n4_c = Match2020BouyColor::NOT_A_BOUY;
	m1_c = Match2020BouyColor::NOT_A_BOUY;
	m2_c = Match2020BouyColor::NOT_A_BOUY;
	m3_c = Match2020BouyColor::NOT_A_BOUY;
	m4_c = Match2020BouyColor::NOT_A_BOUY;
	d1_c = Match2020BouyColor::NOT_A_BOUY;
	d2_c = Match2020BouyColor::NOT_A_BOUY;
	d3_c = Match2020BouyColor::NOT_A_BOUY;
	d4_c = Match2020BouyColor::NOT_A_BOUY;
	o1_c = Match2020BouyColor::NOT_A_BOUY;
	o2_c = Match2020BouyColor::NOT_A_BOUY;
	o3_c = Match2020BouyColor::NOT_A_BOUY;
	o4_c = Match2020BouyColor::NOT_A_BOUY;
	for(int i = 0; i < 5; i++)
	{
		r1_c[i] = Match2020BouyColor::NOT_A_BOUY;
		r2_c[i] = Match2020BouyColor::NOT_A_BOUY;
		r3_c[i] = Match2020BouyColor::NOT_A_BOUY;
	}
}

void Match2020BouyState::intializeColor(Match2020PlayerColor pcolor)
{
	if(initialized) return;
	initialized = true;
	playercolor = pcolor;
	n1_c = config.forColor(pcolor, Match2020BouyColor::BOUY_RED);
	n2_c = config.forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	n3_c = config.forColor(pcolor, Match2020BouyColor::BOUY_RED);
	n4_c = config.forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	m1_c = config.forColor(pcolor, Match2020BouyColor::BOUY_RED);
	m2_c = config.forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	m3_c = config.forColor(pcolor, Match2020BouyColor::BOUY_RED);
	m4_c = config.forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	d1_c = config.forColor(pcolor, Match2020BouyColor::BOUY_RED);
	d2_c = config.forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	d3_c = config.forColor(pcolor, Match2020BouyColor::BOUY_RED);
	d4_c = config.forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	o1_c = config.forColor(pcolor, Match2020BouyColor::BOUY_RED);
	o2_c = config.forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	o3_c = config.forColor(pcolor, Match2020BouyColor::BOUY_RED);
	o4_c = config.forColor(pcolor, Match2020BouyColor::BOUY_GREEN);

	r1_c[0] = config.forColor(pcolor, Match2020BouyColor::BOUY_RED);
	r1_c[1] = config.forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	r1_c[2] = config.forColor(pcolor, Match2020BouyColor::BOUY_RED);
	r1_c[3] = config.forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	r1_c[4] = config.forColor(pcolor, Match2020BouyColor::BOUY_GREEN);

	// in absence of information, take the default config GGGRR
	Match2020PlayerColor oppcolor = Match2020PlayerColor::BLUE;
	if(pcolor == Match2020PlayerColor::BLUE)
		oppcolor = Match2020PlayerColor::YELLOW;
	for(int i = 0; i < 5; i++)
	{
		r2_c[i] = config.forColor(pcolor, config.getColor(Code::VRVVR_VRRVR_1, i));
		r3_c[i] = config.forColor(oppcolor, config.getColor(Code::VRVVR_VRRVR_1, i));
	}
}

void Match2020BouyState::initializeRandom(Code pattern)
{
	for(int i = 0; i < 5; i++)
	{
		if(r2_c[i] != Match2020BouyColor::NOT_A_BOUY)
		{
			r2_c[i] = config.forColor(playercolor, config.getColor(pattern, i));
		}
		if(r3_c[i] != Match2020BouyColor::NOT_A_BOUY)
		{
			r3_c[i] = config.forColor(playercolor, config.getColor(pattern, i));
		}
	}
}

Match2020BouyColor Match2020BouyState::takeN1()
{
	Match2020BouyColor c = n1_c;
	n1_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeN2()
{
	Match2020BouyColor c = n2_c;
	n2_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeN3()
{
	Match2020BouyColor c = n3_c;
	n3_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeN4()
{
	Match2020BouyColor c = n4_c;
	n4_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeM1()
{
	Match2020BouyColor c = m1_c;
	m1_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeM2()
{
	Match2020BouyColor c = m2_c;
	m2_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeM3()
{
	Match2020BouyColor c = m3_c;
	m3_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeM4()
{
	Match2020BouyColor c = m4_c;
	m4_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeD1()
{
	Match2020BouyColor c = d1_c;
	d1_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeD2()
{
	Match2020BouyColor c = d2_c;
	d2_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeD3()
{
	Match2020BouyColor c = d3_c;
	d3_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeD4()
{
	Match2020BouyColor c = d4_c;
	d4_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeO1()
{
	Match2020BouyColor c = o1_c;
	o1_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeO2()
{
	Match2020BouyColor c = o2_c;
	o2_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeO3()
{
	Match2020BouyColor c = o3_c;
	o3_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeO4()
{
	Match2020BouyColor c = o4_c;
	o4_c = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR1_1()
{
	Match2020BouyColor c = r1_c[0];
	r1_c[0] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR1_2()
{
	Match2020BouyColor c = r1_c[1];
	r1_c[1] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR1_3()
{
	Match2020BouyColor c = r1_c[2];
	r1_c[2] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR1_4()
{
	Match2020BouyColor c = r1_c[3];
	r1_c[3] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR1_5()
{
	Match2020BouyColor c = r1_c[4];
	r1_c[4] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR2_1()
{
	Match2020BouyColor c = r2_c[0];
	r2_c[0] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR2_2()
{
	Match2020BouyColor c = r2_c[1];
	r2_c[1] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR2_3()
{
	Match2020BouyColor c = r2_c[2];
	r2_c[2] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR2_4()
{
	Match2020BouyColor c = r2_c[3];
	r2_c[3] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR2_5()
{
	Match2020BouyColor c = r2_c[4];
	r2_c[4] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR3_1()
{
	Match2020BouyColor c = r3_c[0];
	r3_c[0] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR3_2()
{
	Match2020BouyColor c = r3_c[1];
	r3_c[1] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR3_3()
{
	Match2020BouyColor c = r3_c[2];
	r3_c[2] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR3_4()
{
	Match2020BouyColor c = r3_c[3];
	r3_c[3] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}

Match2020BouyColor Match2020BouyState::takeR3_5()
{
	Match2020BouyColor c = r3_c[4];
	r3_c[4] = Match2020BouyColor::NOT_A_BOUY;
	return c;
}
