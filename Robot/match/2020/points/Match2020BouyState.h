/*
 * Match2020BouyState.h
 *
 *  Created on: 29 sept. 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2020_MATCH2020BOUYSTATE_H_
#define MATCH_2020_MATCH2020BOUYSTATE_H_

#include "../../../../Commons/i2c/ActuatorBig2020Commands.h"
#include "../Match2020Enums.h"
#include "Match2020BouyRackConfiguration.h"

class Match2020BouyState {
private:

	Match2020PlayerColor playercolor;
	Match2020BouyRackConfiguration config;

	bool initialized = false;
	bool randomized = false;

	// {{ ours reserved
	// x = 400, y = 300
	Match2020BouyColor n1_c;

	// x = 510, y = 450
	Match2020BouyColor n2_c;

	// x = 1080, y = 450
	Match2020BouyColor n3_c;

	// x = 1200, y = 300
	Match2020BouyColor n4_c;
	// }}}

	// {{{ ours middle
	// x = 100, y = 670
	Match2020BouyColor m1_c;

	// x = 400, y = 950
	Match2020BouyColor m2_c;

	// x = 800, y = 1100
	Match2020BouyColor m3_c;

	// x = 1200, y = 1270
	Match2020BouyColor m4_c;
	// }}}

	// {{{ ours little dock
	// x = 1955, y = 1065
	Match2020BouyColor d1_c;

	// x = 1650, y = 1665
	Match2020BouyColor d2_c;

	// x = 1650, y = 1935
	Match2020BouyColor d3_c;

	// x = 1955, y = 1995
	Match2020BouyColor d4_c;
	// }}}

	// {{{ opp middle
	// x = 1200, y = 1730
	Match2020BouyColor o1_c;

	// x = 800, y = 1900
	Match2020BouyColor o2_c;

	// x = 400, y = 2050
	Match2020BouyColor o3_c;

	// x = 100, y = 2330
	Match2020BouyColor o4_c;
	// }}}}

	// {{{ our rack
	/**
	 * The bouy at index 0 is the nearest from point (0,0)
	 * The bouy at index 5 is the nearest from point (2000,0)
	 */
	Match2020BouyColor r1_c[5];
	// }}}

	// {{{ our neutral rack
	/**
	 * The bouy at index 0 is the nearest from point (0,0)
	 * The bouy at index 5 is the nearest from point (0,1000)
	 */
	Match2020BouyColor r2_c[5];
	// }}}

	// {{{ opp neutral rack
	/**
	 * The bouy at index 0 is the nearest from point (0,2000)
	 * The bouy at index 5 is the nearest from point (0,1000)
	 * WARNING : this is reversed from the r2_c prospective, and should
	 * be integrated when loaded on the actuator.
	 */
	Match2020BouyColor r3_c[5];
	// }}}


public:
	Match2020BouyState();

	/**
	 * Initializes the colors of all known bouys with the given colors of
	 * the player.
	 *
	 * @param pcolor Match2020PlayerColor the color we have to play
	 */
	void intializeColor(Match2020PlayerColor pcolor);

	/**
	 * Initializes the colors of the random rack of bouys according to the
	 * pattern that was choosen. This method does nothing if the bouys were
	 * already taken.
	 *
	 * The method self::initializeColor should be called before this method,
	 * otherwise it will do nothing.
	 *
	 * @param pattern Code the pattern to set colors from
	 */
	void initializeRandom(Code pattern);

	/**
	 * Takes the first bouy on our port (x = 400, y = 300)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeN1();

	/**
	 * Takes the second bouy on our port (x = 510, y = 450)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeN2();

	/**
	 * Takes the third bouy on our port (x = 1080, y = 450)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeN3();

	/**
	 * Takes the fourth bouy on our port (x = 1200, y = 300)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeN4();

	/**
	 * Takes the first bouy on our side (x = 100, y = 670)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeM1();

	/**
	 * Takes the second bouy on our side (x = 400, y = 950)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeM2();

	/**
	 * Takes the third bouy on our side (x = 800, y = 1100)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeM3();

	/**
	 * Takes the fourth bouy on our side (x = 1200, y = 1270)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeM4();

	/**
	 * Takes the first bouy on our little dock (x = 1955, y = 1065)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeD1();

	/**
	 * Takes the second bouy on our little dock (x = 1650, y = 1665)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeD2();

	/**
	 * Takes the third bouy on our little dock (x = 1650, y = 1935)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeD3();

	/**
	 * Takes the fourth bouy on our little dock (x = 1955, y = 1995)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeD4();

	/**
	 * Takes the first bouy on opponent side (x = 1200, y = 1730)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeO1();

	/**
	 * Takes the second bouy on opponent side (x = 800, y = 1900)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeO2();

	/**
	 * Takes the third bouy on opponent side (x = 400, y = 2050)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeO3();

	/**
	 * Takes the fourth bouy on opponent side (x = 100, y = 2330)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeO4();

	/**
	 * Takes the first bouy on our rack (near 0,0)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR1_1();

	/**
	 * Takes the second bouy on our rack
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR1_2();

	/**
	 * Takes the third bouy on our rack
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR1_3();

	/**
	 * Takes the fourth bouy on our rack
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR1_4();

	/**
	 * Takes the fifth bouy on our rack (near 2000,0)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR1_5();

	/**
	 * Takes the first bouy on our random rack (near 0,0)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR2_1();

	/**
	 * Takes the second bouy on our random rack
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR2_2();

	/**
	 * Takes the third bouy on our random rack
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR2_3();

	/**
	 * Takes the fourth bouy on our random rack
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR2_4();

	/**
	 * Takes the fifth bouy on our random rack (near 0,1000)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR2_5();

	/**
	 * Takes the first bouy on opponent random rack (near 0,2000)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR3_1();

	/**
	 * Takes the second bouy on opponent random rack
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR3_2();

	/**
	 * Takes the third bouy on opponent random rack
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR3_3();

	/**
	 * Takes the fourth bouy on opponent random rack
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR3_4();

	/**
	 * Takes the fifth bouy on opponent random rack (near 0,1000)
	 *
	 * @return Match2020BouyColor the color of the bouy, NOT_A_BOUY if the
	 * 		bouy was already taken.
	 */
	Match2020BouyColor takeR3_5();

};

#endif /* MATCH_2020_MATCH2020BOUYSTATE_H_ */
