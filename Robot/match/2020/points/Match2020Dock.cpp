/*
 * Match2020Dock.cpp
 *
 *  Created on: 29 sept. 2019
 *      Author: Anastaszor
 */

#include "Match2020Dock.h"

#include "devices/io/Console.h"

Match2020Dock::Match2020Dock() {
	nb_green_on_green = 0;
	nb_red_on_red = 0;
	nb_on_dock = 0;
}

void Match2020Dock::putBouyRedOnRed() {
	if (nb_red_on_red < 6)
		nb_red_on_red++;
	else
		nb_on_dock++;
}

void Match2020Dock::putBouyGreenOnGreen() {
	if (nb_green_on_green < 6)
		nb_green_on_green++;
	else
		nb_on_dock++;
}

void Match2020Dock::putBouyOnDock() {
	nb_on_dock++;
}

int Match2020Dock::countPoints() const {
	int points = 0;
	points += 2 * nb_red_on_red;
	points += 2 * nb_green_on_green;
	if (nb_red_on_red < nb_green_on_green)
		points += 2 * nb_red_on_red;
	else
		points += 2 * nb_green_on_green;
	points += nb_on_dock;
	return points;
}

void Match2020Dock::sendToConsole() const {
	extern Console console;
	console.sendKeyValue("Green", nb_green_on_green);
	console.sendKeyValue("Red", nb_red_on_red);
	console.sendKeyValue("Dock", nb_on_dock);
}
