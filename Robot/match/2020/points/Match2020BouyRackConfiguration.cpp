/*
 * Match2020BouyRackConfiguration.cpp
 *
 *  Created on: 29 sept. 2019
 *      Author: Anastaszor
 */

#include "Match2020BouyRackConfiguration.h"

#include "../../../../Commons/i2c/ActuatorBig2020Commands.h"

Match2020BouyRackConfiguration::Match2020BouyRackConfiguration()
{
	for(int i = 0; i < 5; i++)
	{
		our_side[i] = Match2020BouyColor::NOT_A_BOUY;
		random_1_our_side[i] = Match2020BouyColor::NOT_A_BOUY;
		random_2_middle[i] = Match2020BouyColor::NOT_A_BOUY;
		random_3_opp_side[i] = Match2020BouyColor::NOT_A_BOUY;
		known_one_error[i] = Match2020BouyColor::NOT_A_BOUY;
	}
}

Match2020BouyColor Match2020BouyRackConfiguration::forColor(Match2020PlayerColor pcolor, Match2020BouyColor bcolor)
{
	if(pcolor == Match2020PlayerColor::BLUE)
		return bcolor;
	// else player color is yellow
	// and their bouys are inverted
	switch(bcolor)
	{
		case Match2020BouyColor::NOT_A_BOUY:
			return Match2020BouyColor::NOT_A_BOUY;
		case Match2020BouyColor::BOUY_GREEN:
			return Match2020BouyColor::BOUY_RED;
		case Match2020BouyColor::BOUY_RED:
			return Match2020BouyColor::BOUY_GREEN;
	}
	return Match2020BouyColor::NOT_A_BOUY;
}

void Match2020BouyRackConfiguration::applyPlayerColor(Match2020PlayerColor pcolor)
{
	our_side[0] = forColor(pcolor, Match2020BouyColor::BOUY_RED);
	our_side[1] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	our_side[2] = forColor(pcolor, Match2020BouyColor::BOUY_RED);
	our_side[3] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	our_side[4] = forColor(pcolor, Match2020BouyColor::BOUY_RED);

	random_1_our_side[0] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	random_1_our_side[1] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	random_1_our_side[2] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	random_1_our_side[3] = forColor(pcolor, Match2020BouyColor::BOUY_RED);
	random_1_our_side[4] = forColor(pcolor, Match2020BouyColor::BOUY_RED);

	random_2_middle[0] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	random_2_middle[1] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	random_2_middle[2] = forColor(pcolor, Match2020BouyColor::BOUY_RED);
	random_2_middle[3] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	random_2_middle[4] = forColor(pcolor, Match2020BouyColor::BOUY_RED);

	random_3_opp_side[0] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	random_3_opp_side[1] = forColor(pcolor, Match2020BouyColor::BOUY_RED);
	random_3_opp_side[2] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	random_3_opp_side[3] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	random_3_opp_side[4] = forColor(pcolor, Match2020BouyColor::BOUY_RED);

	known_one_error[0] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	known_one_error[1] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	known_one_error[2] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	known_one_error[3] = forColor(pcolor, Match2020BouyColor::BOUY_GREEN);
	known_one_error[4] = forColor(pcolor, Match2020BouyColor::BOUY_RED);
}

Match2020BouyColor Match2020BouyRackConfiguration::getColor(Code pattern, int i)
{
	switch(pattern)
	{
		case Code::RVRVR:
			return our_side[i % 5];
		case Code::VRVVR_VRRVR_1:
			return random_1_our_side[i % 5];
		case Code::VVRVR_VRVRR_2:
			return random_2_middle[i % 5];
		case Code::VVVRR_VVRRR_3:
			return random_3_opp_side[i % 5];
		case Code::VVVVR_VRRRR_4:
			return known_one_error[i % 5];
		case Code::Undefined:
			// TODO cas de la combinaison non déterminée
			return known_one_error[i % 5];
	}
	return known_one_error[i % 5];
}
