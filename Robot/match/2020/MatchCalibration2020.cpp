/*
 * MatchCalibration2020.cpp
 *
 *  Created on: 28 sep. 2019
 *      Author: Emmanuel
 */

#include "match/2020/Match2020.h"

#include "devices/beacon/Beacon2018.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"
#include "devices/host/HostCapabilities2020.h"
#include "devices/io/LCDScreen.h"
#include "match/2020/Match2020StrategyIndexCalibration.h"

#include "match/MatchData.h"
#include "math/cenMath.h"
#include "MatchCalibration2020.h"
#include "utils/Text.h"

MatchCalibration2020::MatchCalibration2020(Navigation &navigation) :
		navigation(navigation) {
}

void MatchCalibration2020::prepareCalibrationSmall(int trajectoryIndex) {
	switch (trajectoryIndex) {
		case STRATEGY_INDEX_CALIBRATION_LINEAR_TEST:
			prepareLinearTest();
			break;

		case STRATEGY_INDEX_CALIBRATION_LINEAR_CALIBRATION:
			prepareLinearCalibration();
			break;

		case STRATEGY_INDEX_CALIBRATION_ANGULAR_CALIBRATION:
			prepareAngularCalibration();
			break;

	}
}

void MatchCalibration2020::executeCalibrationSmall(int trajectoryIndex) {
	switch (trajectoryIndex) {
		case STRATEGY_INDEX_CALIBRATION_LINEAR_TEST:
			executeLinearTest();
			break;

		case STRATEGY_INDEX_CALIBRATION_LINEAR_CALIBRATION:
			executeLinearCalibration();
			break;

		case STRATEGY_INDEX_CALIBRATION_ANGULAR_CALIBRATION:
			executeAngularCalibration();
			break;

		case STRATEGY_INDEX_CALIBRATION_TEST_RECALLAGE:
			executeTestRecallage();
			break;

	}
}

void MatchCalibration2020::prepareCalibrationBig(int trajectoryIndex) {
	switch (trajectoryIndex) {
		case STRATEGY_INDEX_CALIBRATION_LINEAR_TEST:
			prepareLinearTest();
			break;

		case STRATEGY_INDEX_CALIBRATION_LINEAR_CALIBRATION:
			prepareLinearCalibration();
			break;

		case STRATEGY_INDEX_CALIBRATION_ANGULAR_CALIBRATION:
			prepareAngularCalibration();
			break;
	}
}

void MatchCalibration2020::executeCalibrationBig(int trajectoryIndex) {
	switch (trajectoryIndex) {
		case STRATEGY_INDEX_CALIBRATION_LINEAR_TEST:
			executeLinearTest();
			break;

		case STRATEGY_INDEX_CALIBRATION_LINEAR_CALIBRATION:
			executeLinearCalibration();
			break;

		case STRATEGY_INDEX_CALIBRATION_ANGULAR_CALIBRATION:
			executeAngularCalibration();
			break;
		case STRATEGY_INDEX_CALIBRATION_TEST_RECALLAGE:
			executeTestRecallage();
			break;
	}
}

void MatchCalibration2020::prepareAngularCalibration() {
}

void MatchCalibration2020::prepareLinearCalibration() {
}

void MatchCalibration2020::prepareLinearTest() {
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();

	PositionControl &control = navigation.getControl();
	control.setSpeedIndex(0);

	navigation.waitForDelay(1000);
	navigation.repositionMM(-200, 1000, geometry.rearWidth, 900);
//	navigation.moveMM(370);
//	navigation.rotateDeciDegrees(900);
//	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
//	navigation.moveMM(175);
}

void MatchCalibration2020::executeAngularCalibration() {
	int turnNumber = 10;

	PositionControl &control = navigation.getControl();
	control.setSpeedIndex(1);
	control.setErrorRatioLeftVsRight(0);
	navigation.repositionMM(-200, 0, 0, 0);
	navigation.moveMM(130);
	navigation.rotateDeciDegrees(3600 * turnNumber);
	Position p = navigation.repositionMM(-200, 0, 0, 0);
	float ratio = 1 + (p.angle / M_2PI) / turnNumber;
	int pas = control.stepsByTurn / ratio;
	extern LCDScreen screen;
	char buffer[10];
	itoa((int) (ANGLE_TO_DEGREES(p.angle) * 100), buffer, 10);
	//Display error:
	screen.displayMessage(buffer);
	navigation.waitForDelay(10000);
	itoa(pas, buffer, 10);
	//Display new value to set for StepBeTurn
	screen.displayMessage(buffer);
	navigation.waitForDelay(10000);
}

void MatchCalibration2020::executeTestRecallage()
{
	PositionControl &control = navigation.getControl();
	extern Beacon2018 beacon;
	beacon.setBeepEnabled(false);
	navigation.getControl().obstacleAvoidance = false;

	control.maxRepositionErrorDistanceMM = 30;
	control.maxRepositionErrorAngleDeciDegree = 70;
	control.ignoreRepositionError = false;

	navigation.moveMM(130);
	navigation.rotateDeciDegrees(3600);
	navigation.waitForDelay(3000);
	navigation.repositionMM(-200, 0, 0, 0);
	navigation.waitForDelay(5000);
	navigation.moveTo(Point(150, 0));
	navigation.waitForDelay(50000);
}

void MatchCalibration2020::executeLinearCalibration() {
	int longueurCalleMM = 1002;
	int marge = 100;

	extern LCDScreen screen;
	char buffer[10];
	PositionControl &control = navigation.getControl();

	control.setSpeedIndex(1);
	navigation.repositionMM(-200, 0, 0, 0);
	// avance de la longueur de la calle plus une marge
	navigation.moveMM(longueurCalleMM + marge);
	Position p = control.getPosition();

	navigation.waitForDelay(3000);
	// recalage sur la cale
	p = navigation.repositionMM(-4 * marge, 0, 0, 0);
	// calcul du ratio de la longueur de la cale mesur�e par rapport � la longueur connue
	float ratio = 1 + (p.x - marge) / longueurCalleMM;
	// calcul du nouveau ratio pas par m�tres
	int newStepsByMeter = control.stepsByMeter / ratio;
	// affichage de la longueur mesur�e
	itoa((int) ((p.x - marge) * 10), buffer, 10);
	screen.displayMessage(buffer);
	navigation.waitForDelay(10000);
	// affichage du nouveau nombre de pas par m�tres
	itoa(newStepsByMeter, buffer, 10);
	screen.displayMessage(buffer);
	navigation.waitForDelay(10000);
}

void MatchCalibration2020::executeLinearTest() {
	navigation.waitForDelay(500);

	extern Beacon2018 beacon;
	beacon.setThreshold(800);
	beacon.setBeepEnabled(true);

	navigation.getControl().obstacleAvoidance = true;

	PositionControl &control = navigation.getControl();
	control.setSpeedIndex(SPEED_SMALL_TEST);
	navigation.moveMM(2600);
}

void MatchCalibration2020::displayTrajectory(int trajectoryIndex) {
	extern LCDScreen screen;

	if (HostDevice::isSmall()) {
		//Case small
		switch (trajectoryIndex) {
			case STRATEGY_INDEX_CALIBRATION_NOMINAL:
				screen.displayMessage(STRATEGY_INDEX_CALIBRATION_NOMINAL_TEXT, 8);
				break;
			case STRATEGY_INDEX_CALIBRATION_LINEAR_TEST:
				screen.displayMessage(STRATEGY_INDEX_CALIBRATION_LINEAR_TEXT, 8);
				break;
			case STRATEGY_INDEX_CALIBRATION_LINEAR_CALIBRATION:
				screen.displayMessage(STRATEGY_INDEX_CALIBRATION_LINEAR_CALIBRATION_TEXT, 8);
				break;
			case STRATEGY_INDEX_CALIBRATION_ANGULAR_CALIBRATION:
				screen.displayMessage(STRATEGY_INDEX_CALIBRATION_ANGULAR_CALIBRATION_TEXT, 8);
				break;
			case STRATEGY_INDEX_CALIBRATION_TEST_RECALLAGE:
				screen.displayMessage(STRATEGY_INDEX_CALIBRATION_TEST_RECALLAGE_TEXT, 8);
				break;

			default:
				char buffer[9];
				itoa(trajectoryIndex, buffer, 10);
				screen.displayMessage(buffer, 8);
		}
	} else if (HostDevice::isBig()) {
		//Case small
		switch (trajectoryIndex) {
			case STRATEGY_INDEX_CALIBRATION_NOMINAL:
				screen.displayMessage(STRATEGY_INDEX_CALIBRATION_NOMINAL_TEXT, 8);
				break;
			case STRATEGY_INDEX_CALIBRATION_LINEAR_TEST:
				screen.displayMessage(STRATEGY_INDEX_CALIBRATION_LINEAR_TEXT, 8);
				break;
			case STRATEGY_INDEX_CALIBRATION_LINEAR_CALIBRATION:
				screen.displayMessage(STRATEGY_INDEX_CALIBRATION_LINEAR_CALIBRATION_TEXT, 8);
				break;
			case STRATEGY_INDEX_CALIBRATION_ANGULAR_CALIBRATION:
				screen.displayMessage(STRATEGY_INDEX_CALIBRATION_ANGULAR_CALIBRATION_TEXT, 8);
				break;
			case STRATEGY_INDEX_CALIBRATION_TEST_RECALLAGE:
				screen.displayMessage(STRATEGY_INDEX_CALIBRATION_TEST_RECALLAGE_TEXT, 8);
				break;



			default:
				char buffer[9];
				itoa(trajectoryIndex, buffer, 10);
				screen.displayMessage(buffer, 8);
		}
	}

}
