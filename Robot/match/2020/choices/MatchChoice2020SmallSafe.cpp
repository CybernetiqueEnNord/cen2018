/*
 * MatchChoice2020SmallSafe.cpp
 *
 *  Created on: 21 d�c. 2019
 *      Author: anastaszor
 */

#include "MatchChoice2020SmallSafe.h"
#include "devices/host/HostDevice.h"
#include "match/2020/actions/small/SmallSafeTaskList.h"
#include "match/2020/MatchData2020.h"

MatchChoice2020SmallSafe::MatchChoice2020SmallSafe(Navigation &navigation, MatchData &matchData) :
		MatchChoice2020(navigation, matchData) {
	name = "SAFE";
}

void MatchChoice2020SmallSafe::prepare() {
	// {{{ Etape 1 : deplacement jusqua la zone de depart
	//	extern SmallActuator smallActuator;
	const HostData &hdata = HostDevice::getHostData();
	const RobotGeometry &geometry = hdata.getGeometry();
	PositionControl &control = navigation.getControl();

	control.setSpeedIndex(SPEED_SMALL_INITIALISATION);

	// TODO intialize and check actionneurs

	// TODO set color side

	// position the small robot on the starting area
	navigation.repositionMM(-200, 2000 - geometry.rearWidth, 0, 1800);
	// va � x > 935
	navigation.moveMM(935 - 2 * geometry.rearWidth - geometry.frontWidth);
	//Recallage en y dans la zone bleu
	navigation.rotateDeciDegrees(-900);
	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	// va � y = 200
	navigation.moveMM(160 - geometry.rearWidth);
	//S'orienter
	navigation.rotateDeciDegrees(-900);
	navigation.moveMM(-280);

	//smallActuator.setFlapClosed();

	// Depart : 655;200 ?
	// }}}

	MatchData2020 &mdata = (MatchData2020&) matchData;
	static SmallSafeTaskList list(mdata);
	executor.setTasksList(list);
	navigation.getControl().setSpeedIndex(SPEED_SMALL_FAST);
}

void MatchChoice2020SmallSafe::execute() {
	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	executor.run();
}

void MatchChoice2020SmallSafe::finish() {
	MatchChoice2020::finish();
}
