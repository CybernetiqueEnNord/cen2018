/*
 * MatchChoice2020.cpp
 *
 *  Created on: 1 juin 2020
 *      Author: emmanuel
 */

#include "MatchChoice2020.h"

#include "devices/databus/2020/DataBus2020.h"
#include "devices/host/HostData.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "match/2020/MatchData2020.h"
#include "devices/host/HostDataF446REBig.h"
#include "devices/host/HostDataF446RENextGenBig.h"
#include "devices/host/HostDataF446RENextGenSmall.h"
#include "devices/host/HostDataF446RESmall.h"
#include "devices/host/HostDevice.h"

MatchChoice2020::MatchChoice2020(Navigation &navigation, MatchData &matchData) :
		MatchChoice(navigation, matchData) {
}

void MatchChoice2020::moveToAnchorArea(Match2020AnchorArea area) {
	MatchData2020 &mdata = (MatchData2020&) matchData;
	if (HostDevice::isSmall()) {
		//Small:
		area = Match2020AnchorArea::SOUTH;
		mdata.initializeAnchor(area);

		if (area == Match2020AnchorArea::NORTH) {
			navigation.moveTo(Point(200, 500));
			navigation.moveTo(Point(300, 400));
			mdata.anchorDown(Match2020AnchorArea::NORTH);
		} else {
			navigation.moveTo(Point(1400, 500));
			PositionControl &control = navigation.getControl();
			control.obstacleAvoidance = false;
			navigation.moveTo(Point(1300, 400));
			mdata.anchorDown(Match2020AnchorArea::SOUTH);
		}

		if (area == Match2020AnchorArea::SOUTH) {
			mdata.anchorDown(Match2020AnchorArea::SOUTH);
		} else {
			mdata.anchorDown(Match2020AnchorArea::NORTH);
		}
	} else {
		//Big:
		//36 80
		area = Match2020AnchorArea::NORTH;
		mdata.initializeAnchor(area);

		extern BigActuator bigActuator;
		Point destination(800, 500);
		const Position &position = navigation.getControl().getPosition();
		if (position.getDistance(destination) > 100) {
			// manoeuvre pour rentrer dans le port
			navigation.moveTo(Point(800, 800));
		}
		navigation.moveTo(destination);
		navigation.getControl().setSpeedIndex(SPEED_BIG_SLOW);
		navigation.moveTo(Point(800, 375));
		if (area == Match2020AnchorArea::SOUTH) {
			bigActuator.setArmLighthouse(true);
		} else {
			bigActuator.setArmEndAtBay(true);
		}
	}

	if (area == Match2020AnchorArea::SOUTH) {
		mdata.anchorDown(Match2020AnchorArea::SOUTH);
	} else {
		mdata.anchorDown(Match2020AnchorArea::NORTH);
	}
}

void MatchChoice2020::finish() {
	if (handleAnchorArea) {
		moveToAnchorArea(Match2020AnchorArea::UNKNOWN);
		/*
		 extern DataBus2020 dataBus;
		 Match2020AnchorArea area;
		 if (dataBus.queryAnchorArea(area)) {
		 // va vers l'ancrage
		 MatchData2020 &mdata = (MatchData2020&) matchData;
		 moveToAnchorArea(area);
		 } else {
		 moveToAnchorArea(Match2020AnchorArea::SOUTH);
		 }
		 */
	}
	MatchChoice::finish();
}
