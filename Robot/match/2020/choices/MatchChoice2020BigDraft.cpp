/*
 * MatchChoice2020BigDraft.cpp
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#include "MatchChoice2020BigDraft.h"

#include "devices/beacon/Beacon2018.h"
#include "devices/actuators/2020/big/BigActuator.h"

MatchChoice2020BigDraft::MatchChoice2020BigDraft(Navigation &navigation, MatchData &matchData) :
		MatchChoice2020(navigation, matchData) {
	name = "DRAFT";
}

void MatchChoice2020BigDraft::prepare() {
}

void MatchChoice2020BigDraft::execute() {
	extern Beacon2018 beacon;
	beacon.setBeepEnabled(false);
	navigation.waitForDelay(20000);
}

void MatchChoice2020BigDraft::finish() {
	MatchChoice2020::finish();
}
