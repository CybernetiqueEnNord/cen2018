/*
 * MatchChoice2020SmallHomologation.cpp
 *
 *  Created on: 21 d�c. 2019
 *      Author: anastaszor
 */

#include "devices/host/HostDevice.h"
#include "devices/actuators/2020/small/SmallActuator.h"
#include "MatchChoice2020SmallHomologation.h"

MatchChoice2020SmallHomologation::MatchChoice2020SmallHomologation(Navigation &navigation, MatchData &matchData) :
		MatchChoice2020(navigation, matchData) {
	name = "HOMOLOG";
}

void MatchChoice2020SmallHomologation::prepare() {
//	extern SmallActuator smallActuator;

	navigation.waitForDelay(500);
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	navigation.repositionMM(-200, 2000 - geometry.rearWidth, 0, 1800);
	// va � x > 935
	navigation.moveMM(935 - 2 * geometry.rearWidth - geometry.frontWidth);
	//Recallage en y dans la zone bleu
	navigation.rotateDeciDegrees(-900);
	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	// va � y = 200
	navigation.moveMM(160 - geometry.rearWidth);
	//S'orienter
	navigation.rotateDeciDegrees(-900);
	navigation.moveMM(-280);
}

void MatchChoice2020SmallHomologation::execute() {
	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;

	// TODO use actions/small/SmallWindsockAction
	extern SmallActuator smallActuator;

	navigation.moveTo(Point(1840, 160));
	smallActuator.setMancheAAirArm(true);
	navigation.moveTo(Point(1840, 720));

	navigation.moveTo(Point(1450, 720));
	smallActuator.setMancheAAirArm(false);

	navigation.moveTo(Point(1450, 2000));
	navigation.moveToBackwards(Point(1450, 250));
}

void MatchChoice2020SmallHomologation::finish() {
	handleAnchorArea = false;
	MatchChoice2020::finish();
}
