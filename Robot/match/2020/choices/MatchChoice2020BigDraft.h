/*
 * MatchChoice2020BigDraft.h
 *
 *  Created on: 8 f�vr. 2020
 *      Author: anastaszor
 */

#ifndef MATCH_2020_CHOICES_MATCHCHOICE2020BIGDRAFT_H_
#define MATCH_2020_CHOICES_MATCHCHOICE2020BIGDRAFT_H_

#include "control/Navigation.h"
#include "match/MatchData.h"
#include "match/2020/choices/MatchChoice2020.h"

class MatchChoice2020BigDraft : public MatchChoice2020
{
public:

	/**
	 * Constructor.
	 */
	MatchChoice2020BigDraft(Navigation &navigation, MatchData &matchData);

	/**
	 * Executes the actions to be executed before the matches, to prepare for
	 * main session.
	 */
	virtual void prepare();

	/**
	 * Executes the actions to be executed during the matches main session.
	 */
	virtual void execute();

	/**
	 * Executes the action to be executed once the matches' timer has reach
	 * end time.
	 */
	virtual void finish();

};

#endif /* MATCH_2020_CHOICES_MATCHCHOICE2020BIGDRAFT_H_ */
