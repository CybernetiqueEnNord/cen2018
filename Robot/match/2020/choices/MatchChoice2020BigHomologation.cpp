/*
 * MatchChoice2020BigHomologation.cpp
 *
 *  Created on: 21 d�c. 2019
 *      Author: anastaszor
 */

#include "MatchChoice2020BigHomologation.h"

#include "../../../control/PositionControl.h"
#include "../../../devices/actuators/2020/big/BigActuator.h"
#include "../../../devices/host/HostData.h"
#include "../../../devices/host/HostDevice.h"
#include "../../../devices/host/RobotGeometry.h"
#include "../../../odometry/Position.h"

MatchChoice2020BigHomologation::MatchChoice2020BigHomologation(Navigation &navigation, MatchData &matchData) :
		MatchChoice2020(navigation, matchData) {
	name = "HOMOLOG";
}

void MatchChoice2020BigHomologation::prepare() {
	navigation.waitForDelay(500);
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	navigation.repositionMM(-200, geometry.rearWidth, 0, 0);
	// x < 665
	navigation.moveMM(665 - 2 * geometry.rearWidth - geometry.frontWidth);
	//Recallage en y dans la zone bleu
	navigation.rotateDeciDegrees(900);
	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	// y = 220
	navigation.moveMM(220 - geometry.rearWidth);
	//S'orienter
	navigation.rotateDeciDegrees(900);

	navigation.moveMM(-260);
}

void MatchChoice2020BigHomologation::execute() {
	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;

//	const HostData &data = HostDevice::getHostData();
//	const RobotGeometry &geometry = data.getGeometry();

	extern BigActuator bigActuator;
//	bigActuator.setPumpFrontTrio(true);
//	bigActuator.setLifterFront(4);

	navigation.getControl().setSpeedIndex(SPEED_BIG_NOMINAL);

	navigation.moveTo(Point(270, 220));
	bigActuator.setArmLighthouse(true);
	navigation.moveTo(Point(270, 1200));
	bigActuator.setArmLighthouse(false);
	navigation.moveTo(Point(510, 1200));
	navigation.moveToBackwards(Point(510, 200));
}

void MatchChoice2020BigHomologation::finish() {
	handleAnchorArea = false;
	MatchChoice2020::finish();
}

