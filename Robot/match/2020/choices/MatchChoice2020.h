/*
 * MatchChoice2020.h
 *
 *  Created on: 1 juin 2020
 *      Author: emmanuel
 */

#ifndef MATCH_2020_CHOICES_MATCHCHOICE2020_H_
#define MATCH_2020_CHOICES_MATCHCHOICE2020_H_

#include "match/MatchChoice.h"

#include "match/2020/Match2020Enums.h"

/**
 * Implémentation de base de MatchChoice pour la coupe 2020.
 * Prend en charge la gestion commune du mouvement vers la zone d'ancrage en fin de match.
 */
class MatchChoice2020 : public MatchChoice {
protected:
	bool handleAnchorArea = true;
	virtual void moveToAnchorArea(Match2020AnchorArea area);

public:
	MatchChoice2020(Navigation &navigation, MatchData &matchData);
	virtual void finish() override;
};

#endif /* MATCH_2020_CHOICES_MATCHCHOICE2020_H_ */
