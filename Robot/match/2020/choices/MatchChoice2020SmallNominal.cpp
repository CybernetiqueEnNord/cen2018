/*
 * MatchChoice2020SmallNominal.cpp
 *
 *  Created on: 21 d�c. 2019
 *      Author: anastaszor
 */

#include "devices/host/HostDevice.h"
#include "devices/actuators/2020/small/SmallActuator.h"
#include "MatchChoice2020SmallNominal.h"
#include "match/2020/MatchData2020.h"
#include "match/2020/actions/small/SmallNominalTaskList.h"

MatchChoice2020SmallNominal::MatchChoice2020SmallNominal(Navigation &navigation, MatchData &matchData) :
		MatchChoice2020(navigation, matchData) {
	name = "NOMINAL";
}

void MatchChoice2020SmallNominal::prepare() {
	// {{{ Etape 1 : deplacement jusqua la zone de depart
	extern SmallActuator smallActuator;
	smallActuator.setMatchSide(navigation.getControl().isReversed());
	const HostData &hdata = HostDevice::getHostData();
	const RobotGeometry &geometry = hdata.getGeometry();
	PositionControl &control = navigation.getControl();

	control.setSpeedIndex(SPEED_SMALL_INITIALISATION);

	// position the small robot on the starting area
	navigation.repositionMM(-200, 2000 - geometry.rearWidth, 0, 1800);
	navigation.moveMM(100);
	smallActuator.testAllServoAreWorking();
	// va � x > 935
	navigation.moveMM(835 - 2 * geometry.rearWidth - geometry.frontWidth);
	//Recallage en y dans la zone bleu
	navigation.rotateDeciDegrees(-900);
	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	// va � y = 200
	navigation.moveMM(160 - geometry.rearWidth);

	navigation.moveTo(Point(930 + 150, 230 + 150));
	navigation.rotateToOrientationDeciDegrees(450);
	navigation.moveMM(-235);


	//smallActuator.setFlapClosed();

	// Depart : 655;200 ?
	// }}}

	// {{{ Etape 2 : setup tasklist
	MatchData2020 &mdata = (MatchData2020&) matchData;
	static SmallNominalTaskList list(mdata);
	executor.setTasksList(list);
	navigation.getControl().setSpeedIndex(SPEED_SMALL_FAST);
	// }}}

}

void MatchChoice2020SmallNominal::execute() {
	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	executor.run();
}

void MatchChoice2020SmallNominal::finish() {
	MatchChoice2020::finish();
}
