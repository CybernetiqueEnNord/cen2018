/*
 * MatchChoice2020SmallDraft.cpp
 *
 *  Created on: 29 ao�t 2020
 *      Author: Robin
 */

#include "MatchChoice2020SmallDraft.h"

#include "../../../control/PositionControl.h"
#include "../../../devices/host/HostData.h"
#include "../../../devices/host/HostDevice.h"
#include "../../../devices/host/RobotGeometry.h"
#include "../../../odometry/Position.h"
#include "../actions/small/SmallDraftTaskList.h"
#include "../MatchData2020.h"

MatchChoice2020SmallDraft::MatchChoice2020SmallDraft(Navigation &navigation, MatchData &matchData) :
		MatchChoice2020(navigation, matchData) {
	name = "DRAFT";
}

void MatchChoice2020SmallDraft::prepare() {
	// {{{ Etape 1 : deplacement jusqua la zone de depart
//	extern SmallActuator smallActuator;
	const HostData &hdata = HostDevice::getHostData();
	const RobotGeometry &geometry = hdata.getGeometry();
	PositionControl &control = navigation.getControl();

	control.setSpeedIndex(SPEED_SMALL_INITIALISATION);

	// position the small robot on the starting area
	navigation.repositionMM(-200, 2000 - geometry.rearWidth, 0, 1800);
	// va � x > 935
	navigation.moveMM(935 - 2 * geometry.rearWidth - geometry.frontWidth);
	//Recallage en y dans la zone bleu
	navigation.rotateDeciDegrees(-900);
	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	// va � y = 200
	navigation.moveMM(160 - geometry.rearWidth);

	navigation.moveTo(Point(930 + 150, 230 + 150));
	navigation.rotateToOrientationDeciDegrees(450);
	navigation.moveMM(-235);

	//smallActuator.setFlapClosed();

	// Depart : 655;200 ?
	// }}}

	// {{{ Etape 2 : setup tasklist
	MatchData2020 &mdata = (MatchData2020&) matchData;
	static SmallDraftTaskList list(mdata);
	executor.setTasksList(list);
	navigation.getControl().setSpeedIndex(SPEED_SMALL_FAST);
	// }}}

}

void MatchChoice2020SmallDraft::execute() {
	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	executor.run();
}

void MatchChoice2020SmallDraft::finish() {
	MatchChoice2020::finish();
}
