/*
 * MatchChoice2020BigSafe.cpp
 *
 *  Created on: 21 d�c. 2019
 *      Author: anastaszor
 */

#include "MatchChoice2020BigSafe.h"
#include "devices/host/HostDevice.h"
#include "match/2020/MatchData2020.h"
#include "devices/actuators/2020/big/BigActuator.h"
#include "match/2020/actions/big/BigSafeTaskList.h"


MatchChoice2020BigSafe::MatchChoice2020BigSafe(Navigation &navigation, MatchData &matchData) :
	MatchChoice2020(navigation, matchData)
{
	name = "SAFE";
}

void MatchChoice2020BigSafe::prepare()
{
	// {{{ Etape 1 : deplacement jusqua la zone de depart

	extern BigActuator bigActuator;
	bigActuator.setMatchSide(navigation.getControl().isReversed());
	bigActuator.testAllServoAreWorking();
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	navigation.repositionMM(-200, geometry.rearWidth, 0, 0);
	// va a x < 665
	navigation.moveMM(665 - 2 * geometry.rearWidth - geometry.frontWidth);
	// recalage en y dans la zone bleu
	navigation.rotateDeciDegrees(900);
	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	// va a y = 200 + 30
	navigation.moveMM(215 - geometry.rearWidth);
	// s'orienter
	navigation.rotateDeciDegrees(900);
	navigation.moveMM(-230);

	// }}}

	// {{{ Etape 2 : setup tasklist
	MatchData2020 &mdata = (MatchData2020&) matchData;
	static BigSafeTaskList list(mdata);
	executor.setTasksList(list);
	//navigation.getControl().setSpeedIndex(SPEED_BIG_NOMINAL);
	navigation.getControl().setSpeedIndex(SPEED_BIG_FAST);
	// }}}

	// {{{ armament
	bigActuator.armBottomBuoyCatcher(FrontRear::Front);
	bigActuator.armBottomBuoyCatcher(FrontRear::Rear);
	// }}}
}

void MatchChoice2020BigSafe::execute()
{
	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	executor.run();
}

void MatchChoice2020BigSafe::finish()
{
	MatchChoice2020::finish();
}
