/*
 * MatchActionUtils2020.h
 *
 *  Created on: 6 juin 2020
 *      Author: Robin
 */

#ifndef MATCH_2020_MATCHACTIONUTILS2020_H_
#define MATCH_2020_MATCHACTIONUTILS2020_H_

#include "control/Navigation.h"

class MatchActionUtils2020 {
public:
	static void waitForMinimalCommandTimeHandling(Navigation &navigation); // sinon �a bouge encore quand on ferme les doigts
	static void waitForGobeletStabilisationInRack(Navigation &navigation);// sinon �a bouge encore quand on ferme les doigts
	static void waitForHold(Navigation &navigation);
	static void waitForRaised(Navigation &navigation);
	static void waitForReleaseCompleted(Navigation &navigation);
};

#endif /* MATCH_2020_MATCHACTIONUTILS2020_H_ */
