/*
 * MatchData.cpp
 *
 *  Created on: 26 oct. 2017
 *      Author: Emmanuel
 */

#include "MatchData.h"

MatchData::MatchData(const MatchTimer& timer) :
		timer(timer) {
}

void MatchData::addScore(int value) {
	score += value;
}

void MatchData::setScore(int value) {
	score = value;
}

int MatchData::getScore() const {
	return score;
}

const MatchTimer& MatchData::getTimer() const {
	return timer;
}

const Position& MatchData::getPosition() const {
	if (position.isValid()) {
		return *position;
	} else {
		static Position defaultPosition;
		return defaultPosition;
	}
}

int MatchData::getComputedScore() const {
	// par d�faut on renvoie la valeur du score connu afin que updateScore() soit invariant
	return score;
}

void MatchData::setPosition(const Position &position) {
	this->position = &position;
}

void MatchData::updateScore() {
	int computedScore = getComputedScore();
	setScore(computedScore);
}
