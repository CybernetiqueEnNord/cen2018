/*
 * SimpleFallbackAction.h
 *
 * An action that takes an action as primary action, and another
 * action as fallback action if the first one fails at any point.
 *
 *  Created on: 30 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_SIMPLEFALLBACKACTION_H_
#define MATCH_SIMPLEFALLBACKACTION_H_

#include "MatchAction.h"

class SimpleFallbackAction : public MatchAction {
private:
	MatchAction &nominal;
	MatchAction &fallback;
	bool doFallback = false;
	int maxStepNominal = 0;

public:
	virtual void executeStep(Navigation &navigation, unsigned int step);
	SimpleFallbackAction(MatchAction &nominal, MatchAction &fallback);
	void abort();
	const Point& getStart() const;
	bool hasFailed();
	bool isAborted();
	bool isTerminated();
	void reset();
};

#endif /* MATCH_SIMPLEFALLBACKACTION_H_ */
