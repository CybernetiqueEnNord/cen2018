#if CEN_EDITION == 2018
#include "match/2018/Match2018.h"
#define MatchDefault Match2018
#elif CEN_EDITION == 2019
#include "match/2019/Match2019.h"
#define MatchDefault Match2019
#elif CEN_EDITION == 2020
#include "match/2020/Match2020.h"
#define MatchDefault Match2020
#endif
