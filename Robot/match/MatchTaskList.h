/*
 * MatchTaskList.h
 *
 *  Created on: 18 f�vr. 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_MATCHTASKLIST_H_
#define MATCH_MATCHTASKLIST_H_

#include "match/MatchTask.h"

class MatchTaskList {
public:
	MatchTaskList();
	virtual unsigned int getCount() const = 0;
	virtual MatchTask& getTaskAt(unsigned int index) const = 0;
};

#endif /* MATCH_MATCHTASKLIST_H_ */
