/*
 * MatchChoiceCollection.cpp
 *
 *  Created on: 21 d�c. 2019
 *      Author: anastaszor
 */

#include "MatchChoiceCollection.h"

#include "utils/Arrays.h"

MatchChoiceCollection::MatchChoiceCollection(MatchChoice &c1, MatchChoice &c2, MatchChoice &c3, MatchChoice &c4, MatchChoice &c5, MatchChoice &c6, MatchChoice &c7, MatchChoice &c8) {
	choices[0] = Pointer<MatchChoice>(c1);
	choices[1] = Pointer<MatchChoice>(c2);
	choices[2] = Pointer<MatchChoice>(c3);
	choices[3] = Pointer<MatchChoice>(c4);
	choices[4] = Pointer<MatchChoice>(c5);
	choices[5] = Pointer<MatchChoice>(c6);
	choices[6] = Pointer<MatchChoice>(c7);
	choices[7] = Pointer<MatchChoice>(c8);
}

const char* MatchChoiceCollection::getName(int index) const {
	ensureRange(index);
	// should not happen
	const char* name = "WRONG";
	if (choices[index].isValid()) {
		name = choices[index]->getName();
	}
	return name;
}

void MatchChoiceCollection::prepare(int index) {
	ensureRange(index);
	if (choices[index].isValid()) {
		choices[index]->prepare();
	}
}

void MatchChoiceCollection::execute(int index) {
	ensureRange(index);
	if (choices[index].isValid()) {
		choices[index]->execute();
	}
}

void MatchChoiceCollection::ensureRange(int& index) const {
	if (index < 0) {
		index = 0;
	}
	if (index >= (signed) ARRAY_SIZE(choices)) {
		index = ARRAY_SIZE(choices) - 1;
	}
}

const Pointer<MatchChoice>& MatchChoiceCollection::getMatchChoice(int index) const {
	ensureRange(index);
	return choices[index];
}

void MatchChoiceCollection::finish(int index) {
	ensureRange(index);
	if (choices[index].isValid()) {
		choices[index]->finish();
	}
}

