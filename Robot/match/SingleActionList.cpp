/*
 * SingleActionList.cpp
 *
 *  Created on: 3 mars 2019
 *      Author: emmanuel
 */

#include "SingleActionList.h"

SingleActionList::SingleActionList(MatchAction &action) :
		action(action) {
}

MatchAction& SingleActionList::getActionAt(unsigned int index) const {
	if (index == 0) {
		return action;
	}
	return MatchAction::none;
}

unsigned int SingleActionList::getCount() const {
	return 1;
}
