/*
 * MatchTest.h
 *
 *  Created on: 19 sept. 2017
 *      Author: Emmanuel
 */

#ifndef MATCH_MATCHTEST_H_
#define MATCH_MATCHTEST_H_

#include "match/Match.h"

class MatchTest : public Match {
protected:
	virtual void execute();
public:
	MatchTest(Navigation &navigation);
};

#endif /* MATCH_MATCHTEST_H_ */
