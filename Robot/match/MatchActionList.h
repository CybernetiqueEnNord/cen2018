/*
 * MatchActionList.h
 *
 *  Created on: 27 janv. 2019
 *      Author: emmanuel
 */

#ifndef MATCH_MATCHACTIONLIST_H_
#define MATCH_MATCHACTIONLIST_H_

#include "MatchAction.h"

class MatchActionList {
public:
	MatchActionList();
	virtual MatchAction& getActionAt(unsigned int index) const = 0;
	virtual unsigned int getCount() const = 0;
};

#endif /* MATCH_MATCHACTIONLIST_H_ */
