/*
 * MatchTask.cpp
 *
 *  Created on: 23 janv. 2019
 *      Author: Emmanuel
 */

#include "MatchTask.h"

#include "match/MatchElement.h"

MatchTask::MatchTask(const char* label, int expectedScore, int staticCost, Pointer<MatchTarget> firstTarget) :
		label(label), expectedScore(expectedScore), staticCost(staticCost), firstTarget(firstTarget) {
}

const Pointer<MatchTarget> MatchTask::getCurrentTarget() const {
	Pointer<MatchTarget> target = firstTarget;
	while (target.isValid() && target->isDone()) {
		target = target->getNext();
	}
	return target;
}

bool MatchTask::isCompletable() const {
	bool result = true;
	Pointer<MatchTarget> target = getCurrentTarget();
	while (result && target.isValid()) {
		MatchElement &element = target->getElement();
		result &= element.isAvailableToTask(*this);
		target = target->getNext();
	}
	return result;
}

int MatchTask::getExpectedScore() const {
	return expectedScore;
}

int MatchTask::getEstimatedCost(const Position& position) const {
	int dynamicCost = computeDynamicCost(position);
	return staticCost + dynamicCost;
}

const char* MatchTask::getLabel() const {
	return label;
}

int MatchTask::computeDynamicCost(const Position& position) const {
	(void) position;
	return 0; // TODO evaluate dynamic cost
}

bool MatchTask::isDone() const {
	Pointer<MatchTarget> target = getCurrentTarget();
	bool done = !target.isValid();
	return done;
}

void MatchTask::setAvailable(bool value) {
	if (value) {
		unavailableTimestamp = 0;
	} else {
		unavailableTimestamp = chVTGetSystemTime();
	}
}

bool MatchTask::isAvailable() const {
	return unavailableTimestamp == 0;
}

void MatchTask::updateAvailability(unsigned int seconds) {
	if (unavailableTimestamp > 0) {
		systime_t elapsed = chVTGetSystemTime() - unavailableTimestamp;
		if (ST2S(elapsed) >= seconds) {
			unavailableTimestamp = 0;
			resetActions();
		}
	}
}

MatchTaskFailurePolicy MatchTask::getFailurePolicy() const {
	return failurePolicy;
}

void MatchTask::resetActions() {
	Pointer<MatchTarget> target = getCurrentTarget();
	if (target.isValid()) {
		const MatchActionList &actions = target->getAvailableActions();
		for (unsigned int i = 0; i < actions.getCount(); i++) {
			MatchAction &action = actions.getActionAt(i);
			action.reset();
		}
	}
}

void MatchTask::setDone() {
	firstTarget = Pointer<MatchTarget>();
}
