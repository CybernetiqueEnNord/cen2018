/*
 * MatchTarget.h
 *
 *  Created on: 23 janv. 2019
 *      Author: Emmanuel
 */

#ifndef MATCHTARGET_H
#define MATCHTARGET_H

#include "match/MatchAction.h"
#include "match/MatchActionList.h"
#include "match/MatchElement.h"
#include "utils/Pointer.h"

enum class MatchTargetType {
	PickUp, DropOff, Carrying
};

class MatchTarget {
private:
	bool done = false;
	Pointer<MatchTarget> next;
	const MatchActionList& actions;
	MatchElement &element;

public:
	MatchTarget(const MatchActionList& actions, MatchElement &element);
	const MatchActionList& getAvailableActions() const;
	MatchElement& getElement() const;
	Pointer<MatchTarget> getNext() const;
	bool isAvailable() const;
	bool isDone() const;
	void setDone();
	void setNext(MatchTarget &next);
};

#endif /* MATCHTARGET_H */
