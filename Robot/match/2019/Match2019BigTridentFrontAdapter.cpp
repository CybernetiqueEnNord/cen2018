/*
 * Match2019BigTridentFrontAdapter.cpp
 *
 *  Created on: 23 mai 2019
 *      Author: Anastaszor
 */

#include "Match2019BigTridentFrontAdapter.h"

Match2019BigTridentFrontAdapter::Match2019BigTridentFrontAdapter(BigActuator &actuator) :
	actuator(actuator)
{
	// nothing to do
}


/**
 * @inheritDoc
 */
int Match2019BigTridentFrontAdapter::loadFromAllPumps()
{
	bool res = actuator.setPumpFrontTrio(true);
	if(res)
		return 3;
	return 0;
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentFrontAdapter::loadFromLeftPump()
{
	// not implemented in actuator
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentFrontAdapter::loadFromMiddlePump()
{
	// not implemented in actuator
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentFrontAdapter::loadFromRightPump()
{
	// not implemented in actuator
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentFrontAdapter::unloadFromAllPumps()
{
	return actuator.setPumpFrontTrio(false);
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentFrontAdapter::unloadFromLeftPump()
{
	return actuator.setPumpFrontOff(TRIO_LEFT_PUMP);
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentFrontAdapter::unloadFromMiddlePump()
{
	return actuator.setPumpFrontOff(TRIO_CENTER_PUMP);
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentFrontAdapter::unloadFromRightPump()
{
	return actuator.setPumpFrontOff(TRIO_RIGHT_PUMP);
}
