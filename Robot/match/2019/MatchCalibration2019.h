/*
 * MatchCalibration.h
 *
 *  Created on: 13 avr. 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_MATCHCALIBRATION_H_
#define MATCH_2019_MATCHCALIBRATION_H_

#include "control/Navigation.h"
#include "devices/host/HostDataF446REBig.h"
#include "devices/host/HostDataF446RENextGenBig.h"
#include "devices/host/HostDataF446RENextGenSmall.h"
#include "devices/host/HostDataF446RESmall.h"
#include "match/2019/MatchData2019.h"

class MatchCalibration2019 {
private:
	Navigation &navigation;

	void prepareAngularCalibration();
	void prepareLinearCalibration();
	void prepareLinearTest();

	void prepareBigNewMiseEnPlace();
	void executeBigNewGetFirstsPalets();

	void prepareSmallNewMiseEnPlace();
	void executeSmallNewDepart();

	void executeAngularCalibration();
	void executeLinearCalibration();
	void executeLinearTest();

public:
	MatchCalibration2019(Navigation &navigation);
	void prepareCalibrationSmall(int trajectoryIndex);
	void executeCalibrationSmall(int trajectoryIndex);
	void prepareCalibrationBig(int trajectoryIndex);
	void executeCalibrationBig(int trajectoryIndex);

	void displayTrajectory(int trajectoryIndex);
};

#endif /* MATCH_2019_MATCHCALIBRATION_H_ */
