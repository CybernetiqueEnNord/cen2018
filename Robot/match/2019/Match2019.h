/*
 * Match2019.h
 *
 *  Created on: 11 oct. 2018
 *      Author: Anastaszor
 */

#ifndef MATCH_MATCH2019_H_
#define MATCH_MATCH2019_H_

#include "../MatchCalibration2019.h"
#include "match/Match.h"
#include "match/MatchExecutor.h"
#include "match/2019/MatchData2019.h"
#include "match/2019/Match2019StrategieIndex.h"
#include "control/Navigation.h"

class Match2019 : public Match, public NavigationListener {
private:
	bool calibration = false;
	MatchCalibration2019 calibrator;
	MatchExecutor executor;

	void changeMatchSide(bool newSide);
	void changeTrajectory(int trajectoryIndex);
	void displayMatchSide();
	void displayTrajectory();
	void updateMatchSide();
	void updateTrajectory();

	void executeBigHomologation();
	void executeSmallHomologation();
	void prepareBigHomologation();
	void prepareSmallHomologation();

	void executeBigMatchRobinToTestPrisePalets();
	void executeBigMatchATransformerEnAction();

	void executeBigMatchNominal();
	void prepareBigMatchNominal();

	void executeSmallMatchNominal();
	void executeSmallMatchSafe();
	void prepareSmallMatchNominal();
	void prepareSmallMatchSafe();

	MatchTaskList& getTestsTasksList();
	void executeTest(int taskIndex);

protected:
	bool hasCapability(unsigned int capability) const;

	virtual MatchData& createMatchData(const MatchTimer& timer);

	virtual void executeBig();
	virtual void executeSmall();
	virtual void prepareBig();
	virtual void prepareSmall();

	virtual void execute();
	virtual void initialize();
	virtual void prepare();
	virtual void onNavigationEvent(const Navigation &navigation);

	virtual void terminateNavigation();

public:
	int trajectoryIndex = STRATEGIE_INDEX_HOMOLOGUATION;
	bool trajectorySelectable = true;

	Match2019(Navigation &navigation);
	MatchData2019& getMatchData() const;
	virtual void terminate();
};

#endif /* MATCH_MATCH2019_H_ */
