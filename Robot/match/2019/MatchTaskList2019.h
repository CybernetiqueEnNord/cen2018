/*
 * MatchTaskList2019.h
 *
 *  Created on: 18 f�vr. 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_MATCHTASKLIST2019_H_
#define MATCH_2019_MATCHTASKLIST2019_H_

#include "match/MatchTaskList.h"

#include "match/2019/MatchData2019.h"

class MatchTaskList2019 : public MatchTaskList {
private:
	MatchData2019 &matchData;
	MatchTask (&tasks)[2];

	MatchTask (&createTasks())[2];

public:
	MatchTaskList2019(MatchData2019 &matchData);
	virtual unsigned int getCount() const;
	virtual MatchTask& getTaskAt(unsigned int index) const;
};

#endif /* MATCH_2019_MATCHTASKLIST2019_H_ */
