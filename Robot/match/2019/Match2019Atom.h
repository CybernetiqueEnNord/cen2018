/*
 * Match2019Atom.h
 *
 *  Created on: 11 oct. 2018
 *      Author: Anastaszor
 */

#ifndef MATCH_MATCH2019ATOM_H_
#define MATCH_MATCH2019ATOM_H_

enum class Match2019Atom
{
	NOT_AN_ATOM,
	GOLDENIUM,
	BLUEIUM,
	GREENIUM,
	REDIUM,
};

#endif /* MATCH_MATCH2019ATOM_H_ */
