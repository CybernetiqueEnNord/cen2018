/*
 * Target1ActionList.h
 *
 *  Created on: 1 mars 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_ACTIONS_TARGET1ACTIONLIST_H_
#define MATCH_2019_ACTIONS_TARGET1ACTIONLIST_H_

#include "match/MatchActionList.h"

class Target1ActionList : public MatchActionList {
public:
	Target1ActionList();
	virtual MatchAction& getActionAt(unsigned int index) const;
	virtual unsigned int getCount() const;
};

#endif /* MATCH_2019_ACTIONS_TARGET1ACTIONLIST_H_ */
