/*
 * SmallSpeedStart.cpp
 *
 *  Created on: 26 mai 2019
 *      Author: Anastaszor
 */

#include "SmallSpeedStart.h"

#include "control/Navigation.h"
#include "control/PositionControl.h"
#include "devices/actuators/2019/small/SmallActuator.h"
#include "devices/beacon/Beacon2018.h"
#include "odometry/Position.h"
#include "match/MatchAction.h"

SmallSpeedStart::SmallSpeedStart(MatchData2019 &matchData) :
		MatchAction("speedStart", Point(450, 280)), matchData(matchData) {
}

void SmallSpeedStart::executeStep(Navigation &navigation, unsigned int step) {

	extern Beacon2018 beacon;
	extern SmallActuator smallActuator;
	PositionControl &control = navigation.getControl();

	switch (step) {
		case 0: {
			control.obstacleAvoidance = true;

			control.setSpeedIndex(SPEED_SMALL_FAST_STARTING_ROTATION);
			navigation.moveMMCircleDecidegree(377, -900);
			break;
		}

		case 1: {
			matchData.runExperience();
			matchData.electronSuccessful();
			beacon.setThreshold(800);
			control.setSpeedIndex(SPEED_SMALL_MAX_FAST_LINEAR);

			class Condition: public NavigationConditionListener {
			protected:
				virtual int onCheckCondition(const Navigation& navigation) {
					Position current = navigation.getControl().getPosition();
					if (current.y >= 1750) {
						return 2;
					} else if (current.y >= 1200) {
						return 1;
					} else {
						return 0;
					}
				}

				virtual bool onConditionMet(const Navigation& navigation, int action) {
					(void) navigation;
					extern SmallActuator smallActuator;
					switch (action) {
						case 1:
							smallActuator.setArm(true);
							return true;

						case 2:
							smallActuator.setArm(false);
							setDone();
							return true;
					}
					return false;
				}

			public:
				Condition() :
						NavigationConditionListener(true) {
				}
			};

			Point dest(230, 2225);
			Condition condition;
			navigation.asynchronous = true;
			navigation.moveTo(dest);
			navigation.waitForCondition(condition);
			navigation.asynchronous = false;

		//	navigation.asynchronous = true;
		//
		//	navigation.moveTo(Point(230, 2225));
		//	//navigation.executeNextMove();
		//
		//	while (control.getMoveState() != MoveState::StraightAcceleration) {
		//		chThdSleepMilliseconds(5);
		//	}
		//	while (control.getMoveState() == MoveState::StraightAcceleration || control.getMoveState() == MoveState::StraightConstant || control.getMoveState() == MoveState::StraightDeceleration || control.getMoveState() == MoveState::EmergencyBrake) {
		//		if (control.getPosition().y >= 1200) {
		//			smallActuator.setArm(true);
		//			break;
		//		}
		//		chThdSleepMilliseconds(5);
		//	}
		//	while (control.getMoveState() == MoveState::StraightAcceleration || control.getMoveState() == MoveState::StraightConstant || control.getMoveState() == MoveState::StraightDeceleration || control.getMoveState() == MoveState::EmergencyBrake) {
		//		if (control.getPosition().y >= 1750) {
		//			smallActuator.setArm(false);
		//			break;
		//		}
		//		chThdSleepMilliseconds(5);
		//	}
		//	navigation.asynchronous = false;
		//	navigation.waitForMove();

			extern Beacon2018 beacon;
			beacon.setThreshold(600);
			control.setSpeedIndex(SPEED_SMALL_NOMINAL);


		//	navigation.rotateToOrientationDeciDegrees(1800);
			break;
		}

		case 2:
			//matchData.addToAccelerateur();	// blueium
			//matchData.unlockGoldenium();
			break;

		case STEP_FINALIZATION:
			smallActuator.setPumpTrio(false);
			smallActuator.setFlapClosed();
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
