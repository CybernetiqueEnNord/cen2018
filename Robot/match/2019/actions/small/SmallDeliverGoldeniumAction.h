/*
 * SmallDeliverGoldeniumAction.h
 *
 * Action qui fait decharger le goldenium au petit robot. Cette action
 * necessite que le petit robot ait son goldenium charge a ce moment
 * afin de pouvoir le decharger dans la balance. Cette action prend
 * le robot au pied de la pente (mais un peu plus vers la zone de depart
 * afin que la position initiale soit atteignable), fait monter au petit
 * la pente et rend le robot proche de sa position initiale afin qu'il
 * puisse executer d'autres actions. Dans le cas ou chainWithDispeser
 * est true alors la position de sortie est directement en bas de la
 * pente.
 *
 *  Created on: 22 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLDELIVERGOLDENIUMACTION_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLDELIVERGOLDENIUMACTION_H_

#include "match/2019/MatchData2019.h"
#include "match/MatchAction.h"

class SmallDeliverGoldeniumAction : public MatchAction {
private:
	MatchData2019 &matchData;
	bool chainWithDispenser = false;

public:
	SmallDeliverGoldeniumAction(MatchData2019 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
	void setChainWithDispenser(bool value);
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLDELIVERGOLDENIUMACTION_H_ */
