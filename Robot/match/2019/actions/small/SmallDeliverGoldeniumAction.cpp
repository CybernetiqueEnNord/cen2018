/*
 * SmallDeliverGoldeniumAction.cpp
 *
 *  Created on: 22 mai 2019
 *      Author: Anastaszor
 */

#include "SmallDeliverGoldeniumAction.h"

#include "devices/actuators/2019/small/SmallActuator.h"
#include "devices/host/RobotGeometry.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"

SmallDeliverGoldeniumAction::SmallDeliverGoldeniumAction(MatchData2019 &matchData) :
		MatchAction("smallDeliverGold", Point(1400, 200)), matchData(matchData) {
}

void SmallDeliverGoldeniumAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	PositionControl &control = navigation.getControl();

	switch (step) {
		case 0:
			navigation.rotateToOrientationDeciDegrees(1800);
			break;

		case 1: {
			// recalage en face de la pente
			navigation.moveMM(-(2000 - geometry.rearWidth - 60 - navigation.getControl().getPosition().x));
			navigation.repositionMM(-300, CURRENT, 2000 - geometry.rearWidth, 1800);
			navigation.moveMM(200 - geometry.rearWidth);
			navigation.rotateDeciDegrees(-900);
			navigation.moveMM(-200 + geometry.rearWidth + 30);
			navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
			break;
		}

		case 2: {
			//mont�e
			control.obstacleAvoidance = false;
			control.setSpeedIndex(1);
			smallActuator.setFlapPickup();
			navigation.moveMM(1075);
			smallActuator.setPumpGoldenium(false); //let the goldenium fall
			matchData.addGreeniumToBalance();
			matchData.addGoldeniumToBalance();
			navigation.waitForDelay(1000);
			smallActuator.setFlapClosed();
			break;
		}

		case 3:
			// recul
			navigation.moveMM(-900);
			break;

		case STEP_FINALIZATION: {
			control.setSpeedIndex(3);
			if (!chainWithDispenser) {
				navigation.moveTo(Point(1400, 200));
			}
			control.obstacleAvoidance = true;
			break;
		}

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
