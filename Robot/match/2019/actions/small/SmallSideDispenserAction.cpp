/*
 * SmallSideDispenserAction.cpp
 *
 *  Created on: 22 mai 2019
 *      Author: Anastaszor
 */

#include "SmallSideDispenserAction.h"

#include "../../../../control/Navigation.h"
#include "../../../../control/PositionControl.h"
#include "../../../../devices/actuators/2019/small/SmallActuator.h"
#include "../../../../devices/host/HostData.h"
#include "../../../../devices/host/HostDevice.h"
#include "../../../../devices/host/RobotGeometry.h"
#include "../../../../odometry/Position.h"
#include "../../Match2019TridentStorage.h"

SmallSideDispenserAction::SmallSideDispenserAction(Match2019TridentStorage &tridentStorage) :
		MatchAction("smallDeliverGold", Point(1400, 200)), tridentStorage(tridentStorage) {
}

void SmallSideDispenserAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	PositionControl &control = navigation.getControl();

	switch (step) {
		case 0:
			smallActuator.setFlapClosed();
			navigation.moveTo(Point(1750, 200));
			break;

		case 1:
			navigation.rotateToOrientationDeciDegrees(900);
			// recalage en face de la pente
			navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
			break;

		case 2:
			// recalage lat�ral
			navigation.moveMM(225 - geometry.rearWidth);
			navigation.rotateDeciDegrees(900);
			navigation.repositionMM(-300, 2000 - geometry.rearWidth, CURRENT, 1800);
			navigation.moveMM(220);
			break;

			// all 3 racks
		case 3:
			navigation.rotateToOrientationDeciDegrees(0);
			smallActuator.setFlapPickup();
			tridentStorage.loadFromSmallRack();
			navigation.moveTo(Point(2000 - 130, 225));
			break;

		case 4:
			navigation.waitForDelay(500); //Delay to have time to get the 3
			navigation.moveToBackwards(Point(1800, 225));
			break;

		case 5: {
			//mont�e
			control.obstacleAvoidance = false;
			control.setSpeedIndex(1);
			navigation.rotateToOrientationDeciDegrees(900);
			navigation.moveMM(650);
			tridentStorage.unloadAllInBalance();
			navigation.waitForDelay(1000);
			navigation.moveMM(380);
			smallActuator.setFlapClosed();
			break;
		}

		case 6:
			// recul
			navigation.moveMM(-900);
			break;

		case STEP_FINALIZATION: {
			control.setSpeedIndex(3);
			navigation.moveTo(Point(1500, 250));
			control.obstacleAvoidance = true;
			break;
		}

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}

}
