/*
 * SmallSafeTaskList.h
 *
 *  Created on: 28 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLSAFETASKLIST_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLSAFETASKLIST_H_

#include "../../../MatchTask.h"
#include "../../../MatchTaskList.h"
#include "../../MatchData2019.h"

class SmallSafeTaskList : public MatchTaskList {
private:
	MatchData2019 &matchData;
	MatchTask (&tasks)[9];

	MatchTask (&createTasks())[9];

public:
	SmallSafeTaskList(MatchData2019 &matchData);
	virtual unsigned int getCount() const;
	virtual MatchTask& getTaskAt(unsigned int index) const;
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLSAFETASKLIST_H_ */
