/*
 * SmallAggressiveTaskList.h
 *
 *  Created on: 29 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLAGGRESSIVETASKLIST_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLAGGRESSIVETASKLIST_H_

#include "../../../MatchTask.h"
#include "../../../MatchTaskList.h"
#include "../../MatchData2019.h"

class SmallAggressiveTaskList : public MatchTaskList {
private:
	MatchData2019 &matchData;
	MatchTask (&tasks)[9];

	MatchTask (&createTasks())[9];

public:
	SmallAggressiveTaskList(MatchData2019 &matchData);
	virtual unsigned int getCount() const;
	virtual MatchTask& getTaskAt(unsigned int index) const;
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLAGGRESSIVETASKLIST_H_ */
