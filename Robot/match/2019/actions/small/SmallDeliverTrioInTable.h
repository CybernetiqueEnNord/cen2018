/*
 * SmallDeliverTrioInTable.h
 *
 * Action qui fait decharger les 3 palets pris dans le trio de ventouses
 * a l'avant du robot dans la zone de depart. Cette action traverse le
 * terrain et depose tout dans la zone rouge.
 *
 *  Created on: 22 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLDELIVERTRIOINTABLE_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLDELIVERTRIOINTABLE_H_

#include "match/2019/MatchData2019.h"
#include "match/MatchAction.h"
#include "../../Match2019TridentStorage.h"

class SmallDeliverTrioInTable : public MatchAction {
private:
	Match2019TridentStorage &tridentStorage;

public:
	SmallDeliverTrioInTable(Match2019TridentStorage &tridentStorage);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLDELIVERTRIOINTABLE_H_ */
