/*
 * SmallDeliverBlueiumInBalance.h
 *
 * Action qui va deposer le blueium pris depuis notre rack de 3 dans notre
 * balance. Cette action necessite d'avoir fait SmallTakeOurRack3NearBalance
 * juste avant afin d'avoir le bleu charge. Cette action amene le robot
 * pres de la balance et arrete la bonne pompe afin de ne liberer que le
 * blueium.
 *
 *  Created on: 22 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLDELIVERBLUEIUMINBALANCE_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLDELIVERBLUEIUMINBALANCE_H_

#include "match/2019/MatchData2019.h"
#include "match/MatchAction.h"
#include "../../Match2019TridentStorage.h"

class SmallDeliverBlueiumInBalance : public MatchAction {
private:
	Match2019TridentStorage &tridentStorage;

public:
	SmallDeliverBlueiumInBalance(Match2019TridentStorage &tridentStorage);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLDELIVERBLUEIUMINBALANCE_H_ */
