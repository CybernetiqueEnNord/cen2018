/*
 * SmallSideDispenserAction.h
 *
 * Action qui fait prendre au petit robot le distributeur sur le cote
 * de la table en bas de la pente, le fait monter la pente et le fait
 * revenir en dehors de la zone en bas de la pente afin qu'il puisse
 * executer d'autres actions.
 *
 *  Created on: 22 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLSIDEDISPENSERACTION_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLSIDEDISPENSERACTION_H_

#include "match/2019/MatchData2019.h"
#include "match/MatchAction.h"
#include "../../Match2019TridentStorage.h"

class SmallSideDispenserAction : public MatchAction {
private:
	Match2019TridentStorage &tridentStorage;

public:
	SmallSideDispenserAction(Match2019TridentStorage &tridentStorage);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLSIDEDISPENSERACTION_H_ */
