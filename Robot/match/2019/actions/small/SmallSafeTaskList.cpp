/*
 * SmallSafeTaskList.cpp
 *
 *  Created on: 28 mai 2019
 *      Author: Anastaszor
 */

#include "SmallSafeTaskList.h"

#include "../../../../devices/actuators/2019/small/SmallActuator.h"
#include "../../../../utils/Arrays.h"
#include "../../../../utils/Pointer.h"
#include "../../../MatchElement.h"
#include "../../../MatchTarget.h"
#include "../../Match2019SmallTridentAdapter.h"
#include "../../Match2019TridentStorage.h"
#include "../SingleActionList.h"
#include "SmallAlternateBlueiumAction.h"
#include "SmallDeliverBlueiumInBalance.h"
#include "SmallDeliverGoldeniumAction.h"
#include "SmallDeliverTrioInTable.h"
#include "SmallSideDispenserAction.h"
#include "SmallTakeGoldeniumAction.h"
#include "SmallTakeOpponentChaosAction.h"
#include "SmallTakeOppRack3NearBalanceAction.h"
#include "SmallTakeOppRack3NearStartAction.h"
#include "SmallTakeOurChaosAction.h"
#include "SmallTakeOurRack3NearBalanceAction.h"
#include "SmallTakeOurRack3NearStartAction.h"

SmallSafeTaskList::SmallSafeTaskList(MatchData2019 &matchData) :
		MatchTaskList(), matchData(matchData), tasks(createTasks()) {
}

unsigned int SmallSafeTaskList::getCount() const {
	return ARRAY_SIZE(tasks);
}

MatchTask& SmallSafeTaskList::getTaskAt(unsigned int index) const {
	return tasks[index];
}

MatchTask (&SmallSafeTaskList::createTasks())[9] {

	extern SmallActuator smallActuator;
	// on est sur le petit, il faut donc d'abord avoir un modele de trident
	// de plus il faut garder le meme trident storage pour toutes les actions
	static Match2019SmallTridentAdapter smallTridentAdapter(smallActuator);
	static Match2019TridentStorage tridentStorage(matchData, smallTridentAdapter);

	// {{{ TASK SMALL RACK

	static MatchElement smallDispenser;

	static SmallSideDispenserAction smallSideDispenserAction(tridentStorage);
	static SingleActionList smallSideDispenserActions(smallSideDispenserAction);
	static MatchTarget target10(smallSideDispenserActions, smallDispenser);

	static MatchTask taskSmallRack("smallrack", 1000, 0, Pointer<MatchTarget>(&target10));

	// }}} END TASK SMALL RACK

	// {{{ BLUEIUM A1

	static MatchElement blueium;

	static SmallAlternateBlueiumAction smallAlternateBlueiumAction(matchData);
	static SingleActionList smallAlternateBlueiumActions(smallAlternateBlueiumAction);
	static MatchTarget target01(smallAlternateBlueiumActions, blueium);

	static MatchTask taskBlueiumAlt("blueiumAlt", 1000, 500, Pointer<MatchTarget>(&target01));

	// }}} END BLUEIUM A1

	// {{{ TASK GOLDENIUM

	static MatchElement dispenserGoldElement;

	// take gold
	static SmallTakeGoldeniumAction smallTakeGoldeniumAction(matchData);
	static SingleActionList smallTakeGoldeniumActions(smallTakeGoldeniumAction);
	static MatchTarget target2(smallTakeGoldeniumActions, dispenserGoldElement);

	// depose gold
	static SmallDeliverGoldeniumAction smallDeliverGoldeniumAction(matchData);
	static SingleActionList smallDeliverGoldeniumActions(smallDeliverGoldeniumAction);
	static MatchTarget target3(smallDeliverGoldeniumActions, dispenserGoldElement);

	// link task
//	target1.setNext(target2);
	target2.setNext(target3);
	static MatchTask taskGoldenium("goldenium", 10 + 10 + 20 + 24, /* ? */40, Pointer<MatchTarget>(&target2));

	// }}} END TASK GOLDENIUM

	// {{{ TASK OUR CHAOS AREA

	static MatchElement ourChaosArea;

	static SmallTakeOurChaosAction smallTakeOurChaosAction(matchData);
	static SingleActionList smallTakeOurChaosActions(smallTakeOurChaosAction);
	static MatchTarget target15(smallTakeOurChaosActions, ourChaosArea);

	static MatchTask taskOurChaosArea("ourChaos", 6 + 6 + 1 + 1, 110, Pointer<MatchTarget>(&target15));

	// }}} END TASK OUR CHAOS AREA

	// {{{ TASK OUR BIG RACK NEAR START

	static MatchElement ourRackNearStart;

	static SmallTakeOurRack3NearStartAction smallTakeOurRack3NearStartAction(tridentStorage);
	static SingleActionList smallTakeOurRack3NearStartActions(smallTakeOurRack3NearStartAction);
	static MatchTarget target20(smallTakeOurRack3NearStartActions, ourRackNearStart);

	static SmallDeliverTrioInTable smallDeliverTrioInTableAction(tridentStorage);
	static SingleActionList smallDeliverTrioInTableActions(smallDeliverTrioInTableAction);
	static MatchTarget target21(smallDeliverTrioInTableActions, ourRackNearStart);

	target20.setNext(target21);
	static MatchTask taskOurBigRackNearStart("ourRackStart", 6 + 6 + 6, 180, Pointer<MatchTarget>(&target20));

	// }}} END TASK OUR BIG RACK NEAR START

	// {{{ TASK OUR BIG RACK NEAR BALANCE

	static MatchElement ourRackNearBalance;

	static SmallTakeOurRack3NearBalanceAction smallTakeOurRack3NearBalanceAction(tridentStorage);
	static SingleActionList smallTakeOurRack3NearBalanceActions(smallTakeOurRack3NearBalanceAction);
	static MatchTarget target30(smallTakeOurRack3NearBalanceActions, ourRackNearBalance);

	static SmallDeliverBlueiumInBalance smallDeliverBlueiumInBalanceAction(tridentStorage);
	static SingleActionList smallDeliverBlueiumInBalanceActions(smallDeliverBlueiumInBalanceAction);
	static MatchTarget target31(smallDeliverBlueiumInBalanceActions, ourRackNearBalance);

	static SmallDeliverTrioInTable smallDeliverTrioInTableAction2(tridentStorage);
	static SingleActionList smallDeliverTrioInTableActions2(smallDeliverTrioInTableAction2);
	static MatchTarget target32(smallDeliverTrioInTableActions2, ourRackNearStart);

	target30.setNext(target31);
	target31.setNext(target32);
	static MatchTask taskOurBigRackNearBalance("ourRackBalanc", 12 + 6 + 6, 260, Pointer<MatchTarget>(&target30));

	// }}} END TASK OUR BIG RACK NEAR BALANCE

	// {{{ TASK OPPONENT CHAOS AREA

	static MatchElement oppChaosArea;

	static SmallTakeOpponentChaosAction smallTakeOpponentChaosAction(matchData);
	static SingleActionList smallTakeOpponentChaosActions(smallTakeOpponentChaosAction);
	static MatchTarget target35(smallTakeOpponentChaosActions, oppChaosArea);

	static MatchTask taskOppChaosArea("oppChaos", 6 + 6 + 1 + 1, 1600, Pointer<MatchTarget>(&target35));

	// }}} END TASK OPPONENT CHAOS AREA

	// {{{ TASK OPPONENT BIG RACK NEAR BALANCE

	static MatchElement oppRackNearBalance;

	static SmallTakeOppRack3NearBalanceAction smallTakeOppRack3NearBalanceAction(tridentStorage);
	static SingleActionList smallTakeOppRack3NearBalanceActions(smallTakeOppRack3NearBalanceAction);
	static MatchTarget target40(smallTakeOppRack3NearBalanceActions, oppRackNearBalance);

	static SmallDeliverBlueiumInBalance smallDeliverBlueiumInBalanceAction3(tridentStorage);
	static SingleActionList smallDeliverBlueiumInBalanceActions3(smallDeliverBlueiumInBalanceAction3);
	static MatchTarget target41(smallDeliverBlueiumInBalanceActions3, ourRackNearBalance);

	static SmallDeliverTrioInTable smallDeliverTrioInTableAction3(tridentStorage);
	static SingleActionList smallDeliverTrioInTableActions3(smallDeliverTrioInTableAction3);
	static MatchTarget target42(smallDeliverTrioInTableActions3, ourRackNearStart);

	target40.setNext(target41);
	target41.setNext(target42);
	static MatchTask taskOppBigRackNearBalance("oppRackBalan", 12 + 6 + 6, 5200, Pointer<MatchTarget>(&target40));

	// }}} END TASK OPPONENT BIG RACK NEAR BALANCE

	// {{{ TASK OPPONENT BIG RACK NEAR START

	static MatchElement oppRackNearStart;

	static SmallTakeOppRack3NearStartAction smallTakeOppRack3NearStartAction(tridentStorage);
	static SingleActionList smallTakeOppRack3NearStartActions(smallTakeOppRack3NearStartAction);
	static MatchTarget target50(smallTakeOppRack3NearStartActions, oppRackNearStart);

	static SmallDeliverTrioInTable smallDeliverTrioInTableAction4(tridentStorage);
	static SingleActionList smallDeliverTrioInTableActions4(smallDeliverTrioInTableAction4);
	static MatchTarget target51(smallDeliverTrioInTableActions4, oppRackNearStart);

	target50.setNext(target51);
	static MatchTask taskOppBigRackNearStart("oppRackStart", 6 + 6 + 6, 4000, Pointer<MatchTarget>(&target50));

	// }}} END TASK OPPONENT BIG RACK NEAR START

	static MatchTask tasks[9] = {
		taskSmallRack,
		taskBlueiumAlt,
		taskGoldenium,
		taskOurChaosArea,
		taskOurBigRackNearStart,
		taskOurBigRackNearBalance,
		taskOppChaosArea,
		taskOppBigRackNearBalance,
		taskOppBigRackNearStart
	};

	return tasks;
}
