/*
 * SmallDeliverBlueiumInBalance.cpp
 *
 *  Created on: 22 mai 2019
 *      Author: Anastaszor
 */

#include "devices/actuators/2019/small/SmallActuator.h"
#include "devices/host/RobotGeometry.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"
#include "SmallDeliverBlueiumInBalance.h"

SmallDeliverBlueiumInBalance::SmallDeliverBlueiumInBalance(Match2019TridentStorage &tridentStorage) :
		MatchAction("smallDeliverBlue", Point(1350, 1050)), tridentStorage(tridentStorage) {
}

void SmallDeliverBlueiumInBalance::executeStep(Navigation &navigation, unsigned int step) {
//	extern SmallActuator smallActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
//	PositionControl &control = navigation.getControl();

	switch (step) {
		case 0:
			navigation.moveTo(Point(1622 - geometry.frontWidth, 1350));
			tridentStorage.unloadBlueiumsInBalance();
			break;

		case 1:
			// marche arriere pour se liberer de la balance
			navigation.moveToBackwards(Point(1350, 1050));
			break;

		case STEP_FINALIZATION:
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
