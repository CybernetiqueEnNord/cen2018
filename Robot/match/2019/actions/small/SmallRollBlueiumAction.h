/*
 * SmallRollBlueiumAction.h
 *
 * Action qui fait arriver le robot sur le cote pour pousser le blueium qui
 * est pose sur le plateau au pied du poteau central dans l'accelerateur qui
 * est a cote de lui. Cette action est designee pour etre executee a pleine
 * vitesse au depart du match afin de perdre le moins de temps possible dans
 * la prise du gold afin que le robot degage de la zone adverse.
 *
 *  Created on: 21 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLROLLBLUEIUMACTION_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLROLLBLUEIUMACTION_H_

#include "match/2019/MatchData2019.h"
#include "match/MatchAction.h"

class SmallRollBlueiumAction : public MatchAction {
private:
	MatchData2019 &matchData;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	SmallRollBlueiumAction(MatchData2019 &matchData);
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLROLLBLUEIUMACTION_H_ */
