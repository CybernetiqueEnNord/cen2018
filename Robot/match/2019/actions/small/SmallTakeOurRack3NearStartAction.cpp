/*
 * SmallTakeOurRack3NearStartAction.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Anastaszor
 */

#include "SmallTakeOurRack3NearStartAction.h"

#include "../../../../control/Navigation.h"
#include "../../../../control/PositionControl.h"
#include "../../../../devices/actuators/2019/small/SmallActuator.h"
#include "../../../../devices/host/HostData.h"
#include "../../../../devices/host/HostDevice.h"
#include "../../../../devices/host/RobotGeometry.h"
#include "../../../../geometry/Point.h"
#include "../../Match2019TridentStorage.h"

SmallTakeOurRack3NearStartAction::SmallTakeOurRack3NearStartAction(Match2019TridentStorage &tridentStorage) :
		MatchAction("takeOurRack3NearStart", Point(1300, 600)), tridentStorage(tridentStorage) {
}

void SmallTakeOurRack3NearStartAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	PositionControl &control = navigation.getControl();
	control.setSpeedIndex(SPEED_SMALL_MAX_FAST_LINEAR);

	switch (step) {
		case 0:
			// on se positionne devant le rack de notre cote
			smallActuator.setFlapPickup();
			navigation.rotateToOrientationDeciDegrees(0);
			navigation.repositionMM(300, 1543 - geometry.frontWidth, CURRENT, 0);
			break;

		case 1:
			// Prise des 3 atomes
			tridentStorage.loadFromOurRackNearStart();
			navigation.waitForDelay(300);
			break;

		case 2: {
			// R�duction de la vitesse
			PositionControl &control = navigation.getControl();
			int tempSpeedIndex = control.getSpeedIndex();
			control.setSpeedIndex(0);

			// Extraction
			navigation.moveMM(-65);

			control.setSpeedIndex(tempSpeedIndex);
			break;
		}

		case STEP_FINALIZATION:
			// nothing to do
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
