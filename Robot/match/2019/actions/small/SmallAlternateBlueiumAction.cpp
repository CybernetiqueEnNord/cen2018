/*
 * SmallAlternateBlueiumAction.cpp
 *
 *  Created on: 28 mai 2019
 *      Author: Anastaszor
 */

#include "SmallAlternateBlueiumAction.h"

#include "../../../../devices/actuators/2019/small/SmallActuator.h"
#include "../../../../devices/host/HostData.h"
#include "../../../../devices/host/HostDevice.h"
#include "../../../../geometry/Point.h"


SmallAlternateBlueiumAction::SmallAlternateBlueiumAction(MatchData2019 &matchData) :
		MatchAction("smallTakeGold", Point(800, 1500)), matchData(matchData) {
}

void SmallAlternateBlueiumAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();

	switch (step) {
		case 0:
			navigation.moveTo(Point(200, 1600));
			break;

		case 1:
			navigation.rotateToOrientationDeciDegrees(0);
			break;

		case 2:
			navigation.repositionMM(-300, 30 + geometry.rearWidth, CURRENT, 0);
			break;

		case 3:
			navigation.moveMM(200 - geometry.rearWidth);
			break;

		case 4:
			navigation.rotateToOrientationDeciDegrees(900);
			break;

		case 5:
			smallActuator.setArm(true);
			navigation.waitForDelay(200);
			navigation.moveTo(Point(230, 1738)); // position blue + 1/2 palet
			break;

		case 6:
			smallActuator.setArm(false);
			navigation.waitForDelay(200);
			navigation.moveTo(Point(300, 2225));
			break;

		case STEP_FINALIZATION:
			smallActuator.setArm(false);
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
