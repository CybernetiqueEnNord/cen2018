/*
 * SmallTakeOppRack3NearBalanceAction.h
 *
 * Action qui prend les 3 palets les plus proches de la balance dans le
 * grand rack de notre adversaire. Le robot se place devant le grand rack,
 * active les pompes et ensuite avance pour ventouser les palets, et fait
 * une marche arriere et un demi tour pour pouvoir repartir sur une autre
 * action de notre cote du terrain.
 *
 *  Created on: 21 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLTAKEOPPRACK3NEARBALANCEACTION_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLTAKEOPPRACK3NEARBALANCEACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"
#include "../../Match2019TridentStorage.h"

class SmallTakeOppRack3NearBalanceAction : public MatchAction {
private:
	Match2019TridentStorage &tridentStorage;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	SmallTakeOppRack3NearBalanceAction(Match2019TridentStorage &tridentStorage);
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLTAKEOPPRACK3NEARBALANCEACTION_H_ */
