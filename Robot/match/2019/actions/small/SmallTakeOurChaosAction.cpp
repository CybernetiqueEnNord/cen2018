/*
 * SmallTakeOurChaosAction.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Anastaszor
 */

#include "SmallTakeOurChaosAction.h"

#include "devices/actuators/2019/small/SmallActuator.h"
#include "devices/host/HostDevice.h"

SmallTakeOurChaosAction::SmallTakeOurChaosAction(MatchData2019 &matchData) :
		MatchAction("oppChaos", Point(1200, 1200)), matchData(matchData) {
	// le point de depart est au milieu du terrain parce que c'est la
	// qu'on a le moins de chances de renverser le chaos qu'on essaye
	// de prendre
}

void SmallTakeOurChaosAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;
//	const HostData &data = HostDevice::getHostData();
//	const RobotGeometry &geometry = data.getGeometry();
	switch (step) {
		case 0:
			// on sort les flip flap
			smallActuator.setFlapPickup();
			// et on pousse tout jusqu'a notre zone de depart
			navigation.moveTo(Point(600, 450));
			break;

		case STEP_FINALIZATION:
			// nothing to do
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
