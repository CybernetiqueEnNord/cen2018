/*
 * SmallPrimaryTaskList.h
 *
 *  Created on: 22 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLPRIMARYTASKLIST_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLPRIMARYTASKLIST_H_

#include "match/MatchTaskList.h"
#include "match/2019/MatchData2019.h"

class SmallPrimaryTaskList : public MatchTaskList {
private:
	MatchData2019 &matchData;
	MatchTask (&tasks)[10];

	MatchTask (&createTasks())[10];

public:
	SmallPrimaryTaskList(MatchData2019 &matchData);
	virtual unsigned int getCount() const;
	virtual MatchTask& getTaskAt(unsigned int index) const;
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLPRIMARYTASKLIST_H_ */
