/*
 * SmallTakeGoldeniumAction.cpp
 *
 *  Created on: 22 mai 2019
 *      Author: Anastaszor
 */

#include "SmallTakeGoldeniumAction.h"

#include "devices/actuators/2019/small/SmallActuator.h"
#include "devices/host/RobotGeometry.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"

SmallTakeGoldeniumAction::SmallTakeGoldeniumAction(MatchData2019 &matchData) :
		MatchAction("smallTakeGold", Point(300, 2225)), matchData(matchData) {
}

void SmallTakeGoldeniumAction::executeStep(Navigation &navigation, unsigned int step) {
//	extern SmallActuator smallActuator;
//	const HostData &data = HostDevice::getHostData();
//	const RobotGeometry &geometry = data.getGeometry();

	switch (step) {
		case 0:
			navigation.moveTo(Point(175, 2225));
			//navigation.rotateToOrientationDeciDegrees(1800);
			break;

		case 1:
			//navigation.repositionMM(200, CURRENT, CURRENT, CURRENT);
			break;

		case 2: {
			extern SmallActuator smallActuator;
			smallActuator.checkGoldenium();
			navigation.waitForDelay(500);
			bool doorOpen = smallActuator.queryGoldeniumDoorOpen();
			if (!doorOpen) {
				navigation.moveMM(-120);
				MatchAction::executeStep(navigation, step);
				break;
			}
			matchData.addToAccelerateur();
			matchData.unlockGoldenium();
			smallActuator.setPumpGoldenium(true);
			navigation.waitForDelay(500); //Delay to have time to get the goldenium
			break;
		}

		case 3: {
			//Reduction de la vitesse car le goldenium frote sur son logement:
			PositionControl &control = navigation.getControl();
			int tempSpeedIndex = control.getSpeedIndex();
			control.setSpeedIndex(0);

			//extraction du goldenium
			navigation.moveMM(-120);
			control.setSpeedIndex(tempSpeedIndex);
			matchData.takeGoldeniumOffAcelerateur();
			break;
		}

		case STEP_FINALIZATION: {
			// nothing to do
			break;
		}

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}

void SmallTakeGoldeniumAction::setChainWithDispenser(bool value) {
	chainWithDispenser = value;
}

