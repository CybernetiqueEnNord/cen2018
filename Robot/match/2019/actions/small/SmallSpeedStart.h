/*
 * SmallSpeedStart.h
 *
 * Action qui
 *
 *  Created on: 26 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLSPEEDSTART_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLSPEEDSTART_H_

#include "../../../MatchAction.h"
#include "../../MatchData2019.h"

class SmallSpeedStart : public MatchAction {
private:
	MatchData2019 &matchData;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	SmallSpeedStart(MatchData2019 &matchData);
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLSPEEDSTART_H_ */
