/*
 * SmallTakeGoldeniumAction.h
 *
 * Action qui fait prendre le goldenium au petit robot. Attention, cette
 * action necessite que le goldenium soit ouvert, cad que soit le blueium
 * qui est present au debut de la pente soit tombe dans l'accelerateur,
 * soit que le gros ait deja verse dans l'accelerateur des palets. Cette
 * action met le petit en position contre le goldenium et l'extrait
 * uniquement.
 *
 *  Created on: 22 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLTAKEGOLDENIUMACTION_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLTAKEGOLDENIUMACTION_H_

#include "match/2019/MatchData2019.h"
#include "match/MatchAction.h"

class SmallTakeGoldeniumAction : public MatchAction {
private:
	MatchData2019 &matchData;
	bool chainWithDispenser = false;

public:
	SmallTakeGoldeniumAction(MatchData2019 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
	void setChainWithDispenser(bool value);
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLTAKEGOLDENIUMACTION_H_ */
