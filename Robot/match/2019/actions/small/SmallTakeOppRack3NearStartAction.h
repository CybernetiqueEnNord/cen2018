/*
 * SmallTakeOppRack3NearStartAction.h
 *
 * Action qui prend les 3 palets les plus proches de la zone de depart
 * adverse dans le grand rack du cote adverse. Le robot se place devant
 * le grand rack, active les pompes et ensuite avance pour ventouser les
 * palets, et fait une marche arriere et un demi tour pour pouvoir repartir
 * sur une autre action de notre cote de la table.
 *
 *  Created on: 21 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLTAKEOPPRACK3NEARSTARTACTION_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLTAKEOPPRACK3NEARSTARTACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"
#include "../../Match2019TridentStorage.h"

class SmallTakeOppRack3NearStartAction : public MatchAction {
private:
	Match2019TridentStorage &tridentStorage;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	SmallTakeOppRack3NearStartAction(Match2019TridentStorage &tridentStorage);
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLTAKEOPPRACK3NEARSTARTACTION_H_ */
