/*
 * SmallDeliverTrioInTable.cpp
 *
 *  Created on: 22 mai 2019
 *      Author: Anastaszor
 */

#include "SmallDeliverTrioInTable.h"

#include "devices/actuators/2019/small/SmallActuator.h"
#include "devices/host/RobotGeometry.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"

SmallDeliverTrioInTable::SmallDeliverTrioInTable(Match2019TridentStorage &tridentStorage) :
		MatchAction("smallDeliverGold", Point(1050, 800)), tridentStorage(tridentStorage) {
}

void SmallDeliverTrioInTable::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;
//	const HostData &data = HostDevice::getHostData();
//	const RobotGeometry &geometry = data.getGeometry();
//	PositionControl &control = navigation.getControl();

	switch (step) {
		case 0:
			if(tridentStorage.hasBlueiums())
			{
				// on depose les blueiums quand y'en a
				navigation.moveTo(Point(1050, 700));
			}
			break;

		case 1:
			if(tridentStorage.hasBlueiums())
			{
				tridentStorage.unloadBlueiumsInArea();
				navigation.moveToBackwards(Point(1050, 700));
			}
			break;

		case 2:
			if(tridentStorage.hasGreeniums())
			{
				// on depose les greeniums quand y'en a
				navigation.moveTo(Point(750, 400));
			}
			break;

		case 3:
			if(tridentStorage.hasGreeniums())
			{
				tridentStorage.unloadGreeniumsInArea();
				navigation.moveToBackwards(Point(750, 700));
			}
			break;

		case 4:
			if(tridentStorage.hasRediums())
			{
				// on depose les rediums quand y'en a
				navigation.moveTo(Point(450, 700));
			}
			break;

		case 5:
			if(tridentStorage.hasRediums())
			{
				navigation.moveTo(Point(450, 400));
				tridentStorage.unloadRediumsInArea();
				navigation.moveToBackwards(Point(450, 700));
			}
			break;

		case STEP_FINALIZATION:
			smallActuator.setPumpTrio(false);
			smallActuator.setFlapClosed();
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
