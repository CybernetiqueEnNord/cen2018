/*
 * SmallTakeOurChaosAction.h
 *
 * Action qui ramasse les atomes de notre zone de chaos avec le petit
 * robot. Le robot se place au milieu de la zone et pousse les palets
 * qui sont par terre et les ramene en ligne droite dans notre zone de
 * depart (rouge).
 *
 *  Created on: 21 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLTAKEOURCHAOSACTION_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLTAKEOURCHAOSACTION_H_

#include "match/2019/MatchData2019.h"
#include "match/MatchAction.h"

class SmallTakeOurChaosAction : public MatchAction {
private:
	MatchData2019 &matchData;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	SmallTakeOurChaosAction(MatchData2019 &matchData);
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLTAKEOURCHAOSACTION_H_ */
