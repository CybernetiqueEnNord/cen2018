/*
 * SmallTakeOpponentChaosAction.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Anastaszor
 */

#include "SmallTakeOpponentChaosAction.h"

#include "devices/actuators/2019/small/SmallActuator.h"
#include "devices/host/HostDevice.h"

SmallTakeOpponentChaosAction::SmallTakeOpponentChaosAction(MatchData2019 &matchData) :
		MatchAction("oppChaos", Point(1050, 1700)), matchData(matchData) {
	// le point de depart est au milieu du terrain parce que c'est la
	// qu'on a le moins de chances de renverser le chaos qu'on essaye
	// de prendre
}

void SmallTakeOpponentChaosAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;
//	const HostData &data = HostDevice::getHostData();
//	const RobotGeometry &geometry = data.getGeometry();

	switch (step) {
		case 0:
			// on s'oriente correctement (on regarde depuis la balance vers le poteau central)
			navigation.rotateToOrientationDeciDegrees(1800);
			// 1/4 de tour autour de la zone
			navigation.moveMMCircleDecidegree(500, -900);	// rayon 40cm, angle par rapport a la position initale + -90�
			// et encore un
			navigation.moveMMCircleDecidegree(500, -900);
			// et on se place dans l'angle vers la zone de depart
			navigation.rotateToOrientationDeciDegrees(2700);
			break;

		case 1:
			// on sort les flip flap
			smallActuator.setFlapPickup();
			// et on pousse tout jusqu'a notre zone de depart
			navigation.moveTo(Point(450, 450));
			break;

		case STEP_FINALIZATION:
			// nothing to do
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
