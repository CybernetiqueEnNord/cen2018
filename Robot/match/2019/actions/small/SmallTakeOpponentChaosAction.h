/*
 * SmallTakeOpponentChaosAction.h
 *
 * Action qui ramasse les atomes adverses avec le petit robot. Le robot
 * se place du cote de la zone de depart adverse pour prendre les palets
 * qui sont par terre et les ramene en ligne droite dans notre zone de
 * depart (rouge).
 *
 *  Created on: 21 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLTAKEOPPONENTCHAOSACTION_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLTAKEOPPONENTCHAOSACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"

class SmallTakeOpponentChaosAction : public MatchAction {
private:
	MatchData2019 &matchData;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	SmallTakeOpponentChaosAction(MatchData2019 &matchData);
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLTAKEOPPONENTCHAOSACTION_H_ */
