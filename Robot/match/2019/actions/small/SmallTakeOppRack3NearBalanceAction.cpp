/*
 * SmallTakeOppRack3NearBalanceAction.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Anastaszor
 */

#include "SmallTakeOppRack3NearBalanceAction.h"

#include "../../../../control/Navigation.h"
#include "../../../../control/PositionControl.h"
#include "../../../../devices/actuators/2019/small/SmallActuator.h"
#include "../../../../devices/host/HostData.h"
#include "../../../../devices/host/HostDevice.h"
#include "../../../../devices/host/RobotGeometry.h"
#include "../../../../geometry/Point.h"
#include "../../Match2019TridentStorage.h"

SmallTakeOppRack3NearBalanceAction::SmallTakeOppRack3NearBalanceAction(Match2019TridentStorage &tridentStorage) :
		MatchAction("takeOppRack3NearBalance", Point(1300, 2100)), tridentStorage(tridentStorage) {
}

void SmallTakeOppRack3NearBalanceAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();

	switch (step) {
		case 0:
			// on se positionne devant le rack du cote adv
			smallActuator.setFlapPickup();
			navigation.moveTo(Point(1543 - geometry.frontWidth, 2100));
			break;

		case 1:
			// Prise des 3 atomes
			tridentStorage.loadFromOppRackNearBalance();
			navigation.waitForDelay(300);
			break;

		case 2: {
			// R�duction de la vitesse
			PositionControl &control = navigation.getControl();
			int tempSpeedIndex = control.getSpeedIndex();
			control.setSpeedIndex(0);

			// Extraction
			navigation.moveMM(-65);

			control.setSpeedIndex(tempSpeedIndex);
			break;
		}

		case 3:
			// demi tour et on avance pour passer la bordure au centre
			navigation.moveTo(Point(1050, 2100));
			break;

		case STEP_FINALIZATION:
			// nothing to do
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
