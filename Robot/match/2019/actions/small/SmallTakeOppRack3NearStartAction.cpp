/*
 * SmallTakeOppRack3NearStartAction.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Anastaszor
 */

#include "SmallTakeOppRack3NearStartAction.h"

#include "../../../../control/Navigation.h"
#include "../../../../control/PositionControl.h"
#include "../../../../devices/actuators/2019/small/SmallActuator.h"
#include "../../../../devices/host/HostData.h"
#include "../../../../devices/host/HostDevice.h"
#include "../../../../devices/host/RobotGeometry.h"
#include "../../../../geometry/Point.h"
#include "../../Match2019TridentStorage.h"

SmallTakeOppRack3NearStartAction::SmallTakeOppRack3NearStartAction(Match2019TridentStorage &tridentStorage) :
		MatchAction("takeOppRack3NearStart", Point(1300, 2400)), tridentStorage(tridentStorage) {
}

void SmallTakeOppRack3NearStartAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();

	switch (step) {
		case 0:
			// on se positionne devant le rack de notre cote
			smallActuator.setFlapPickup();
			navigation.moveTo(Point(1543 - geometry.frontWidth, 2400));
			break;

		case 1:
			// Prise des 3 atomes
			tridentStorage.loadFromOppRackNearStart();
			navigation.waitForDelay(300);
			break;

		case 2: {
			// R�duction de la vitesse
			PositionControl &control = navigation.getControl();
			int tempSpeedIndex = control.getSpeedIndex();
			control.setSpeedIndex(0);

			// Extraction
			navigation.moveMM(-65);

			control.setSpeedIndex(tempSpeedIndex);
			break;
		}

		case STEP_FINALIZATION:
			// nothing to do
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}

