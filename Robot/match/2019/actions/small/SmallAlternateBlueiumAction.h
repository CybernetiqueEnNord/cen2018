/*
 * SmallAlternateBlueiumAction.h
 *
 *  Created on: 28 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_ACTIONS_SMALL_SMALLALTERNATEBLUEIUMACTION_H_
#define MATCH_2019_ACTIONS_SMALL_SMALLALTERNATEBLUEIUMACTION_H_

#include "../../../MatchAction.h"
#include "../../MatchData2019.h"

class SmallAlternateBlueiumAction : public MatchAction {
private:
	MatchData2019 &matchData;

public:
	SmallAlternateBlueiumAction(MatchData2019 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2019_ACTIONS_SMALL_SMALLALTERNATEBLUEIUMACTION_H_ */
