/*
 * GoldeniumAction.h
 *
 *  Created on: 3 mars 2019
 *      Author: emmanuel
 */

#ifndef MATCH_2019_ACTIONS_GOLDENIUMACTION_H_
#define MATCH_2019_ACTIONS_GOLDENIUMACTION_H_

#include "match/2019/MatchData2019.h"
#include "match/MatchAction.h"

class GoldeniumAction : public MatchAction {
private:
	MatchData2019 &matchData;
	bool chainWithDispenser = false;

public:
	GoldeniumAction(MatchData2019 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
	void setChainWithDispenser(bool value);
};

#endif /* MATCH_2019_ACTIONS_GOLDENIUMACTION_H_ */
