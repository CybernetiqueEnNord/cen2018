/*
 * SmallAtomsDispenserAction.h
 *
 *  Created on: 7 mai 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_ACTIONS_SMALLATOMSDISPENSERACTION_H_
#define MATCH_2019_ACTIONS_SMALLATOMSDISPENSERACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"

class SmallAtomsDispenserAction : public MatchAction {
private:
	MatchData2019 &matchData;
	bool hasGoldenium = false;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	SmallAtomsDispenserAction(MatchData2019 &matchData);
	void setHasGoldenium(bool value);
};

#endif /* MATCH_2019_ACTIONS_SMALLATOMSDISPENSERACTION_H_ */
