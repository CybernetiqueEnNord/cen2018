/*
 * Target1ActionList.cpp
 *
 *  Created on: 1 mars 2019
 *      Author: Emmanuel
 */

#include "Target1ActionList.h"

Target1ActionList::Target1ActionList() {
}

MatchAction& Target1ActionList::getActionAt(unsigned int index) const {
	switch (index) {
		case 0:
			static Point action1Start(1480, 200);
			static MatchAction action1("prise", action1Start);
			return action1;
			break;

		case 1:
			static Point action2Start(1460, 200);
			static MatchAction action2("depose", action1Start);
			return action2;
			break;

		default:
			return MatchAction::none;
	}
}

unsigned int Target1ActionList::getCount() const {
	return 2;
}
