/*
 * BlueiumAction.h
 *
 *  Created on: 2 mars 2019
 *      Author: emmanuel
 */

#ifndef MATCH_2019_ACTIONS_BLUEIUMACTION_H_
#define MATCH_2019_ACTIONS_BLUEIUMACTION_H_

#include "match/2019/MatchData2019.h"
#include "match/MatchAction.h"

class BlueiumAction : public MatchAction {
private:
	MatchData2019 &matchData;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	BlueiumAction(MatchData2019 &matchData);
};

#endif /* MATCH_2019_ACTIONS_BLUEIUMACTION_H_ */
