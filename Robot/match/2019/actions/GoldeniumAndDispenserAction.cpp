/*
 * GoldeniumAndDispenserAction.cpp
 *
 *  Created on: 8 mai 2019
 *      Author: Emmanuel
 *      @deprecated on a pas les moyens de faire les 2 en mm temps
 */

#include "GoldeniumAndDispenserAction.h"

GoldeniumAndDispenserAction::GoldeniumAndDispenserAction(MatchData2019 &matchData) :
		MatchAction("goldenium+atoms", Point(300, 2225)), goldeniumAction(matchData), dispenserAction(matchData) {
	goldeniumAction.setChainWithDispenser(true);
	dispenserAction.setHasGoldenium(true);
}

void GoldeniumAndDispenserAction::executeStep(Navigation &navigation, unsigned int step) {
	if (step == STEP_FINALIZATION) {
		return;
	}

	bool goldeniumDone = executeActionStep(goldeniumAction, navigation, step);
	bool dispenserDone = goldeniumDone && executeActionStep(dispenserAction, navigation, step);
	terminated = dispenserDone;
}

bool GoldeniumAndDispenserAction::executeActionStep(MatchAction &action, Navigation &navigation, unsigned int step) {
	if (!action.isTerminated()) {
		action.executeStep(navigation, step - stepsOffset);
		if (action.isTerminated()) {
			// la prochaine �tape sera l'�tape 0 de la prochaine action
			stepsOffset = step + 1;
		}
		return false;
	}
	return true;
}
