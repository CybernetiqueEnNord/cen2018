/*
 * GoldeniumAction.cpp
 *
 *  Created on: 3 mars 2019
 *      Author: emmanuel
 *      // Attention il manque entre les 2 actions
 *      // l'action step 3 navigation.moveTo(Point(1300, 1100));
 */

#include "GoldeniumAction.h"

#include "../../../devices/actuators/2019/small/SmallActuator.h"
#include "devices/host/RobotGeometry.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"

GoldeniumAction::GoldeniumAction(MatchData2019 &matchData) :
		MatchAction("goldenium", Point(300, 2225)), matchData(matchData) {
}

void GoldeniumAction::executeStep(Navigation &navigation, unsigned int step) {
	switch (step) {
		case 0:
			navigation.moveTo(Point(300, 2225));
			navigation.moveTo(Point(155, 2225));
			break;

		case 1:
			extern SmallActuator smallActuator;
			smallActuator.setPumpGoldenium(true);
			navigation.waitForDelay(500); //Delay to have time to get the goldenium
			break;

		case 2: {
			//Reduction de la vitesse car le goldenium frote sur son logement:
			PositionControl &control = navigation.getControl();
			int tempSpeedIndex = control.getSpeedIndex();
			control.setSpeedIndex(0);

			//extraction du goldenium
			navigation.moveMM(-120);
			control.setSpeedIndex(tempSpeedIndex);
			matchData.takeGoldeniumOffAcelerateur();
			break;
		}

		case 3:
			// acc�s � la pente
			navigation.moveTo(Point(1300, 1100));
			navigation.moveTo(Point(1300, 200));
			break;

		case 4:
			if (chainWithDispenser) {
				// si trajectoire combin�e avec le distributeur, on encha�ne sur l'autre trajectoire
				terminated = true;
			} else {
				// devant � la pente
				navigation.rotateToOrientationDeciDegrees(1800);
			}
			break;

		case 5: {
			// recalage en face de la pente
			const HostData &data = HostDevice::getHostData();
			const RobotGeometry &geometry = data.getGeometry();
			navigation.moveMM(-650 + geometry.rearWidth + 30);
			navigation.repositionMM(-300, CURRENT, 2000 - geometry.rearWidth, 1800);

			navigation.moveMM(200 - geometry.rearWidth);
			navigation.rotateDeciDegrees(-900);
			navigation.moveMM(-200 + geometry.rearWidth + 30);
			navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
			break;
		}

		case 6: {
			//mont�e
			PositionControl &control = navigation.getControl();
			control.setSpeedIndex(1);
			navigation.moveMM(1075);
			matchData.addGreeniumToBalance();
			break;
		}

		case 7:
			navigation.moveMM(-120);
			break;

		case 8:
			smallActuator.setPumpGoldenium(false);
			navigation.waitForDelay(1000); //let the goldenium fall
			break;

		case 9:
			// pouss�e
			navigation.moveMM(120);
			matchData.addGoldeniumToBalance();
			break;

		case 10:
			// recul
			navigation.moveMM(-900);
			break;

		case 11: {
			// recalage en face de la pente
			const HostData &data = HostDevice::getHostData();
			const RobotGeometry &geometry = data.getGeometry();
			navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
			break;
		}

		case 12: {
			// recalage lat�ral
			const HostData &data = HostDevice::getHostData();
			const RobotGeometry &geometry = data.getGeometry();
			navigation.moveMM(225 - geometry.rearWidth);
			navigation.rotateDeciDegrees(900);
			navigation.repositionMM(-300, 2000 - geometry.rearWidth, CURRENT, 1800);
			navigation.moveMM(150);
			break;
		}

			// Greenium
		case 13:
			navigation.moveTo(Point(2000 - 155, 225));
			break;

		case 14:
			smallActuator.setPumpGoldenium(true);
			navigation.waitForDelay(500); //Delay to have time to get the goldenium
			break;

		case 15: {
			//Reduction de la vitesse car le goldenium frote sur son logement:
			PositionControl &control = navigation.getControl();
			int tempSpeedIndex = control.getSpeedIndex();
			control.setSpeedIndex(0);

			//extraction du greenium
			navigation.moveMM(-45);
			control.setSpeedIndex(tempSpeedIndex);
			break;
		}

		case 16: {
			//mont�e
			PositionControl &control = navigation.getControl();
			control.setSpeedIndex(1);
			navigation.rotateToOrientationDeciDegrees(900);
			navigation.moveMM(675);
			smallActuator.setPumpGoldenium(false);
			navigation.moveMM(400);
			matchData.addGreeniumToBalance();
			break;
		}

		case 17:
			// recul
			navigation.moveMM(-900);
			break;

			// Blueium
		case 18: {
			// recalage en face de la pente
			const HostData &data = HostDevice::getHostData();
			const RobotGeometry &geometry = data.getGeometry();
			navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
			break;
		}

		case 19:
			navigation.moveTo(Point(1800, 130));
			navigation.moveTo(Point(1845, 130));
			break;

		case 20:
			smallActuator.setPumpGoldenium(true);
			navigation.waitForDelay(500); //Delay to have time to get the goldenium
			break;

		case 21: {
			//Reduction de la vitesse car le goldenium frote sur son logement:
			PositionControl &control = navigation.getControl();
			int tempSpeedIndex = control.getSpeedIndex();
			control.setSpeedIndex(0);

			//extraction du greenium
			navigation.moveMM(-45);
			control.setSpeedIndex(tempSpeedIndex);
			break;
		}

		case 22: {
			//mont�e
			PositionControl &control = navigation.getControl();
			control.setSpeedIndex(1);
			navigation.rotateToOrientationDeciDegrees(900);
			navigation.moveMM(675);
			smallActuator.setPumpGoldenium(false);
			navigation.moveMM(400);
			matchData.addBlueiumToBalance();
			break;
		}

		case 23:
			// recul
			navigation.moveMM(-900);
			break;

		case STEP_FINALIZATION: {
			PositionControl &control = navigation.getControl();
			control.setSpeedIndex(3);
			navigation.moveTo(Point(1500, 200));
			break;
		}

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}

void GoldeniumAction::setChainWithDispenser(bool value) {
	chainWithDispenser = value;
}
