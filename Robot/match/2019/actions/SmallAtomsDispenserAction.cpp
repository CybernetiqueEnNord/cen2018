/*
 * SmallAtomsDispenserAction.cpp
 *
 *  Created on: 7 mai 2019
 *      Author: Emmanuel
 */

#include "SmallAtomsDispenserAction.h"

#include "../../../devices/actuators/2019/small/SmallActuator.h"

SmallAtomsDispenserAction::SmallAtomsDispenserAction(MatchData2019 &matchData) :
		MatchAction("distributeur", Point(1370, 225)), matchData(matchData) {
}

void SmallAtomsDispenserAction::executeStep(Navigation &navigation, unsigned int step) {
	extern SmallActuator smallActuator;
	switch (step) {
		case 0:
			// Devant le distributeur
			smallActuator.setFlapPickup();
			navigation.moveTo(Point(1865, 225));
			break;

		case 1:
			// Prise des 3 atomes
			smallActuator.setPumpTrio(true);
			navigation.waitForDelay(300);
			break;

		case 2: {
			// R�duction de la vitesse
			PositionControl &control = navigation.getControl();
			int tempSpeedIndex = control.getSpeedIndex();
			control.setSpeedIndex(0);

			// Extraction
			navigation.moveMM(-65);

			control.setSpeedIndex(tempSpeedIndex);
			break;
		}

		case 3: {
			// Mont�e
			PositionControl &control = navigation.getControl();
			control.setSpeedIndex(1);
			smallActuator.setFlapClosed(); //pour la rotation
			navigation.rotateToOrientationDeciDegrees(900);
			smallActuator.setFlapPickup();
			navigation.moveMM(675);
			smallActuator.setPumpTrio(false);
			smallActuator.setPumpGoldenium(false);
			navigation.waitForDelay(500);
			navigation.moveMM(245);
			matchData.addRediumToBalance();
			matchData.addGreeniumToBalance();
			matchData.addBlueiumToBalance();
			if (hasGoldenium) {
				matchData.addGoldeniumToBalance();
			}
			smallActuator.setFlapClosed();
			break;
		}

		case 4:
			// recul
			navigation.moveMM(-900);
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}

void SmallAtomsDispenserAction::setHasGoldenium(bool value) {
	hasGoldenium = value;
}
