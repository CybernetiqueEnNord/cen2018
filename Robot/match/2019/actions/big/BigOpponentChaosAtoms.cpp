/*
 * BigOpponentChaosAtoms.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#include "BigOpponentChaosAtoms.h"

#include "control/Navigation.h"
#include "devices/actuators/2019/big/BigActuator.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"
#include "geometry/Point.h"

BigOpponentChaosAtoms::BigOpponentChaosAtoms(MatchData2019 &matchData) :
		MatchAction("oppChaos", Point(600, 2000)), matchData(matchData) {
	// le point de depart est au milieu du terrain parce que c'est la
	// qu'on a le moins de chances de renverser le chaos qu'on essaye
	// de prendre
}

void BigOpponentChaosAtoms::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;

	switch (step) {
		case 0:
			navigation.getControl().setSpeedIndex(SPEED_SMALL_NOMINAL);
			navigation.moveTo(Point(600, 2000));
			break;

		case 1:
			// prise par le c�t� vers la balance
			navigation.rotateToOrientationDeciDegrees(0);
			break;

		case 2:
			// ouverture + prise
			bigActuator.setFlapPickup();
			navigation.moveTo(Point(1100, 2000));
			break;

		case 3:
			navigation.getControl().setSpeedIndex(SPEED_SMALL_SAFE);
			navigation.rotateDeciDegrees(-1050);
			break;

		case 4:
			// d�pose dans la zone verte
			navigation.getControl().setSpeedIndex(SPEED_SMALL_NOMINAL);
			navigation.moveTo(Point(700, 450));
			break;

		case 5:
			matchData.addRediumToArea();
			matchData.addRediumToArea();
			matchData.addGreeniumToGreenArea();
			matchData.addRediumToArea();
			break;

		case STEP_FINALIZATION:
			navigation.moveMM(-300);
			bigActuator.setFlapClosed();
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
