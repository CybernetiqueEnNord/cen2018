/*
 * BigOpponentDispenserPickupAction.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#include "BigOpponentDispenserPickupAction.h"

#include "devices/actuators/2019/big/BigActuator.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"
#include "geometry/Point.h"

BigOpponentDispenserPickupAction::BigOpponentDispenserPickupAction(MatchData2019 &matchData) :
		MatchAction("oppDispenser", Point(1300, 2400)), matchData(matchData) {
}

void BigOpponentDispenserPickupAction::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	PositionControl &control = navigation.getControl();

	switch (step) {
		case 0: {
			control.setSpeedIndex(SPEED_BIG_NOMINAL);
			bigActuator.setLifterFront(4); //full down
			bigActuator.setLifterBack(4); //full down
			int y = control.getPosition().y;
			if (y < 2380 || y > 2420) {
				navigation.moveTo(Point(1300, 2400));
			}
			break;
		}

		case 1:
			navigation.moveTo(Point(1543 - geometry.frontWidth - 10, control.getPosition().y));
			break;

		case 2:
			bigActuator.setPumpFrontTrio(true);
			navigation.repositionMM(500, 1543 - geometry.frontWidth, CURRENT, 0);
			break;

		case 3:
			navigation.waitForDelay(200);
			bigActuator.setLifterFront(2);	// depose center
			navigation.moveMMCircleDecidegree(-471, 1800);
			break;

		case 4:
			bigActuator.setPumpBackTrio(true);
			navigation.repositionMM(-500, 1543 - geometry.rearWidth, CURRENT, 1800);
			break;

		case 5:
			navigation.waitForDelay(200);
			bigActuator.setLifterFront(4);	// full down
			bigActuator.setLifterBack(2);	// depose center
			navigation.moveMM(200);
			break;

		case 6:
			bigActuator.setLifterBack(4);	// full down
			break;

		case STEP_FINALIZATION:
			// nothing to do
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
