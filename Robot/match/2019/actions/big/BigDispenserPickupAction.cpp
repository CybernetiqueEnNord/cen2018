/*
 * BigAtomsDispenserAction.cpp
 *
 *  Created on: 14 mai 2019
 *      Author: Emmanuel
 */

#include "BigDispenserPickupAction.h"

#include "devices/actuators/2019/big/BigActuator.h"
#include "devices/host/HostDevice.h"

BigDispenserPickupAction::BigDispenserPickupAction(MatchData2019 &matchData) :
		MatchAction("dispenser", Point(1300, 600)), matchData(matchData) {
}

void BigDispenserPickupAction::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	PositionControl &control = navigation.getControl();

	switch (step) {
		case 0: {
			control.setSpeedIndex(SPEED_BIG_NOMINAL);
			bigActuator.setLifterFront(4); //full down
			bigActuator.setLifterBack(4); //full down
			int y = control.getPosition().y;
			if (y < 580 || y > 620) {
				navigation.moveTo(Point(1300, 600));
			}
			break;
		}

		case 1:
			navigation.moveTo(Point(1543 - geometry.frontWidth - 10, control.getPosition().y));
			break;

		case 2:
			// prise par l'avant
			bigActuator.setPumpFrontTrio(true);
			navigation.repositionMM(500, 1543 - geometry.frontWidth, CURRENT, 0);
			break;

		case 3:
			// rotation demi-cercle
			navigation.waitForDelay(200);
			bigActuator.setLifterFront(2); //depose center
			navigation.moveMMCircleDecidegree(-471, -1800);
			break;

		case 4:
			// prise par l'arri�re
//			bigActuator.setLifterFront(4); //full down
			bigActuator.setPumpBackTrio(true);
			navigation.repositionMM(-500, 1543 - geometry.rearWidth, CURRENT, 1800);
			break;

		case 5:
			navigation.waitForDelay(200);
			bigActuator.setLifterBack(2); //depose center
			navigation.moveMM(200);
			break;

		case 6:
//			bigActuator.setLifterBack(4); //full down
			navigation.getControl().setSpeedIndex(SPEED_BIG_NOMINAL);	// previously _BIG_SLOW
			break;

		case STEP_FINALIZATION:
			bigActuator.setFlapSustain();
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
