/*
 * BigPrimaryTaskList.cpp
 *
 *  Created on: 14 mai 2019
 *      Author: Emmanuel
 */

#include "BigPrimaryTaskList.h"

#include "../../../../devices/actuators/2019/big/BigActuator.h"
#include "../../../../utils/Arrays.h"
#include "../../../../utils/Pointer.h"
#include "../../../MatchElement.h"
#include "../../../MatchTarget.h"
#include "../../../MatchTask.h"
#include "../../Match2019BigTridentBackAdapter.h"
#include "../../Match2019BigTridentFrontAdapter.h"
#include "../../Match2019TridentStorage.h"
#include "../SingleActionList.h"
#include "BigAcceleratorDropAction.h"
#include "BigDispenserPickupAction.h"
#include "BigOpponentChaosAtoms.h"
#include "BigOpponentDispenserPickupAction.h"
#include "BigStartAllGroundAtomsAction.h"
#include "BigStartDropAction.h"

BigPrimaryTaskList::BigPrimaryTaskList(MatchData2019 &matchData) :
		MatchTaskList(), matchData(matchData), tasks(createTasks()) {
}

unsigned int BigPrimaryTaskList::getCount() const {
	return ARRAY_SIZE(tasks);
}

MatchTask& BigPrimaryTaskList::getTaskAt(unsigned int index) const {
	return tasks[index];
}

MatchTask (&BigPrimaryTaskList::createTasks())[5] {

	extern BigActuator bigActuator;
	// on est sur le gros, il faut d'abord avoir un mod�le de trident
	// de plus il faut garder le meme trident storage pour toutes les actions
	static Match2019BigTridentFrontAdapter bigTridentFrontAdapter(bigActuator);
	static Match2019TridentStorage frontTridentStorage(matchData, bigTridentFrontAdapter);
	static Match2019BigTridentBackAdapter bigTridentBackAdapter(bigActuator);
	static Match2019TridentStorage backTridentStorage(matchData, bigTridentBackAdapter);

	// {{{ BIG START ALL GROUND

	// Atomes au sol dans la zone de d�part
	static MatchElement groundAtomsElement;

	// prise
	static BigStartAllGroundAtomsAction bigStartAllGroundAtomsAction(matchData);
	static SingleActionList bigStartAllGroundAtomsActions(bigStartAllGroundAtomsAction);
	static MatchTarget bigStartAllGroundAtomsTarget1(bigStartAllGroundAtomsActions, groundAtomsElement);
	static MatchTask taskBigStartAllGroundAtoms("ground atoms", 27, 0, Pointer<MatchTarget>(&bigStartAllGroundAtomsTarget1));

	// }}} END BIG START ALL GROUND

	// {{{ BIG DISPENSER

	// Distributeur 6 atomes
	static MatchElement dispenserElement;

	// prise
	static BigDispenserPickupAction bigDispenserPickupAction(matchData);
	static SingleActionList bigDispenserPickupActions(bigDispenserPickupAction);
	static MatchTarget target1(bigDispenserPickupActions, dispenserElement);

	static MatchTask taskDispenser("ourDispenser", 60, 40, Pointer<MatchTarget>(&target1));

	static MatchElement dispenserUnloadElement;
	static bool doFallback = true;
	// d�pose
	static BigAcceleratorDropAction bigAcceleratorDropAction(matchData, doFallback);
	static SingleActionList bigAcceleratorDropActions(bigAcceleratorDropAction);
	static MatchTarget target2(bigAcceleratorDropActions, dispenserUnloadElement);
	// fallback depose
//	static BigStartDropAction bigStartDropAction(matchData, doFallback);
//	static SingleActionList bigStartDropActions(bigStartDropAction);
//	static MatchTarget target3(bigStartDropActions, dispenserUnloadElement);
	// t�che
//	target2.setNext(target3);
	static MatchTask taskUnloadDispenser("unloadDisp", 60, 41, Pointer<MatchTarget>(&target2));

	// }}} END BIG DISPENSER

	// {{{ BIG OPPONENT CHAOS

	static MatchElement opponentChaosElement;

	static BigOpponentChaosAtoms bigOpponentChaosAtoms(matchData);
	static SingleActionList bigOpponentChaosAtomsActions(bigOpponentChaosAtoms);
	static MatchTarget target10(bigOpponentChaosAtomsActions, opponentChaosElement);

	static MatchTask taskOpponentChaos("oppChaos", 22, 10, Pointer<MatchTarget>(&target10));

	// }}} END BIG OPPONENT CHAOS

	// {{{ BIG OPPONENT DISPENSER

	static MatchElement opponentDispenserElement;

	static BigOpponentDispenserPickupAction bigOpponentDispenserPickupAction(matchData);
	static SingleActionList bigOpponentDispenserPickupActions(bigOpponentDispenserPickupAction);
	static MatchTarget targetOpponentDispenserElement(bigOpponentDispenserPickupActions, opponentDispenserElement);

	static bool doit = true;
	static BigStartDropAction bigStartDropAction2(matchData, doit);
	static SingleActionList bigStartDropActions2(bigStartDropAction2);
	static MatchTarget targetStartDrop2(bigStartDropActions2, opponentDispenserElement);
	targetOpponentDispenserElement.setNext(targetStartDrop2);

	static MatchTask taskOpponentDispenser("oppDispenser", 60, 70, Pointer<MatchTarget>(&targetOpponentDispenserElement));

	// }}}

	static MatchTask tasks[5] = {
		taskDispenser,
		taskUnloadDispenser,
		taskBigStartAllGroundAtoms,
		taskOpponentChaos,
		taskOpponentDispenser
	};

	return tasks;
}
