/*
 * BigAcceleratorDropAction.h
 *
 *  Created on: 14 mai 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_ACTIONS_BIG_BIGACCELERATORDROPACTION_H_
#define MATCH_2019_ACTIONS_BIG_BIGACCELERATORDROPACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"

/*
 * D�pose de 6 palets dans l'acc�l�rateur.
 */
class BigAcceleratorDropAction : public MatchAction {
private:
	MatchData2019 &matchData;
	bool &doFallback;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	BigAcceleratorDropAction(MatchData2019 &matchData, bool &doFallback);
};

#endif /* MATCH_2019_ACTIONS_BIG_BIGACCELERATORDROPACTION_H_ */
