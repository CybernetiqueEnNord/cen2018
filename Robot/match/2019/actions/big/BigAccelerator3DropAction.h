/*
 * BigAccelerator3DropAction.h
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_ACTIONS_BIG_BIGACCELERATOR3DROPACTION_H_
#define MATCH_2019_ACTIONS_BIG_BIGACCELERATOR3DROPACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"

/*
 * D�pose de 3 palets dans l'acc�l�rateur.
 */
class BigAccelerator3DropAction : public MatchAction {
private:
	MatchData2019 &matchData;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	BigAccelerator3DropAction(MatchData2019 &matchData);
};

#endif /* MATCH_2019_ACTIONS_BIG_BIGACCELERATOR3DROPACTION_H_ */
