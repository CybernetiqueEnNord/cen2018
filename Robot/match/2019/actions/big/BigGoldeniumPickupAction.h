/*
 * BigGoldeniumPickupAction.h
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_ACTIONS_BIG_BIGGOLDENIUMPICKUPACTION_H_
#define MATCH_2019_ACTIONS_BIG_BIGGOLDENIUMPICKUPACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"

/*
 * Prise du goldenium.
 */
class BigGoldeniumPickupAction : public MatchAction {
private:
	MatchData2019 &matchData;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	BigGoldeniumPickupAction(MatchData2019 &matchData);
};

#endif /* MATCH_2019_ACTIONS_BIG_BIGGOLDENIUMPICKUPACTION_H_ */
