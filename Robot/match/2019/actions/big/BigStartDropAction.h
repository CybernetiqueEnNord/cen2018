/*
 * BigStartDropAction.h
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_ACTIONS_BIG_BIGSTARTDROPACTION_H_
#define MATCH_2019_ACTIONS_BIG_BIGSTARTDROPACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"

/*
 * D�pose dans la zone de d�part.
 */
class BigStartDropAction : public MatchAction {
private:
	MatchData2019 &matchData;
	bool &doFallback;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	BigStartDropAction(MatchData2019 &matchData, bool &nominalFailed);
};

#endif /* MATCH_2019_ACTIONS_BIG_BIGSTARTDROPACTION_H_ */
