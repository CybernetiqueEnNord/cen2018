/*
 * BigAllGroundAtoms.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#include "BigStartAllGroundAtomsAction.h"

#include "devices/actuators/2019/big/BigActuator.h"

BigStartAllGroundAtomsAction::BigStartAllGroundAtomsAction(MatchData2019 &matchData) :
		MatchAction("allGround", Point(820, 225)), matchData(matchData) {
}

void BigStartAllGroundAtomsAction::executeStep(Navigation &navigation, unsigned int step) {
	PositionControl control = navigation.getControl();
	extern BigActuator bigActuator;

	switch (step) {
		case 0:
			control.setSpeedIndex(SPEED_BIG_FAST);
			navigation.moveTo(Point(820, 225));
			break;

		case 1: {
			// prise 1er palet
			bigActuator.setFlapPickup();
			class Condition: public NavigationConditionListener {
			protected:
				virtual int onCheckCondition(const Navigation& navigation) {
					Position current = navigation.getControl().getPosition();
					if (current.y >= 400) {
						return 1;
					} else {
						return 0;
					}
				}

				virtual bool onConditionMet(const Navigation& navigation, int action) {
					(void) navigation;
					extern BigActuator bigActuator;
					switch (action) {
						case 1:
							bigActuator.setFlapClosed();
							return true;
					}
					return false;
				}

			public:
				Condition() :
						NavigationConditionListener(true) {
				}
			};

			Condition condition;
			navigation.asynchronous = true;
			navigation.moveTo(Point(1350, 800));
			navigation.waitForCondition(condition);
			navigation.asynchronous = false;
			break;
		}

		case 2:
			// contournement tas
			navigation.moveTo(Point(1350, 1300));
			break;

		case 3:
			// prise tas
			bigActuator.setFlapPickup();
			control.setSpeedIndex(SPEED_BIG_NOMINAL);
			navigation.moveTo(Point(500, 400));
			matchData.addRediumToRedArea();
			matchData.addRediumToRedArea();
			matchData.addRediumToRedArea();
			matchData.addGreeniumToGreenArea();
			matchData.addRediumToArea();
			matchData.addGreeniumToArea();
			matchData.addBlueiumToArea();
			break;

		case 4:
			navigation.rotateToOrientationDeciDegrees(-900);
			break;

		case 5:
			navigation.waitForDelay(3000);
			break;

		case STEP_FINALIZATION:
			navigation.moveMM(-300);
			bigActuator.setFlapClosed();
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
