/*
 * BigWeighingTrayBlueDropAction.h
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_ACTIONS_BIG_BIGWEIGHINGTRAYBLUEDROPACTION_H_
#define MATCH_2019_ACTIONS_BIG_BIGWEIGHINGTRAYBLUEDROPACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"

/*
 * D�pose d'un blueium dans la balance.
 */
class BigWeighingTrayBlueDropAction : public MatchAction {
private:
	MatchData2019 &matchData;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	BigWeighingTrayBlueDropAction(MatchData2019 &matchData);
};

#endif /* MATCH_2019_ACTIONS_BIG_BIGWEIGHINGTRAYBLUEDROPACTION_H_ */
