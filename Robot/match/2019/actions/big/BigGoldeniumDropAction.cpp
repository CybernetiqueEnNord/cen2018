/*
 * BigGoldeniumDropAction.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#include "BigGoldeniumDropAction.h"

BigGoldeniumDropAction::BigGoldeniumDropAction(MatchData2019 &matchData) :
		MatchAction("golddrop", Point(1000, 1500)), matchData(matchData) {
}

void BigGoldeniumDropAction::executeStep(Navigation &navigation, unsigned int step) {
	// nothing to do
}
