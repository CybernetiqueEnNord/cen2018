/*
 * BigOpponentDispenserPickupAction.h
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_ACTIONS_BIG_BIGOPPONENTDISPENSERPICKUPACTION_H_
#define MATCH_2019_ACTIONS_BIG_BIGOPPONENTDISPENSERPICKUPACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"

/*
 * Prise des atomes dans le distributeur adverse.
 */
class BigOpponentDispenserPickupAction : public MatchAction {
private:
	MatchData2019 &matchData;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	BigOpponentDispenserPickupAction(MatchData2019 &matchData);
};

#endif /* MATCH_2019_ACTIONS_BIG_BIGOPPONENTDISPENSERPICKUPACTION_H_ */
