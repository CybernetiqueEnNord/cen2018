/*
 * BigAtomsDispenserAction.h
 *
 *  Created on: 14 mai 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_ACTIONS_BIG_BIGDISPENSERPICKUPACTION_H_
#define MATCH_2019_ACTIONS_BIG_BIGDISPENSERPICKUPACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"

/*
 * Prise de 6 palets dans le distributeur.
 */
class BigDispenserPickupAction : public MatchAction {
private:
	MatchData2019 &matchData;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	BigDispenserPickupAction(MatchData2019 &matchData);
};

#endif /* MATCH_2019_ACTIONS_BIG_BIGDISPENSERPICKUPACTION_H_ */
