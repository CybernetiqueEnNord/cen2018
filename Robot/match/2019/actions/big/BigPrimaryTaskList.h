/*
 * BigPrimaryTaskList.h
 *
 *  Created on: 14 mai 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_ACTIONS_BIG_BIGPRIMARYTASKLIST_H_
#define MATCH_2019_ACTIONS_BIG_BIGPRIMARYTASKLIST_H_

#include "match/MatchTaskList.h"

#include "match/2019/MatchData2019.h"

class BigPrimaryTaskList : public MatchTaskList {
private:
	MatchData2019 &matchData;
	MatchTask (&tasks)[5];

	MatchTask (&createTasks())[5];

public:
	BigPrimaryTaskList(MatchData2019 &matchData);
	virtual unsigned int getCount() const;
	virtual MatchTask& getTaskAt(unsigned int index) const;
};

#endif /* MATCH_2019_ACTIONS_BIG_BIGPRIMARYTASKLIST_H_ */
