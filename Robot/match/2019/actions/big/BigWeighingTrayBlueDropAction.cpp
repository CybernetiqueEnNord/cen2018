/*
 * BigWeighingTrayBlueDropAction.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#include "BigWeighingTrayBlueDropAction.h"

BigWeighingTrayBlueDropAction::BigWeighingTrayBlueDropAction(MatchData2019 &matchData) :
		MatchAction("dropBlue", Point(1000, 1500)), matchData(matchData) {
}

void BigWeighingTrayBlueDropAction::executeStep(Navigation &navigation, unsigned int step) {
	// nothing to do
}
