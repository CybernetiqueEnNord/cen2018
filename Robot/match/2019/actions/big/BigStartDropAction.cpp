/*
 * BigStartDropAction.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#include "BigStartDropAction.h"

#include "../../../../control/Navigation.h"
#include "../../../../devices/actuators/2019/big/BigActuator.h"
#include "../../../../devices/io/LCDScreen.h"
#include "../../../../geometry/Point.h"

BigStartDropAction::BigStartDropAction(MatchData2019 &matchData, bool &doFallback) :
		MatchAction("dropAtStart", Point(700, 500)), matchData(matchData), doFallback(doFallback) {
}

void BigStartDropAction::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;
	extern LCDScreen screen;

	if(!doFallback)
	{
		MatchAction::executeStep(navigation, step);
		return;
	}

	switch (step) {
		case 0:
			navigation.moveTo(Point(700, 500));
			break;

		case 1:
			bigActuator.setPumpFrontTrio(false);
			navigation.waitForDelay(1000);
			navigation.rotateToOrientationDeciDegrees(900);
			break;

		case 2:
			bigActuator.setPumpBackTrio(false);
			navigation.waitForDelay(1000);
			navigation.moveMM(300);
			break;

		case 3:
			navigation.rotateToOrientationDeciDegrees(-900);
			break;

		case 4:
			bigActuator.setFlapPickup();
			navigation.moveTo(Point(700, 450));
			break;

		case STEP_FINALIZATION:
			navigation.moveMM(-300);
			bigActuator.setFlapClosed();
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
