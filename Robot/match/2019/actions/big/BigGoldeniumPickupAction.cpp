/*
 * BigGoldeniumPickupAction.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#include "BigGoldeniumPickupAction.h"

BigGoldeniumPickupAction::BigGoldeniumPickupAction(MatchData2019 &matchData) :
		MatchAction("goldPickup", Point(1000, 1500)), matchData(matchData) {
}

void BigGoldeniumPickupAction::executeStep(Navigation &navigation, unsigned int step) {
	// nothing to do
}
