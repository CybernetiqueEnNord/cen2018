/*
 * BigAcceleratorDropAction.cpp
 *
 *  Created on: 14 mai 2019
 *      Author: Emmanuel
 */

#include "BigAcceleratorDropAction.h"

#include "../../../../control/Navigation.h"
#include "../../../../control/PositionControl.h"
#include "../../../../devices/actuators/2019/big/BigActuator.h"
#include "../../../../devices/host/HostData.h"
#include "../../../../devices/host/HostDevice.h"
#include "../../../../devices/host/RobotGeometry.h"
#include "../../../../devices/io/LCDScreen.h"
#include "../../../../odometry/Position.h"

BigAcceleratorDropAction::BigAcceleratorDropAction(MatchData2019 &matchData, bool &doFallback) :
		MatchAction("accelerator", Point(500, 2000)), matchData(matchData), doFallback(doFallback) {
}

void BigAcceleratorDropAction::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();

	switch (step) {
		case 0:
			bigActuator.setLifterFront(0); //full up
			bigActuator.setLifterBack(0); //full up
			bigActuator.setFlapSustain();
			break;

		case 1:
			navigation.rotateToOrientationDeciDegrees(1800);
			break;

		case 2:
			navigation.getControl().setSpeedIndex(SPEED_BIG_SLOW);
			// 130mm = longueur des pinces a la perpendiculaire
			navigation.moveTo(Point(geometry.rearWidth + 35 + 10 + 140, 2000));
			break;

		case 3:
			bigActuator.setFlapPickup();
			navigation.waitForDelay(400);
			bigActuator.setFlapSustain();
			navigation.waitForDelay(400);
			break;

		case 4:
			navigation.rotateToOrientationDeciDegrees(1800 - 600);
			break;

		case 5:
			navigation.rotateToOrientationDeciDegrees(1800 + 600);
			break;

		case 6:
			bigActuator.setFlapClosed();
			navigation.rotateToOrientationDeciDegrees(1800);
			break;

		case 7:
			navigation.getControl().setSpeedIndex(SPEED_BIG_SLOW);
			navigation.moveTo(Point(geometry.rearWidth + 35 + 10, 2000));
			break;

		case 8:
			navigation.repositionMM(500, geometry.rearWidth, CURRENT, 1800);
			break;

		case 9:
			bigActuator.setLifterFront(2); //Depose down
			navigation.waitForDelay(400);
			navigation.moveMM(-18);
			break;

		case 10:
			bigActuator.setPumpFrontTrio(false); //Depose all
			navigation.waitForDelay(1000);
			matchData.addToAccelerateur();
			matchData.addToAccelerateur();
			matchData.addToAccelerateur();
			navigation.moveMM(-200);
			break;

		case 11:
			// 130mm = longueur des pinces a la perpendiculaire
			navigation.moveToBackwards(Point(geometry.rearWidth + 35 + 10 + 140, 2000));
			break;

		case 12:
			bigActuator.setFlapClosed();
			navigation.rotateToOrientationDeciDegrees(0);
			break;

		case 13:
			navigation.repositionMM(-500, geometry.rearWidth, CURRENT, 0);
			break;

		case 14:
			bigActuator.setLifterBack(2); //Depose down
			navigation.waitForDelay(400);
			navigation.moveMM(18);
			break;

		case 15:
			bigActuator.setPumpBackTrio(false); //Depose all
			navigation.waitForDelay(1000);
			matchData.addToAccelerateur();
			matchData.addToAccelerateur();
			matchData.addToAccelerateur();
			navigation.moveMM(200);
			break;

		case 16:
			bigActuator.setLifterBack(4); //down
			bigActuator.setLifterFront(4); //down
			doFallback = false;
			break;

		case STEP_FINALIZATION:
			// nothing to do
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
