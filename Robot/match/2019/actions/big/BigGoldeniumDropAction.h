/*
 * BigGoldeniumDropAction.h
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#ifndef MATCH_2019_ACTIONS_BIG_BIGGOLDENIUMDROPACTION_H_
#define MATCH_2019_ACTIONS_BIG_BIGGOLDENIUMDROPACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"

/*
 * D�pose du goldenium.
 */
class BigGoldeniumDropAction : public MatchAction {
private:
	MatchData2019 &matchData;

protected:
	virtual void executeStep(Navigation &navigation, unsigned int step);

public:
	BigGoldeniumDropAction(MatchData2019 &matchData);
};

#endif /* MATCH_2019_ACTIONS_BIG_BIGGOLDENIUMDROPACTION_H_ */
