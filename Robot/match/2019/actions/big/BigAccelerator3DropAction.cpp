/*
 * BigAccelerator3DropAction.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#include "BigAccelerator3DropAction.h"

#include "control/Navigation.h"
#include "devices/actuators/2019/big/BigActuator.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"
#include "devices/host/RobotGeometry.h"
#include "odometry/Position.h"

BigAccelerator3DropAction::BigAccelerator3DropAction(MatchData2019 &matchData) :
		MatchAction("accel3drop", Point(1000, 1500)), matchData(matchData) {
}

void BigAccelerator3DropAction::executeStep(Navigation &navigation, unsigned int step) {
	extern BigActuator bigActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();

	switch (step) {
		case 0:
			navigation.moveTo(Point(300, 2000));
			break;

		case 1:
			navigation.moveTo(Point(geometry.rearWidth + 35 + 10, 2000));
			break;

		case 2:
			navigation.repositionMM(500, geometry.rearWidth, CURRENT, 1800);
			break;

		case 3:
			bigActuator.setLifterFront(2); //Depose down
			navigation.waitForDelay(400);
			navigation.moveMM(-18);
			break;

		case 4:
			bigActuator.setPumpFrontTrio(false); //Depose all
			navigation.waitForDelay(2000);
			matchData.addToAccelerateur();
			matchData.addToAccelerateur();
			matchData.addToAccelerateur();
			navigation.moveMM(-200);
			break;

		case STEP_FINALIZATION:
			// nothing to do
			break;
	}
}
