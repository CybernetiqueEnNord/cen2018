/*
 * BigSideDispenserPickupAction.cpp
 *
 *  Created on: 21 mai 2019
 *      Author: Emmanuel
 */

#include "BigSideDispenserPickupAction.h"

#include "control/Navigation.h"
#include "devices/actuators/2019/big/BigActuator.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"
#include "devices/host/RobotGeometry.h"
#include "odometry/Position.h"

BigSideDispenserPickupAction::BigSideDispenserPickupAction(MatchData2019 &matchData) :
		MatchAction("sideRackPickup", Point(800, 225)), matchData(matchData) {
}

void BigSideDispenserPickupAction::executeStep(Navigation &navigation, unsigned int step) {
//	extern BigActuator bigActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
//	PositionControl &control = navigation.getControl();

	switch (step) {
		case 0: {
			// recalage en face de la pente
			navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
			break;
		}

		case 1: {
			// recalage lat�ral
			navigation.moveMM(225 - geometry.rearWidth);
			navigation.rotateDeciDegrees(900);
			navigation.repositionMM(-300, 2000 - geometry.rearWidth, CURRENT, 1800);
			navigation.moveMM(150);
			break;
		}

		case 2:
			navigation.moveTo(Point(800, 225));
			break;

		case STEP_FINALIZATION: {
			break;
		}

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
