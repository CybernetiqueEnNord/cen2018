/*
 * GoldeniumAndDispenserAction.h
 *
 *  Created on: 8 mai 2019
 *      Author: Emmanuel
 *      @deprecated on a pas les moyens de faire les 2 en mm temps
 */

#ifndef MATCH_2019_ACTIONS_GOLDENIUMANDDISPENSERACTION_H_
#define MATCH_2019_ACTIONS_GOLDENIUMANDDISPENSERACTION_H_

#include "match/MatchAction.h"
#include "match/2019/MatchData2019.h"
#include "match/2019/actions/GoldeniumAction.h"
#include "match/2019/actions/SmallAtomsDispenserAction.h"

class GoldeniumAndDispenserAction : public MatchAction {
private:
	GoldeniumAction goldeniumAction;
	SmallAtomsDispenserAction dispenserAction;
	unsigned int stepsOffset = 0;

	bool executeActionStep(MatchAction &action, Navigation &navigation, unsigned int step);

public:
	GoldeniumAndDispenserAction(MatchData2019 &matchData);
	virtual void executeStep(Navigation &navigation, unsigned int step);
};

#endif /* MATCH_2019_ACTIONS_GOLDENIUMANDDISPENSERACTION_H_ */
