/*
 * BlueiumAction.cpp
 *
 *  Created on: 2 mars 2019
 *      Author: emmanuel
 */

#include "BlueiumAction.h"

#include "../../../devices/actuators/2019/small/SmallActuator.h"
#include "devices/host/HostDevice.h"

BlueiumAction::BlueiumAction(MatchData2019 &matchData) :
		MatchAction("blueium", Point(300, 1600)), matchData(matchData) {
}

void BlueiumAction::executeStep(Navigation &navigation, unsigned int step) {
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	switch (step) {
		case 0:
			navigation.moveTo(Point(200, 1600));
			matchData.runExperience();
			matchData.electronSuccessful();
			break;

		case 1:
			navigation.rotateToOrientationDeciDegrees(0);
			navigation.moveMM(-150 + geometry.rearWidth); //Se coller, moins l'epaisseur du robot
			navigation.repositionMM(-200, geometry.rearWidth, CURRENT, 0);
			navigation.moveMM(155 - geometry.rearWidth); //Se d�coller
			navigation.rotateToOrientationDeciDegrees(900);
			break;

		case 2:
			extern SmallActuator smallActuator;
			smallActuator.setArm(true);
			navigation.waitForDelay(500);
			navigation.moveMM(120);
			matchData.addToAccelerateur();
			matchData.unlockGoldenium();
			break;

		case STEP_FINALIZATION:
			smallActuator.setArm(false);
			navigation.waitForDelay(200); //Laisser le temps au bras de remonter pour ne pas l'accrocher.
			break;

		default:
			MatchAction::executeStep(navigation, step);
			break;
	}
}
