/*
 * MatchCalibration.cpp
 *
 *  Created on: 13 avr. 2019
 *      Author: Emmanuel
 */

#include "MatchCalibration2019.h"

#include "match/2019/Match2019.h"

#include "devices/actuators/2019/big/BigActuator.h"
#include "devices/actuators/2019/small/SmallActuator.h"
#include "devices/beacon/Beacon2018.h"
#include "devices/host/HostData.h"
#include "devices/host/HostDevice.h"
#include "devices/host/HostCapabilities2019.h"
#include "devices/io/LCDScreen.h"
#include "match/2019/Match2019StrategieIndexCalibration.h"

#include "match/2019/actions/small/SmallDeliverBlueiumInBalance.h"
#include "match/2019/actions/small/SmallDeliverGoldeniumAction.h"
#include "match/2019/actions/small/SmallDeliverTrioInTable.h"
#include "match/2019/actions/small/SmallRollBlueiumAction.h"

#include "match/MatchData.h"
#include "math/cenMath.h"
#include "utils/Text.h"

MatchCalibration2019::MatchCalibration2019(Navigation &navigation) :
		navigation(navigation) {
}

void MatchCalibration2019::prepareCalibrationSmall(int trajectoryIndex) {
	switch (trajectoryIndex) {
		case STRATEGIE_INDEX_CALIBRATION_NOMINAL:
			prepareSmallNewMiseEnPlace();
			break;

		case STRATEGIE_INDEX_CALIBRATION_LINEAR:
			prepareLinearTest();
			break;

		case STRATEGIE_INDEX_CALIBRATION_CALIBRATION_LINEAR:
			prepareLinearCalibration();
			break;

		case STRATEGIE_INDEX_CALIBRATION_CALIBRATION_ANGULAR:
			prepareAngularCalibration();
			break;

		default:
			prepareSmallNewMiseEnPlace();
			break;
	}
}

void MatchCalibration2019::executeCalibrationSmall(int trajectoryIndex) {
	switch (trajectoryIndex) {
		case STRATEGIE_INDEX_CALIBRATION_NOMINAL:
			executeSmallNewDepart();
			break;

		case STRATEGIE_INDEX_CALIBRATION_LINEAR:
			executeLinearTest();
			break;

		case STRATEGIE_INDEX_CALIBRATION_CALIBRATION_LINEAR:
			executeLinearCalibration();
			break;

		case STRATEGIE_INDEX_CALIBRATION_CALIBRATION_ANGULAR:
			executeAngularCalibration();
			break;

	}
}

void MatchCalibration2019::prepareCalibrationBig(int trajectoryIndex) {
	switch (trajectoryIndex) {
		case STRATEGIE_INDEX_CALIBRATION_NOMINAL:
			prepareBigNewMiseEnPlace();
			break;

		case STRATEGIE_INDEX_CALIBRATION_LINEAR:
			prepareLinearTest();
			break;

		case STRATEGIE_INDEX_CALIBRATION_CALIBRATION_LINEAR:
			prepareLinearCalibration();
			break;

		case STRATEGIE_INDEX_CALIBRATION_CALIBRATION_ANGULAR:
			prepareAngularCalibration();
			break;

		default:
			prepareBigNewMiseEnPlace();
			break;

	}
}

void MatchCalibration2019::executeCalibrationBig(int trajectoryIndex) {
	switch (trajectoryIndex) {
		case STRATEGIE_INDEX_CALIBRATION_NOMINAL:
			executeBigNewGetFirstsPalets();
			break;

		case STRATEGIE_INDEX_CALIBRATION_LINEAR:
			executeLinearTest();
			break;

		case STRATEGIE_INDEX_CALIBRATION_CALIBRATION_LINEAR:
			executeLinearCalibration();
			break;

		case STRATEGIE_INDEX_CALIBRATION_CALIBRATION_ANGULAR:
			executeAngularCalibration();
			break;
	}
}

void MatchCalibration2019::prepareSmallNewMiseEnPlace() {
	extern SmallActuator smallActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	PositionControl &control = navigation.getControl();

	control.setSpeedIndex(SPEED_SMALL_INITIALISATION);

	//Wait for the tirette to be correctly inserted.
	smallActuator.setPumpGoldenium(true);
	smallActuator.setFlapDropGoldenium();
	navigation.waitForDelay(500);
	smallActuator.setPumpGoldenium(false);
	smallActuator.setFlapPickup();

	bool side = control.isReversed();
	smallActuator.setMatchSide(side);

	navigation.repositionMM(-200, geometry.rearWidth, 0, 0);
	smallActuator.setFlapClosed();
	smallActuator.setArm(true);

	navigation.moveMM(450 - geometry.rearWidth);
	smallActuator.setArm(false);
	smallActuator.setPumpTrio(true);
	navigation.rotateDeciDegrees(900);
	smallActuator.setPumpTrio(false);

	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	navigation.moveMM(300 - geometry.rearWidth);
	navigation.rotateDeciDegrees(900);

	// Points de pr�sence de l'exp�rience
//	getMatchData().setupExperience();
}

void MatchCalibration2019::executeSmallNewDepart() {
	PositionControl &control = navigation.getControl();

	control.obstacleAvoidance = true;

	control.setSpeedIndex(SPEED_SMALL_FAST_STARTING_ROTATION);
	navigation.moveMMCircleDecidegree(377, -900);

	extern Beacon2018 beacon;
	beacon.setThreshold(800);
	control.setSpeedIndex(SPEED_SMALL_MAX_FAST_LINEAR);
	//control.setSpeedIndex(SPEED_SMALL_MAX_STARTING_FAST_LINEAR); // A little slower temporary to test actionneur

	//navigation.moveTo(Point(230, 2225));

	class Condition: public NavigationConditionListener {
	protected:
		virtual int onCheckCondition(const Navigation& navigation) {
			Position current = navigation.getControl().getPosition();
			if (current.y >= 1750) {
				return 2;
			} else if (current.y >= 1200) {
				return 1;
			} else {
				return 0;
			}
		}

		virtual bool onConditionMet(const Navigation& navigation, int action) {
			(void) navigation;
			extern SmallActuator smallActuator;
			switch (action) {
				case 1:
					smallActuator.setArm(true);
					return true;

				case 2:
					smallActuator.setArm(false);
					setDone();
					return true;
			}
			return false;
		}

	public:
		Condition() :
				NavigationConditionListener(true) {
		}
	};

	Point dest(230, 2225);
	Condition condition;
	navigation.asynchronous = true;
	navigation.moveTo(dest);
	navigation.waitForCondition(condition);
	navigation.asynchronous = false;

//	extern SmallActuator smallActuator;
//	navigation.asynchronous = true;
//
//	navigation.moveTo(Point(230, 2225));
//	//navigation.executeNextMove();
//
//	while (control.getMoveState() != MoveState::StraightAcceleration) {
//		chThdSleepMilliseconds(5);
//	}
//	while (control.getMoveState() == MoveState::StraightAcceleration || control.getMoveState() == MoveState::StraightConstant || control.getMoveState() == MoveState::StraightDeceleration || control.getMoveState() == MoveState::EmergencyBrake) {
//		if (control.getPosition().y >= 1200) {
//			smallActuator.setArm(true);
//			break;
//		}
//		chThdSleepMilliseconds(5);
//	}
//	while (control.getMoveState() == MoveState::StraightAcceleration || control.getMoveState() == MoveState::StraightConstant || control.getMoveState() == MoveState::StraightDeceleration || control.getMoveState() == MoveState::EmergencyBrake) {
//		if (control.getPosition().y >= 1750) {
//			smallActuator.setArm(false);
//			break;
//		}
//		chThdSleepMilliseconds(5);
//	}
//	navigation.asynchronous = false;
//	navigation.waitForMove();

	extern Beacon2018 beacon;
	beacon.setThreshold(600);
	control.setSpeedIndex(SPEED_SMALL_NOMINAL);

	navigation.rotateToOrientationDeciDegrees(1800);

	//smallActuator.setArm(false);
}

void MatchCalibration2019::prepareBigNewMiseEnPlace() {
	extern BigActuator bigActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	PositionControl &control = navigation.getControl();

	control.setSpeedIndex(SPEED_BIG_INITIALISATION);

	//Wait for the tirette to be correctly inserted.
	navigation.waitForDelay(500);

	bool side = control.isReversed();
	bigActuator.setMatchSide(side);

	navigation.repositionMM(-200, 2000 - geometry.rearWidth, 0, 1800);

	bigActuator.setLifterBack(0);	//full up
	//Go to the center of the terrain:
	navigation.moveMM(1000 - geometry.rearWidth);
	//Recallage en y dans la zone bleu
	navigation.rotateDeciDegrees(-900);
	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	//Se centrer dans la zone bleu
	navigation.moveTo(Point(control.getPosition().x, 225));
	//rentrer dans la zone verte
	navigation.moveToBackwards(Point(750, 225));

	control.setSpeedIndex(SPEED_BIG_NOMINAL);
}

void MatchCalibration2019::executeBigNewGetFirstsPalets() {
	extern BigActuator bigActuator;
	PositionControl &control = navigation.getControl();

	control.obstacleAvoidance = true;

	control.setSpeedIndex(SPEED_BIG_FAST);

	//Target = 1350 Actual=750 => r=600 => l= 2*Pi*r /4 =
	navigation.moveMMCircleDecidegree(942, 900);

	control.setSpeedIndex(SPEED_BIG_FAST);

	navigation.moveTo(Point(1350, 1300));

	navigation.rotateToOrientationDeciDegrees(1800 + 450);

	bigActuator.setFlapPickup();

	control.setSpeedIndex(SPEED_SMALL_NOMINAL);
	navigation.moveTo(Point(500, 400));
	navigation.moveMM(-200);
}

void MatchCalibration2019::prepareAngularCalibration() {
}

void MatchCalibration2019::prepareLinearCalibration() {
}

void MatchCalibration2019::prepareLinearTest() {
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();

	PositionControl &control = navigation.getControl();
	control.setSpeedIndex(0);

	navigation.waitForDelay(1000);
	navigation.repositionMM(-200, 1000, geometry.rearWidth, 900);
//	navigation.moveMM(370);
//	navigation.rotateDeciDegrees(900);
//	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
//	navigation.moveMM(175);
}

void MatchCalibration2019::executeAngularCalibration() {
	int turnNumber = 10;

	const HostData &hostData = HostDevice::getHostData();
	if ((hostData.getCapabilities() & CAPABILITY_SMALL) != 0) {
		extern SmallActuator smallActuator;
		smallActuator.setFlapClosed();
	}

	PositionControl &control = navigation.getControl();
	control.setSpeedIndex(1);
	control.setErrorRatioLeftVsRight(0);
	navigation.repositionMM(-200, 0, 0, 0);
	navigation.moveMM(130);
	navigation.rotateDeciDegrees(3600 * turnNumber);
	Position p = navigation.repositionMM(-200, 0, 0, 0);
	float ratio = 1 + (p.angle / M_2PI) / turnNumber;
	int pas = control.stepsByTurn / ratio;
	extern LCDScreen screen;
	char buffer[10];
	itoa((int) (ANGLE_TO_DEGREES(p.angle) * 100), buffer, 10);
	//Display error:
	screen.displayMessage(buffer);
	navigation.waitForDelay(10000);
	itoa(pas, buffer, 10);
	//Display new value to set for StepBeTurn
	screen.displayMessage(buffer);
	navigation.waitForDelay(10000);
}

void MatchCalibration2019::executeLinearCalibration() {
	int longueurCalleMM = 1002;
	int marge = 100;

	extern LCDScreen screen;
	char buffer[10];
	PositionControl &control = navigation.getControl();

	control.setSpeedIndex(1);
	navigation.repositionMM(-200, 0, 0, 0);
	// avance de la longueur de la calle plus une marge
	navigation.moveMM(longueurCalleMM + marge);
	Position p = control.getPosition();

	navigation.waitForDelay(3000);
	// recalage sur la cale
	p = navigation.repositionMM(-4 * marge, 0, 0, 0);
	// calcul du ratio de la longueur de la cale mesur�e par rapport � la longueur connue
	float ratio = 1 + (p.x - marge) / longueurCalleMM;
	// calcul du nouveau ratio pas par m�tres
	int newStepsByMeter = control.stepsByMeter / ratio;
	// affichage de la longueur mesur�e
	itoa((int) ((p.x - marge) * 10), buffer, 10);
	screen.displayMessage(buffer);
	navigation.waitForDelay(10000);
	// affichage du nouveau nombre de pas par m�tres
	itoa(newStepsByMeter, buffer, 10);
	screen.displayMessage(buffer);
	navigation.waitForDelay(10000);
}

void MatchCalibration2019::executeLinearTest() {
	navigation.waitForDelay(500);

	extern Beacon2018 beacon;
	beacon.setThreshold(800);
	beacon.setBeepEnabled(true);

	navigation.getControl().obstacleAvoidance = true;

	PositionControl &control = navigation.getControl();
	control.setSpeedIndex(SPEED_SMALL_TEST);
	navigation.moveMM(2600);
}

void MatchCalibration2019::displayTrajectory(int trajectoryIndex) {
	extern LCDScreen screen;

	DeviceId id = HostDevice::getDeviceId();
	if (id == HostData_F446RE_Small::getDeviceId() || id == HostData_F446RE_NextGen_Small::getDeviceId()) {
		//Case small
		switch (trajectoryIndex) {
			case STRATEGIE_INDEX_CALIBRATION_NOMINAL:
				screen.displayMessage(STRATEGIE_INDEX_CALIBRATION_NOMINAL_TEXT, 8);
				break;
			case STRATEGIE_INDEX_CALIBRATION_LINEAR:
				screen.displayMessage(STRATEGIE_INDEX_CALIBRATION_LINEAR_TEXT, 8);
				break;
			case STRATEGIE_INDEX_CALIBRATION_CALIBRATION_LINEAR:
				screen.displayMessage(STRATEGIE_INDEX_CALIBRATION_CALIBRATION_LINEAR_TEXT, 8);
				break;
			case STRATEGIE_INDEX_CALIBRATION_CALIBRATION_ANGULAR:
				screen.displayMessage(STRATEGIE_INDEX_CALIBRATION_CALIBRATION_ANGULAR_TEXT, 8);
				break;

			default:
				char buffer[9];
				itoa(trajectoryIndex, buffer, 10);
				screen.displayMessage(buffer, 8);
		}
	} else if (id == HostData_F446RE_Big::getDeviceId() || id == HostData_F446RE_NextGen_Big::getDeviceId()) {
		//Case small
		switch (trajectoryIndex) {
			case STRATEGIE_INDEX_CALIBRATION_NOMINAL:
				screen.displayMessage(STRATEGIE_INDEX_CALIBRATION_NOMINAL_TEXT, 8);
				break;
			case STRATEGIE_INDEX_CALIBRATION_LINEAR:
				screen.displayMessage(STRATEGIE_INDEX_CALIBRATION_LINEAR_TEXT, 8);
				break;
			case STRATEGIE_INDEX_CALIBRATION_CALIBRATION_LINEAR:
				screen.displayMessage(STRATEGIE_INDEX_CALIBRATION_CALIBRATION_LINEAR_TEXT, 8);
				break;
			case STRATEGIE_INDEX_CALIBRATION_CALIBRATION_ANGULAR:
				screen.displayMessage(STRATEGIE_INDEX_CALIBRATION_CALIBRATION_ANGULAR_TEXT, 8);
				break;

			default:
				char buffer[9];
				itoa(trajectoryIndex, buffer, 10);
				screen.displayMessage(buffer, 8);
		}
	}

}
