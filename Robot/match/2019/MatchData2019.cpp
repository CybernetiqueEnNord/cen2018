/*
 * MatchData2019.cpp
 *
 *  Created on: 11 oct. 2018
 *      Author: Anastaszor
 */

#include "MatchData2019.h"

MatchData2019::MatchData2019(const MatchTimer& timer) : MatchData(timer) {
	for (int i = 0; i < 6; i++) {
		balance[i] = Match2019Atom::NOT_AN_ATOM;
	}
}

void MatchData2019::addGoldeniumToBalance() {
	for (int i = 0; i < 6; i++) {
		// adds a goldenium in an empty space
		if (balance[i] == Match2019Atom::NOT_AN_ATOM) {
			balance[i] = Match2019Atom::GOLDENIUM;
			addScore(24);
			return;
		}
	}
	// all the slots are full, ignore the goldenium
}

void MatchData2019::addBlueiumToBalance() {
	for (int i = 0; i < 6; i++) {
		// adds a blueium in an empty space
		if (balance[i] == Match2019Atom::NOT_AN_ATOM) {
			balance[i] = Match2019Atom::BLUEIUM;
			addScore(12);
			return;
		}
	}
	for (int i = 0; i < 6; i++) {
		// adds a blueium instead of a goldenium
		if (balance[i] == Match2019Atom::GOLDENIUM) {
			balance[i] = Match2019Atom::BLUEIUM;
			addScore(-12);
			return;
		}
	}
	// all the atoms in the balance are blueium or lower
}

void MatchData2019::addGreeniumToBalance() {
	for (int i = 0; i < 6; i++) {
		// adds a greenium in an empty space
		if (balance[i] == Match2019Atom::NOT_AN_ATOM) {
			balance[i] = Match2019Atom::GREENIUM;
			addScore(8);
			return;
		}
	}
	for (int i = 0; i < 6; i++) {
		// adds a greenium instead of a goldenium
		if (balance[i] == Match2019Atom::GOLDENIUM) {
			balance[i] = Match2019Atom::GREENIUM;
			addScore(-16);
			return;
		}
	}
	for (int i = 0; i < 6; i++) {
		// adds a greenium instead of a blueium
		if (balance[i] == Match2019Atom::BLUEIUM) {
			balance[i] = Match2019Atom::GREENIUM;
			addScore(-4);
			return;
		}
	}
	// all slots are greenium or redium
}

void MatchData2019::addRediumToBalance() {
	for (int i = 0; i < 6; i++) {
		// adds a redium in an empty space
		if (balance[i] == Match2019Atom::NOT_AN_ATOM) {
			balance[i] = Match2019Atom::REDIUM;
			addScore(4);
			return;
		}
	}
	for (int i = 0; i < 6; i++) {
		// adds a redium instead of a goldenium
		if (balance[i] == Match2019Atom::GOLDENIUM) {
			balance[i] = Match2019Atom::REDIUM;
			addScore(-20);
			return;
		}
	}
	for (int i = 0; i < 6; i++) {
		// adds a redium instead of a blueium
		if (balance[i] == Match2019Atom::BLUEIUM) {
			balance[i] = Match2019Atom::REDIUM;
			addScore(-8);
			return;
		}
	}
	for (int i = 0; i < 6; i++) {
		// adds a redium instead of a greenium
		if (balance[i] == Match2019Atom::GREENIUM) {
			balance[i] = Match2019Atom::REDIUM;
			addScore(-4);
			return;
		}
	}
	// all atoms are rediums
}

void MatchData2019::addGoldeniumToArea() {
	addScore(6);
}

void MatchData2019::addBlueiumToBlueArea() {
	addScore(5);
	addBlueiumToArea();
}

void MatchData2019::addBlueiumToArea() {
	addScore(1);
}

void MatchData2019::addGreeniumToGreenArea() {
	addScore(5);
	addGreeniumToArea();
}

void MatchData2019::addGreeniumToArea() {
	addScore(1);
}

void MatchData2019::addRediumToRedArea() {
	addScore(5);
	addRediumToArea();
}

void MatchData2019::addRediumToArea() {
	addScore(1);
}

void MatchData2019::setupExperience() {
	addScore(5);
}

void MatchData2019::runExperience() {
	addScore(15);
}

void MatchData2019::electronSuccessful() {
	addScore(20);
}

void MatchData2019::addToAccelerateur() {
	addScore(10);
}

void MatchData2019::takeGoldeniumOffAcelerateur() {
	addScore(20);
}

void MatchData2019::unlockGoldenium() {
	addScore(10);
}
