/*
 * Match2019SmallTridentAdapter.h
 *
 *  Created on: 23 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_MATCH2019SMALLTRIDENTADAPTER_H_
#define MATCH_2019_MATCH2019SMALLTRIDENTADAPTER_H_

#include "../../devices/actuators/2019/small/SmallActuator.h"
#include "Match2019TridentToActuatorAdapter.h"

class Match2019SmallTridentAdapter : public Match2019TridentToActuatorAdapter {
private:
	SmallActuator &actuator;

public:
	Match2019SmallTridentAdapter(SmallActuator &actuator);

	/**
	 * @inheritDoc
	 */
	virtual int loadFromAllPumps();
	/**
	 * @inheritDoc
	 */
	virtual bool loadFromLeftPump();
	/**
	 * @inheritDoc
	 */
	virtual bool loadFromMiddlePump();
	/**
	 * @inheritDoc
	 */
	virtual bool loadFromRightPump();
	/**
	 * @inheritDoc
	 */
	virtual bool unloadFromAllPumps();
	/**
	 * @inheritDoc
	 */
	virtual bool unloadFromLeftPump();
	/**
	 * @inheritDoc
	 */
	virtual bool unloadFromMiddlePump();
	/**
	 * @inheritDoc
	 */
	virtual bool unloadFromRightPump();
};

#endif /* MATCH_2019_MATCH2019SMALLTRIDENTADAPTER_H_ */
