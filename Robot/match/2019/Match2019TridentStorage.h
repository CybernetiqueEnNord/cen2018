/*
 * Match2019TridentStorage.h
 *
 *  Created on: 23 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_MATCH2019TRIDENTSTORAGE_H_
#define MATCH_2019_MATCH2019TRIDENTSTORAGE_H_

#include "../MatchData.h"
#include "Match2019Atom.h"
#include "MatchData2019.h"
#include "Match2019TridentToActuatorAdapter.h"

class Match2019TridentStorage {
private:
	/**
	 * The match data to calculate the score when elements
	 * are unload in the right place
	 */
	MatchData2019 &matchData;
	/**
	 * The actual actuator that will do the loading and unloading.
	 *
	 * @see Match2019SmallTridentAdapter
	 * @see Match2019BigTridentFrontAdapter
	 * @see Match2019BigTridentBackAdapter
	 */
	Match2019TridentToActuatorAdapter &adapter;
	/**
	 * The current carried load in the left pump.
	 */
	Match2019Atom left_load;
	/**
	 * The current carried load in the middle pump.
	 */
	Match2019Atom middle_load;
	/**
	 * The current carried load in the right pump.
	 */
	Match2019Atom right_load;

public:
	Match2019TridentToActuatorAdapter &getAdapter();
	/**
	 * Constructor with the match data.
	 */
	Match2019TridentStorage(MatchData2019 &matchData, Match2019TridentToActuatorAdapter &adapter);
	/**
	 * Loads this storage according to the disposition of the atoms
	 * on the big rack from our side on the starting position half.
	 *
	 * @return integer the number of atoms loaded.
	 */
	int loadFromOurRackNearStart();
	/**
	 * Loads this storage according to the disposition of the atoms
	 * on the big rack from our side on the balance half.
	 *
	 * @return integer the number of atoms loaded.
	 */
	int loadFromOurRackNearBalance();
	/*
	 * Loads this storage according to the disposition of the atoms
	 * on the big rack from the opponent side on the starting
	 * position half.
	 *
	 * @return integer the number of atoms loaded.
	 */
	int loadFromOppRackNearStart();
	/**
	 * Loads this storage according to the disposition of the atoms
	 * on the big rack from the opponent side on the balance half.
	 *
	 * @return integer the number of atoms loaded.
	 */
	int loadFromOppRackNearBalance();
	/**
	 * Loads this storage according to the disposition of the atoms
	 * on the small rack from our side.
	 *
	 * @return integer the number of atoms loaded.
	 */
	int loadFromSmallRack();
	/**
	 * Unloads all the atoms and count the points as if they were
	 * successfully put on the accelerator.
	 *
	 * @return integer the number of atoms unloaded.
	 */
	int unloadAllInAccelerator();
	/**
	 * Unloads all the atoms and count the points as if they were
	 * successfully put on the balance.
	 *
	 * @return integer the number of atoms unloaded.
	 */
	int unloadAllInBalance();

	/**
	 * Pushes the atom from the ramp into the balance.
	 * /!\ This method does nothing apart counting the points /!\
	 *
	 * @return integer the number of atoms unloaded.
	 */
	int putGreenFromRampToBalance();
	/**
	 * Unloads all the blueium atoms and count the points as if they
	 * were successfully put in the balance.
	 *
	 * @return integer the number of atoms unloaded.
	 */
	int unloadBlueiumsInBalance();
	/**
	 * Unloads all the greenium atoms and count the points as if they
	 * were successfully put in the balance.
	 *
	 * @return integer the number of atoms unloaded.
	 */
	int unloadGreeniumsInBalance();
	/**
	 * Unloads all the redium atoms and count the points as if they
	 * were successfully put in the balance.
	 *
	 * @return integer the number of atoms unloaded.
	 */
	int unloadRediumsInBalance();
	/**
	 * Unloads all the blueium atoms and count the points as if they
	 * were successfully put in the blue area.
	 *
	 * @return integer the number of atoms unloaded.
	 */
	int unloadBlueiumsInArea();
	/**
	 * Unloads all the greeniums and count the points as if they
	 * were successfully put in the green area.
	 *
	 * @return integer the number of atoms unloaded.
	 */
	int unloadGreeniumsInArea();
	/**
	 * Unloads all the rediums and count the points as if they
	 * were successfully put in the red area.
	 *
	 * @return integer the number of atoms unloaded.
	 */
	int unloadRediumsInArea();
	/**
	 * Gets whether there is still blueiums in this storage.
	 *
	 * @return boolean true if there is at least one.
	 */
	bool hasBlueiums();
	/**
	 * Gets whether there is still greeniums in this storage.
	 *
	 * @return boolean true if there is at least one.
	 */
	bool hasGreeniums();
	/**
	 * Gets whether there is still rediums in this storage.
	 *
	 * @return boolean true if there is at least one.
	 */
	bool hasRediums();
	/**
	 * Gets whether there are no more atoms in this storage.
	 *
	 * @return boolean true if there are none.
	 */
	bool isEmpty();
};

#endif /* MATCH_2019_MATCH2019TRIDENTSTORAGE_H_ */
