/*
 * Match2019TridentStorage.cpp
 *
 *  Created on: 23 mai 2019
 *      Author: Anastaszor
 */

#include "Match2019TridentStorage.h"

#include "MatchData2019.h"

Match2019TridentToActuatorAdapter &Match2019TridentStorage::getAdapter()
{
	return adapter;
}

Match2019TridentStorage::Match2019TridentStorage(MatchData2019 &matchData, Match2019TridentToActuatorAdapter &adapter) :
	matchData(matchData), adapter(adapter)
{
	left_load = Match2019Atom::NOT_AN_ATOM;
	middle_load = Match2019Atom::NOT_AN_ATOM;
	right_load = Match2019Atom::NOT_AN_ATOM;
}

int Match2019TridentStorage::loadFromOurRackNearStart()
{
	int res = adapter.loadFromAllPumps();
	if(res == 3)
	{
		left_load = Match2019Atom::REDIUM;
		middle_load = Match2019Atom::GREENIUM;
		right_load = Match2019Atom::REDIUM;
	}
	return res;
}

int Match2019TridentStorage::loadFromOurRackNearBalance()
{
	int res = adapter.loadFromAllPumps();
	if(res == 3)
	{
		left_load = Match2019Atom::GREENIUM;
		middle_load = Match2019Atom::REDIUM;
		right_load = Match2019Atom::BLUEIUM;
	}
	return res;
}

int Match2019TridentStorage::loadFromOppRackNearStart()
{
	int res = adapter.loadFromAllPumps();
	if(res == 3)
	{
		left_load = Match2019Atom::BLUEIUM;
		middle_load = Match2019Atom::REDIUM;
		right_load = Match2019Atom::GREENIUM;
	}
	return res;
}

int Match2019TridentStorage::loadFromOppRackNearBalance()
{
	int res = adapter.loadFromAllPumps();
	if(res == 3)
	{
		left_load = Match2019Atom::REDIUM;
		middle_load = Match2019Atom::GREENIUM;
		right_load = Match2019Atom::REDIUM;
	}
	return res;
}

int Match2019TridentStorage::loadFromSmallRack()
{
	int res = adapter.loadFromAllPumps();
	if(res == 3)
	{
		left_load = Match2019Atom::REDIUM;
		middle_load = Match2019Atom::GREENIUM;
		right_load = Match2019Atom::BLUEIUM;
	}
	return res;
}

int Match2019TridentStorage::unloadAllInAccelerator()
{
	bool res = adapter.unloadFromAllPumps();
	if(res)
	{
		left_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addToAccelerateur();
		middle_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addToAccelerateur();
		right_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addToAccelerateur();
		return 3;
	}
	return 0;
}

int Match2019TridentStorage::unloadAllInBalance()
{
	bool res1 = adapter.unloadFromAllPumps();
	if(res1)
	{
		int res = 0;
		switch(left_load)
		{
			case Match2019Atom::BLUEIUM:
				matchData.addBlueiumToBalance();
				res++;
				break;
			case Match2019Atom::GREENIUM:
				matchData.addGreeniumToBalance();
				res++;
				break;
			case Match2019Atom::REDIUM:
				matchData.addRediumToBalance();
				res++;
				break;
			case Match2019Atom::GOLDENIUM:
				matchData.addGoldeniumToBalance();
				res++;
				break;
			default:
				// nothing to do
				break;
		}
		left_load = Match2019Atom::NOT_AN_ATOM;
		switch(middle_load)
		{
			case Match2019Atom::BLUEIUM:
				matchData.addBlueiumToBalance();
				res++;
				break;
			case Match2019Atom::GREENIUM:
				matchData.addGreeniumToBalance();
				res++;
				break;
			case Match2019Atom::REDIUM:
				matchData.addRediumToBalance();
				res++;
				break;
			case Match2019Atom::GOLDENIUM:
				matchData.addGoldeniumToBalance();
				res++;
				break;
			default:
				// nothing to do
				break;
		}
		middle_load = Match2019Atom::NOT_AN_ATOM;
		switch(right_load)
		{
			case Match2019Atom::BLUEIUM:
				matchData.addBlueiumToBalance();
				res++;
				break;
			case Match2019Atom::GREENIUM:
				matchData.addGreeniumToBalance();
				res++;
				break;
			case Match2019Atom::REDIUM:
				matchData.addRediumToBalance();
				res++;
				break;
			case Match2019Atom::GOLDENIUM:
				matchData.addGoldeniumToBalance();
				res++;
				break;
			default:
				// nothing to do
				break;
		}
		right_load = Match2019Atom::NOT_AN_ATOM;
		return res;
	}
	return 0;
}

int Match2019TridentStorage::putGreenFromRampToBalance()
{
	matchData.addGreeniumToBalance();
	return 1;
}

int Match2019TridentStorage::unloadBlueiumsInBalance()
{
	int res = 0;
	if(left_load == Match2019Atom::BLUEIUM)
	{
		adapter.unloadFromLeftPump();
		left_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addBlueiumToBalance();
		res++;
	}
	if(middle_load == Match2019Atom::BLUEIUM)
	{
		adapter.unloadFromMiddlePump();
		middle_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addBlueiumToBalance();
		res++;
	}
	if(right_load == Match2019Atom::BLUEIUM)
	{
		adapter.unloadFromMiddlePump();
		right_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addBlueiumToBalance();
		res++;
	}
	return res;
}

int Match2019TridentStorage::unloadGreeniumsInBalance()
{
	int res = 0;
	if(left_load == Match2019Atom::GREENIUM)
	{
		adapter.unloadFromLeftPump();
		left_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addGreeniumToBalance();
		res++;
	}
	if(middle_load == Match2019Atom::GREENIUM)
	{
		adapter.unloadFromMiddlePump();
		middle_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addGreeniumToBalance();
		res++;
	}
	if(right_load == Match2019Atom::GREENIUM)
	{
		adapter.unloadFromMiddlePump();
		right_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addGreeniumToBalance();
		res++;
	}
	return res;
}

int Match2019TridentStorage::unloadRediumsInBalance()
{
	int res = 0;
	if(left_load == Match2019Atom::REDIUM)
	{
		adapter.unloadFromLeftPump();
		left_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addRediumToBalance();
		res++;
	}
	if(middle_load == Match2019Atom::REDIUM)
	{
		adapter.unloadFromMiddlePump();
		middle_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addRediumToBalance();
		res++;
	}
	if(right_load == Match2019Atom::REDIUM)
	{
		adapter.unloadFromMiddlePump();
		right_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addRediumToBalance();
		res++;
	}
	return res;
}

int Match2019TridentStorage::unloadBlueiumsInArea()
{
	int res = 0;
	if(left_load == Match2019Atom::BLUEIUM)
	{
		adapter.unloadFromLeftPump();
		left_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addBlueiumToBlueArea();
		res++;
	}
	if(middle_load == Match2019Atom::BLUEIUM)
	{
		adapter.unloadFromMiddlePump();
		middle_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addBlueiumToBlueArea();
		res++;
	}
	if(right_load == Match2019Atom::BLUEIUM)
	{
		adapter.unloadFromMiddlePump();
		right_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addBlueiumToBlueArea();
		res++;
	}
	return res;
}

int Match2019TridentStorage::unloadGreeniumsInArea()
{
	int res = 0;
	if(left_load == Match2019Atom::GREENIUM)
	{
		adapter.unloadFromLeftPump();
		left_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addGreeniumToGreenArea();
		res++;
	}
	if(middle_load == Match2019Atom::GREENIUM)
	{
		adapter.unloadFromMiddlePump();
		middle_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addGreeniumToGreenArea();
		res++;
	}
	if(right_load == Match2019Atom::GREENIUM)
	{
		adapter.unloadFromMiddlePump();
		right_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addGreeniumToGreenArea();
		res++;
	}
	return res;
}

int Match2019TridentStorage::unloadRediumsInArea()
{
	int res = 0;
	if(left_load == Match2019Atom::REDIUM)
	{
		adapter.unloadFromLeftPump();
		left_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addRediumToRedArea();
		res++;
	}
	if(middle_load == Match2019Atom::REDIUM)
	{
		adapter.unloadFromMiddlePump();
		middle_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addRediumToRedArea();
		res++;
	}
	if(right_load == Match2019Atom::REDIUM)
	{
		adapter.unloadFromMiddlePump();
		right_load = Match2019Atom::NOT_AN_ATOM;
		matchData.addRediumToRedArea();
		res++;
	}
	return res;
}

bool Match2019TridentStorage::hasBlueiums()
{
	return   left_load == Match2019Atom::BLUEIUM
		|| middle_load == Match2019Atom::BLUEIUM
		||  right_load == Match2019Atom::BLUEIUM
	;
}

bool Match2019TridentStorage::hasGreeniums()
{
	return   left_load == Match2019Atom::GREENIUM
		|| middle_load == Match2019Atom::GREENIUM
		||  right_load == Match2019Atom::GREENIUM
	;
}

bool Match2019TridentStorage::hasRediums()
{
	return   left_load == Match2019Atom::REDIUM
		|| middle_load == Match2019Atom::REDIUM
		||  right_load == Match2019Atom::REDIUM
	;
}

bool Match2019TridentStorage::isEmpty()
{
	return   left_load == Match2019Atom::NOT_AN_ATOM
		&& middle_load == Match2019Atom::NOT_AN_ATOM
		&&  right_load == Match2019Atom::NOT_AN_ATOM
	;
}
