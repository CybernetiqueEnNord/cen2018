/*
 * Match2019.h
 *
 *  Created on: may. 2019
 *      Author: Robin
 */

#ifndef MATCH_MATCH2019_STRATEGY_INDEX_H_
#define MATCH_MATCH2019_STRATEGY_INDEX_H_


#define STRATEGIE_INDEX_HOMOLOGUATION 0
#define STRATEGIE_INDEX_NOMINAL 1
#define STRATEGIE_INDEX_SAFE 2

//Quantity of strategie defined.
#define STRATEGIE_INDEX_NOMINAL_QUANTITY 2


#endif /* MATCH_MATCH2019_H_ */
