/*
 * Match2019TridentToActuatorAdapter.cpp
 *
 *  Created on: 23 mai 2019
 *      Author: Anastaszor
 */

#include "Match2019TridentToActuatorAdapter.h"

/**
 * @inheritDoc
 */
int Match2019TridentToActuatorAdapter::loadFromAllPumps()
{
	return 0;
}

/**
 * @inheritDoc
 */
bool Match2019TridentToActuatorAdapter::loadFromLeftPump()
{
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019TridentToActuatorAdapter::loadFromMiddlePump()
{
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019TridentToActuatorAdapter::loadFromRightPump()
{
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019TridentToActuatorAdapter::unloadFromAllPumps()
{
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019TridentToActuatorAdapter::unloadFromLeftPump()
{
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019TridentToActuatorAdapter::unloadFromMiddlePump()
{
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019TridentToActuatorAdapter::unloadFromRightPump()
{
	return false;
}
