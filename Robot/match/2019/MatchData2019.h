/*
 * MatchData2019.h
 *
 *  Created on: 11 oct. 2018
 *      Author: Aeon
 */

#ifndef MATCH_MATCHDATA2019_H_
#define MATCH_MATCHDATA2019_H_

#include "devices/timer/MatchTimer.h"
#include "match/MatchData.h"
#include "match/2019/Match2019Atom.h"

class MatchData2019 : public MatchData {
private:
	Match2019Atom balance[6];

public:
	MatchData2019(const MatchTimer& timer);

	void addGoldeniumToBalance();
	void addBlueiumToBalance();
	void addGreeniumToBalance();
	void addRediumToBalance();
	void addGoldeniumToArea();
	void addBlueiumToBlueArea();
	void addBlueiumToArea();
	void addGreeniumToGreenArea();
	void addGreeniumToArea();
	void addRediumToRedArea();
	void addRediumToArea();
	void setupExperience();
	void runExperience();
	void electronSuccessful();
	void addToAccelerateur();
	void takeGoldeniumOffAcelerateur();
	void unlockGoldenium();
};

#endif /* MATCH_MATCHDATA2019_H_ */
