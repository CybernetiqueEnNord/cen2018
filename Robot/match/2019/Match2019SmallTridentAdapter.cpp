/*
 * Match2019SmallTridentAdapter.cpp
 *
 *  Created on: 23 mai 2019
 *      Author: Anastaszor
 */

#include "Match2019SmallTridentAdapter.h"

Match2019SmallTridentAdapter::Match2019SmallTridentAdapter(SmallActuator &actuator) :
	actuator(actuator)
{
	// nothing to do
}

/**
 * @inheritDoc
 */
int Match2019SmallTridentAdapter::loadFromAllPumps()
{
	bool res = actuator.setPumpTrio(true);
	if(res)
		return 3;
	return 0;
}

/**
 * @inheritDoc
 */
bool Match2019SmallTridentAdapter::loadFromLeftPump()
{
	// not implemented in actuator
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019SmallTridentAdapter::loadFromMiddlePump()
{
	// not implemented in actuator
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019SmallTridentAdapter::loadFromRightPump()
{
	// not implemented in actuator
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019SmallTridentAdapter::unloadFromAllPumps()
{
	return actuator.setPumpTrio(false);
}

/**
 * @inheritDoc
 */
bool Match2019SmallTridentAdapter::unloadFromLeftPump()
{
	return actuator.setPumpOff(TRIO_LEFT_PUMP);
}

/**
 * @inheritDoc
 */
bool Match2019SmallTridentAdapter::unloadFromMiddlePump()
{
	return actuator.setPumpOff(TRIO_CENTER_PUMP);
}

/**
 * @inheritDoc
 */
bool Match2019SmallTridentAdapter::unloadFromRightPump()
{
	return actuator.setPumpOff(TRIO_RIGHT_PUMP);
}
