/*
 * Match2019BigTridentBackAdapter.cpp
 *
 *  Created on: 23 mai 2019
 *      Author: Anastaszor
 */

#include "Match2019BigTridentBackAdapter.h"

Match2019BigTridentBackAdapter::Match2019BigTridentBackAdapter(BigActuator &actuator) :
	actuator(actuator)
{
// nothing to do
}


/**
 * @inheritDoc
 */
int Match2019BigTridentBackAdapter::loadFromAllPumps()
{
	bool res = actuator.setPumpBackTrio(true);
	if(res)
		return 3;
	return 0;
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentBackAdapter::loadFromLeftPump()
{
	// not implemented in actuator
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentBackAdapter::loadFromMiddlePump()
{
	// not implemented in actuator
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentBackAdapter::loadFromRightPump()
{
	// not implemented in actuator
	return false;
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentBackAdapter::unloadFromAllPumps()
{
	return actuator.setPumpBackTrio(false);
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentBackAdapter::unloadFromLeftPump()
{
	// intentionally reversed because of central symmetry
	return actuator.setPumpBackOff(TRIO_RIGHT_PUMP);
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentBackAdapter::unloadFromMiddlePump()
{
	return actuator.setPumpBackOff(TRIO_CENTER_PUMP);
}

/**
 * @inheritDoc
 */
bool Match2019BigTridentBackAdapter::unloadFromRightPump()
{
	// intentionally reversed because of central symmetry
	return actuator.setPumpBackOff(TRIO_LEFT_PUMP);
}
