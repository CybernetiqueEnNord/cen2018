/*
 * Match2019.cpp
 *
 *  Created on: 11 oct. 2019
 *      Author: Anastaszor
 */

#include "Match2019.h"

#include "control/Navigation.h"
#include "devices/actuators/2019/big/BigActuator.h"
#include "devices/actuators/2019/small/SmallActuator.h"
#include "devices/beacon/Beacon2018.h"
#include "devices/host/HostCapabilities2019.h"
#include "devices/host/HostDevice.h"
#include "devices/host/HostDataF446REBig.h"
#include "devices/host/HostDataF446RENextGenBig.h"
#include "devices/host/HostDataF446RENextGenSmall.h"
#include "devices/host/HostDataF446RESmall.h"
#include "devices/io/LCDScreen.h"
#include "match/2019/MatchTaskList2019.h"
#include "match/2019/Match2019StrategieIndexCalibration.h"
#include "match/2019/actions/SmallAtomsDispenserAction.h"
#include "match/2019/actions/GoldeniumAndDispenserAction.h"
#include "match/2019/actions/big/BigPrimaryTaskList.h"
#include "match/2019/actions/small/SmallPrimaryTaskList.h"
#include "match/2019/actions/small/SmallSafeTaskList.h"
#include "math/cenMath.h"
#include "utils/Text.h"

#include "match/2019/Match2019TridentStorage.h"
#include "match/2019/Match2019SmallTridentAdapter.h"

Match2019::Match2019(Navigation &navigation) :
		Match(navigation, createMatchData(matchTimer)), calibrator(navigation), executor(navigation) {
}

void Match2019::execute() {
	extern LCDScreen screen;
	screen.displayMessage("C'est parti !");

	PositionControl &control = navigation.getControl();
	// d�tection d'obstacle active par d�faut sauf pour la calibration
	control.obstacleAvoidance = !calibration;

	extern Beacon2018 beacon;
	beacon.setBeepEnabled(true);

	if (calibration) {
		bool test = trajectoryIndex >= CALIBRATION_STRATEGY_COUNT;
		if (test) {
			executeTest(trajectoryIndex - CALIBRATION_STRATEGY_COUNT);
		} else {
			DeviceId id = HostDevice::getDeviceId();
			if (id == HostData_F446RE_Small::getDeviceId() || id == HostData_F446RE_NextGen_Small::getDeviceId()) {
				calibrator.executeCalibrationSmall(trajectoryIndex);
			} else if (id == HostData_F446RE_Big::getDeviceId() || id == HostData_F446RE_NextGen_Big::getDeviceId()) {
				calibrator.executeCalibrationBig(trajectoryIndex);
			} else {
				extern LCDScreen screen;
				screen.displayMessage("Calibration Hote non pris en charge");
			}
		}
	} else {
		DeviceId id = HostDevice::getDeviceId();
		if (id == HostData_F446RE_Small::getDeviceId() || id == HostData_F446RE_NextGen_Small::getDeviceId()) {
			executeSmall();
		} else if (id == HostData_F446RE_Big::getDeviceId() || id == HostData_F446RE_NextGen_Big::getDeviceId()) {
			executeBig();
		} else {
			screen.displayMessage("Hote non pris en charge");
		}
	}

	setActionResult(ActionResult::Next);
}

void Match2019::prepare() {
	extern Beacon2018 beacon;
	beacon.setLedBlinking(100, 100, 0);

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = false;

	//Set threshold on all strategie, and enable beep. => Beep to be tested during initialisation.
	beacon.setThreshold(600);
	beacon.setBeepEnabled(false);

	displayMatchSide();
	displayTrajectory();

	if (calibration) {
		DeviceId id = HostDevice::getDeviceId();
		if (id == HostData_F446RE_Small::getDeviceId() || id == HostData_F446RE_NextGen_Small::getDeviceId()) {
			calibrator.prepareCalibrationSmall(trajectoryIndex);
		} else if (id == HostData_F446RE_Big::getDeviceId() || id == HostData_F446RE_NextGen_Big::getDeviceId()) {
			calibrator.prepareCalibrationBig(trajectoryIndex);
		} else {
			extern LCDScreen screen;
			screen.displayMessage("Calibration Hote non pris en charge");
		}
	} else {
		DeviceId id = HostDevice::getDeviceId();
		if (id == HostData_F446RE_Small::getDeviceId() || id == HostData_F446RE_NextGen_Small::getDeviceId()) {
			prepareSmall();
		} else if (id == HostData_F446RE_Big::getDeviceId() || id == HostData_F446RE_NextGen_Big::getDeviceId()) {
			prepareBig();
		} else {
			extern LCDScreen screen;
			screen.displayMessage("Hote non pris en charge");
		}
	}

	navigation.waitForStart(this);
}

void Match2019::executeTest(int taskIndex) {
	// Tests unitaires
	MatchTaskList &list = getTestsTasksList();
	for (int i = 0; i < (signed) list.getCount(); i++) {
		if (i != taskIndex) {
			MatchTask &task = list.getTaskAt(i);
			task.setDone();
		}
	}
	executor.setTasksList(list);
	executor.run();
}

void Match2019::initialize() {
	Match::initialize();

	displayMatchSide();
	displayTrajectory();

	extern Beacon2018 beacon;
	beacon.setLedBlinking(100, 400, 0);

	const Beacon2018Data &data = beacon.getData();
	do {
		beacon.updateHMIData();
		updateMatchSide();
		if (trajectorySelectable) {
			updateTrajectory();
		}
		chThdSleep(50);
	} while (data.started);
}

void Match2019::onNavigationEvent(const Navigation &navigation) {
	(void) navigation;
}

void Match2019::terminate() {
	const PositionControl &control = navigation.getControl();
	MoveError error = control.getError();

	// arr�t actionneurs
	if (hasCapability(CAPABILITY_SMALL)) {
		extern SmallActuator smallActuator;
		smallActuator.kill();
	}
	if (hasCapability(CAPABILITY_BIG)) {
		extern BigActuator bigActuator;
		bigActuator.kill();
	}

	Match::terminate();

	extern LCDScreen screen;
	if (navigation.getCommand() == MoveCommand::EmergencyStop) {
		screen.displayMessage("  ARRET D'URGENCE");
	} else if (error == MoveError::Overrun) {
		screen.displayMessage("      OVERRUN");
	} else {
		screen.displayMessage("   MATCH TERMINE");
	}
}

void Match2019::displayMatchSide() {
	extern LCDScreen screen;
	bool side = navigation.getControl().isReversed();
	if (calibration) {
		screen.displayMessage(side ? "CalibV" : "CalibJ");
	} else {
		screen.displayMessage(side ? "Violet" : "Jaune");
	}
}

void Match2019::displayTrajectory() {
	extern LCDScreen screen;

	if (calibration) {
		if (trajectoryIndex < CALIBRATION_STRATEGY_COUNT) {
			calibrator.displayTrajectory(trajectoryIndex);
		} else {
			// Tests unitaires
			MatchTaskList& list = getTestsTasksList();
			unsigned int index = trajectoryIndex - CALIBRATION_STRATEGY_COUNT;
			if (index >= list.getCount()) {
				extern LCDScreen screen;
				screen.displayMessage("none", 8); //"!Act nbr:" + trajectoryIndex - STRATEGIE_INDEX_NOMINAL_QUANTITY + " Mx:" + list.getCount());
			} else {
				MatchTask &task = list.getTaskAt(index);
				screen.displayMessage(task.getLabel(), 8);
			}
		}
	} else {
		switch (trajectoryIndex) {
			case STRATEGIE_INDEX_HOMOLOGUATION:
				screen.displayMessage("HOMOLOG", 8);
				break;

			case STRATEGIE_INDEX_NOMINAL:
				screen.displayMessage("NOMINAL", 8);
				break;

			default: {
				char buffer[10];
				itoa(trajectoryIndex, buffer, 10);
				screen.displayMessage(buffer, 8);
				break;
			}
		}
	}

}

void Match2019::changeMatchSide(bool newSide) {
	PositionControl &control = navigation.getControl();
	control.setReversed(newSide);
	extern Beacon2018 beacon;
	beacon.setReversed(newSide);
	displayMatchSide();
}

void Match2019::changeTrajectory(int trajectoryIndex) {
	this->trajectoryIndex = trajectoryIndex;
	displayTrajectory();
}

void Match2019::updateMatchSide() {
	extern Beacon2018 beacon;
	const Beacon2018Data &data = beacon.getData();
	bool side = data.hmiValue1 < 128;
	bool calibration = (data.hmiValue1 > 96) && (data.hmiValue1 < 160);
	PositionControl &control = navigation.getControl();
	if (side != control.isReversed() || calibration != this->calibration) {
		this->calibration = calibration;
		changeMatchSide(side);
	}

}

void Match2019::updateTrajectory() {
	extern Beacon2018 beacon;
	const Beacon2018Data &data = beacon.getData();
	// 20 positions en calibration, 8 en match
	int index = data.hmiValue2 / (calibration ? 13 : 36);
	if (index != trajectoryIndex) {
		changeTrajectory(index);
	}
}

bool Match2019::hasCapability(unsigned int capability) const {
	const HostData &data = HostDevice::getHostData();
	unsigned int capabilities = data.getCapabilities();
	bool result = (capabilities & capability) != 0;
	return result;
}

void Match2019::executeBig() {
	switch (trajectoryIndex) {
		case STRATEGIE_INDEX_HOMOLOGUATION:
			executeBigHomologation();
			break;

		case STRATEGIE_INDEX_NOMINAL:
			executeBigMatchNominal();
			break;

		default:
			break;
	}
}

void Match2019::executeSmall() {
	switch (trajectoryIndex) {
		case STRATEGIE_INDEX_HOMOLOGUATION:
			executeSmallHomologation();
			break;

		case STRATEGIE_INDEX_NOMINAL:
			executeSmallMatchNominal();
			break;

		case STRATEGIE_INDEX_SAFE:
			executeSmallMatchSafe();
			break;

		case 3: {
			MatchData2019 &data = getMatchData();
			data.setupExperience();
			chThdSleepSeconds(1);
			data.runExperience();
			chThdSleepSeconds(1);
			data.electronSuccessful();
			chThdSleepSeconds(1);
			data.takeGoldeniumOffAcelerateur();
			chThdSleepSeconds(1);
			data.addGreeniumToBalance();
			chThdSleepSeconds(1);
			data.addGoldeniumToBalance();
			chThdSleepSeconds(1);

			extern SmallActuator smallActuator;
			Match2019SmallTridentAdapter adapter(smallActuator);
			Match2019TridentStorage storage(data, adapter);
			storage.loadFromSmallRack();
			chThdSleepSeconds(1);
			storage.unloadAllInBalance();
			chThdSleepSeconds(1);
			break;
		}

		default:
			executeSmallMatchNominal();
			break;
	}
}

void Match2019::prepareBig() {
	switch (trajectoryIndex) {
		case STRATEGIE_INDEX_HOMOLOGUATION:
			prepareBigHomologation();
			break;

		case STRATEGIE_INDEX_NOMINAL:
			prepareBigMatchNominal();
			break;

		default:
			// include STRATEGIE_INDEX_SAFE
			prepareBigMatchNominal();
			break;
	}
}

void Match2019::prepareSmall() {
	switch (trajectoryIndex) {
		case STRATEGIE_INDEX_HOMOLOGUATION:
			prepareSmallHomologation();
			break;

		case STRATEGIE_INDEX_NOMINAL:
			prepareSmallMatchNominal();
			break;

		case STRATEGIE_INDEX_SAFE:
			prepareSmallMatchSafe();
			break;

		case 3:
			break;

		default:
			prepareSmallMatchNominal();
			break;
	}
}

// Homologation Gros

void Match2019::prepareBigHomologation() {
	//Same as match 1
	navigation.waitForDelay(500);
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	navigation.repositionMM(-200, 2000 - geometry.rearWidth, 0, 1800);
	//Go to the center of the terrain:
	navigation.moveMM(1000 - geometry.rearWidth);
	//Recallage en y dans la zone bleu
	navigation.rotateDeciDegrees(-900);
	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	//Se centrer dans la zone bleu
	navigation.moveMM(225 - geometry.rearWidth);
	//S'orienter
	navigation.rotateDeciDegrees(-900);
	//rentrer dans la zone verte
	navigation.moveMM(-250);
}

void Match2019::executeBigHomologation() {
	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;

	navigation.moveTo(Point(1370, 225));
	navigation.moveTo(Point(1370, 1300));
	navigation.moveTo(Point(1050, 1300));
	navigation.moveTo(Point(1050, 300));

	return;
}

void Match2019::prepareSmallHomologation() {
	navigation.waitForDelay(500);

	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();

	extern SmallActuator smallActuator;
	PositionControl &control = navigation.getControl();

	smallActuator.setPumpGoldenium(true);
	smallActuator.setFlapDropGoldenium();
	navigation.waitForDelay(500);
	smallActuator.setPumpGoldenium(false);
	smallActuator.setFlapPickup();

	bool side = control.isReversed();
	smallActuator.setMatchSide(side);

	navigation.repositionMM(-200, geometry.rearWidth, 0, 0);
	smallActuator.setFlapClosed();
	smallActuator.setArm(true);

	navigation.moveMM(450 - geometry.rearWidth);
	smallActuator.setArm(false);
	smallActuator.setPumpTrio(true);
	navigation.rotateDeciDegrees(900);
	smallActuator.setPumpTrio(false);

	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	navigation.moveMM(300 - geometry.rearWidth);
	navigation.rotateDeciDegrees(900);
	// D�part : 450;300
}

void Match2019::executeSmallHomologation() {
	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;

	// D�part : 450;300
	navigation.moveTo(Point(250, 300));
	navigation.moveTo(Point(250, 1000));
	navigation.moveTo(Point(450, 1000));
	navigation.moveTo(Point(450, 400));
}

void Match2019::prepareBigMatchNominal() {
	// liste des t�ches
	MatchData2019 &matchData = getMatchData();
	static BigPrimaryTaskList list(matchData);

	executor.setTasksList(list);

	extern BigActuator bigActuator;
	//Same as homologuation
	navigation.waitForDelay(500);
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	navigation.repositionMM(-200, 2000 - geometry.rearWidth, 0, 1800);
	PositionControl &control = navigation.getControl();
	bool side = control.isReversed();
	bigActuator.setMatchSide(side);

	extern Beacon2018 beacon;
	beacon.setThreshold(600);

	bigActuator.setLifterBack(0);	//full up
	//Go to the center of the terrain:
	navigation.moveMM(1000 - geometry.rearWidth);
	//Recallage en y dans la zone bleu
	navigation.rotateDeciDegrees(-900);
	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	//Se centrer dans la zone bleu
	navigation.moveMM(225 - geometry.rearWidth);
	//S'orienter
	navigation.rotateDeciDegrees(-900);
	//rentrer dans la zone verte
	navigation.moveMM(-250);
}

void Match2019::executeBigMatchNominal() {
	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	control.setSpeedIndex(3);

	navigation.waitForDelay(500);

	// prise en compte de l'�chec apr�s 10 secondes
	executor.setObstacleTimeout(10000);
	executor.run();
}

void Match2019::executeBigMatchRobinToTestPrisePalets() {
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;

	control.setSpeedIndex(11);

	navigation.moveTo(Point(1370, 225));

	// prise du tas
	navigation.moveTo(Point(1350, 1280));
	navigation.moveTo(Point(1270, 1280));
	navigation.moveTo(Point(580, 360));
	navigation.moveMM(-200);
	getMatchData().addRediumToRedArea();
	getMatchData().addRediumToRedArea();
	getMatchData().addRediumToArea();
	getMatchData().addRediumToArea();

	// prise du redium
	navigation.moveTo(Point(650, 800));
	navigation.moveTo(Point(450, 800));
	navigation.moveTo(Point(450, 350));
	navigation.moveMM(-200);

	// d�clenchement de l'exp�rience
	navigation.moveTo(Point(240, 550));
	navigation.moveTo(Point(240, 250));
	navigation.rotateToOrientationDeciDegrees(0);
	navigation.repositionMM(-300, geometry.rearWidth, CURRENT, 0);
	navigation.moveMM(150);
	navigation.rotateToOrientationDeciDegrees(900);
	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	navigation.moveMM(200);

	// prise en face de la zone bleue
	navigation.moveTo(Point(350, 750));
	navigation.moveTo(Point(1300, 750));
	navigation.moveTo(Point(1300, 580));
	navigation.moveTo(Point(665, 390));
	getMatchData().addRediumToArea();
	getMatchData().addRediumToArea();
	getMatchData().addRediumToRedArea();
}

void Match2019::executeBigMatchATransformerEnAction() {
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();

	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;

	control.setSpeedIndex(11);

	extern BigActuator bigActuator;
	bool side = control.isReversed();
	bigActuator.setMatchSide(side);


	bigActuator.setLifterFront(4);	//full down
	bigActuator.setLifterBack(4);	//full down

	navigation.moveTo(Point(1300, 600));
	navigation.moveTo(Point(1543 - geometry.rearWidth - 10, 600));
	bigActuator.setPumpFrontTrio(true);
	navigation.repositionMM(500, 1543 - geometry.rearWidth, CURRENT, 0);
	navigation.waitForDelay(200);
	bigActuator.setLifterFront(0); //Full up

	navigation.moveMMCircleDecidegree(-471, -1800);
	bigActuator.setPumpBackTrio(true);
	navigation.repositionMM(-500, 1543 - geometry.rearWidth, CURRENT, 1800);
	navigation.waitForDelay(200);
	bigActuator.setLifterBack(0); //full up
	navigation.moveMM(200);

	//Move to depose
	navigation.moveTo(Point(300, 2000));
	navigation.moveTo(Point(geometry.rearWidth + 35 + 10, 2000));
	navigation.repositionMM(500, geometry.rearWidth, CURRENT, 1800);
	bigActuator.setLifterFront(2); //Depose down
	navigation.waitForDelay(400);
	navigation.moveMM(-18);
	bigActuator.setPumpFrontTrio(false); //Depose all
	navigation.waitForDelay(2000);
	navigation.moveMM(-200);

	navigation.moveToBackwards(Point(geometry.rearWidth + 35 + 10, 2000));
	navigation.repositionMM(-500, geometry.rearWidth, CURRENT, 0);
	bigActuator.setLifterBack(2); //Depose down
	navigation.waitForDelay(400);
	navigation.moveMM(18);
	bigActuator.setPumpBackTrio(false); //Depose all
	navigation.waitForDelay(2000);

	navigation.moveMM(200);
	bigActuator.setLifterBack(4); //down
	bigActuator.setLifterFront(4); //down

}

// Match 1 Petit
void Match2019::prepareSmallMatchNominal() {
	// liste des t�ches
	MatchData2019 &matchData = getMatchData();
	static SmallPrimaryTaskList list(matchData);
	executor.setTasksList(list);

	// Points de pr�sence de l'exp�rience
	matchData.setupExperience();
	extern SmallActuator smallActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	PositionControl &control = navigation.getControl();

	control.setSpeedIndex(SPEED_SMALL_INITIALISATION);

	//Wait for the tirette to be correctly inserted.
	smallActuator.setPumpGoldenium(true);
	smallActuator.setFlapDropGoldenium();
	navigation.waitForDelay(500);
	smallActuator.setPumpGoldenium(false);
	smallActuator.setFlapPickup();

	bool side = control.isReversed();
	smallActuator.setMatchSide(side);

	navigation.repositionMM(-200, geometry.rearWidth, 0, 0);
	smallActuator.setFlapClosed();
	smallActuator.setArm(true);

	navigation.moveMM(450 - geometry.rearWidth);
	smallActuator.setArm(false);
	smallActuator.setPumpTrio(true);
	navigation.rotateDeciDegrees(900);
	smallActuator.setPumpTrio(false);

	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	navigation.moveMM(280 - geometry.rearWidth);
	navigation.rotateDeciDegrees(900);

	// D�part : 450;280
}

void Match2019::prepareSmallMatchSafe() {
	// liste des t�ches
	MatchData2019 &matchData = getMatchData();
	static SmallSafeTaskList list(matchData);
	executor.setTasksList(list);

	// Points de pr�sence de l'exp�rience
	matchData.setupExperience();
	extern SmallActuator smallActuator;
	const HostData &data = HostDevice::getHostData();
	const RobotGeometry &geometry = data.getGeometry();
	PositionControl &control = navigation.getControl();

	control.setSpeedIndex(SPEED_SMALL_INITIALISATION);

	//Wait for the tirette to be correctly inserted.
	smallActuator.setPumpGoldenium(true);
	smallActuator.setFlapDropGoldenium();
	navigation.waitForDelay(500);
	smallActuator.setPumpGoldenium(false);
	smallActuator.setFlapPickup();

	bool side = control.isReversed();
	smallActuator.setMatchSide(side);

	navigation.repositionMM(-200, geometry.rearWidth, 0, 0);
	smallActuator.setFlapClosed();
	smallActuator.setArm(true);

	navigation.moveMM(450 - geometry.rearWidth);
	smallActuator.setArm(false);
	smallActuator.setPumpTrio(true);
	navigation.rotateDeciDegrees(900);
	smallActuator.setPumpTrio(false);

	navigation.repositionMM(-300, CURRENT, geometry.rearWidth, 900);
	navigation.moveMM(300 - geometry.rearWidth);
	navigation.rotateDeciDegrees(900);

	// Points de pr�sence de l'exp�rience
	//	getMatchData().setupExperience();
}

void Match2019::executeSmallMatchNominal() {
	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	control.setSpeedIndex(SPEED_SMALL_NOMINAL);

	// prise en compte de l'�chec apr�s 10 secondes
	executor.setObstacleTimeout(10000);
	executor.run();
}

void Match2019::executeSmallMatchSafe() {
	PositionControl &control = navigation.getControl();
	control.obstacleAvoidance = true;
	control.setSpeedIndex(SPEED_SMALL_NOMINAL);

	// prise en compte de l'�chec apr�s 10 secondes
	executor.setObstacleTimeout(10000);
	executor.run();
}

void Match2019::terminateNavigation() {
	// Pas d'arr�t si proc�dure de recalage
	if (calibration) {
		return;
	}
	Match::terminateNavigation();
}

MatchData& Match2019::createMatchData(const MatchTimer& timer) {
	static MatchData2019 data(timer);
	return data;
}

MatchData2019 & Match2019::getMatchData() const {
	return (MatchData2019 &) Match::getMatchData();
}

MatchTaskList& Match2019::getTestsTasksList() {
	MatchData2019 &matchData = getMatchData();
	DeviceId id = HostDevice::getDeviceId();
	if (id == HostData_F446RE_Small::getDeviceId() || id == HostData_F446RE_NextGen_Small::getDeviceId()) {
		static SmallPrimaryTaskList tasks(matchData);
		return tasks;
	} else if (id == HostData_F446RE_Big::getDeviceId() || id == HostData_F446RE_NextGen_Big::getDeviceId()) {
		static BigPrimaryTaskList tasks(matchData);
		return tasks;
	} else {
		class EmptyTaskList: public MatchTaskList {
		protected:
			virtual unsigned int getCount() const {
				return 0;
			}
			virtual MatchTask& getTaskAt(unsigned int index) const {
				(void) index;
				static MatchTask none("none", 0, 0, Pointer<MatchTarget>());
				return none;
			}
		};
		static EmptyTaskList tasks;
		return tasks;
	}
}
