/*
 * Match2019TridentToActuatorAdapter.h
 *
 *  Created on: 23 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_MATCH2019TRIDENTTOACTUATORADAPTER_H_
#define MATCH_2019_MATCH2019TRIDENTTOACTUATORADAPTER_H_

/**
 * Abstract class for actuators that controls only the pumps that
 * are used in trident formation.
 *
 * @see Match2019SmallTridentAdapter for concrete implementation
 * 		for the small robot
 * @see Match2019BigTridentFrontAdapter for concrete implementation
 * 		for the big robot with the front actuator
 * @see Match2019BigTridentBackAdapter for concrete implementation
 * 		for the big robot with the back actuator
 */
class Match2019TridentToActuatorAdapter {
public:
	/**
	 * Loads atoms from all pumps.
	 *
	 * @return int the number of atoms loaded.
	 */
	virtual int loadFromAllPumps();
	/**
	 * Loads an atom from the left pump.
	 *
	 * @return boolean an atom is loaded.
	 */
	virtual bool loadFromLeftPump();
	/**
	 * Loads an atom from the middle pump.
	 *
	 * @return boolean an atom is loaded.
	 */
	virtual bool loadFromMiddlePump();
	/**
	 * Loads an atom from the right pump.
	 *
	 * @return boolean an atom is loaded.
	 */
	virtual bool loadFromRightPump();
	/**
	 * Unloads all atoms from all pumps.
	 *
	 * @return boolean there is no atoms left in all pumps.
	 */
	virtual bool unloadFromAllPumps();
	/**
	 * Unloads an atom from the left pump.
	 *
	 * @return boolean there is no atoms left in this pump.
	 */
	virtual bool unloadFromLeftPump();
	/**
	 * Unloads an atom from the middle pump.
	 *
	 * @return boolean there is no atoms left in this pump.
	 */
	virtual bool unloadFromMiddlePump();
	/**
	 * Unloads an atom from the right pump.
	 *
	 * @return boolean there is no atoms left in this pump.
	 */
	virtual bool unloadFromRightPump();
};

#endif /* MATCH_2019_MATCH2019TRIDENTTOACTUATORADAPTER_H_ */
