/*
 * Match2019BigTridentBackAdapter.h
 *
 *  Created on: 23 mai 2019
 *      Author: Anastaszor
 */

#ifndef MATCH_2019_MATCH2019BIGTRIDENTBACKADAPTER_H_
#define MATCH_2019_MATCH2019BIGTRIDENTBACKADAPTER_H_

#include "../../devices/actuators/2019/big/BigActuator.h"
#include "Match2019TridentToActuatorAdapter.h"

class Match2019BigTridentBackAdapter : public Match2019TridentToActuatorAdapter {
private:
	BigActuator &actuator;

public:
	Match2019BigTridentBackAdapter(BigActuator &actuator);

	/**
	 * @inheritDoc
	 */
	virtual int loadFromAllPumps();
	/**
	 * @inheritDoc
	 */
	virtual bool loadFromLeftPump();
	/**
	 * @inheritDoc
	 */
	virtual bool loadFromMiddlePump();
	/**
	 * @inheritDoc
	 */
	virtual bool loadFromRightPump();
	/**
	 * @inheritDoc
	 */
	virtual bool unloadFromAllPumps();
	/**
	 * @inheritDoc
	 */
	virtual bool unloadFromLeftPump();
	/**
	 * @inheritDoc
	 */
	virtual bool unloadFromMiddlePump();
	/**
	 * @inheritDoc
	 */
	virtual bool unloadFromRightPump();
};

#endif /* MATCH_2019_MATCH2019BIGTRIDENTBACKADAPTER_H_ */
