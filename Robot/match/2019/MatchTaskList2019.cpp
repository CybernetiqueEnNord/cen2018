/*
 * MatchTaskList2019.cpp
 *
 *  Created on: 18 f�vr. 2019
 *      Author: Emmanuel
 */

#include "MatchTaskList2019.h"

#include "match/MatchTarget.h"
#include "match/2019/actions/BlueiumAction.h"
#include "match/2019/actions/GoldeniumAction.h"
#include "match/SingleActionList.h"
#include "match/2019/actions/Target1ActionList.h"
#include "utils/Arrays.h"

class MatchTimerCondition: public MatchElementCallback {
private:
	MatchData2019 &data;
	int minimumTime;

protected:
	virtual MatchElementState customGetState() const {
		const MatchTimer &timer = data.getTimer();
		int remainingTime = timer.getRemainingTimeMS();
		return remainingTime > minimumTime ? MatchElementState::Available : MatchElementState::Unavailable;
	}

public:
	MatchTimerCondition(MatchData2019 &data, int minimumTime) :
			MatchElementCallback(), data(data), minimumTime(minimumTime) {
	}
};

MatchTaskList2019::MatchTaskList2019(MatchData2019 &matchData) :
		MatchTaskList(), matchData(matchData), tasks(createTasks()) {
}

unsigned int MatchTaskList2019::getCount() const {
	return ARRAY_SIZE(tasks);
}

MatchTask& MatchTaskList2019::getTaskAt(unsigned int index) const {
	return tasks[index];
}

MatchTask (&MatchTaskList2019::createTasks())[2] {
			// redium
			static MatchElement rediumStartElement;
			// prise
			static MatchAction rediumStartAction("prise", Point(1480, 200));
			static SingleActionList rediumPickupActions(rediumStartAction);
			static MatchTarget target1(rediumPickupActions, rediumStartElement);
			// d�pose
			static MatchAction rediumDropAction("depose", Point(1460, 200));
			static SingleActionList rediumDropActions(rediumDropAction);
			static MatchTarget target2(rediumDropActions, rediumStartElement);
			// t�che
			target1.setNext(target2);
			static MatchTask task1("red start", 5, 5, Pointer<MatchTarget>(&target1));

			// blueium
			static MatchElement blueiumElement;
			static BlueiumAction blueiumAction(matchData);
			static SingleActionList blueiumActions(blueiumAction);
			static MatchTarget blueiumTarget(blueiumActions, blueiumElement);
			// goldenium
			static MatchElement goldeniumElement;
			static MatchTimerCondition goldeniumState(matchData, 20000);
			goldeniumElement.setCustomGetState(goldeniumState);
			static GoldeniumAction goldeniumAction(matchData);
			static SingleActionList goldeniumActions(goldeniumAction);
			static MatchTarget goldeniumTarget(goldeniumActions, goldeniumElement);
			// t�che
			blueiumTarget.setNext(goldeniumTarget);
			static MatchTask taskBlueium("blueium", 20, 5, Pointer<MatchTarget>(&blueiumTarget));

			static MatchTask tasks[2] = { task1, taskBlueium };
			return tasks;
		}
