/*
 ChibiOS - Copyright (C) 2006..2016 Giovanni Di Sirio

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#include "ch.h"
#include "hal.h"
#include "hal_community.h"
#include "ch_test.h"

#include "devices/beacon/Beacon2018.h"
#include "devices/host/HostDevice.h"
#include "devices/io/AnalogReader.h"
#include "devices/io/Console.h"
#include "devices/io/LCDScreen.h"
#include "devices/io/StatusLed.h"
#include "devices/motors/MotorVNH5019.h"
#include "init/Initialization.h"
#include "init/Peripherals.h"
#include "match/MatchTest.h"
#include "match/MatchDefault.h"
#include "parser/InputParser.h"

extern Console console;
extern InputParser inputParser;
extern Navigation navigation;
extern PositionControl control;

StatusLed &statusLed = StatusLed::getInstance();

/*
 * Green LED blinker thread, times are in milliseconds.
 */
static THD_WORKING_AREA(waThreadBlinker, 128);
static THD_FUNCTION(ThreadBlinker, arg) {

	(void) arg;
	chRegSetThreadName("blinker");

	statusLed.run();
}

static THD_WORKING_AREA(waThreadInput, 2048);
static THD_FUNCTION(ThreadInput, arg) {

	(void) arg;
	chRegSetThreadName("console");

	inputParser.run();
}

void transmitData() {
	console.cyclicSend();
}

static THD_WORKING_AREA(waThreadControl, 768);
static THD_FUNCTION(ThreadControl, arg) {

	(void) arg;
	chRegSetThreadName("control");

	control.callback = transmitData;
	control.start();
}

static THD_WORKING_AREA(waThreadBeacon, 768);
static THD_FUNCTION(ThreadBeacon, arg) {
	Match *match = (Match*) arg;
	chRegSetThreadName("beacon");

	extern Beacon2018 beacon;
	MoveCommand command = navigation.getCommand();
	while (command != MoveCommand::Terminated && command != MoveCommand::EmergencyStop) {
		// pas d'interrogation en mode test pour ne pas interférer avec les tests
		if (!console.isInTest()) {
			beacon.updateStatus();
			if (beacon.isAdvancedMode()) {
				beacon.updateMeasures();
			}
		}
		MoveError error = control.getError();
		if (error == MoveError::Overrun) {
			break;
		}
		chThdSleepMilliseconds(20);
		command = navigation.getCommand();
	}
	match->terminate();
}

/*
 * Application entry point.
 */
int main(void) {
	/*
	 * System initializations.
	 * - HAL initialization, this also initializes the configured device drivers
	 *   and performs the board-specific initializations.
	 * - Kernel initialization, the main() function becomes a thread and the
	 *   RTOS is active.
	 */
	halInit();
	halCommunityInit();
	chSysInit();

	AnalogReader &reader = AnalogReader::getInstance();
	reader.init();
	reader.start();

	Initialization init;
	init.initializePeripherals();

	chThdCreateStatic(waThreadBlinker, sizeof(waThreadBlinker), LOWPRIO, ThreadBlinker, NULL);

	statusLed.blink(500, 500, 3, true);
	statusLed.blinkContinuously(50, 500);

	chThdCreateStatic(waThreadInput, sizeof(waThreadInput), NORMALPRIO, ThreadInput, NULL);
	chThdCreateStatic(waThreadControl, sizeof(waThreadControl), HIGHPRIO, ThreadControl, NULL);

	init.executeTests();
	if (navigation.getCommand() == MoveCommand::EmergencyStop) {
		navigation.reset();
	}

	control.simulateCoders = false;

	MatchDefault match(navigation);
	extern LCDScreen screen;
	screen.matchData = &match.getMatchData();

	chThdCreateStatic(waThreadBeacon, sizeof(waThreadBeacon), NORMALPRIO, ThreadBeacon, &match);

	inputParser.setMatch(match);

	match.run();

	/*
	 * Normal main() thread activity, in this demo it does nothing except
	 * sleeping in a loop and check the button state.
	 */
	while (true) {
		if (!palReadPad(GPIOC, GPIOC_BUTTON))
			test_execute((BaseSequentialStream*) &SD2);
		chThdSleepMilliseconds(500);
	}
}
