/*
 * Water2018.h
 *
 *  Created on: 21 avr. 2018
 *      Author: Emmanuel
 */

#ifndef SMALL2020_H_
#define SMALL2020_H_
#include "Arduino.h"
#include "Constants.h"
#include "Wire.h"
#include "Servo.h"
#include "CenServo_Direct.h"
#include "CenPump.h"
#include "Actuator.h"
#include "ActuatorSmall2020.h"

#include "../Commons/i2c/ActuatorSmall2020Commands.h"

#define PUMP_COUNT 				7

enum class Action {
	None, SetInitialPosition, TestAllServoAreWorking, Kill, Servo, RaiseFlags, SetArm, SetBothFrontArm, SetPump,
};

#define INPUT_BUFFER_SIZE 8

#define slave Wire

class Small2020: public Actuator {
private:
	static uint8_t slaveParametersCount;
	static uint8_t slaveParameters[INPUT_BUFFER_SIZE];
	static bool i2cAcknowledged;

	static void i2cHandleSlaveInput(int bytesCount);
	static void i2cHandleSlaveRequest();
	static void i2cHandleCommand();

	uint8_t i2cSlaveAddress;
	Color color = Color::Undefined;
	Action command = Action::None;
	char commandParameters[INPUT_BUFFER_SIZE];

	void handleAction();

	void i2cScanMasterBus();
	void i2cSendStatus();
	void initializeActuators();
	void initializeMasterI2C();
	void initializeSlaveI2C();
	bool isBusy() const;

	void printI2CAddress();

	// Servo
	void handleServoCommand();

	void handleCodeCommand();

	// arr�t d�finitif
	void kill();

protected:
	virtual void enterDebug();
	virtual void runInitialization();
	virtual void runStart();
	virtual void runMain();
	virtual void runDebug();
	virtual void serialParseInput(const char *buffer, int length);
	void switchI2COrSerialCommand(bool isI2C, const char *buffer, int length);

public:
	//Functionnal actions:
	void handleStartPosition();
	void handleTestAllServoAreWorking();
	void handleRaiseFlags();
	void handleSetPump();
	void handleSetArm();
	void handleSetBothFrontArm();

public:
	Small2020(uint8_t address);
};

extern Small2020 small2020;

extern CenPump pumps[];

extern CenServo_Direct servoFlags;
extern CenServo_Direct servoArm[];

extern ServoPosition servoArmPosition[];
extern ServoPosition servoFlagPosition[];


#endif /* SMALL2020_H_ */
