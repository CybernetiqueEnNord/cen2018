/*
 * Actuator 2020
 *
 *  Created on: 13/11/2019
 *      Author: Robin Ouallet
 */
#include "ActuatorSmall2020.h"

Small2020 small2020(I2C_ADDRESS_ACTUATORSMALL2020);

#define PIN_SERVO_FLAG 			2
#define PIN_SERVO_ARM_START 	3
#define SERVO_ARM_COUNT 		5

#define PIN_SERVO_PUMP_START 	7


//Rentr�  millieux  Sorti , working2
CenServo_Direct servoFlags(PIN_SERVO_FLAG, { 97, 120, 174 });
ServoPosition servoFlagPosition[1];

//Rentr�  millieux  Sorti , working2
CenServo_Direct servoLateralArmLeft(PIN_SERVO_ARM_START, { 73, 30, 25, 69, 80 }); // Down, Center, Rentr�, attrapage rack, depose
CenServo_Direct servoLateralArmRight(PIN_SERVO_ARM_START + 1, { 67, 120, 130, 68, 55 }); // Down (manche a air), Center, Rentr� , attrapage rack, depose
CenServo_Direct servoFrontRight(PIN_SERVO_ARM_START + 2, { 20, 150, 170 }); // Down, Center, Rentr�
CenServo_Direct servoFrontLeft(PIN_SERVO_ARM_START + 3, { 157, 40, 30 }); // Down, Center, Rentr�
CenServo_Direct servoRearLifter(A0, { 15, 90, 180, 1 }); // Down, Center, Rentr�, d�pose  //14 is A0

CenPump pumps[PUMP_COUNT] = { { PIN_SERVO_PUMP_START }, { PIN_SERVO_PUMP_START + 1 }, { PIN_SERVO_PUMP_START + 4 }, { PIN_SERVO_PUMP_START + 5 }, { PIN_SERVO_PUMP_START + 3 }, { PIN_SERVO_PUMP_START + 6 }, { PIN_SERVO_PUMP_START + 2 } };


CenServo_Direct servoArm[] = { servoLateralArmLeft, servoLateralArmRight, servoFrontRight, servoFrontLeft, servoRearLifter };
ServoPosition servoArmPosition[SERVO_ARM_COUNT];

void setupPins() {
}
void setup() {
	setupPins();

	Serial.begin(115200);
	Serial.setTimeout(3);

	small2020.init(); // output version string on serial.
	small2020.handleStartPosition();
}

void loop() {
	small2020.loop();
}

