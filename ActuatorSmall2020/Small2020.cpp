#include "Small2020.h"

#include <avr/pgmspace.h>
#include <Arduino.h>
#include <HardwareSerial.h>
#include <stdint.h>
#include <WString.h>

#define log(x) Serial.println(x)

PROGMEM const char *const Actuator::versionString = "vSmall2020 " __DATE__ " - " __TIME__;

Small2020::Small2020(uint8_t address) :
		Actuator(), i2cSlaveAddress(address) {
}

uint8_t Small2020::slaveParametersCount = 0;

uint8_t Small2020::slaveParameters[8];

bool Small2020::i2cAcknowledged = false;

void Small2020::initializeSlaveI2C() {
	slave.begin(i2cSlaveAddress);
	slave.onRequest(i2cHandleSlaveRequest);
	slave.onReceive(i2cHandleSlaveInput);
}

void Small2020::initializeActuators() {
}

void Small2020::runInitialization() {
	Actuator::runInitialization();
	initializeSlaveI2C();
	initializeActuators();
}

void Small2020::runStart() {
	Actuator::runStart();
}

void Small2020::runMain() {
	Actuator::runMain();
	handleAction();
}

void Small2020::runDebug() {
	Actuator::runDebug();
	uint8_t count = slaveParametersCount;
	if (count > 0) {
		Serial.print(F("i2c buffer: "));
		for (uint8_t i = 0; i < count; i++) {
			Serial.print((char) slaveParameters[i]);
		}
		Serial.println();
		slaveParametersCount = 0;
	}
	handleAction();
}

void Small2020::i2cHandleSlaveInput(int bytesCount) {
	unsigned int i = 0;
	while (slave.available() > 0 && i < min(ARRAY_SIZE(slaveParameters), (unsigned ) bytesCount)) {
		char c = slave.read();
		slaveParameters[i++] = c;
	}
	slaveParametersCount = i;
}

void Small2020::i2cHandleSlaveRequest() {
	if (slaveParametersCount != 0) {
		i2cHandleCommand();
		slaveParametersCount = 0;
	}
}

bool Small2020::isBusy() const {
	return command != Action::None;
}

void Small2020::i2cSendStatus() {
	i2cAcknowledged = true;
	slave.write(command == Action::None ? I2C_STATUS_READY : I2C_STATUS_BUSY);
}

void Small2020::i2cHandleCommand() {
	if (slaveParametersCount == 0) {
		small2020.i2cSendStatus();
	} else if (slaveParameters[0] == I2C_COMMAND_STATUS) {
		small2020.i2cSendStatus();
	} else {
		if (small2020.isBusy()) {
			return;
		}
		i2cAcknowledged = false;
		small2020.switchI2COrSerialCommand(true, slaveParameters, slaveParametersCount);
	}
}

void Small2020::enterDebug() {
	Actuator::enterDebug();
}

void Small2020::printI2CAddress() {
	Serial.print('a');
	serialPrintHex(i2cSlaveAddress);
}

void Small2020::serialParseInput(const char *buffer, int length) {
	switchI2COrSerialCommand(false, buffer, length);
}



void Small2020::switchI2COrSerialCommand(bool isI2C, const char *buffer, int length) {
	switch (buffer[0]) {
		case COMMAND_SERIAL_I2C_ADDRESS:
			printI2CAddress();
			break;

		case I2C_COMMAND_SET_COLOR_BLUE:
			small2020.color = Color::Blue;
			small2020.command = Action::SetInitialPosition;
			break;
		case I2C_COMMAND_SET_COLOR_YELLOW:
			small2020.color = Color::Yellow;
			small2020.command = Action::SetInitialPosition;
			break;

		case I2C_COMMAND_SERVO:
			small2020.command = Action::Servo;
			if (isI2C) {
				strncpy(small2020.commandParameters, (const char*) (small2020.slaveParameters + 1), ARRAY_SIZE(small2020.commandParameters));
				small2020.commandParameters[small2020.slaveParametersCount - 1] = 0;
			} else {
				strncpy(small2020.commandParameters, buffer + 1, ARRAY_SIZE(small2020.commandParameters));
			}
			break;

		case COMMAND_SET_PUMP:
			small2020.command = Action::SetPump;
			strncpy(small2020.commandParameters, buffer + 1, ARRAY_SIZE(small2020.commandParameters));
			break;

		case COMMAND_SET_ARM:
			small2020.command = Action::SetArm;
			strncpy(small2020.commandParameters, buffer + 1, ARRAY_SIZE(small2020.commandParameters));
			break;

		case COMMAND_SET_BOTH_FRONT_ARM:
			small2020.command = Action::SetBothFrontArm;
			strncpy(small2020.commandParameters, buffer + 1, ARRAY_SIZE(small2020.commandParameters));
			break;

		case I2C_COMMAND_START_POSITION:
			small2020.command = Action::SetInitialPosition;
			break;

		case I2C_COMMAND_TEST_ALL_SERVO_ARE_WORKING:
			small2020.command = Action::TestAllServoAreWorking;
			break;

		case COMMAND_FLAGS:
			small2020.command = Action::RaiseFlags;
			break;

		case I2C_COMMAND_KILL:
			small2020.command = Action::Kill;
			break;

		default:
			if (!isI2C) {
				Actuator::serialParseInput(buffer, length);
			}
			break;
	}
}

void Small2020::handleAction() {
	switch (command) {
		case Action::None:
			break;
		case Action::Kill:
			kill();
			break;
		case Action::Servo:
			handleServoCommand();
			break;
		case Action::SetPump:
			handleSetPump();
			break;
		case Action::SetArm:
			handleSetArm();
			break;
		case Action::SetBothFrontArm:
			handleSetBothFrontArm();
			break;
		case Action::SetInitialPosition:
			handleStartPosition();
			break;
		case Action::TestAllServoAreWorking:
			handleTestAllServoAreWorking();
			break;
		case Action::RaiseFlags:
			handleRaiseFlags();
			break;
	}

	command = Action::None;
	for (int i = 0; !i2cAcknowledged && i < 200; i++) {
		delay(1);
	}
}

void Small2020::kill() {

	for (int pumpNumber = 0; pumpNumber < PUMP_COUNT; pumpNumber++) {
		pumps[pumpNumber].setPump(false);
	}
	servoFlags.setPosition(ServoPosition::Deactivated);
	servoArm[0].setPosition(ServoPosition::Deactivated);
	servoArm[1].setPosition(ServoPosition::Deactivated);
	servoArm[2].setPosition(ServoPosition::Deactivated);
	servoArm[3].setPosition(ServoPosition::Deactivated);
	servoArm[4].setPosition(ServoPosition::Deactivated);

	slave.end();
}

//Unitzary command to a Servo, use only in integration.
void Small2020::handleServoCommand() {
	char *tok = strtok(commandParameters, ";\n");
	int index = 0;
	int servoIndex = 0, servoType = 0;
	int servoPosition = 0;
	while (tok) {
		switch (index) {
			case 0:
				servoType = atoi(tok);
				break;

			case 1:
				servoIndex = atoi(tok);
				break;

			case 2:
				servoPosition = atoi(tok);
				break;

			case 3:
				goto done;
				break;
		}
		index++;
		tok = strtok(NULL, ";\n");
	}
	done: switch (servoType) {
		case 0:
			switch (servoIndex) {
				case 0:
					servoFlags.setPositionRaw(servoPosition);
					break;
			}
			break;
		case 1:
			servoArm[servoIndex].setPositionRaw(servoPosition);
			break;
	}
}



void Small2020::handleStartPosition() {

	//servoLateralArmLeft, servoLateralArmRight, servoFrontArm, servoRearArm
	for (int pumpNumber = 0; pumpNumber < PUMP_COUNT; pumpNumber++) {
		pumps[pumpNumber].setPump(false);
	}

	servoArm[0].setPosition(ServoPosition::Up);
	servoArm[1].setPosition(ServoPosition::Up);
	servoArm[2].setPosition(ServoPosition::Up);
	servoArm[3].setPosition(ServoPosition::Up);
	servoArm[4].setPosition(ServoPosition::Up);

	servoFlags.setPosition(ServoPosition::Down);
	delay(1000);
	servoFlags.setPosition(ServoPosition::Deactivated);

	servoArm[0].setPosition(ServoPosition::Deactivated);
	servoArm[1].setPosition(ServoPosition::Deactivated);
	servoArm[2].setPosition(ServoPosition::Deactivated);
	servoArm[3].setPosition(ServoPosition::Deactivated);
	servoArm[4].setPosition(ServoPosition::Deactivated);



}

void Small2020::handleTestAllServoAreWorking() {
	//servoLateralArmLeft, servoLateralArmRight, servoFrontArm, servoRearArm
	servoArm[0].setPosition(ServoPosition::Down);
	servoArm[1].setPosition(ServoPosition::Down);
	servoArm[2].setPosition(ServoPosition::Down);
	servoArm[3].setPosition(ServoPosition::Down);
	servoArm[4].setPosition(ServoPosition::Down);
	handleRaiseFlags();

	for (int pumpNumber = 0; pumpNumber < PUMP_COUNT; pumpNumber++)
	{
		pumps[pumpNumber].setPump(true);
		delay(200);
		pumps[pumpNumber].setPump(false);
		delay(100);
	}
	pumps[0].setPump(true);
	delay(200);
	pumps[0].setPump(false);
	delay(100);

	handleStartPosition();
}

void Small2020::handleRaiseFlags() {
//2time in case of lock.
	servoFlags.setPosition(ServoPosition::Up);
	delay(1000);
	servoFlags.setPosition(ServoPosition::Deactivated);
	delay(500);
	servoFlags.setPosition(ServoPosition::Up);
	delay(1000);
	servoFlags.setPosition(ServoPosition::Deactivated);
	delay(500);
}


void Small2020::handleSetPump() {
	int pumpNumber = (commandParameters[0] - '0');
	bool requestedStatus = (commandParameters[1] - '0') == 1;
	pumps[pumpNumber].setPump(requestedStatus);
}

void Small2020::handleSetArm() {
	int armNumber = (commandParameters[0] - '0');
	int requestPosition = (commandParameters[1] - '0');
	ServoPosition position;
	//Ask Manu help to cast
	switch (requestPosition) {
		case 0:
			position = ServoPosition::Deactivated;
			break;
		case 1:
			position = ServoPosition::Center;
			break;
		case 2:
			position = ServoPosition::Up;
			break;
		case 3:
			position = ServoPosition::Down;
			break;
		case 4:
			position = ServoPosition::Working;
			break;
		case 5:
			position = ServoPosition::Depose;
			break;
		default:
			position = ServoPosition::Deactivated;
	}
	servoArm[armNumber].setPosition(position);
	delay(600);
}

void Small2020::handleSetBothFrontArm() {
	int requestPosition = (commandParameters[0] - '0');
	ServoPosition position;
	//Ask Manu help to cast
	switch (requestPosition) {
		case 0:
			position = ServoPosition::Deactivated;
			break;
		case 1:
			position = ServoPosition::Center;
			break;
		case 2:
			position = ServoPosition::Up;
			break;
		case 3:
			position = ServoPosition::Down;
			break;
		case 4:
			position = ServoPosition::Working;
			break;
		case 5:
			position = ServoPosition::Depose;
			break;
		default:
			position = ServoPosition::Deactivated;
	}
	servoArm[2].setPosition(position);
	servoArm[3].setPosition(position);
	delay(600);
}







