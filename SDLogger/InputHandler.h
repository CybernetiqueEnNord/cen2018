#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H

#include "CenBLEServer.h"
#include "CenUdpNode.h"
#include "CenWebSocketServer.h"
#include "Logger.h"
#include "Pointer.h"

class InputHandlerEvents {
public:
	virtual void transmitCallback(bool value) = 0;
};

class InputHandler {
private:
	Logger &logger;
	CenWebSocketServer &webSocket;
	CenBLEServer &bleServer;
	CenUdpNode &hub;
	Pointer<InputHandlerEvents> events;
	char buffer[512];
	unsigned int position = 0;

	void handleBLEData(const char * data, size_t length);
	void handleData(const char * data, size_t length);
	void insertChar(char c);
	void insertTimestamp();
	void logData(const char * data, size_t length);
	void transmit(bool value);
	void transmitBLEData(const char * data, size_t length);
	void transmitData(const char * data, size_t length);

public:
	InputHandler(Logger &logger, CenWebSocketServer &webSocket, CenBLEServer &bleServer, CenUdpNode &hub);
	void initialize();
	void loop();
	void setEvents(InputHandlerEvents &events);
};

#endif
