#include "CenWebServer.h"

#include "arrays.h"
#include "CenDebug.h"

#define ARG_PATH "path"
#define ARG_BODY "plain"

CenWebServer::CenWebServer(SdFat &sd) :
		server(80), sd(sd) {
}

void CenWebServer::fail(const String &message) {
	error(500, message);
}

void CenWebServer::error(int code, const String &message) {
	server.send(code, F("text/plain"), message + "\r\n");
}

void CenWebServer::initialize() {
	server.on(F("/console"), HTTP_POST, std::bind(&CenWebServer::onConsoleInput, this));
	server.on(F("/list"), HTTP_GET, std::bind(&CenWebServer::onListDirectory, this));
	server.on(F("/file"), HTTP_GET, std::bind(&CenWebServer::onGetFile, this));
	server.on(F("/"), HTTP_GET, std::bind(&CenWebServer::onWebRedirect, this));
	server.onNotFound(std::bind(&CenWebServer::onNotFound, this));
	server.begin();
}

void CenWebServer::onConsoleInput() {
	if (server.hasArg(ARG_BODY)) {
		const String &body = server.arg(ARG_BODY);
		if (input.isValid()) {
			input->inputCallback(body);
		}
	}
	server.send(200);
}

void CenWebServer::onGetFile() {
	if (!server.hasArg(ARG_PATH)) {
		return fail(F("missing path"));
	}
	const String &path = server.arg(ARG_PATH);
	if (loadFromSdCard(path)) {
		return;
	}
	fail(F("invalid path"));
}

void CenWebServer::onNotFound() {
	const String &uri = server.uri();
	String path = "/www" + uri;
	if (loadFromSdCard(path)) {
		return;
	}

	int args = server.args();
	String message = F("NOT FOUND\n");
	message += F("\nURI: ");
	message += uri;
	message += F("\nArguments: ");
	message += args;
	for (uint8_t i = 0; i < args; i++) {
		message += F("\n name: ");
		message += server.argName(i);
		message += F("\n value: ");
		message += server.arg(i);
	}
	message += "\r\n";
	server.send(404, F("text/plain"), message);
	DBG(message);
}

void CenWebServer::onWebRedirect() {
	server.sendHeader(F("Location"), F("/index.html"), true);
	server.send(302, F("text/plain"), "");
}

void CenWebServer::onListDirectory() {
	if (!server.hasArg(ARG_PATH)) {
		return fail(F("missing path"));
	}
	String path = server.arg(ARG_PATH);
	if (path != "/" && !sd.exists(path.c_str())) {
		return fail(F("bad path"));
	}
	SdFile dir(path.c_str(), O_READ);
	if (!dir.isDir()) {
		dir.close();
		return fail(F("invalid directory"));
	}
	dir.rewind();

	server.setContentLength(CONTENT_LENGTH_UNKNOWN);
	server.send(200, "text/json", "");
	server.sendContent("[");
	SdFile entry;
	char filename[256];
	for (int cnt = 0; true; ++cnt) {
		if (!entry.openNext(&dir)) {
			break;
		}

		if (!entry.getName(filename, ARRAY_SIZE(filename))) {
			continue;
		}

		String output;
		if (cnt > 0) {
			output = ',';
		}

		output += "{\"type\":";
		output += entry.isDir() ? "1" : "0";
		output += ",\"name\":\"";
		output += filename;
		output += "\",\"path\":\"";
		output += path;
		output += "\"";
		output += "}";
		server.sendContent(output);
		entry.close();
	}
	server.sendContent("]");
	dir.close();
}

bool CenWebServer::loadFromSdCard(String path) {
	String dataType = F("text/plain");
	if (path.endsWith("/")) {
		path += "index.html";
	}

	if (path.endsWith(F(".src"))) {
		path = path.substring(0, path.lastIndexOf("."));
	} else if (path.endsWith(F(".htm")) || path.endsWith(F(".html"))) {
		dataType = F("text/html");
	} else if (path.endsWith(F(".css"))) {
		dataType = F("text/css");
	} else if (path.endsWith(F(".js"))) {
		dataType = F("application/javascript");
	} else if (path.endsWith(F(".png"))) {
		dataType = F("image/png");
	} else if (path.endsWith(F(".svg"))) {
		dataType = F("image/svg+xml");
	} else if (path.endsWith(F(".gif"))) {
		dataType = F("image/gif");
	} else if (path.endsWith(F(".jpg"))) {
		dataType = F("image/jpeg");
	} else if (path.endsWith(F(".ico"))) {
		dataType = F("image/x-icon");
	} else if (path.endsWith(F(".xml"))) {
		dataType = F("text/xml");
	} else if (path.endsWith(F(".pdf"))) {
		dataType = F("application/pdf");
	} else if (path.endsWith(F(".zip"))) {
		dataType = F("application/zip");
	}

	SdFile dataFile;
	String gzPath = path + ".gz";
	bool gz = sd.exists(gzPath.c_str());
	if (gz) {
		path = gzPath;
	}
	bool b = dataFile.open(path.c_str(), O_READ);
	if (dataFile.isDir()) {
		path += F("/index.html");
		dataType = F("text/html");
		dataFile.close();
		b = dataFile.open(path.c_str(), O_READ);
	}

	if (!b) {
		return false;
	}

	if (server.hasArg(F("download"))) {
		dataType = F("application/octet-stream");
	}

	size_t fileSize = dataFile.fileSize();
	server.setContentLength(fileSize);
	server.sendHeader(F("Cache-Control"), F("max-age=86400"));
	if (gz) {
		server.sendHeader(F("Content-Encoding"), F("gzip"));
	}
	server.send(200, dataType, String(""));
	char buffer[512];
	WiFiClient client = server.client();
	while (fileSize > 0) {
		int l = fileSize > ARRAY_SIZE(buffer) ? ARRAY_SIZE(buffer) : fileSize;
		if (dataFile.read(buffer, l) < l) {
			DBG(F("sent less data than expected"));
			break;
		}
		client.write(buffer, l);
		fileSize -= l;
	}

	dataFile.close();
	return true;
}

void CenWebServer::loop() {
	server.handleClient();
}

void CenWebServer::setInput(WebServerInput &input) {
	this->input = Pointer<WebServerInput>(&input);
}
