#ifndef LOGGER_H
#define LOGGER_H

#include <Arduino.h>
#include <time.h>

enum class LogLevel {
	Debug, Info, Warning, Error
};

class Logger {
public:
	Logger();
	virtual void logData(const char *data, int length) = 0;
	virtual void logDebug(LogLevel level, const String &text, time_t timestamp = 0) = 0;
};

#endif
