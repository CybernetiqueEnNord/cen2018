#ifndef INIFILE_H
#define INIFILE_H

#include "SdFat.h"

class INIFileReader {
public:
	virtual void parseCallback(const String &section, const String &key, const String &value) = 0;
};

class INIFile {
private:
	File &file;

public:
	INIFile(File &file);
	virtual ~INIFile();
	void parse(INIFileReader &reader);
};

#endif

