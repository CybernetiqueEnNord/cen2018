#ifndef CENWEBSERVER_H
#define CENWEBSERVER_H

#include "Pointer.h"
#include <SdFat.h>
#include <WebServer.h>

class WebServerInput {
public:
	virtual void inputCallback(const String &data) = 0;
};

class CenWebServer {
private:
	WebServer server;
	Pointer<WebServerInput> input;
	SdFat &sd;

	void error(int code, const String &message);
	void fail(const String &message);
	bool loadFromSdCard(String path);
	void onConsoleInput();
	void onGetFile();
	void onListDirectory();
	void onNotFound();
	void onWebRedirect();

public:
	CenWebServer(SdFat &sd);
	void initialize();
	void loop();
	void setInput(WebServerInput &input);
};

#endif // CENWEBSERVER_H
