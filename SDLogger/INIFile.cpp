#include "INIFile.h"

INIFile::INIFile(File &file) :
		file(file) {
}

INIFile::~INIFile() {
}

void INIFile::parse(INIFileReader &reader) {
	String section = "";
	String key = "";
	String value = "";
	while (file.available() > 0) {
		String line = file.readStringUntil('\n');

		// Commentaires
		if (line.length() > 0) {
			char c = line.charAt(0);
			if (c == ';' || c == '#') {
				continue;
			}
		}

		// Section
		int start = line.indexOf('[');
		int end = line.indexOf(']');
		if (start == 0 && end > 0) {
			section = line.substring(start + 1, end);
			continue;
		}

		// Clef / valeur
		int pos = line.indexOf('=');
		if (pos < 0) {
			key = line;
			key.trim();
			value = "";
		} else {
			key = line.substring(0, pos);
			value = line.substring(pos + 1);
			value.trim();
		}
		if (key.length() > 0) {
			reader.parseCallback(section, key, value);
		}
	}
}

