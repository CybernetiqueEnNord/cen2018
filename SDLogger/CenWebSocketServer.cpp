/*
 * CenWebSocketServer.cpp
 *
 *  Created on: 5 oct. 2019
 *      Author: emmanuel
 */

#include "CenWebSocketServer.h"

#include "arrays.h"
#include "CenDebug.h"
#include <functional>

CenWebSocketServer::CenWebSocketServer(uint16_t port) :
		server(port) {
}

CenWebSocketServer::~CenWebSocketServer() {
	server.close();
}

void CenWebSocketServer::initialize() {
	using namespace std::placeholders;
	server.onEvent(std::bind(&CenWebSocketServer::eventHandler, this, _1, _2, _3, _4));
	server.begin();
}

void CenWebSocketServer::loop() {
	server.loop();
}

void CenWebSocketServer::eventHandler(uint8_t clientId, WStype_t type, uint8_t *payload, size_t length) {
	switch (type) {
		case WStype_t::WStype_CONNECTED:
			DBG_skv(F("websocket"), F("connected"), clientId);
			setConnected(clientId, true);
			connected = true;
			break;

		case WStype_t::WStype_DISCONNECTED:
			DBG_skv(F("websocket"), F("disconnected"), clientId);
			setConnected(clientId, false);
			connected = server.connectedClients(false) > 0;
			break;

		case WStype_t::WStype_TEXT:
			DBG_skv(F("websocket"), F("input"), clientId);
			if (handler.isValid()) {
				handler->inputCallback(clientId, payload, length);
			}
			break;

		default:
			break;
	}
}

void CenWebSocketServer::setEventHandler(WebSocketServerEventHandler &handler) {
	this->handler = Pointer<WebSocketServerEventHandler>(&handler);
}

bool CenWebSocketServer::send(const char *data, size_t length) {
	if (connected) {
		bool success = false;
		for (uint8_t i = 0; i < 255; i++) {
			if (!isConnected(i)) {
				continue;
			}
			bool b = server.sendTXT(i, (const uint8_t *) data, length);
			DBG_skv(F("websocket"), b ? F("success") : F("error"), i);
			success |= b;
		}
		return success;
	} else {
		DBG(F("not connected"));
		return false;
	}
}

bool CenWebSocketServer::isConnected(uint8_t clientId) const {
	unsigned int index;
	uint32_t mask;
	getIndexAndMask(clientId, index, mask);
	bool result = (clients[index] & mask) != 0;
	return result;
}

void CenWebSocketServer::setConnected(uint8_t clientId, bool connected) {
	unsigned int index;
	uint32_t mask;
	getIndexAndMask(clientId, index, mask);
	if (connected) {
		clients[index] |= mask;
	} else {
		clients[index] &= ~mask;
	}
}

void CenWebSocketServer::getIndexAndMask(uint8_t clientId, unsigned int &index, uint32_t &mask) const {
	index = clientId / 32;
	mask = 1 << (clientId & 31);
}
