#include <Arduino.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <WebSockets.h>
#include <HTTPClient.h>
//#include <LLMNR.h>
#include <ESPmDNS.h>
#include <DS1307RTC.h>
#include "CenDebug.h"
#include "INIFile.h"
#include "InputHandler.h"
#include "SdFat.h"
#include "SDLogger.h"
#include <SPI.h>
#include <lwip/apps/sntp.h>
#include "time.h"
#include <Wire.h>

#include "CenWebServer.h"
#include "CenWebSocketServer.h"
#include "CenBLEServer.h"
#include "arrays.h"

#include "CenUdpNode.h"

#ifdef CENOTA
#include "CenOTA.h"
CenOTA ota;
#endif

// Horodatage de référence pour déterminer la validité (01/01/2000)
#define TIMESTAMP_REF 946684800

// Vitesse du bus SPI
#define SPI_SPEED SPI_SIXTEENTH_SPEED

WiFiMulti wifiMulti;
String hostname;
String sntp;
String bleBaseUUID;
String bleDeviceName = "CenLogger";
uint16_t webSocketPort = 8080;
String dataLogName = "/log.txt";
String debugLogName = "/debug.txt";
SdFat SD;
SDLogger logger(SD, dataLogName, debugLogName);
CenWebServer webServer(SD);
CenWebSocketServer webSocketServer(webSocketPort);
IPAddress hubAddress;
IPAddress ipLocal;
IPAddress ipGateway;
IPAddress ipMask;
uint16_t hubPort = 9999;
WiFiUDP udp;
CenUdpNode hub(udp, hubAddress, hubPort);
CenBLEServer bleServer;
InputHandler inputHandler(logger, webSocketServer, bleServer, hub);

void setLed(bool value);

class InputHandlerEventsImpl : public InputHandlerEvents
{
public:
	virtual void transmitCallback(bool value) override
	{
		setLed(value);
	}
};

InputHandlerEventsImpl events;

class InputReader : public WebServerInput
{
public:
	virtual void inputCallback(const String &data) override
	{
		Serial.print(data);
	}
};

InputReader inputReader;

class WebSocketEventHandler : public WebSocketServerEventHandler
{
public:
	virtual void inputCallback(uint8_t clientId, const uint8_t *data, size_t length) override
	{
		Serial.write(data, length);
	}
};

WebSocketEventHandler webSocketEventHandler;

class BLEEventHandler : public CenBLEServerEventHandler
{
public:
	virtual void inputCallback(const uint8_t *data, size_t length) override
	{
		Serial.write('x');
		Serial.write(data, length);
		Serial.write('\n');
		std::string s = ">x" + std::string((const char *)data, length) + '\n';
		logger.logData(s.c_str(), s.length());
	}
};

BLEEventHandler bleEventHandler;

class WifiInputHandler : public CenUdpNodeCallback
{
protected:
	virtual void handleInput(const char *data, size_t length) override
	{
		Serial.write('x');
		Serial.write((const uint8_t *)data, length);
		Serial.write('\n');
		std::string s = ">x" + std::string(data, length) + '\n';
		logger.logData(s.c_str(), s.length());
	}
} wifiInputHandler;

class ConfigReader : public INIFileReader
{
private:
	String ssid;

public:
	virtual void parseCallback(const String &section, const String &key, const String &value)
	{
		DBG_skv(section, key, value);
		bool b = false;
		if (section == "wifi")
		{
			if (key == "ssid")
			{
				ssid = value;
				b = true;
			}
			else if (key == "key")
			{
				b = wifiMulti.addAP(ssid.c_str(), value.c_str());
			}
			else if (key == "ip")
			{
				b = ipLocal.fromString(value);
			}
			else if (key == "gateway")
			{
				b = ipGateway.fromString(value);
			}
			else if (key == "netmask")
			{
				b = ipMask.fromString(value);
			}
		}
		else if (section == "client")
		{
			if (key == "hostname")
			{
				hostname = value;
				b = WiFi.setHostname(value.c_str());
			}
		}
		else if (section == "websocket")
		{
			if (key == "port")
			{
				webSocketPort = value.toInt();
				webSocketServer = CenWebSocketServer(webSocketPort);
				b = true;
			}
		}
		else if (section == "time")
		{
			if (key == "sntp")
			{
				sntp = value;
				sntp_setservername(0, (char *)sntp.c_str());
				b = true;
			}
			else if (key == "timezone")
			{
				setenv("TZ", value.c_str(), 1);
				b = true;
			}
		}
		else if (section == "ble")
		{
			if (key == "uuid")
			{
				bleBaseUUID = value;
				b = true;
			}
			else if (key == "devicename")
			{
				bleDeviceName = value;
				b = true;
			}
		}
		else if (section == "node")
		{
			if (key == "address")
			{
				hubAddress.fromString(value);
				hub = CenUdpNode(udp, hubAddress, hubPort);
				b = true;
			}
			else if (key == "port")
			{
				hubPort = value.toInt();
				hub = CenUdpNode(udp, hubAddress, hubPort);
				b = true;
			}
		}
		DBG_b(b);
	}
};

void setupSNTP()
{
	sntp_init();
}

void setupBLE()
{
	if (bleBaseUUID.isEmpty())
	{
		DBG(F("BLE not configured"));
	}
	else
	{
		bleServer.setEventHandler(bleEventHandler);
		bleServer.initialize(bleDeviceName, bleBaseUUID);
	}
}

void setupWifiNode()
{
	IPAddress ipDefault;
	if (ipLocal != ipDefault)
	{
		WiFi.config(ipLocal, ipGateway, ipMask);
	}
	udp.begin(hubPort);
	hub.setCallback(&wifiInputHandler);
}

void setupMDNS()
{
	if (hostname.length() == 0)
	{
		return;
	}
	if (!MDNS.begin(hostname.c_str()))
	{
		DBG(F("MDNS error"));
		return;
	}
	MDNS.setInstanceName(hostname);
	MDNS.addService("http", "tcp", 80);
	MDNS.addService("ws", "tcp", webSocketPort);
}

void setupLLMNR()
{
	if (hostname.length() == 0)
	{
		return;
	}
	//LLMNR.begin(hostname.c_str());
}

String getTimestampText(const time_t &time)
{
	char buf[16];
	tm *tm = localtime(&time);
	sprintf(buf, "%.4d%.2d%.2d-%.2d%.2d%.2d", 1900 + tm->tm_year, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
	String result(buf);
	return result;
}

void updateTimestamp(const time_t &timestamp)
{
	if (timestamp < TIMESTAMP_REF)
	{
		return;
	}

	String text = getTimestampText(timestamp);
	String dataLogPath = "/log-" + text + ".txt";
	String debugLogPath = "/debug-" + text + ".txt";
	logger.rename(dataLogPath, debugLogPath);
	DBG_skv("log", "txt", dataLogPath);
	DBG_skv("debug", "txt", debugLogPath);
}

void setupClock()
{
	time_t timestamp = DS1307RTC::get();
	bool present = DS1307RTC::chipPresent();
	DBG_skv("DS1307RTC", "present", present ? "true" : "false");
	if (timestamp > TIMESTAMP_REF)
	{
		DBG_skv("DS1307RTC", "timestamp", timestamp);
		updateTimestamp(timestamp);
	}
	else
	{
		DBG_skv("DS1307RTC", "timestamp", "invalid");
	}
}

void setLed(bool value)
{
	// HIGH = led allumée, LOW = éteinte
	digitalWrite(LED_BUILTIN, value ? HIGH : LOW);
}

bool loadConfiguration()
{
	File file = SD.open("/config.ini");
	if (!file)
	{
		DBG("no SD card");
		return false;
	}
	INIFile ini(file);
	ConfigReader reader;
	ini.parse(reader);
	file.close();
	return true;
}

void handleSNTP()
{
	static bool sntp = false;
	if (!sntp)
	{
		time_t timestamp = time(NULL);
		if (timestamp > TIMESTAMP_REF)
		{
			DBG_skv("sntp", "timestamp", timestamp);
			sntp = true;

			DS1307RTC::set(timestamp);
			updateTimestamp(timestamp);
		}
	}
}

void runWifi(void *data)
{
	DBG(F("task started"));
	DBG_skv(F("wifi"), F("core"), xPortGetCoreID());
	while (true)
	{
		static bool connected = false;
		bool lastConnected = connected;

#ifdef CENOTA
		ota.handle();
#endif

		int status = wifiMulti.run();

		switch (status)
		{
		case WL_CONNECTED:
			connected = true;
			handleSNTP();
			break;

		default:
			connected = false;
			break;
		}

		if (connected != lastConnected)
		{
			DBG(connected ? F("CONNECTED") : F("DISCONNECTED"));
			if (connected)
			{
				DBG_skv("wifi", "SSID", WiFi.SSID());
				DBG_skv("wifi", "IP", WiFi.localIP().toString());
			}
			if (!connected)
			{
				WiFi.reconnect();
			}
		}
	}
	vTaskDelete(NULL);
}

void setup()
{
	Serial.begin(115200);
#ifdef CENDEBUG
	Serial.setDebugOutput(true);
#endif

#ifdef CENOTA
	ota.setupOTA(173);
#endif

	//pinMode(MISO, INPUT_PULLUP);
	//pinMode(MOSI, INPUT_PULLUP);
	pinMode(LED_BUILTIN, OUTPUT);
	setLed(false);
	DBG("started");

	Wire.begin();

	if (!SD.begin(SS, SPI_SPEED))
	{
		DBG("SD init failed");
	}
	WiFi.persistent(false);
	WiFi.mode(WIFI_STA);

	// attente de la carte SD
	while (!loadConfiguration())
	{
		setLed(true);
		delay(10);
		setLed(false);
		delay(500);
		SD.begin(SS, SPI_SPEED);
	}

	// Horloge
	setupClock();

	// SNTP
	setupSNTP();

	// mDNS
	setupMDNS();

	// LLMNR
	setupLLMNR();

	// activation de la journalisation
	logger.rotate();
	logger.setEnabled(true);

	// server BLE
	setupBLE();

	// noeud udp wifi
	setupWifiNode();

	// setup WIFI
	TaskHandle_t taskWifi = NULL;
	xTaskCreate(runWifi, "wifi", 4096, nullptr, 1, &taskWifi);

	// serveur web
	webServer.setInput(inputReader);
	webServer.initialize();

	// serveur WebSocket
	webSocketServer.setEventHandler(webSocketEventHandler);
	webSocketServer.initialize();

	// gestion de l'entrée série
	inputHandler.setEvents(events);
	inputHandler.initialize();

#ifdef CENOTA
	ota.beginOTA();
#endif

	DBG(F("setup done"));
}

#ifdef CENDEBUG
#define LOOP(x)                                   \
	{                                             \
		long start = millis();                    \
		x.loop();                                 \
		long end = millis();                      \
		if (end - start > 500)                    \
		{                                         \
			DBG_skv(F("loop"), F("timeout"), #x); \
		}                                         \
	}
#else
#define LOOP(x) x.loop()
#endif

void loop()
{
	LOOP(inputHandler);
	LOOP(webServer);
	LOOP(webSocketServer);
	LOOP(bleServer);
	LOOP(hub);
}
