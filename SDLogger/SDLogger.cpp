#include "SDLogger.h"

#include "arrays.h"
#include "CenDebug.h"

SDLogger::SDLogger(SdFat &sdFat, const String &dataPath, const String &debugPath) :
		Logger(), sdFat(sdFat), dataPath(dataPath), debugPath(debugPath) {
	start = millis();
	mutex = xSemaphoreCreateMutex();
}

void SDLogger::writeLine(Print &stream, const char *data, int length) {
	if (length == 0) {
		return;
	}
	if (insertTimestamp) {
		stream.print(millis() - start);
		stream.write(' ');
		insertTimestamp = false;
	}
	stream.write(data, length);
}

void SDLogger::logData(const char *data, int length) {
	if (!enabled) {
		return;
	}

	if (xSemaphoreTake(mutex, portMAX_DELAY)) {
		SdFile file(dataPath.c_str(), FILE_WRITE);
		int startIndex = 0;
		/*
		// timestamp traité en amont
		for (int i = 0; i < length; i++) {
			bool isLF = data[i] == '\n';
			if (isLF) {
				writeLine(file, &data[startIndex], i - startIndex + 1);
				startIndex = i + 1;
				insertTimestamp = true;
			}
		}
		*/
		writeLine(file, &data[startIndex], length - startIndex);
		int error = file.getError();
		if (error != 0) {
			DBG_skv(F("write error"), F("error"), error);
		}
		file.close();
		xSemaphoreGive(mutex);
	}
}

void SDLogger::logDebug(LogLevel level, const String &text, time_t timestamp) {
	if (enabled) {
		if (xSemaphoreTake(mutex, portMAX_DELAY)) {
			SdFile file(debugPath.c_str(), FILE_WRITE);
			logDebug(file, level, text, timestamp);
			file.close();
			xSemaphoreGive(mutex);
		}
	} else {
		logDebug(Serial, level, text, timestamp);
	}
}

void SDLogger::logDebug(Print &stream, LogLevel level, const String &text, time_t timestamp) {
	char l;
	switch (level) {
		case LogLevel::Debug:
			l = 'D';
			break;

		case LogLevel::Info:
			l = 'I';
			break;

		case LogLevel::Warning:
			l = 'W';
			break;

		case LogLevel::Error:
			l = 'E';
			break;

		default:
			l = '?';
			break;
	}
	stream.print(l);
	stream.print('\t');
	printTimestamp(stream, timestamp);
	stream.print('\t');
	stream.println(text);
}

void SDLogger::printTimestamp(Print &stream, time_t timestamp) {
	char buf[20];
	tm *tm = localtime(&timestamp);
	sprintf(buf, "%.2d/%.2d/%.4d %.2d:%.2d:%.2d", tm->tm_mday, tm->tm_mon, 1900 + tm->tm_year, tm->tm_hour, tm->tm_min, tm->tm_sec);
	stream.write(buf, ARRAY_SIZE(buf) - 1);
}

void SDLogger::rename(const String &dataPath, const String &debugPath) {
	if (this->dataPath != dataPath) {
		renameFile(this->dataPath, dataPath);
		this->dataPath = dataPath;
	}
	if (this->debugPath != debugPath) {
		renameFile(this->debugPath, debugPath);
		this->debugPath = debugPath;
	}
}

void SDLogger::renameFile(const String &oldName, const String &newName) {
	SdFile file(oldName.c_str(), O_READ);
	file.rename(sdFat.vwd(), newName.c_str());
}

void SDLogger::setEnabled(bool value) {
	enabled = value;
}

bool SDLogger::getEnabled() const {
	return enabled;
}

void SDLogger::rotate() {
	rotate(dataPath);
	rotate(debugPath);
}

void SDLogger::rotate(String &path) {
	int pos = path.lastIndexOf('.');
	String name = pos > 0 ? path.substring(0, pos) : path;
	String ext = pos > 0 ? path.substring(pos) : "";
	int i = 1;
	String newName;
	do {
		newName = name + i + ext;
		i++;
	} while (sdFat.exists(newName.c_str()));
	sdFat.rename(path.c_str(), newName.c_str());
}

SDLogger::~SDLogger() {
}
