#include "InputHandler.h"

#include "arrays.h"
#include "CenDebug.h"
#include <HTTPClient.h>

InputHandler::InputHandler(Logger &logger, CenWebSocketServer &webSocket, CenBLEServer &bleServer, CenUdpNode &hub) :
		logger(logger), webSocket(webSocket), bleServer(bleServer), hub(hub) {
}

void InputHandler::initialize() {
}

void InputHandler::insertChar(char c) {
	buffer[position++] = c;
	if (position >= ARRAY_SIZE(buffer)) {
		handleData(buffer, position);
		position = 0;
	}
}

void InputHandler::insertTimestamp() {
	char timestamp[22];
	ltoa(millis(), timestamp, 10);
	char *c = timestamp;
	while (*c != 0) {
		insertChar(*c);
		c++;
	}
	insertChar(' ');
}

void InputHandler::loop() {
	static char bleBuffer[22];
	static unsigned int blePosition = 0;
	static long timestamp = 0;
	static bool startOfLine = true;

	bool dataAvailable = false;

	// Traitement des données en attente
	while (Serial.available() > 0) {
		dataAvailable = true;
		// Ajout dans le tampon
		if (startOfLine) {
			startOfLine = false;
			insertTimestamp();
		}
		char c = (char) Serial.read();
		buffer[position++] = c;
		bleBuffer[blePosition++] = c;
		startOfLine = c == 0xA;
		if (startOfLine || blePosition >= ARRAY_SIZE(bleBuffer)) {
			// Fin de commande ou tampon plein : transmission du tampon BLE
			handleBLEData(bleBuffer, blePosition);
			blePosition = 0;
		}
		if (position >= ARRAY_SIZE(buffer)) {
			// Tampon plein : transmission du tampon
			handleData(buffer, position);
			position = 0;
		}
	}

	if (dataAvailable) {
		// Horodatage de la dernière donnée reçue
		timestamp = millis();
	} else if (position > 0 && millis() - timestamp > 1000) {
		// Transmission si pas de nouvelles données après le laps de temps d'attente
		handleData(buffer, position);
		position = 0;
	}
}

void InputHandler::transmitData(const char *data, size_t length) {
	webSocket.send(data, length);
}

void InputHandler::logData(const char *data, size_t length) {
	logger.logData(data, length);
}

void InputHandler::handleData(const char *data, size_t length) {
	transmit(true);
	DBG(F("WS TRANSMIT"));
	transmitData(data, length);
	logData(data, length);
	DBG(F("DONE"));
	transmit(false);
}

void InputHandler::transmitBLEData(const char *data, size_t length) {
	if (length > 1 && data[0] == 'x') {
		bleServer.send((const uint8_t*) &data[1], length - 1);
		hub.send(&data[1], length - 1);
	}
}

void InputHandler::handleBLEData(const char *data, size_t length) {
	transmit(true);
	DBG(F("BLE TRANSMIT"));
	transmitBLEData(data, length);
	DBG(F("DONE"));
	transmit(false);
}

void InputHandler::setEvents(InputHandlerEvents &events) {
	this->events = Pointer<InputHandlerEvents>(&events);
}

void InputHandler::transmit(bool value) {
	if (events.isValid()) {
		events->transmitCallback(value);
	}
}
