/*
 * CenWebSocketServer.h
 *
 *  Created on: 5 oct. 2019
 *      Author: emmanuel
 */

#ifndef CENWEBSOCKETSERVER_H_
#define CENWEBSOCKETSERVER_H_

#include <WebSocketsServer.h>

#include "Pointer.h"

class WebSocketServerEventHandler {
public:
	virtual void inputCallback(uint8_t clientId, const uint8_t *data, size_t length) = 0;
};

class CenWebSocketServer {
private:
	WebSocketsServer server;
	Pointer<WebSocketServerEventHandler> handler;
	bool connected = false;
	uint32_t clients[8] = { 0 };

	void eventHandler(uint8_t clientId, WStype_t type, uint8_t * payload, size_t length);
	inline void getIndexAndMask(uint8_t clientId, unsigned int &index, uint32_t &mask) const;
	bool isConnected(uint8_t clientId) const;
	void setConnected(uint8_t clientId, bool connected);

public:
	CenWebSocketServer(uint16_t port = 8080);
	virtual ~CenWebSocketServer();
	void initialize();
	void loop();
	void setEventHandler(WebSocketServerEventHandler &handler);
	bool send(const char *data, size_t length);
};

#endif /* CENWEBSOCKETSERVER_H_ */
