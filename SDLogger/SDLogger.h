#ifndef SDLOGGER_H
#define SDLOGGER_H

#include "Logger.h"

#include "FreeRTOS.h"
#include "SdFat.h"

class SDLogger: public Logger {
private:
	SdFat &sdFat;
	String dataPath;
	String debugPath;

	SemaphoreHandle_t mutex;

	bool enabled = false;
	long start = 0;
	bool insertTimestamp = false;

	void logDebug(Print &stream, LogLevel level, const String &text, time_t timestamp);
	void printTimestamp(Print &stream, time_t timestamp);
	void renameFile(const String &oldName, const String &newName);
	void rotate(String &path);
	void writeLine(Print &stream, const char *data, int length);

public:
	SDLogger(SdFat &sdFat, const String &dataPath, const String &debugPath);
	bool getEnabled() const;
	virtual ~SDLogger();
	virtual void logData(const char *data, int length);
	virtual void logDebug(LogLevel level, const String &text, time_t timestamp = 0);
	void rename(const String &dataPath, const String &debugPath);
	void rotate();
	void setEnabled(bool value);
};

#endif
