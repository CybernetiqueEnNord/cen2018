const { app, BrowserWindow } = require('electron');
const serve = require('electron-serve');

const loadURL = serve({ directory: 'app.asar' });

let win;

(async () => {
  await app.whenReady();

  createWindow();

  await loadURL(win);
})();

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 600,
    height: 600,
    show: false,
    backgroundColor: '#ffffff',
    icon: `${__dirname}/app.asar/favicon.ico`
  });

  win.maximize();
  win.show();

  // uncomment below to open the DevTools.
  win.webContents.openDevTools();

  // Event when the window is closed.
  win.on('closed', function () {
    win = null
  })
}

// Create window on electron intialization
//app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // macOS specific close process
  if (win === null) {
    createWindow();
    win.loadURL("app://-");
  }
})
