export class NavigationPosition {
    public x: number;
    public y: number;
    public orientation: number;
}
