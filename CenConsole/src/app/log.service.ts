import { Injectable } from '@angular/core';
import { PositionLogEntry, LogEntry, EnergyLogEntry, TextLogEntry, StateLogEntry, MotorsLogEntry, ObstacleLogEntry } from './log-entry';
import { Observable, ReplaySubject } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Source } from './source';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  private entries: ReplaySubject<LogEntry>;

  constructor() {
    this.reset();
  }

  public getEnergy(): Observable<EnergyLogEntry> {
    let entries = this.entries.pipe(filter(x => x instanceof EnergyLogEntry), map(x => x as EnergyLogEntry));
    return entries;
  }

  public getPositions(): Observable<PositionLogEntry> {
    let entries = this.entries.pipe(filter(x => x instanceof PositionLogEntry), map(x => x as PositionLogEntry));
    return entries;
  }

  public getDebugLog(): Observable<TextLogEntry> {
    let entries = this.entries.pipe(filter(x => x instanceof TextLogEntry), map(x => x as TextLogEntry));
    return entries;
  }

  public getObstacles(): Observable<ObstacleLogEntry> {
    let entries = this.entries.pipe(filter(x => x instanceof ObstacleLogEntry), map(x => x as ObstacleLogEntry));
    return entries;
  }

  public getState(): Observable<StateLogEntry> {
    let entries = this.entries.pipe(filter(x => x instanceof StateLogEntry), map(x => x as StateLogEntry));
    return entries;
  }

  public getMotors(): Observable<MotorsLogEntry> {
    let entries = this.entries.pipe(filter(x => x instanceof MotorsLogEntry), map(x => x as MotorsLogEntry));
    return entries;
  }

  public addEntries(entries: Observable<LogEntry>) {
    if (this.entries != null) {
      entries.subscribe(x => this.entries.next(x));
    }
  }

  public reset(): void {
    if (this.entries != null) {
      this.entries.complete();
    }
    this.entries = new ReplaySubject();
  }

  public removeEntriesFromSource(source: Source) {
    this.entries.complete();
    let entries = this.entries.pipe(filter(x => x.deviceDescriptor.source != source));
    let filtered = new ReplaySubject<LogEntry>();
    let subscription = entries.subscribe(x => filtered.next(x));
    subscription.unsubscribe();
    this.entries = filtered;
  }
}
