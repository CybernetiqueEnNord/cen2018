import { Injectable } from '@angular/core';
import { DeviceDescriptor } from './device-descriptor';
import { Observable, Subscriber, TeardownLogic, from, of } from 'rxjs';
import { LogEntry } from './log-entry';
import { Source, SourceType, SourceState, WebSocketSourceConnection } from './source';
import { LogService } from './log.service';

class ParserData {
  public remainder: string = "";
  public input: string;
  public lineIndex: number = 0;
  public deviceDescriptor: DeviceDescriptor = new DeviceDescriptor();
  public line: string;
}

class SourceData {
  type: SourceType;
  url: string;
}

@Injectable({
  providedIn: 'root'
})
export class LogParserService {
  private sources: Array<Source> = new Array();
  private loading: boolean = false;

  constructor(private logService: LogService) {
    this.loadSources();
  }

  private loadSources(): void {
    console.log("loading sources");
    this.loading = true;
    try {
      let data = localStorage.getItem("sources");
      if (data == null) {
        return;
      }
      let sources: SourceData[] = JSON.parse(data);
      sources.forEach(x => this.restoreSource(x));
    } catch (e) {
      console.error(e);
    } finally {
      this.loading = false;
    }
  }

  private restoreSource(data: SourceData): void {
    if (data.type == SourceType.WebSocket) {
      let source = new Source();
      source.type = data.type;
      source.url = data.url;
      this.addSource(source);
    }
  }

  private saveSources(): void {
    if (this.loading) {
      return;
    }
    try {
      let data = JSON.stringify(this.sources.filter(x => x.type == SourceType.WebSocket).map(x => {
        var sourceData = new SourceData();
        sourceData.type = x.type;
        sourceData.url = x.url;
        return sourceData;
      }));
      localStorage.setItem("sources", data);
    } catch (e) {
      console.error(e);
    }
  }

  private handleLine(parserData: ParserData): LogEntry {
    if (parserData.line.length < 1) {
      return null;
    }
    let type: string;
    let parameters: string[];
    let timestamp: number = 0;
    let c = parserData.line.charAt(0);
    if (c >= "0" && c <= "9") {
      // présence de l'horodatage
      let pos = parserData.line.indexOf(' ');
      if (pos > 0) {
        timestamp = +parserData.line.substring(0, pos);
        type = parserData.line.charAt(pos + 1);
        parameters = parserData.line.substring(pos + 2).split(";");
      }
    } else {
      type = c;
      parameters = parserData.line.substring(1).split(";");
    }
    let entry = LogEntry.fromData(parserData.deviceDescriptor, parserData.lineIndex++, timestamp, type, parameters)
    return entry;
  }

  private handleChunk(parserData: ParserData, subscriber: Subscriber<LogEntry>) {
    let data = parserData.remainder + parserData.input;
    let lines = data.split(/\r?\n/);
    parserData.remainder = lines.slice(-1)[0];
    lines.pop();
    lines.forEach(line => {
      parserData.line = line;
      let entry = this.handleLine(parserData);
      if (entry != null && this.isSourceActive(parserData)) {
        subscriber.next(entry);
      }
    });
  }

  private isSourceActive(parserData: ParserData): boolean {
    let source = parserData.deviceDescriptor.source;
    return source != null && source.state != SourceState.Suspended;
  }

  private parseFile(source: Source, subscriber: Subscriber<LogEntry>) {
    const BUFFER_SIZE = 128000;

    let parserData = new ParserData();
    let file = source.file;

    var chunk = 0;

    let nextChunk = () => {
      let slice = file.slice(chunk * BUFFER_SIZE, (chunk + 1) * BUFFER_SIZE, file.type);
      chunk++;
      let reader = new FileReader();
      reader.onload = () => {
        parserData.input = "" + reader.result;
        this.handleChunk(parserData, subscriber);

        if (chunk * BUFFER_SIZE < file.size) {
          setTimeout(nextChunk, 0);
        } else {
          subscriber.complete();
          source.state = SourceState.Completed;
        }
      }
      reader.readAsText(slice, slice.type);
    }

    source.state = SourceState.Connected;
    parserData.deviceDescriptor.source = source;
    nextChunk();
  }

  private getLogEntriesFromFile(source: Source): Observable<LogEntry> {
    let result: Observable<LogEntry> = new Observable(subscriber => this.parseFile(source, subscriber));
    return result;
  }

  private getLogEntriesFromEvents(source: Source, observable: Observable<MessageEvent>): Observable<LogEntry> {
    let result: Observable<LogEntry> = new Observable(subscriber => this.parseEvents(source, observable, subscriber));
    return result;
  }

  private parseEvents(source: Source, observable: Observable<MessageEvent>, subscriber: Subscriber<LogEntry>) {
    let parserData = new ParserData();
    parserData.deviceDescriptor.source = source;
    observable.subscribe(x => {
      parserData.input = x.data;
      this.handleChunk(parserData, subscriber);
    });
  }

  public readFile(source: Source): Observable<LogEntry> {
    let entries = this.getLogEntriesFromFile(source);
    return entries;
  }

  public readWebSocket(source: Source): Observable<LogEntry> {
    source.connection = new WebSocketSourceConnection();
    let observable = new Observable<MessageEvent>(subscriber => {
      let connect = (): TeardownLogic => {
        if (source.state == SourceState.Completed) {
          console.log("closed");
          return;
        }
        console.log("connecting");
        source.state = SourceState.Connecting;
        let socket: WebSocket = new WebSocket(source.url);
        socket.onopen = event => {
          console.log("connected");
          source.state = SourceState.Connected;
        }
        socket.onmessage = subscriber.next.bind(subscriber);
        // TODO: gérer les erreurs
        //socket.onerror = subscriber.error.bind(subscriber);
        socket.onclose = event => {
          socket.close();
          if (source.state != SourceState.Completed) {
            console.log("reconnection");
            source.state = SourceState.Connecting;
            setTimeout(connect, 1000);
          } else {
            console.log("closed");
          }
        }
        source.connection.socket = socket;
        return () => source.connection.socket.close();
      };
      source.connection.subscriber = subscriber;
      return connect();
    });
    let entries = this.getLogEntriesFromEvents(source, observable);
    return entries;
  }

  private readFromSource(source: Source): Observable<LogEntry> {
    switch (source.type) {
      case SourceType.WebSocket:
        return this.readWebSocket(source);

      case SourceType.File:
        return this.readFile(source);
    }
  }

  public getSources(): Observable<Source> {
    let sources = from(this.sources);
    return sources;
  }

  public addSource(source: Source): Observable<Source> {
    this.sources.push(source);
    this.saveSources();
    let result = of(source);
    result.subscribe(x => {
      let entries = this.readFromSource(x);
      this.logService.addEntries(entries);
    });
    return result;
  }

  public removeSource(source: Source) {
    let index = this.sources.indexOf(source);
    if (index >= 0) {
      this.sources.splice(index, 1);
    }
    this.close(source);
    this.logService.removeEntriesFromSource(source);
    this.saveSources();
  }

  private close(source: Source) {
    source.state = SourceState.Completed;
    if (source.connection instanceof WebSocketSourceConnection && source.connection.subscriber != null) {
      source.connection.subscriber.complete();
    }
  }

  public send(source: Source, data: string) {
    if (source.connection instanceof WebSocketSourceConnection) {
      let socket = source.connection.socket;
      if (socket.readyState == socket.OPEN) {
        socket.send(data);
      }
    }
  }
}
