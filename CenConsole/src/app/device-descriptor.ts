import { Source } from './source';

export class DeviceDescriptor {
    public name: string;
    public version: string;
    public address: string;
    public source: Source;
}
