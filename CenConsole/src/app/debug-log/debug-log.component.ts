import { Component, OnInit, OnDestroy } from '@angular/core';
import { LogService } from '../log.service';
import { TextLogEntry } from '../log-entry';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-debug-log',
  templateUrl: './debug-log.component.html',
  styleUrls: ['./debug-log.component.css']
})
export class DebugLogComponent implements OnInit, OnDestroy {
  private subscription: Subscription;

  public data: Array<string> = new Array();

  constructor(private logService: LogService) { }

  ngOnInit() {
    let entries = this.logService.getDebugLog();
    this.subscription = entries.subscribe(x => this.addToLog(x));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  addToLog(entry: TextLogEntry): void {
    this.data.push(entry.text);
  }
}
