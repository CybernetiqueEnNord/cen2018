import { Component, OnInit } from '@angular/core';
import { Source, SourceType } from '../source';
import { LogParserService } from '../log-parser.service';

@Component({
  selector: 'cen-sources',
  templateUrl: './sources.component.html',
  styleUrls: ['./sources.component.css']
})
export class SourcesComponent implements OnInit {
  public sources: Array<Source> = new Array();

  constructor(private logParserService: LogParserService) { }

  ngOnInit() {
    this.logParserService.getSources().subscribe(x => this.sources.push(x));
  }

  public addFile(e) {
    let source = new Source();
    source.type = SourceType.File;
    source.file = e.target.files[0];
    this.addSource(source);
  }

  public addWebSocket(url: string) {
    if (!url.startsWith("ws://")) {
      url = "ws://" + url;
    }
    let source = new Source();
    source.type = SourceType.WebSocket;
    source.url = url;
    this.addSource(source);
  }

  public addSource(source: Source) {
    this.logParserService.addSource(source).subscribe(x => {
      this.sources.push(x);
    });
  }

  public remove(source: Source) {
    this.logParserService.removeSource(source);
    let index = this.sources.indexOf(source);
    if (index >= 0) {
      this.sources.splice(index, 1);
    }
  }
}
