import { Injectable } from '@angular/core';
import { MoveCommand } from './move-command';
import { LogParserService } from './log-parser.service';
import { Source } from './source';
import { ReplaySubject, Observable } from 'rxjs';
import { Point } from './point';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  private fZoomScale: number = 1;
  private fCurrentSource: Source = null;
  private fDestination: ReplaySubject<Point> = new ReplaySubject(1);

  constructor(private logParserService: LogParserService) { }

  public get zoomScale(): number {
    return this.fZoomScale;
  }

  public set zoomScale(value: number) {
    this.fZoomScale = value;
  }

  public get source(): Source {
    return this.fCurrentSource;
  }

  public set source(value: Source) {
    this.fCurrentSource = value;
  }

  public addCommand(command: MoveCommand) {
    if (this.fCurrentSource == null) {
      throw new Error("No selected source");
    }
    let data = command.getCommand();
    this.logParserService.send(this.fCurrentSource, data);
  }

  public setDestination(position: Point): void {
    this.fDestination.next(position);
  }

  public getDestination(): Observable<Point> {
    return this.fDestination;
  }
}
