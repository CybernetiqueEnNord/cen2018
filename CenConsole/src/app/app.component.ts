import { Component } from '@angular/core';
import { LogParserService } from './log-parser.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CenConsole';

  constructor(private logParserService: LogParserService) {
  }
}
