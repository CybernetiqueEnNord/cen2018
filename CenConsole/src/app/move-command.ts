import { Point } from './point';
import { NavigationPosition } from './navigation-position';

export interface MoveCommand {
    getCommand(): string;
    getDescription(): string;
}

export class StraightMoveCommand implements MoveCommand {
    constructor(private distance: number) { }

    public getCommand(): string {
        return `M${this.distance.toFixed(0)}`;
    }

    public getDescription(): string {
        return `Ligne droite de ${this.distance.toFixed(0)} mm`;
    }
}

export class RotationMoveCommand implements MoveCommand {
    constructor(private angle: number) { }

    public getCommand(): string {
        return `R${this.angle.toFixed(0)}`;
    }

    public getDescription(): string {
        return `Rotation de ${(this.angle / 10).toFixed(1)}°`;
    }
}

export class TargetMoveCommand implements MoveCommand {
    constructor(private target: Point) { }

    public getCommand(): string {
        return `G${this.target.x.toFixed(0)};${this.target.y.toFixed(0)}`;
    }

    public getDescription(): string {
        return `Aller au point (${this.target.x.toFixed(0)} ; ${this.target.y.toFixed(0)})`;
    }
}

export class RepositionMoveCommand implements MoveCommand {
    constructor(private position: NavigationPosition, private distance: number) { }

    public getCommand(): string {
        return `r${this.position.x.toFixed(0)};${this.position.y.toFixed(0)};${this.position.orientation.toFixed(0)};${this.distance.toFixed(0)}`;
    }

    public getDescription(): string {
        return `Recallage (${this.position.x.toFixed(0)} ; ${this.position.y.toFixed(0)}), ${this.position.orientation.toFixed(0)} d°, distance : ${this.distance.toFixed(0)} mm`;
    }
}

export class StopMoveCommand implements MoveCommand {
    constructor() { }

    public getCommand(): string {
        return "Z";
    }

    public getDescription(): string {
        return "Stop";
    }
}

export class ResetMoveCommand implements MoveCommand {
    constructor() { }

    public getCommand(): string {
        return "^";
    }

    public getDescription(): string {
        return "Reset";
    }
}
