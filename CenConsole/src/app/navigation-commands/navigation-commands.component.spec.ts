import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationCommandsComponent } from './navigation-commands.component';

describe('NavigationCommandsComponent', () => {
  let component: NavigationCommandsComponent;
  let fixture: ComponentFixture<NavigationCommandsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationCommandsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationCommandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
