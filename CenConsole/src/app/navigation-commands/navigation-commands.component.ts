import { Component, OnInit, OnDestroy, ViewChildren, QueryList } from '@angular/core';
import { StraightMoveCommand, RotationMoveCommand, TargetMoveCommand, RepositionMoveCommand, StopMoveCommand, ResetMoveCommand } from '../move-command';
import { NavigationService } from '../navigation.service';
import { Subscription } from 'rxjs';
import { Point } from '../point';
import { MatInput } from '@angular/material/input';
import { NavigationPosition } from '../navigation-position';

@Component({
  selector: 'cen-navigation-commands',
  templateUrl: './navigation-commands.component.html',
  styleUrls: ['./navigation-commands.component.css']
})
export class NavigationCommandsComponent implements OnInit, OnDestroy {
  @ViewChildren(MatInput) private inputs: QueryList<MatInput>;
  private destinationSubscription: Subscription;

  public destination: Point;

  constructor(private navigationService: NavigationService) { }

  ngOnInit() {
    this.destinationSubscription = this.navigationService.getDestination().subscribe(x => this.destination = x);
  }

  ngOnDestroy(): void {
    if (this.destinationSubscription != null) {
      this.destinationSubscription.unsubscribe();
    }
  }

  public move(distance: number): void {
    let command = new StraightMoveCommand(distance);
    this.navigationService.addCommand(command);
  }

  public rotate(angle: number): void {
    let command = new RotationMoveCommand(angle);
    this.navigationService.addCommand(command);
  }

  public goto(x: number, y: number): void {
    let command = new TargetMoveCommand(new Point(x, y));
    this.navigationService.addCommand(command);
  }

  public isValidDestination(x: number, y: number): boolean {
    return x != 0 && y != 0;
  }

  public isValidDistance(distance: number): boolean {
    return distance != 0;
  }

  public isValidAngle(angle: number): boolean {
    return angle != 0;
  }

  public isValidReposition(x: number, y: number, angle: number, distance: number) {
    return this.isValidDestination(x, y) && this.isValidDistance(distance) && this.isValidAngle(angle);
  }

  public clear(): void {
    this.inputs.forEach(x => x.value = null);
  }

  public reposition(x: number, y: number, angle: number, distance: number): void {
    let position = new NavigationPosition();
    position.x = x;
    position.y = y;
    position.orientation = angle;
    let command = new RepositionMoveCommand(position, distance);
    this.navigationService.addCommand(command);
  }

  public stopMove(): void {
    let command = new StopMoveCommand();
    this.navigationService.addCommand(command);
  }

  public resetMove(): void {
    let command = new ResetMoveCommand();
    this.navigationService.addCommand(command);
  }
}
