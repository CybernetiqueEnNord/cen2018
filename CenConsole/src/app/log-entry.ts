import { NavigationPosition } from './navigation-position';
import { DeviceDescriptor } from './device-descriptor';
import { Point } from './point';

export enum LogEntryType {
    Unknown,
    Position,
    State,
    Motors,
    Energy,
    Comment,
    Debug,
    Obstacle
}

export class LogEntry {
    private fType: LogEntryType;
    private fParameters: string[];
    private fDeviceDescriptor: DeviceDescriptor;
    private fTimestamp: number;
    private fIndex: number;

    protected constructor(deviceDescriptor: DeviceDescriptor, index: number, timestamp: number, type: LogEntryType, parameters: string[]) {
        this.fDeviceDescriptor = deviceDescriptor;
        this.fIndex = index;
        this.fTimestamp = timestamp;
        this.fType = type;
        this.fParameters = parameters;
    }

    public static fromData(deviceDescriptor: DeviceDescriptor, index: number, timestamp: number, type: string, parameters: string[]) {
        let entryType = LogEntry.getType(type);
        let entry: LogEntry;
        switch (entryType) {
            case LogEntryType.Position:
                entry = new PositionLogEntry(deviceDescriptor, index, timestamp, parameters);
                break;

            case LogEntryType.State:
                entry = new StateLogEntry(deviceDescriptor, index, timestamp, parameters);
                break;

            case LogEntryType.Motors:
                entry = new MotorsLogEntry(deviceDescriptor, index, timestamp, parameters);
                break;

            case LogEntryType.Energy:
                entry = new EnergyLogEntry(deviceDescriptor, index, timestamp, parameters);
                break;

            case LogEntryType.Comment:
                entry = new TextLogEntry(deviceDescriptor, index, timestamp, parameters);
                break;

            case LogEntryType.Debug:
                entry = new DebugLogEntry(deviceDescriptor, index, timestamp, parameters);
                break;

            case LogEntryType.Obstacle:
                entry = new ObstacleLogEntry(deviceDescriptor, index, timestamp, parameters);
                break;

            default:
                let p = [].concat(type, parameters);
                entry = new LogEntry(deviceDescriptor, index, timestamp, entryType, p);
                break;
        }
        return entry;
    }

    private static getType(type: string): LogEntryType {
        switch (type) {
            case 'p':
                return LogEntryType.Position;

            case 'S':
                return LogEntryType.State;

            case 'C':
                return LogEntryType.Motors;

            case 'E':
                return LogEntryType.Energy;

            case ':':
                return LogEntryType.Comment;

            case 'D':
                return LogEntryType.Debug;

            case 'F':
                return LogEntryType.Obstacle;

            default:
                return LogEntryType.Unknown;
        }
    }

    public toString(): string {
        return `unknown(${this.parameters.join(", ")})`;
    }

    public get deviceDescriptor(): DeviceDescriptor { return this.fDeviceDescriptor; }

    public get timestamp(): number { return this.fTimestamp; }

    public get index(): number { return this.fIndex; }

    public get parameters(): string[] { return this.fParameters; }

    public get type(): LogEntryType { return this.fType; }
}

export class PositionLogEntry extends LogEntry {
    private fPosition: NavigationPosition;

    constructor(deviceDescriptor: DeviceDescriptor, index: number, timestamp: number, parameters: string[]) {
        super(deviceDescriptor, index, timestamp, LogEntryType.Position, parameters);
        this.fPosition = this.buildPosition(parameters);
    }

    private buildPosition(parameters: string[]): NavigationPosition {
        let result = new NavigationPosition();
        result.x = +parameters[0];
        result.y = +parameters[1];
        result.orientation = 0.1 * +parameters[2];
        return result;
    }

    public get position(): NavigationPosition { return this.fPosition; }

    public toString(): string {
        return `position(${this.position.x}, ${this.position.y}, ${this.position.orientation})`;
    }
}

export class TextLogEntry extends LogEntry {
    constructor(deviceDescriptor: DeviceDescriptor, index: number, timestamp: number, parameters: string[]) {
        super(deviceDescriptor, index, timestamp, LogEntryType.Comment, parameters);
    }

    public toString(): string {
        return `text: ${this.text}`;
    }

    public get text(): string {
        return this.parameters.join(";");
    }
}

export enum State {
    Unknown,
    Ready,
    Busy,
    Error
}

export class StateLogEntry extends LogEntry {
    private fState: State;

    constructor(deviceDescriptor: DeviceDescriptor, index: number, timestamp: number, parameters: string[]) {
        super(deviceDescriptor, index, timestamp, LogEntryType.State, parameters);
        this.fState = +parameters[0];
    }

    public toString(): string {
        return `state(${State[this.state]})`;
    }

    public get state(): State { return this.fState; }
}

export class MotorsLogEntry extends LogEntry {
    private fLeftMotor: number;
    private fRightMotor: number;

    constructor(deviceDescriptor: DeviceDescriptor, index: number, timestamp: number, parameters: string[]) {
        super(deviceDescriptor, index, timestamp, LogEntryType.Motors, parameters);
        this.fLeftMotor = +parameters[0];
        this.fRightMotor = +parameters[1];
    }

    public toString(): string {
        return `motors(${this.leftMotor}, ${this.rightMotor})`;
    }

    public get leftMotor(): number { return this.fLeftMotor; }

    public get rightMotor(): number { return this.fRightMotor; }
}

export class EnergyLogEntry extends LogEntry {
    private fLeftCurrent: number;
    private fLeftCharge: number;
    private fRightCurrent: number;
    private fRightCharge: number;

    constructor(deviceDescriptor: DeviceDescriptor, index: number, timestamp: number, parameters: string[]) {
        super(deviceDescriptor, index, timestamp, LogEntryType.Energy, parameters);
        this.fLeftCurrent = +parameters[0];
        this.fLeftCharge = +parameters[1];
        this.fRightCurrent = +parameters[2];
        this.fRightCharge = +parameters[3];
    }

    public get leftCurrent(): number { return this.fLeftCurrent; }

    public get leftCharge(): number { return this.fLeftCharge; }

    public get rightCurrent(): number { return this.fRightCurrent; }

    public get rightCharge(): number { return this.fRightCharge; }

    public toString(): string {
        return `energy(${this.leftCurrent} mA, ${this.rightCurrent} mA, ${this.leftCharge} mAh, ${this.rightCharge} mAh)`;
    }
}

/**
 * État du déplacement.
 */
export enum MoveState {
    /** En attente. */
    StandBy = 0,
    /** Asservissement sur position cible. */
    TargetPosition = 1,
    /** Accélération droite. */
    StraightAcceleration = 2,
    /** Vitesse linéaire constante. */
    StraightConstant = 3,
    /** Décélération droite. */
    StraightDeceleration = 4,
    /** Accélération en rotation. */
    RotationAcceleration = 6,
    /** Vitesse de rotation constante. */
    RotationConstant = 7,
    /** Décélération en rotation. */
    RotationDeceleration = 8,
    /** Freinage dû à un obstacle. */
    EmergencyBrake = 9,
    /** Accélération en clothoïde. */
    ClothoidAcceleration = 11,
    /** Arc de clothoïde. */
    ClothoidArc = 12,
    /** Décélération en clothoïde. */
    ClothoidDeceleration = 13
};

/**
 * \brief Erreur de déplacement.
 *
 * Type d'erreur de déplacement.
 */
export enum MoveError {
    /** Aucune erreur. */
    None,
    /** Dépassement du maximum d'erreur. */
    Overrun,
    /** Dépassement du maximum d'erreur en mode calibration. */
    CalibrationOverrun,
    /** Dépassement de la durée d'attente sur la détection d'un obstacle. */
    ObstacleTimeout
};

/**
 * État de navigation.
 */
export enum MoveCommand {
    /** En attente. */
    Idle,
    /** En déplacement. */
    Moving,
    /** En attente du signal de départ. */
    WaitingForStart,
    /** En attente d'une durée. */
    WaitingForDelay,
    /** Match terminé. */
    Terminated,
    /** Arrêt d'urgence pressé. */
    EmergencyStop
};

export class DebugLogEntry extends LogEntry {
    private fMoveState: MoveState;
    private fSpeedIndex: number;
    private fErrorState: MoveError;
    private fCommand: MoveCommand;
    private fDistanceOrder: number;
    private fDistanceTravelled: number;
    private fValue1: number;
    private fValue2: number;
    private fCpuLoad: number;

    constructor(deviceDescriptor: DeviceDescriptor, index: number, timestamp: number, parameters: string[]) {
        super(deviceDescriptor, index, timestamp, LogEntryType.Debug, parameters);
        this.fMoveState = +parameters[0];
        this.fSpeedIndex = +parameters[1];
        this.fErrorState = +parameters[2];
        this.fCommand = +parameters[3];
        this.fDistanceOrder = +parameters[4];
        this.fDistanceTravelled = +parameters[5];
        this.fValue1 = +parameters[6];
        this.fValue2 = +parameters[7];
        this.fCpuLoad = +parameters[8];
    }

    public get moveState(): MoveState { return this.fMoveState; }
    public get speedIndex(): number { return this.fSpeedIndex; }
    public get errorState(): MoveError { return this.fErrorState; }
    public get command(): MoveCommand { return this.fCommand; }
    public get distanceOrder(): number { return this.fDistanceOrder; }
    public get distanceTravelled(): number { return this.fDistanceTravelled; }
    public get value1(): number { return this.fValue1; }
    public get value2(): number { return this.fValue2; }
    public get cpuLoad(): number { return this.fCpuLoad; }

    public toString(): string {
        return `debug(state = ${MoveState[this.moveState]}, speed = ${this.speedIndex}, error = ${MoveError[this.errorState]}, command = ${MoveCommand[this.command]}, distanceOrder = ${this.distanceOrder}, distanceTravelled = ${this.distanceTravelled}, value1 = ${this.value1}, value2 = ${this.value2}, cpuLoad = ${this.cpuLoad})`;
    }
}

export class ObstacleLogEntry extends LogEntry {
    private fAvoidanceEnabled: boolean;
    private fObstacleDetected: boolean;
    private fObstaclePosition: Point;

    constructor(deviceDescriptor: DeviceDescriptor, index: number, timestamp: number, parameters: string[]) {
        super(deviceDescriptor, index, timestamp, LogEntryType.Debug, parameters);
        this.fAvoidanceEnabled = parameters[0].charAt(0) != '0';
        this.fObstacleDetected = parameters[0].charAt(1) != '0';
        if (parameters.length > 1) {
            this.fObstaclePosition = new Point(+parameters[1], +parameters[2]);
        }
    }

    public get avoidanceEnabled(): boolean { return this.fAvoidanceEnabled; }
    public get obstacleDetected(): boolean { return this.fObstacleDetected; }
    public get obstaclePosition(): Point { return this.fObstaclePosition; }

    public toString(): string {
        return `obstacle(enabled = ${this.avoidanceEnabled}, detected = ${this.obstacleDetected}, position = ${this.obstaclePosition})`;
    }
}