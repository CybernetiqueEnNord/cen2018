import { AfterViewInit, Component, ElementRef, ViewChild, ViewChildren, QueryList, ChangeDetectorRef } from '@angular/core';
import { NavigationPosition } from '../navigation-position';
import { Point } from '../point';
import { DeviceDescriptor } from '../device-descriptor';
import { PositionLogEntry } from '../log-entry';
import { NavigationService } from '../navigation.service';

class GaugeData {
  public index: number;
  public lastPosition: NavigationPosition = null;
}

@Component({
  selector: 'cen-gameboard',
  templateUrl: './gameboard.component.html',
  styleUrls: ['./gameboard.component.css']
})
export class GameboardComponent implements AfterViewInit {
  @ViewChild('canvas', { static: false }) private canvas: ElementRef;
  @ViewChildren('gauge') private gauges: QueryList<ElementRef>;

  DEFAULT_CANVAS_WIDTH: number = 400;
  DEFAULT_CANVAS_HEIGHT: number = 600;

  private xRatio: number;
  private yRatio: number;
  private gameBoardWidth: number = 2000;
  private gameBoardHeight: number = 3000;
  private devices: Map<DeviceDescriptor, GaugeData> = new Map();
  private zoomScale: number;

  public canvasWidth: number = this.DEFAULT_CANVAS_WIDTH;
  public canvasHeight: number = this.DEFAULT_CANVAS_HEIGHT;
  public cursorPosition: Point = null;
  public devicesValues: Array<GaugeData> = new Array();

  constructor(private navigationService: NavigationService, private cd: ChangeDetectorRef) {
  }

  private gameBoardToPixel(p: Point, div: HTMLDivElement) {
    p.x = p.x / this.xRatio - div.clientWidth / 2;
    p.y = this.canvas.nativeElement.clientHeight - p.y / this.yRatio - div.clientHeight / 2;
  }

  private pixelToGameboard(p: Point) {
    p.x *= this.xRatio;
    p.y = (this.canvas.nativeElement.clientHeight - p.y) * this.yRatio;
  }

  private addSegment(x1: number, y1: number, x2: number, y2: number) {
    var canvasElement: HTMLCanvasElement = this.canvas.nativeElement;
    var ctx: CanvasRenderingContext2D = canvasElement.getContext('2d');
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
    /*
    var dx = x2 - x1;
    var dy = y2 - y1;
    var angle = Math.atan2(dx, dy);
    this.setGaugePosition(x2, y2, angle);
    */
  }

  ngAfterViewInit(): void {
    this.zoomScale = this.navigationService.zoomScale;
    this.applyZoom();

    var canvasElement: HTMLCanvasElement = this.canvas.nativeElement;
    var ctx: CanvasRenderingContext2D = canvasElement.getContext('2d');
    canvasElement.width = this.gameBoardWidth;
    canvasElement.height = this.gameBoardHeight;
    this.xRatio = canvasElement.width / canvasElement.clientWidth;
    this.yRatio = canvasElement.height / canvasElement.clientHeight;
    ctx.transform(1, 0, 0, -1, 0, this.gameBoardHeight);
    ctx.lineWidth = 10;
    ctx.strokeStyle = "#0000FF";

    //this.addSegment(150, 150, 1000, 1500);

    /*
    var img = new Image();
    img.onload = function() { ctx.drawImage(img,0,0,2000,3000); };
    img.src='assets/test.svg';
    */
  }

  public handlePositionEntry(entry: PositionLogEntry): void {
    let gaugeData = this.devices.get(entry.deviceDescriptor);
    if (gaugeData == undefined) {
      gaugeData = new GaugeData();
      gaugeData.index = this.devices.size;
      this.devices.set(entry.deviceDescriptor, gaugeData);
      this.devicesValues = Array.from(this.devices.values());
      this.cd.detectChanges();
    }
    this.addPosition(gaugeData, entry.position);
  }

  private addPosition(gaugeData: GaugeData, position: NavigationPosition) {
    if (gaugeData.lastPosition != null) {
      this.addSegment(gaugeData.lastPosition.x, gaugeData.lastPosition.y, position.x, position.y);
    }
    this.setGaugePosition(gaugeData, position.x, position.y, position.orientation);
    gaugeData.lastPosition = position;
  }

  private setGaugePosition(gaugeData: GaugeData, x: number, y: number, angle: number) {
    var gauges = this.gauges.toArray();
    var div: HTMLDivElement = gauges[gaugeData.index].nativeElement;
    var p = new Point(x, y);
    this.gameBoardToPixel(p, div);
    div.style.left = p.x.toString() + "px";
    div.style.top = p.y.toString() + "px";
    div.style.transform = `rotate(${90 - angle}deg) scale(${this.zoomScale})`;
  }

  private drawTrajectory(points: Array<NavigationPosition>) {
    var canvasElement: HTMLCanvasElement = this.canvas.nativeElement;
    var ctx: CanvasRenderingContext2D = canvasElement.getContext('2d');
    var first: boolean = true;
    points.map(current => {
      if (first) {
        ctx.moveTo(current.x, current.y);
        first = false;
      } else {
        ctx.lineTo(current.x, current.y);
      }
    });
    ctx.stroke();
  }

  public handleClick(event: MouseEvent) {
    let p = this.getMousePosition(event);
    this.navigationService.setDestination(p);
  }

  public handleMove(event: MouseEvent) {
    this.cursorPosition = this.getMousePosition(event);
  }

  private getMousePosition(event: MouseEvent): Point {
    let canvasElement: HTMLCanvasElement = this.canvas.nativeElement;
    let rect = canvasElement.getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    var p = new Point(x, y);
    this.pixelToGameboard(p);
    return p;
  }

  public handleWheel(event: WheelEvent) {
    let ratio: number = 0;
    switch (event.deltaMode) {
      case event.DOM_DELTA_LINE:
        ratio = event.deltaY / 100;
        break;

      case event.DOM_DELTA_PIXEL:
        ratio = event.deltaY / 3000;
        break;

      case event.DOM_DELTA_PAGE:
        // TODO
        ratio = 0;
        break;
    }
    this.zoomScale *= 1 - ratio;
    this.applyZoom();

    this.navigationService.zoomScale = this.zoomScale;

    setTimeout(() => this.updateGaugesPositions(), 0);
  }

  private applyZoom() {
    this.canvasWidth = this.DEFAULT_CANVAS_WIDTH * this.zoomScale;
    this.canvasHeight = this.DEFAULT_CANVAS_HEIGHT * this.zoomScale;

    setTimeout(() => {
      let canvasElement: HTMLCanvasElement = this.canvas.nativeElement;
      this.xRatio = canvasElement.width / canvasElement.clientWidth;
      this.yRatio = canvasElement.height / canvasElement.clientHeight;
    }, 0);
  }

  private updateGaugesPositions() {
    this.devicesValues.forEach(data => {
      if (data.lastPosition != null) {
        this.setGaugePosition(data, data.lastPosition.x, data.lastPosition.y, data.lastPosition.orientation);
      }
    });
  }

  public handleExit(event: MouseEvent) {
    this.cursorPosition = null;
  }
}
