import { Pipe, PipeTransform } from '@angular/core';
import { NavigationPosition } from './navigation-position';

@Pipe({ name: 'position' })
export class NavigationPositionPipe implements PipeTransform {
  transform(position: NavigationPosition): string {
    return position ? `(${position.x.toFixed(1)} ; ${position.y.toFixed(1)}); ${position.orientation.toFixed(1)}°` : 'undefined';
  }
}

@Pipe({ name: 'longPosition' })
export class LongNavigationPositionPipe implements PipeTransform {
  transform(position: NavigationPosition): string {
    return position ? `x=${position.x.toFixed(1)} mm, y=${position.y.toFixed(1)} mm, orientation=${position.orientation.toFixed(1)}°` : 'undefined';
  }
}
