import { Subscriber } from 'rxjs';

export enum SourceType {
    File,
    WebSocket
}

export enum SourceState {
    Connecting,
    Connected,
    Completed,
    Suspended
}

export class WebSocketSourceConnection {
    public socket: WebSocket;
    public subscriber: Subscriber<any>;
}

export class Source {
    public url: string;
    public type: SourceType;
    public state: SourceState;
    public file: File;
    public connection: null | WebSocketSourceConnection;

    public get label(): string {
        switch (this.type) {
            case SourceType.File:
                return this.file.name;

            case SourceType.WebSocket:
                return this.url;
        }
    }

    public get displayState(): string {
        return SourceState[this.state];
    }

    public get online(): boolean {
        return this.state == SourceState.Connected;
    }
}
