import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChartModule } from 'angular-highcharts';

import { LongNavigationPositionPipe, NavigationPositionPipe } from './navigation-position.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';

import { AppComponent } from './app.component';
import { GameboardComponent } from './gameboard/gameboard.component';
import { NavigationComponent } from './navigation/navigation.component';
import { DebugLogComponent } from './debug-log/debug-log.component';
import { EnergyComponent } from './energy/energy.component';
import { SourcesComponent } from './sources/sources.component';
import { SourceDetailComponent } from './source-detail/source-detail.component';
import { NavigationDevicesComponent } from './navigation-devices/navigation-devices.component';
import { NavigationCommandsComponent } from './navigation-commands/navigation-commands.component';
import { NavigationStatusComponent } from './navigation-status/navigation-status.component';
import { MotorsComponent } from './motors/motors.component';

const appRoutes: Routes = [
  { path: 'sources', component: SourcesComponent },
  { path: 'navigation', component: NavigationComponent },
  { path: 'motors', component: MotorsComponent },
  { path: 'energy', component: EnergyComponent },
  { path: 'debug', component: DebugLogComponent },
  { path: '', redirectTo: 'navigation', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    GameboardComponent,
    LongNavigationPositionPipe,
    NavigationPositionPipe,
    NavigationComponent,
    DebugLogComponent,
    EnergyComponent,
    SourcesComponent,
    SourceDetailComponent,
    NavigationDevicesComponent,
    NavigationCommandsComponent,
    NavigationStatusComponent,
    MotorsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatSidenavModule,
    MatListModule,
    MatInputModule,
    MatGridListModule,
    MatRippleModule,
    MatIconModule,
    ChartModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
