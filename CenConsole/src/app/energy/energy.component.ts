import { Component, OnInit, OnDestroy } from '@angular/core';
import { LogService } from '../log.service';
import { EnergyLogEntry } from '../log-entry';
import { Chart } from 'angular-highcharts';
import { Subscription } from 'rxjs';
import { Point } from 'highcharts';

@Component({
  selector: 'app-energy',
  templateUrl: './energy.component.html',
  styleUrls: ['./energy.component.css']
})
export class EnergyComponent implements OnInit, OnDestroy {
  public chartCharge: Chart = new Chart({
    chart: {
      type: 'line',
      zoomType: 'x'
    },
    title: {
      text: 'Charge'
    },
    credits: {
      enabled: false
    },
    series: [
      {
        name: 'Left',
        data: [],
        type: 'line'
      },
      {
        name: 'Right',
        data: [],
        type: 'line'
      }
    ]
  });

  public chartCurrent: Chart = new Chart({
    chart: {
      type: 'line',
      zoomType: 'x'
    },
    title: {
      text: 'Courant'
    },
    credits: {
      enabled: false
    },
    series: [
      {
        name: 'Left',
        data: [],
        type: 'line'
      },
      {
        name: 'Right',
        data: [],
        type: 'line'
      }
    ]
  });

  private timeout: any;
  private subscription: Subscription;

  constructor(private logService: LogService) { }

  ngOnInit() {
    let entries = this.logService.getEnergy();
    this.subscription = entries.subscribe(x => this.addEntry(x));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  addEntry(entry: EnergyLogEntry): void {
    this.addPoint(this.chartCurrent, entry, x => x.leftCurrent, 0);
    this.addPoint(this.chartCurrent, entry, x => x.rightCurrent, 1);
    this.addPoint(this.chartCharge, entry, x => x.leftCharge, 0);
    this.addPoint(this.chartCharge, entry, x => x.rightCharge, 1);
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.chartCharge.ref.redraw(true)
      this.chartCurrent.ref.redraw(true)
    }, 300);
  }

  private addPoint(chart: Chart, entry: EnergyLogEntry, valueGetter: (entry: EnergyLogEntry) => number, serieIndex: number): void {
    let point = new Point();
    point.x = entry.timestamp;
    point.y = valueGetter(entry);
    chart.addPoint(point, serieIndex, false, false);
  }
}
