import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationDevicesComponent } from './navigation-devices.component';

describe('NavigationDevicesComponent', () => {
  let component: NavigationDevicesComponent;
  let fixture: ComponentFixture<NavigationDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
