import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../navigation.service';
import { Source, SourceState } from '../source';
import { LogParserService } from '../log-parser.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'cen-navigation-devices',
  templateUrl: './navigation-devices.component.html',
  styleUrls: ['./navigation-devices.component.css']
})
export class NavigationDevicesComponent implements OnInit {
  public sources: Source[] = [];
  public selectedSource: Source = null;

  constructor(private navigationService: NavigationService, private logParserService: LogParserService) { }

  ngOnInit() {
    let selectedSource = this.navigationService.source;
    if (selectedSource != null && selectedSource.state != SourceState.Completed) {
      this.selectedSource = selectedSource;
    }
    let sources = this.logParserService.getSources();
    sources.pipe(filter(x => x.state != SourceState.Completed)).subscribe(x => {
      this.sources.push(x);
      if (this.selectedSource == null) {
        this.selectSource(x);
      }
    });
  }

  public selectSource(source: Source) {
    this.selectedSource = source;
    this.navigationService.source = source;
  }
}
