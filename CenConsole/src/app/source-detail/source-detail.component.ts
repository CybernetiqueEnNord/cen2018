import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Source, SourceType, SourceState } from '../source';

@Component({
  selector: 'cen-source-detail',
  templateUrl: './source-detail.component.html',
  styleUrls: ['./source-detail.component.css']
})
export class SourceDetailComponent implements OnInit {
  @Input() source: Source;
  @Output() removed = new EventEmitter<Source>();

  constructor() { }

  ngOnInit() {
  }

  public canSuspend(): boolean {
    return this.source.type == SourceType.WebSocket && this.source.state == SourceState.Connected;
  }

  public canResume(): boolean {
    return this.source.state == SourceState.Suspended;
  }

  public remove(): void {
    this.removed.emit(this.source);
  }

  public suspend(): void {
    this.source.state = SourceState.Suspended;
  }

  public resume(): void {
    this.source.state = SourceState.Connected;
  }
}
