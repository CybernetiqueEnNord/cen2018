import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { LogService } from '../log.service';
import { Subscription } from 'rxjs';
import { GameboardComponent } from '../gameboard/gameboard.component';
import { PositionLogEntry, StateLogEntry, ObstacleLogEntry } from '../log-entry';
import { NavigationStatusComponent } from '../navigation-status/navigation-status.component';

@Component({
  selector: 'cen-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('gameboard', { static: false }) private gameboard: GameboardComponent;
  @ViewChild('status', { static: false }) private status: NavigationStatusComponent;

  private positionsSubscription: Subscription;
  private stateSubscription: Subscription;
  private obstaclesSubscription: Subscription;

  constructor(private logService: LogService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.positionsSubscription != null) {
      this.positionsSubscription.unsubscribe();
    }
    if (this.stateSubscription != null) {
      this.stateSubscription.unsubscribe();
    }
    if (this.obstaclesSubscription != null) {
      this.obstaclesSubscription.unsubscribe();
    }
  }

  ngAfterViewInit() {
    setTimeout(() => this.start(), 0);
  }

  private start(): void {
    let positions = this.logService.getPositions();
    this.positionsSubscription = positions.subscribe(e => this.handlePositionEntry(e));

    let state = this.logService.getState();
    this.stateSubscription = state.subscribe(e => this.handleStateEntry(e));

    let obstacles = this.logService.getObstacles();
    this.obstaclesSubscription = obstacles.subscribe(e => this.handleObstacleEntry(e));
  }

  private handleObstacleEntry(entry: ObstacleLogEntry) {
    this.status.obstacle = entry;
  }

  private handleStateEntry(entry: StateLogEntry): void {
    this.status.state = entry.state;
  }

  private handlePositionEntry(entry: PositionLogEntry): void {
    this.gameboard.handlePositionEntry(entry);
    this.status.position = entry.position;
  }
}
