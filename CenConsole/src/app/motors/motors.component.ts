import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { Subscription } from 'rxjs';
import { LogService } from '../log.service';
import { MotorsLogEntry } from '../log-entry';
import { Point } from 'highcharts';

@Component({
  selector: 'app-motors',
  templateUrl: './motors.component.html',
  styleUrls: ['./motors.component.css']
})
export class MotorsComponent implements OnInit {
  public chartMotors: Chart = new Chart({
    chart: {
      type: 'line',
      zoomType: 'x'
    },
    title: {
      text: 'Moteurs'
    },
    credits: {
      enabled: false
    },
    series: [
      {
        name: 'Left',
        data: [],
        type: 'line'
      },
      {
        name: 'Right',
        data: [],
        type: 'line'
      }
    ]
  });

  private timeout;
  private subscription: Subscription;

  constructor(private logService: LogService) { }

  ngOnInit() {
    let entries = this.logService.getMotors();
    this.subscription = entries.subscribe(x => this.addEntry(x));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  addEntry(entry: MotorsLogEntry): void {
    this.addPoint(this.chartMotors, entry, x => x.leftMotor, 0);
    this.addPoint(this.chartMotors, entry, x => x.rightMotor, 1);
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.chartMotors.ref.redraw(true)
    }, 300);
  }

  private addPoint(chart: Chart, entry: MotorsLogEntry, valueGetter: (entry: MotorsLogEntry) => number, serieIndex: number): void {
    let point = new Point();
    point.x = entry.timestamp;
    point.y = valueGetter(entry);
    chart.addPoint(point, serieIndex, false, false);
  }
}
