import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationStatusComponent } from './navigation-status.component';

describe('NavigationStatusComponent', () => {
  let component: NavigationStatusComponent;
  let fixture: ComponentFixture<NavigationStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
