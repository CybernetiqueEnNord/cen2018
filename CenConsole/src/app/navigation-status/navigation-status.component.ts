import { Component, OnInit } from '@angular/core';
import { NavigationPosition } from '../navigation-position';
import { State, ObstacleLogEntry } from '../log-entry';

@Component({
  selector: 'cen-navigation-status',
  templateUrl: './navigation-status.component.html',
  styleUrls: ['./navigation-status.component.css']
})
export class NavigationStatusComponent implements OnInit {
  public position: NavigationPosition = null;
  public state: State = null;
  public obstacle: ObstacleLogEntry = null;

  constructor() { }

  ngOnInit() {
  }

  public get stateText(): string { return State[this.state]; }
}
