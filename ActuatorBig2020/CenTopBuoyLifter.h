/*
 * CenServo.h
 *
 *  Created on: 16 nov. 2019
 *      Author: zoro
 */

#ifndef CENHIGHTGLASSLIFTER_H_
#define CENHIGHTGLASSLIFTER_H_

#include "CenServo_PCA9685.h"
#include "FastLED.h"
#include "../Commons/i2c/ActuatorBig2020Commands.h"

class CenTopBuoyLifter {
public:
	CenTopBuoyLifter(CenServo_PCA9685 &servoArm, CenServo_PCA9685 &servoFinger, int pinSwitchSensor, CRGB &led);

public:
	int pinSwitchSensor; //Channel number of the PCA9685 on which the servo is plugged in.
	CenServo_PCA9685 &servoArm;
	CenServo_PCA9685 &servoFinger;
	CRGB &led;

	CenGlassColor color = CenGlassColor::undefined;


	bool getSensorStatus();
	void updateLed(bool sensorStatus);

};

#endif /* CENHIGHTGLASSLIFTER_H_ */
