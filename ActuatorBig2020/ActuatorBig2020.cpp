/*
 * Actuator 2020
 *
 *  Created on: 13/11/2019
 *      Author: Robin Ouallet
 */
#include "ActuatorBig2020.h"

SoftWire masterSoftWire;
Big2020 big2020(I2C_ADDRESS_ACTUATORBIG2020, masterSoftWire);

PCA9685 pwmController1;
PCA9685 pwmController2;

// 											down  center  up
CenServo_PCA9685 servoArm0(pwmController1, 3, { 500, 300, 55 }); // reversed
CenServo_PCA9685 servoArm1(pwmController1, 4, { 55, 300, 550 });
CenServo_PCA9685 servoArm2(pwmController1, 14, { 55, 300, 550 });
CenServo_PCA9685 servoArm3(pwmController1, 11, { 55, 300, 550 });
CenServo_PCA9685 servoArm4(pwmController1, 9, { 55, 300, 550 });
CenServo_PCA9685 servoArm5(pwmController2, 1, { 500, 300, 55 });
CenServo_PCA9685 servoArm6(pwmController2, 4, { 55, 300, 500 });
CenServo_PCA9685 servoArm7(pwmController2, 8, { 80, 300, 500 });
CenServo_PCA9685 servoArm8(pwmController2, 12, { 120, 300, 500 });
CenServo_PCA9685 servoArm9(pwmController2, 14, { 120, 300, 500 });

//												   closed    open
CenServo_PCA9685 servoFinger0(pwmController1, 1, { 220, 300, 460 });
CenServo_PCA9685 servoFinger1(pwmController1, 2, { 360, 200, 130 });
CenServo_PCA9685 servoFinger2(pwmController1, 15, { 380, 200, 150 });
CenServo_PCA9685 servoFinger3(pwmController1, 12, { 410, 300, 180 });
CenServo_PCA9685 servoFinger4(pwmController1, 10, { 430, 300, 180 });
CenServo_PCA9685 servoFinger5(pwmController2, 5, { 180, 300, 450 });
CenServo_PCA9685 servoFinger6(pwmController2, 0, { 330, 300, 100 });
CenServo_PCA9685 servoFinger7(pwmController2, 9, { 420, 360, 200 });
CenServo_PCA9685 servoFinger8(pwmController2, 13, { 380, 320, 150 });
CenServo_PCA9685 servoFinger9(pwmController2, 15, { 470, 1000, 200 });

// 														down  center  up
CenServo_PCA9685 servoBotomCatcher0(pwmController1, 0, { 250, 350, 475 });
CenServo_PCA9685 servoBotomCatcher1(pwmController1, 7, { 200, 300, 475 });
CenServo_PCA9685 servoBotomCatcher2(pwmController1, 8, { 450, 300, 200 });
CenServo_PCA9685 servoBotomCatcher3(pwmController1, 13, { 500, 400, 300 });
CenServo_PCA9685 servoBotomCatcher4(pwmController2, 3, { 130, 200, 330 });
CenServo_PCA9685 servoBotomCatcher5(pwmController2, 2, { 360, 300, 215 });
CenServo_PCA9685 servoBotomCatcher6(pwmController2, 11, { 190, 300, 330 });
CenServo_PCA9685 servoBotomCatcher7(pwmController2, 10, { 470, 350, 250 });

CenBottomBuoyCatcher BottomBuoyCatcherTab[] = { { servoBotomCatcher0, proximitySensors[0], lifterLeds[16], lifterLeds[8] }, { servoBotomCatcher1, proximitySensors[1], lifterLeds[17], lifterLeds[8] }, { servoBotomCatcher2, proximitySensors[2], lifterLeds[18], lifterLeds[8] }, { servoBotomCatcher3, proximitySensors[3], lifterLeds[19], lifterLeds[8] }, { servoBotomCatcher4, proximitySensors[4], lifterLeds[20], lifterLeds[8] }, { servoBotomCatcher5, proximitySensors[5], lifterLeds[21], lifterLeds[8] }, { servoBotomCatcher6, proximitySensors[6], lifterLeds[22], lifterLeds[8] }, { servoBotomCatcher7, proximitySensors[7], lifterLeds[23], lifterLeds[8] } };

CenTopBuoyLifter TopBuoyLifterTab[] = {
		{ servoArm0, servoFinger0, 46, lifterLeds[0] },
		{ servoArm1, servoFinger1, 47, lifterLeds[1] },
		{ servoArm2, servoFinger2, 48, lifterLeds[2] },
		{ servoArm3, servoFinger3, 49, lifterLeds[3] },
		{ servoArm4, servoFinger4,  44, lifterLeds[4] },
		{ servoArm5, servoFinger5, 69, lifterLeds[9] },  //A15
		{ servoArm6, servoFinger6, 68, lifterLeds[10] }, //A14
		{ servoArm7, servoFinger7, 67, lifterLeds[11] },  //A13
		{ servoArm8, servoFinger8, 65, lifterLeds[12] },  //A11
		{ servoArm9, servoFinger9, 66, lifterLeds[13] } };  //A12


CenServo_PCA9685 servoFlags(pwmController1, 6, { 165, 0, 335 });

// 														down  center  up
CenServo_PCA9685 servoLateralArmLeft(pwmController2, 6, { 120, 285, 490, 115 }); //down: manche a air, center: phare, up: rentr�. working: balayage

CenServo_PCA9685 servoLateralArmRight(pwmController1, 5, { 400, 340, 140, 500 });

SensorVL6180X proximitySensors[8];

void setupPins() {
	digitalWrite(PIN_PCA9685_1_OUTPUT_ENABLE, HIGH); // OE pin: pwm active if LOW. Rq: there is pull up on the board.
	digitalWrite(PIN_PCA9685_2_OUTPUT_ENABLE, HIGH);

	pinMode(SDA, INPUT_PULLUP);
	pinMode(SCL, INPUT_PULLUP);

	pinMode(PIN_PCA9685_1_OUTPUT_ENABLE, OUTPUT);
	pinMode(PIN_PCA9685_2_OUTPUT_ENABLE, OUTPUT);

	//Set all switch sensor pin to input.
	for (int i = PIN_HIGHT_GLASS_SENSOR_START; i < PIN_HIGHT_GLASS_SENSOR_START + PIN_HIGHT_GLASS_SENSOR_COUNT; i++) {
		pinMode(i, INPUT);
	}

	FastLED.addLeds<WS2812, LIFTER_LED_PIN, GRB>(lifterLeds, NUM_LEDS_LIFTER);
	FastLED.setBrightness(2);

}

void setupProximitySensors() {

	for (unsigned int i = 0; i < ARRAY_SIZE(proximitySensors); i++) {
		digitalWrite(PIN_PROXIMITYSENSOR_XSHUT_BASE + i, LOW);
		pinMode(PIN_PROXIMITYSENSOR_XSHUT_BASE + i, OUTPUT);
	}

	for (unsigned int i = 0; i < ARRAY_SIZE(proximitySensors); i++) {
		SensorVL6180XConfig config(i, I2C_PROXIMITYSENSOR_ADDRESS_BASE + i, PIN_PROXIMITYSENSOR_XSHUT_BASE + i);
		proximitySensors[i].setConfig(config);
		proximitySensors[i].initialize<SoftWire>(masterSoftWire);
	}
}

void setup() {

	setupPins();

	Serial.begin(115200);
	Serial.setTimeout(3);
	Wire.begin();

	big2020.init(); // output version string on serial.

	pwmController1.resetDevices();
	pwmController1.init(B000000);
	pwmController2.init(B001010); //Warning, if using software I2C; make *2 on the adress.

	pwmController1.setPWMFrequency(50);
	pwmController2.setPWMFrequency(50);

	setupProximitySensors();

	big2020.handleStartPosition();

	digitalWrite(PIN_PCA9685_1_OUTPUT_ENABLE, LOW); // OE pin: pwm active if LOW. Rq: there is pull up on the board.
	digitalWrite(PIN_PCA9685_2_OUTPUT_ENABLE, LOW);

	for (unsigned int i = 0; i < ARRAY_SIZE(BottomBuoyCatcherTab); i++) {
		BottomBuoyCatcherTab[i].servoCatcher.setPosition(ServoPosition::Deactivated);
	}

	for (int i = 0; i < 600; i++) {
		fill_rainbow(lifterLeds, NUM_LEDS_LIFTER, i, 16);
		FastLED.show();
		delay(2);
	}
	fill_solid(lifterLeds, NUM_LEDS_LIFTER, CRGB::Black);
	FastLED.show();

}

void loop() {
	big2020.loop();
}

