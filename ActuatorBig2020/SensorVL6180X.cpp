#include "SensorVL6180X.h"

SensorVL6180XConfig::SensorVL6180XConfig() {
}

SensorVL6180XConfig::SensorVL6180XConfig(uint8_t index, uint8_t address, uint8_t xshutPin) :
		index(index), address(address), xshutPin(xshutPin) {
}

void SensorVL6180XConfig::clear() {
	address = 0;
}

SensorVL6180X::SensorVL6180X() {
}

const SensorVL6180XConfig& SensorVL6180X::getConfig() const {
	return config;
}

void SensorVL6180X::setConfig(SensorVL6180XConfig &config) {
	this->config = config;
	enabled = true;
}

void SensorVL6180X::setEnabled(bool value) {
	enabled = value;
}

bool SensorVL6180X::isEnabled() const {
	return enabled && isConfigured();
}

bool SensorVL6180X::isConfigured() const {
	return config.address != 0;
}

void SensorVL6180X::clearConfiguration() {
	config.clear();
	enabled = true;
}

void SensorVL6180X::setMeasure(unsigned int distance, unsigned int ambientLight) {
	measure.timestamp = millis();
	measure.distance = distance;
	measure.ambientLight = ambientLight;
}


const SensorVL6180XMeasure& SensorVL6180X::getMeasure() const {
	return measure;
}

void SensorVL6180X::setPinModeAndValue(uint8_t index, uint8_t mode, uint8_t value) {
	digitalWrite(index, value);
	pinMode(index, mode);
}

void SensorVL6180X::setShutdown(uint8_t pin, bool value) {
	if (value) {
		// extinction : mise � 0V
		setPinModeAndValue(pin, OUTPUT, LOW);
	} else {
		// allumage : utilisation des pull-ups
		setPinModeAndValue(pin, INPUT_PULLUP, LOW);
	}
}

void SensorVL6180X::printHex(uint8_t value, bool addNewLine) {
	if (value < 16) {
		Serial.print('0');
	}
	Serial.print(value, HEX);
	if (addNewLine) {
		Serial.println();
	}
}
