#include "Big2020.h"

#include <avr/pgmspace.h>
#include <Arduino.h>
#include <HardwareSerial.h>
#include <stdint.h>
#include <WString.h>

#define log(x) Serial.println(x)

PROGMEM const char *const Actuator::versionString = "vBig2020 " __DATE__ " - " __TIME__;

Big2020::Big2020(uint8_t address, SoftWire &master) :
		Actuator(), i2cSlaveAddress(address), master(master) {
}

uint8_t Big2020::slaveParametersCount = 0;

uint8_t Big2020::slaveParameters[8];

bool Big2020::i2cAcknowledged = false;

void Big2020::initializeSlaveI2C() {
	slave.begin(i2cSlaveAddress);
	slave.onRequest(i2cHandleSlaveRequest);
	slave.onReceive(i2cHandleSlaveInput);
}

void Big2020::initializeMasterI2C() {
	master.begin();
}

void Big2020::initializeActuators() {
}

void Big2020::runInitialization() {
	Actuator::runInitialization();
	initializeSlaveI2C();
	initializeMasterI2C();
	initializeActuators();
}

void Big2020::runStart() {
	Actuator::runStart();
}

void Big2020::runMain() {
	Actuator::runMain();
	handleAction();
	BuoyNumber++;
	if (BuoyNumber >= ARRAY_SIZE(BottomBuoyCatcherTab)) {
		BuoyNumber = 0;
	}
	handleBuoy(BuoyNumber);
}

void Big2020::runDebug() {
	Actuator::runDebug();
	uint8_t count = slaveParametersCount;
	if (count > 0) {
		Serial.print(F("i2c buffer: "));
		for (uint8_t i = 0; i < count; i++) {
			Serial.print((char) slaveParameters[i]);
		}
		Serial.println();
		slaveParametersCount = 0;
	}
	handleAction();
	BuoyNumber++;
	if (BuoyNumber >= ARRAY_SIZE(BottomBuoyCatcherTab)) {
		BuoyNumber = 0;
	}
	handleBuoy(BuoyNumber);
}

void Big2020::i2cHandleSlaveInput(int bytesCount) {
	unsigned int i = 0;
	while (slave.available() > 0 && i < min(ARRAY_SIZE(slaveParameters), (unsigned ) bytesCount)) {
		char c = slave.read();
		slaveParameters[i++] = c;
	}
	slaveParametersCount = i;
}

void Big2020::i2cHandleSlaveRequest() {
	if (slaveParametersCount != 0) {
		i2cHandleCommand();
		slaveParametersCount = 0;
	}
}

bool Big2020::isBusy() const {
	return command != Action::None;
}

void Big2020::i2cSendStatus() {
	i2cAcknowledged = true;
	slave.write(command == Action::None ? I2C_STATUS_READY : I2C_STATUS_BUSY);
}

void Big2020::i2cHandleCommand() {
	if (slaveParametersCount == 0) {
		big2020.i2cSendStatus();
	} else if (slaveParameters[0] == I2C_COMMAND_STATUS) {
		big2020.i2cSendStatus();
	} else {
		if (big2020.isBusy()) {
			return;
		}
		i2cAcknowledged = false;

		big2020.switchI2COrSerialCommand(true, slaveParameters, slaveParametersCount);

	}
}

void Big2020::enterDebug() {
	Actuator::enterDebug();
}

void Big2020::i2cScanMasterBus() {
	Serial.println(F(":scanning I2C bus"));
	for (uint8_t address = 1; address < 127; address++) {
		// The i2c_scanner uses the return value of
		// the Write.endTransmisstion to see if
		// a device did acknowledge to the address.
		master.beginTransmission(address);
		uint8_t error = master.endTransmission();

		if (error == 0) {
			Serial.print('D');
			serialPrintHex(address);
		}
	}
	serialPrintSuccess();
}

void Big2020::printI2CAddress() {
	Serial.print('a');
	serialPrintHex(i2cSlaveAddress);
}

void Big2020::serialParseInput(const char *buffer, int length) {
	switchI2COrSerialCommand(false, buffer, length);
}

void Big2020::switchI2COrSerialCommand(bool isI2C, const char *buffer, int length) {
	switch (buffer[0]) {
		case COMMAND_SERIAL_I2C_SCAN:
			i2cScanMasterBus();
			break;
		case COMMAND_SERIAL_I2C_ADDRESS:
			printI2CAddress();
			break;

		case I2C_COMMAND_10_SECOND_WAIT_READY_TEST:
			big2020.command = Action::WaitReadyTest;
			break;

		case I2C_COMMAND_SET_COLOR_BLUE:
			big2020.color = Color::Blue;
			big2020.command = Action::SetInitialPosition;
			break;
		case I2C_COMMAND_SET_COLOR_YELLOW:
			big2020.color = Color::Yellow;
			big2020.command = Action::SetInitialPosition;
			break;

		case I2C_COMMAND_SERVO:
			big2020.command = Action::Servo;
			if (isI2C) {
				strncpy(big2020.commandParameters, (const char*) (big2020.slaveParameters + 1), ARRAY_SIZE(big2020.commandParameters));
				big2020.commandParameters[big2020.slaveParametersCount - 1] = 0;
			} else {
				strncpy(big2020.commandParameters, buffer + 1, ARRAY_SIZE(big2020.commandParameters));
			}
			break;

		case COMMAND_SET_CODE:
			big2020.command = Action::SetCode;
			big2020.commandParameters[0] = buffer[2] - '0';
			break;

		case I2C_COMMAND_START_POSITION:
			big2020.command = Action::SetInitialPosition;
			break;
		case I2C_COMMAND_TEST_ALL_SERVO_ARE_WORKING:
			big2020.command = Action::TestAllServoAreWorking;
			break;
		case I2C_COMMAND_DANCE:
			big2020.command = Action::TestHandleDance;
			break;

		case COMMAND_FRONT_RELEASE:
			big2020.command = Action::FrontRelease;
			break;
		case COMMAND_REAR_RELEASE:
			big2020.command = Action::RearRelease;
			break;

		case COMMAND_FRONT_DOWN:
			big2020.command = Action::FrontDown;
			break;
		case COMMAND_REAR_DOWN:
			big2020.command = Action::RearDown;
			break;

		case COMMAND_SET_ARM:
			if (isI2C) {
				strncpy(big2020.commandParameters, (const char*) (big2020.slaveParameters + 1), ARRAY_SIZE(big2020.commandParameters));
				big2020.commandParameters[big2020.slaveParametersCount - 1] = 0;
			} else {
				strncpy(big2020.commandParameters, buffer + 1, ARRAY_SIZE(big2020.commandParameters));
			}
			big2020.command = Action::SetArm;
			break;

		case COMMAND_BOTTOM_BOTTOM_BUOY_REAR_RELEASE_DISARM_AND_RESET:
			big2020.command = Action::RearBottomBuoyReleaseAndDisarm;
			break;
		case COMMAND_BOTTOM_BOTTOM_BUOY_FRONT_RELEASE_DISARM_AND_RESET:
			big2020.command = Action::FrontBottomBuoyReleaseAndDisarm;
			break;

		case COMMAND_BOTTOM_BOTTOM_BUOY_REAR_ARM:
			big2020.command = Action::RearBottomBuoyArm;
			break;
		case COMMAND_BOTTOM_BOTTOM_BUOY_FRONT_ARM:
			big2020.command = Action::FrontBottomBuoyArm;
			break;

		case COMMAND_HOLD:
			big2020.command = Action::Hold;
			break;
		case COMMAND_RAISE:
			big2020.command = Action::Raise;
			break;
		case COMMAND_FLAGS:
			big2020.command = Action::RaiseFlags;
			break;
		case COMMAND_LATERAL_ARM_MANCHE_A_AIR_OUT:
			big2020.command = Action::LateralArmMancheAAirOut;
			break;
		case COMMAND_LATERAL_ARM_MANCHE_A_AIR_RETRACTED:
			big2020.command = Action::LateralArmMancheAAirRetracted;
			break;
		case COMMAND_LATERAL_ARM_PHARE_OUT:
			big2020.command = Action::LateralArmPhareOut;
			break;
		case COMMAND_LATERAL_ARM_PHARE_RETRACTED:
			big2020.command = Action::LateralArmPhareRetracted;
			break;

		case COMMAND_LATERAL_ARM_PUSH_BUOY:
			big2020.command = Action::ArmPushBuoy;
			break;
		case COMMAND_LATERAL_ARM_PUSH_BUOY_RETRACTED:
			big2020.command = Action::ArmPushBuoyRetracted;
			break;

		case COMMAND_READY_TO_TAKE:
			big2020.command = Action::ReadyToPickup;
			strncpy(big2020.commandParameters, buffer + 1, ARRAY_SIZE(big2020.commandParameters));
//			if (isI2C) {
//				strncpy(big2020.commandParameters, (const char*) (big2020.slaveParameters + 1), ARRAY_SIZE(big2020.commandParameters));
//				big2020.commandParameters[big2020.slaveParametersCount - 1] = 0;
//			} else {
//
//			}
			break;

		case COMMAND_READY_TO_TAKE_INDIVIDUAL:
			big2020.command = Action::ReadyToPickupIndividual;
			if (isI2C) {
				strncpy(big2020.commandParameters, (const char*) (big2020.slaveParameters + 1), ARRAY_SIZE(big2020.commandParameters));
				big2020.commandParameters[big2020.slaveParametersCount - 1] = 0;
			} else {
				strncpy(big2020.commandParameters, buffer + 1, ARRAY_SIZE(big2020.commandParameters));
			}
			break;

		case I2C_COMMAND_KILL:
			big2020.command = Action::Kill;
			break;

		case COMMAND_GET_TOP_BUOY_DATA: {
			uint8_t data_front = 0;
			uint8_t data_rear = 0;
			//0bxxx43210: presence / absence d'une bouée front
			for (int i = 0; i < 5; i++) {
				data_front = data_front | (TopBuoyLifterTab[i].getSensorStatus() << i);
			}
			//0bxxx98765: presence / absence d'une bouée rear
			for (int i = 0; i < 5; i++) {
				data_rear = data_rear | (TopBuoyLifterTab[i + 5].getSensorStatus() << i);
			}

			if (isI2C) {
				slave.write(data_front);
				slave.write(data_rear);
			} else {
				Serial.println(data_front, BIN);
				Serial.println(data_rear, BIN);
			}
			break;
		}

		case COMMAND_GET_BOTTOM_BUOY_DATA: {
			uint8_t data_front = 0;
			uint8_t data_rear = 0;
			//0bxxxx3210: presence / absence d'une bouée front
			for (int i = 0; i < 4; i++) {
				data_front = data_front | ((BottomBuoyCatcherTab[i].getStatus() == BottomBuoyStatus::Locked) << i);
			}

			for (int i = 0; i < 4; i++) {
				data_rear = data_rear | ((BottomBuoyCatcherTab[i + 4].getStatus() == BottomBuoyStatus::Locked) << i);
			}

			if (isI2C) {
				slave.write(data_front);
				slave.write(data_rear);
			} else {
				Serial.println(data_front, BIN);
				Serial.println(data_rear, BIN);
			}
			break;
		}

		case COMMAND_SENSOR_STATUS: {
			Serial.println("UpperGlass:");

			for (unsigned int i = 0; i < 10; i++) {
				Serial.println(TopBuoyLifterTab[i].getSensorStatus());
			}

			Serial.println();
			Serial.println("LowerGlass:");

			for (unsigned int i = 0; i < ARRAY_SIZE(proximitySensors); i++) {
				Serial.println(proximitySensors[i].read<SoftWire>(master).distance);
			}

			for (unsigned int i = 0; i < ARRAY_SIZE(proximitySensors); i++) {
				Serial.println(proximitySensors[i].read<SoftWire>(master).distance);
			}

			for (int i = 0; i < 10; i++) {

				switch (TopBuoyLifterTab[i].servoArm.getServoPosition()) {
					case ServoPosition::Center:
						Serial.println("Arm Center");
						break;
					case ServoPosition::Up:
						Serial.println("Arm Up");
						break;
					case ServoPosition::Down:
						Serial.println("Arm Down");
						break;
					case ServoPosition::Deactivated:
						Serial.println("Arm Deactivated");
						break;
				}
				switch (TopBuoyLifterTab[i].servoFinger.getServoPosition()) {
					case ServoPosition::Center:
						Serial.println("servoFinger Center");
						break;
					case ServoPosition::Up:
						Serial.println("servoFinger Up");
						break;
					case ServoPosition::Down:
						Serial.println("servoFinger Down");
						break;
					case ServoPosition::Deactivated:
						Serial.println("servoFinger Deactivated");
						break;
				}

			}

			// Front rack, format 0bUUU43210
			//slave.write(Lifter0.getSensorStatus() + Lifter1.getSensorStatus() << 1 + Lifter2.getSensorStatus() << 2 + Lifter3.getSensorStatus() << 3 + Lifter4.getSensorStatus() << 4);
			//slave.println();
			// Rear rack,  format 0bUUU98765
			//slave.write(Lifter5.getSensorStatus() + Lifter6.getSensorStatus() << 1 + Lifter7.getSensorStatus() << 2 + Lifter8.getSensorStatus() << 3 + Lifter9.getSensorStatus() << 4);
			//slave.println();

			break;
		}

		default:
			if (!isI2C) {
				Actuator::serialParseInput(buffer, length);
			}
			break;
	}
}

void Big2020::handleAction() {
	switch (command) {
		case Action::None:
			break;
		case Action::Kill:
			kill();
			break;
		case Action::Servo:
			handleServoCommand();
			break;
		case Action::SetCode:
			handleCodeCommand();
			break;

		case Action::WaitReadyTest:
			Serial.println("Busy");
			delay(10000);
			Serial.println("Ready");
			break;

		case Action::SetInitialPosition:
			handleStartPosition();
			break;
		case Action::TestAllServoAreWorking:
			handleTestAllServoAreWorking();
			break;
		case Action::TestHandleDance:
			handleDance();
			break;
		case Action::FrontRelease:
			handleRelease(FrontRear::Front);
			break;
		case Action::RearRelease:
			handleRelease(FrontRear::Rear);
			break;
		case Action::FrontDown:
			handleSetDown(FrontRear::Front);
			break;
		case Action::RearDown:
			handleSetDown(FrontRear::Rear);
			break;

		case Action::Hold:
			handleHold();
			break;
		case Action::Raise:
			handleRaise();
			break;
		case Action::ReadyToPickup:
			handleReadyToPickup();
			break;
		case Action::ReadyToPickupIndividual:
			handleReadyToPickupIndividual();
			break;
		case Action::RaiseFlags:
			handleRaiseFlags();
			break;
		case Action::LateralArmPhareRetracted:
			handleLateralArmPhareRetracted();
			break;
		case Action::LateralArmPhareOut:
			handleLateralArmPhareOut();
			break;
		case Action::LateralArmMancheAAirRetracted:
			handleLateralArmMancheAAirRetracted();
			break;
		case Action::LateralArmMancheAAirOut:
			handleLateralArmMancheAAirOut();
			break;
		case Action::FrontBottomBuoyReleaseAndDisarm:
			handleReleaseBottomBuoy(FrontRear::Front);
			break;
		case Action::RearBottomBuoyReleaseAndDisarm:
			handleReleaseBottomBuoy(FrontRear::Rear);
			break;
		case Action::FrontBottomBuoyArm:
			handleArmBottomBuoy(FrontRear::Front);
			break;
		case Action::RearBottomBuoyArm:
			handleArmBottomBuoy(FrontRear::Rear);
			break;
		case Action::SetArm:
			handleSetArm();
			break;
		case Action::ArmPushBuoy:
			handleSetPushBuoy();
			break;
		case Action::ArmPushBuoyRetracted:
			handleSetPushBuoyRetracted();
			break;

	}

	if (command != Action::None) {
		for (int i = 0; !i2cAcknowledged && i < 200; i++) {
			delay(1);
		}
		command = Action::None;
	}
}

void Big2020::kill() {
	digitalWrite(PIN_PCA9685_1_OUTPUT_ENABLE, HIGH); // OE pin: pwm active if LOW. Rq: there is pull up on the board.
	digitalWrite(PIN_PCA9685_2_OUTPUT_ENABLE, HIGH);

	FastLED.clear();

	slave.end();
}

//Unitzary command to a Servo, use only in integration.
void Big2020::handleServoCommand() {
	char *tok = strtok(commandParameters, ";\n");
	int index = 0;
	int servoIndex = 0, servoType = 0;
	int servoPosition = 0;
	while (tok) {
		switch (index) {
			case 0:
				servoType = atoi(tok);
				break;

			case 1:
				servoIndex = atoi(tok);
				break;

			case 2:
				servoPosition = atoi(tok);
				break;

			case 3:
				goto done;
				break;
		}
		index++;
		tok = strtok(NULL, ";\n");
	}
	done: switch (servoType) {
		case 0:
			TopBuoyLifterTab[servoIndex].servoArm.setPositionRaw(servoPosition);
			break;
		case 1:
			TopBuoyLifterTab[servoIndex].servoFinger.setPositionRaw(servoPosition);
			break;

		case 2:
			switch (servoIndex) {
				case 0:
					servoLateralArmLeft.setPositionRaw(servoPosition);
					break;
				case 1:
					servoLateralArmRight.setPositionRaw(servoPosition);
					break;
			}
			break;

		case 3:
			BottomBuoyCatcherTab[servoIndex].servoCatcher.setPositionRaw(servoPosition);
			break;
	}
}

void Big2020::handleReadyToPickupIndividual() {
	FrontRear frontOrRear = FrontRear::Front;

	//w<FrontRear>01010

	frontOrRear = static_cast<FrontRear>(commandParameters[0] - '0');

	int addIndex = 0;
	if (frontOrRear == FrontRear::Rear) {
		addIndex = 5;
	}

	int servoIndexList[5];
	int servoCount = 0;
	for (unsigned int i = 0; i < ARRAY_SIZE(servoIndexList); i++) {
		if (commandParameters[1 + i] == '1') {
			servoIndexList[servoCount] = i + addIndex;
			servoCount++;
		}
	}
	this->setServoListReadyToPickup(servoIndexList, servoCount);
}

void Big2020::handleReadyToPickup() {

	FrontRear frontOrRear = FrontRear::Front;
	FirstOrComplement firstOrComplement = FirstOrComplement::FirstOponent;

	frontOrRear = ((commandParameters[1] - '0') == 0) ? FrontRear::Front : FrontRear::Rear;
	firstOrComplement = static_cast<FirstOrComplement>(commandParameters[3] - '0');

	int addIndex = 0;

	if (frontOrRear == FrontRear::Rear) {
		addIndex = 5;
	}

	switch (big2020.code) {
		case Code::RVRVR:
			if (frontOrRear == FrontRear::Front) {
			int servoIndexList[5] = { 0, 1, 2, 3, 4 };
			this->setServoListReadyToPickup(servoIndexList, 5);
			} else {
				int servoIndexList[5] = { 5, 6, 7, 8, 9 };
				this->setServoListReadyToPickup(servoIndexList, 5);
			}
			break;

		case Code::VVVRR_VVRRR_3:
			if (this->color == Color::Blue) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue FirstOponent=Right side of the field: 		_VVRRR
						//								servo index front :  	  		 43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[3] = { 0, 1, 2 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					} else {
						//VVVRR_VVRRR_3 Blue FirstOponent=Right side of the field: 		_VVRRR
						//								servo index rear :  	 		-98765
						//blue + rear, we take the green
						int servoIndexList[2] = { 8, 9 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue ComplementOurSide=Left side of the field: 	 VVVRR_
						//								    servo index front :             43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[2] = { 3, 4 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					} else {
						//VVVRR_VVRRR_3 Blue FirstOponent=Left side of the field: 		   VVVRR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						int servoIndexList[3] = { 5, 6, 7 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue FirstOurSide=Left side of the field: 	     VVVRR_
						//								    servo index front :          43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[2] = { 0, 1 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					} else {
						//VVVRR_VVRRR_3 Blue FirstOurSide=Left side of the field: 		 VVVRR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						int servoIndexList[3] = { 7, 8, 9 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue ComplementOponent=Right side of the field: 	_VVRRR
						//									servo index front :  	  		   43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[3] = { 2, 3, 4 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					} else {
						//VVVRR_VVRRR_3 Blue ComplementOponent=Right side of the field: 	   _VVRRR
						//									servo index rear :  	 		-98765
						//blue + rear, we take the green
						int servoIndexList[2] = { 5, 6 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					}
				} else {
					Serial.println("Error, handleReadyToPickup called to an unknown FirstOrComplement");
				}
			} else if (this->color == Color::Yellow) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Left side of the field: 		VVVRR_
						//										servo index front :  	43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[3] = { 2, 3, 4 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					} else {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Left side of the field: 		 VVVRR_
						//										servo index rear :  	-98765
						//yellow + rear, we take the red
						int servoIndexList[2] = { 5, 6 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Right side of the field: 		    _VVRRR
						//								servo index front :  	  		  43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[2] = { 0, 1 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					} else {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Right side of the field: 		_VVRRR
						//								servo index front : 	      	  -98765
						//yellow + rear, we take the red
						int servoIndexList[3] = { 7, 8, 9 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow FirstOurSide=Right side of the field: 		 _VVRRR
						//								servo index front :  	  		  43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[2] = { 3, 4 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					} else {
						//VVVRR_VVRRR_3 Yellow FirstOurSide=Right side of the field: 		_VVRRR
						//								servo index front : 	      	-98765
						//yellow + rear, we take the red
						int servoIndexList[3] = { 5, 6, 7 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow ComplementOponent=Left side of the field: 		VVVRR_
						//										servo index front :  	  43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[3] = { 0, 1, 2 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					} else {
						//VVVRR_VVRRR_3 Yellow ComplementOponent=Left side of the field: 		 VVVRR_
						//											servo index rear :  	   -98765
						//yellow + rear, we take the red
						int servoIndexList[2] = { 8, 9 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					}
				} else {
					Serial.println("Error, handleReadyToPickup called to an unknown FirstOrComplement");
				}
			}
			break;
		case Code::VVRVR_VRVRR_2:

			if (this->color == Color::Blue) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Blue FirstOponent=Right side of the field: 		_VRVRR
						//								servo index front :  	  		 43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[3] = { 0, 1, 3 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					} else {
						//VVRVR_VRVRR_2 Blue FirstOponent=Right side of the field: 		  _VRVRR
						//								servo index rear :  	 		-98765
						//blue + rear, we take the green
						int servoIndexList[2] = { 5, 7 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Blue ComplementOurSide=Left side of the field: 	  VVRVR_
						//								    servo index front :             43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[2] = { 2, 4 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					} else {
						//VVRVR_VRVRR_2 Blue FirstOponent=Left side of the field: 	     VVRVR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						int servoIndexList[3] = { 6, 8, 9 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Blue FirstOurSide=Left side of the field: 	     VVRVR_
						//								    servo index front :            43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[2] = { 2, 4 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					} else {
						//VVRVR_VRVRR_2 Blue FirstOurSide=Left side of the field: 		 VVRVR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						int servoIndexList[3] = { 6, 8, 9 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Blue ComplementOponent=Right side of the field: 	  _VRVRR
						//								servo index front :  	  		   43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[3] = { 0, 1, 3 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					} else {
						//VVRVR_VRVRR_2 Blue ComplementOponent=Right side of the field: 	   _VRVRR
						//								servo index rear :  	 		 -98765
						//blue + rear, we take the green
						int servoIndexList[2] = { 5, 7 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					}
				} else {
					Serial.println("Error, handleReadyToPickup called to an unknown FirstOrComplement");
				}
			} else if (this->color == Color::Yellow) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Yellow FirstOponent=Left side of the field: 		VVRVR_
						//										servo index front :  	43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[3] = { 4, 3, 1 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					} else {
						//VVRVR_VRVRR_2 Yellow FirstOponent=Left side of the field: 		 VVRVR_
						//										servo index rear :  	  -98765
						//yellow + rear, we take the red
						int servoIndexList[2] = { 7, 9 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Yellow FirstOponent=Right side of the field: 		    _VRVRR
						//								servo index front :  	  		   43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[2] = { 0, 2 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					} else {
						//VVRVR_VRVRR_2 Yellow FirstOponent=Right side of the field: 		 _VRVRR
						//								servo index front : 	      	 -98765
						//yellow + rear, we take the red
						int servoIndexList[3] = { 5, 6, 8 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Yellow FirstOurSide=Right side of the field: 		 _VRVRR
						//								servo index front :  	  		43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[2] = { 0, 2 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					} else {
						//VVRVR_VRVRR_2 Yellow FirstOurSide=Right side of the field: 		_VRVRR
						//								servo index front : 	      	-98765
						//yellow + rear, we take the red
						int servoIndexList[3] = { 5, 6, 8 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVRVR_VRVRR_2 Yellow ComplementOponent=Left side of the field: 		VVRVR_
						//										servo index front :  	    43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[3] = { 1, 3, 4 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					} else {
						//VVRVR_VRVRR_2 Yellow ComplementOponent=Left side of the field: 		  VVRVR_
						//											servo index rear :  	   -98765
						//yellow + rear, we take the red
						int servoIndexList[2] = { 7, 9 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					}
				} else {
					Serial.println("Error, handleReadyToPickup called to an unknown FirstOrComplement");
				}
			}

			break;

		case Code::VRVVR_VRRVR_1:

			if (this->color == Color::Blue) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Blue FirstOponent=Right side of the field: 		_VRRVR
						//								servo index front :  	  		 43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[3] = { 0, 2, 3 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					} else {
						//VRVVR_VRRVR_1 Blue FirstOponent=Right side of the field: 		  _VRRVR
						//								servo index rear :  	 		 -98765
						//blue + rear, we take the green
						int servoIndexList[2] = { 5, 8 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Blue ComplementOurSide=Left side of the field: 	  VRVVR_
						//								    servo index front :            43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[2] = { 1, 4 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					} else {
						//VRVVR_VRRVR_1 Blue FirstOponent=Left side of the field: 	     VRVVR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						int servoIndexList[3] = { 6, 7, 9 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Blue FirstOurSide=Left side of the field: 	     VRVVR_
						//								    servo index front :           43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[2] = { 1, 4 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					} else {
						//VRVVR_VRRVR_1 Blue FirstOurSide=Left side of the field: 		 VRVVR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						int servoIndexList[3] = { 6, 7, 9 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Blue ComplementOponent=Right side of the field: 	  _VRRVR
						//								servo index front :  	  		   43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[3] = { 0, 2, 3 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					} else {
						//VRVVR_VRRVR_1 Blue ComplementOponent=Right side of the field: 	   _VRRVR
						//								servo index rear :  	 		  -98765
						//blue + rear, we take the green
						int servoIndexList[2] = { 5, 8 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					}
				} else {
					Serial.println("Error, handleReadyToPickup called to an unknown FirstOrComplement");
				}
			} else if (this->color == Color::Yellow) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Yellow FirstOponent=Left side of the field: 		VRVVR_
						//										servo index front :  	43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[3] = { 4, 2, 1 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					} else {
						//VRVVR_VRRVR_1 Yellow FirstOponent=Left side of the field: 		 VRVVR_
						//										servo index rear :  	 -98765
						//yellow + rear, we take the red
						int servoIndexList[2] = { 6, 9 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Yellow FirstOponent=Right side of the field: 		    _VRRVR
						//								servo index front :  	  		    43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[2] = { 0, 3 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					} else {
						//VRVVR_VRRVR_1 Yellow FirstOponent=Right side of the field: 		 _VRRVR
						//								servo index front : 	      	 -98765
						//yellow + rear, we take the red
						int servoIndexList[3] = { 5, 7, 8 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Yellow FirstOurSide=Right side of the field: 		 _VRRVR
						//								servo index front :  	  		 43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[2] = { 0, 3 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					} else {
						//VRVVR_VRRVR_1 Yellow FirstOurSide=Right side of the field: 		_VRRVR
						//								servo index front : 	      	-98765
						//yellow + rear, we take the red
						int servoIndexList[3] = { 5, 7, 8 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VRVVR_VRRVR_1 Yellow ComplementOponent=Left side of the field: 		VRVVR_
						//										servo index front :  	    43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[3] = { 1, 2, 4 };
						this->setServoListReadyToPickup(servoIndexList, 3);
					} else {
						//VRVVR_VRRVR_1 Yellow ComplementOponent=Left side of the field: 		  VRVVR_
						//											servo index rear :  	  -98765
						//yellow + rear, we take the red
						int servoIndexList[2] = { 6, 9 };
						this->setServoListReadyToPickup(servoIndexList, 2);
					}
				} else {
					Serial.println("Error, handleReadyToPickup called to an unknown FirstOrComplement");
				}
			}
			break;

//		case Code::Undefined:
//		case Code::VVVVR_VRRRR_4:
		default:
			if (this->color == Color::Blue) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue FirstOponent=Right side of the field: 		_VRRRR
						//								servo index front :  	  		 43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[4] = { 0, 1, 2, 3 };
						this->setServoListReadyToPickup(servoIndexList, 4);
					} else {
						//VVVRR_VVRRR_3 Blue FirstOponent=Right side of the field: 		_VRRRR
						//								servo index rear :  	 		-98765
						//blue + rear, we take the green
						int servoIndexList[1] = { 9 };
						this->setServoListReadyToPickup(servoIndexList, 1);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue ComplementOurSide=Left side of the field: 	 VVVVR_
						//								    servo index front :              43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[1] = { 4 };
						this->setServoListReadyToPickup(servoIndexList, 1);
					} else {
						//VVVRR_VVRRR_3 Blue FirstOponent=Left side of the field: 		   VVVVR_
						//								    servo index rear :   		 -98765
						//blue + rear, we take the green
						int servoIndexList[4] = { 5, 6, 7, 8 };
						this->setServoListReadyToPickup(servoIndexList, 4);
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue FirstOurSide=Left side of the field: 	     VVVVR_
						//								    servo index front :          43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[1] = { 0 };
						this->setServoListReadyToPickup(servoIndexList, 1);
					} else {
						//VVVRR_VVRRR_3 Blue FirstOurSide=Left side of the field: 		 VVVVR_
						//								    servo index rear :   		-98765
						//blue + rear, we take the green
						int servoIndexList[4] = { 6, 7, 8, 9 };
						this->setServoListReadyToPickup(servoIndexList, 4);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Blue ComplementOponent=Right side of the field: 	_VRRRR
						//									servo index front :  	  		  43210-
						//blue + front, we take the red (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[4] = { 1, 2, 3, 4 };
						this->setServoListReadyToPickup(servoIndexList, 4);
					} else {
						//VVVRR_VVRRR_3 Blue ComplementOponent=Right side of the field: 	   _VRRRR
						//									servo index rear :  	 	   -98765
						//blue + rear, we take the green
						int servoIndexList[1] = { 5 };
						this->setServoListReadyToPickup(servoIndexList, 1);
					}
				} else {
					Serial.println("Error, handleReadyToPickup called to an unknown FirstOrComplement");
				}
			} else if (this->color == Color::Yellow) {
				//Rq: FirstOponent vas toujours avec ComplementOurSide et    (FirstOurSide // ComplementOponent)
				if (firstOrComplement == FirstOrComplement::FirstOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Left side of the field:		VVVVR_
						//										servo index front :		43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[4] = { 1, 2, 3, 4 };
						this->setServoListReadyToPickup(servoIndexList, 4);
					} else {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Left side of the field:		 VVVVR_
						//										servo index rear :  	-98765
						//yellow + rear, we take the red
						int servoIndexList[1] = { 5 };
						this->setServoListReadyToPickup(servoIndexList, 1);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Right side of the field:		     _VRRRR
						//								servo index front :					  43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[1] = { 0 };
						this->setServoListReadyToPickup(servoIndexList, 1);
					} else {
						//VVVRR_VVRRR_3 Yellow FirstOponent=Right side of the field: 	_VRRRR
						//								servo index front : 	         -98765
						//yellow + rear, we take the red
						int servoIndexList[4] = { 6, 7, 8, 9 };
						this->setServoListReadyToPickup(servoIndexList, 4);
					}
				} else if (firstOrComplement == FirstOrComplement::FirstOurSide) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow FirstOurSide=Right side of the field: 	 _VRRRR
						//								servo index front :  	  		  43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[1] = { 4 };
						this->setServoListReadyToPickup(servoIndexList, 1);
					} else {
						//VVVRR_VVRRR_3 Yellow FirstOurSide=Right side of the field: 	_VRRRR
						//								servo index front : 	      	-98765
						//yellow + rear, we take the red
						int servoIndexList[4] = { 5, 6, 7, 8 };
						this->setServoListReadyToPickup(servoIndexList, 4);
					}
				} else if (firstOrComplement == FirstOrComplement::ComplementOponent) {
					if (frontOrRear == FrontRear::Front) {
						//VVVRR_VVRRR_3 Yellow ComplementOponent=Left side of the field: 		VVVVR_
						//										servo index front :  		   43210-
						//yellow + front, we take the green (rear of the robot will be directed to the phare pour la dépose)
						int servoIndexList[4] = { 0, 1, 2, 3 };
						this->setServoListReadyToPickup(servoIndexList, 4);
					} else {
						//VVVRR_VVRRR_3 Yellow ComplementOponent=Left side of the field: 	 VVVVR_
						//											servo index rear :  	    -98765
						//yellow + rear, we take the red
						int servoIndexList[1] = { 9 };
						this->setServoListReadyToPickup(servoIndexList, 1);
					}
				} else {
					Serial.println("Error, handleReadyToPickup called to an unknown FirstOrComplement");
				}
			}
			break;
	}
}

void Big2020::setServoListReadyToPickup(int servoIndexList[], int size) {
	for (int i = 0; i < size; i++) {
		TopBuoyLifterTab[servoIndexList[i]].servoArm.setPosition(ServoPosition::Down);
		TopBuoyLifterTab[servoIndexList[i]].servoFinger.setPosition(ServoPosition::Up);
	}
	delay(1300);
	for (int i = 0; i < size; i++) {
		TopBuoyLifterTab[servoIndexList[i]].servoArm.setPosition(ServoPosition::Deactivated);
		TopBuoyLifterTab[servoIndexList[i]].servoFinger.setPosition(ServoPosition::Deactivated);

	}
}

void Big2020::handleCodeCommand() {
	this->code = static_cast<Code>(commandParameters[0]);
}

void Big2020::handleStartPosition() {

	servoFlags.setPosition(ServoPosition::Up);
	servoLateralArmLeft.setPosition(ServoPosition::Up);
	servoLateralArmRight.setPosition(ServoPosition::Up);

	for (int i = 0; i < 10; i++) {
		TopBuoyLifterTab[i].servoArm.setPosition(ServoPosition::Up);
		TopBuoyLifterTab[i].servoFinger.setPosition(ServoPosition::Down);
	}
	delay(1000);
	//Deactivate all arms and finger to save power
	servoFlags.setPosition(ServoPosition::Deactivated);
	servoLateralArmLeft.setPosition(ServoPosition::Deactivated);
	servoLateralArmRight.setPosition(ServoPosition::Deactivated);

	for (int i = 0; i < 10; i++) {
		TopBuoyLifterTab[i].servoArm.setPosition(ServoPosition::Deactivated);
		TopBuoyLifterTab[i].servoFinger.setPosition(ServoPosition::Deactivated);
	}

	handleReleaseBottomBuoy(FrontRear::Front);
	handleReleaseBottomBuoy(FrontRear::Rear);

	for (unsigned int i = 0; i < ARRAY_SIZE(BottomBuoyCatcherTab); i++) {
		BottomBuoyCatcherTab[i].disarm();
	}

	delay(300);
	for (unsigned int i = 0; i < ARRAY_SIZE(BottomBuoyCatcherTab); i++) {
		BottomBuoyCatcherTab[i].servoCatcher.setPosition(ServoPosition::Deactivated);
	}
	servoFlags.setPosition(ServoPosition::Down);
}

void Big2020::handleTestAllServoAreWorking() {

	for (unsigned int i = 0; i < ARRAY_SIZE(BottomBuoyCatcherTab); i++) {
		BottomBuoyCatcherTab[i].servoCatcher.setPosition(ServoPosition::Down);
	}

	handleLateralArmPhareOut();
	handleRaiseFlags();

	handleRelease(FrontRear::Front);
	handleRelease(FrontRear::Rear);

	handleStartPosition();
}

void Big2020::handleDance() {

	for (unsigned int i = 0; i < ARRAY_SIZE(TopBuoyLifterTab); i++) {
		TopBuoyLifterTab[i].servoArm.setPosition(ServoPosition::Down);
		delay(100);
		TopBuoyLifterTab[i].servoFinger.setPosition(ServoPosition::Up);
		delay(100);
	}
	for (unsigned int i = 0; i < ARRAY_SIZE(TopBuoyLifterTab); i++) {
		TopBuoyLifterTab[i].servoArm.setPosition(ServoPosition::Up);
		delay(100);
		TopBuoyLifterTab[i].servoFinger.setPosition(ServoPosition::Down);
		delay(100);
	}
	for (unsigned int i = 0; i < ARRAY_SIZE(BottomBuoyCatcherTab); i++) {
		BottomBuoyCatcherTab[i].servoCatcher.setPosition(ServoPosition::Down);
		delay(100);
	}
	for (unsigned int i = 0; i < ARRAY_SIZE(BottomBuoyCatcherTab); i++) {
		BottomBuoyCatcherTab[i].servoCatcher.setPosition(ServoPosition::Up);
		delay(100);
	}
	handleStartPosition();
}

void Big2020::handleSetDown(FrontRear frontOrRear) {
//Set all arm down and open the finger
	int addIndex = 0;
	if (frontOrRear == FrontRear::Rear) {
		addIndex = 5;
	}

	for (int i = 0; i < 5; i++) {
		TopBuoyLifterTab[i + addIndex].servoArm.setPosition(ServoPosition::Down);
	}
	delay(900);
	//Deactivate all Arms
	for (int i = 0; i < 5; i++) {
		TopBuoyLifterTab[i + addIndex].servoArm.setPosition(ServoPosition::Deactivated);
		TopBuoyLifterTab[i].getSensorStatus();
	}
}

void Big2020::handleRelease(FrontRear frontOrRear) {
//Set all arm down and open the finger
	int addIndex = 0;
	if (frontOrRear == FrontRear::Rear) {
		addIndex = 5;
	}

	for (int i = 0; i < 5; i++) {
		TopBuoyLifterTab[i + addIndex].servoArm.setPosition(ServoPosition::Down);
	}
	delay(900);
	for (int i = 0; i < 5; i++) {
		TopBuoyLifterTab[i + addIndex].servoFinger.setPosition(ServoPosition::Up);
	}
	delay(500);
	//Deactivate all Arms
	for (int i = 0; i < 5; i++) {
		TopBuoyLifterTab[i + addIndex].servoArm.setPosition(ServoPosition::Deactivated);
		TopBuoyLifterTab[i].getSensorStatus();
	}
	//Deactivate all fingers
	for (int i = 0; i < 5; i++) {
		TopBuoyLifterTab[i + addIndex].servoFinger.setPosition(ServoPosition::Deactivated);
	}

}

//Set the finger down for all arm that are down.
void Big2020::handleHold() {

	for (int i = 0; i < 10; i++) {
		if (TopBuoyLifterTab[i].servoArm.getServoPosition() == ServoPosition::Down) {
			TopBuoyLifterTab[i].servoFinger.setPosition(ServoPosition::Down);
		}
	}
}

//Set the arm up for all arm that are actually down, and deactivate all arms.
void Big2020::handleRaise() {
	for (int i = 0; i < 10; i++) {
		if (TopBuoyLifterTab[i].servoArm.getServoPosition() == ServoPosition::Down) {
			TopBuoyLifterTab[i].servoArm.setPosition(ServoPosition::Up);
		}
	}
	delay(1000);
	for (int i = 0; i < 10; i++) {
		TopBuoyLifterTab[i].servoArm.setPosition(ServoPosition::Deactivated);
		TopBuoyLifterTab[i].getSensorStatus();
	}

	//FastLED.show();
}

void Big2020::handleRaiseFlags() {
//2time in case of lock.
	servoFlags.setPosition(ServoPosition::Up);
	delay(1000);
	servoFlags.setPosition(ServoPosition::Deactivated);
	delay(200);
	servoFlags.setPosition(ServoPosition::Up);
	delay(500);
	servoFlags.setPosition(ServoPosition::Deactivated);
}

void Big2020::handleLateralArmPhareRetracted() {
	if (big2020.color == Color::Blue) {
		servoLateralArmLeft.setPosition(ServoPosition::Up);
		delay(800);
		servoLateralArmLeft.setPosition(ServoPosition::Deactivated);
	} else {
		servoLateralArmRight.setPosition(ServoPosition::Up);
		delay(800);
		servoLateralArmRight.setPosition(ServoPosition::Deactivated);
	}
}
void Big2020::handleLateralArmPhareOut() {
	if (big2020.color == Color::Blue) {
		servoLateralArmLeft.setPosition(ServoPosition::Center);
		delay(500);
		servoLateralArmLeft.setPosition(ServoPosition::Deactivated);
	} else {
		servoLateralArmRight.setPosition(ServoPosition::Center);
		delay(500);
		servoLateralArmRight.setPosition(ServoPosition::Deactivated);
	}
}
void Big2020::handleLateralArmMancheAAirRetracted() {
	if (big2020.color != Color::Blue) {
		servoLateralArmLeft.setPosition(ServoPosition::Up);
		delay(500);
		servoLateralArmLeft.setPosition(ServoPosition::Deactivated);
	} else {
		servoLateralArmRight.setPosition(ServoPosition::Up);
		delay(500);
		servoLateralArmRight.setPosition(ServoPosition::Deactivated);
	}
}
void Big2020::handleLateralArmMancheAAirOut() {
	if (big2020.color != Color::Blue) {
		servoLateralArmLeft.setPosition(ServoPosition::Down);
		delay(500);
		servoLateralArmLeft.setPosition(ServoPosition::Deactivated);
	} else {
		servoLateralArmRight.setPosition(ServoPosition::Down);
		delay(500);
		servoLateralArmRight.setPosition(ServoPosition::Deactivated);
	}
}

void Big2020::handleBuoy(int buoyNumber) {

	BottomBuoyCatcherTab[buoyNumber].computeSensor();
	BottomBuoyCatcherTab[buoyNumber].updateLed();

	if (buoyNumber == 0) {
		for (int i = 0; i < 10; i++) {
			TopBuoyLifterTab[i].updateLed(TopBuoyLifterTab[i].getSensorStatus());
		}
		FastLED.show();
	}
}

void Big2020::handleReleaseBottomBuoy(FrontRear frontOrRear) {
	int addIndex = 0;
	if (frontOrRear != FrontRear::Front) {
		addIndex = 4;
	}
	for (int i = 0; i < 4; i++) {
		BottomBuoyCatcherTab[i + addIndex].servoCatcher.setPosition(ServoPosition::Up);
		BottomBuoyCatcherTab[i + addIndex].disarm();
	}
	delay(600);
	for (int i = 0; i < 4; i++) {
		BottomBuoyCatcherTab[i + addIndex].servoCatcher.setPosition(ServoPosition::Deactivated);
	}
}

void Big2020::handleArmBottomBuoy(FrontRear frontOrRear) {
	int addIndex = 0;
	if (frontOrRear != FrontRear::Front) {
		addIndex = 4;
	}
	for (int i = 0; i < 4; i++) {
		BottomBuoyCatcherTab[i + addIndex].arm();
	}
}

void Big2020::handleSetPushBuoy() {
	if (big2020.color != Color::Blue) {
		servoLateralArmLeft.setPositionRaw(180);
		delay(800);
		servoLateralArmLeft.setPosition(ServoPosition::Deactivated);
	} else {
		servoLateralArmRight.setPositionRaw(450);
		delay(800);
		servoLateralArmRight.setPosition(ServoPosition::Deactivated);
	}
}
void Big2020::handleSetPushBuoyRetracted() {
	if (big2020.color != Color::Blue) {
		servoLateralArmLeft.setPosition(ServoPosition::Up);
		delay(800);
		servoLateralArmLeft.setPosition(ServoPosition::Deactivated);
	} else {
		servoLateralArmRight.setPosition(ServoPosition::Up);
		delay(800);
		servoLateralArmRight.setPosition(ServoPosition::Deactivated);
	}
}



void Big2020::handleSetArm() {
	int armNumber = (commandParameters[0] - '0');
	int requestPosition = (commandParameters[1] - '0');
	ServoPosition position;
	//Ask Manu help to cast
	switch (requestPosition) {
		case 0:
			position = ServoPosition::Deactivated;
			break;
		case 1:
			position = ServoPosition::Center;
			break;
		case 2:
			position = ServoPosition::Up;
			break;
		case 3:
			position = ServoPosition::Down;
			break;
		case 4:
			position = ServoPosition::Working;
			break;
		default:
			position = ServoPosition::Deactivated;
	}
	if (armNumber == 0) {
		servoLateralArmLeft.setPosition(position);
	} else {
		servoLateralArmRight.setPosition(position);
	}
	delay(600);
}

