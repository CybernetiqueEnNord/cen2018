/*
 * CenServo.cpp
 *
 *  Created on: 16 nov. 2019
 *      Author: Robin
 */

#include "CenBottomBuoyCatcher.h"
#include "FastLED.h"

CenBottomBuoyCatcher::CenBottomBuoyCatcher(CenServo_PCA9685 &servoCatcher, SensorVL6180X &sensor, CRGB &ledAffichage, CRGB &ledLightning) :
		servoCatcher(servoCatcher), sensor(sensor), ledAffichage(ledAffichage), ledLightning(ledLightning) {
}

BottomBuoySensorStatus CenBottomBuoyCatcher::updateSensor() {
	distance = sensor.read(masterSoftWire).distance;
	if (distance == 0) {
		sensorStatus = BottomBuoySensorStatus::Error;
	} else if (distance <= THREASHOLD_BUOY_PRESENT_MM) {
		sensorStatus = BottomBuoySensorStatus::BuoyPresent;
	} else if (distance <= THREASHOLD_BUOY_UNCERTAIN_MM) {
		sensorStatus = BottomBuoySensorStatus::Uncertain;
	} else {
		sensorStatus = BottomBuoySensorStatus::Nothing;
	}
	return sensorStatus;
}

void CenBottomBuoyCatcher::updateLed() {

	if (sensorStatus == BottomBuoySensorStatus::Error) {
		if (bascule) {
			fill_solid(&ledAffichage, 1, CRGB::Red);
			bascule = false;
		} else {
			fill_solid(&ledAffichage, 1, CRGB::Black);
			bascule = true;
		}
		return;
	}

	switch (status) {
		case BottomBuoyStatus::Disarmed:
			if (sensorStatus == BottomBuoySensorStatus::BuoyPresent) {
				color1 = CRGB::Purple;
				color2 = color1;
			} else if (sensorStatus == BottomBuoySensorStatus::Uncertain) {
				color1 = CRGB::Blue;
				color2 = color1;
			} else {
				color1 = CRGB::White;
				color2 = color1;
			}
			break;
		case BottomBuoyStatus::Armed:
			if (sensorStatus == BottomBuoySensorStatus::BuoyPresent) {
				color1 = CRGB::Purple;
				color2 = CRGB::Black;
			} else if (sensorStatus == BottomBuoySensorStatus::Uncertain) {
				color1 = CRGB::Blue;
				color2 = CRGB::Black;
			} else {
				color1 = CRGB::White;
				color2 = CRGB::Black;
			}
			break;
		case BottomBuoyStatus::Locked:
			if (sensorStatus == BottomBuoySensorStatus::BuoyPresent) {
				color1 = CRGB::Yellow;
				color2 = CRGB::Yellow; //To be replaced by gobelet color sensed;
			} else if (sensorStatus == BottomBuoySensorStatus::Uncertain) {
				color1 = CRGB::Blue;
				color2 = CRGB::Yellow; //To be replaced by gobelet color sensed;
			} else {
				color1 = CRGB::Purple;
				color2 = CRGB::Yellow; //To be replaced by gobelet color sensed;
			}
			break;
	}

	if (bascule) {
		fill_solid(&ledAffichage, 1, color1);
		bascule = false;
	} else {
		fill_solid(&ledAffichage, 1, color2);
		bascule = true;
	}
}

void CenBottomBuoyCatcher::computeSensor() {
	updateSensor();

	if (sensorStatus == BottomBuoySensorStatus::Nothing) {
		if (accumulator > 0) {
			accumulator--;
		}
	} else if (sensorStatus == BottomBuoySensorStatus::Uncertain) {

	} else if (sensorStatus == BottomBuoySensorStatus::BuoyPresent) {
		if (accumulator <= THRESHOLD_ACCUMULATOR) {
			accumulator++;
		}
	} else { //error
		//accumulator = 0;
	}
	if (status == BottomBuoyStatus::Armed) {
		if (accumulator == THRESHOLD_ACCUMULATOR) {
			status = BottomBuoyStatus::Locked;
			servoCatcher.setPosition(ServoPosition::Down);
		}
	} else if (status == BottomBuoyStatus::Locked && accumulator == 0) {
		servoCatcher.setPosition(ServoPosition::Up);
		status = BottomBuoyStatus::Armed;
	}

}

void CenBottomBuoyCatcher::disarm() {
	status = BottomBuoyStatus::Disarmed;
	accumulator = 0;
	servoCatcher.setPosition(ServoPosition::Up);
	updateLed();
}
void CenBottomBuoyCatcher::arm() {
	if (status == BottomBuoyStatus::Disarmed) {
		status = BottomBuoyStatus::Armed;
		accumulator = 0;
	}
}

BottomBuoyStatus CenBottomBuoyCatcher::getStatus() {
	return status;
}

bool CenBottomBuoyCatcher::getCount() {
	return (status == BottomBuoyStatus::Locked);
}

unsigned int CenBottomBuoyCatcher::getCountColor(CenGlassColor expectedColor) {
	if (status == BottomBuoyStatus::Locked && buoyColor == expectedColor)
		return 1;
	else
		return 0;
}

