#ifndef SENSORVL6180X_H
#define SENSORVL6180X_H

#include <stdint.h>

#include "VL6180X.h"

#define DELAY_SHUTDOWN 2

struct SensorVL6180XConfig {
public:
	uint8_t index = 0;
	uint8_t address = 0;
	uint8_t xshutPin = 0;
	uint8_t scaling = 0;
	uint16_t timeout = 0;
	SensorVL6180XConfig();
	SensorVL6180XConfig(uint8_t index, uint8_t address, uint8_t xshutPin);
	void clear();
};

struct SensorVL6180XMeasure {
	unsigned long timestamp = 0;
	unsigned int distance = 0;
	unsigned int ambientLight = 0;
};

class SensorVL6180X {
private:
	SensorVL6180XConfig config;
	SensorVL6180XMeasure measure;
	bool enabled;

	void printHex(uint8_t value, bool addNewLine = true);
	template<typename T> bool setAddress(T& wire);
	void setMeasure(unsigned int distance, unsigned int ambientLight);
	void setPinModeAndValue(uint8_t index, uint8_t mode, uint8_t value);
	void setShutdown(uint8_t pin, bool value);

public:
	SensorVL6180X();
	void clearConfiguration();
	const SensorVL6180XConfig& getConfig() const;
	const SensorVL6180XMeasure& getMeasure() const;
	bool isConfigured() const;
	bool isEnabled() const;
	template<typename T> bool initialize(T& wire);
	template<typename T> const SensorVL6180XMeasure& read(T& wire);
	void setConfig(SensorVL6180XConfig &config);
	void setEnabled(bool value);
};



// Implémentation

template<typename T> bool SensorVL6180X::setAddress(T& wire) {
	Serial.print(F(":setting sensor address at index "));
	Serial.print(config.index);
	Serial.print(F(" to address 0x"));
	printHex(config.address, false);
	Serial.print(F(" with pin "));
	Serial.println(config.xshutPin);

	// reset
	uint8_t pin = config.xshutPin;
	setShutdown(pin, true);
	delay(DELAY_SHUTDOWN);
	setShutdown(pin, false);
	delay(DELAY_SHUTDOWN);

	VL6180X<T> sensor(wire);

	//sensor.init();
	sensor.setAddress(config.address);

	return sensor.last_status == 0;
}

template<typename T> bool SensorVL6180X::initialize(T& wire) {
	unsigned int error = 1;
	if (!isConfigured()) {
		goto error;
	}
	error++;

	if (!setAddress<T>(wire)) {
		goto error;
	}
	error++;

	VL6180X<T> device(wire, config.address);
	if (!device.init() || device.last_status != 0) {
		goto error;
	}
	error++;

	device.setTimeout(500);

	// Start continuous back-to-back mode (take readings as
	// fast as possible).  To use continuous timed mode
	// instead, provide a desired inter-measurement period in
	// ms (e.g. sensor.startContinuous(100)).
	device.configureDefault();
	if (device.last_status != 0) {
		goto error;
	}
	error++;

	device.startInterleavedContinuous(200);
	if (device.last_status != 0) {
		goto error;
	}
	error++;

	// Sauvegarde des valeurs non persistentes
	config.scaling = device.getScaling();
	config.timeout = device.getTimeout();

	return true;

	error: Serial.print(F("VL6180X ERROR"));
	Serial.println(error);
	setEnabled(false);
	return false;
}

template<typename T> const SensorVL6180XMeasure& SensorVL6180X::read(T& wire) {
	if (isEnabled()) {
		const SensorVL6180XConfig& config = getConfig();
		VL6180X<T> device(wire, config.address, config.scaling, config.timeout);
		unsigned int distance = device.readRangeContinuousMillimeters();
		//unsigned int ambientLight = device.readAmbientContinuous();
		if (device.last_status != 0) {
			Serial.println(F("VL6180X ERROR READ"));
		} else {
			setMeasure(distance, 0);
		}
	}
	return getMeasure();
}

#endif
