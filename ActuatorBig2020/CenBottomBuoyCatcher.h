/*
 * CenServo.h
 *
 *  Created on: 16 nov. 2019
 *      Author: zoro
 */

#ifndef CENBOTTOMBUOYCATCHER_H_
#define CENBOTTOMBUOYCATCHER_H_

#include "CenServo_PCA9685.h"
#include "SoftWire.h"
#include "SensorVL6180X.h"
#include "FastLED.h"
#include "../Commons/i2c/ActuatorBig2020Commands.h"

#define THRESHOLD_ACCUMULATOR 2
#define THREASHOLD_BUOY_PRESENT_MM 30
#define THREASHOLD_BUOY_UNCERTAIN_MM 60
#define NUMBER_OF_LED_LIGHTING 1

extern SoftWire masterSoftWire;

class CenBottomBuoyCatcher {
public:
	CenBottomBuoyCatcher(CenServo_PCA9685 &servoCatcher, SensorVL6180X &sensor, CRGB &led, CRGB &ledLightning);

public:
	CenServo_PCA9685 &servoCatcher;
	SensorVL6180X &sensor; //Channel number of the PCA9685 on which the servo is plugged in.
	CRGB &ledAffichage;
	CRGB &ledLightning;

private:
	CRGB::HTMLColorCode color1 = CRGB::Black;
	CRGB::HTMLColorCode color2 = CRGB::Black;

	CenGlassColor buoyColor = CenGlassColor::undefined;

	int accumulator = 0;
	unsigned int distance = 0;

	BottomBuoyStatus status = BottomBuoyStatus::Disarmed;
	BottomBuoySensorStatus sensorStatus = BottomBuoySensorStatus::Error;

	bool bascule = true; // to make led flash

public:
	BottomBuoySensorStatus updateSensor();
	void updateLed();
	void computeSensor();
	void disarm();
	void arm();
	BottomBuoyStatus getStatus();
	bool getCount();
	unsigned int getCountColor(CenGlassColor expectedColor);

};

#endif /* CENBOTTOMBUOYCATCHER_H_ */
