/*
 * Actuator 2020
 *
 *  Created on: 13/11/2019
 *      Author: Robin Ouallet
 */
#ifndef ACTUATOR_BIG_2020_H_
#define ACTUATOR_BIG_2020_H_

#include "Big2020.h"
#include "CenServo_PCA9685.h"
#include "CenBottomBuoyCatcher.h"
#include "FastLED.h"

#include "CenTopBuoyLifter.h"

CRGB lifterLeds[NUM_LEDS_LIFTER];

#endif //ACTUATOR_BIG_2020_H_
