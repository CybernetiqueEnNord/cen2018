#ifndef VL6180X_h
#define VL6180X_h

#include <Arduino.h>

// The Arduino two-wire interface uses a 7-bit number for the address,
// and sets the last bit correctly based on reads and writes
#define ADDRESS_DEFAULT 0b0101001

template<typename T>
class VL6180X {
public:
	// register addresses
	enum regAddr {
		IDENTIFICATION__MODEL_ID = 0x000, IDENTIFICATION__MODEL_REV_MAJOR = 0x001, IDENTIFICATION__MODEL_REV_MINOR = 0x002, IDENTIFICATION__MODULE_REV_MAJOR = 0x003, IDENTIFICATION__MODULE_REV_MINOR = 0x004, IDENTIFICATION__DATE_HI = 0x006, IDENTIFICATION__DATE_LO = 0x007, IDENTIFICATION__TIME = 0x008, // 16-bit

		SYSTEM__MODE_GPIO0 = 0x010,
		SYSTEM__MODE_GPIO1 = 0x011,
		SYSTEM__HISTORY_CTRL = 0x012,
		SYSTEM__INTERRUPT_CONFIG_GPIO = 0x014,
		SYSTEM__INTERRUPT_CLEAR = 0x015,
		SYSTEM__FRESH_OUT_OF_RESET = 0x016,
		SYSTEM__GROUPED_PARAMETER_HOLD = 0x017,

		SYSRANGE__START = 0x018,
		SYSRANGE__THRESH_HIGH = 0x019,
		SYSRANGE__THRESH_LOW = 0x01A,
		SYSRANGE__INTERMEASUREMENT_PERIOD = 0x01B,
		SYSRANGE__MAX_CONVERGENCE_TIME = 0x01C,
		SYSRANGE__CROSSTALK_COMPENSATION_RATE = 0x01E, // 16-bit
		SYSRANGE__CROSSTALK_VALID_HEIGHT = 0x021,
		SYSRANGE__EARLY_CONVERGENCE_ESTIMATE = 0x022, // 16-bit
		SYSRANGE__PART_TO_PART_RANGE_OFFSET = 0x024,
		SYSRANGE__RANGE_IGNORE_VALID_HEIGHT = 0x025,
		SYSRANGE__RANGE_IGNORE_THRESHOLD = 0x026, // 16-bit
		SYSRANGE__MAX_AMBIENT_LEVEL_MULT = 0x02C,
		SYSRANGE__RANGE_CHECK_ENABLES = 0x02D,
		SYSRANGE__VHV_RECALIBRATE = 0x02E,
		SYSRANGE__VHV_REPEAT_RATE = 0x031,

		SYSALS__START = 0x038,
		SYSALS__THRESH_HIGH = 0x03A,
		SYSALS__THRESH_LOW = 0x03C,
		SYSALS__INTERMEASUREMENT_PERIOD = 0x03E,
		SYSALS__ANALOGUE_GAIN = 0x03F,
		SYSALS__INTEGRATION_PERIOD = 0x040,

		RESULT__RANGE_STATUS = 0x04D,
		RESULT__ALS_STATUS = 0x04E,
		RESULT__INTERRUPT_STATUS_GPIO = 0x04F,
		RESULT__ALS_VAL = 0x050, // 16-bit
		RESULT__HISTORY_BUFFER_0 = 0x052, // 16-bit
		RESULT__HISTORY_BUFFER_1 = 0x054, // 16-bit
		RESULT__HISTORY_BUFFER_2 = 0x056, // 16-bit
		RESULT__HISTORY_BUFFER_3 = 0x058, // 16-bit
		RESULT__HISTORY_BUFFER_4 = 0x05A, // 16-bit
		RESULT__HISTORY_BUFFER_5 = 0x05C, // 16-bit
		RESULT__HISTORY_BUFFER_6 = 0x05E, // 16-bit
		RESULT__HISTORY_BUFFER_7 = 0x060, // 16-bit
		RESULT__RANGE_VAL = 0x062,
		RESULT__RANGE_RAW = 0x064,
		RESULT__RANGE_RETURN_RATE = 0x066, // 16-bit
		RESULT__RANGE_REFERENCE_RATE = 0x068, // 16-bit
		RESULT__RANGE_RETURN_SIGNAL_COUNT = 0x06C, // 32-bit
		RESULT__RANGE_REFERENCE_SIGNAL_COUNT = 0x070, // 32-bit
		RESULT__RANGE_RETURN_AMB_COUNT = 0x074, // 32-bit
		RESULT__RANGE_REFERENCE_AMB_COUNT = 0x078, // 32-bit
		RESULT__RANGE_RETURN_CONV_TIME = 0x07C, // 32-bit
		RESULT__RANGE_REFERENCE_CONV_TIME = 0x080, // 32-bit

		RANGE_SCALER = 0x096, // 16-bit - see STSW-IMG003 core/inc/vl6180x_def.h

		READOUT__AVERAGING_SAMPLE_PERIOD = 0x10A,
		FIRMWARE__BOOTUP = 0x119,
		FIRMWARE__RESULT_SCALER = 0x120,
		I2C_SLAVE__DEVICE_ADDRESS = 0x212,
		INTERLEAVED_MODE__ENABLE = 0x2A3,
	};

	uint8_t last_status; // status of last I2C transmission

	VL6180X(T& wire, uint8_t address = ADDRESS_DEFAULT, uint8_t scaling = 0, uint16_t timeout = 0);

	void setAddress(uint8_t new_addr);

	bool init(void);

	void configureDefault(void);

	void writeReg(uint16_t reg, uint8_t value);
	void writeReg16Bit(uint16_t reg, uint16_t value);
	void writeReg32Bit(uint16_t reg, uint32_t value);
	uint8_t readReg(uint16_t reg);
	uint16_t readReg16Bit(uint16_t reg);
	uint32_t readReg32Bit(uint16_t reg);

	void setScaling(uint8_t new_scaling);
	inline uint8_t getScaling(void) {
		return scaling;
	}

	uint8_t readRangeSingle(void);
	inline uint16_t readRangeSingleMillimeters(void) {
		return (uint16_t) scaling * readRangeSingle();
	}
	uint16_t readAmbientSingle(void);

	void startRangeContinuous(uint16_t period = 100);
	void startAmbientContinuous(uint16_t period = 500);
	void startInterleavedContinuous(uint16_t period = 500);
	void stopContinuous();

	uint8_t readRangeContinuous(void);
	inline uint16_t readRangeContinuousMillimeters(void) {
		return (uint16_t) scaling * readRangeContinuous();
	}
	uint16_t readAmbientContinuous(void);

	inline void setTimeout(uint16_t timeout) {
		io_timeout = timeout;
	}
	inline uint16_t getTimeout(void) {
		return io_timeout;
	}
	bool timeoutOccurred(void);

private:
	T& wire;
	uint8_t address;
	uint8_t scaling;
	uint8_t ptp_offset;
	uint16_t io_timeout;
	bool did_timeout;
};

#endif

// Implémentation

// Defines /////////////////////////////////////////////////////////////////////

// RANGE_SCALER values for 1x, 2x, 3x scaling - see STSW-IMG003 core/src/vl6180x_api.c (ScalerLookUP[])
static uint16_t const ScalerValues[] = { 0, 253, 127, 84 };

// Constructors ////////////////////////////////////////////////////////////////

template<typename T> VL6180X<T>::VL6180X(T& wire, uint8_t address, uint8_t scaling, uint16_t timeout) :
		wire(wire), address(address), scaling(scaling), ptp_offset(0), io_timeout(timeout) // no timeout
				, did_timeout(false) {
}

// Public Methods //////////////////////////////////////////////////////////////

template<typename T> void VL6180X<T>::setAddress(uint8_t new_addr) {
	writeReg(I2C_SLAVE__DEVICE_ADDRESS, new_addr & 0x7F);
	address = new_addr;
}

// Initialize sensor with settings from ST application note AN4545, section 9 -
// "Mandatory : private registers"
template<typename T> bool VL6180X<T>::init() {
	// Store part-to-part range offset so it can be adjusted if scaling is changed
	ptp_offset = readReg(SYSRANGE__PART_TO_PART_RANGE_OFFSET);

	if (readReg(SYSTEM__FRESH_OUT_OF_RESET) == 1) {
		scaling = 1;

		writeReg(0x207, 0x01);
		writeReg(0x208, 0x01);
		writeReg(0x096, 0x00);
		writeReg(0x097, 0xFD); // RANGE_SCALER = 253
		writeReg(0x0E3, 0x00);
		writeReg(0x0E4, 0x04);
		writeReg(0x0E5, 0x02);
		writeReg(0x0E6, 0x01);
		writeReg(0x0E7, 0x03);
		writeReg(0x0F5, 0x02);
		writeReg(0x0D9, 0x05);
		writeReg(0x0DB, 0xCE);
		writeReg(0x0DC, 0x03);
		writeReg(0x0DD, 0xF8);
		writeReg(0x09F, 0x00);
		writeReg(0x0A3, 0x3C);
		writeReg(0x0B7, 0x00);
		writeReg(0x0BB, 0x3C);
		writeReg(0x0B2, 0x09);
		writeReg(0x0CA, 0x09);
		writeReg(0x198, 0x01);
		writeReg(0x1B0, 0x17);
		writeReg(0x1AD, 0x00);
		writeReg(0x0FF, 0x05);
		writeReg(0x100, 0x05);
		writeReg(0x199, 0x05);
		writeReg(0x1A6, 0x1B);
		writeReg(0x1AC, 0x3E);
		writeReg(0x1A7, 0x1F);
		writeReg(0x030, 0x00);

		writeReg(SYSTEM__FRESH_OUT_OF_RESET, 0);
	} else {
		// Sensor has already been initialized, so try to get scaling settings by
		// reading registers.

		uint16_t s = readReg16Bit(RANGE_SCALER);

		if (s == ScalerValues[3]) {
			scaling = 3;
		} else if (s == ScalerValues[2]) {
			scaling = 2;
		} else {
			scaling = 1;
		}

		// Adjust the part-to-part range offset value read earlier to account for
		// existing scaling. If the sensor was already in 2x or 3x scaling mode,
		// precision will be lost calculating the original (1x) offset, but this can
		// be resolved by resetting the sensor and Arduino again.
		ptp_offset *= scaling;
	}

	return true;
}

// Configure some settings for the sensor's default behavior from AN4545 -
// "Recommended : Public registers" and "Optional: Public registers"
//
// Note that this function does not set up GPIO1 as an interrupt output as
// suggested, though you can do so by calling:
// writeReg(SYSTEM__MODE_GPIO1, 0x10);
template<typename T> void VL6180X<T>::configureDefault(void) {
	// "Recommended : Public registers"

	// readout__averaging_sample_period = 48
	writeReg(READOUT__AVERAGING_SAMPLE_PERIOD, 0x30);

	// sysals__analogue_gain_light = 6 (ALS gain = 1 nominal, actually 1.01 according to Table 14 in datasheet)
	writeReg(SYSALS__ANALOGUE_GAIN, 0x46);

	// sysrange__vhv_repeat_rate = 255 (auto Very High Voltage temperature recalibration after every 255 range measurements)
	writeReg(SYSRANGE__VHV_REPEAT_RATE, 0xFF);

	// sysals__integration_period = 99 (100 ms)
	// AN4545 incorrectly recommends writing to register 0x040; 0x63 should go in the lower byte, which is register 0x041.
	writeReg16Bit(SYSALS__INTEGRATION_PERIOD, 0x0063);

	// sysrange__vhv_recalibrate = 1 (manually trigger a VHV recalibration)
	writeReg(SYSRANGE__VHV_RECALIBRATE, 0x01);

	// "Optional: Public registers"

	// sysrange__intermeasurement_period = 9 (100 ms)
	writeReg(SYSRANGE__INTERMEASUREMENT_PERIOD, 0x09);

	// sysals__intermeasurement_period = 49 (500 ms)
	writeReg(SYSALS__INTERMEASUREMENT_PERIOD, 0x31);

	// als_int_mode = 4 (ALS new sample ready interrupt); range_int_mode = 4 (range new sample ready interrupt)
	writeReg(SYSTEM__INTERRUPT_CONFIG_GPIO, 0x24);

	// Reset other settings to power-on defaults

	// sysrange__max_convergence_time = 49 (49 ms)
	//writeReg(VL6180X<T>::SYSRANGE__MAX_CONVERGENCE_TIME, 0x31);
	writeReg(VL6180X<T>::SYSRANGE__MAX_CONVERGENCE_TIME, 0x30);

	// disable interleaved mode
	writeReg(INTERLEAVED_MODE__ENABLE, 0);

	// reset range scaling factor to 1x
	setScaling(1);
}

// Writes an 8-bit register
template<typename T> void VL6180X<T>::writeReg(uint16_t reg, uint8_t value) {
	wire.beginTransmission(address);
	wire.write((reg >> 8) & 0xff);  // reg high byte
	wire.write(reg & 0xff);         // reg low byte
	wire.write(value);
	last_status = wire.endTransmission();
}

// Writes a 16-bit register
template<typename T> void VL6180X<T>::writeReg16Bit(uint16_t reg, uint16_t value) {
	wire.beginTransmission(address);
	wire.write((reg >> 8) & 0xff);  // reg high byte
	wire.write(reg & 0xff);         // reg low byte
	wire.write((value >> 8) & 0xff);  // value high byte
	wire.write(value & 0xff);         // value low byte
	last_status = wire.endTransmission();
}

// Writes a 32-bit register
template<typename T> void VL6180X<T>::writeReg32Bit(uint16_t reg, uint32_t value) {
	wire.beginTransmission(address);
	wire.write((reg >> 8) & 0xff);  // reg high byte
	wire.write(reg & 0xff);         // reg low byte
	wire.write((value >> 24) & 0xff); // value highest byte
	wire.write((value >> 16) & 0xff);
	wire.write((value >> 8) & 0xff);
	wire.write(value & 0xff);         // value lowest byte
	last_status = wire.endTransmission();
}

// Reads an 8-bit register
template<typename T> uint8_t VL6180X<T>::readReg(uint16_t reg) {
	uint8_t value;

	wire.beginTransmission(address);
	wire.write((reg >> 8) & 0xff);  // reg high byte
	wire.write(reg & 0xff);         // reg low byte
	last_status = wire.endTransmission();

	wire.requestFrom(address, (uint8_t) 1);
	value = wire.read();
	wire.endTransmission();

	return value;
}

// Reads a 16-bit register
template<typename T> uint16_t VL6180X<T>::readReg16Bit(uint16_t reg) {
	uint16_t value;

	wire.beginTransmission(address);
	wire.write((reg >> 8) & 0xff);  // reg high byte
	wire.write(reg & 0xff);         // reg low byte
	last_status = wire.endTransmission();

	wire.requestFrom(address, (uint8_t) 2);
	value = (uint16_t) wire.read() << 8; // value high byte
	value |= wire.read();               // value low byte
	wire.endTransmission();

	return value;
}

// Reads a 32-bit register
template<typename T> uint32_t VL6180X<T>::readReg32Bit(uint16_t reg) {
	uint32_t value;

	wire.beginTransmission(address);
	wire.write((reg >> 8) & 0xff);  // reg high byte
	wire.write(reg & 0xff);         // reg low byte
	last_status = wire.endTransmission();

	wire.requestFrom(address, (uint8_t) 4);
	value = (uint32_t) wire.read() << 24;  // value highest byte
	value |= (uint32_t) wire.read() << 16;
	value |= (uint16_t) wire.read() << 8;
	value |= wire.read();                 // value lowest byte
	wire.endTransmission();

	return value;
}

// Set range scaling factor. The sensor uses 1x scaling by default, giving range
// measurements in units of mm. Increasing the scaling to 2x or 3x makes it give
// raw values in units of 2 mm or 3 mm instead. In other words, a bigger scaling
// factor increases the sensor's potential maximum range but reduces its
// resolution.

// Implemented using ST's VL6180X API as a reference (STSW-IMG003); see
// VL6180x_UpscaleSetScaling() in vl6180x_api.c.
template<typename T> void VL6180X<T>::setScaling(uint8_t new_scaling) {
	uint8_t const DefaultCrosstalkValidHeight = 20; // default value of SYSRANGE__CROSSTALK_VALID_HEIGHT

	// do nothing if scaling value is invalid
	if (new_scaling < 1 || new_scaling > 3) {
		return;
	}

	scaling = new_scaling;
	writeReg16Bit(RANGE_SCALER, ScalerValues[scaling]);

	// apply scaling on part-to-part offset
	writeReg(VL6180X<T>::SYSRANGE__PART_TO_PART_RANGE_OFFSET, ptp_offset / scaling);

	// apply scaling on CrossTalkValidHeight
	writeReg(VL6180X<T>::SYSRANGE__CROSSTALK_VALID_HEIGHT, DefaultCrosstalkValidHeight / scaling);

	// This function does not apply scaling to RANGE_IGNORE_VALID_HEIGHT.

	// enable early convergence estimate only at 1x scaling
	uint8_t rce = readReg(VL6180X<T>::SYSRANGE__RANGE_CHECK_ENABLES);
	writeReg(VL6180X<T>::SYSRANGE__RANGE_CHECK_ENABLES, (rce & 0xFE) | (scaling == 1));
}

// Performs a single-shot ranging measurement
template<typename T> uint8_t VL6180X<T>::readRangeSingle() {
	writeReg(SYSRANGE__START, 0x01);
	return readRangeContinuous();
}

// Performs a single-shot ambient light measurement
template<typename T> uint16_t VL6180X<T>::readAmbientSingle() {
	writeReg(SYSALS__START, 0x01);
	return readAmbientContinuous();
}

// Starts continuous ranging measurements with the given period in ms
// (10 ms resolution; defaults to 100 ms if not specified).
//
// The period must be greater than the time it takes to perform a
// measurement. See section 2.4.4 ("Continuous mode limits") in the datasheet
// for details.
template<typename T> void VL6180X<T>::startRangeContinuous(uint16_t period) {
	int16_t period_reg = (int16_t) (period / 10) - 1;
	period_reg = constrain(period_reg, 0, 254);

	writeReg(SYSRANGE__INTERMEASUREMENT_PERIOD, period_reg);
	writeReg(SYSRANGE__START, 0x03);
}

// Starts continuous ambient light measurements with the given period in ms
// (10 ms resolution; defaults to 500 ms if not specified).
//
// The period must be greater than the time it takes to perform a
// measurement. See section 2.4.4 ("Continuous mode limits") in the datasheet
// for details.
template<typename T> void VL6180X<T>::startAmbientContinuous(uint16_t period) {
	int16_t period_reg = (int16_t) (period / 10) - 1;
	period_reg = constrain(period_reg, 0, 254);

	writeReg(SYSALS__INTERMEASUREMENT_PERIOD, period_reg);
	writeReg(SYSALS__START, 0x03);
}

// Starts continuous interleaved measurements with the given period in ms
// (10 ms resolution; defaults to 500 ms if not specified). In this mode, each
// ambient light measurement is immediately followed by a range measurement.
//
// The datasheet recommends using this mode instead of running "range and ALS
// continuous modes simultaneously (i.e. asynchronously)".
//
// The period must be greater than the time it takes to perform both
// measurements. See section 2.4.4 ("Continuous mode limits") in the datasheet
// for details.
template<typename T> void VL6180X<T>::startInterleavedContinuous(uint16_t period) {
	int16_t period_reg = (int16_t) (period / 10) - 1;
	period_reg = constrain(period_reg, 0, 254);

	writeReg(INTERLEAVED_MODE__ENABLE, 1);
	writeReg(SYSALS__INTERMEASUREMENT_PERIOD, period_reg);
	writeReg(SYSALS__START, 0x03);
}

// Stops continuous mode. This will actually start a single measurement of range
// and/or ambient light if continuous mode is not active, so it's a good idea to
// wait a few hundred ms after calling this function to let that complete
// before starting continuous mode again or taking a reading.
template<typename T> void VL6180X<T>::stopContinuous() {

	writeReg(SYSRANGE__START, 0x01);
	writeReg(SYSALS__START, 0x01);

	writeReg(INTERLEAVED_MODE__ENABLE, 0);
}

// Returns a range reading when continuous mode is activated
// (readRangeSingle() also calls this function after starting a single-shot
// range measurement)
template<typename T> uint8_t VL6180X<T>::readRangeContinuous() {
	uint16_t millis_start = millis();
	while ((readReg(RESULT__INTERRUPT_STATUS_GPIO) & 0x04) == 0) {
		if (io_timeout > 0 && ((uint16_t) millis() - millis_start) > io_timeout) {
			did_timeout = true;
			return 255;
		}
	}

	uint8_t range = readReg(RESULT__RANGE_VAL);
	writeReg(SYSTEM__INTERRUPT_CLEAR, 0x01);

	return range;
}

// Returns an ambient light reading when continuous mode is activated
// (readAmbientSingle() also calls this function after starting a single-shot
// ambient light measurement)
template<typename T> uint16_t VL6180X<T>::readAmbientContinuous() {
	uint16_t millis_start = millis();
	while ((readReg(RESULT__INTERRUPT_STATUS_GPIO) & 0x20) == 0) {
		if (io_timeout > 0 && ((uint16_t) millis() - millis_start) > io_timeout) {
			did_timeout = true;
			return 0;
		}
	}

	uint16_t ambient = readReg16Bit(RESULT__ALS_VAL);
	writeReg(SYSTEM__INTERRUPT_CLEAR, 0x02);

	return ambient;
}

// Did a timeout occur in one of the read functions since the last call to
// timeoutOccurred()?
template<typename T> bool VL6180X<T>::timeoutOccurred() {
	bool tmp = did_timeout;
	did_timeout = false;
	return tmp;
}
