/*
 * Water2018.h
 *
 *  Created on: 21 avr. 2018
 *      Author: Emmanuel
 */

#ifndef BIG2020_H_
#define BIG2020_H_

#include "Constants.h"
#include "Servo.h"
#include "Actuator.h"
#include "Wire.h"
#include "SoftWire.h"
#include "PCA9685.h"
#include "CenServo_PCA9685.h"
#include "CenBottomBuoyCatcher.h"
#include "FastLED.h"
#include "SensorVL6180X.h"

#include "../Commons/i2c/ActuatorBig2020Commands.h"
#include "CenTopBuoyLifter.h"

#define slave Wire

#define NUM_LEDS_LIFTER 24
#define LIFTER_LED_PIN 2

enum class Action {
	None, SetInitialPosition, SetCode, WaitReadyTest, TestAllServoAreWorking, TestHandleDance, Kill, Servo, FrontRelease, RearRelease, FrontDown, RearDown, Hold, Raise, ReadyToPickup, ReadyToPickupIndividual, RaiseFlags, LateralArmPhareRetracted, LateralArmPhareOut, LateralArmMancheAAirOut, LateralArmMancheAAirRetracted, FrontBottomBuoyReleaseAndDisarm, RearBottomBuoyReleaseAndDisarm, FrontBottomBuoyArm, RearBottomBuoyArm, SetArm, ArmPushBuoy, ArmPushBuoyRetracted
};

#define INPUT_BUFFER_SIZE 8

class Big2020: public Actuator {
private:
	static uint8_t slaveParametersCount;
	static uint8_t slaveParameters[INPUT_BUFFER_SIZE];
	static bool i2cAcknowledged;
	unsigned int BuoyNumber = 0;

	static void i2cHandleSlaveInput(int bytesCount);
	static void i2cHandleSlaveRequest();
	static void i2cHandleCommand();

	uint8_t i2cSlaveAddress;
	SoftWire &master;
	Color color = Color::Undefined;
	Code code = Code::Undefined;
	Action command = Action::None;
	char commandParameters[INPUT_BUFFER_SIZE];

	void handleAction();

	void i2cScanMasterBus();
	void i2cSendStatus();
	void initializeActuators();
	void initializeMasterI2C();
	void initializeSlaveI2C();
	bool isBusy() const;

	void printI2CAddress();

	// Servo
	void handleServoCommand();
	void handleCodeCommand();

	// arr�t d�finitif
	void kill();
	void caseLifterservoArm(int servoIndex, int servoPosition);
	void caseLifterServoFinger(int servoIndex, int servoPosition);

protected:
	virtual void enterDebug();
	virtual void runInitialization();
	virtual void runStart();
	virtual void runMain();
	virtual void runDebug();
	virtual void serialParseInput(const char *buffer, int length);
	void switchI2COrSerialCommand(bool isI2C, const char *buffer, int length);
	void setServoListReadyToPickup(int *servoIndexList, int size);

public:
	//Functionnal actions:
	void handleStartPosition();
	void handleTestAllServoAreWorking();
	void handleDance();
	void handleRelease(FrontRear frontOrRear);
	void handleSetDown(FrontRear frontOrRear);
	void handleHold();
	void handleRaise();
	void handleReadyToPickup();
	void handleReadyToPickupIndividual();
	void handleRaiseFlags();
	void handleLateralArmPhareRetracted();
	void handleLateralArmPhareOut();
	void handleLateralArmMancheAAirRetracted();
	void handleLateralArmMancheAAirOut();

	//Sub command:
	void setServoPosition(ServoType servoType, int servoIndex, ServoPosition requestedPosition);
	void handleBuoy(int buoyNumber);
	void handleReleaseBottomBuoy(FrontRear frontOrRear);
	void handleArmBottomBuoy(FrontRear frontOrRear);
	void handleSetPushBuoy();
	void handleSetPushBuoyRetracted();
	void handleSetArm();

public:
	Big2020(uint8_t address, SoftWire &master);
};

extern Big2020 big2020;

extern PCA9685 pwmController1;
extern PCA9685 pwmController2;

extern CenServo_PCA9685 servoFlags;

extern ServoPosition servoArmPosition[SERVO_COUNT / 2];
extern ServoPosition servoFingerPosition[SERVO_COUNT / 2];
extern ServoPosition servoLateralArmPosition[2];
extern ServoPosition servoGlassRetainerPositionn[4];
extern ServoPosition servoFlagPosition[1];

extern CenServo_PCA9685 servoLateralArmLeft;
extern CenServo_PCA9685 servoLateralArmRight;

extern CRGB lifterLeds[NUM_LEDS_LIFTER];

extern SensorVL6180X proximitySensors[8];

extern CenTopBuoyLifter TopBuoyLifterTab[10];
extern CenBottomBuoyCatcher BottomBuoyCatcherTab[8];

#endif /* BIG2020_H_ */
