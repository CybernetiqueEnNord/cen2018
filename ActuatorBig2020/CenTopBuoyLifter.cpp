/*
 * CenServo.cpp
 *
 *  Created on: 16 nov. 2019
 *      Author: Robin
 */

#include "CenTopBuoyLifter.h"

#include "FastLED.h"

CenTopBuoyLifter::CenTopBuoyLifter(CenServo_PCA9685 &servoArm, CenServo_PCA9685 &servoFinger, int pinSwitchSensor, CRGB &led) :
		servoArm(servoArm), servoFinger(servoFinger), pinSwitchSensor(pinSwitchSensor), led(led) {
}

bool CenTopBuoyLifter::getSensorStatus() {
	bool tmp = digitalRead(pinSwitchSensor) == HIGH;
	return tmp;
}

void CenTopBuoyLifter::updateLed(bool sensorStatus) {
	if (!sensorStatus) {
		fill_solid(&led, 1, CRGB::Yellow);
	} else {
		if (color == CenGlassColor::undefined) {
			fill_solid(&led, 1, CRGB::Purple);
		} else if (color == CenGlassColor::Red) {
			fill_solid(&led, 1, CRGB::Red);
		} else if (color == CenGlassColor::Green) {
			fill_solid(&led, 1, CRGB::Green);
		}
	}
}

