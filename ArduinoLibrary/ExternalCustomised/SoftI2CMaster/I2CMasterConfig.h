// Configuration

#ifdef ARDUINO_AVR_MEGA2560
#pragma message("SOFTWIRE ARDUINO MEGA")
// A0 on MEGA
#define SDA_PORT PORTF
#define SDA_PIN 0
// A1 on MEGA
#define SCL_PORT PORTF
#define SCL_PIN 1
#else
#pragma message("SOFTWIRE ARDUINO NANO")
// A0 on NANO
#define SDA_PORT PORTC
#define SDA_PIN 0
// A1 on NANO
#define SCL_PORT PORTC
#define SCL_PIN 1
#endif

#define I2C_TIMEOUT 5
#define I2C_FASTMODE 1
