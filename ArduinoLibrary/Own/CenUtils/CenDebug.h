#ifndef DEBUG_H
#define DEBUG_H

#ifdef CENDEBUG

#include "HardwareSerial.h"

#define DBG(x) do { Serial.print(':'); Serial.println(x); } while(0)
#define DBG_skv(s, k, v) do { Serial.print(':'); Serial.print(s); Serial.print('.'); Serial.print(k); Serial.print('='); Serial.println(v); } while(0)
#define DBG_b(b) DBG(b ? "OK" : "KO")
#define DBG_p(printable) do { printable.printTo(Serial); } while(0)

#else

#define DBG(x) do { } while(0)
#define DBG_skv(s, k, v) do { } while(0)
#define DBG_b(b) do { } while(0)
#define DBG_p(printable) do { } while(0)

#endif

#endif
