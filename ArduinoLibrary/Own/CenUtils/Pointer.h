/*
 * Pointer.h
 *
 *  Created on: 5 nov. 2017
 *      Author: Emmanuel
 */

#ifndef UTILS_POINTER_H_
#define UTILS_POINTER_H_

#include <cstddef>

template<typename T>
class Pointer {
private:
	T* pointer = nullptr;

public:
	Pointer() {
	}
	Pointer(T* value) :
			pointer(value) {
	}
	T* operator->() const {
		return pointer;
	}
	T& operator*() const {
		return *pointer;
	}
	void operator=(Pointer<T> value) {
		pointer = value.pointer;
	}
	void clear() {
		pointer = nullptr;
	}
	bool isValid() const {
		return pointer != nullptr;
	}
};

#endif /* UTILS_POINTER_H_ */
