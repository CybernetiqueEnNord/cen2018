#include "CenOTA.h"

#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

CenOTA::CenOTA() {
}

void CenOTA::handle()
{
ArduinoOTA.handle();
}

void CenOTA::setupOTA(int endIP)
{
WiFi.mode(WIFI_STA);
  IPAddress local_IP(IP0, IP1, IP2, endIP);
  IPAddress gateway(IP0, IP1, IP2, 1);
  IPAddress subnet(255, 255, 0, 0);
  IPAddress primaryDNS(8, 8, 8, 8); 
  IPAddress secondaryDNS(8, 8, 4, 4);

  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
  Serial.println("STA Failed to configure");
  }
  WiFi.begin(ssid, password);

ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
}

void CenOTA::beginOTA(){
	int wifiTimeout=0;
  while (WiFi.status() != WL_CONNECTED && wifiTimeout < 50)
  {
    wifiTimeout++;
    delay(200);
    Serial.print(".");
  }
  if(WiFi.status() == WL_CONNECTED)
  {
    Serial.println("Wifi connected");
    Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  }
  else{
    Serial.print("Wifi NOT CONNECTED!");
  }

  ArduinoOTA.begin();
}