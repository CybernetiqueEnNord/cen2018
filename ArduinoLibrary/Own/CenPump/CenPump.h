/*
 * CenPump.h
 *
 *  Created on: 16 nov. 2019
 *      Author: zoro
 */

#ifndef CENPUMP_H_
#define CENPUMP_H_

#include "Arduino.h"
#include "Servo.h"


class CenPump {

private:
	int pinNumber;
	bool pumpState=false; //false=off

public:
	CenPump(int pinNumber);
	void setPump(bool state);
	bool getState();
};

#endif /* CENPUMP_H_ */
