/*
 * CenPump.cpp
 *
 *  Created on: 16 nov. 2019
 *      Author: zoro
 */

#include "CenPump.h"

CenPump::CenPump(int pinNumber) :
		pinNumber(pinNumber) {

	pinMode(pinNumber, OUTPUT);
	digitalWrite(pinNumber, LOW);
}

void CenPump::setPump(bool state) {
	this->pumpState = state;
	digitalWrite(this->pinNumber, (state) ? HIGH : LOW);

}

bool CenPump::getState() {
	return this->pumpState;
}

