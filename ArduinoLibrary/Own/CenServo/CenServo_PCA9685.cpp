/*
 * CenServoPCA9685.cpp
 *
 *  Created on: 16 nov. 2019
 *      Author: zoro
 */

#include "CenServo_PCA9685.h"

CenServo_PCA9685::CenServo_PCA9685(PCA9685 &pwmController, int channelNumber, CenServoPositions positions) :
		pwmController(pwmController), channelNumber(channelNumber), positions(positions) {

}

void CenServo_PCA9685::setPosition(ServoPosition position) {



	switch (position) {
		case ServoPosition::Deactivated:
			setPositionRaw(0);
			break;
		case ServoPosition::Center:
			setPositionRaw(positions.centeredPosition);
			break;
		case ServoPosition::Up:
			setPositionRaw(positions.upPosition);
			break;
		case ServoPosition::Down:
			setPositionRaw(positions.downPosition);
			break;
		case ServoPosition::Working:
			setPositionRaw(positions.workingPosition);
			break;
		default:
			break;
	}

	if (position != ServoPosition::Deactivated) {
		currentPosition = position;
	}

}

void CenServo_PCA9685::setPositionRaw(int position) {
	if (position == 0) {
		pwmController.setChannelOff(channelNumber);
	} else {
		pwmController.setChannelOn(channelNumber);
		delay(1);
		pwmController.setChannelOn(channelNumber);
	}
	pwmController.setChannelPWM(channelNumber, position);
	delay(1);
	pwmController.setChannelPWM(channelNumber, position);
}

ServoPosition CenServo_PCA9685::getServoPosition() {
	return currentPosition;
}

