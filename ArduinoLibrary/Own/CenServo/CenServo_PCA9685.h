/*
 * CenServoPCA9685.h
 *
 *  Created on: 16 nov. 2019
 *      Author: zoro
 */

#ifndef CENSERVO_PCA9685_H_
#define CENSERVO_PCA9685_H_

#include "CenServo.h"
#include "PCA9685.h"

class CenServo_PCA9685: public CenServo {

private:
	PCA9685 &pwmController;
	int channelNumber; //Channel number of the PCA9685 on which the servo is plugged in.
	CenServoPositions positions;
	ServoPosition currentPosition = ServoPosition::Deactivated;

public:
	CenServo_PCA9685(PCA9685 &pwmController, int channelNumber, CenServoPositions positions); //Positions are in 0-4096 (pwm on 12 bits)

	virtual void setPosition(ServoPosition position) override;

	virtual void setPositionRaw(int position) override;

	virtual ServoPosition getServoPosition() override;
};

#endif /* CENSERVO_PCA9685_H_ */
