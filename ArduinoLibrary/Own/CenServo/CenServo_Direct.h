/*
 * CenServoPCA9685.h
 *
 *  Created on: 16 nov. 2019
 *      Author: zoro
 */

#ifndef CENSERVO_DIRECT_H_
#define CENSERVO_DIRECT_H_

#include "CenServo.h"
#include "Servo.h"

class CenServo_Direct: public CenServo {

private:
	int pinNumber; //Channel number of the PCA9685 on which the servo is plugged in.
	CenServoPositions positions;
	ServoPosition currentPosition = ServoPosition::Deactivated;
	Servo servo;

public:
	CenServo_Direct(int pinNumber, CenServoPositions positions); //Positions are in 0-4096 (pwm on 12 bits)

	virtual void setPosition(ServoPosition position) override;

	virtual void setPositionRaw(int position) override;

	ServoPosition getServoPosition() override;
};

#endif /* CENSERVO_DIRECT_H_ */
