/*
 * CenServoPCA9685.cpp
 *
 *  Created on: 16 nov. 2019
 *      Author: zoro
 */

#include "CenServo_Direct.h"

CenServo_Direct::CenServo_Direct(int pinNumber, CenServoPositions positions) :
		pinNumber(pinNumber), positions(positions) {
}

void CenServo_Direct::setPosition(ServoPosition position) {

	currentPosition = position;

	switch (position) {
		case ServoPosition::Deactivated:
			setPositionRaw(0);
			break;
		case ServoPosition::Center:
			setPositionRaw(positions.centeredPosition);
			break;
		case ServoPosition::Up:
			setPositionRaw(positions.upPosition);
			break;
		case ServoPosition::Down:
			setPositionRaw(positions.downPosition);
			break;
		case ServoPosition::Working:
			setPositionRaw(positions.workingPosition);
			break;
		case ServoPosition::Depose:
			setPositionRaw(positions.deposePosition);
			break;
		default:
			break;
	}
}

void CenServo_Direct::setPositionRaw(int position) {
	if (position == 0) {
		servo.detach();
	} else {
		servo.attach(pinNumber);
	}
	servo.write(position);
}

ServoPosition CenServo_Direct::getServoPosition() {
	return currentPosition;
}

