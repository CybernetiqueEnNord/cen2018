/*
 * CenServo.h
 *
 *  Created on: 16 nov. 2019
 *      Author: zoro
 */

#ifndef CENSERVO_H_
#define CENSERVO_H_

#include "../../../Commons/i2c/CommonCommands.h"

struct CenServoPositions {
	int downPosition;
	int centeredPosition;
	int upPosition;
	int workingPosition = 0;
	int deposePosition = 0;
	CenServoPositions(int downPosition, int centeredPosition, int upPosition) :
			downPosition(downPosition), centeredPosition(centeredPosition), upPosition(upPosition), workingPosition(0) {
	}

	CenServoPositions(int downPosition, int centeredPosition, int upPosition, int workingPosition) :
			downPosition(downPosition), centeredPosition(centeredPosition), upPosition(upPosition), workingPosition(workingPosition) {
	}

	CenServoPositions(int downPosition, int centeredPosition, int upPosition, int workingPosition, int deposePosition) :
			downPosition(downPosition), centeredPosition(centeredPosition), upPosition(upPosition), workingPosition(workingPosition), deposePosition(deposePosition) {
	}

};

class CenServo {
public:
	CenServo();

	virtual void setPosition(ServoPosition position) = 0;
	virtual void setPositionRaw(int position) = 0;
	virtual ServoPosition getServoPosition() =0;

};

#endif /* CENSERVO_H_ */
