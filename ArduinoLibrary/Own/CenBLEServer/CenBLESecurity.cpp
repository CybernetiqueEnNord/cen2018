#include "CenBLESecurity.h"

#include "CenBLEConfig.h"
#include "CenDebug.h"

uint32_t CenBLESecurityCallbacks::onPassKeyRequest()
{
    DBG_skv(F("BLE"), F("security"), F("pass key request"));
    return BLE_PIN;
}

void CenBLESecurityCallbacks::onPassKeyNotify(uint32_t pass_key)
{
    (void)pass_key;
    DBG_skv(F("BLE"), F("security"), F("pass key notify"));
}

bool CenBLESecurityCallbacks::onSecurityRequest()
{
    DBG_skv(F("BLE"), F("security"), F("security request"));
    return true;
}

void CenBLESecurityCallbacks::onAuthenticationComplete(esp_ble_auth_cmpl_t status)
{
    DBG_skv(F("BLE"), F("auth status"), status.success ? F("success") : F("failure"));
    if (!status.success)
    {
        DBG_skv(F("BLE"), F("auth reason"), status.fail_reason);
    }
}

bool CenBLESecurityCallbacks::onConfirmPIN(uint32_t pin)
{
    DBG_skv(F("BLE"), F("security"), F("confirm pin"));
    return pin == BLE_PIN;
}
