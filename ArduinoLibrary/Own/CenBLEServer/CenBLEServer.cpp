/*
 * CenBLEServer.cpp
 *
 *  Created on: 17 nov. 2019
 *      Author: emmanuel
 */

#include "CenBLEServer.h"

#include "arrays.h"
#include "BLE2902.h"
#include "BLESecurity.h"
#include "CenBLEConfig.h"

CenBLEServer::CenBLEServer() {
}

void CenBLEServer::setStaticPIN(uint32_t pin) {
	esp_ble_gap_set_security_param(ESP_BLE_SM_SET_STATIC_PASSKEY, &pin, sizeof(pin));
	esp_ble_io_cap_t iocap = ESP_IO_CAP_OUT;
	esp_ble_gap_set_security_param(ESP_BLE_SM_IOCAP_MODE, &iocap, sizeof(iocap));
}

void CenBLEServer::initialize(const String &deviceName, const String &baseUUID) {
	BLEDevice::init(deviceName.c_str());

	this->baseUUID = baseUUID;
	server = BLEDevice::createServer();
	server->setCallbacks(this);
	this->baseUUID[7] = '0';
	BLEUUID serviceUUID(this->baseUUID.c_str());
	service = server->createService(serviceUUID);
	this->baseUUID[7] = '1';
	characteristicIn = service->createCharacteristic(this->baseUUID.c_str(), BLECharacteristic::PROPERTY_WRITE);
	characteristicIn->setCallbacks(this);
	this->baseUUID[7] = '2';
	characteristicOut = service->createCharacteristic(this->baseUUID.c_str(), BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);
	BLE2902 *descriptor = new BLE2902();
	descriptor->setAccessPermissions(ESP_GATT_PERM_READ_ENCRYPTED | ESP_GATT_PERM_WRITE_ENCRYPTED);
	characteristicOut->addDescriptor(descriptor);

	setStaticPIN(BLE_PIN);

	service->start();

	BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
	pAdvertising->addServiceUUID(serviceUUID);
	pAdvertising->setScanResponse(true);
	pAdvertising->setMinPreferred(0x06); // functions that help with iPhone connections issue
	pAdvertising->setMinPreferred(0x12);
	BLEDevice::startAdvertising();

	initialized = true;
}

void CenBLEServer::loop() {
	if (!initialized) {
		return;
	}
}

void CenBLEServer::onConnect(BLEServer *pServer) {
	(void) pServer;
	clientsCount++;
}

void CenBLEServer::onDisconnect(BLEServer *pServer) {
	(void) pServer;
	clientsCount--;
}

void CenBLEServer::onWrite(BLECharacteristic *pCharacteristic) {
	// relai vers le gestionnaire
	if (handler.isValid()) {
		std::string data = pCharacteristic->getValue();
		const uint8_t *array = reinterpret_cast<const uint8_t *>(&data[0]);
		handler->inputCallback(array, data.length());
	}
}

void CenBLEServer::setEventHandler(CenBLEServerEventHandler &handler) {
	this->handler = Pointer<CenBLEServerEventHandler>(&handler);
}

void CenBLEServer::send(const uint8_t *data, size_t length) {
	characteristicOut->setValue((uint8_t *) data, length);
	characteristicOut->notify(true);
}

void CenBLEServer::send(String text) {
	const uint8_t *data = (const uint8_t *) text.c_str();
	unsigned int l = text.length();
	send(data, l);
}
