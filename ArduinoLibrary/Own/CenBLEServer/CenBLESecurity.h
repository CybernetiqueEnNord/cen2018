#ifndef CENBLESECURITY_H
#define CENBLESECURITY_H

#include "BLEDevice.h"

class CenBLESecurityCallbacks : public BLESecurityCallbacks
{
public:
    virtual uint32_t onPassKeyRequest() override;
    virtual void onPassKeyNotify(uint32_t pass_key) override;
    virtual bool onSecurityRequest() override;
    virtual void onAuthenticationComplete(esp_ble_auth_cmpl_t) override;
    virtual bool onConfirmPIN(uint32_t pin) override;
};

#endif
