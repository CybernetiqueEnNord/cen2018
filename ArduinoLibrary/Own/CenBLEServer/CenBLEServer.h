/*
 * CenBLEServer.h
 *
 *  Created on: 17 nov. 2019
 *      Author: emmanuel
 */

#ifndef CENBLESERVER_H_
#define CENBLESERVER_H_

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <WString.h>

#include "Pointer.h"

class CenBLEServerEventHandler {
public:
	virtual void inputCallback(const uint8_t *data, size_t length) = 0;
};

class CenBLEServer: public BLEServerCallbacks, public BLECharacteristicCallbacks {
private:
	String baseUUID;
	int clientsCount = 0;
	BLEServer *server;
	BLEService *service;
	BLECharacteristic *characteristicIn;
	BLECharacteristic *characteristicOut;
	bool initialized = false;
	Pointer<CenBLEServerEventHandler> handler;

	void setStaticPIN(uint32_t pin);

protected:
	virtual void onConnect(BLEServer* pServer) override;
	virtual void onDisconnect(BLEServer* pServer) override;
	virtual void onWrite(BLECharacteristic* pCharacteristic) override;

public:
	CenBLEServer();
	void initialize(const String &deviceName, const String &baseUUID);
	void loop();
	void setEventHandler(CenBLEServerEventHandler &handler);
	void send(const uint8_t* data, size_t length);
	void send(String text);
};

#endif /* CENBLESERVER_H_ */
