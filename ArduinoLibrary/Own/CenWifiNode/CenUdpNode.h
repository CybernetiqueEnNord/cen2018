#ifndef CENUDPNODE_H
#define CENUDPNODE_H

#include <IPAddress.h>
#include <WiFiUdp.h>

class CenUdpNodeCallback;

class CenUdpNode
{
    friend class CenUdpNodeCallback;

private:
    WiFiUDP &udp;
    IPAddress &hubIP;
    uint16_t port;

    bool connected = false;

    uint8_t outBuffer[20];
    uint8_t inBuffer[20];
    size_t outBufferLength = 0;
    CenUdpNodeCallback *callback = nullptr;

    unsigned long aliveTimestamp = 0;

    void receive();
    void sendAlive();

public:
    CenUdpNode(WiFiUDP &udp, IPAddress &hubIP, uint16_t port);
    CenUdpNode &operator=(const CenUdpNode &source);
    void loop();
    void send(const char *data, size_t length);
    void send(String text);
    void setCallback(CenUdpNodeCallback *callback);
};

class CenUdpNodeCallback
{
    friend void CenUdpNode::receive();

protected:
    virtual void handleInput(const char *data, size_t length);
};

#endif