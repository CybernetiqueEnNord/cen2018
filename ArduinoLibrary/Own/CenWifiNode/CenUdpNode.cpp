#include "CenUdpNode.h"

#include <WiFi.h>

#include "CenDebug.h"

CenUdpNode::CenUdpNode(WiFiUDP &udp, IPAddress &hubIP, uint16_t port) : udp(udp), hubIP(hubIP), port(port)
{
}

CenUdpNode &CenUdpNode::operator=(const CenUdpNode &source)
{
    if (this != &source)
    {
        udp = source.udp;
        hubIP = source.hubIP;
        port = source.port;
    }
    return *this;
}

void CenUdpNode::send(const char *data, size_t length)
{
    if (!WiFi.isConnected())
    {
        DBG_skv(F("udpnode"), F("send"), F("not connected"));
        outBufferLength = length;
        memcpy(outBuffer, data, max(length, sizeof outBuffer));
        return;
    }
    udp.beginPacket(hubIP, port);
    if (length > 0)
    {
        udp.write((const uint8_t *)data, length);
    }
    udp.endPacket();
}

void CenUdpNode::send(String text)
{
    unsigned int l = text.length();
    if (l == 0)
    {
        return;
    }
    const uint8_t *data = (const uint8_t *)text.c_str();
    send((const char *)data, l);
}

void CenUdpNode::receive()
{
    int len = udp.parsePacket();
    if (len == 0)
    {
        return;
    }
    size_t size = udp.readBytes(inBuffer, sizeof(inBuffer));
    if (callback)
    {
        callback->handleInput((const char *)inBuffer, size);
    }
}

void CenUdpNode::setCallback(CenUdpNodeCallback *callback)
{
    this->callback = callback;
}

void CenUdpNode::loop()
{
    bool connected = WiFi.isConnected();

    if (connected != this->connected)
    {
        DBG_skv(F("udpnode"), F("connected"), connected ? F("YES") : F("NO"));
        if (connected && (outBufferLength > 0))
        {
            send((const char *)outBuffer, outBufferLength);
            outBufferLength = 0;
        }
    }
    this->connected = connected;
    if (!connected)
    {
        return;
    }

    unsigned long now = millis();

    receive();
    if (now - aliveTimestamp > 5000)
    {
        aliveTimestamp = now;
        sendAlive();
        DBG("alive");
    }
}

void CenUdpNode::sendAlive()
{
    const char data[] = "ping";
    send(data, sizeof(data));
}