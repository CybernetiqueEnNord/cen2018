#ifndef CENUDPSERVER_H
#define CENUDPSERVER_H

#include <IPAddress.h>
#include <WiFiUdp.h>

#include "CenUdpClient.h"

class CenUdpServer
{
private:
    WiFiUDP &udp;

    CenUdpClient **clients;
    size_t clientsCount;

    char udpBuffer[20];

public:
    CenUdpServer(WiFiUDP &udp);
    void handleInput();
    void setClients(CenUdpClient **clients, size_t clientsCount);
};

#endif
