#include "CenUdpClient.h"

#include "arrays.h"
#include "CenDebug.h"

#define CONNECTION_TIMEOUT 6000

CenUdpClient::CenUdpClient(WiFiUDP &udp, IPAddress ip) : udp(udp), ip(ip)
{
}

void CenUdpClient::send(const char *data, size_t length)
{
    udp.beginPacket();
    udp.write((const uint8_t *)data, length);
    udp.endPacket();
}

void CenUdpClient::clearInput()
{
    setInput(nullptr, 0);
}

bool CenUdpClient::isConnected() const
{
    unsigned long now = millis();
    bool connected = (timestamp > 0) && (now - timestamp < CONNECTION_TIMEOUT);
    return connected;
}

const char *CenUdpClient::getInput(size_t &length) const
{
    length = inputLength;
    return input;
}

bool CenUdpClient::hasInput() const
{
    return inputLength > 0;
}

void CenUdpClient::setInput(const char *data, size_t length)
{
    inputLength = length;
    if (data != nullptr)
    {
        memcpy(input, data, std::min(ARRAY_SIZE(input), length));
    }
    setConnected(true);
}

const IPAddress &CenUdpClient::getAddress() const
{
    return ip;
}

void CenUdpClient::setConnected(bool connected)
{
    timestamp = connected ? millis() : 0;
}

void CenUdpClient::connect()
{
}