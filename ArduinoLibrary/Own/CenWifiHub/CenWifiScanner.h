#ifndef CEN_WIFI_SCANNER_H
#define CEN_WIFI_SCANNER_H

class CenWifiScanner
{
public:
    CenWifiScanner();
    bool isScanning() const;
    void loop();
};

#endif