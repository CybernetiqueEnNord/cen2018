#include "CenUdpServer.h"

#include "CenDebug.h"

CenUdpServer::CenUdpServer(WiFiUDP &udp) : udp(udp)
{
}

void CenUdpServer::handleInput()
{
    int len = udp.parsePacket();
    if (len == 0)
    {
        return;
    }
    int udpDataLength = udp.read(udpBuffer, sizeof(udpBuffer));
    DBG_skv(F("udpserver"), F("input.len"), udpDataLength);
    IPAddress udpSource = udp.remoteIP();
    DBG_skv(F("udpserver"), F("input.ip"), udpSource.toString());
    for (int i = 0; i < clientsCount; i++) {
        CenUdpClient *client = clients[i];
        if (client->getAddress() == udpSource) {
            client->setInput(udpBuffer, udpDataLength);
        }
    }
}

void CenUdpServer::setClients(CenUdpClient **clients, size_t clientsCount)
{
    this->clients = clients;
    this->clientsCount = clientsCount;
}