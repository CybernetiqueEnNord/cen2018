#ifndef CENUDPCLIENT_H
#define CENUDPCLIENT_H

#include <IPAddress.h>
#include <WiFiUdp.h>

class CenUdpClient
{
private:
    WiFiUDP &udp;
    IPAddress ip;

    unsigned long timestamp = 0;

    char input[20];
    size_t inputLength = 0;

public:
    CenUdpClient(WiFiUDP &udp, IPAddress ip);
    void clearInput();
    void connect();
    bool isConnected() const;
    const IPAddress &getAddress() const;
    const char *getInput(size_t &length) const;
    bool hasInput() const;
    void send(const char *data, size_t length = 0);
    void setConnected(bool connected);
    void setInput(const char *data, size_t length);
};

#endif