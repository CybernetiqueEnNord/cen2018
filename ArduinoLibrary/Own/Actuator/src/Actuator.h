#ifndef PROGRAM_H
#define PROGRAM_H

#include "Arduino.h"

#define ARRAY_SIZE(x) (sizeof x / sizeof(x[0]))

#define COMMAND_SERIAL_EXIT 'x'

enum class State {
	Init, Start, Main, Debug
};

class Actuator {
private:
	State state = State::Init;
	void serialHandleInput();

protected:
	static PROGMEM const char * const versionString;

	virtual void enterDebug();
	virtual void exitDebug();
	virtual void runInitialization();
	virtual void runStart();
	virtual void runMain();
	virtual void runDebug();
	virtual void serialParseInput(const char *buffer, int length);
	void serialPrintError();
	void serialPrintHex(uint8_t value, bool addNewLine = true);
	void serialPrintSuccess();
	void serialPrintVersion();
	State getState();

public:
	Actuator();
	void init();
	void loop();
};

#endif
