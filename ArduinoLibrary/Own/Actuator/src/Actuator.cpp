#include "Actuator.h"

Actuator::Actuator() {
}

void Actuator::serialPrintVersion() {
	Serial.println(versionString);
}

void Actuator::init() {
	serialPrintVersion();
}

void Actuator::runInitialization() {
	state = State::Start;
}

void Actuator::runStart() {
	state = State::Main;
}

void Actuator::runMain() {
	if (Serial.available()) {
		if (Serial.find("d\n")) {
			enterDebug();
			return;
		}
	}
}

void Actuator::serialHandleInput() {
	char buffer[10];
	int n = Serial.readBytesUntil('\n', buffer, ARRAY_SIZE(buffer) - 1);
	if (n > 0) {
		buffer[n] = 0;
		serialParseInput(buffer, n);
	}
}

void Actuator::runDebug() {
	if (Serial.available()) {
		serialHandleInput();
	}
}

void Actuator::loop() {
	switch (state) {
		case State::Init:
			runInitialization();
			break;

		case State::Start:
			runStart();
			break;

		case State::Main:
			runMain();
			break;

		case State::Debug:
			runDebug();
			break;
	}
}

void Actuator::enterDebug() {
	state = State::Debug;
	Serial.println(F(":entering debug mode"));
}

void Actuator::exitDebug() {
	state = State::Start;
	Serial.println(F(":exiting debug mode"));
}

void Actuator::serialPrintHex(uint8_t value, bool addNewLine) {
	if (value < 16) {
		Serial.print('0');
	}
	Serial.print(value, HEX);
	if (addNewLine) {
		Serial.println();
	}
}

void Actuator::serialPrintError() {
	Serial.println(F("ERROR"));
}

void Actuator::serialPrintSuccess() {
	Serial.println(F("OK"));
}

void Actuator::serialParseInput(const char *buffer, int length) {
	switch (buffer[0]) {
		case COMMAND_SERIAL_EXIT:
			exitDebug();
			break;

		default:
			goto error;
			break;
	}
	return;

	error: serialPrintError();
}

State Actuator::getState() {
	return state;
}

